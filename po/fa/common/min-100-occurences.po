# Common msgids
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-10-02 12:39+0200\n"
"PO-Revision-Date: 2021-07-25 14:07+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr ""

#: debian-bookworm debian-unstable opensuse-leap-15-6 archlinux fedora-39
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "*"
msgstr "*"

#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ""

#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed mageia-cauldron archlinux debian-bullseye
#, no-wrap
msgid "AUTHOR"
msgstr "نویسنده"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed fedora-38
#: debian-bullseye
#, no-wrap
msgid "AUTHORS"
msgstr ""

#: opensuse-leap-15-6 archlinux debian-bookworm debian-unstable fedora-39
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed fedora-38 debian-bullseye
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#: mageia-cauldron debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "April 2022"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-39 fedora-rawhide
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 fedora-38 opensuse-tumbleweed
#: debian-bullseye
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-39 fedora-rawhide fedora-38 debian-bullseye
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed debian-bullseye
#, no-wrap
msgid "BUGS"
msgstr "اشکال ها"

#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr ""

#: opensuse-leap-15-6 archlinux debian-bookworm debian-unstable fedora-39
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "COLOPHON"
msgstr "پایان نگاشت"

#: debian-bookworm opensuse-leap-15-6 archlinux debian-unstable fedora-39
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "مطابق با"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "حق نشر"

#: opensuse-leap-15-6 fedora-39 fedora-rawhide
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#: debian-bookworm debian-unstable mageia-cauldron archlinux
#: opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#: fedora-39 fedora-rawhide opensuse-tumbleweed archlinux mageia-cauldron
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-39 fedora-rawhide debian-bullseye fedora-38
#, no-wrap
msgid "DESCRIPTION"
msgstr "توضیح"

#: opensuse-leap-15-6 fedora-38 debian-bookworm fedora-rawhide archlinux
#: debian-bullseye mageia-cauldron
msgid "Display help text and exit."
msgstr ""

#: opensuse-leap-15-6 archlinux debian-bookworm debian-unstable fedora-39
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed debian-bullseye
msgid "Display version information and exit."
msgstr ""

#: debian-bookworm debian-unstable mageia-cauldron fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed archlinux
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#: debian-bookworm debian-unstable archlinux mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-39 fedora-rawhide
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#: debian-bookworm debian-unstable mageia-cauldron archlinux fedora-39
#: fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed fedora-38
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed mageia-cauldron archlinux debian-bullseye
#, no-wrap
msgid "FILES"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#: opensuse-leap-15-6 fedora-38 debian-bookworm fedora-rawhide archlinux
#: mageia-cauldron
msgid "For bug reports, use the issue tracker at"
msgstr ""

#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#: fedora-rawhide opensuse-tumbleweed archlinux
#, fuzzy, no-wrap
#| msgid "GNU coreutils 9.1"
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.1"

#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed archlinux
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"راهنما آنلاین GNU coreutils : E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr "لینوکس"

#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed archlinux
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "راهنمای برنامه نویس لینوکس"

#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr ""

#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed debian-bookworm
#: debian-unstable mageia-cauldron opensuse-leap-15-6
#, fuzzy
#| msgid "Linux"
msgid "Linux."
msgstr "لینوکس"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-39 fedora-rawhide debian-bullseye fedora-38
#, no-wrap
msgid "NAME"
msgstr "نام"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed debian-bullseye
#, no-wrap
msgid "NOTES"
msgstr "یادداشت ها"

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-39 fedora-rawhide fedora-38 debian-bullseye
#, no-wrap
msgid "OPTIONS"
msgstr ""

#: opensuse-leap-15-6 mageia-cauldron
#, no-wrap
msgid "October 2021"
msgstr ""

#: debian-bookworm debian-unstable opensuse-leap-15-6 mageia-cauldron archlinux
#: fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr ""

#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed debian-bookworm
#: debian-unstable opensuse-leap-15-6 mageia-cauldron
msgid "POSIX.1-2008."
msgstr ""

#: debian-bookworm opensuse-leap-15-6 fedora-38 archlinux fedora-39
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed debian-unstable
msgid "Print version and exit."
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr ""

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-39 fedora-rawhide fedora-38
#, no-wrap
msgid "REPORTING BUGS"
msgstr "گزارش اشکال ها"

#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed archlinux mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed archlinux
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"خطا در ترجمه را به E<lt>https://translationproject.org/team/E<gt> گزارش دهید"

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-39 fedora-rawhide fedora-38 debian-bullseye
#, no-wrap
msgid "SEE ALSO"
msgstr "همچنین ببینید"

#: debian-bookworm debian-unstable archlinux fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-39 fedora-rawhide debian-bullseye fedora-38
#, no-wrap
msgid "SYNOPSIS"
msgstr "چکیده"

#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr ""

#: fedora-39 fedora-rawhide opensuse-tumbleweed archlinux debian-unstable
#: mageia-cauldron
#, no-wrap
msgid "September 2023"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed archlinux
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"این صفحه بخشی از انتشار ۴.۱۶ پروژه صفحات راهنما لینوکس I<man-pages> میباشد."
"توضیح ای از پروژه، اطلاعات لازم برای اعلام خطا و آخرین نسخه این صفحه را در \\"
"%https://www.kernel.org/doc/man-pages/ بیابید."

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-39 fedora-rawhide fedora-38
#, no-wrap
msgid "User Commands"
msgstr "فرمان های کاربر"

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed archlinux
msgid "display this help and exit"
msgstr "نمایش دادن راهنما و خروج از برنامه"

#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed archlinux
msgid "output version information and exit"
msgstr "نمایش اطلاعات نسخه و خروج از برنامه"

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-39 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr ""

#: opensuse-leap-15-6
#, no-wrap
msgid "systemd 249"
msgstr "systemd 249"

#: debian-bookworm archlinux debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "systemd 252"
msgstr "systemd 252"

#: mageia-cauldron
#, no-wrap
msgid "systemd 253"
msgstr "systemd 253"

#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#: fedora-38 debian-bookworm archlinux mageia-cauldron
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"
