# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Ralf Demmer <rdemmer@rdemmer.de>
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-08-27 17:12+0200\n"
"PO-Revision-Date: 2023-05-01 09:40+0200\n"
"Last-Translator: Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "outb"
msgstr "outb"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"outb, outw, outl, outsb, outsw, outsl, inb, inw, inl, insb, insw, insl, "
"outb_p, outw_p, outl_p, inb_p, inw_p, inl_p - port I/O"
msgstr ""
"outb, outw, outl, outsb, outsw, outsl, inb, inw, inl, insb, insw, insl, "
"outb_p, outw_p, outl_p, inb_p, inw_p, inl_p - Ein-/Ausgabe mittels Ports"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/io.hE<gt>>\n"
msgstr "B<#include E<lt>sys/io.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<unsigned char inb(unsigned short >I<port>B<);>\n"
"B<unsigned char inb_p(unsigned short >I<port>B<);>\n"
"B<unsigned short inw(unsigned short >I<port>B<);>\n"
"B<unsigned short inw_p(unsigned short >I<port>B<);>\n"
"B<unsigned int inl(unsigned short >I<port>B<);>\n"
"B<unsigned int inl_p(unsigned short >I<port>B<);>\n"
msgstr ""
"B<unsigned char inb(unsigned short >I<port>B<);>\n"
"B<unsigned char inb_p(unsigned short >I<port>B<);>\n"
"B<unsigned short inw(unsigned short >I<port>B<);>\n"
"B<unsigned short inw_p(unsigned short >I<port>B<);>\n"
"B<unsigned int inl(unsigned short >I<port>B<);>\n"
"B<unsigned int inl_p(unsigned short >I<port>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void outb(unsigned char >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outb_p(unsigned char >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outw(unsigned short >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outw_p(unsigned short >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outl(unsigned int >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outl_p(unsigned int >I<value>B<, unsigned short >I<port>B<);>\n"
msgstr ""
"B<void outb(unsigned char >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outb_p(unsigned char >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outw(unsigned short >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outw_p(unsigned short >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outl(unsigned int >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outl_p(unsigned int >I<value>B<, unsigned short >I<port>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void insb(unsigned short >I<port>B<, void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void insw(unsigned short >I<port>B<, void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void insl(unsigned short >I<port>B<, void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void outsb(unsigned short >I<port>B<, const void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void outsw(unsigned short >I<port>B<, const void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void outsl(unsigned short >I<port>B<, const void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
msgstr ""
"B<void insb(unsigned short >I<port>B<, void >I<Adr>B<[.>I<Anzahl>B<],>\n"
"B<           unsigned long >I<Anzahl>B<);>\n"
"B<void insw(unsigned short >I<port>B<, void >I<Adr>B<[.>I<Anzahl>B<],>\n"
"B<           unsigned long >I<Anzahl>B<);>\n"
"B<void insl(unsigned short >I<port>B<, void >I<Adr>B<[.>I<Anzahl>B<],>\n"
"B<           unsigned long >I<Anzahl>B<);>\n"
"B<void outsb(unsigned short >I<port>B<, const void >I<Adr>B<[.>I<Anzahl>B<],>\n"
"B<           unsigned long >I<Anzahl>B<);>\n"
"B<void outsw(unsigned short >I<port>B<, const void >I<Adr>B<[.>I<Anzahl>B<],>\n"
"B<           unsigned long >I<Anzahl>B<);>\n"
"B<void outsl(unsigned short >I<port>B<, const void >I<Adr>B<[.>I<Anzahl>B<],>\n"
"B<           unsigned long >I<Anzahl>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This family of functions is used to do low-level port input and output.  The "
"out* functions do port output, the in* functions do port input; the b-suffix "
"functions are byte-width and the w-suffix functions word-width; the _p-"
"suffix functions pause until the I/O completes."
msgstr ""
"Diese Funktionenfamilie dient zur systemnahen Ein- und Ausgabe auf "
"Hardwareschnittstellen (Ports). Die Out*-Funktionen schreiben dorthin, die "
"In*-Funktionen lesen von dort. Die Suffixe bedeuten: b - die Funktion "
"bearbeitet Byte; w - die Funktion bearbeitet Worte; _p - die Funktion "
"pausiert bis zum Abschluss des E/A-Vorgangs."

#.  , given the following information
#.  in addition to that given in
#.  .BR outb (9).
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"They are primarily designed for internal kernel use, but can be used from "
"user space."
msgstr ""
"Sie wurden ursprünglich als interne Kernelfunktionen konzipiert, können aber "
"auch in User-Space-Programmen verwendet werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"You must compile with B<-O> or B<-O2> or similar.  The functions are defined "
"as inline macros, and will not be substituted in without optimization "
"enabled, causing unresolved references at link time."
msgstr ""
"Sie müssen die Funktionen mit B<-O> oder B<-O2> oder ähnlichem kompilieren. "
"Die Funktionen sind als Inline-Makros definiert und werden ohne aktivierte "
"Optimierungen nicht eingefügt, was zu nicht aufgelösten Verweisen während "
"des Linkvorgangs führt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"You use B<ioperm>(2)  or alternatively B<iopl>(2)  to tell the kernel to "
"allow the user space application to access the I/O ports in question.  "
"Failure to do this will cause the application to receive a segmentation "
"fault."
msgstr ""
"Durch B<ioperm>(2) oder alternativ auch B<iopl>(2) wird der Kernel "
"angewiesen, Benutzeranwendungen den Zugriff auf die betreffenden Ein- und "
"Ausgabeschnittstellen zu gestatten. Wird dies nicht gemacht, treten in der "
"Anwendung Speicherzugriffsfehler auf."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<outb>()  and friends are hardware-specific.  The I<value> argument is "
"passed first and the I<port> argument is passed second, which is the "
"opposite order from most DOS implementations."
msgstr ""
"B<outb>() und Co. sind hardwarespezifisch. Die Argumente I<value> und "
"I<port> werden in dieser Reihenfolge übergeben. Die meisten DOS-"
"Implementierungen verwenden die umgekehrte Reihenfolge."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr "Keine."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ioperm>(2), B<iopl>(2)"
msgstr "B<ioperm>(2), B<iopl>(2)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-11-10"
msgstr "10. November 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "OUTB"
msgstr "OUTB"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<unsigned char inb(unsigned short int >I<port>B<);>\n"
"B<unsigned char inb_p(unsigned short int >I<port>B<);>\n"
"B<unsigned short int inw(unsigned short int >I<port>B<);>\n"
"B<unsigned short int inw_p(unsigned short int >I<port>B<);>\n"
"B<unsigned int inl(unsigned short int >I<port>B<);>\n"
"B<unsigned int inl_p(unsigned short int >I<port>B<);>\n"
msgstr ""
"B<unsigned char inb(unsigned short int >I<port>B<);>\n"
"B<unsigned char inb_p(unsigned short int >I<port>B<);>\n"
"B<unsigned short int inw(unsigned short int >I<port>B<);>\n"
"B<unsigned short int inw_p(unsigned short int >I<port>B<);>\n"
"B<unsigned int inl(unsigned short int >I<port>B<);>\n"
"B<unsigned int inl_p(unsigned short int >I<port>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<void outb(unsigned char >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outb_p(unsigned char >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outw(unsigned short int >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outw_p(unsigned short int >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outl(unsigned int >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outl_p(unsigned int >I<value>B<, unsigned short int >I<port>B<);>\n"
msgstr ""
"B<void outb(unsigned char >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outb_p(unsigned char >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outw(unsigned short int >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outw_p(unsigned short int >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outl(unsigned int >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outl_p(unsigned int >I<value>B<, unsigned short int >I<port>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<void insb(unsigned short int >I<port>B<, void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void insw(unsigned short int >I<port>B<, void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void insl(unsigned short int >I<port>B<, void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void outsb(unsigned short int >I<port>B<, const void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void outsw(unsigned short int >I<port>B<, const void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void outsl(unsigned short int >I<port>B<, const void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
msgstr ""
"B<void insb(unsigned short int >I<port>B<, void *>I<Adr>B<,>\n"
"B<           unsigned long int >I<Anzahl>B<);>\n"
"B<void insw(unsigned short int >I<port>B<, void *>I<Adr>B<,>\n"
"B<           unsigned long int >I<Anzahl>B<);>\n"
"B<void insl(unsigned short int >I<port>B<, void *>I<Adr>B<,>\n"
"B<           unsigned long int >I<Anzahl>B<);>\n"
"B<void outsb(unsigned short int >I<port>B<, const void *>I<Adr>B<,>\n"
"B<           unsigned long int >I<Anzahl>B<);>\n"
"B<void outsw(unsigned short int >I<port>B<, const void *>I<Adr>B<,>\n"
"B<           unsigned long int >I<Anzahl>B<);>\n"
"B<void outsl(unsigned short int >I<port>B<, const void *>I<Adr>B<,>\n"
"B<           unsigned long int >I<Anzahl>B<);>\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
