# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Stefan Janke <gonzo@burg.studfb.unibw-muenchen.de>, 1996.
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2012.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2016, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-08-27 17:21+0200\n"
"PO-Revision-Date: 2023-01-12 20:21+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "setgid"
msgstr "setgid"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "setgid - set group identity"
msgstr "setgid - setzt die Gruppenidentität (GID)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int setgid(gid_t >I<gid>B<);>\n"
msgstr "B<int setgid(gid_t >I<gid>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<setgid>()  sets the effective group ID of the calling process.  If the "
"calling process is privileged (more precisely: has the B<CAP_SETGID> "
"capability in its user namespace), the real GID and saved set-group-ID are "
"also set."
msgstr ""
"B<setgid>() setzt die effektive Gruppen-ID des aufrufenden Prozesses. Falls "
"der aufrufende Prozess privilegiert ist (genauer: über die Capability "
"B<CAP_SETGID> in seinem Benutzernamensraum verfügt), werden auch die reale "
"und die gespeicherte set-group-ID gesetzt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Under Linux, B<setgid>()  is implemented like the POSIX version with the "
"B<_POSIX_SAVED_IDS> feature.  This allows a set-group-ID program that is not "
"set-user-ID-root to drop all of its group privileges, do some un-privileged "
"work, and then reengage the original effective group ID in a secure manner."
msgstr ""
"Unter Linux ist B<setgid>() wie die POSIX-Version mit dem "
"B<_POSIX_SAVED_IDS>-Merkmal implementiert. Das ermöglicht einem set-group-ID-"
"Programm, das nicht set-user-ID-root ist, alle seine Gruppenprivilegien "
"abzugeben, einige nicht privilegierte Arbeiten zu erledigen und dann auf "
"sichere Art und Weise weiter die ursprüngliche effektive Gruppenkennung zu "
"nutzen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"Bei Erfolg wird Null zurückgegeben. Bei einem Fehler wird -1 zurückgegeben "
"und I<errno> gesetzt, um den Fehler anzuzeigen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The group ID specified in I<gid> is not valid in this user namespace."
msgstr ""
"Die in I<GID> angegebene Gruppenkennung ist in diesem Benutzer-Namensraum "
"unzulässig."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The calling process is not privileged (does not have the B<CAP_SETGID> "
"capability in its user namespace), and I<gid> does not match the real group "
"ID or saved set-group-ID of the calling process."
msgstr ""
"Der aufrufende Prozess ist nicht privilegiert (verfügt nicht über die "
"B<CAP_SETGID>-Capability in seinem Benutzernamensraum) und I<gid> entspricht "
"nicht der realen Gruppenkennung oder der gespeicherten set-group-ID des "
"aufrufenden Prozesses."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Unterschiede C-Bibliothek/Kernel"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"At the kernel level, user IDs and group IDs are a per-thread attribute.  "
"However, POSIX requires that all threads in a process share the same "
"credentials.  The NPTL threading implementation handles the POSIX "
"requirements by providing wrapper functions for the various system calls "
"that change process UIDs and GIDs.  These wrapper functions (including the "
"one for B<setgid>())  employ a signal-based technique to ensure that when "
"one thread changes credentials, all of the other threads in the process also "
"change their credentials.  For details, see B<nptl>(7)."
msgstr ""
"Auf der Kernelebene sind Benutzer- und Gruppenkennungen Attribute pro "
"Thread. POSIX verlangt aber, dass sich alle Threads in einem Prozess die "
"gleichen Berechtigungsnachweise teilen. Die NPTL-Threading-Implementierung "
"behandelt die POSIX-Anforderungen durch Bereitstellung von Wrapper-"
"Funktionen für die verschiedenen Systemaufrufe, die die UIDs und GIDs der "
"Prozesse ändern. Diese Wrapper-Funktionen (darunter die für B<setgid>()) "
"verwenden eine signalbasierte Technik, um sicherzustellen, dass bei der "
"Änderung der Berechtigungsnachweise eines Threads auch alle anderen Threads "
"des Prozesses ihre Berechtigungsnachweise ändern. Für Details siehe "
"B<nptl>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4."
msgstr "POSIX.1-2001, SVr4."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The original Linux B<setgid>()  system call supported only 16-bit group "
"IDs.  Subsequently, Linux 2.4 added B<setgid32>()  supporting 32-bit IDs.  "
"The glibc B<setgid>()  wrapper function transparently deals with the "
"variation across kernel versions."
msgstr ""
"Der ursprüngliche B<setgid>()-Systemaufruf unterstützte nur 16-Bit-Gruppen-"
"IDs. Danach führte Linux 2.4 mit B<setgid32>() die Unterstützung für 32-Bit-"
"IDs hinzu. Die Glibc-Wrapperfunktion B<setgid>() behandelt die Unterschiede "
"zwischen den Kernel-Versionen transparent."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<getgid>(2), B<setegid>(2), B<setregid>(2), B<capabilities>(7), "
"B<credentials>(7), B<user_namespaces>(7)"
msgstr ""
"B<getgid>(2), B<setegid>(2), B<setregid>(2), B<capabilities>(7), "
"B<credentials>(7), B<user_namespaces>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "30. Oktober 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, SVr4."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SETGID"
msgstr "SETGID"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/types.hE<gt>>"
msgstr "B<#include E<lt>sys/types.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int setgid(gid_t >I<gid>B<);>"
msgstr "B<int setgid(gid_t >I<gid>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"Bei Erfolg wird Null zurückgegeben. Bei einem Fehler wird -1 zurückgegeben "
"und I<errno> entsprechend gesetzt."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The calling process is not privileged (does not have the B<CAP_SETGID> "
"capability), and I<gid> does not match the real group ID or saved set-group-"
"ID of the calling process."
msgstr ""
"Der aufrufende Prozess ist nicht privilegiert (verfügt nicht über die "
"B<CAP_SETGID>-Capability) und I<gid> entspricht nicht der realen "
"Gruppenkennung oder der gespeicherten set-group-ID des aufrufenden Prozesses."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
