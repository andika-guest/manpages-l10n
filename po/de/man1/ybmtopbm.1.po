# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-08-27 17:33+0200\n"
"PO-Revision-Date: 2023-04-15 16:19+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Ybmtopbm User Manual"
msgstr "Ybmtopbm-Benutzerhandbuch"

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "06 March 1990"
msgstr "6. März 1990"

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr "Netpbm-Dokumentation"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "ybmtopbm - convert a Bennet Yee \"face\" file to PBM"
msgstr "ybmtopbm - Eine Bennet-Yee-»face«-Datei in PBM konvertieren"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ybmtopbm>"
msgstr "B<ybmtopbm>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "[I<facefile>]"
msgstr "[I<face-Datei>]"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)\\&."
msgstr "Dieses Programm ist Teil von B<netpbm>(1)\\&."

# FIXME .  and → and
#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<ymtopbm> reads a file acceptable to the B<face> and B<xbm> programs by "
"Bennet Yee (I<bsy+@cs.cmu.edu>).  and writes a PBM image as output."
msgstr ""
"Liest eine Datei, die von den Programmen B<face> und B<xbm> von Bennet Yee "
"(bsy+@cs.cmu.edu) akzeptiert wird und schreibt ein PBM-Bild als Ausgabe."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

# FIXME Remove hard line breaks
# FIXME See → see
#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"There are no command line options defined specifically\n"
"for B<ybmtopbm>, but it recognizes the options common to all\n"
"programs based on libnetpbm (See \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&.)\n"
msgstr ""
"Für B<ybmtopbm> gibt es keine spezifischen Befehlszeilenoptionen, aber es\n"
"werden die gemeinsamen Optionen für alle auf libnetpbm basierenden Programme\n"
"akzeptiert (siehe E<.UR index.html#commonoptions>Common OptionsE<.UE>\\&.)\n"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
msgid "B<pbmtoybm>(1)\\&, B<pbm>(1)\\&"
msgstr "B<pbmtoybm>(1)\\&, B<pbm>(1)\\&"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Copyright (C) 1991 by Jamie Zawinski and Jef Poskanzer."
msgstr "Copyright (C) 1991 Jamie Zawinski und Jef Poskanzer."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr "URSPRUNG DES DOKUMENTS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""
"Diese Handbuchseite wurde vom Netpbm-Werkzeug »makeman« aus der HTML-Quelle "
"erstellt. Das Master-Dokument befindet sich unter"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/ybmtopbm.html>"
msgstr "B<http://netpbm.sourceforge.net/doc/ybmtopbm.html>"

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<pbmtoybm>(1)\\&, B<pbm>(5)\\&"
msgstr "B<pbmtoybm>(1)\\&, B<pbm>(5)\\&"
