# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.2.0\n"
"POT-Creation-Date: 2023-08-27 17:08+0200\n"
"PO-Revision-Date: 2021-06-13 14:24+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mkmanifest"
msgstr "mkmanifest"

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "21Mar23"
msgstr "21. März 2023"

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "mtools-4.0.43"
msgstr "mtools-4.0.43"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Name"
msgstr "Name"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mkmanifest - makes list of file names and their DOS 8+3 equivalent"
msgstr ""
"mkmanifest - Listen von Dateinamen und deren DOS-konformer 8+3-Entsprechung "
"erstellen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Note\\ of\\ warning"
msgstr "Warnhinweis"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation, and may not be entirely accurate or complete.  See the end of "
"this man page for details."
msgstr ""
"Diese Handbuchseite wurde automatisch aus der Texinfo-Dokumentation der "
"Mtools erstellt und kann fehlerhaft bzw. unvollständig sein. Lesen Sie die "
"Details am Ende dieser Handbuchseite."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Beschreibung"

# FIXME Unix → UNIX
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The \\&CW<mkmanifest> command is used to create a shell script (packing "
"list) to restore Unix filenames. Its syntax is:"
msgstr ""
"Der Befehl \\&CW<mkmanifest> erstellt ein Shellskript (eine Packliste) zum "
"Wiederherstellen von UNIX-Dateinamen. Die Syntax lautet wie folgt:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\&\\&CW<mkmanifest> [ I<files> ]"
msgstr "\\&\\&CW<mkmanifest> [ I<Dateien> ]"

# FIXME makmanifest doesn't touch filenames with both lowercase and uppercase letters (see the example below, v4.0.26).
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"\\&\\&CW<Mkmanifest> creates a shell script that aids in the restoration of "
"Unix filenames that got clobbered by the MS-DOS filename restrictions.  MS-"
"DOS filenames are restricted to 8 character names, 3 character extensions, "
"upper case only, no device names, and no illegal characters."
msgstr ""
"\\&\\&CW<Mkmanifest> erstellt ein Shellskript, das bei der Wiederherstellung "
"von UNIX-Dateinamen hilft, die aufgrund der Dateinamensbeschränkungen auf MS-"
"DOS-Systemen verändert wurden. Dort sind Dateinamen auf 8 Zeichen für den "
"Basisnamen und 3 Zeichen für die Erweiterung beschränkt, außerdem sind nur "
"Großbuchstaben erlaubt sowie Gerätenamen sowie Sonderzeichen unzulässig."

# FIXME mkmanifest → \\&CW<mkmanifest>
# FIXME Unix → UNIX
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The mkmanifest program is compatible with the methods used in "
"\\&\\&CW<pcomm, arc,> and \\&CW<mtools> to change perfectly good Unix "
"filenames to fit the MS-DOS restrictions. This command is only useful if the "
"target system which will read the diskette cannot handle VFAT long names."
msgstr ""
"Das Programm \\&CW<mkmanifest> ist zu den in \\&\\&CW<pcomm, arc> und "
"\\&CW<mtools> verwendeten Methoden kompatibel, mit Hilfe derer UNIX-"
"Dateinamen perfekt an die Einschränkungen von MS-DOS angepasst werden. "
"Dieser Befehl ist nur nützlich, wenn das Zielsystem, auf dem die Diskette "
"gelesen werden soll, nicht mit langen VFAT-Dateinamen umgehen kann."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Example"
msgstr "Beispiel"

# FIXME Unix → UNIX
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"You want to copy the following Unix files to a MS-DOS diskette (using the "
"\\&\\&CW<mcopy> command)."
msgstr ""
"Nehmen wir an, Sie wollen die folgenden UNIX-Dateien mit dem Befehl "
"\\&\\&CW<mcopy> auf eine MS-DOS-Diskette kopieren."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<  very_long_name\n"
"  2.many.dots\n"
"  illegal:\n"
"  good.c\n"
"  prn.dev\n"
"  Capital>\n"
msgstr ""
"B<  very_long_name\n"
"  2.many.dots\n"
"  illegal:\n"
"  good.c\n"
"  prn.dev\n"
"  Capital>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\&\\&CW<ASCII> converts the names to:"
msgstr "\\&\\&CW<ASCII> wandelt die Namen folgendermaßen um:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<  very_lon\n"
"  2xmany.dot\n"
"  illegalx\n"
"  good.c\n"
"  xprn.dev\n"
"  capital>\n"
msgstr ""
"B<  very_lon\n"
"  2xmany.dot\n"
"  illegalx\n"
"  good.c\n"
"  xprn.dev\n"
"  capital>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The command:"
msgstr "Der Befehl"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<mkmanifest very_long_name 2.many.dots illegal: good.c prn.dev Capital E<gt>manifest>\n"
msgstr "B<mkmanifest very_long_name 2.many.dots illegal: good.c prn.dev Capital E<gt>manifest>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\&would produce the following:"
msgstr "\\&würde Folgendes erzeugen:"

# FIXME Capitalization is obviously not of particular interest in the current mtools-4.0.26. Should be fixed in the man page (or even fixed in mtools itself).
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<  mv very_lon very_long_name\n"
"  mv 2xmany.dot 2.many.dots\n"
"  mv illegalx illegal:\n"
"  mv xprn.dev prn.dev\n"
"  mv capital Capital>\n"
msgstr ""
"B<  mv very_lon very_long_name\n"
"  mv 2xmany.dot 2.many.dots\n"
"  mv illegalx illegal:\n"
"  mv xprn.dev prn.dev\n"
"  mv capital Capital>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Notice that \"good.c\" did not require any conversion, so it did not appear "
"in the output."
msgstr ""
"Beachten Sie, dass »good.c« keinerlei Umwandlung erfordert, so dass es nicht "
"in der Ausgabe erscheint."

# FIXME Unix → UNIX
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Suppose I've copied these files from the diskette to another Unix system, "
"and I now want the files back to their original names.  If the file "
"\"manifest\" (the output captured above) was sent along with those files, it "
"could be used to convert the filenames."
msgstr ""
"Nehmen wir an, Sie haben diese Dateien von der Diskette auf ein anderes UNIX-"
"System kopiert und wollen nun die ursprünglichen Dateinamen "
"wiederherstellen. Sofern die Datei »manifest« (aus der vorstehenden Ausgabe) "
"mit diesen Dateien zusammen kopiert wurde, kann sie zum Umwandeln der "
"Dateinamen verwendet werden."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Bugs"
msgstr "Fehler"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The short names generated by \\&CW<mkmanifest> follow the old convention "
"(from mtools-2.0.7) and not the one from Windows 95 and mtools-3.0."
msgstr ""
"Die von \\&CW<mkmanifest> erzeugten Kurznamen folgen der alten Konvention "
"aus mtools-2.0.7 und nicht der aus Windows 95 und mtools-3.0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "See\\ Also"
msgstr "Siehe\\ auch"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Mtools' texinfo doc"
msgstr "Texinfo-Dokumentation von Mtools"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Viewing\\ the\\ texi\\ doc"
msgstr "Texi\\ Dokumentation\\ anzeigen"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation. However, this process is only approximative, and some items, "
"such as crossreferences, footnotes and indices are lost in this translation "
"process.  Indeed, these items have no appropriate representation in the "
"manpage format.  Moreover, not all information has been translated into the "
"manpage version.  Thus I strongly advise you to use the original texinfo "
"doc.  See the end of this manpage for instructions how to view the texinfo "
"doc."
msgstr ""
"Diese Handbuchseite wurde automatisch aus der Texinfo-Dokumentation der "
"Mtools erstellt. Dieser Prozess funktioniert allerdings nur ungefähr und "
"einige Einträge, wie Querverweise, Fußnoten und Indizes, gehen bei der "
"Umwandlung verloren. Allerdings haben diese Einträge auch keine "
"entsprechende Darstellung im Handbuchseitenformat. Desweiteren wurde nicht "
"sämtliche Information in das Handbuchseitenformat übertragen. Daher empfehle "
"ich Ihnen nachdrücklich, die ursprüngliche Texinfo-Dokumentation zu "
"verwenden. Lesen Sie am Ende dieser Handbuchseite die Anweisungen, wie Sie "
"die Texinfo-Dokumentation ansehen können."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "* \\ \\ "
msgstr "* \\ \\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To generate a printable copy from the texinfo doc, run the following "
"commands:"
msgstr ""
"Um eine ausdruckbare Version aus der Texinfo-Dokumentation zu erzeugen, "
"führen Sie folgende Befehle aus:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make dvi; dvips mtools.dvi>\n"
msgstr "B<    ./configure; make dvi; dvips mtools.dvi>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "To generate a html copy, run:"
msgstr "Um eine Kopie im HTML-Format zu erzeugen, führen Sie Folgendes aus:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make html>\n"
msgstr "B<    ./configure; make html>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"\\&A premade html can be found at \\&\\&CW<\\(ifhttp://www.gnu.org/software/"
"mtools/manual/mtools.html\\(is>"
msgstr ""
"\\&Ein bereits erstelltes HTML befindet sich unter \\&\\&CW<\\(ifhttp://www."
"gnu.org/software/mtools/manual/mtools.html\\(is>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "To generate an info copy (browsable using emacs' info mode), run:"
msgstr ""
"Um eine Kopie im Info-Format (im Emacs-Info-Modus verwendbar) zu erzeugen, "
"führen Sie Folgendes aus:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make info>\n"
msgstr "B<    ./configure; make info>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The texinfo doc looks most pretty when printed or as html.  Indeed, in the "
"info version certain examples are difficult to read due to the quoting "
"conventions used in info."
msgstr ""
"Diese Texinfo-Dokumentation sieht ausgedruckt oder im HTML-Format am besten "
"aus, denn in der Info-Version sind einige Beispiele aufgrund der "
"Zitierkonventionen von Info schwer zu lesen."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10Jul21"
msgstr "10. Juli 2021"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "mtools-4.0.32"
msgstr "mtools-4.0.32"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "06Aug21"
msgstr "6. August 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "mtools-4.0.35"
msgstr "mtools-4.0.35"
