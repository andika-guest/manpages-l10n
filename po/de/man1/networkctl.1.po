# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2018-2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2023-10-02 12:12+0200\n"
"PO-Revision-Date: 2023-09-16 09:29+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NETWORKCTL"
msgstr "NETWORKCTL"

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "networkctl"
msgstr "networkctl"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "networkctl - Query or modify the status of network links"
msgstr ""
"networkctl - Den Status der Netzwerkverbindungen abfragen oder verändern"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<networkctl> [OPTIONS...] COMMAND [LINK...]"
msgstr "B<networkctl> [OPTIONEN…] BEFEHL [VERBINDUNG…]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<networkctl> may be used to query or modify the state of the network links "
"as seen by B<systemd-networkd>\\&. Please refer to B<systemd-networkd."
"service>(8)  for an introduction to the basic concepts, functionality, and "
"configuration syntax\\&."
msgstr ""
"B<networkctl> kann zum Abfragen und Verändern des Zustands von "
"Netzwerkverbindungen, wie sie von B<systemd-networkd> gesehen werden, "
"verwandt werden\\&. Bitte lesen Sie B<systemd-networkd.service>(8) für eine "
"Einführung in die grundlegenden Konzepte, Funktionalitäten und "
"Konfigurationssyntax\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "COMMANDS"
msgstr "BEFEHLE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The following commands are understood:"
msgstr "Die folgenden Befehle werden verstanden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<list> [I<PATTERN\\&...>]"
msgstr "B<list> [I<MUSTER…>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Show a list of existing links and their status\\&. If one or more "
"I<PATTERN>s are specified, only links matching one of them are shown\\&. If "
"no further arguments are specified shows all links, otherwise just the "
"specified links\\&. Produces output similar to:"
msgstr ""
"Zeigt eine Liste der existierenden Verbindungen und ihren Status\\&. Falls "
"eine oder mehrere I<MUSTER> angegeben sind, werden nur die Links, die darauf "
"passen, angezeigt\\&. Falls keine weiteren Argumente angegeben sind, werden "
"alle Verbindungen angezeigt, andernfalls nur die angegebenen "
"Verbindungen\\&. Erstellt Ausgabe ähnlich folgender:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"IDX LINK         TYPE     OPERATIONAL SETUP\n"
"  1 lo           loopback carrier     unmanaged\n"
"  2 eth0         ether    routable    configured\n"
"  3 virbr0       ether    no-carrier  unmanaged\n"
"  4 virbr0-nic   ether    off         unmanaged\n"
msgstr ""
"IDX LINK         TYPE     OPERATIONAL SETUP\n"
"  1 lo           loopback carrier     unmanaged\n"
"  2 eth0         ether    routable    configured\n"
"  3 virbr0       ether    no-carrier  unmanaged\n"
"  4 virbr0-nic   ether    off         unmanaged\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "4 links listed\\&.\n"
msgstr "4 links listed\\&.\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The operational status is one of the following:"
msgstr "Der Betriebsstatus ist einer der Folgenden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "missing"
msgstr "missing"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "the device is missing"
msgstr "das Gerät fehlt "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "off"
msgstr "off"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "the device is powered down"
msgstr "das Gerät ist ausgeschaltet"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "no-carrier"
msgstr "no-carrier"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "the device is powered up, but it does not yet have a carrier"
msgstr "das Gerät ist eingeschaltet, hat aber noch kein Signal (Träger)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "dormant"
msgstr "dormant"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "the device has a carrier, but is not yet ready for normal traffic"
msgstr ""
"das Gerät hat ein Signal, ist aber noch nicht für normalen Verkehr bereit"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "degraded-carrier"
msgstr "degraded-carrier"

# FIXME Most entries do not have a full stop, a few do? Maybe harmonize
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"one of the bonding or bridge slave network interfaces is in off, no-carrier, "
"or dormant state, and the master interface has no address\\&."
msgstr ""
"einer der gebündelten oder Bridge-Slave-Netzwerkschnittstellen ist "
"ausgeschaltet, hat kein Signal oder ist in einem ruhendem Zustand und die "
"Master-Schnittstelle hat keine Adresse\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "carrier"
msgstr "carrier"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"the link has a carrier, or for bond or bridge master, all bonding or bridge "
"slave network interfaces are enslaved to the master"
msgstr ""
"der Link hat ein Signal oder für Master bei Bündelungen oder Bridges, alle "
"gebündelten oder Bridge-Slave-Netzwerkschnittstellen sind an dem Master "
"angebunden\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "degraded"
msgstr "degraded"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"the link has carrier and addresses valid on the local link configured\\&. "
"For bond or bridge master this means that not all slave network interfaces "
"have carrier but at least one does\\&."
msgstr ""
"der Link hat ein Signal und eine auf dem konfigurierten lokalen Link gültige "
"Adresse\\&. Für gebundene oder Bridge-Master bedeutet dies, dass nicht alle "
"Slave-Netzwerkschnittstellen über ein Signal verfügen, aber mindestens "
"eine\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "enslaved"
msgstr "enslaved"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"the link has carrier and is enslaved to bond or bridge master network "
"interface"
msgstr ""
"der Link hat ein Signal und ist an eine Bündelung oder Bridge-Master-"
"Netzwerkschnittstelle angebunden "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "routable"
msgstr "routable"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"the link has carrier and routable address configured\\&. For bond or bridge "
"master it is not necessary for all slave network interfaces to have carrier, "
"but at least one must\\&."
msgstr ""
"der Link hat ein Signal und eine weiterleitbare Adresse\\&.  Für Master bei "
"Bündelungen oder Bridges ist es nicht notwendig, dass alle Slave-"
"Netzwerkschnittstellen über ein Signal verfügen, aber mindestens eine "
"muss\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The setup status is one of the following:"
msgstr "Der Einrichtungsstatus ist einer der Folgenden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "pending"
msgstr "pending"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"udev is still processing the link, we don\\*(Aqt yet know if we will manage "
"it"
msgstr ""
"Udev bearbeitet den Link noch, es ist noch nicht klar, ob es den Link "
"verwalten wird"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "initialized"
msgstr "initialized"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"udev has processed the link, but we don\\*(Aqt yet know if we will manage it"
msgstr ""
"Udev hat den Link verarbeitet, aber es ist noch nicht klar, ob es den Link "
"verwalten wird"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "configuring"
msgstr "configuring"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "in the process of retrieving configuration or configuring the link"
msgstr ""
"derzeit dabei, die Konfiguration des Links abzufragen oder durchzuführen"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "configured"
msgstr "configured"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "link configured successfully"
msgstr "Link erfolgreich konfiguriert"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "unmanaged"
msgstr "unmanaged"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "networkd is not handling the link"
msgstr "Networkd handhabt den Link nicht"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "failed"
msgstr "failed"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "networkd failed to manage the link"
msgstr "Networkd konnten den Link nicht verwalten"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "linger"
msgstr "linger"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "the link is gone, but has not yet been dropped by networkd"
msgstr ""
"der Link ist verschwunden, wurde aber von Networkd noch nicht freigegeben"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<status> [I<PATTERN\\&...>]"
msgstr "B<status> [I<MUSTER…>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Show information about the specified links: type, state, kernel module "
"driver, hardware and IP address, configured DNS servers, etc\\&. If one or "
"more I<PATTERN>s are specified, only links matching one of them are shown\\&."
msgstr ""
"Zeigt Informationen über die angegebenen Verbindungen: Typ, Status, "
"Kernelmodultreiber, Hardware- und IP-Adresse, konfigurierte DNS-Server "
"usw\\&. Falls eine oder mehrere I<MUSTER> angegeben sind, werden nur die "
"Links, die darauf passen, angezeigt\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"When no links are specified, an overall network status is shown\\&. Also see "
"the option B<--all>\\&."
msgstr ""
"Wenn keine Verbindungen angegeben wurden, wird ein Gesamtnetzwerkstatus "
"angezeigt\\&. Siehe auch die Option B<--all>\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Produces output similar to:"
msgstr "Erstellt Ausgabe ähnlich folgender:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"●        State: routable\n"
"  Online state: online\n"
"       Address: 10\\&.193\\&.76\\&.5 on eth0\n"
"                192\\&.168\\&.122\\&.1 on virbr0\n"
"                169\\&.254\\&.190\\&.105 on eth0\n"
"                fe80::5054:aa:bbbb:cccc on eth0\n"
"       Gateway: 10\\&.193\\&.11\\&.1 (CISCO SYSTEMS, INC\\&.) on eth0\n"
"           DNS: 8\\&.8\\&.8\\&.8\n"
"                8\\&.8\\&.4\\&.4\n"
msgstr ""
"●        State: routable\n"
"  Online state: online\n"
"       Address: 10\\&.193\\&.76\\&.5 on eth0\n"
"                192\\&.168\\&.122\\&.1 on virbr0\n"
"                169\\&.254\\&.190\\&.105 on eth0\n"
"                fe80::5054:aa:bbbb:cccc on eth0\n"
"       Gateway: 10\\&.193\\&.11\\&.1 (CISCO SYSTEMS, INC\\&.) on eth0\n"
"           DNS: 8\\&.8\\&.8\\&.8\n"
"                8\\&.8\\&.4\\&.4\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In the overall network status, the online state depends on the individual "
"online state of all required links\\&. Managed links are required for online "
"by default\\&. In this case, the online state is one of the following:"
msgstr ""
"Im Gesamtnetzwerkstatus hängt der Online-Status von den einzelnen Online-"
"Status aller benötigten Links ab\\&. Standardmäßig werden verwaltete Links "
"für »online« benötigt\\&. In diesem Fall ist der Online-Status einer der "
"folgenden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "unknown"
msgstr "unknown"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"all links have unknown online status (i\\&.e\\&. there are no required links)"
msgstr ""
"Alle Links haben einen unbekannten Online-Status (d\\&.h\\&. es gibt keine "
"benötigten Links)\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "offline"
msgstr "offline"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "all required links are offline"
msgstr "alle benötigten Links sind offline"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "partial"
msgstr "partial"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "some, but not all, required links are online"
msgstr "einige, aber nicht alle, benötigten Links sind online"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "online"
msgstr "online"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "all required links are online"
msgstr "alle benötigten Links sind online"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<lldp> [I<PATTERN\\&...>]"
msgstr "B<lldp> [I<MUSTER…>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Show discovered LLDP (Link Layer Discovery Protocol) neighbors\\&. If one or "
"more I<PATTERN>s are specified only neighbors on those interfaces are "
"shown\\&. Otherwise shows discovered neighbors on all interfaces\\&. Note "
"that for this feature to work, I<LLDP=> must be turned on for the specific "
"interface, see B<systemd.network>(5)  for details\\&."
msgstr ""
"Zeigt entdeckte LLDP- (Link Layer Discovery Protocol)Nachbarn\\&. Falls ein "
"oder mehrere I<MUSTER> angegeben sind, werden nur die Nachbarn auf diesen "
"Schnittstellen angezeigt\\&. Andernfalls werden die Nachbarn auf allen "
"Schnittstellen angezeigt\\&. Beachten Sie, dass I<LLDP=> auf der angegebenen "
"Schnittstelle eingeschaltet sein muss, damit diese funktioniert, siehe "
"B<systemd.network>(5) für Details\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"LINK             CHASSIS ID        SYSTEM NAME      CAPS        PORT ID           PORT DESCRIPTION\n"
"enp0s25          00:e0:4c:00:00:00 GS1900           \\&.\\&.b\\&.\\&.\\&.\\&.\\&.\\&.\\&.\\&. 2                 Port #2\n"
msgstr ""
"LINK             CHASSIS ID        SYSTEM NAME      CAPS        PORT ID           PORT DESCRIPTION\n"
"enp0s25          00:e0:4c:00:00:00 GS1900           \\&.\\&.b\\&.\\&.\\&.\\&.\\&.\\&.\\&.\\&. 2                 Port #2\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"Capability Flags:\n"
"o - Other; p - Repeater;  b - Bridge; w - WLAN Access Point; r - Router;\n"
"t - Telephone; d - DOCSIS cable device; a - Station; c - Customer VLAN;\n"
"s - Service VLAN, m - Two-port MAC Relay (TPMR)\n"
msgstr ""
"Capability Flags:\n"
"o - Other; p - Repeater;  b - Bridge; w - WLAN Access Point; r - Router;\n"
"t - Telephone; d - DOCSIS cable device; a - Station; c - Customer VLAN;\n"
"s - Service VLAN, m - Two-port MAC Relay (TPMR)\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "1 neighbors listed\\&.\n"
msgstr "1 neighbors listed\\&.\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<label>"
msgstr "B<label>"

# FIXME Mario Blättermann, 2018-12-05:
# Also in dem verlinkten Dokument wird gar nichts diskutiert. Vielmehr ist dort
# vermerkt, dass der Standard diskussionswürdig ist und Verbesserungsvorschläge
# erwünscht sind.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Show numerical address labels that can be used for address selection\\&. "
"This is the same information that B<ip-addrlabel>(8)  shows\\&. See "
"\\m[blue]B<RFC 3484>\\m[]\\&\\s-2\\u[1]\\d\\s+2 for a discussion of address "
"labels\\&."
msgstr ""
"Zeigt numerische Adresskennzeichnungen, die zur Adressauswahl verwandt "
"werden können\\&. Dies ist die gleiche Information, die B<ip-addrlabel>(8) "
"zeigt\\&. Siehe \\m[blue]B<RFC 3484>\\m[]\\&\\s-2\\u[1]\\d\\s+2 für eine "
"Diskussion von Adresskennzeichnungen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"Prefix/Prefixlen                          Label\n"
"        ::/0                                  1\n"
"    fc00::/7                                  5\n"
"    fec0::/10                                11\n"
"    2002::/16                                 2\n"
"    3ffe::/16                                12\n"
" 2001:10::/28                                 7\n"
"    2001::/32                                 6\n"
"::ffff:0\\&.0\\&.0\\&.0/96                             4\n"
"        ::/96                                 3\n"
"       ::1/128                                0\n"
msgstr ""
"Prefix/Prefixlen                          Label\n"
"        ::/0                                  1\n"
"    fc00::/7                                  5\n"
"    fec0::/10                                11\n"
"    2002::/16                                 2\n"
"    3ffe::/16                                12\n"
" 2001:10::/28                                 7\n"
"    2001::/32                                 6\n"
"::ffff:0\\&.0\\&.0\\&.0/96                             4\n"
"        ::/96                                 3\n"
"       ::1/128                                0\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<delete> I<DEVICE\\&...>"
msgstr "B<delete> I<GERÄT…>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Deletes virtual netdevs\\&. Takes interface name or index number\\&."
msgstr ""
"Löscht virtuelle Netdevs\\&. Akzeptiert Schnittstellennamen oder "
"Indexnummer\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<up> I<DEVICE\\&...>"
msgstr "B<up> I<GERÄT…>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Bring devices up\\&. Takes interface name or index number\\&."
msgstr ""
"Aktiviert Geräte\\&. Akzeptiert Schnittstellennamen oder Indexnummer\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<down> I<DEVICE\\&...>"
msgstr "B<down> I<GERÄT…>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Bring devices down\\&. Takes interface name or index number\\&."
msgstr ""
"Deaktiviert Geräte\\&. Akzeptiert Schnittstellennamen oder Indexnummer\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<renew> I<DEVICE\\&...>"
msgstr "B<renew> I<GERÄT…>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Renew dynamic configurations e\\&.g\\&. addresses received from DHCP "
"server\\&. Takes interface name or index number\\&."
msgstr ""
"Erneuert dynamische Konfigurationen, z\\&.B\\&. vom DHCP-Server empfangene "
"Adressen\\&. Akzeptiert Schnittstellennamen oder Indexnummer\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<forcerenew> I<DEVICE\\&...>"
msgstr "B<forcerenew> I<GERÄT…>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Send a FORCERENEW message to all connected clients, triggering DHCP "
"reconfiguration\\&. Takes interface name or index number\\&."
msgstr ""
"Sendet FORCERENEW-Nachrichten an alle verbundenen Clients, löst DHCP-"
"Rekonfiguration aus\\&. Akzeptiert Schnittstellennamen oder Indexnummer\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<reconfigure> I<DEVICE\\&...>"
msgstr "B<reconfigure> I<GERÄT…>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Reconfigure network interfaces\\&. Takes interface name or index number\\&. "
"Note that this does not reload \\&.netdev or \\&.network corresponding to "
"the specified interface\\&. So, if you edit config files, it is necessary to "
"call B<networkctl reload> first to apply new settings\\&."
msgstr ""
"Konfiguriert Netzwerkschnittstellen neu\\&. Akzeptiert einen "
"Schnittstellennamen oder eine Index-Nummer\\&. Beachten Sie, dass dies nicht "
"die der angegebenen Schnittstelle entsprechenden »\\&.netdev« oder »\\&."
"network« neu lädt\\&. Falls Sie daher diese Konfigurationsdateien "
"bearbeiten, ist es notwendig, zuerst B<networkctl reload> aufzurufen, um die "
"neuen Einstellungen anzuwenden\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<reload>"
msgstr "B<reload>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Reload \\&.netdev and \\&.network files\\&. If a new \\&.netdev file is "
"found, then the corresponding netdev is created\\&. Note that even if an "
"existing \\&.netdev is modified or removed, B<systemd-networkd> does not "
"update or remove the netdev\\&. If a new, modified or removed \\&.network "
"file is found, then all interfaces which match the file are reconfigured\\&."
msgstr ""
"Lädt \\&.netdev- und \\&.network-Dateien neu\\&. Falls eine neue \\&.netdev-"
"Datei gefunden wird, dann wird das entsprechende Netdev erstellt\\&. "
"Beachten Sie, dass B<systemd-networkd> das Netdev nicht aktualisiert oder "
"entfernt, falls ein bestehendes \\&.netdev verändert oder entfernt wird\\&. "
"Falls eine neue, veränderte oder entfernte \\&.network-Datei gefunden wird, "
"dann werden alle Schnittstellen, die auf die Datei passen, neu "
"konfiguriert\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "B<edit> I<FILE>|I<@DEVICE>\\&..."
msgstr "B<edit> I<FILE>|I<@GERÄT>…"

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Edit network configuration files, which include \\&.network, \\&.netdev, and "
"\\&.link files\\&. If no network config file matching the given name is "
"found, a new one will be created under /etc/\\&. Specially, if the name is "
"prefixed by \"@\", it will be treated as a network interface, and editing "
"will be performed on the network config files associated with it\\&. "
"Additionally, the interface name can be suffixed with \":network\" (default) "
"or \":link\", in order to choose the type of network config to operate on\\&."
msgstr ""
"Bearbeitet Netzwerkkonfigurationsdateien, einschließlich \\&.network-, \\&."
"netdev- und \\&.link-Dateien\\&. Falls keine Netzwerkkonfigurationsdatei "
"gefunden wird, die auf den angegebenen Namen passt, wird eine neue unter /"
"etc/ erstellt\\&. Falls insbesondere dem Namen »@« vorangestellt wird, wird "
"diese als Netzwerkschnittstelle behandelt und das Bearbeiten wird auf den "
"ihr zugeordneten Netzwerkkonfigurationsdateien erfolgen\\&. Zusätzlich kann "
"dem Schnittstellennamen »:network« (die Vorgabe) oder »:link« angehängt "
"werden, um den Typ der Netzwerkkonfiguration auszuwählen, auf dem gearbeitet "
"werden soll\\&."

# FIXME B<systemd-udevd> → B<systemd-udevd>(8)
#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"If B<--drop-in=> is specified, edit the drop-in file instead of the main "
"configuration file\\&. Unless B<--no-reload> is specified, B<systemd-"
"networkd> will be reloaded after the edit of the \\&.network or \\&.netdev "
"files finishes\\&. The same applies for \\&.link files and B<systemd-"
"udevd>\\&. Note that the changed link settings are not automatically applied "
"after reloading\\&. To achieve that, trigger uevents for the corresponding "
"interface\\&. Refer to B<systemd.link>(5)  for more information\\&."
msgstr ""
"Falls B<--drop-in=> angegeben ist, wird die Ergänzungsdatei anstelle der "
"Hauptkonfigurationsdatei bearbeitet\\&. Sofern B<--no-reload> nicht "
"angegeben ist, wird B<systemd-networkd> nach Abschluss der Bearbeitung der "
"\\&.network- oder \\&.netdev-Dateien neu geladen\\&. Das gleiche gilt für "
"\\&.link-Dateien und B<systemd-udevd>(8)\\&. Beachten Sie, dass die "
"geänderten Link-Einstellungen nach dem Neuladen nicht automatisch angewandt "
"werden\\&. Um dies zu erreichen, lösen Sie Uevents für die entsprechende "
"Schnittstelle aus\\&. Weitere Informationen finden Sie in B<systemd."
"link>(5)\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "B<cat> I<FILE>|I<@DEVICE>\\&..."
msgstr "B<cat> I<DATEI>|I<@GERÄT>…"

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Show network configuration files\\&. This command honors the \"@\" prefix in "
"the same way as B<edit>\\&."
msgstr ""
"Zeigt Netzwerkkonfigurationsdateien\\&. Dieser Befehl berücksichtigt "
"vorangestellte »@« auf die gleiche Art wie B<edit>\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The following options are understood:"
msgstr "Die folgenden Optionen werden verstanden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<-a> B<--all>"
msgstr "B<-a> B<--all>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Show all links with B<status>\\&."
msgstr "Zeigt mit B<status> alle Verbindungen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<-s> B<--stats>"
msgstr "B<-s> B<--stats>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Show link statistics with B<status>\\&."
msgstr "Zeigt mit B<status> Link-Statistiken\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<-l>, B<--full>"
msgstr "B<-l>, B<--full>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Do not ellipsize the output\\&."
msgstr "Verkürzt die Ausgabe nicht\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<-n>, B<--lines=>"
msgstr "B<-n>, B<--lines=>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"When used with B<status>, controls the number of journal lines to show, "
"counting from the most recent ones\\&. Takes a positive integer argument\\&. "
"Defaults to 10\\&."
msgstr ""
"Steuert bei der Verwendung mit B<status> die Anzahl der anzuzeigenden "
"Journal-Einträge, gezählt vom neusten\\&. Akzeptiert eine positive Ganzzahl "
"als Argument\\&. Standardmäßig 10\\&."

# FIXME B<--drop-in=> → B<--drop-in=>I<NAME>
#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "B<--drop-in=>"
msgstr "B<--drop-in=>I<NAME>"

# FIXME I<NAME>When → When
#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"I<NAME>When used with B<edit>, edit the drop-in file I<NAME> instead of the "
"main configuration file\\&."
msgstr ""
"Bearbeitet bei der Verwendung mit B<edit> die Ergänzungsdatei I<NAME> "
"anstelle der Hauptkonfigurationsdatei\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "B<--no-reload>"
msgstr "B<--no-reload>"

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"When used with B<edit>, B<systemd-networkd> or B<systemd-udevd> will not be "
"reloaded after the editing finishes\\&."
msgstr ""
"Bei der Verwendung mit B<edit> werden B<systemd-networkd> oder B<systemd-"
"udevd> nicht nach Abschluss der Bearbeitung neu geladen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--json=>I<MODE>"
msgstr "B<--json=>I<MODUS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Shows output formatted as JSON\\&. Expects one of \"short\" (for the "
"shortest possible output without any redundant whitespace or line breaks), "
"\"pretty\" (for a pretty version of the same, with indentation and line "
"breaks) or \"off\" (to turn off JSON output, the default)\\&."
msgstr ""
"Zeigt die Ausgabe als JSON formatiert\\&. Erwartet entweder »short« (für die "
"kürzest mögliche Ausgabe ohne unnötigen Leerraum oder Zeilenumbrüche), "
"»pretty« (für eine schönere Version der gleichen Ausgabe, mit Einzügen und "
"Zeilenumbrüchen) oder »off« (um die standardmäßig aktivierte JSON-Ausgabe "
"auszuschalten)\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Print a short help text and exit\\&."
msgstr "Zeigt einen kurzen Hilfetext an und beendet das Programm\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Print a short version string and exit\\&."
msgstr "Zeigt eine kurze Versionszeichenkette an und beendet das Programm\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--no-legend>"
msgstr "B<--no-legend>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Do not print the legend, i\\&.e\\&. column headers and the footer with "
"hints\\&."
msgstr ""
"Gibt die Legende nicht aus, d\\&.h\\&. die Spaltenköpfe und die Fußzeile mit "
"Hinweisen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--no-pager>"
msgstr "B<--no-pager>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Do not pipe output into a pager\\&."
msgstr "Leitet die Ausgabe nicht an ein Textanzeigeprogramm weiter\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "EXIT-STATUS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "On success, 0 is returned, a non-zero failure code otherwise\\&."
msgstr ""
"Bei Erfolg wird 0 zurückgegeben, anderenfalls ein Fehlercode ungleich "
"Null\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<systemd-networkd.service>(8), B<systemd.network>(5), B<systemd.netdev>(5), "
"B<ip>(8)"
msgstr ""
"B<systemd-networkd.service>(8), B<systemd.network>(5), B<systemd.netdev>(5), "
"B<ip>(8)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr " 1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "RFC 3484"
msgstr "RFC 3484"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "\\%https://tools.ietf.org/html/rfc3484"
msgstr "\\%https://tools.ietf.org/html/rfc3484"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 252"
msgstr "systemd 252"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 253"
msgstr "systemd 253"
