# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Patrick Rother <krd@gulu.net>, 1996.
# Chris Leick <c.leick@vollbio.de>, 2011-2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-08-27 17:30+0200\n"
"PO-Revision-Date: 2023-05-01 11:40+0200\n"
"Last-Translator: Chris Leick <c.leick@vollbio.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "tmpnam"
msgstr "tmpnam"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20. Juli 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "tmpnam, tmpnam_r - create a name for a temporary file"
msgstr "tmpnam, tmpnam_r - einen Namen für eine temporäre Datei erzeugen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]] char *tmpnam(char *>I<s>B<);>\n"
"B<[[deprecated]] char *tmpnam_r(char *>I<s>B<);>\n"
msgstr ""
"B<[[veraltet]] char *tmpnam(char *>I<s>B<);>\n"
"B<[[veraltet]] char *tmpnam_r(char *>I<s>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. #-#-#-#-#  archlinux: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  fedora-39: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-leap-15-6: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-tumbleweed: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<tmpnam_r>()"
msgstr "B<tmpnam_r>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Up to and including glibc 2.19:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    Seit Glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Bis zu einschließlich Glibc 2.19:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<Note:> avoid using these functions; use B<mkstemp>(3)  or B<tmpfile>(3)  "
"instead."
msgstr ""
"B<Hinweis>: Vermeiden Sie die Verwendung dieser Funktionen; verwenden Sie "
"stattdessen B<mkstemp>(3) oder B<tmpfile>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<tmpnam>()  function returns a pointer to a string that is a valid "
"filename, and such that a file with this name did not exist at some point in "
"time, so that naive programmers may think it a suitable name for a temporary "
"file.  If the argument I<s> is NULL, this name is generated in an internal "
"static buffer and may be overwritten by the next call to B<tmpnam>().  If "
"I<s> is not NULL, the name is copied to the character array (of length at "
"least I<L_tmpnam>)  pointed to by I<s> and the value I<s> is returned in "
"case of success."
msgstr ""
"Die Funktion B<tmpnam>() gibt einen Zeiger auf eine Zeichenkette zurück, die "
"ein gültiger Dateiname ist, und sorgt dafür, dass zu diesem Zeitpunkt keine "
"Datei mit diesem Namen existiert, so dass ahnungslose Programmierer denken "
"könnten, es sei ein geeigneter Name für eine temporäre Datei. Falls das "
"Argument I<s> NULL ist, wird dieser Name in einem statischen Puffer "
"generiert und könnte von nächsten B<tmpnam>()-Aufruf überschrieben werden. "
"Falls I<s> nicht NULL ist, wird der Name in das Zeichenfeld kopiert (das "
"mindestens die Länge I<L_tmpnam> hat), auf das I<s> zeigt. Im Erfolgsfall "
"wird I<s> zurückgegeben."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The created pathname has a directory prefix I<P_tmpdir>.  (Both I<L_tmpnam> "
"and I<P_tmpdir> are defined in I<E<lt>stdio.hE<gt>>, just like the "
"B<TMP_MAX> mentioned below.)"
msgstr ""
"Der erzeugte Pfadname hat das Pfad-Präfix I<P_tmpdir>. (Sowohl I<L_tmpnam> "
"als auch I<P_tmpdir> sind in I<E<lt>stdio.hE<gt>> definiert, genauso wie das "
"im Folgenden erwähnte B<TMP_MAX>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<tmpnam_r>()  function performs the same task as B<tmpnam>(), but "
"returns NULL (to indicate an error) if I<s> is NULL."
msgstr ""
"Die Funktion B<tmpnam_r>() erledigt die gleiche Aufgabe wie B<tmpnam>(), "
"liefert aber NULL (um einen Fehler anzuzeigen) zurück, falls I<s> NULL ist."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions return a pointer to a unique temporary filename, or NULL if "
"a unique name cannot be generated."
msgstr ""
"Diese Funktionen geben einen Zeiger auf den eindeutigen temporären "
"Dateinamen zurück oder NULL, wenn kein eindeutiger Name generiert werden "
"konnte."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "No errors are defined."
msgstr "Es sind keine Fehler definiert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. #-#-#-#-#  archlinux: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  fedora-39: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-leap-15-6: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-tumbleweed: tmpnam.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<tmpnam>()"
msgstr "B<tmpnam>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:tmpnam/!s"
msgstr "MT-Unsafe race:tmpnam/!s"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr "Keine."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "SVr4, 4.3BSD, C89, POSIX.1-2001.  Obsolete in POSIX.1-2008."
msgstr "SVr4, 4.3BSD, C89, POSIX.1-2001. Veraltet in POSIX.1-2008."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Solaris."
msgstr "Solaris."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<tmpnam>()  function generates a different string each time it is "
"called, up to B<TMP_MAX> times.  If it is called more than B<TMP_MAX> times, "
"the behavior is implementation defined."
msgstr ""
"Die Funktion B<tmpnam>() generiert jedesmal, wenn sie aufgerufen wird, eine "
"andere Zeichenkette. Dies geschieht bis zu B<TMP_MAX> Mal. Falls sie öfter "
"als B<TMP_MAX> Mal aufgerufen wird, wird ihr Verhalten durch die "
"Implementierung definiert."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Although these functions generate names that are difficult to guess, it is "
"nevertheless possible that between the time that the pathname is returned "
"and the time that the program opens it, another program might create that "
"pathname using B<open>(2), or create it as a symbolic link.  This can lead "
"to security holes.  To avoid such possibilities, use the B<open>(2)  "
"B<O_EXCL> flag to open the pathname.  Or better yet, use B<mkstemp>(3)  or "
"B<tmpfile>(3)."
msgstr ""
"Obwohl diese Funktionen Namen generiert, die schwer zu erahnen sind, ist es "
"dennoch möglich, dass in der Zeit zwischen der Rückgabe des Pfadnamens und "
"der Zeit, in der er vom Programm geöffnet wird, ein anderes Programm einen "
"Pfadnamen mit B<open>(2) erzeugt oder ihn als symbolischen Link erstellt. "
"Dies kann zu Sicherheitslücken führen. Um solche Möglichkeiten zu vermeiden, "
"benutzen Sie den Schalter B<O_EXCL> von B<open>(2), um diesen Pfadnamen zu "
"öffnen oder besser noch – benutzen Sie B<mkstemp>(3) oder B<tmpfile>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Portable applications that use threads cannot call B<tmpnam>()  with a NULL "
"argument if either B<_POSIX_THREADS> or B<_POSIX_THREAD_SAFE_FUNCTIONS> is "
"defined."
msgstr ""
"Portierbare Anwendungen, die Threads benutzen, können B<tmpnam>() nicht mit "
"einem Argument aufrufen, das NULL ist, wenn entweder B<_POSIX_THREADS> oder "
"B<_POSIX_THREAD_SAFE_FUNCTIONS> definiert sind."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Never use these functions.  Use B<mkstemp>(3)  or B<tmpfile>(3)  instead."
msgstr ""
"Benutzen Sie diese Funktionen niemals. Benutzen Sie stattdessen "
"B<mkstemp>(3) oder B<tmpfile>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<mkstemp>(3), B<mktemp>(3), B<tempnam>(3), B<tmpfile>(3)"
msgstr "B<mkstemp>(3), B<mktemp>(3), B<tempnam>(3), B<tmpfile>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-29"
msgstr "29. Dezember 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid ""
"B<char *tmpnam(char *>I<s>B<);>\n"
"B<char *tmpnam_r(char *>I<s>B<);>\n"
msgstr ""
"B<char *tmpnam(char *>I<s>B<);>\n"
"B<char *tmpnam_r(char *>I<s>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"B<tmpnam>(): SVr4, 4.3BSD, C99, POSIX.1-2001.  POSIX.1-2008 marks "
"B<tmpnam>()  as obsolete."
msgstr ""
"B<tmpnam>(): SVr4, 4.3BSD, C99, POSIX.1-2001. POSIX.1-2008 kennzeichnet "
"B<tmpnam>() als veraltet."

#.  Appears to be on Solaris
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<tmpnam_r>()  is a nonstandard extension that is also available on a few "
"other systems."
msgstr ""
"B<tmpnam_r>() ist eine nicht standardisierte Erweiterung, die auch auf "
"einigen anderen Systemen verfügbar ist."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "TMPNAM"
msgstr "TMPNAM"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Since glibc 2.19:"
msgstr "Seit Glibc 2.19:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_DEFAULT_SOURCE"
msgstr "_DEFAULT_SOURCE"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Up to and including glibc 2.19:"
msgstr "Bis einschließlich Glibc 2.19:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_BSD_SOURCE || _SVID_SOURCE"
msgstr "_BSD_SOURCE || _SVID_SOURCE"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<tmpnam>(): SVr4, 4.3BSD, C89, C99, POSIX.1-2001.  POSIX.1-2008 marks "
"B<tmpnam>()  as obsolete."
msgstr ""
"B<tmpnam>(): SVr4, 4.3BSD, C89, C99, POSIX.1-2001. POSIX.1-2008 kennzeichnet "
"B<tmpnam>() als veraltet."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
