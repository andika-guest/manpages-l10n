# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.16.0\n"
"POT-Creation-Date: 2023-08-27 17:17+0200\n"
"PO-Revision-Date: 2023-01-12 12:43+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "round"
msgstr "round"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20. Juli 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "round, roundf, roundl - round to nearest integer, away from zero"
msgstr "round, roundf, roundl - zur nächsten Ganzzahl, weg von Null, runden"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Mathematik-Bibliothek (I<libm>, I<-lm>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double round(double >I<x>B<);>\n"
"B<float roundf(float >I<x>B<);>\n"
"B<long double roundl(long double >I<x>B<);>\n"
msgstr ""
"B<double round(double >I<x>B<);>\n"
"B<float roundf(float >I<x>B<);>\n"
"B<long double roundl(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<round>(), B<roundf>(), B<roundl>():"
msgstr "B<round>(), B<roundf>(), B<roundl>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions round I<x> to the nearest integer, but round halfway cases "
"away from zero (regardless of the current rounding direction, see "
"B<fenv>(3)), instead of to the nearest even integer like B<rint>(3)."
msgstr ""
"Diese Funktionen runden I<x> zur nächsten Ganzzahl. Sie runden allerdings "
"Fälle in der Mitte weg von Null (unabhängig von der aktuellen "
"Rundungsrichtung, siehe B<fenv>(3)), statt zu der nächsten geraden Ganzzahl "
"wie B<rint>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "For example, I<round(0.5)> is 1.0, and I<round(-0.5)> is -1.0."
msgstr "Beispielsweise ist I<round(0.5)> 1.0 und I<round(-0.5)> ist -1.0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These functions return the rounded integer value."
msgstr "Diese Funktionen liefern den gerundeten Ganzzahlwert zurück."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is integral, +0, -0, NaN, or infinite, I<x> itself is returned."
msgstr ""
"Falls I<x> ganzzahlig, +0, -0, NaN (keine Zahl) oder unendlich ist, wird  "
"I<x> selbst zurückgegeben."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"No errors occur.  POSIX.1-2001 documents a range error for overflows, but "
"see NOTES."
msgstr ""
"Es treten keine Fehler auf. POSIX.1-2001 dokumentiert für Überläufe einen "
"Bereichsfehler, aber lesen Sie dazu die ANMERKUNGEN."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<round>(),\n"
"B<roundf>(),\n"
"B<roundl>()"
msgstr ""
"B<round>(),\n"
"B<roundf>(),\n"
"B<roundl>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.1.  C99, POSIX.1-2001."
msgstr "Glibc 2.1. C99, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

# http://de.wikipedia.org/wiki/Mantisse
#.  The POSIX.1-2001 APPLICATION USAGE SECTION discusses this point.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"POSIX.1-2001 contains text about overflow (which might set I<errno> to "
"B<ERANGE>, or raise an B<FE_OVERFLOW> exception).  In practice, the result "
"cannot overflow on any current machine, so this error-handling stuff is just "
"nonsense.  (More precisely, overflow can happen only when the maximum value "
"of the exponent is smaller than the number of mantissa bits.  For the "
"IEEE-754 standard 32-bit and 64-bit floating-point numbers the maximum value "
"of the exponent is 127 (respectively, 1023), and the number of mantissa bits "
"including the implicit bit is 24 (respectively, 53).)"
msgstr ""
"POSIX.1-2001 enthält Text über Überläufe (die I<errno> auf B<ERANGE> setzen "
"oder eine B<FE_OVERFLOW>-Ausnahme auslösen können). In der Praxis kann das "
"Ergebnis auf einem aktuellen Rechner nicht überlaufen, so dass diese "
"Überlegungen zur Fehlerbehandlung einfach Unsinn sind. (Genauer gesagt, kann "
"ein Überlauf nur eintreten, wenn der maximale Wert des Exponenten kleiner "
"als die Anzahl der Mantissen-Bits ist. Für die IEEE-754-Standard-32- und 64-"
"Bit-Fließkommazahlen ist der Maximalwert für den Exponenten 127 "
"(beziehungsweise 1023) und die Anzahl der Mantissen-Bits einschließlich des "
"impliziten Bits 24 (beziehungsweise 53).)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If you want to store the rounded value in an integer type, you probably want "
"to use one of the functions described in B<lround>(3)  instead."
msgstr ""
"Falls Sie den gerundeten Wert im Typ »integer« speichern möchten, wollen Sie "
"wahrscheinlich stattdessen eine der in B<lround>(3) beschriebenen Funktionen "
"verwenden."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<ceil>(3), B<floor>(3), B<lround>(3), B<nearbyint>(3), B<rint>(3), "
"B<trunc>(3)"
msgstr ""
"B<ceil>(3), B<floor>(3), B<lround>(3), B<nearbyint>(3), B<rint>(3), "
"B<trunc>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15. Dezember 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid "These functions were added in glibc 2.1."
msgstr "Diese Funktionen wurden in Glibc 2.1 hinzugefügt."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "ROUND"
msgstr "ROUND"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Link with I<-lm>."
msgstr "Linken Sie mit der Option I<-lm>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"
msgstr "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"

#. type: Plain text
#: opensuse-leap-15-6
msgid "These functions first appeared in glibc in version 2.1."
msgstr "Diese Funktionen kamen erstmals in Glibc in Version 2.1 vor."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#.  The POSIX.1-2001 APPLICATION USAGE SECTION discusses this point.
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"POSIX.1-2001 contains text about overflow (which might set I<errno> to "
"B<ERANGE>, or raise an B<FE_OVERFLOW> exception).  In practice, the result "
"cannot overflow on any current machine, so this error-handling stuff is just "
"nonsense.  (More precisely, overflow can happen only when the maximum value "
"of the exponent is smaller than the number of mantissa bits.  For the "
"IEEE-754 standard 32-bit and 64-bit floating-point numbers the maximum value "
"of the exponent is 128 (respectively, 1024), and the number of mantissa bits "
"is 24 (respectively, 53).)"
msgstr ""
"POSIX.1-2001 enthält Text über Überläufe (die I<errno> auf B<ERANGE> setzen "
"oder eine B<FE_OVERFLOW>-Ausnahme auslösen können). In der Praxis kann das "
"Ergebnis auf einem aktuellen Rechner nicht überlaufen, so dass diese "
"Überlegungen zur Fehlerbehandlung einfach Unsinn sind. (Genauer gesagt, kann "
"ein Überlauf nur eintreten, wenn der maximale Wert des Exponenten kleiner "
"als die Anzahl der Mantissen-Bits ist. Für die IEEE-754-Standard-32- und 64-"
"Bit-Fließkommazahlen ist der Maximalwert für den Exponenten 128 "
"(beziehungsweise 1024) und die Anzahl der Mantissen-Bits 24 (beziehungsweise "
"53).)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
