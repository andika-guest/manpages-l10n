# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
# Helge Kreutzmann <debian@helgefjell.de>, 2016-2017.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.9.3\n"
"POT-Creation-Date: 2023-08-27 16:55+0200\n"
"PO-Revision-Date: 2023-02-21 21:30+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "environ"
msgstr "environ"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "environ - user environment"
msgstr "environ - Umgebung des Benutzers"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<extern char **>I<environ>B<;>\n"
msgstr "B<extern char **>I<environ>B<;>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The variable I<environ> points to an array of pointers to strings called the "
"\"environment\".  The last pointer in this array has the value NULL.  This "
"array of strings is made available to the process by the B<execve>(2)  call "
"when a new program is started.  When a child process is created via "
"B<fork>(2), it inherits a I<copy> of its parent's environment."
msgstr ""
"Die Variable I<environ> zeigt auf die »Umgebung«, ein Feld von Zeigern auf "
"Zeichenketten. Der letzte Zeiger in diesem Feld hat den Wert NULL. Die "
"Umgebung wird dem Prozess von dem B<exec>(2)-Aufruf verfügbar gemacht, wenn "
"ein neues Programm gestartet wird. Wenn ein Kindprozess mit B<fork>(2) "
"erstellt wird, erbt es eine I<Kopie> der Umgebung seines Elternprozesses."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"By convention, the strings in I<environ> have the form "
"\"I<name>B<=>I<value>\".  The name is case-sensitive and may not contain the "
"character \"B<=>\".  The value can be anything that can be represented as a "
"string.  The name and the value may not contain an embedded null byte "
"(\\[aq]\\e0\\[aq]), since this is assumed to terminate the string."
msgstr ""
"Die Zeichenketten in I<environ> haben die Form »I<Name>B<=>I<Wert>«. Groß- "
"oder Kleinschreibung wird für den Namen berücksichtigt; außerdem darf der "
"Name nicht das Zeichen »B<=>« enthalten. Der Wert kann alles sein, was als "
"Zeichenkette dargestellt werden kann. Weder der Name noch der Wert dürfen "
"ein eingebettetes Null-Byte enthalten (»\\e0«), da dieses als Ende der "
"Zeichenkette interpretiert wird."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Environment variables may be placed in the shell's environment by the "
"I<export> command in B<sh>(1), or by the I<setenv> command if you use "
"B<csh>(1)."
msgstr ""
"Umgebungsvariablen können in die Umgebung der Shell mit dem Befehl I<export> "
"von B<sh>(1) oder dem Befehl I<setenv> von B<csh>(1) gelegt werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The initial environment of the shell is populated in various ways, such as "
"definitions from I</etc/environment> that are processed by B<pam_env>(8)  "
"for all users at login time (on systems that employ B<pam>(8)).  In "
"addition, various shell initialization scripts, such as the system-wide I</"
"etc/profile> script and per-user initializations script may include commands "
"that add variables to the shell's environment; see the manual page of your "
"preferred shell for details."
msgstr ""
"Die anfängliche Umgebung der Shell wird auf verschiedenen Arten aufgebaut, "
"wie Definitionen aus I</etc/environment>, die (auf Systemen, die B<pam>(8) "
"einsetzen) durch B<pam_env>(8) für alle Benutzer zum Anmeldezeitpunkt "
"verarbeitet wird. Zusätzlich können verschiedene Shell-"
"Initialisierungsskripte, wie das systemweite Skript I</etc/profile> oder "
"benutzerbezogene Initialisierungsskripte Befehle enthalten, die Variablen zu "
"der Umgebung der Shell hinzufügen. Lesen Sie dafür die Handbuchseite Ihrer "
"bevorzugten Shell für weitere Details."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Bourne-style shells support the syntax"
msgstr "Bourne-artige Shells unterstützen die Syntax"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME=value command\n"
msgstr "NAME=Wert Befehl\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"to create an environment variable definition only in the scope of the "
"process that executes I<command>.  Multiple variable definitions, separated "
"by white space, may precede I<command>."
msgstr ""
"um eine Umgebungsvariablendefinition zu erstellen, deren Geltungsbereich nur "
"den Prozess, der I<Befehl> ausführt, umfasst. Mehrere Variablendefinitionen, "
"getrennt durch Leerraumzeichen, können I<Befehl> vorangestellt werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Arguments may also be placed in the environment at the point of an "
"B<exec>(3).  A C program can manipulate its environment using the functions "
"B<getenv>(3), B<putenv>(3), B<setenv>(3), and B<unsetenv>(3)."
msgstr ""
"Argumente können auch beim Aufruf von B<exec>(3) an die Umgebung "
"weitergegeben werden. C-Programme können ihre Umgebung mit den Funktionen "
"B<getenv>(3), B<putenv>(3), B<setenv>(3) und B<unsetenv>(3) beeinflussen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"What follows is a list of environment variables typically seen on a system.  "
"This list is incomplete and includes only common variables seen by average "
"users in their day-to-day routine.  Environment variables specific to a "
"particular program or library function are documented in the ENVIRONMENT "
"section of the appropriate manual page."
msgstr ""
"Es folgt eine Liste der typischerweise in einem System enthaltenen "
"Umgebungsvariablen. Diese Liste ist unvollständig und enthält nur häufige "
"Variablen, die von durchschnittlichen Benutzern in täglichen Routinen "
"gesehen werden. Die spezifischen Umgebungsvariablen eines Programms oder "
"einer Bibliotheksfunktion finden Sie im Abschnitt UMGEBUNG oder "
"UMGEBUNGSVARIABLEN der jeweiligen Handbuchseite."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<USER>"
msgstr "B<USER>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The name of the logged-in user (used by some BSD-derived programs).  Set at "
"login time, see section NOTES below."
msgstr ""
"Der Name des angemeldeten Benutzers (wird von einigen Programmen aus der BSD-"
"Welt ausgewertet). Wird zum Anmeldezeitpunkt gesetzt, siehe den "
"nachfolgenden Abschnitt ANMERKUNGEN."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LOGNAME>"
msgstr "B<LOGNAME>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The name of the logged-in user (used by some System-V derived programs).  "
"Set at login time, see section NOTES below."
msgstr ""
"Der Name des angemeldeten Benutzers (wird von einigen Programmen aus der "
"System-V-Welt ausgewertet). Wird zum Anmeldezeitpunkt gesetzt, siehe den "
"nachfolgenden Abschnitt ANMERKUNGEN."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<HOME>"
msgstr "B<HOME>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "A user's login directory.  Set at login time, see section NOTES below."
msgstr ""
"Das Anmeldeverzeichnis eines Benutzers. Siehe den nachfolgenden Abschnitt "
"ANMERKUNGEN."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LANG>"
msgstr "B<LANG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The name of a locale to use for locale categories when not overridden by "
"B<LC_ALL> or more specific environment variables such as B<LC_COLLATE>, "
"B<LC_CTYPE>, B<LC_MESSAGES>, B<LC_MONETARY>, B<LC_NUMERIC>, and B<LC_TIME> "
"(see B<locale>(7)  for further details of the B<LC_*> environment variables)."
msgstr ""
"Der Name einer Locale (eines Gebietsschemas), die für Locale-Kategorien "
"angewendet werden soll. Die Locale kann durch B<LC_ALL> oder spezielle "
"Umgebungsvariablen wie B<LC_COLLATE>, B<LC_CTYPE>, B<LC_MESSAGES>, "
"B<LC_MONETARY>, B<LC_NUMERIC> und B<LC_TIME> überschrieben werden (siehe "
"B<locale>(7) für weitere Details über die Umgebungsvariablen B<LC_*>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PATH>"
msgstr "B<PATH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The sequence of directory prefixes that B<sh>(1)  and many other programs "
"employ when searching for an executable file that is specified as a simple "
"filename (i.a., a pathname that contains no slashes).  The prefixes are "
"separated by colons (B<:>).  The list of prefixes is searched from beginning "
"to end, by checking the pathname formed by concatenating a prefix, a slash, "
"and the filename, until a file with execute permission is found."
msgstr ""
"Die Abfolge der Verzeichnispräfixe, die von B<sh>(1) und vielen anderen "
"Programmen bei der Suche nach ausführbaren Dateien verwendet wird, wird als "
"einfacher Dateiname angegeben (das heißt, als Pfadname ohne Schrägstriche). "
"Die Präfixe werden durch Doppelpunkte getrennt (B<:>). Die Liste der Präfixe "
"wird vom Anfang bis zum Ende durchsucht, indem der Pfadname aus einem "
"vorangestellten Präfix, einem Schrägstrich und dem Dateinamen gebildet wird, "
"bis eine ausführbare Datei gefunden wird."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"As a legacy feature, a zero-length prefix (specified as two adjacent colons, "
"or an initial or terminating colon)  is interpreted to mean the current "
"working directory.  However, use of this feature is deprecated, and POSIX "
"notes that a conforming application shall use an explicit pathname (e.g., I<."
">)  to specify the current working directory."
msgstr ""
"Außerdem kann als klassische Funktion ein Präfix der Länge Null verwendet "
"werden (als zwei aufeinander folgende Doppelpunkte oder einen "
"vorangestellten oder angehängten Doppelpunkt. Dies wird als das aktuelle "
"Arbeitsverzeichnis interpretiert. Allerdings gilt diese Form der Angabe als "
"veraltet. POSIX merkt an, dass eine konforme Anwendung einen expliziten "
"Pfadnamen verwenden sollte (zum Beispiel I<.>), um das aktuelle "
"Arbeitsverzeichnis anzugeben."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Analogously to B<PATH>, one has B<CDPATH> used by some shells to find the "
"target of a change directory command, B<MANPATH> used by B<man>(1)  to find "
"manual pages, and so on."
msgstr ""
"Analog zu B<PATH> benutzen einige Shells B<CDPATH>, um das Ziel eines B<cd>-"
"Befehls zu finden, B<man>(1) sucht in B<MANPATH> nach Handbuchseiten, usw."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PWD>"
msgstr "B<PWD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Absolute path to the current working directory; required to be partially "
"canonical (no I<.\\&> or I<..\\&> components)."
msgstr ""
"Absoluter Pfad zum aktuellen Arbeitsverzeichnis; muss teilweise kanonisch "
"sein (keine I<.\\&>- oder I<..\\&>-Komponenten)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SHELL>"
msgstr "B<SHELL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The absolute pathname of the user's login shell.  Set at login time, see "
"section NOTES below."
msgstr ""
"Der absolute Pfadname der Anmeldeshell des Benutzers. Wird zum "
"Anmeldezeitpunkt gesetzt, siehe den nachfolgenden Abschnitt ANMERKUNGEN."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<TERM>"
msgstr "B<TERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The terminal type for which output is to be prepared."
msgstr "Der Terminaltyp, für den Ausgaben aufbereitet werden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PAGER>"
msgstr "B<PAGER>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The user's preferred utility to display text files.  Any string acceptable "
"as a command-string operand to the I<sh\\ -c> command shall be valid.  If "
"B<PAGER> is null or is not set, then applications that launch a pager will "
"default to a program such as B<less>(1)  or B<more>(1)."
msgstr ""
"Das vom Benutzer bevorzugte Dienstprogramm zum Anzeigen von Textdateien. "
"Jede als »command_string«-Operand zum Befehl I<sh\\ -c> akzeptierbare "
"Zeichenkette sollte gültig sein. Falls B<PAGER> Null oder nicht festgelegt "
"ist, dann fallen Anwendungen, die ein Textanzeigeprogramm starten, auf ein "
"Programm wie B<less>(1) oder B<more>(1) zurück."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EDITOR>/B<VISUAL>"
msgstr "B<EDITOR>/B<VISUAL>"

#.  .TP
#.  .B BROWSER
#.  The user's preferred utility to browse URLs. Sequence of colon-separated
#.  browser commands. See http://www.catb.org/\[ti]esr/BROWSER/ .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The user's preferred utility to edit text files.  Any string acceptable as a "
"command_string operand to the I<sh\\ -c> command shall be valid."
msgstr ""
"Das vom Benutzer bevorzugte Dienstprogramm zum Bearbeiten von Textdateien. "
"Jede als »command_string«-Operand zum Befehl I<sh\\ -c> akzeptierbare "
"Zeichenkette sollte gültig sein."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that the behavior of many programs and library routines is influenced "
"by the presence or value of certain environment variables.  Examples include "
"the following:"
msgstr ""
"Bitte beachten Sie, dass das Verhalten vieler Programme und "
"Bibliotheksroutinen vom Vorhandensein oder dem Inhalt bestimmter "
"Umgebungsvariablen beeinflusst wird. Zu den Beispielen gehören:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The variables B<LANG>, B<LANGUAGE>, B<NLSPATH>, B<LOCPATH>, B<LC_ALL>, "
"B<LC_MESSAGES>, and so on influence locale handling; see B<catopen>(3), "
"B<gettext>(3), and B<locale>(7)."
msgstr ""
"Die Variablen B<LANG>, B<LANGUAGE>, B<NLSPATH>, B<LOCPATH>, B<LC_ALL>, "
"B<LC_MESSAGES> und so weiter beeinflussen die Handhabung der Locale, vgl. "
"B<catopen>(3), B<gettext>(3) und B<locale>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<TMPDIR> influences the path prefix of names created by B<tempnam>(3)  and "
"other routines, and the temporary directory used by B<sort>(1)  and other "
"programs."
msgstr ""
"B<TMPDIR> beeinflusst die Pfadangabe für B<tempnam>(3) beim Anlegen von "
"Dateien und das temporäre Verzeichnis von B<sort>(1) usw."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<LD_LIBRARY_PATH>, B<LD_PRELOAD>, and other B<LD_*> variables influence the "
"behavior of the dynamic loader/linker.  See also B<ld.so>(8)."
msgstr ""
"B<LD_LIBRARY_PATH>, B<LD_PRELOAD> und andere B<LD_*>-Variablen beeinflussen "
"das Verhalten des dynamischen Laders/Linkers. Siehe auch B<ld.so>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<POSIXLY_CORRECT> makes certain programs and library routines follow the "
"prescriptions of POSIX."
msgstr ""
"B<POSIXLY_CORRECT> veranlasst bestimmte Programme und Bibliotheksroutinen, "
"sich an die POSIX-Vorgaben zu halten."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The behavior of B<malloc>(3)  is influenced by B<MALLOC_*> variables."
msgstr ""
"Das Verhalten von B<malloc>(3) wird von B<MALLOC_*>-Variablen gesteuert."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The variable B<HOSTALIASES> gives the name of a file containing aliases to "
"be used with B<gethostbyname>(3)."
msgstr ""
"Die Variable B<HOSTALIASES> enthält den Namen der Datei, in der die Alias-"
"Namen für B<gethostbyname>(3) stehen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<TZ> and B<TZDIR> give timezone information used by B<tzset>(3)  and "
"through that by functions like B<ctime>(3), B<localtime>(3), B<mktime>(3), "
"B<strftime>(3).  See also B<tzselect>(8)."
msgstr ""
"B<TZ> und B<TZDIR> stellen Informationen über Zeitzonen für B<tzset>(3) "
"bereit und dadurch auch für Funktionen wie B<ctime>(3), B<localtime>(3), "
"B<mktime>(3) und B<strftime>(3). Siehe auch B<tzselect>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<TERMCAP> gives information on how to address a given terminal (or gives "
"the name of a file containing such information)."
msgstr ""
"B<TERMCAP> informiert darüber, wie bestimmte Terminals angesteuert werden "
"müssen (oder enthält den Namen einer Datei, die diese Informationen "
"bereitstellt)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<COLUMNS> and B<LINES> tell applications about the window size, possibly "
"overriding the actual size."
msgstr ""
"B<COLUMNS> und B<LINES> informieren Programme über die Größe des Fensters "
"und setzen damit vielleicht die tatsächliche Größe außer Kraft."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<PRINTER> or B<LPDEST> may specify the desired printer to use.  See "
"B<lpr>(1)."
msgstr ""
"B<PRINTER> oder B<LPDEST> können den gewünschten Drucker angeben, siehe "
"B<lpr>(1)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Historically and by standard, I<environ> must be declared in the user "
"program.  However, as a (nonstandard) programmer convenience, I<environ> is "
"declared in the header file I<E<lt>unistd.hE<gt>> if the B<_GNU_SOURCE> "
"feature test macro is defined (see B<feature_test_macros>(7))."
msgstr ""
"Aus historischen Gründen muss I<environ> standardmäßig im Programm des "
"Benutzers deklariert sein. Dennoch kann I<environ> als (nicht "
"standardmäßiges) Zugeständnis an Programmierer in der Header-Datei "
"I<E<lt>unistd.hE<gt>> deklariert sein, falls das Feature-Test-Makro "
"B<_GNU_SOURCE> definiert ist (siehe B<feature_test_macros>(7))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<prctl>(2)  B<PR_SET_MM_ENV_START> and B<PR_SET_MM_ENV_END> operations "
"can be used to control the location of the process's environment."
msgstr ""
"Die Aktionen B<prctl>(2) B<PR_SET_MM_ENV_START> und B<PR_SET_MM_ENV_END> "
"können zur Steuerung des Orts der Umgebung des Prozesses verwandt werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<HOME>, B<LOGNAME>, B<SHELL>, and B<USER> variables are set when the "
"user is changed via a session management interface, typically by a program "
"such as B<login>(1)  from a user database (such as B<passwd>(5)).  "
"(Switching to the root user using B<su>(1)  may result in a mixed "
"environment where B<LOGNAME> and B<USER> are retained from old user; see the "
"B<su>(1)  manual page.)"
msgstr ""
"Die Variablen B<HOME>, B<LOGNAME>, B<SHELL> und B<USER> werden gesetzt, wenn "
"der Benutzer über die Sitzungsverwaltungs-Schnittstelle gewechselt wird, was "
"typischerweise durch ein Programm wie B<login>(1) aus der Benutzerdatenbank "
"(wie B<passwd>(5)) geschieht. (Der Wechsel zum Systembenutzer »root« mittels "
"B<su>(1) kann zu einer gemischten Umgebung führen, in der B<LOGNAME> und "
"B<USER> vom vorherigen Benutzer übernommen werden, siehe B<su>(1).)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Clearly there is a security risk here.  Many a system command has been "
"tricked into mischief by a user who specified unusual values for B<IFS> or "
"B<LD_LIBRARY_PATH>."
msgstr ""
"Es ist offensichtlich, dass es hier ein Sicherheitsproblem gibt. Schon "
"mancher Systembefehl hat den Pfad der Tugend verlassen, weil ein Benutzer "
"ungebräuchliche Werte für B<IFS> oder B<LD_LIBRARY_PATH> angegeben hat."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"There is also the risk of name space pollution.  Programs like I<make> and "
"I<autoconf> allow overriding of default utility names from the environment "
"with similarly named variables in all caps.  Thus one uses B<CC> to select "
"the desired C compiler (and similarly B<MAKE>, B<AR>, B<AS>, B<FC>, B<LD>, "
"B<LEX>, B<RM>, B<YACC>, etc.).  However, in some traditional uses such an "
"environment variable gives options for the program instead of a pathname.  "
"Thus, one has B<MORE> and B<LESS>.  Such usage is considered mistaken, and "
"to be avoided in new programs."
msgstr ""
"Es besteht auch die Gefahr der »Verschmutzung des Namensraums«. Programme "
"wie I<make> und I<autoconf> erlauben Überschreiben der Namen von Standard-"
"Dienstprogrammen aus der Umgebung mit ähnlich benannten Variablen in "
"Großbuchstaben. So verwendet man B<CC>, um den gewünschten C-Compiler zu "
"wählen (und analog B<MAKE>, B<AR>, B<AS>, B<FC>, B<LD>, B<LEX>, B<RM>, "
"B<YACC>, usw.). Aber in einigen traditionellen Nutzungen gibt eine "
"Umgebungsvariable keinen Pfadnamen, sondern Programmoptionen an. So gibt es "
"B<MORE> und B<LESS>. Diese Verwendung wird als falsch verstanden und sollte "
"für neue Programme vermieden werden."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<bash>(1), B<csh>(1), B<env>(1), B<login>(1), B<printenv>(1), B<sh>(1), "
"B<su>(1), B<tcsh>(1), B<execve>(2), B<clearenv>(3), B<exec>(3), "
"B<getenv>(3), B<putenv>(3), B<setenv>(3), B<unsetenv>(3), B<locale>(7), B<ld."
"so>(8), B<pam_env>(8)"
msgstr ""
"B<bash>(1), B<csh>(1), B<env>(1), B<login>(1), B<printenv>(1), B<sh>(1), "
"B<su>(1), B<tcsh>(1), B<execve>(2), B<clearenv>(3), B<exec>(3), "
"B<getenv>(3), B<putenv>(3), B<setenv>(3), B<unsetenv>(3), B<locale>(7), B<ld."
"so>(8), B<pam_env>(8)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "ENVIRON"
msgstr "ENVIRON"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The variable I<environ> points to an array of pointers to strings called the "
"\"environment\".  The last pointer in this array has the value NULL.  (This "
"variable must be declared in the user program, but is declared in the header "
"file I<E<lt>unistd.hE<gt>> if the B<_GNU_SOURCE> feature test macro is "
"defined.)  This array of strings is made available to the process by the "
"B<exec>(3)  call that started the process.  When a child process is created "
"via B<fork>(2), it inherits a I<copy> of its parent's environment."
msgstr ""
"Die Variable I<environ> zeigt auf die »Umgebung«, ein Feld von Zeigern auf "
"Zeichenketten. Der letzte Zeiger in diesem Feld hat den Wert NULL. (Diese "
"Variable muss im Anwenderprogramm deklariert werden, ist aber in der Header-"
"Datei I<E<lt>unistd.hE<gt>> definiert, falls das »feature test macro« "
"B<_GNU_SOURCE> definiert ist). Die Umgebung wird dem Prozess von dem "
"B<exec>(3)-Aufruf, der den Prozess gestartet hat, verfügbar gemacht. Wenn "
"ein Kindprozess mit B<fork>(2) erstellt wird, erbt es eine I<Kopie> der "
"Umgebung seines Elternprozesses."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"By convention the strings in I<environ> have the form "
"\"I<name>B<=>I<value>\".  Common examples are:"
msgstr ""
"Konventionsgemäß haben Zeichenketten in I<environ> die Form "
"»I<Name>=I<Wert>«. Häufige Beispiele sind:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "The name of the logged-in user (used by some BSD-derived programs)."
msgstr ""
"Der Name des angemeldeten Benutzers (wird von einigen Programmen aus der BSD-"
"Welt ausgewertet)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The name of the logged-in user (used by some System-V derived programs)."
msgstr ""
"Der Name des angemeldeten Benutzers (wird von einigen Programmen aus der "
"System-V-Welt ausgewertet)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"A user's login directory, set by B<login>(1)  from the password file "
"B<passwd>(5)."
msgstr ""
"Das Anmeldeverzeichnis eines Benutzers, wird von B<login>(1) aus der Datei "
"B<passwd>(5) entnommen."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The sequence of directory prefixes that B<sh>(1)  and many other programs "
"apply in searching for a file known by an incomplete pathname.  The prefixes "
"are separated by \\(aqB<:>\\(aq.  (Similarly one has B<CDPATH> used by some "
"shells to find the target of a change directory command, B<MANPATH> used by "
"B<man>(1)  to find manual pages, and so on)"
msgstr ""
"Die Folge von Verzeichnisnamen, in denen B<sh>(1) und viele weitere "
"Programme nach Dateien mit unvollständigen Namen suchen. Die einzelnen "
"Bestandteile/Präfixe werden durch »B<:>« getrennt. (Gleichermaßen benutzen "
"einige Shells B<CDPATH>, um das Ziel eines B<cd>-Befehls zu finden, "
"B<man>(1) sucht in B<MANPATH> nach Handbuchseiten, usw.)"

#. type: Plain text
#: opensuse-leap-15-6
msgid "The current working directory.  Set by some shells."
msgstr "das aktuelle Arbeitsverzeichnis; gesetzt von einigen Shells"

#. type: Plain text
#: opensuse-leap-15-6
msgid "The pathname of the user's login shell."
msgstr ""
"Der Pfadname der Anmelde-Shell des Benutzers. (Mit dieser Shell arbeitet der "
"Benutzer nach seiner Anmeldung.)"

#. type: Plain text
#: opensuse-leap-15-6
msgid "The user's preferred utility to display text files."
msgstr "das vom Benutzer bevorzugte Programm für die Anzeige von Textdateien"

#.  .TP
#.  .B BROWSER
#.  The user's preferred utility to browse URLs. Sequence of colon-separated
#.  browser commands. See http://www.catb.org/~esr/BROWSER/ .
#. type: Plain text
#: opensuse-leap-15-6
msgid "The user's preferred utility to edit text files."
msgstr ""
"das vom Benutzer bevorzugte Programm für die Bearbeitung von Textdateien"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Names may be placed in the shell's environment by the I<export> command in "
"B<sh>(1), or by the I<setenv> command if you use B<csh>(1)."
msgstr ""
"Namen können in die Umgebung der Shell mit dem Befehl I<export> von B<sh>(1) "
"oder dem Befehl I<setenv> von B<csh>(1) gelegt werden."

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    NAME=value command\n"
msgstr "    NAME=Wert Befehl\n"

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<LD_LIBRARY_PATH>, B<LD_PRELOAD>, and other B<LD_*> variables influence the "
"behavior of the dynamic loader/linker."
msgstr ""
"B<LD_LIBRARY_PATH>, B<LD_PRELOAD> und andere B<LD_*>-Variablen beeinflussen "
"das Verhalten des dynamischen Laders/Linkers."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"There is also the risk of name space pollution.  Programs like I<make> and "
"I<autoconf> allow overriding of default utility names from the environment "
"with similarly named variables in all caps.  Thus one uses B<CC> to select "
"the desired C compiler (and similarly B<MAKE>, B<AR>, B<AS>, B<FC>, B<LD>, "
"B<LEX>, B<RM>, B<YACC>, etc.).  However, in some traditional uses such an "
"environment variable gives options for the program instead of a pathname.  "
"Thus, one has B<MORE>, B<LESS>, and B<GZIP>.  Such usage is considered "
"mistaken, and to be avoided in new programs.  The authors of I<gzip> should "
"consider renaming their option to B<GZIP_OPT>."
msgstr ""
"Es besteht auch die Gefahr der »Verschmutzung des Namensraums«. Programme "
"wie I<make> und I<autoconf> erlauben Überschreiben der Namen von Standard-"
"Dienstprogrammen aus der Umgebung mit ähnlich benannten Variablen in "
"Großbuchstaben. So verwendet man B<CC>, um den gewünschten C-Compiler zu "
"wählen (und analog B<MAKE>, B<AR>, B<AS>, B<FC>, B<LD>, B<LEX>, B<RM>, "
"B<YACC>, usw.). Aber in einigen traditionellen Nutzungen gibt eine "
"Umgebungsvariable keinen Pfadnamen, sondern Programmoptionen an. So gibt es "
"B<MORE>, B<LESS> und B<GZIP>. Diese Verwendung wird als falsch verstanden "
"und sollte für neue Programme vermieden werden. Die Autoren von I<gzip> "
"sollten erwägen, ihre Option in B<GZIP_OPT> umzubenennen."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<bash>(1), B<csh>(1), B<env>(1), B<login>(1), B<printenv>(1), B<sh>(1), "
"B<tcsh>(1), B<execve>(2), B<clearenv>(3), B<exec>(3), B<getenv>(3), "
"B<putenv>(3), B<setenv>(3), B<unsetenv>(3), B<locale>(7), B<ld.so>(8), "
"B<pam_env>(8)"
msgstr ""
"B<bash>(1), B<csh>(1), B<env>(1), B<login>(1), B<printenv>(1), B<sh>(1), "
"B<tcsh>(1), B<execve>(2), B<clearenv>(3), B<exec>(3), B<getenv>(3), "
"B<putenv>(3), B<setenv>(3), B<unsetenv>(3), B<locale>(7), B<ld.so>(8), "
"B<pam_env>(8)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
