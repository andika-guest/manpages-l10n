# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2015-2016.
# Helge Kreutzmann <debian@helgefjell.de>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.15.0\n"
"POT-Creation-Date: 2023-08-27 17:27+0200\n"
"PO-Revision-Date: 2022-10-27 17:53+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SYSTEMD-REMOUNT-FS\\&.SERVICE"
msgstr "SYSTEMD-REMOUNT-FS\\&.SERVICE"

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "systemd-remount-fs.service"
msgstr "systemd-remount-fs.service"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"systemd-remount-fs.service, systemd-remount-fs - Remount root and kernel "
"file systems"
msgstr ""
"systemd-remount-fs.service, systemd-remount-fs - Wurzel- und Kernel-"
"Dateisysteme erneut einhängen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "systemd-remount-fs\\&.service"
msgstr "systemd-remount-fs\\&.service"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
msgid "/usr/lib/systemd/systemd-remount-fs"
msgstr "/usr/lib/systemd/systemd-remount-fs"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-remount-fs\\&.service is an early boot service that applies mount "
"options listed in B<fstab>(5), or gathered from the partition table (when "
"B<systemd-gpt-auto-generator>(8)  is active) to the root file system, the /"
"usr/ file system, and the kernel API file systems\\&. This is required so "
"that the mount options of these file systems \\(em which are pre-mounted by "
"the kernel, the initrd, container environments or system manager code \\(em "
"are updated to those configured in /etc/fstab and the other sources\\&. This "
"service ignores normal file systems and only changes the root file system "
"(i\\&.e\\&.  /), /usr/, and the virtual kernel API file systems such as /"
"proc/, /sys/ or /dev/\\&. This service executes no operation if no "
"configuration is found (/etc/fstab does not exist or lists no entries for "
"the mentioned file systems, or the partition table does not contain relevant "
"entries)\\&."
msgstr ""
"Systemd-remount-fs\\&.service ist ein Dienst, der in der frühen Phase des "
"Systemstarts die in B<fstab>(5) aufgeführten oder von der Partitionstabelle "
"ermittelten (wenn B<systemd-gpt-auto-generator>(8) aktiv ist) "
"Einhängeoptionen auf das Wurzeldateisystem, das /usr/-Dateisystem und die "
"Dateisysteme der Kernel-API anwendet\\&. Dies ist erforderlich, damit die "
"Einhängeoptionen dieser Dateisysteme – die vom Kernel, der Initrd, den "
"Containerumgebungen oder dem Code des Systemverwalters vorab eingehängt "
"werden – auf die in /etc/fstab und anderen Quellen konfigurierten Optionen "
"aktualisiert werden\\&. Der Dienst ignoriert normale Dateisysteme und ändert "
"nur das Wurzeldateisystem (also /), /usr/ und die virtuellen Dateisysteme "
"der Kernel-API, wie /proc/, /sys/ oder /dev/\\&. Er bleibt untätig, wenn "
"keine Konfiguration gefunden wird (die Datei /etc/fstab nicht existiert oder "
"keine Einträge für die genannten Dateisysteme enthält oder die "
"Partitionstabelle keine relevanten Einträge enthält)\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"For a longer discussion of kernel API file systems see \\m[blue]B<API File "
"Systems>\\m[]\\&\\s-2\\u[1]\\d\\s+2\\&."
msgstr ""
"Eine ausführlichere Beschreibung der Dateisysteme der Kernel-API finden Sie "
"in \\m[blue]B<API-Dateisysteme>\\m[]\\&\\s-2\\u[1]\\d\\s+2\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"Note: systemd-remount-fs\\&.service is usually pulled in by B<systemd-fstab-"
"generator>(8), hence it is also affected by the kernel command line option "
"I<fstab=>, which may be used to disable the generator\\&. It may also pulled "
"in by B<systemd-gpt-auto-generator>(8), which is affected by I<systemd\\&."
"gpt_auto> and other options\\&."
msgstr ""
"Beachten Sie: systemd-remount-fs\\&.service wird normalerweise von B<systemd-"
"fstab-generator>(8) hereingezogen und wird daher auch von der "
"Kernelbefehlszeilenoption I<fstab=> beeinflusst, die zur Deaktivierung "
"dieses Generators verwandt werden kann\\&. Es könnte auch durch B<systemd-"
"gpt-auto-generator>(8) hereingezogen werden, der von I<systemd\\&.gpt_auto> "
"und anderen Optionen beeinflusst wird\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"B<systemd>(1), B<fstab>(5), B<mount>(8), B<systemd-fstab-generator>(8), "
"B<systemd-gpt-auto-generator>(8)"
msgstr ""
"B<systemd>(1), B<fstab>(5), B<mount>(8), B<systemd-fstab-generator>(8), "
"B<systemd-gpt-auto-generator>(8)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid " 1."
msgstr " 1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "API File Systems"
msgstr "API-Dateisysteme"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
msgid "\\%https://www.freedesktop.org/wiki/Software/systemd/APIFileSystems"
msgstr "\\%https://www.freedesktop.org/wiki/Software/systemd/APIFileSystems"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 252"
msgstr "systemd 252"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "/lib/systemd/systemd-remount-fs"
msgstr "/lib/systemd/systemd-remount-fs"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 253"
msgstr "systemd 253"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "systemd 249"
msgstr "systemd 249"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"systemd-remount-fs\\&.service is an early boot service that applies mount "
"options listed in B<fstab>(5), or gathered from the partition table (when "
"B<systemd-gpt-auto-generator>(8)  is active) to the root file system, the /"
"usr/ file system, and the kernel API file systems\\&. This is required so "
"that the mount options of these file systems \\(em which are pre-mounted by "
"the kernel, the initial RAM disk, container environments or system manager "
"code \\(em are updated to those configured in /etc/fstab and the other "
"sources\\&. This service ignores normal file systems and only changes the "
"root file system (i\\&.e\\&.  /), /usr/, and the virtual kernel API file "
"systems such as /proc/, /sys/ or /dev/\\&. This service executes no "
"operation if no configuration is found (/etc/fstab does not exist or lists "
"no entries for the mentioned file systems, or the partition table does not "
"contain relevant entries)\\&."
msgstr ""
"Systemd-remount-fs\\&.service ist ein Dienst, der in der frühen Phase des "
"Systemstarts die in B<fstab>(5) aufgeführten oder von der Partitionstabelle "
"ermittelten (wenn B<systemd-gpt-auto-generator>(8) aktiv ist) "
"Einhängeoptionen auf das Wurzeldateisystem, das /usr/-Dateisystem und die "
"Dateisysteme der Kernel-API anwendet\\&. Dies ist erforderlich, damit die "
"Einhängeoptionen dieser Dateisysteme – die vom Kernel, der initialen RAM-"
"Platte, den Containerumgebungen oder dem Code des Systemverwalters vorab "
"eingehängt werden – auf die in /etc/fstab und anderen Quellen konfigurierten "
"Optionen aktualisiert werden\\&. Der Dienst ignoriert normale Dateisysteme "
"und ändert nur das Wurzeldateisystem (also /), /usr/ und die virtuellen "
"Dateisysteme der Kernel-API, wie /proc/, /sys/ oder /dev/\\&. Er bleibt "
"untätig, wenn keine Konfiguration gefunden wird (die Datei /etc/fstab nicht "
"existiert oder keine Einträge für die genannten Dateisysteme enthält oder "
"die Partitionstabelle keine relevanten Einträge enthält)\\&."
