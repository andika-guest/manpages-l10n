# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-08-27 17:06+0200\n"
"PO-Revision-Date: 2022-08-08 20:30+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.3\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "LOSETUP"
msgstr "LOSETUP"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-07-20"
msgstr "20. Juli 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "System Administration"
msgstr "System-Administration"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "losetup - set up and control loop devices"
msgstr "losetup - Loop-Geräte einrichten und steuern"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Get info:"
msgstr "Informationen erhalten:"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<losetup> [I<loopdev>]"
msgstr "B<losetup> [I<Loop-Gerät>]"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<losetup> B<-l> [B<-a>]"
msgstr "B<losetup> B<-l> [B<-a>]"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<losetup> B<-j> I<file> [B<-o> I<offset>]"
msgstr "B<losetup> B<-j> I<Datei> [B<-o> I<Versatz>]"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Detach a loop device:"
msgstr "Ein Loop-Gerät abhängen:"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<losetup> B<-d> I<loopdev> ..."
msgstr "B<losetup> B<-d> I<Loop-Gerät> …"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Detach all associated loop devices:"
msgstr "Alle zugehörigen Loop-Geräte abhängen:"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<losetup> B<-D>"
msgstr "B<losetup> B<-D>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Set up a loop device:"
msgstr "Ein Loop-Gerät einrichten:"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<losetup> [B<-o> I<offset>] [B<--sizelimit> I<size>] [B<--sector-size> "
"I<size>] [B<-Pr>] [B<--show>] B<-f>|I<loopdev file>"
msgstr ""
"B<losetup> [B<-o> I<Versatz>] [B<--sizelimit> I<Größe>] [B<--sector-size> "
"I<Größe>] [B<-Pr>] [B<--show>] B<-f>|I<Loop-Gerät Datei>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Resize a loop device:"
msgstr "Größe eines Loop-Geräts ändern:"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<losetup> B<-c> I<loopdev>"
msgstr "B<losetup> B<-c> I<Loop-Gerät>"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<losetup> is used to associate loop devices with regular files or block "
"devices, to detach loop devices, and to query the status of a loop device. "
"If only the I<loopdev> argument is given, the status of the corresponding "
"loop device is shown. If no option is given, all loop devices are shown."
msgstr ""
"B<losetup> wird zum Zuweisen von Loop-Geräten zu regulären Dateien oder "
"blockorientierten Geräten, zum Abhängen von Loop-Geräten und zum Abfragen "
"des Status eines Loop-Gerätes verwendet. Wenn nur das Argument I<Loop-Gerät> "
"angegeben ist, wird der Status des korrespondierenden Loop-Geräts angezeigt. "
"Falls keine Option angegeben ist, werden alle Loop-Geräte angezeigt."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Note that the old output format (i.e., B<losetup -a>) with comma-delimited "
"strings is deprecated in favour of the B<--list> output format."
msgstr ""
"Beachten Sie, dass das alte Ausgabeformat (d.h. B<losetup -a>) mit durch "
"Kommata getrennten Zeichenketten als veraltet markiert wurde und nun das "
"Ausgabeformat mit B<--list> bevorzugt wird."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"It\\(cqs possible to create more independent loop devices for the same "
"backing file. B<This setup may be dangerous, can cause data loss, corruption "
"and overwrites.> Use B<--nooverlap> with B<--find> during setup to avoid "
"this problem."
msgstr ""
"Es ist möglich, mehrere unabhängige Loop-Geräte für die gleiche zugrunde "
"liegende Datei anzulegen. B<Diese Art der Einrichtung kann gefährlich sein, "
"kann Datenverlust, Beschädigungen und Überschreibungen verursachen.> "
"Verwenden Sie während der Einrichtung B<--nooverlap> mit B<--find>, um "
"dieses Problem zu vermeiden."

#. type: Plain text
#: debian-bookworm
msgid ""
"The loop device setup is not an atomic operation when used with B<--find>, "
"and B<losetup> does not protect this operation by any lock. The number of "
"attempts is internally restricted to a maximum of 16. It is recommended to "
"use for example B<flock>(1) to avoid a collision in heavily parallel use "
"cases."
msgstr ""
"Die Einrichtung des Loop-Geräts ist keine atomare Aktion, wenn sie mit B<--"
"find> zusammen verwandt wird, und B<losetup> schützt diese Aktion nicht "
"durch eine Sperre. Die Anzahl der Versuche ist intern auf maximal 16 "
"beschränkt. Es wird empfohlen, beispielsweise B<flock>(1) zu verwenden, um "
"eine Kollision in stark parallelisierten Anwendungsfällen zu verwenden."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The I<size> and I<offset> arguments may be followed by the multiplicative "
"suffixes KiB (=1024), MiB (=1024*1024), and so on for GiB, TiB, PiB, EiB, "
"ZiB and YiB (the \"iB\" is optional, e.g., \"K\" has the same meaning as "
"\"KiB\") or the suffixes KB (=1000), MB (=1000*1000), and so on for GB, TB, "
"PB, EB, ZB and YB."
msgstr ""
"Den Argumenten I<Größe> und I<Versatz> können die multiplikativen Suffixe "
"KiB (=1024), MiB (=1024*1024) und so weiter für GiB, TiB, PiB, EiB, ZiB und "
"YiB folgen (das »iB« ist optional, zum Beispiel ist »K« gleichbedeutend mit "
"»KiB«) oder die Suffixe KB (=1000), MB (=1000*1000) und so weiter für GB, "
"TB, PB, EB, ZB und YB."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Show the status of all loop devices. Note that not all information is "
"accessible for non-root users. See also B<--list>. The old output format (as "
"printed without B<--list>) is deprecated."
msgstr ""
"zeigt den Status aller Loop-Geräte an. Beachten Sie, dass gewöhnliche "
"Benutzer nicht auf alle Informationen zugreifen können. Siehe auch B<--"
"list>. Das frühere Format (wie es ohne B<--list> ausgegeben wird) ist "
"veraltet."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-d>, B<--detach> I<loopdev>..."
msgstr "B<-d>, B<--detach> I<Loop-Gerät> …"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Detach the file or device associated with the specified loop device(s). Note "
"that since Linux v3.7 kernel uses \"lazy device destruction\". The detach "
"operation does not return B<EBUSY> error anymore if device is actively used "
"by system, but it is marked by autoclear flag and destroyed later."
msgstr ""
"hängt die Datei oder das Gerät aus, das dem oder den angegebenen Loop-"
"Gerät(en) zugeordnet ist. Beachten Sie, dass der Kernel seit Linux v3.7 die "
"»lazy device destruction« (lockere Gerätezerstörung) verwendet. Der "
"Abhängevorgang gibt keinen B<EBUSY>-Fehler mehr zurück, falls das Gerät "
"aktiv vom System verwendet wird, aber es wird mit einer »autoclear«-"
"Markierung versehen und später zerstört."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-D>, B<--detach-all>"
msgstr "B<-D>, B<--detach-all>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Detach all associated loop devices."
msgstr "hängt alle zugehörigen Loop-Geräte ab."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-f>, B<--find> [I<file>]"
msgstr "B<-f>, B<--find> [I<Datei>]"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Find the first unused loop device. If a I<file> argument is present, use the "
"found device as loop device. Otherwise, just print its name."
msgstr ""
"sucht nach dem ersten ungenutzten Loop-Gerät. Wenn ein I<Datei>-Argument "
"vorhanden ist, wird das gefundene Gerät als Loop-Gerät verwendet. "
"Anderenfalls wird einfach dessen Name ausgegeben."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--show>"
msgstr "B<--show>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Display the name of the assigned loop device if the B<-f> option and a "
"I<file> argument are present."
msgstr ""
"zeigt den Namen des zugehörigen Loop-Geräts an, wenn die Option B<-f> und "
"das Argument I<Datei> vorhanden sind."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-L>, B<--nooverlap>"
msgstr "B<-L>, B<--nooverlap>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Check for conflicts between loop devices to avoid situation when the same "
"backing file is shared between more loop devices. If the file is already "
"used by another device then re-use the device rather than a new one. The "
"option makes sense only with B<--find>."
msgstr ""
"prüft auf Konflikte zwischen Loop-Geräten, um die Situation zu vermeiden, "
"bei der die gleiche zugrunde liegende Datei von mehreren Loop-Geräten "
"gemeinsam verwendet wird. Wenn die Datei bereits von einem anderen Gerät "
"verwendet wird, dann wird das Gerät erneut verwendet, anstatt ein neues zu "
"einzurichten. Diese Option ist nur zusammen mit B<--find> sinnvoll."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-j>, B<--associated> I<file> [B<-o> I<offset>]"
msgstr "B<-j>, B<--associated> I<Datei> [B<-o> I<Versatz>]"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Show the status of all loop devices associated with the given I<file>."
msgstr ""
"zeigt den Status aller Loop-Geräte an, die der angegebenen I<Datei> "
"zugeordnet sind."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-o>, B<--offset> I<offset>"
msgstr "B<-o>, B<--offset> I<Versatz>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The data start is moved I<offset> bytes into the specified file or device. "
"The I<offset> may be followed by the multiplicative suffixes; see above."
msgstr ""
"verschiebt den Start der Daten um den angegebenen I<Versatz> in Byte in der "
"Datei oder dem angegebenen  Gerät. Dem I<Versatz> dürfen multiplikative "
"Suffixe folgen; siehe oben."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--sizelimit> I<size>"
msgstr "B<--sizelimit> I<Größe>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The data end is set to no more than I<size> bytes after the data start. The "
"I<size> may be followed by the multiplicative suffixes; see above."
msgstr ""
"setzt das Ende der Daten auf nicht mehr als die angegebene I<Größe> in Byte "
"nach dem Anfang der Daten. Dem I<Versatz> dürfen multiplikative Suffixe "
"folgen; siehe oben."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-b>, B<--sector-size> I<size>"
msgstr "B<-b>, B<--sector-size> I<Größe>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Set the logical sector size of the loop device in bytes (since Linux 4.14). "
"The option may be used when creating a new loop device as well as a stand-"
"alone command to modify sector size of the already existing loop device."
msgstr ""
"legt die logische Sektorengröße des Loop-Geräts in Bytes fest (seit Linux "
"4.14). Die Option darf sowohl beim Erstellen neuer Loop-Geräte als auch als "
"einzelner Befehl zum Ändern der Sektorengröße eines bereits existierenden "
"Loop-Geräts verwendet werden."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-c>, B<--set-capacity> I<loopdev>"
msgstr "B<-c>, B<--set-capacity> I<Loop-Gerät>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Force the loop driver to reread the size of the file associated with the "
"specified loop device."
msgstr ""
"zwingt den Loop-Treiber, die Größe der Datei neu einzulesen, der das "
"angegebene Loop-Gerät zugeordnet ist."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-P>, B<--partscan>"
msgstr "B<-P>, B<--partscan>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Force the kernel to scan the partition table on a newly created loop device. "
"Note that the partition table parsing depends on sector sizes. The default "
"is sector size is 512 bytes, otherwise you need to use the option B<--sector-"
"size> together with B<--partscan>."
msgstr ""
"zwingt den Kernel, die Partitionstabelle auf einem neu erstellten Loop-Gerät "
"einzulesen. Beachten Sie, dass das Einlesen der Partitionstabelle von den "
"Sektorengrößen abhängt. Die standardmäßige Sektorengröße ist 512 Byte, "
"anderenfalls müssen Sie die Option B<--sector-size> zusammen mit B<--"
"partscan> verwenden."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-r>, B<--read-only>"
msgstr "B<-r>, B<--read-only>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Set up a read-only loop device."
msgstr "richtet ein schreibgeschütztes Loop-Gerät ein."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--direct-io>[B<=on>|B<off>]"
msgstr "B<--direct-io>[B<=on>|B<off>]"

#. type: Plain text
#: debian-bookworm
msgid ""
"Enable or disable direct I/O for the backing file. The optional argument can "
"be either B<on> or B<off>. If the optional argument is omitted, it defaults "
"to B<on>."
msgstr ""
"aktiviert oder deaktiviert die direkten Ein-/Ausgaben für die zugrunde "
"liegende Datei. Das optionale Argument kann entweder B<on> oder B<off> sein. "
"Wird kein optionales Argument angegeben, ist B<on> die Vorgabe."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Verbose mode."
msgstr "aktiviert den ausführlichen Modus."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-l>, B<--list>"
msgstr "B<-l>, B<--list>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If a loop device or the B<-a> option is specified, print the default columns "
"for either the specified loop device or all loop devices; the default is to "
"print info about all devices. See also B<--output>, B<--noheadings>, B<--"
"raw>, and B<--json>."
msgstr ""
"Falls ein Loop-Gerät oder die Option B<-a> angegeben ist, werden die "
"Standardspalten für entweder das angegebene Loop-Gerät oder alle Loop-Geräte "
"ausgegeben; die Vorgabe ist die Ausgabe von Informationen zu allen Geräten. "
"Siehe auch B<--output>, B<--noheadings>, B<--raw> und B<--json>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-O>, B<--output> I<column>[,I<column>]..."
msgstr "B<-O>, B<--output> I<Spalte>[,I<Spalte>] …"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify the columns that are to be printed for the B<--list> output. Use B<--"
"help> to get a list of all supported columns."
msgstr ""
"gibt an, welche Spalten mit B<--list> ausgegeben werden sollen. Mit B<--"
"help> erhalten Sie eine Liste aller unterstützten Spalten."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--output-all>"
msgstr "B<--output-all>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Output all available columns."
msgstr "gibt alle verfügbaren Spalten aus."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-n>, B<--noheadings>"
msgstr "B<-n>, B<--noheadings>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Don\\(cqt print headings for B<--list> output format."
msgstr "zeigt bei der Ausgabe mit B<--list> keine Überschriften an."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--raw>"
msgstr "B<--raw>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Use the raw B<--list> output format."
msgstr "verwendet das Rohformat für die Ausgabe mit B<--list>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-J>, B<--json>"
msgstr "B<-J>, B<--json>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Use JSON format for B<--list> output."
msgstr "verwendet das JSON-Format für die Ausgabe mit B<--list>."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "ENCRYPTION"
msgstr "VERSCHLÜSSELUNG"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<Cryptoloop is no longer supported in favor of dm-crypt.> For more details "
"see B<cryptsetup>(8)."
msgstr ""
"B<Cryptoloop wird zugunsten von dm-crypt nicht mehr unterstützt.> Für "
"weitere Details siehe B<cryptsetup>(8)."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXIT STATUS"
msgstr "EXIT-STATUS"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<losetup> returns 0 on success, nonzero on failure. When B<losetup> "
"displays the status of a loop device, it returns 1 if the device is not "
"configured and 2 if an error occurred which prevented determining the status "
"of the device."
msgstr ""
"B<losetup> gibt 0 im Erfolgsfall und einen von 0 verschiedenen Wert bei "
"einem Fehlschlag zurück. Wenn B<losetup> den Status eines Loop-Geräts "
"anzeigt, wird 1 zurückgegeben, falls das Gerät nicht eingerichtet ist und 2, "
"falls ein Fehler aufgetreten ist, der die Statusermittlung des Geräts "
"verhindert hat."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Since version 2.37 B<losetup> uses B<LOOP_CONFIGURE> ioctl to setup a new "
"loop device by one ioctl call. The old versions use B<LOOP_SET_FD> and "
"B<LOOP_SET_STATUS64> ioctls to do the same."
msgstr ""
"Seit Version 2.37 verwendet B<losetup> das Ioctl B<LOOP_CONFIGURE>, um ein "
"neues Loop-Gerät mit einem einzigen Ioctl-Aufruf einzurichten. Die alten "
"Versionen verwenden die Ioctls B<LOOP_SET_FD> und B<LOOP_SET_STATUS64>, um "
"das gleiche zu erreichen."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: Plain text
#: debian-bookworm
msgid "B<LOOPDEV_DEBUG>=all"
msgstr "B<LOOPDEV_DEBUG>=all"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "enables debug output."
msgstr "aktiviert die Debug-Ausgabe."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I</dev/loop[0..N]>"
msgstr "I</dev/loop[0..N]>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "loop block devices"
msgstr "Loop-Blockgeräte"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I</dev/loop-control>"
msgstr "I</dev/loop-control>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "loop control device"
msgstr "Loop-Steuergerät"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "BEISPIEL"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The following commands can be used as an example of using the loop device."
msgstr ""
"Die folgenden Befehle können als Beispiel für die Nutzung eines Loop-Geräts "
"verwendet werden."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"# dd if=/dev/zero of=~/file.img bs=1024k count=10\n"
"# losetup --find --show ~/file.img\n"
"/dev/loop0\n"
"# mkfs -t ext2 /dev/loop0\n"
"# mount /dev/loop0 /mnt\n"
"\\&...\n"
"# umount /dev/loop0\n"
"# losetup --detach /dev/loop0\n"
msgstr ""
"# dd if=/dev/zero of=~/file.img bs=1024k count=10\n"
"# losetup --find --show ~/file.img\n"
"/dev/loop0\n"
"# mkfs -t ext2 /dev/loop0\n"
"# mount /dev/loop0 /mnt\n"
"… \n"
"# umount /dev/loop0\n"
"# losetup --detach /dev/loop0\n"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "based on the original version from"
msgstr "basierend auf der Originalversion von"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<losetup> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Der Befehl B<losetup> ist Teil des Pakets util-linux, welches "
"heruntergeladen werden kann von:"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "14. Februar 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<losetup> [B<-o> I<offset>] [B<--sizelimit> I<size>] [B<--sector-size> "
"I<size>] [B<-Pr>] [B<--show>] B<-f> I<loopdev file>"
msgstr ""
"B<losetup> [B<-o> I<Versatz>] [B<--sizelimit> I<Größe>] [B<--sector-size> "
"I<Größe>] [B<-Pr>] [B<--show>] B<-f> I<Loop-Gerät Datei>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The loop device setup is not an atomic operation when used with B<--find>, "
"and B<losetup> does not protect this operation by any lock. The number of "
"attempts is internally restricted to a maximum of 16. It is recommended to "
"use for example flock1 to avoid a collision in heavily parallel use cases."
msgstr ""
"Die Einrichtung des Loop-Geräts ist keine atomare Aktion, wenn sie mit B<--"
"find> zusammen verwandt wird, und B<losetup> schützt diese Aktion nicht "
"durch eine Sperre. Die Anzahl der Versuche ist intern auf maximal 16 "
"beschränkt. Es wird empfohlen, beispielsweise B<flock>(1) zu verwenden, um "
"eine Kollision in stark parallelisierten Anwendungsfällen zu verwenden."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Show the status of all loop devices. Note that not all information is "
"accessible for non-root users. See also B<--list>. The old output format (as "
"printed without B<--list)> is deprecated."
msgstr ""
"zeigt den Status aller Loop-Geräte an. Beachten Sie, dass gewöhnliche "
"Benutzer nicht auf alle Informationen zugreifen können. Siehe auch B<--"
"list>. Das frühere Format (wie es ohne B<--list> ausgegeben wird) ist "
"veraltet."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Set the logical sector size of the loop device in bytes (since Linux 4.14). "
"The option may be used when create a new loop device as well as stand-alone "
"command to modify sector size of the already existing loop device."
msgstr ""
"legt die logische Sektorengröße des Loop-Geräts in Bytes fest (seit Linux "
"4.14). Die Option darf sowohl beim Erstellen neuer Loop-Geräte als auch als "
"einzelner Befehl zum Ändern der Sektorengröße eines bereits existierenden "
"Loop-Geräts verwendet werden."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Enable or disable direct I/O for the backing file. The optional argument can "
"be either B<on> or B<off>. If the argument is omitted, it defaults to B<off>."
msgstr ""
"aktiviert oder deaktiviert die direkten Ein-/Ausgaben für die zugrunde "
"liegende Datei. Das optionale Argument kann entweder B<on> oder B<off> sein. "
"Wird kein Argument angegeben, ist B<off> die Vorgabe."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "zeigt Versionsinformationen an und beendet das Programm."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: Plain text
#: opensuse-leap-15-6
msgid "LOOPDEV_DEBUG=all"
msgstr "LOOPDEV_DEBUG=all"
