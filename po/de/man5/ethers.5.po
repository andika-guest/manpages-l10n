# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.14.0\n"
"POT-Creation-Date: 2023-08-10 18:47+0200\n"
"PO-Revision-Date: 2022-06-07 18:37+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ETHERS"
msgstr "ETHERS"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2008-10-03"
msgstr "3. Oktober 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "net-tools"
msgstr "net-tools"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Linux System Administrator's Manual"
msgstr "Linux-Systemverwaltungshandbuch"

#. #-#-#-#-#  archlinux: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-bookworm: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-unstable: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-39: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. {{{roff}}}\"{{{
#. type: SH
#. #-#-#-#-#  fedora-rawhide: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. {{{roff}}}\"{{{
#. type: SH
#. #-#-#-#-#  mageia-cauldron: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. #-#-#-#-#  archlinux: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-39: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. {{{
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. {{{
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "ethers - Ethernet address to IP number database"
msgstr "ethers - Datenbank für Ethernet-Adressen auf IP-Nummern"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. #-#-#-#-#  archlinux: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-39: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. }}}
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. }}}
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B</etc/ethers> contains 48 bit Ethernet addresses and their corresponding IP "
"numbers, one line for each IP number:"
msgstr ""
"B</etc/ethers> enthält die 48-bit-Ethernet-Adressen und ihre entsprechenden "
"IP-Nummern, mit einer Zeile für jede IP-Nummer:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<Ethernet-address> I<IP-number>"
msgstr "I<Ethernet-Adresse> I<IP-Nummer>"

#. #-#-#-#-#  archlinux: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-39: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. {{{
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. {{{
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The two items are separated by any number of SPACE and/or TAB characters.  A "
"B<#> at the beginning of a line starts a comment which extends to the end of "
"the line.  The I<Ethernet-address> is written as I<x>:I<x>:I<x>:I<x>:I<x>:"
"I<x>, where I<x> is a hexadecimal number between B<0> and B<ff> which "
"represents one byte of the address, which is in network byte order (big-"
"endian).  The I<IP-number> may be a hostname which can be resolved by DNS or "
"a dot separated number."
msgstr ""
"Die beiden Einträge sind durch eine beliebige Anzahl von Leerzeichen und/"
"oder Tabulatoren getrennt. Ein B<#> am Anfang der Zeile beginnt einen "
"Kommentar, der bis zum Zeilenende geht. Die I<Ethernet-Adresse> wird als "
"I<x>:I<x>:I<x>:I<x>:I<x>:I<x> geschrieben, wobei I<x> eine hexadezimale Zahl "
"zwischen B<0> und B<ff> ist, die ein Byte der Adresse darstellt und in "
"Netzwerk-Byte-Reihenfolge vorliegt (big-endian). Die I<IP-Nummer> kann ein "
"Rechnername sein, der mittels DNS aufgelöst werden kann, oder eine durch "
"Punkte getrennte Zahl."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. #-#-#-#-#  archlinux: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-39: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. }}}
#. {{{
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. }}}
#. {{{
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "08:00:20:00:61:CA pal"
msgstr "08:00:20:00:61:CA pal"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. #-#-#-#-#  archlinux: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-39: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. }}}
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. }}}
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: ethers.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "/etc/ethers"
msgstr "/etc/ethers"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "B<arp>(8), B<rarp>(8)"
msgstr "B<arp>(8), B<rarp>(8)"
