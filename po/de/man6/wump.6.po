# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-08-27 17:33+0200\n"
"PO-Revision-Date: 2023-06-01 08:28+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Dd
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "May 31, 1993"
msgstr "31. Mai 1993"

#. type: Dt
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "WUMP 6"
msgstr "WUMP 6"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "E<.Nm wump>"
msgstr "E<.Nm wump>"

#. type: Nd
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "hunt the wumpus in an underground cave"
msgstr "Jagen Sie den Wumpus in einer unterirdischen Höhle"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux
msgid ""
"E<.Nm> E<.Op Fl a Ar arrows> E<.Op Fl b Ar bats> E<.Op Fl p Ar pits> E<.Op "
"Fl r Ar rooms> E<.Op Fl t Ar tunnels>"
msgstr ""
"E<.Nm> E<.Op Fl a Ar Pfeile> E<.Op Fl b Ar Fledermäuse> E<.Op Fl p Ar "
"Gruben> E<.Op Fl r Ar Räume> E<.Op Fl t Ar Tunnel>"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The game E<.Nm> is based on a fantasy game first presented in the pages of "
"E<.Em People's Computer Company> in 1973.  In Hunt the Wumpus you are placed "
"in a cave built of many different rooms, all interconnected by tunnels.  "
"Your quest is to find and shoot the evil Wumpus that resides elsewhere in "
"the cave without running into any pits or using up your limited supply of "
"arrows."
msgstr ""
"Das Spiel E<.Nm> basiert auf einem Fantasy-Spiel, das erstmalig 1973 in den "
"Seiten von E<.Em People's Computer Company> vorgestellt wurde. In »Hunt the "
"Wumpus« (Jage den Wumpus) werden Sie in eine Höhle versetzt, die aus vielen "
"verschiedenen Räumen besteht, die mit Tunneln verbunden sind. Ihre "
"Herausforderung besteht darin, den bösen Wumpus, der irgendwo in der Höhle "
"wohnt, zu finden und zu erschießen, ohne dabei in Gruben zu fallen oder "
"Ihren begrenzten Vorrat an Pfeilen aufzubrauchen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The options are as follows:"
msgstr "Folgende Optionen stehen zur Verfügung:"

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl a"
msgstr "Fl a"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Specifies the number of magic arrows the adventurer gets.  The default is "
"five."
msgstr ""
"Gibt die Anzahl der magischen Pfeile an, die der Abenteurer erhält. "
"Standardmäßig fünf."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl b"
msgstr "Fl b"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Specifies the number of rooms in the cave which contain bats.  The default "
"is three."
msgstr ""
"Gibt die Anzahl der Räume in der Höhle an, die Fledermäuse enthalten. "
"Standarmäßig drei."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl p"
msgstr "Fl p"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Specifies the number of rooms in the cave which contain bottomless pits.  "
"The default is three."
msgstr ""
"Gibt die Anzahl der Räume in der Höhle an, die Gruben ohne Boden enthalten. "
"Standardmäßig drei."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl r"
msgstr "Fl r"

#. type: Plain text
#: archlinux
msgid ""
"Specifies the number of rooms in the cave.  The default cave size is twenty "
"rooms."
msgstr ""
"Gibt die Anzahl an Räumen in der Höhle an. Die Standardhöhlengröße ist "
"zwanzig Räume."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl t"
msgstr "Fl t"

#. type: Plain text
#: archlinux
msgid ""
"Specifies the number of tunnels connecting each room in the cave to another "
"room. Beware, too many tunnels in a small cave can easily cause it to "
"collapse! The default cave room has three tunnels to other rooms."
msgstr ""
"Gibt die Anzahl der Tunnel an, die jeden Raum in der Höhle mit einem anderen "
"Raum verbinden. Beachten Sie, dass zu viele Tunnel in einer kleinen Höhle "
"sie zum Zusammensturz führen können. Die Standardhöhle hat drei Tunnel zu "
"anderen Räumen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"While wandering through the cave you'll notice that, while there are tunnels "
"everywhere, there are some mysterious quirks to the cave topology, including "
"some tunnels that go from one room to another, but not necessarily back! "
"Also, most pesky of all are the rooms that are home to large numbers of "
"bats, which, upon being disturbed, will en masse grab you and move you to "
"another portion of the cave (including those housing bottomless pits, sure "
"death for unwary explorers)."
msgstr ""
"Beim Wandern durch die Höhle werden Sie bemerken, dass es zwar überall "
"Tunnel gibt, die Topologie der Höhle aber Eigenarten hat. Dazu gehören "
"Tunnel, die von einem Raum in einen anderen führen, aber nicht "
"notwendigerweise auch zurück! Am ärgerlichsten sind die Räume voller "
"Fledermäuse. Wenn diese gestört werden, werden sie Sie massenweise packen "
"und in einen anderen Teil der Höhle befördern (einschließlich der Teile, in "
"der die Gruben ohne Boden sind, was zum sicheren Tod von unvorbereiteten "
"Entdeckern führt)."

# FIXME might be sleeping → stay
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Fortunately, you're not going into the cave without any weapons or tools, "
"and in fact your biggest aids are your senses; you can often smell the "
"rather odiferous Wumpus up to E<.Em two> rooms away, and you can always feel "
"the drafts created by the occasional bottomless pit and hear the rustle of "
"the bats in caves they might be sleeping within."
msgstr ""
"Glücklicherweise gehen Sie in die Höhle nicht ohne Waffen oder Werkzeuge. "
"Tatsächlich sind Ihre Sinne Ihre größten Helfer. Sie können den duftenden "
"Wumpus oft schon bis zu E<.Em zwei> Räume im voraus riechen und Sie können "
"immer den Luftzug, der durch die bodenlosen Gruben erzeugt wird, fühlen und "
"das Rascheln der Fledermäuse in den Höhlen, in denen diese schlafen könnten, "
"hören."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To kill the wumpus, you'll need to shoot it with one of your magic arrows.  "
"Fortunately, you don't have to be in the same room as the creature, and can "
"instead shoot the arrow from as far as three or four rooms away!"
msgstr ""
"Um den Wumpus zu töten, müssen Sie ihn mit einem Ihrer magischen Pfeile "
"erschießen. Glücklicherweise müssen Sie nicht im gleichen Raum wie die "
"Kreatur sein. Sie können stattdessen den Pfeil aus der Entfernung von bis zu "
"drei oder vier Räumen schießen!"

#. type: Plain text
#: archlinux
msgid ""
"When you shoot an arrow, you do so by typing in a list of rooms that you'd "
"like it to travel to. If at any point in its travels it cannot find a tunnel "
"to the room you specify from the room it's in, it will instead randomly fly "
"down one of the tunnels, possibly, if you're real unlucky, even flying back "
"into the room you're in and hitting you!"
msgstr ""
"Wenn Sie einen Pfeil schießen, geben Sie eine Liste von Räumen ein, in die "
"der Pfeil fliegen soll. Falls er an einem Punkt seiner Reise aus dem Raum, "
"in dem er sich befindet, keinen Tunnel zu dem angegebenen Raum finden kann, "
"wird er stattdessen zufällig einen der Tunnel durchfliegen. Möglicherweise, "
"wenn Sie richtiges Pech haben, fliegt er zurück in den Raum, in dem Sie sich "
"befinden, und trifft Sie selbst."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"E<.Nm> E<.Op Fl h> E<.Op Fl a Ar arrows> E<.Op Fl b Ar bats> E<.Op Fl p Ar "
"pits> E<.Op Fl r Ar rooms> E<.Op Fl t Ar tunnels>"
msgstr ""
"E<.Nm> E<.Op Fl h> E<.Op Fl a Ar Pfeile> E<.Op Fl b Ar Fledermäuse> E<.Op Fl "
"p Ar Gruben> E<.Op Fl r Ar Räume> E<.Op Fl t Ar Tunnel>"

#. type: It
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl h"
msgstr "Fl h"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Play the hard version -- more pits, more bats, and a generally more "
"dangerous cave."
msgstr ""
"Die schwierige Version spielen -- mehr Gruben, mehr Fledermäuse und eine im "
"allgemeinen schwierigere Höhle."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Specifies the number of rooms in the cave.  The default cave size is twenty-"
"five rooms."
msgstr ""
"Gibt die Anzahl der Räume in der Höhle an. Die Standardhöhlengröße ist "
"fünfundzwanzig Räume."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Specifies the number of tunnels connecting each room in the cave to another "
"room.  Beware, too many tunnels in a small cave can easily cause it to "
"collapse! The default cave room has three tunnels to other rooms."
msgstr ""
"Gibt die Anzahl der Tunnel an, die jeden Raum in der Höhle mit einem anderen "
"Raum verbinden. Beachten Sie, dass zu viele Tunnel in einer kleinen Höhle "
"sie zum Zusammensturz führen können. Die Standardhöhle hat drei Tunnel zu "
"anderen Räumen."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When you shoot an arrow, you do so by typing in a list of rooms that you'd "
"like it to travel to.  If at any point in its travels it cannot find a "
"tunnel to the room you specify from the room it's in, it will instead "
"randomly fly down one of the tunnels, possibly, if you're real unlucky, even "
"flying back into the room you're in and hitting you!"
msgstr ""
"Wenn Sie einen Pfeil schießen, geben Sie eine Liste von Räumen ein, in die "
"der Pfeil fliegen soll. Falls er an einem Punkt seiner Reise aus dem Raum, "
"in dem er sich befindet, keinen Tunnel zu dem angegebenen Raum finden kann, "
"wird er stattdessen zufällig einen der Tunnel durchfliegen. Möglicherweise, "
"wenn Sie richtiges Pech haben, fliegt er zurück in den Raum, in dem Sie sich "
"befinden, und trifft Sie selbst."
