# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-uk\n"
"POT-Creation-Date: 2023-08-27 17:01+0200\n"
"PO-Revision-Date: 2023-09-20 18:53+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "hosts"
msgstr "hosts"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-05-03"
msgstr "3 травня 2023 року"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "hosts - static table lookup for hostnames"
msgstr "hosts — статична таблиця пошуку для назв вузлів"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B</etc/hosts>\n"
msgstr "B</etc/hosts>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manual page describes the format of the I</etc/hosts> file.  This file "
"is a simple text file that associates IP addresses with hostnames, one line "
"per IP address.  For each host a single line should be present with the "
"following information:"
msgstr ""
"На цій сторінці підручника подано опису формату файла I</etc/hosts>. Цей "
"файл є простим текстовим файлом, які пов'язує IP-адреси із назвами вузлів, "
"по одному рядку на IP-адресу. Для кожного вузла має бути один рядок із "
"такими даними:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "IP_address canonical_hostname [aliases...]"
msgstr "IP_адреса канонічна_назва_вузла [альтернативи...]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The IP address can conform to either IPv4 or IPv6.  Fields of the entry are "
"separated by any number of blanks and/or tab characters.  Text from a \"#\" "
"character until the end of the line is a comment, and is ignored.  Host "
"names may contain only alphanumeric characters, minus signs (\"-\"), and "
"periods (\".\").  They must begin with an alphabetic character and end with "
"an alphanumeric character.  Optional aliases provide for name changes, "
"alternate spellings, shorter hostnames, or generic hostnames (for example, "
"I<localhost>).  If required, a host may have two separate entries in this "
"file; one for each version of the Internet Protocol (IPv4 and IPv6)."
msgstr ""
"IP-адреса може бути адресою IPv4 або IPv6. Поля запису можна відокремлювати "
"довільною кількістю пробілів і/або символів табуляції. Текст від символу «#» "
"і до кінця рядка вважається коментарем — його буде проігноровано. Назви "
"вузлів можуть містити лише літери латинки і цифри, символи «мінус» («-») і "
"крапки («.»). Вони мають починатися з літери і завершуватися літерою або "
"цифрою. Необов'язкові альтернативи вказують для змінених назв, "
"альтернативних варіантів написання, скорочених назв вузлів або типових назв "
"вузлів (наприклад, I<localhost>). Якщо потрібно, у вузла може бути два "
"окремих записи у цьому файлі; по одному для кожної версії протоколу "
"інтернету (IPv4 і IPv6)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The Berkeley Internet Name Domain (BIND) Server implements the Internet name "
"server for UNIX systems.  It augments or replaces the I</etc/hosts> file or "
"hostname lookup, and frees a host from relying on I</etc/hosts> being up to "
"date and complete."
msgstr ""
"Сервер Berkeley Internet Name Domain (BIND) реалізує сервер назв інтернету "
"для систем UNIX. Він розширює можливості і замінює собою файл I</etc/hosts> "
"або пошук назв вузлів і вивільняє вузол мережі від залежності від "
"актуальності і повноти I</etc/hosts>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In modern systems, even though the host table has been superseded by DNS, it "
"is still widely used for:"
msgstr ""
"У сучасних системах, хоча таблицю вузлів замінено на DNS, нею усе ще "
"користуються для виконання таких завдань:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<bootstrapping>"
msgstr "B<самоналаштовування>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Most systems have a small host table containing the name and address "
"information for important hosts on the local network.  This is useful when "
"DNS is not running, for example during system bootup."
msgstr ""
"У більшості систем є мала таблиця вузлів, що містить дані щодо назв і адрес "
"для важливих вузлів у локальній мережі. Це корисно, якщо DNS не запущено, "
"наприклад, під час завантаження системи."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<NIS>"
msgstr "B<NIS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Sites that use NIS use the host table as input to the NIS host database.  "
"Even though NIS can be used with DNS, most NIS sites still use the host "
"table with an entry for all local hosts as a backup."
msgstr ""
"Сайти, які використовують NIS, використовують таблицю вузлів як вхідні дані "
"для бази даних вузлів NIS. Хоча NIS можна використовувати з DNS, на "
"більшості сайтів NIS усе ще використовують таблицю вузлів із записами для "
"усіх локальних вузлів як резервний варіант."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<isolated nodes>"
msgstr "B<ізольовані вузли>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Very small sites that are isolated from the network use the host table "
"instead of DNS.  If the local information rarely changes, and the network is "
"not connected to the Internet, DNS offers little advantage."
msgstr ""
"На дуже маленьких сайтах, які ізольовано від мережі, використовують таблицю "
"вузлів замість DNS. Якщо локальні відомості змінюються нечасто, і мережу не "
"з'єднано із інтернетом, у використанні DNS небагато переваг."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</etc/hosts>"
msgstr "I</etc/hosts>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМІТКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Modifications to this file normally take effect immediately, except in cases "
"where the file is cached by applications."
msgstr ""
"Внесені до цього файла зміни, зазвичай, набувають чинності негайно, окрім "
"випадків, коли файл кешовано програмами."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Historical notes"
msgstr "Нотатки щодо історії"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"RFC\\ 952 gave the original format for the host table, though it has since "
"changed."
msgstr ""
"Початковий формат таблиці вузлів було надано у RFC\\ 952, хоча з того часу "
"він змінився."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Before the advent of DNS, the host table was the only way of resolving "
"hostnames on the fledgling Internet.  Indeed, this file could be created "
"from the official host data base maintained at the Network Information "
"Control Center (NIC), though local changes were often required to bring it "
"up to date regarding unofficial aliases and/or unknown hosts.  The NIC no "
"longer maintains the hosts.txt files, though looking around at the time of "
"writing (circa 2000), there are historical hosts.txt files on the WWW.  I "
"just found three, from 92, 94, and 95."
msgstr ""
"До появи DNS, таблиця вузлів була єдиним способом визначення назв вузлів у "
"початковій версії інтернету. Справді, цей файл могло бути створено на основі "
"офіційної бази даних вузлів, супровід якої здійснював Центр керування "
"мережевою інформацією (NIC), хоча часто були потрібні зміни для підтримання "
"актуальності списку через появу неофіційних альтернативних назв і/або "
"невідомих вузлів. NIC більше не працює над супроводом файлів hosts.txt, "
"хоча, пошукавши навколо, на час написання цього підручника (близько 2000), у "
"WWW можна знайти старі файли hosts.txt. Автору вдалося знайти три таких "
"файли з 92, 94 та 95 років."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИКЛАДИ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ""
"# The following lines are desirable for IPv4 capable hosts\n"
"127.0.0.1       localhost\n"
"\\&\n"
"# 127.0.1.1 is often used for the FQDN of the machine\n"
"127.0.1.1       thishost.example.org   thishost\n"
"192.168.1.10    foo.example.org        foo\n"
"192.168.1.13    bar.example.org        bar\n"
"146.82.138.7    master.debian.org      master\n"
"209.237.226.90  www.opensource.org\n"
"\\&\n"
"# The following lines are desirable for IPv6 capable hosts\n"
"::1             localhost ip6-localhost ip6-loopback\n"
"ff02::1         ip6-allnodes\n"
"ff02::2         ip6-allrouters\n"
msgstr ""
"# Наступні рядки є бажаними для вузлів із можливостями IPv4\n"
"127.0.0.1       localhost\n"
"\\&\n"
"# 127.0.1.1 is often used for the FQDN of the machine\n"
"# 127.0.1.1 часто використовують як повноцінну доменну адресу власного комп'ютера\n"
"127.0.1.1       thishost.example.org   thishost\n"
"192.168.1.10    foo.example.org        foo\n"
"192.168.1.13    bar.example.org        bar\n"
"146.82.138.7    master.debian.org      master\n"
"209.237.226.90  www.opensource.org\n"
"\\&\n"
"# Наступні рядки є бажаними для вузлів із можливостями IPv6\n"
"::1             localhost ip6-localhost ip6-loopback\n"
"ff02::1         ip6-allnodes\n"
"ff02::2         ip6-allrouters\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<hostname>(1), B<resolver>(3), B<host.conf>(5), B<resolv.conf>(5), "
"B<resolver>(5), B<hostname>(7), B<named>(8)"
msgstr ""
"B<hostname>(1), B<resolver>(3), B<host.conf>(5), B<resolv.conf>(5), "
"B<resolver>(5), B<hostname>(7), B<named>(8)"

#. #-#-#-#-#  archlinux: hosts.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: hosts.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: hosts.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-39: hosts.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: hosts.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: hosts.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: hosts.5.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH AUTHOR
#.  This manual page was written by Manoj Srivastava <srivasta@debian.org>,
#.  for the Debian GNU/Linux system.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: hosts.5.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Internet RFC\\ 952"
msgstr "Internet RFC\\ 952"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 жовтня 2022 року"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"# The following lines are desirable for IPv4 capable hosts\n"
"127.0.0.1       localhost\n"
msgstr ""
"# Наступні рядки є бажаними для вузлів із можливостями IPv4\n"
"127.0.0.1       localhost\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"# 127.0.1.1 is often used for the FQDN of the machine\n"
"127.0.1.1       thishost.example.org   thishost\n"
"192.168.1.10    foo.example.org        foo\n"
"192.168.1.13    bar.example.org        bar\n"
"146.82.138.7    master.debian.org      master\n"
"209.237.226.90  www.opensource.org\n"
msgstr ""
"# 127.0.1.1 часто використовують як повноцінну доменну адресу власного комп'ютера\n"
"127.0.1.1       thishost.example.org   thishost\n"
"192.168.1.10    foo.example.org        foo\n"
"192.168.1.13    bar.example.org        bar\n"
"146.82.138.7    master.debian.org      master\n"
"209.237.226.90  www.opensource.org\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"# The following lines are desirable for IPv6 capable hosts\n"
"::1             localhost ip6-localhost ip6-loopback\n"
"ff02::1         ip6-allnodes\n"
"ff02::2         ip6-allrouters\n"
msgstr ""
"# Наступні рядки є бажаними для вузлів мережі із можливостями IPv6\n"
"::1             localhost ip6-localhost ip6-loopback\n"
"ff02::1         ip6-allnodes\n"
"ff02::2         ip6-allrouters\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "HOSTS"
msgstr "HOSTS"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 вересня 2017 року"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Підручник програміста Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B</etc/hosts>"
msgstr "B</etc/hosts>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Fields of the entry are separated by any number of blanks and/or tab "
"characters.  Text from a \"#\" character until the end of the line is a "
"comment, and is ignored.  Host names may contain only alphanumeric "
"characters, minus signs (\"-\"), and periods (\".\").  They must begin with "
"an alphabetic character and end with an alphanumeric character.  Optional "
"aliases provide for name changes, alternate spellings, shorter hostnames, or "
"generic hostnames (for example, I<localhost>)."
msgstr ""
"Поля запису можна відокремлювати довільною кількістю пробілів і/або символів "
"табуляції. Текст від символу «#» і до кінця рядка вважається коментарем — "
"його буде проігноровано. Назви вузлів можуть містити лише літери латинки і "
"цифри, символи «мінус» («-») і крапки («.»). Вони мають починатися з літери "
"і завершуватися літерою або цифрою. Необов'язкові альтернативи вказують для "
"змінених назв, альтернативних варіантів написання, скорочених назв вузлів "
"або типових назв вузлів (наприклад, I<localhost>)."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "ПРИКЛАДИ"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"# 127.0.1.1 is often used for the FQDN of the machine\n"
"127.0.1.1       thishost.mydomain.org  thishost\n"
"192.168.1.10    foo.mydomain.org       foo\n"
"192.168.1.13    bar.mydomain.org       bar\n"
"146.82.138.7    master.debian.org      master\n"
"209.237.226.90  www.opensource.org\n"
msgstr ""
"# 127.0.1.1 часто використовують як повноцінну доменну адресу власного комп'ютера\n"
"127.0.1.1       thishost.mydomain.org  thishost\n"
"192.168.1.10    foo.mydomain.org       foo\n"
"192.168.1.13    bar.mydomain.org       bar\n"
"146.82.138.7    master.debian.org      master\n"
"209.237.226.90  www.opensource.org\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ПІСЛЯМОВА"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Ця сторінка є частиною випуску 4.16 проєкту I<man-pages> Linux. Опис "
"проєкту, дані щодо звітування про вади та найсвіжішу версію цієї сторінки "
"можна знайти за адресою \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
