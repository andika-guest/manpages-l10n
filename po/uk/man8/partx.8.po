# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:12+0200\n"
"PO-Revision-Date: 2022-06-21 13:43+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "PARTX"
msgstr "PARTX"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 травня 2022 року"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "System Administration"
msgstr "Керування системою"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"partx - tell the kernel about the presence and numbering of on-disk "
"partitions"
msgstr "partx — повідомлення ядру щодо наявності і нумерації розділів на диску"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<partx> [B<-a>|B<-d>|B<-P>|B<-r>|B<-s>|B<-u>] [B<-t> I<type>] [B<-n> I<M>:"
"_N_] [-] I<disk>"
msgstr ""
"B<partx> [B<-a>|B<-d>|B<-P>|B<-r>|B<-s>|B<-u>] [B<-t> I<тип>] [B<-n> I<M>:"
"I<N>] [-] I<диск>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<partx> [B<-a>|B<-d>|B<-P>|B<-r>|B<-s>|B<-u>] [B<-t> I<type>] I<partition> "
"[I<disk>]"
msgstr ""
"B<partx> [B<-a>|B<-d>|B<-P>|B<-r>|B<-s>|B<-u>] [B<-t> I<тип>] I<розділ> "
"[I<диск>]"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Given a device or disk-image, B<partx> tries to parse the partition table "
"and list its contents. It can also tell the kernel to add or remove "
"partitions from its bookkeeping."
msgstr ""
"За заданим пристроєм або образом диска B<partx> намагається обробити таблицю "
"розділів і вивести список її вмісту. Програма також може наказати ядру "
"додати або вилучити розділи його облікового списку."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The I<disk> argument is optional when a I<partition> argument is provided. "
"To force scanning a partition as if it were a whole disk (for example to "
"list nested subpartitions), use the argument \"-\" (hyphen-minus). For "
"example:"
msgstr ""
"Аргумент I<диск> є необов'язковим, якщо вказано аргумент I<розділ>. Щоб "
"програма примусово сканувала розділ так, наче він є цілим диском (наприклад, "
"щоб отримати список вкладених підлеглих розділів), скористайтеся аргументом "
"«-» (дефіс або мінус). Приклад:"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "partx --show - /dev/sda3"
msgstr "partx --show - /dev/sda3"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "This will see sda3 as a whole-disk rather than as a partition."
msgstr "Оброблятиме sda3 як цілий диск, а не як розділ."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<partx is not an fdisk program> - adding and removing partitions does not "
"change the disk, it just tells the kernel about the presence and numbering "
"of on-disk partitions."
msgstr ""
"B<partx не є програмою fdisk> — додавання і вилучення розділів не призводить "
"до змін на диску, а лише повідомляє ядру про наявність і нумерацію розділів "
"на диску."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-a>, B<--add>"
msgstr "B<-a>, B<--add>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Add the specified partitions, or read the disk and add all partitions."
msgstr "Додати вказані розділи або прочитати диск і додати усі розділи."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-b>, B<--bytes>"
msgstr "B<-b>, B<--bytes>"

#. type: Plain text
#: debian-bookworm
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr "Вивести розмір у байтах, а не у зручному для читання форматі."

#. type: Plain text
#: debian-bookworm
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""
"Типово, одиницею, у якій показано розміри, є байт, а префікси одиниць є "
"степенями 2^10 (1024). Для забезпечення зручності читання відбувається "
"скорочення позначень до першої літери запису; приклади: «1 КіБ» та «1 МіБ» "
"буде показано як «1 K» та «1 M», із вилученням «іБ», яке є частиною цих "
"скорочень."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-d>, B<--delete>"
msgstr "B<-d>, B<--delete>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Delete the specified partitions or all partitions. It is not error to remove "
"non-existing partitions, so this option is possible to use together with "
"large B<--nr> ranges without care about the current partitions set on the "
"device."
msgstr ""
"Вилучити вказані розділи або усі розділи. Вилучення розділів, яких не існує, "
"не вважається помилкою, тому цим параметром можна користуватися у поєднанні "
"із великими діапазонами B<--nr>, не переймаючись поточним набором розділів "
"на пристрої."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-g>, B<--noheadings>"
msgstr "B<-g>, B<--noheadings>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Do not print a header line with B<--show> or B<--raw>."
msgstr "Не виводити рядок заголовка при використанні B<--show> або B<--raw>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-l>, B<--list>"
msgstr "B<-l>, B<--list>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"List the partitions. Note that all numbers are in 512-byte sectors. This "
"output format is DEPRECATED in favour of B<--show>. Do not use it in newly "
"written scripts."
msgstr ""
"Вивести список розділів. Зауважте, що усі числа буде вказано у 512-байтових "
"секторах. Цей формат виведення є ЗАСТАРІЛИМ. Користуйтеся замість нього B<--"
"show>. Не використовуйте його у новонаписаних скриптах."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-n>, B<--nr> I<M>B<:>I<N>"
msgstr "B<-n>, B<--nr> I<M>B<:>I<N>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify the range of partitions. For backward compatibility also the format "
"I<M>B<->I<N> is supported. The range may contain negative numbers, for "
"example B<--nr -1:-1> means the last partition, and B<--nr -2:-1> means the "
"last two partitions. Supported range specifications are:"
msgstr ""
"Вказати діапазон розділів. З метою забезпечення зворотної сумісності "
"передбачено також підтримку формату I<M>B<->I<N>. Діапазон може містити "
"від'ємні числа, наприклад, B<--nr -1:-1> означає «останній розділ», а B<--nr "
"-2:-1> означає «два останніх розділи». Підтримувані специфікації діапазонів:"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<M>"
msgstr "I<M>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Specifies just one partition (e.g. B<--nr 3>)."
msgstr "Визначає точно один розділ (приклад: B<--nr 3>)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<M>B<:>"
msgstr "I<M>B<:>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Specifies the lower limit only (e.g. B<--nr 2:>)."
msgstr "Визначає лише нижню межу (приклад: B<--nr 2:>)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<:>I<N>"
msgstr "B<:>I<N>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Specifies the upper limit only (e.g. B<--nr :4>)."
msgstr "Визначає лише верхню межу (приклад: B<--nr :4>)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<M>B<:>I<N>"
msgstr "I<M>B<:>I<N>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Specifies the lower and upper limits (e.g. B<--nr 2:4>)."
msgstr "Визначає нижню і верхню межі (приклад: B<--nr 2:4>)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-o>, B<--output> I<list>"
msgstr "B<-o>, B<--output> I<список>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Define the output columns to use for B<--show>, B<--pairs> and B<--raw> "
"output. If no output arrangement is specified, then a default set is used. "
"Use B<--help> to get I<list> of all supported columns. This option cannot be "
"combined with the B<--add>, B<--delete>, B<--update> or B<--list> options."
msgstr ""
"Визначити стовпчики виведення для використання з B<--show>, B<--pairs> і B<--"
"raw>. Якщо не визначено компонування виведених даних, буде використано "
"типовий набір. Скористайтеся параметром B<--help>, щоб отримати I<список> "
"усіх підтримуваних стовпчиків. Цей параметр не можна поєднувати з "
"параметрами B<--add>, B<--delete>, B<--update> та B<--list>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--output-all>"
msgstr "B<--output-all>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Output all available columns."
msgstr "Вивести список усіх доступних стовпчиків."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-P>, B<--pairs>"
msgstr "B<-P>, B<--pairs>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "List the partitions using the KEY=\"value\" format."
msgstr "Вивести список розділів з використанням формату КЛЮЧ=\"значення\"."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-r>, B<--raw>"
msgstr "B<-r>, B<--raw>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "List the partitions using the raw output format."
msgstr "Вивести список розділів з використання формату виведення без обробки."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-s>, B<--show>"
msgstr "B<-s>, B<--show>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"List the partitions. The output columns can be selected and rearranged with "
"the B<--output> option. All numbers (except SIZE) are in 512-byte sectors."
msgstr ""
"Вивести список розділів. Стовпчики виведення можна вибрати або "
"переупорядкувати за допомогою параметра B<--output>. Усі числа (окрім SIZE) "
"буде вказано у 512-байтових секторах."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-t>, B<--type> I<type>"
msgstr "B<-t>, B<--type> I<тип>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Specify the partition table type."
msgstr "Вказати тип таблиці розділів."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--list-types>"
msgstr "B<--list-types>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "List supported partition types and exit."
msgstr "Вивести список усіх підтримуваних типів розділів і завершити роботу."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-u>, B<--update>"
msgstr "B<-u>, B<--update>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Update the specified partitions."
msgstr "Оновити вказані розділи."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-S>, B<--sector-size> I<size>"
msgstr "B<-S>, B<--sector-size> I<розмір>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Overwrite default sector size."
msgstr "Перезаписати типовий розмір сектора."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Verbose mode."
msgstr "Режим докладних повідомлень."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "ENVIRONMENT"
msgstr "СЕРЕДОВИЩЕ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "LIBBLKID_DEBUG=all"
msgstr "LIBBLKID_DEBUG=all"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "enables libblkid debug output."
msgstr "вмикає показ діагностичних повідомлень libblkid."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "ПРИКЛАДИ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"partx --show /dev/sdb3, partx --show --nr 3 /dev/sdb, partx --show /dev/"
"sdb3 /dev/sdb"
msgstr ""
"partx --show /dev/sdb3, partx --show --nr 3 /dev/sdb, partx --show /dev/"
"sdb3 /dev/sdb"

#. type: Plain text
#: debian-bookworm
msgid "All three commands list partition 3 of I</dev/sdb>."
msgstr "Усі три команди виводять список розділу 3 I</dev/sdb>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "partx --show - /dev/sdb3"
msgstr "partx --show - /dev/sdb3"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Lists all subpartitions on I</dev/sdb3> (the device is used as whole-disk)."
msgstr ""
"Виводить усі підрозділи на I</dev/sdb3> (пристрій буде використано як цілий "
"диск)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "partx -o START -g --nr 5 /dev/sdb"
msgstr "partx -o START -g --nr 5 /dev/sdb"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Prints the start sector of partition 5 on I</dev/sdb> without header."
msgstr "Виводить початковий сектор розділу 5 на I</dev/sdb> без заголовка."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "partx -o SECTORS,SIZE /dev/sda5 /dev/sda"
msgstr "partx -o SECTORS,SIZE /dev/sda5 /dev/sda"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Lists the length in sectors and human-readable size of partition 5 on I</dev/"
"sda>."
msgstr ""
"Виводить довжину у секторах і зручний для читання розмір розділу 5 на I</dev/"
"sda>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "partx --add --nr 3:5 /dev/sdd"
msgstr "partx --add --nr 3:5 /dev/sdd"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Adds all available partitions from 3 to 5 (inclusive) on I</dev/sdd>."
msgstr "Додає усі доступні розділи від 3 до 5 (включно) на I</dev/sdd>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "partx -d --nr :-1 /dev/sdd"
msgstr "partx -d --nr :-1 /dev/sdd"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Removes the last partition on I</dev/sdd>."
msgstr "Вилучає останній розділ на I</dev/sdd>."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The original version was written by"
msgstr "Початкову версію програми було написано"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<addpart>(8), B<delpart>(8), B<fdisk>(8), B<parted>(8), B<partprobe>(8)"
msgstr ""
"B<addpart>(8), B<delpart>(8), B<fdisk>(8), B<parted>(8), B<partprobe>(8)"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<partx> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<partx> є частиною пакунка util-linux, який можна отримати з"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "14 лютого 2022 року"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Print the SIZE column in bytes rather than in human-readable format."
msgstr ""
"Вивести стовпчик розміру у байтах, а не у зручному для читання форматі."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: Plain text
#: opensuse-leap-15-6
msgid "All three commands list partition 3 of /dev/sdb."
msgstr "Усі три команди виводять список розділу 3 /dev/sdb."
