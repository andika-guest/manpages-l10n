# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:05+0200\n"
"PO-Revision-Date: 2022-06-28 17:58+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "LDATTACH"
msgstr "LDATTACH"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 травня 2022 року"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "System Administration"
msgstr "Керування системою"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "ldattach - attach a line discipline to a serial line"
msgstr "ldattach — прив'язка порядку обслуговування для послідовної лінії"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<ldattach> [B<-1278denoVh>] [B<-i> I<iflag>] [B<-s> I<speed>] I<ldisc "
"device>"
msgstr ""
"B<ldattach> [B<-1278denoVh>] [B<-i> I<прапорець-інтерфейсу>] [B<-s> "
"I<швидкість>] I<порядок-обслуговування пристрій>"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<ldattach> daemon opens the specified I<device> file (which should "
"refer to a serial device) and attaches the line discipline I<ldisc> to it "
"for processing of the sent and/or received data. It then goes into the "
"background keeping the device open so that the line discipline stays loaded."
msgstr ""
"Фонова служба B<ldattach> відкриває вказаний файл I<пристрій> (файл має бути "
"файлом послідовного пристрою) і долучає до нього порядок обслуговування "
"I<порядок-обслуговування> для обробки надісланих і/або отриманих даних. "
"Потім служба переходить у фоновий режим, зберігаючи пристрій відкритим так, "
"щоб порядок обслуговування залишався завантаженим."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The line discipline I<ldisc> may be specified either by name or by number."
msgstr ""
"Порядок обслуговування лінії I<порядок-обслуговування> може бути вказано за "
"назвою або числом."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"In order to detach the line discipline, B<kill>(1) the B<ldattach> process."
msgstr ""
"Для від'єднання порядку обслуговування лінії скористайтеся B<kill>(1) для "
"процесу B<ldattach>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "With no arguments, B<ldattach> prints usage information."
msgstr ""
"Якщо не вказано аргументів, команда B<ldattach> виведе дані щодо "
"користування програмою."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "LINE DISCIPLINES"
msgstr "ПОРЯДОК ОБСЛУГОВУВАННЯ ЛІНІЇ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Depending on the kernel release, the following line disciplines are "
"supported:"
msgstr ""
"Залежно від випуску ядра, передбачено підтримку таких порядків "
"обслуговування:"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<TTY>(B<0>)"
msgstr "B<TTY>(B<0>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The default line discipline, providing transparent operation (raw mode) as "
"well as the habitual terminal line editing capabilities (cooked mode)."
msgstr ""
"Типовий порядок обслуговування, який забезпечуватиме прозору роботу (режим "
"без обробки, а також звичайні можливості редагування рядка термінала "
"(приготований режим)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<SLIP>(B<1>)"
msgstr "B<SLIP>(B<1>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Serial Line IP (SLIP) protocol processor for transmitting TCP/IP packets "
"over serial lines."
msgstr ""
"Процесор протоколу Serial Line IP (SLIP) для передавання пакетів TCP/IP "
"лініями послідовного з'єднання."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<MOUSE>(B<2>)"
msgstr "B<MOUSE>(B<2>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Device driver for RS232 connected pointing devices (serial mice)."
msgstr ""
"Драйвер пристроїв для з'єднаних координатних пристроїв RS232 (послідовні "
"миші)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<PPP>(B<3>)"
msgstr "B<PPP>(B<3>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Point to Point Protocol (PPP) processor for transmitting network packets "
"over serial lines."
msgstr ""
"Процесор Point to Point Protocol (PPP) для передавання пакетів мережі "
"лініями послідовного з'єднання."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<STRIP>(B<4>); B<AX25>(B<5>); B<X25>(B<6>)"
msgstr "B<STRIP>(B<4>); B<AX25>(B<5>); B<X25>(B<6>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Line driver for transmitting X.25 packets over asynchronous serial lines."
msgstr ""
"Лінійний драйвер для передавання пакетів X.25 асинхронними лініями "
"послідовного з'єднання."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<6PACK>(B<7>); B<R3964>(B<9>)"
msgstr "B<6PACK>(B<7>); B<R3964>(B<9>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Driver for Simatic R3964 module."
msgstr "Драйвер для модуля Simatic R3964."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<IRDA>(B<11>)"
msgstr "B<IRDA>(B<11>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Linux IrDa (infrared data transmission) driver - see"
msgstr "Драйвер IrDa (передавання даних інфрачервоним зв'язком) Linux — див."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<HDLC>(B<13>)"
msgstr "B<HDLC>(B<13>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Synchronous HDLC driver."
msgstr "Синхронний драйвер HDLC."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<SYNC_PPP>(B<14>)"
msgstr "B<SYNC_PPP>(B<14>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Synchronous PPP driver."
msgstr "Синхронний драйвер PPP."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<HCI>(B<15>)"
msgstr "B<HCI>(B<15>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Bluetooth HCI UART driver."
msgstr "Драйвер HCI UART Bluetooth."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<GIGASET_M101>(B<16>)"
msgstr "B<GIGASET_M101>(B<16>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Driver for Siemens Gigaset M101 serial DECT adapter."
msgstr "Драйвер для послідовного адаптера DECT Siemens Gigaset M101."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<PPS>(B<18>)"
msgstr "B<PPS>(B<18>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Driver for serial line Pulse Per Second (PPS) source."
msgstr "Драйвер для послідовної лінії джерела Pulse Per Second (PPS)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<GSM0710>(B<21>)"
msgstr "B<GSM0710>(B<21>)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Driver for GSM 07.10 multiplexing protocol modem (CMUX)."
msgstr "Драйвер для модема протоколу мультиплексування GSM 07.10 (CMUX)."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-1>, B<--onestopbit>"
msgstr "B<-1>, B<--onestopbit>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Set the number of stop bits of the serial line to one."
msgstr "Встановити кількість стоп-бітів послідовної лінії у значення одиниці."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-2>, B<--twostopbits>"
msgstr "B<-2>, B<--twostopbits>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Set the number of stop bits of the serial line to two."
msgstr "Встановити кількість стоп-бітів послідовної лінії у значення двійки."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-7>, B<--sevenbits>"
msgstr "B<-7>, B<--sevenbits>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Set the character size of the serial line to 7 bits."
msgstr "Встановити значення розмірності символу послідовної лінії у 7 бітів."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-8>, B<--eightbits>"
msgstr "B<-8>, B<--eightbits>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Set the character size of the serial line to 8 bits."
msgstr "Встановити значення розмірності символу послідовної лінії у 8 бітів."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-d>, B<--debug>"
msgstr "B<-d>, B<--debug>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Keep B<ldattach> in the foreground so that it can be interrupted or "
"debugged, and to print verbose messages about its progress to standard error "
"output."
msgstr ""
"Підтримувати роботу B<ldattach> у режимі переднього плану так, щоб виконання "
"програми не можна було перервати або передати засобу діагностики, і виводити "
"докладні повідомлення щодо поступу роботи до стандартного виведення помилок."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-e>, B<--evenparity>"
msgstr "B<-e>, B<--evenparity>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Set the parity of the serial line to even."
msgstr "Встановити значення парності послідовної лінії «парна»."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-i>, B<--iflag> I<value>..."
msgstr "B<-i>, B<--iflag> I<значення>..."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Set the specified bits in the c_iflag word of the serial line. The given "
"I<value> may be a number or a symbolic name. If I<value> is prefixed by a "
"minus sign, the specified bits are cleared instead. Several comma-separated "
"values may be given in order to set and clear multiple bits."
msgstr ""
"Встановити вказані біти у слові c_iflag лінії послідовного зв'язку. Вказаним "
"I<значенням> може бути число або символічна назва. Якщо перед I<значенням> "
"вказано мінус, вказані біти буде спорожнено. Можна вказати декілька "
"відокремлених комами значень, щоб встановити або зняти декілька бітів "
"одночасно."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-n>, B<--noparity>"
msgstr "B<-n>, B<--noparity>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Set the parity of the serial line to none."
msgstr "Встановити значення парності послідовної лінії «немає»."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-o>, B<--oddparity>"
msgstr "B<-o>, B<--oddparity>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Set the parity of the serial line to odd."
msgstr "Встановити значення парності послідовної лінії «непарна»."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-s>, B<--speed> I<value>"
msgstr "B<-s>, B<--speed> I<значення>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Set the speed (the baud rate) of the serial line to the specified I<value>."
msgstr ""
"Встановити для швидкості (у бодах) послідовної лінії вказане I<значення>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-c>, B<--intro-command> I<string>"
msgstr "B<-c>, B<--intro-command> I<рядок>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Define an intro command that is sent through the serial line before the "
"invocation of B<ldattach>. E.g. in conjunction with line discipline GSM0710, "
"the command \\(aqAT+CMUX=0\\(rsr\\(aq is commonly suitable to switch the "
"modem into the CMUX mode."
msgstr ""
"Визначити вступну команду, яку буде надіслано лінією послідовного зв'язку до "
"виклику B<ldattach>. Наприклад, у поєднанні із порядком обслуговування "
"GSM0710 доречною є команда \\(aqAT+CMUX=0\\(rsr\\(aq для перемикання модема "
"у режим CMUX."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-p>, B<--pause> I<value>"
msgstr "B<-p>, B<--pause> I<значення>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Sleep for I<value> seconds before the invocation of B<ldattach>. Default is "
"one second."
msgstr ""
"Приспати на I<значення> секунд до виклику B<ldattach>. Типовим часом є одна "
"секунда."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<inputattach>(1), B<ttys>(4)"
msgstr "B<inputattach>(1), B<ttys>(4)"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<ldattach> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<ldattach> є частиною пакунка util-linux, який можна отримати з"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "14 лютого 2022 року"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Define an intro command that is sent through the serial line before the "
"invocation of ldattach. E.g. in conjunction with line discipline GSM0710, "
"the command \\(aqAT+CMUX=0\\(rsr\\(aq is commonly suitable to switch the "
"modem into the CMUX mode."
msgstr ""
"Визначити вступну команду, яку буде надіслано лінією послідовного зв'язку до "
"виклику B<ldattach>. Наприклад, у поєднанні із порядком обслуговування "
"GSM0710 доречною є команда \\(aqAT+CMUX=0\\(rsr\\(aq для перемикання модема "
"у режим CMUX."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Sleep for I<value> seconds before the invocation of ldattach. Default is one "
"second."
msgstr ""
"Приспати на I<значення> секунд до виклику B<ldattach>. Типовим часом є одна "
"секунда."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "Вивести дані щодо версії і завершити роботу."
