# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:52+0200\n"
"PO-Revision-Date: 2022-10-13 21:21+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CHATTR"
msgstr "CHATTR"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "February 2023"
msgstr "Лютий 2023 року"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "E2fsprogs version 1.47.0"
msgstr "E2fsprogs версії 1.47.0"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "chattr - change file attributes on a Linux file system"
msgstr "chattr — зміна атрибутів файла у файловій системі Linux"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<chattr> [ B<-RVf> ] [ B<-v> I<version> ] [ B<-p> I<project> ] [ I<mode> ] "
"I<files...>"
msgstr ""
"B<chattr> [ B<-RVf> ] [ B<-v> I<версія> ] [ B<-p> I<проєкт> ] [ I<режим> ] "
"I<файли...>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<chattr> changes the file attributes on a Linux file system."
msgstr "B<chattr> змінює атрибути файлів у файловій системі Linux."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The format of a symbolic I<mode> is B<+-=>[B<aAcCdDeFijmPsStTux>]."
msgstr "Формат символічного режиму є таким: B<+-=>[B<aAcCdDeFijmPsStTux>]."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The operator 'B<+>' causes the selected attributes to be added to the "
"existing attributes of the files; 'B<->' causes them to be removed; and "
"'B<=>' causes them to be the only attributes that the files have."
msgstr ""
"Оператор «B<+>» наказує програмі додати вибрані атрибути до наявних "
"атрибутів файлів; «B<->» наказує вилучити атрибути; а «B<=>» наказує "
"встановити лише вказані атрибути для файла."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The letters 'B<aAcCdDeFijmPsStTux>' select the new attributes for the files: "
"append only (B<a>), no atime updates (B<A>), compressed (B<c>), no copy on "
"write (B<C>), no dump (B<d>), synchronous directory updates (B<D>), extent "
"format (B<e>), case-insensitive directory lookups (B<F>), immutable (B<i>), "
"data journaling (B<j>), don't compress (B<m>), project hierarchy (B<P>), "
"secure deletion (B<s>), synchronous updates (B<S>), no tail-merging (B<t>), "
"top of directory hierarchy (B<T>), undeletable (B<u>), and direct access for "
"files (B<x>)."
msgstr ""
"Літери «B<aAcCdDeFijmPsStTux>» вибирають нові атрибути для файлів: лише "
"дописування (B<a>), без оновлень atime (B<A>), стиснений (B<c>), без "
"копіювання при записі (B<C>), без дампу (B<d>), синхронні оновлення каталогу "
"(B<D>), формат із розширеннями (B<e>), пошук у каталозі без врахування "
"регістру символів (B<F>), незмінний (B<i>), журналювання даних (B<j>), не "
"стискати (B<m>), ієрархія проєкту (B<P>), безпечне вилучення (B<s>), "
"синхронні оновлення (B<S>), без об'єднання за кінцем (B<t>), вершина "
"ієрархії каталогів (B<T>), не можна вилучати (B<u>) і безпосередній доступ "
"до файлів (B<x>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The following attributes are read-only, and may be listed by B<lsattr>(1)  "
"but not modified by chattr: encrypted (B<E>), indexed directory (B<I>), "
"inline data (B<N>), and verity (B<V>)."
msgstr ""
"Наступні атрибути є придатними лише до читання, їх може бути виведено у "
"списку атрибутів B<lsattr>(1), але не може бути змінено за допомогою chattr: "
"зашифрований (B<E>), індексований каталог (B<I>), вбудовані дані (B<N>) та "
"дійсність (B<V>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Not all flags are supported or utilized by all file systems; refer to file "
"system-specific man pages such as B<btrfs>(5), B<ext4>(5), B<mkfs.f2fs>(8), "
"and B<xfs>(5)  for more file system-specific details."
msgstr ""
"Підтримку усіх прапорців та використання в усіх файлових системах "
"передбачено не усюди; ознайомтеся із специфічними для файлової системи "
"сторінками підручника, зокрема сторінками B<btrfs>(5), B<ext4>(5), B<mkfs."
"f2fs>(8), та B<xfs>(5), щоб дізнатися більше про специфічні для файлових "
"систем подробиці."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>"
msgstr "B<-R>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Recursively change attributes of directories and their contents."
msgstr "Рекурсивно змінити атрибути каталогів та їхнього вмісту."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Be verbose with chattr's output and print the program version."
msgstr ""
"Увімкнути докладний режим виведення chattr і вивести дані щодо версії "
"програми."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Suppress most error messages."
msgstr "Придушити більшість повідомлень про помилки."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>I< version>"
msgstr "B<-v>I< версія>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Set the file's version/generation number."
msgstr "Встановити номер версії або генерації файла."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>I< project>"
msgstr "B<-p>I< проєкт>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Set the file's project number."
msgstr "Встановити номер проєкту файла."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<a>"
msgstr "B<a>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A file with the 'a' attribute set can only be opened in append mode for "
"writing.  Only the superuser or a process possessing the CAP_LINUX_IMMUTABLE "
"capability can set or clear this attribute."
msgstr ""
"Файл зі встановленим атрибутом «a» можна відкривати для запису лише у режимі "
"дописування. Встановити або зняти цей атрибут може лише суперкористувач або "
"процес, який має можливість CAP_LINUX_IMMUTABLE."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<A>"
msgstr "B<A>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When a file with the 'A' attribute set is accessed, its atime record is not "
"modified.  This avoids a certain amount of disk I/O for laptop systems."
msgstr ""
"Якщо буде здійснено спробу доступу до файла із встановленим атрибутом «A», "
"його запис atime не буде змінено. Таким чином можна уникнути певного зайвого "
"навантаження на систему введення-виведення на системах для ноутбуків."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<c>"
msgstr "B<c>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A file with the 'c' attribute set is automatically compressed on the disk by "
"the kernel.  A read from this file returns uncompressed data.  A write to "
"this file compresses data before storing them on the disk.  Note: please "
"make sure to read the bugs and limitations section at the end of this "
"document.  (Note: For btrfs, If the 'c' flag is set, then the 'C' flag "
"cannot be set. Also conflicts with btrfs mount option 'nodatasum')"
msgstr ""
"Файл зі встановленим атрибутом «c» буде автоматично стиснуто на диску ядром "
"системи. У відповідь на спробу читання цього файла буде повернуто "
"розпаковані дані. Спроба запису до цього файла призведе до стискання даних "
"до збереження їх на диск. Зауваження: будь ласка, ознайомтеся із розділом "
"щодо вад і обмежень наприкінці цього документа. (Зауваження: у btrfs, якщо "
"встановлено прапорець «c», не може бути встановлено прапорець «C». Також "
"конфліктує із параметром монтування btrfs «nodatasum».)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<C>"
msgstr "B<C>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A file with the 'C' attribute set will not be subject to copy-on-write "
"updates.  This flag is only supported on file systems which perform copy-on-"
"write.  (Note: For btrfs, the 'C' flag should be set on new or empty files.  "
"If it is set on a file which already has data blocks, it is undefined when "
"the blocks assigned to the file will be fully stable.  If the 'C' flag is "
"set on a directory, it will have no effect on the directory, but new files "
"created in that directory will have the No_COW attribute set. If the 'C' "
"flag is set, then the 'c' flag cannot be set.)"
msgstr ""
"Файл із встановленим атрибутом «C» не підлягатиме оновленням копіювання-при-"
"записі. Підтримку цього прапорця передбачено лише у файлових системах, де "
"виконується копіювання-при-записі. (Зауваження: у btrfs прапорець «C» слід "
"встановлювати для нових або порожніх файлів. Якщо цей прапорець встановлено "
"для файла, який вже має блоки даних, прапорець лишиться невизначеним, аж "
"доки файл не стане повністю стабільним. Якщо встановити прапорець «C» для "
"каталогу, він не вплине на сам каталог, але нові файли, які буде створено у "
"цьому каталозі, матимуть встановлений атрибут No_COW. Якщо встановлено "
"прапорець «C», не можна буде встановити прапорець «c».)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<d>"
msgstr "B<d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A file with the 'd' attribute set is not a candidate for backup when the "
"B<dump>(8)  program is run."
msgstr ""
"Файл зі встановленим атрибутом «d» не є кандидатом на резервне копіювання, "
"коли запущено програму B<dump>(8)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<D>"
msgstr "B<D>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When a directory with the 'D' attribute set is modified, the changes are "
"written synchronously to the disk; this is equivalent to the 'dirsync' mount "
"option applied to a subset of the files."
msgstr ""
"Якщо буде внесено зміни до каталогу зі встановленим атрибутом «D», зміни "
"буде синхронно записано на диск; це еквівалент застосування параметра "
"монтування «dirsync» до підмножини файлів."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<e>"
msgstr "B<e>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The 'e' attribute indicates that the file is using extents for mapping the "
"blocks on disk.  It may not be removed using B<chattr>(1)."
msgstr ""
"Атрибут «e» вказує на те, що у файлі використано розширення для прив'язки до "
"блоків на диску. Цей атрибути не може бути вилучено за допомогою "
"B<chattr>(1)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<E>"
msgstr "B<E>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A file, directory, or symlink with the 'E' attribute set is encrypted by the "
"file system.  This attribute may not be set or cleared using B<chattr>(1), "
"although it can be displayed by B<lsattr>(1)."
msgstr ""
"Файл, каталог або символічне посилання із встановленим атрибутом «E» буде "
"зашифровано файловою системою. Цей атрибут не можна встановити або зняти за "
"допомогою B<chattr>(1), хоча його може бути показано B<lsattr>(1)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<F>"
msgstr "B<F>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A directory with the 'F' attribute set indicates that all the path lookups "
"inside that directory are made in a case-insensitive fashion.  This "
"attribute can only be changed in empty directories on file systems with the "
"casefold feature enabled."
msgstr ""
"Встановлення атрибуту «F» для каталогу вказує на те, що усі пошуки шляхів у "
"каталозі будуть виконуватися без врахування регістру символів у назвах. Цей "
"атрибут можна змінювати лише у порожніх каталогах у файлових системах із "
"увімкненою можливістю casefold."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<i>"
msgstr "B<i>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A file with the 'i' attribute cannot be modified: it cannot be deleted or "
"renamed, no link can be created to this file, most of the file's metadata "
"can not be modified, and the file can not be opened in write mode.  Only the "
"superuser or a process possessing the CAP_LINUX_IMMUTABLE capability can set "
"or clear this attribute."
msgstr ""
"До файлів із встановленим атрибутом «i» не можна вносити зміни: їх не можна "
"вилучати або перейменовувати, не можна створювати посилання на такі файли, "
"більшість метаданих таких файлів змінювати не можна, також файл не може бути "
"відкрито у режимі запису. Встановлювати або знімати цей атрибут може лише "
"суперкористувач або процес із можливістю CAP_LINUX_IMMUTABLE."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<I>"
msgstr "B<I>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The 'I' attribute is used by the htree code to indicate that a directory is "
"being indexed using hashed trees.  It may not be set or cleared using "
"B<chattr>(1), although it can be displayed by B<lsattr>(1)."
msgstr ""
"Атрибут «I» використовують у коді htree для того, щоб вказати, що каталог "
"буде індексовано за допомогою хешованих ієрархій. Атрибут не може бути "
"встановлено або знято за допомогою B<chattr>(1), хоча його можна переглянути "
"за допомогою B<lsattr>(1)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<j>"
msgstr "B<j>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A file with the 'j' attribute has all of its data written to the ext3 or "
"ext4 journal before being written to the file itself, if the file system is "
"mounted with the \"data=ordered\" or \"data=writeback\" options and the file "
"system has a journal.  When the file system is mounted with the "
"\"data=journal\" option all file data is already journalled and this "
"attribute has no effect.  Only the superuser or a process possessing the "
"CAP_SYS_RESOURCE capability can set or clear this attribute."
msgstr ""
"Для файлів зі встановленим атрибутом «j» усі дані записують до журналу ext3 "
"або ext4 до записування даних до самого файла, якщо систему змонтовано з "
"використанням параметра «data=ordered» або «data=writeback», і файлова "
"система має журнал. Якщо файлову систему змонтовано з використанням "
"параметра «data=journal», усі дані файлів вже журнальовано, а цей атрибут ні "
"на що не впливає. Встановлювати або знімати цей атрибут може лише "
"суперкористувач або процес із можливістю CAP_SYS_RESOURCE."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<m>"
msgstr "B<m>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A file with the 'm' attribute is excluded from compression on file systems "
"that support per-file compression."
msgstr ""
"Файл із встановленим атрибутом «m» буде виключено зі стискання у файлових "
"системах, де передбачено підтримку стискання для окремих файлів."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<N>"
msgstr "B<N>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A file with the 'N' attribute set indicates that the file has data stored "
"inline, within the inode itself. It may not be set or cleared using "
"B<chattr>(1), although it can be displayed by B<lsattr>(1)."
msgstr ""
"Встановлення атрибуту «N» для файла, вказує на те, що у файлі зберігаються "
"вбудовані дані, тобто дані у самому inode. Цей атрибут не може бути "
"встановлено або знято за допомогою B<chattr>(1), хоча B<lsattr>(1) може "
"показувати цей атрибут."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<P>"
msgstr "B<P>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A directory with the 'P' attribute set will enforce a hierarchical structure "
"for project id's.  This means that files and directories created in the "
"directory will inherit the project id of the directory, rename operations "
"are constrained so when a file or directory is moved into another directory, "
"that the project ids must match.  In addition, a hard link to file can only "
"be created when the project id for the file and the destination directory "
"match."
msgstr ""
"Для каталогу зі встановленим атрибутом «P» буде примусово встановлено "
"ієрархічну структуру для ідентифікаторів проєкту. Це означає, що файли і "
"каталоги, які створено у цьому каталозі, успадкують ідентифікатор проєкту "
"каталогу. Дії з перейменовування буде обмежено так, що при пересуванні файла "
"або каталогу до іншого каталогу, ідентифікатори проєктів мають збігатися. "
"Крім того, жорсткі посилання на файл можна буде створити, лише якщо "
"ідентифікатори проєкту для файла і каталогу призначення є однаковими."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<s>"
msgstr "B<s>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When a file with the 's' attribute set is deleted, its blocks are zeroed and "
"written back to the disk.  Note: please make sure to read the bugs and "
"limitations section at the end of this document."
msgstr ""
"Якщо буде здійснено спробу вилучення файла зі встановленим атрибутом «s», "
"його блоки буде заповнено нулями і знову записано на диск. Зауваження: будь "
"ласка, ознайомтеся із розділом щодо вад і обмежень наприкінці цього "
"документа."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S>"
msgstr "B<S>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When a file with the 'S' attribute set is modified, the changes are written "
"synchronously to the disk; this is equivalent to the 'sync' mount option "
"applied to a subset of the files."
msgstr ""
"Якщо буде внесено зміни до файла зі встановленим атрибутом «S», зміни буде "
"синхронно записано на диск; це еквівалент застосування параметра монтування "
"«sync» до підмножини файлів."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<t>"
msgstr "B<t>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A file with the 't' attribute will not have a partial block fragment at the "
"end of the file merged with other files (for those file systems which "
"support tail-merging).  This is necessary for applications such as LILO "
"which read the file system directly, and which don't understand tail-merged "
"files.  Note: As of this writing, the ext2, ext3, and ext4 file systems do "
"not support tail-merging."
msgstr ""
"Файл із атрибутом «t» не матиме часткового блокового фрагмента наприкінці "
"файла, об'єднаного із іншими файлами (для тих файлових систем, де "
"передбачено підтримку хвостового об'єднання). Це потрібно для програм, "
"подібних до LILO, які виконують читання файлової системи безпосередньо, і "
"які не можуть обробляти файлів із хвостовим об'єднанням. Зауваження: на час "
"написання цієї сторінки підручника у файлових системах ext2, ext3 і ext4 не "
"було реалізовано підтримки хвостового об'єднання."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<T>"
msgstr "B<T>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A directory with the 'T' attribute will be deemed to be the top of directory "
"hierarchies for the purposes of the Orlov block allocator.  This is a hint "
"to the block allocator used by ext3 and ext4 that the subdirectories under "
"this directory are not related, and thus should be spread apart for "
"allocation purposes.  For example it is a very good idea to set the 'T' "
"attribute on the /home directory, so that /home/john and /home/mary are "
"placed into separate block groups.  For directories where this attribute is "
"not set, the Orlov block allocator will try to group subdirectories closer "
"together where possible."
msgstr ""
"Каталог із атрибутом «T» буде вважатися верхнім каталогом ієрархій каталогів "
"для засобу розподілу блоків Орлова. Це вказівка для засобу розподілу блоків, "
"яку використовують у ext3 і ext4 для того, щоб зазначити, що підкаталоги у "
"цьому каталозі не пов'язано між собою, а отже, їх можна розділити при "
"розподілі на диску. Наприклад, варто встановити атрибут «T» для каталогу /"
"home, щоб /home/ivan і /home/mariya було розташовано у окремих групах "
"блоків. Для каталогів, де цей атрибут не встановлено, засіб розподілу блоків "
"Орлова намагатиметься групувати підкаталоги якомога ближче один до одного."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<u>"
msgstr "B<u>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When a file with the 'u' attribute set is deleted, its contents are saved.  "
"This allows the user to ask for its undeletion.  Note: please make sure to "
"read the bugs and limitations section at the end of this document."
msgstr ""
"Якщо буде здійснено спробу вилучення файла зі встановленим атрибутом «u», "
"його буде збережено. Це надасть користувачеві змогу наказати згодом його "
"відновити. Зауваження: будь ласка, ознайомтеся із розділом щодо вад і "
"обмежень наприкінці цього документа."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<x>"
msgstr "B<x>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"A file with the 'x' requests the use of direct access (dax) mode, if the "
"kernel supports DAX.  This can be overridden by the 'dax=never' mount "
"option.  For more information see the kernel documentation for dax: "
"E<lt>https://www.kernel.org/doc/html/latest/filesystems/dax.htmlE<gt>."
msgstr ""
"Файл із «x»' надсилає запит щодо використання режиму безпосереднього доступу "
"(dax), якщо у ядрі передбачено підтримку DAX. Таку поведінку може бути "
"перевизначено за допомогою параметра монтування «dax=never». Щоб дізнатися "
"більше, ознайомтеся із документацією до ядра для dax: E<lt>https://www."
"kernel.org/doc/html/latest/filesystems/dax.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If the attribute is set on an existing directory, it will be inherited by "
"all files and subdirectories that are subsequently created in the "
"directory.  If an existing directory has contained some files and "
"subdirectories, modifying the attribute on the parent directory doesn't "
"change the attributes on these files and subdirectories."
msgstr ""
"Якщо атрибут встановлено для наявного каталогу, його буде успадковано усіма "
"файлами та підкаталогами, які згодом буде створено у цьому каталозі. Якщо у "
"наявному каталозі вже містяться якісь файли і підкаталоги, внесення змін до "
"атрибута батьківського каталогу не змінить атрибутів цих файлів і "
"підкаталогів."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<V>"
msgstr "B<V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A file with the 'V' attribute set has fs-verity enabled.  It cannot be "
"written to, and the file system will automatically verify all data read from "
"it against a cryptographic hash that covers the entire file's contents, e.g. "
"via a Merkle tree.  This makes it possible to efficiently authenticate the "
"file.  This attribute may not be set or cleared using B<chattr>(1), although "
"it can be displayed by B<lsattr>(1)."
msgstr ""
"Для файла із встановленим атрибутом «V» увімкнено fs-verity. Запис до нього "
"буде заборонено, а файлова система автоматично перевірятиме усі дані, які з "
"нього прочитано з використанням криптографічного хешу, який стосується "
"усього вмісту файла, наприклад, з використанням дерева Меркла. Це уможливлює "
"ефективне розпізнавання файла. Цей атрибут не може бути встановлено або "
"знято за допомогою B<chattr>(1), хоча B<lsattr>(1) може його показувати."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "АВТОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<chattr> was written by Remy Card E<lt>Remy.Card@linux.orgE<gt>.  It is "
"currently being maintained by Theodore Ts'o E<lt>tytso@alum.mit.eduE<gt>."
msgstr ""
"Автором B<chattr> є Remy Card E<lt>Remy.Card@linux.orgE<gt>. Поточний "
"супровід програми здійснює Theodore Ts'o E<lt>tytso@alum.mit.eduE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS AND LIMITATIONS"
msgstr "ВАДИ І ОБМЕЖЕННЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The 'c', 's', and 'u' attributes are not honored by the ext2, ext3, and ext4 "
"file systems as implemented in the current mainline Linux kernels.  Setting "
"'a' and 'i' attributes will not affect the ability to write to already "
"existing file descriptors."
msgstr ""
"Атрибути «c», «s» і «u» не буде взято до уваги у файлових системах ext2, "
"ext3 і ext4 у тому вигляді, у якому їх реалізовано у поточних ядрах Linux з "
"основної гілки розробки. Встановлення атрибутів «a» and «i» не вплине на "
"можливість запису до вже наявних дескрипторів файлів."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The 'j' option is only useful for ext3 and ext4 file systems."
msgstr "Параметр «j» має сенс лише для файлових систем ext3 і ext4."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The 'D' option is only useful on Linux kernel 2.5.19 and later."
msgstr "Параметр «D» має сенс лише для ядер Linux 2.5.19 і новіших."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<chattr> is part of the e2fsprogs package and is available from http://"
"e2fsprogs.sourceforge.net."
msgstr ""
"B<chattr> є частиною пакунка e2fsprogs, код якого доступний зі сторінки "
"http://e2fsprogs.sourceforge.net."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<lsattr>(1), B<btrfs>(5), B<ext4>(5), B<mkfs.f2fs>(8), B<xfs>(5)."
msgstr "B<lsattr>(1), B<btrfs>(5), B<ext4>(5), B<mkfs.f2fs>(8), B<xfs>(5)."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "August 2021"
msgstr "Серпень 2021 року"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "E2fsprogs version 1.46.4"
msgstr "E2fsprogs версії 1.46.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid "The format of a symbolic mode is +-=[aAcCdDeFijmPsStTux]."
msgstr "Формат символічного режиму є таким: +-=[aAcCdDeFijmPsStTux]."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The operator '+' causes the selected attributes to be added to the existing "
"attributes of the files; '-' causes them to be removed; and '=' causes them "
"to be the only attributes that the files have."
msgstr ""
"Оператор «+» наказує програмі додати вибрані атрибути до наявних атрибутів "
"файлів; «-» наказує вилучити атрибути; а «=» наказує встановити лише вказані "
"атрибути для файла."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The letters 'aAcCdDeFijmPsStTux' select the new attributes for the files: "
"append only (a), no atime updates (A), compressed (c), no copy on write (C), "
"no dump (d), synchronous directory updates (D), extent format (e), case-"
"insensitive directory lookups (F), immutable (i), data journaling (j), don't "
"compress (m), project hierarchy (P), secure deletion (s), synchronous "
"updates (S), no tail-merging (t), top of directory hierarchy (T), "
"undeletable (u), and direct access for files (x)."
msgstr ""
"Літери «aAcCdDeFijmPsStTux» вибирають нові атрибути для файлів: лише "
"дописування (a), без оновлень atime (A), стиснений (c), без копіювання при "
"записі (C), без дампу (d), синхронні оновлення каталогу (D), формат із "
"розширеннями (e), пошук у каталозі без врахування регістру символів (F), "
"незмінний (i), журналювання даних (j), не стискати (m), ієрархія проєкту "
"(P), безпечне вилучення (s), синхронні оновлення (S), без об'єднання за "
"кінцем (t), вершина ієрархії каталогів (T), не можна вилучати (u) і "
"безпосередній доступ до файлів (x)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The following attributes are read-only, and may be listed by B<lsattr>(1)  "
"but not modified by chattr: encrypted (E), indexed directory (I), inline "
"data (N), and verity (V)."
msgstr ""
"Наступні атрибути є придатними лише до читання, їх може бути виведено у "
"списку атрибутів B<lsattr>(1), але не може бути змінено за допомогою chattr: "
"зашифрований (E), індексований каталог (I), вбудовані дані (N) та дійсність "
"(V)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Not all flags are supported or utilized by all file systems; refer to file "
"system-specific man pages such as B<btrfs>(5), B<ext4>(5), and B<xfs>(5)  "
"for more file system-specific details."
msgstr ""
"Підтримку усіх прапорців та використання в усіх файлових системах "
"передбачено не усюди; ознайомтеся із специфічними для файлової системи "
"сторінками підручника, зокрема сторінками B<btrfs>(5), B<ext4>(5) та "
"B<xfs>(5), щоб дізнатися більше про специфічні для файлових систем подробиці."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The 'x' attribute can be set on a directory or file.  If the attribute is "
"set on an existing directory, it will be inherited by all files and "
"subdirectories that are subsequently created in the directory.  If an "
"existing directory has contained some files and subdirectories, modifying "
"the attribute on the parent directory doesn't change the attributes on these "
"files and subdirectories."
msgstr ""
"Атрибут «x» можна встановити для каталогу або файла. Якщо атрибут "
"встановлено для наявного каталогу, його буде успадковано усіма файлами та "
"підкаталогами, які згодом буде створено у цьому каталозі. Якщо у наявному "
"каталозі вже містяться якісь файли і підкаталоги, внесення змін до атрибута "
"батьківського каталогу не змінить атрибутів цих файлів і підкаталогів."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<lsattr>(1), B<btrfs>(5), B<ext4>(5), B<xfs>(5)."
msgstr "B<lsattr>(1), B<btrfs>(5), B<ext4>(5), B<xfs>(5)."
