# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andriy Rysin <arysin@gmail.com>, 2022.
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:03+0200\n"
"PO-Revision-Date: 2022-07-07 17:50+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "IONICE"
msgstr "IONICE"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 травня 2022 року"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr "Команди користувача"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "ionice - set or get process I/O scheduling class and priority"
msgstr ""
"ionice — встановлення або отримання класу і пріоритетності планування "
"введення-виведення процесів"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<ionice> [B<-c> I<class>] [B<-n> I<level>] [B<-t>] B<-p> I<PID>"
msgstr "B<ionice> [B<-c> I<клас>] [B<-n> I<рівень>] [B<-t>] B<-p> I<PID>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<ionice> [B<-c> I<class>] [B<-n> I<level>] [B<-t>] B<-P> I<PGID>"
msgstr "B<ionice> [B<-c> I<клас>] [B<-n> I<рівень>] [B<-t>] B<-P> I<PGID>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<ionice> [B<-c> I<class>] [B<-n> I<level>] [B<-t>] B<-u> I<UID>"
msgstr "B<ionice> [B<-c> I<клас>] [B<-n> I<рівень>] [B<-t>] B<-u> I<UID>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<ionice> [B<-c> I<class>] [B<-n> I<level>] [B<-t>] I<command> [argument] ..."
msgstr ""
"B<ionice> [B<-c> I<клас>] [B<-n> I<рівень>] [B<-t>] I<команда> [аргумент] ..."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This program sets or gets the I/O scheduling class and priority for a "
"program. If no arguments or just B<-p> is given, B<ionice> will query the "
"current I/O scheduling class and priority for that process."
msgstr ""
"Ця програма встановлює або отримує клас і пріоритетність планування введення-"
"виведення для певної програми. Якщо не вказано аргументів або вказано лише "
"B<-p>, B<ionice> надсилатиме запит щодо класу і пріоритетності планування "
"поточного введення-виведення для відповідного процесу."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"When I<command> is given, B<ionice> will run this command with the given "
"arguments. If no I<class> is specified, then I<command> will be executed "
"with the \"best-effort\" scheduling class. The default priority level is 4."
msgstr ""
"Якщо вказано I<команду>, B<ionice> запустить цю команду із вказаними "
"аргументами. Якщо I<клас> не вказано, I<команду> буде виконано із класом "
"планування «найкращі зусилля» («best-effort»). Типовим рівнем пріоритетності "
"є 4."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"As of this writing, a process can be in one of three scheduling classes:"
msgstr ""
"На час написання цієї сторінки процес може належати до одного з трьох класів "
"планування:"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<Idle>"
msgstr "B<Бездіяльність>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"A program running with idle I/O priority will only get disk time when no "
"other program has asked for disk I/O for a defined grace period. The impact "
"of an idle I/O process on normal system activity should be zero. This "
"scheduling class does not take a priority argument. Presently, this "
"scheduling class is permitted for an ordinary user (since kernel 2.6.25)."
msgstr ""
"Програма, яка працює із лінивою пріоритетністю, отримуватиме час для роботи "
"з диском, якщо ніяка інша програма не просить про квоту для введення-"
"виведення на диск протягом періоду лояльності. Вплив процесу із лінивим "
"введенням-виведенням на звичайну роботу системи має бути нульовим. Цей клас "
"планування не приймає аргументу пріоритетності. На сьогодні, цей клас "
"планування є дозволеним для звичайних користувачів (з часу появи 2.6.25)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<Best-effort>"
msgstr "B<Оптимальне навантаження>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This is the effective scheduling class for any process that has not asked "
"for a specific I/O priority. This class takes a priority argument from "
"I<0-7>, with a lower number being higher priority. Programs running at the "
"same best-effort priority are served in a round-robin fashion."
msgstr ""
"Це клас планування для будь-якого процесу, для якого не визначено певного "
"рівня пріоритетності введення-виведення. Цей клас приймає аргумент "
"пріоритетності I<0-7>, де менші числа означають вищу пріоритетність. "
"Програми, які працюють на одному рівні пріоритетності найкращих зусиль, "
"обслуговуються у циклічний спосіб."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Note that before kernel 2.6.26 a process that has not asked for an I/O "
"priority formally uses \"B<none>\" as scheduling class, but the I/O "
"scheduler will treat such processes as if it were in the best-effort class. "
"The priority within the best-effort class will be dynamically derived from "
"the CPU nice level of the process: io_priority = (cpu_nice + 20) / 5."
msgstr ""
"Зауважте, що до ядра 2.6.26 процес, який не просив про пріоритетність "
"введення-виведення формально використовував клас планування «B<none>», але "
"засіб планування введення-виведення обробляв такі процеси так, наче вони "
"належать до класу найкращих зусиль. Пріоритетність у межах класу найкращих "
"зусиль буде динамічно визначено за рівнем пріоритетності для процесора: "
"пріоритетність_введення_виведення = (пріоритетність_у_процесорі + 20) / 5."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"For kernels after 2.6.26 with the CFQ I/O scheduler, a process that has not "
"asked for an I/O priority inherits its CPU scheduling class. The I/O "
"priority is derived from the CPU nice level of the process (same as before "
"kernel 2.6.26)."
msgstr ""
"Для ядер після 2.6.26 із планувальником введення-виведення CFQ процес, який "
"не надіслав запиту щодо пріоритетності введення-виведення, успадковує клас "
"планування свого процесора. Пріоритетність введення-виведення є похідною від "
"рівня пріоритетності процесу у процесорі (те саме, що і до ядра 2.6.26)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<Realtime>"
msgstr "B<Режим реального часу>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The RT scheduling class is given first access to the disk, regardless of "
"what else is going on in the system. Thus the RT class needs to be used with "
"some care, as it can starve other processes. As with the best-effort class, "
"8 priority levels are defined denoting how big a time slice a given process "
"will receive on each scheduling window. This scheduling class is not "
"permitted for an ordinary (i.e., non-root) user."
msgstr ""
"Клас планування реального часу надає доступ до диска одразу, незалежно від "
"того, що відбувається у системі. Таким чином, клас реального часу слід "
"використовувати обережно, оскільки він лишає без ресурсів усі інші процеси. "
"Так само, як і з класом найкращих зусиль, визначено 8 рівнів пріоритетності, "
"які визначають величину частки, яку отримує вказаний процес у вікні "
"планування. Користування цим класом планування заборонено для звичайних (не-"
"root) користувачів."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-c>, B<--class> I<class>"
msgstr "B<-c>, B<--class> I<клас>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify the name or number of the scheduling class to use; \\f(CR0\\fR for "
"none, \\f(CR1\\fR for realtime, \\f(CR2\\fR for best-effort, \\f(CR3\\fR for "
"idle."
msgstr ""
"Вказати назву або число класу планування, яким слід скористатися; «0» — "
"немає, «1» — планування реального часу, «2» — найкращі зусилля, «3» — "
"лінивий."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-n>, B<--classdata> I<level>"
msgstr "B<-n>, B<--classdata> I<рівень>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify the scheduling class data. This only has an effect if the class "
"accepts an argument. For realtime and best-effort, I<0-7> are valid data "
"(priority levels), and \\f(CR0\\fR represents the highest priority level."
msgstr ""
"Вказати дані класу планування. Буде враховано, лише якщо клас приймає "
"аргумент. Для класів планування реального часу та найкращих зусиль "
"коректними є значення I<0-7> (рівні пріоритетності), а «0» відповідає "
"найвищому рівню пріоритетності."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-p>, B<--pid> I<PID>..."
msgstr "B<-p>, B<--pid> I<PID>..."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify the process IDs of running processes for which to get or set the "
"scheduling parameters."
msgstr ""
"Вказати ідентифікатори процесів для запущених процесів, для яких слід "
"отримати або встановити параметри планування."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-P>, B<--pgid> I<PGID>..."
msgstr "B<-P>, B<--pgid> I<PGID>..."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify the process group IDs of running processes for which to get or set "
"the scheduling parameters."
msgstr ""
"Вказати ідентифікатори груп процесів для запущених процесів, для яких слід "
"отримати або встановити параметри планування."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-t>, B<--ignore>"
msgstr "B<-t>, B<--ignore>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Ignore failure to set the requested priority. If I<command> was specified, "
"run it even in case it was not possible to set the desired scheduling "
"priority, which can happen due to insufficient privileges or an old kernel "
"version."
msgstr ""
"Ігнорувати помилки при встановленні бажаної пріоритетності. Якщо було "
"вказано I<команду>, запустити її, навіть якщо неможливо встановити бажану "
"пріоритетність планування, що може трапитися через недостатні права доступу "
"або стару версію ядра."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-u>, B<--uid> I<UID>..."
msgstr "B<-u>, B<--uid> I<UID>..."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify the user IDs of running processes for which to get or set the "
"scheduling parameters."
msgstr ""
"Вказати ідентифікатори користувача для запущених процесів, для яких слід "
"отримати або встановити параметри планування."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "ПРИМІТКИ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Linux supports I/O scheduling priorities and classes since 2.6.13 with the "
"CFQ I/O scheduler."
msgstr ""
"У Linux передбачено підтримку пріоритетності та класів планування введення-"
"виведення з версії 2.6.13, із введенням планувальника введення-виведення CFQ."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИКЛАДИ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "# B<ionice> -c 3 -p 89"
msgstr "# B<ionice> -c 3 -p 89"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Sets process with PID 89 as an idle I/O process."
msgstr ""
"Встановлює процес із PID 89 як процес із лінивим введенням-виведенням.."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "# B<ionice> -c 2 -n 0 bash"
msgstr "# B<ionice> -c 2 -n 0 bash"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Runs \\(aqbash\\(aq as a best-effort program with highest priority."
msgstr ""
"Запускає «bash» як програму із найкращими зусиллями і найвищою "
"пріоритетністю."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "# B<ionice> -p 89 91"
msgstr "# B<ionice> -p 89 91"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Prints the class and priority of the processes with PID 89 and 91."
msgstr "Виводить клас і пріоритетність процесів із PID 89 і 91."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<ioprio_set>(2)"
msgstr "B<ioprio_set>(2)"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<ionice> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<ionice> є частиною пакунка util-linux, який можна отримати з"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "14 лютого 2022 року"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "Вивести дані щодо версії і завершити роботу."
