# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999,2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013.
# Denis Barbier <barbier@debian.org>, 2006,2010.
# David Prévot <david@tilapin.org>, 2010, 2013.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2011, 2012, 2013.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:07+0200\n"
"PO-Revision-Date: 2023-07-01 00:23+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 20.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "mem"
msgstr "mem"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 octobre 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mem, kmem, port - system memory, kernel memory and system ports"
msgstr ""
"mem, kmem, port — Mémoire système, mémoire du noyau, ports d'entrées-sorties"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I</dev/mem> is a character device file that is an image of the main memory "
"of the computer.  It may be used, for example, to examine (and even patch) "
"the system."
msgstr ""
"I</dev/mem> est un périphérique caractère représentant une image de la "
"mémoire principale de l'ordinateur. Il peut être utilisé, par exemple, pour "
"examiner (et même modifier) la mémoire système."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Byte addresses in I</dev/mem> are interpreted as physical memory addresses.  "
"References to nonexistent locations cause errors to be returned."
msgstr ""
"Les adresses d'octet dans I</dev/mem> sont interprétées comme des adresses "
"physiques. Les références à des adresses inexistantes renvoient des erreurs."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Examining and patching is likely to lead to unexpected results when read-"
"only or write-only bits are present."
msgstr ""
"Examiner et modifier la mémoire est susceptible de conduire à des résultats "
"indésirables quand les bits lecture seule ou écriture seule sont concernés."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since Linux 2.6.26, and depending on the architecture, the "
"B<CONFIG_STRICT_DEVMEM> kernel configuration option limits the areas which "
"can be accessed through this file.  For example: on x86, RAM access is not "
"allowed but accessing memory-mapped PCI regions is."
msgstr ""
"Depuis Linux\\ 2.6.26 et selon l'architecture, l'option de configuration du "
"noyau B<CONFIG_STRICT_DEVMEM> limite les zones auxquelles il est possible "
"d'accéder au moyen de ce fichier. Par exemple, sur les systèmes x86, l'accès "
"à la RAM n'est pas permis, mais accéder aux régions PCI mappés en mémoire "
"l'est."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "It is typically created by:"
msgstr "Il est typiquement créé ainsi\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"mknod -m 660 /dev/mem c 1 1\n"
"chown root:kmem /dev/mem\n"
msgstr ""
"mknod -m 660 /dev/mem c 1 1\n"
"chown root:kmem /dev/mem\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The file I</dev/kmem> is the same as I</dev/mem>, except that the kernel "
"virtual memory rather than physical memory is accessed.  Since Linux 2.6.26, "
"this file is available only if the B<CONFIG_DEVKMEM> kernel configuration "
"option is enabled."
msgstr ""
"Le fichier I</dev/kmem> est identique à I</dev/mem> sauf qu'il s'agit de la "
"mémoire virtuelle du noyau plutôt que de la mémoire physique. Depuis Linux\\ "
"2.6.26, ce fichier n'est disponible que si l'option de configuration "
"B<CONFIG_DEVKMEM> du noyau est activée."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"mknod -m 640 /dev/kmem c 1 2\n"
"chown root:kmem /dev/kmem\n"
msgstr ""
"mknod -m 640 /dev/kmem c 1 2\n"
"chown root:kmem /dev/kmem\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/port> is similar to I</dev/mem>, but the I/O ports are accessed."
msgstr ""
"I</dev/port> est identique à I</dev/mem>, mais ici, c'est aux ports "
"d'entrées-sorties qu'on accède."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"mknod -m 660 /dev/port c 1 4\n"
"chown root:kmem /dev/port\n"
msgstr ""
"mknod -m 660 /dev/port c 1 4\n"
"chown root:kmem /dev/port\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/mem>"
msgstr "I</dev/mem>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/kmem>"
msgstr "I</dev/kmem>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/port>"
msgstr "I</dev/port>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<chown>(1), B<mknod>(1), B<ioperm>(2)"
msgstr "B<chown>(1), B<mknod>(1), B<ioperm>(2)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "MEM"
msgstr "MEM"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2015-01-02"
msgstr "2 janvier 2015"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
