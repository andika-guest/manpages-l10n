# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Grégory Colpart <reg@evolix.fr>
# Nicolas François <nicolas.francois@centraliens.net>, 2008-2010.
# Bastien Scher <bastien0705@gmail.com>, 2011-2013.
# David Prévot <david@tilapin.org>, 2012-2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: coreutils manpages\n"
"POT-Creation-Date: 2023-10-02 12:35+0200\n"
"PO-Revision-Date: 2022-10-16 03:08+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bits\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "WHO"
msgstr "WHO"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "Septembre 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "who - show who is logged on"
msgstr "who - Montrer qui est connecté"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<who> [I<\\,OPTION\\/>]... [ I<\\,FILE | ARG1 ARG2 \\/>]"
msgstr ""
"B<who> [I<\\,OPTION\\/>] ... [ I<\\,FICHIER//> | I<\\PARAM1 PARAM2\\/> ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print information about users who are currently logged in."
msgstr "Afficher les informations sur les utilisateurs actuellement connectés."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "same as B<-b> B<-d> B<--login> B<-p> B<-r> B<-t> B<-T> B<-u>"
msgstr "identique à B<-b> B<-d> B<--login> B<-p> B<-r> B<-t> B<-T> B<-u>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--boot>"
msgstr "B<-b>, B<--boot>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "time of last system boot"
msgstr "afficher l'heure du dernier démarrage"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--dead>"
msgstr "B<-d>, B<--dead>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "print dead processes"
msgstr "afficher les processus morts"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>, B<--heading>"
msgstr "B<-H>, B<--heading>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "print line of column headings"
msgstr "afficher le titre des colonnes"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--login>"
msgstr "B<-l>, B<--login>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "print system login processes"
msgstr "afficher les processus «\\ login\\ »"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--lookup>"
msgstr "B<--lookup>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "attempt to canonicalize hostnames via DNS"
msgstr "tenter d'afficher les noms d'hôtes grâce aux résolutions DNS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "only hostname and user associated with stdin"
msgstr "n'afficher que l'hôte et l'utilisateur associés à «\\ stdin\\ »"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--process>"
msgstr "B<-p>, B<--process>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "print active processes spawned by init"
msgstr "afficher la liste des processus lancés par init"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-q>, B<--count>"
msgstr "B<-q>, B<--count>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "all login names and number of users logged on"
msgstr "afficher tous les utilisateurs connectés et leur nombre"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--runlevel>"
msgstr "B<-r>, B<--runlevel>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "print current runlevel"
msgstr "afficher le niveau d'exécution (runlevel) actuel"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--short>"
msgstr "B<-s>, B<--short>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "print only name, line, and time (default)"
msgstr "afficher seulement le nom, la ligne et l'heure (par défaut)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--time>"
msgstr "B<-t>, B<--time>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "print last system clock change"
msgstr "afficher la date du dernier changement d'heure du système"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<-w>, B<--mesg>"
msgstr "B<-T>, B<-w>, B<--mesg>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "add user's message status as +, - or ?"
msgstr ""
"ajouter un renseignement sur l'état de l'utilisateur avec B<+>, B<-> ou B<?>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--users>"
msgstr "B<-u>, B<--users>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "list users logged in"
msgstr "lister les utilisateurs connectés"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--message>"
msgstr "B<--message>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "same as B<-T>"
msgstr "identique à B<-T>"

# NOTE: typo s/writeable/writable/ également dans le --help français
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--writable>"
msgstr "B<--writable>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afficher l'aide-mémoire et quitter."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "afficher les informations de version et quitter."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If FILE is not specified, use I<\\,/var/run/utmp\\/>.  I<\\,/var/log/wtmp\\/"
"> as FILE is common.  If ARG1 ARG2 given, B<-m> presumed: 'am i' or 'mom "
"likes' are usual."
msgstr ""
"Si le paramètre I<FICHIER> n'est pas précisé, le fichier I<\\,/var/run/"
"utmp\\/> sera utilisé. Le fichier I<\\,/var/log/wtmp\\/> est un I<FICHIER> "
"souvent utilisé. Si I<PARAM1> et I<PARAM2> sont fournis, le comportement est "
"le même qu'avec l'option B<-m>\\ : «\\ am i\\ » ou «\\ mom likes\\ » peuvent "
"être employés."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Joseph Arceneaux, David MacKenzie, and Michael Stone."
msgstr "Écrit par Joseph Arceneaux, David MacKenzie et Michael Stone."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Aide en ligne de GNU coreutils : E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Signaler toute erreur de traduction à E<lt>https://translationproject.org/"
"team/fr.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ce programme est un logiciel libre. Vous pouvez le modifier et le "
"redistribuer. Il n'y a AUCUNE GARANTIE dans la mesure autorisée par la loi."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/whoE<gt>"
msgstr ""
"Documentation complète : E<lt>I<https://www.gnu.org/software/coreutils/"
"who>E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) who invocation\\(aq"
msgstr ""
"aussi disponible localement à l’aide de la commande : info \\(aq(coreutils) "
"who invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "Septembre 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--ips>"
msgstr "B<--ips>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"print ips instead of hostnames. with B<--lookup>, canonicalizes based on "
"stored IP, if available, rather than stored hostname"
msgstr ""
"afficher les adresses IP plutôt que les noms d'hôte. Avec B<--lookup>, la "
"représentation est simplifiée en se basant sur l'adresse IP enregistrée si "
"elle est disponible, plutôt que sur le nom d'hôte enregistré."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Avril 2022"

#. type: Plain text
#: mageia-cauldron
msgid ""
"If FILE is not specified, use I<\\,/run/utmp\\/>.  I<\\,/var/log/wtmp\\/> as "
"FILE is common.  If ARG1 ARG2 given, B<-m> presumed: 'am i' or 'mom likes' "
"are usual."
msgstr ""
"Si le paramètre I<FICHIER> n'est pas précisé, le fichier I<\\,/run/utmp\\/> "
"sera utilisé. Le fichier I<\\,/var/log/wtmp\\/> est un I<FICHIER> souvent "
"utilisé. Si I<PARAM1> et I<PARAM2> sont fournis, le comportement est le même "
"qu'avec l'option B<-m>\\ : «\\ am i\\ » ou «\\ mom likes\\ » peuvent être "
"employés."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "Octobre 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
