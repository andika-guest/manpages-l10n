# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-10-02 12:02+0200\n"
"PO-Revision-Date: 2023-07-14 18:31+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-GLUE-EFI"
msgstr "GRUB-GLUE-EFI"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2023"
msgstr "Juillet 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12rc1-1"
msgstr "GRUB 2:2.12rc1-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-glue-efi - generate a fat binary for EFI"
msgstr "grub-glue-efi – Générer un binaire fusionné pour EFI"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-glue-efi> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgstr "B<grub-glue-efi> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"grub-glue-efi processes ia32 and amd64 EFI images and glues them according "
"to Apple format."
msgstr ""
"grub-glue-efi traite des images EFI ia32 et amd64 et les assemble "
"conformément au format Apple."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Glue 32-bit and 64-bit binary into Apple universal one."
msgstr ""
"Assembler des binaires 32 bits et 64 bits en un binaire Apple universel."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-3>, B<--input32>=I<\\,FILE\\/>"
msgstr "B<-3>, B<--input32>=I<\\,FICHIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set input filename for 32-bit part."
msgstr "Déterminer le fichier d'entrée pour la partie 32 bits."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-6>, B<--input64>=I<\\,FILE\\/>"
msgstr "B<-6>, B<--input64>=I<\\,FICHIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set input filename for 64-bit part."
msgstr "Déterminer le fichier d'entrée pour la partie 64 bits."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FICHIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set output filename. Default is STDOUT"
msgstr "Déterminer le fichier de sortie. Par défaut la sortie standard."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "Afficher des messages détaillés."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "Afficher l’aide-mémoire."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "Afficher un court message pour l’utilisation."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "Afficher la version du programme."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Les paramètres obligatoires ou facultatifs pour les options de forme longue "
"le sont aussi pour les options correspondantes de forme courte."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Signaler toute erreur à E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-glue-efi> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-glue-efi> programs are properly installed "
"at your site, the command"
msgstr ""
"La documentation complète de B<grub-glue-efi> est disponible dans un manuel "
"Texinfo. Si les programmes B<info> et B<grub-glue-efi> sont correctement "
"installés, la commande"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-glue-efi>"
msgstr "B<info grub-glue-efi>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "devrait vous donner accès au manuel complet."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "April 2023"
msgstr "Avril 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13"
msgstr "GRUB 2.06-13"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "September 2023"
msgstr "Septembre 2023"

#. type: TH
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "GRUB 2:2.12rc1-1"
msgid "GRUB 2.12~rc1-10"
msgstr "GRUB 2:2.12rc1-1"
