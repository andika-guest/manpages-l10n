# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>, 2021
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"POT-Creation-Date: 2023-08-27 17:08+0200\n"
"PO-Revision-Date: 2022-05-03 19:04+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: vim\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "MKFS.MINIX"
msgstr "MKFS.MINIX"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "System Administration"
msgstr "Administration Système"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "mkfs.minix - make a Minix filesystem"
msgstr "mkfs.minix - Créer un système de fichiers MINIX"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<mkfs.minix> [options] I<device> [I<size-in-blocks>]"
msgstr "B<mkfs.minix> [I<options>] I<périphérique> [I<taille_en_bloc>]"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<mkfs.minix> creates a Linux MINIX filesystem on a device (usually a disk "
"partition)."
msgstr ""
"B<mkfs.minix> crée un système de fichiers MINIX sur un périphérique "
"(généralement une partition d'un disque)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The I<device> is usually of the following form:"
msgstr ""
"Le I<périphérique> est généralement indiqué dans un des formats suivants :"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"/dev/hda[1-8] (IDE disk 1)\n"
"/dev/hdb[1-8] (IDE disk 2)\n"
"/dev/sda[1-8] (SCSI disk 1)\n"
"/dev/sdb[1-8] (SCSI disk 2)\n"
msgstr ""
"/dev/hda[1-8] (disque IDE 1)\n"
"/dev/hdb[1-8] (disque IDE 2)\n"
"/dev/sda[1-8] (disque SCSI 1)\n"
"/dev/sdb[1-8] (disque SCSI 2)\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The device may be a block device or an image file of one, but this is not "
"enforced. Expect not much fun on a character device :-)."
msgstr ""
"Le périphérique peut être un périphérique bloc ou un de ses fichiers image "
"mais ce n’est pas obligatoire. Ne vous attendez pas à un quelconque miracle "
"pour un périphérique caractère :-)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The I<size-in-blocks> parameter is the desired size of the file system, in "
"blocks. It is present only for backwards compatibility. If omitted the size "
"will be determined automatically. Only block counts strictly greater than 10 "
"and strictly less than 65536 are allowed."
msgstr ""
"Le paramètre I<taille_en_bloc> est la taille désirée pour le système de "
"fichiers (en nombre de blocs). Il n'est présent que pour être compatible "
"avec les anciennes versions. S'il est omis, la taille sera déterminée "
"automatiquement. Il doit exister au moins 10 blocs et il ne peut y en avoir "
"plus de 65 536."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-c>, B<--check>"
msgstr "B<-c>, B<--check>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Check the device for bad blocks before creating the filesystem. If any are "
"found, the count is printed."
msgstr ""
"Vérifier si les blocs sont corrompus ou non sur le périphérique avant de "
"créer le système de fichiers. Si B<mkfs.minix> en trouve, le total est "
"affiché."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-n>, B<--namelength> I<length>"
msgstr "B<-n>, B<--namelength> I<taille>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify the maximum length of filenames. Currently, the only allowable "
"values are 14 and 30 for file system versions 1 and 2. Version 3 allows only "
"value 60. The default is 30."
msgstr ""
"Indiquer la taille maximale d'un nom de fichier. Actuellement, les seules "
"valeurs autorisées sont 14 et 30 pour les versions 1 et 2 de système de "
"fichiers. La version 3 n’accepte que la valeur 60. La valeur par défaut "
"est 30."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--lock>[=I<mode>]"
msgstr "B<--lock>[B<=>I<mode>]"

#. type: Plain text
#: debian-bookworm
msgid ""
"Use exclusive BSD lock for device or file it operates. The optional argument "
"I<mode> can be B<yes>, B<no> (or 1 and 0) or B<nonblock>. If the I<mode> "
"argument is omitted, it defaults to B<yes>. This option overwrites "
"environment variable B<$LOCK_BLOCK_DEVICE>. The default is not to use any "
"lock at all, but it\\(cqs recommended to avoid collisions with B<systemd-"
"udevd>(8) or other tools."
msgstr ""
"Utiliser un verrou BSD exclusif pour le périphérique ou le fichier visé. "
"L’argument facultatif I<mode> peut être B<yes>, B<no> (ou B<1> et B<0>) ou "
"B<nonblock>. Si cet argument est absent, sa valeur par défaut est B<yes>. "
"Cette option écrase la variable d’environnement B<$LOCK_BLOCK_DEVICE>. Le "
"comportement par défaut est de n’utiliser aucun verrou, mais cela est "
"recommandé pour éviter des collisions avec B<systemd-udevd> ou d’autres "
"outils."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-i>, B<--inodes> I<number>"
msgstr "B<-i>, B<--inodes> I<nombre>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Specify the number of inodes for the filesystem."
msgstr "Indiquer le nombre d'inœuds sur le système de fichiers."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-l>, B<--badblocks> I<filename>"
msgstr "B<-l>, B<--badblocks> I<fichier>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Read the list of bad blocks from I<filename>. The file has one bad-block "
"number per line. The count of bad blocks read is printed."
msgstr ""
"Lire la liste des blocs corrompus depuis le I<fichier>. Ce fichier contient "
"un numéro de bloc défectueux par ligne. Le total des secteurs défectueux lus "
"est ensuite affiché."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-1>"
msgstr "B<-1>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Make a Minix version 1 filesystem. This is the default."
msgstr ""
"Créer un système de fichiers MINIX version 1. C’est le comportement par "
"défaut."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-2>, B<-v>"
msgstr "B<-2>, B<-v>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Make a Minix version 2 filesystem."
msgstr "Créer un système de fichiers MINIX version 2."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-3>"
msgstr "B<-3>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Make a Minix version 3 filesystem."
msgstr "Créer un système de fichiers MINIX version 3."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Afficher l’aide-mémoire puis quitter."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Print version and exit.  The long option cannot be combined with other "
"options."
msgstr ""
"Afficher la version et quitter. L'option longue ne peut pas être combinée "
"avec d’autres options."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ENVIRONNEMENT"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "LOCK_BLOCK_DEVICE=E<lt>modeE<gt>"
msgstr "LOCK_BLOCK_DEVICE=E<lt>modeE<gt>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"use exclusive BSD lock. The mode is \"1\" or \"0\". See B<--lock> for more "
"details."
msgstr ""
"Utiliser un verrou exclusif BSD. Le mode est « 1 » ou « 0 ». Consulter B<--"
"lock> pour davantage de détails."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXIT STATUS"
msgstr "CODE DE RETOUR"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The exit status returned by B<mkfs.minix> is one of the following:"
msgstr ""
"Le code de retour renvoyé par B<mkfs.minix> est l'un des codes suivants :"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "0"
msgstr "0"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "No errors"
msgstr "Pas d'erreur."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "8"
msgstr "8"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Operational error"
msgstr "Erreur lors de l'opération."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "16"
msgstr "16"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Usage or syntax error"
msgstr "Erreur d'utilisation ou de syntaxe."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<fsck>(8), B<mkfs>(8), B<reboot>(8)"
msgstr "B<fsck>(8), B<mkfs>(8), B<reboot>(8)"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Pour envoyer un rapport de bogue, utilisez le système de gestion des "
"problèmes à l'adresse"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<mkfs.minix> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"La commande B<mkfs.minix> fait partie du paquet util-linux, qui peut être "
"téléchargé de"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "14 février 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Use exclusive BSD lock for device or file it operates. The optional argument "
"I<mode> can be B<yes>, B<no> (or 1 and 0) or B<nonblock>. If the I<mode> "
"argument is omitted, it defaults to B<\"yes\">. This option overwrites "
"environment variable B<$LOCK_BLOCK_DEVICE>. The default is not to use any "
"lock at all, but it\\(cqs recommended to avoid collisions with udevd or "
"other tools."
msgstr ""
"Utiliser un verrou BSD exclusif pour le périphérique ou le fichier visé. "
"L’argument facultatif I<mode> peut être B<yes>, B<no> (ou B<1> et B<0>) ou "
"B<nonblock>. Si cet argument est absent, sa valeur par défaut est B<yes>. "
"Cette option écrase la variable d’environnement B<$LOCK_BLOCK_DEVICE>. Le "
"comportement par défaut est de n’utiliser aucun verrou, mais cela est "
"recommandé pour éviter des collisions avec udevd ou d’autres outils."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Display version information and exit. The long option cannot be combined "
"with other options."
msgstr ""
"Afficher les informations sur la version et quitter. L'option longue ne peut "
"pas être combinée avec d’autres options."
