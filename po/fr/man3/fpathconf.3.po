# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2011.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Lucien Gentis <lucien.gentis@waika9.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 16:57+0200\n"
"PO-Revision-Date: 2023-03-14 12:29+0100\n"
"Last-Translator: Lucien Gentis <lucien.gentis@waika9.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 2.4.2\n"
"X-Poedit-Bookmarks: 41,-1,-1,-1,-1,-1,-1,-1,-1,-1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "fpathconf"
msgstr "fpathconf"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 juillet 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "fpathconf, pathconf - get configuration values for files"
msgstr "fpathconf, pathconf - Obtenir des valeurs de configuration de fichiers"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long fpathconf(int >I<fd>B<, int >I<name>B<);>\n"
"B<long pathconf(const char *>I<path>B<, int >I<name>B<);>\n"
msgstr ""
"B<long fpathconf(int >I<fd>B<, int >I<nom_option>B<);>\n"
"B<long pathconf(const char *>I<chemin>B<, int >I<nom_option>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<fpathconf>()  gets a value for the configuration option I<name> for the "
"open file descriptor I<fd>."
msgstr ""
"La fonction B<fpathconf>() récupère la valeur de l'option de configuration "
"I<nom_option> pour le descripteur de fichier ouvert I<fd>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<pathconf>()  gets a value for configuration option I<name> for the "
"filename I<path>."
msgstr ""
"La fonction B<pathconf>() récupère la valeur de l'option de configuration "
"I<nom_option> pour le fichier I<chemin>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The corresponding macros defined in I<E<lt>unistd.hE<gt>> are minimum "
"values; if an application wants to take advantage of values which may "
"change, a call to B<fpathconf>()  or B<pathconf>()  can be made, which may "
"yield more liberal results."
msgstr ""
"Les macros correspondantes définies dans I<E<lt>unistd.hE<gt>> donnent des "
"informations minimales\\ ; si une application désire tirer partie des "
"valeurs qui peuvent évoluer, elle peut faire appel à B<fpathconf>() ou "
"B<pathconf>() pour obtenir des résultats plus détaillés."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Setting I<name> equal to one of the following constants returns the "
"following configuration options:"
msgstr ""
"Définir I<nom_option> à l'une des constantes suivantes renvoie les options "
"de configuration suivantes\\ :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<_PC_LINK_MAX>"
msgstr "B<_PC_LINK_MAX>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The maximum number of links to the file.  If I<fd> or I<path> refer to a "
"directory, then the value applies to the whole directory.  The corresponding "
"macro is B<_POSIX_LINK_MAX>."
msgstr ""
"Le nombre maximal de liens sur le fichier. Si I<fd> ou I<chemin> "
"correspondent à un répertoire, la valeur s'applique à l'ensemble du "
"répertoire. La macro correspondante est B<_POSIX_LINK_MAX>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<_PC_MAX_CANON>"
msgstr "B<_PC_MAX_CANON>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The maximum length of a formatted input line, where I<fd> or I<path> must "
"refer to a terminal.  The corresponding macro is B<_POSIX_MAX_CANON>."
msgstr ""
"La longueur maximale des lignes de saisie formatées\\ ; I<fd> ou I<chemin> "
"doivent correspondre à un terminal. La macro correspondante est "
"B<_POSIX_MAX_CANON>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<_PC_MAX_INPUT>"
msgstr "B<_PC_MAX_INPUT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The maximum length of an input line, where I<fd> or I<path> must refer to a "
"terminal.  The corresponding macro is B<_POSIX_MAX_INPUT>."
msgstr ""
"La longueur maximale d'une ligne de saisie\\ ; I<fd> ou I<chemin> doivent "
"correspondre à un terminal. La macro correspondante est B<_POSIX_MAX_INPUT>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<_PC_NAME_MAX>"
msgstr "B<_PC_NAME_MAX>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The maximum length of a filename in the directory I<path> or I<fd> that the "
"process is allowed to create.  The corresponding macro is B<_POSIX_NAME_MAX>."
msgstr ""
"La longueur maximale du nom d'un fichier que le processus a le droit de "
"créer dans les répertoires I<chemin> ou I<fd>. La macro correspondante est "
"B<_POSIX_NAME_MAX>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<_PC_PATH_MAX>"
msgstr "B<_PC_PATH_MAX>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The maximum length of a relative pathname when I<path> or I<fd> is the "
"current working directory.  The corresponding macro is B<_POSIX_PATH_MAX>."
msgstr ""
"La longueur maximale des chemins relatifs lorsque I<chemin> ou I<fd> est le "
"répertoire courant. La macro correspondante est B<_POSIX_PATH_MAX>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<_PC_PIPE_BUF>"
msgstr "B<_PC_PIPE_BUF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The maximum number of bytes that can be written atomically to a pipe of "
"FIFO.  For B<fpathconf>(), I<fd> should refer to a pipe or FIFO.  For "
"B<fpathconf>(), I<path> should refer to a FIFO or a directory; in the latter "
"case, the returned value corresponds to FIFOs created in that directory.  "
"The corresponding macro is B<_POSIX_PIPE_BUF>."
msgstr ""
"Le nombre maximal d'octets qui peuvent être écrits en une seule fois dans un "
"tube ou une file FIFO. Pour B<fpathconf>(), I<fd> doit faire référence à un "
"tube ou à une file FIFO. Pour B<pathconf>(), I<chemin> doit faire référence "
"à une file FIFO ou à un répertoire ; dans ce dernier cas, la valeur renvoyée "
"correspondra aux files FIFO créées dans ce répertoire. La macro "
"correspondante est B<_POSIX_PIPE_BUF>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<_PC_CHOWN_RESTRICTED>"
msgstr "B<_PC_CHOWN_RESTRICTED>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This returns a positive value if the use of B<chown>(2)  and B<fchown>(2)  "
"for changing a file's user ID is restricted to a process with appropriate "
"privileges, and changing a file's group ID to a value other than the "
"process's effective group ID or one of its supplementary group IDs is "
"restricted to a process with appropriate privileges.  According to POSIX.1, "
"this variable shall always be defined with a value other than -1.  The "
"corresponding macro is B<_POSIX_CHOWN_RESTRICTED>."
msgstr ""
"La valeur renvoyée est positive si l'utilisation de B<chown>(2) et "
"B<fchown>(2) pour changer l'UID d'un fichier est restreinte à un processus "
"possédant des privilèges appropriés, et si leur utilisation pour changer le "
"GID d'un fichier à une valeur autre que celle du GID effectif du processus "
"ou de ses GID supplémentaires est restreinte à un processus possédant les "
"privilèges appropriés. En accord avec POSIX.1, cette variable sera toujours "
"définie avec une valeur différente de B<-1>. La macro correspondante est "
"B<_POSIX_CHOWN_RESTRICTED>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<fd> or I<path> refers to a directory, then the return value applies to "
"all files in that directory."
msgstr ""
"Si I<fd> ou I<chemin> fait référence à un répertoire, la valeur renvoyée est "
"valable pour tous les fichiers du répertoire considéré."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<_PC_NO_TRUNC>"
msgstr "B<_PC_NO_TRUNC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This returns nonzero if accessing filenames longer than B<_POSIX_NAME_MAX> "
"generates an error.  The corresponding macro is B<_POSIX_NO_TRUNC>."
msgstr ""
"Renvoie une valeur non nulle si l'accès à des noms de fichier plus long que "
"B<_POSIX_NAME_MAX> génère une erreur. La macro correspondante est "
"B<_POSIX_NO_TRUNC>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<_PC_VDISABLE>"
msgstr "B<_PC_VDISABLE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This returns nonzero if special character processing can be disabled, where "
"I<fd> or I<path> must refer to a terminal."
msgstr ""
"Renvoie une valeur non nulle si la gestion des caractères spéciaux peut être "
"désactivée, auquel cas I<fd> ou I<path> doit correspondre à un terminal."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The return value of these functions is one of the following:"
msgstr "Ces fonctions peuvent renvoyer une des valeurs suivantes :"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "-"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On error, -1 is returned and I<errno> is set to indicate the error (for "
"example, B<EINVAL>, indicating that I<name> is invalid)."
msgstr ""
"En cas d'erreur, la valeur renvoyée est B<-1> et I<errno> est définie pour "
"préciser l'erreur (par exemple, B<EINVAL> pour signaler que I<nom_option> "
"est non valable)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<name> corresponds to a maximum or minimum limit, and that limit is "
"indeterminate, -1 is returned and I<errno> is not changed.  (To distinguish "
"an indeterminate limit from an error, set I<errno> to zero before the call, "
"and then check whether I<errno> is nonzero when -1 is returned.)"
msgstr ""
"Si I<nom_option> correspond à une limite supérieure ou inférieure et si "
"cette limite est indéterminée, la valeur renvoyée est B<-1> et I<errno> "
"n'est pas modifiée (pour faire la distinction entre une limite indéterminée "
"et une erreur, définir I<errno> à zéro avant l'appel, puis vérifier si la "
"valeur de I<errno> est différente de zéro lorsque la valeur renvoyée "
"est B<-1>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<name> corresponds to an option, a positive value is returned if the "
"option is supported, and -1 is returned if the option is not supported."
msgstr ""
"Si I<nom_option> est un nom d'option valable, une valeur positive est "
"renvoyée si l'option correspondante est prise en charge, ou B<-1> dans le "
"cas contraire."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Otherwise, the current value of the option or limit is returned.  This value "
"will not be more restrictive than the corresponding value that was described "
"to the application in I<E<lt>unistd.hE<gt>> or I<E<lt>limits.hE<gt>> when "
"the application was compiled."
msgstr ""
"Sinon, la valeur actuelle de l'option ou de la limite est renvoyée. Cette "
"valeur ne sera pas plus restrictive que la valeur correspondante indiquée à "
"l'application dans I<E<lt>unistd.hE<gt>> ou I<E<lt>limits.hE<gt>> à la "
"compilation de cette application."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<pathconf>())  Search permission is denied for one of the directories in "
"the path prefix of I<path>."
msgstr ""
"B<pathconf>() : la permission de recherche est refusée pour un des "
"répertoires situés dans le préfixe de chemin de I<chemin>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "(B<fpathconf>())  I<fd> is not a valid file descriptor."
msgstr "B<fpathconf>() : I<fd> n'est pas un descripteur de fichier valable."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<name> is invalid."
msgstr "I<nom_option> n'est pas valable."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The implementation does not support an association of I<name> with the "
"specified file."
msgstr ""
"L'implémentation ne prend pas en charge l'association de I<nom_option> avec "
"le fichier spécifié."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<pathconf>())  Too many symbolic links were encountered while resolving "
"I<path>."
msgstr ""
"B<pathconf>() : trop de liens symboliques rencontrés en résolvant I<chemin>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "(B<pathconf>())  I<path> is too long."
msgstr "B<pathconf>() : I<chemin> est trop long."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<pathconf>())  A component of I<path> does not exist, or I<path> is an "
"empty string."
msgstr ""
"B<pathconf>() : un composant de I<chemin> n'existe pas ou I<chemin> est une "
"chaîne vide."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<pathconf>())  A component used as a directory in I<path> is not in fact a "
"directory."
msgstr ""
"B<pathconf>() : un élément utilisé comme répertoire de I<chemin> n'est en "
"fait pas un répertoire."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<fpathconf>(),\n"
"B<pathconf>()"
msgstr ""
"B<fpathconf>(),\n"
"B<pathconf>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Files with name lengths longer than the value returned for I<name> equal to "
"B<_PC_NAME_MAX> may exist in the given directory."
msgstr ""
"Les fichiers dont la longueur du nom excède la valeur renvoyée pour "
"I<nom_option>, définie à B<_PC_NAME_MAX>, peuvent exister dans le répertoire "
"considéré."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Some returned values may be huge; they are not suitable for allocating "
"memory."
msgstr ""
"Certaines valeurs renvoyées peuvent être énormes\\ ; elles ne sont pas "
"utilisables pour réaliser des allocations mémoires."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getconf>(1), B<open>(2), B<statfs>(2), B<confstr>(3), B<sysconf>(3)"
msgstr "B<getconf>(1), B<open>(2), B<statfs>(2), B<confstr>(3), B<sysconf>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "FPATHCONF"
msgstr "FPATHCONF"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-07-13"
msgstr "13 juillet 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On error, -1 is returned and I<errno> is set to indicate the cause of the "
"error (for example, B<EINVAL>, indicating that I<name> is invalid)."
msgstr ""
"En cas d'erreur, B<-1> est renvoyée et I<errno> est définie pour indiquer la "
"cause de l'erreur (par exemple, B<EINVAL> indique que I<nom_option> n'est "
"pas valable)."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
