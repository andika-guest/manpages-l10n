# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Baptiste Holcroft <jean-baptiste@holcroft.fr>, 2018.
# Grégoire Scano <gregoire.scano@malloc.fr>, 2019-2021.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:32+0200\n"
"PO-Revision-Date: 2023-03-08 11:58+0100\n"
"Last-Translator: Grégoire Scano <gregoire.scano@malloc.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "wcsncmp"
msgstr "wcsncmp"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 juillet 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "wcsncmp - compare two fixed-size wide-character strings"
msgstr "wcsncmp - Comparer deux chaînes de caractères larges de longueur fixe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr "B<#include E<lt>wchar.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int wcsncmp(const wchar_t >I<s1>B<[.>I<n>B<], const wchar_t >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr "B<int wcsncmp(const wchar_t >I<s1>B<[.>I<n>B<], const wchar_t >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<wcsncmp>()  function is the wide-character equivalent of the "
"B<strncmp>(3)  function.  It compares the wide-character string pointed to "
"by I<s1> and the wide-character string pointed to by I<s2>, but at most I<n> "
"wide characters from each string.  In each string, the comparison extends "
"only up to the first occurrence of a null wide character "
"(L\\[aq]\\e0\\[aq]), if any."
msgstr ""
"La fonction B<wcsncmp>() est l'équivalent pour les caractères larges de la "
"fonction B<strncmp>(3). Elle compare la chaîne de caractères larges pointée "
"par I<s1> et celle pointée par I<s2>, en ne considérant que I<n> caractères "
"larges au maximum pour chaque chaîne. Au sein de chaque chaîne, la "
"comparaison n'a lieu que jusqu'à la première occurrence d’un caractère large "
"nul final (L\\[aq]\\e0\\[aq]), s'il y en a un."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<wcsncmp>()  function returns zero if the wide-character strings at "
"I<s1> and I<s2>, truncated to at most length I<n>, are equal.  It returns an "
"integer greater than zero if at the first differing position I<i> (I<i> "
"E<lt> I<n>), the corresponding wide-character I<s1[i]> is greater than "
"I<s2[i]>.  It returns an integer less than zero if at the first differing "
"position I<i> (I<i> E<lt> I<n>), the corresponding wide-character I<s1[i]> "
"is less than I<s2[i]>."
msgstr ""
"La fonction B<wcsncmp>() renvoie zéro si les chaînes de caractères larges "
"I<s1> et I<s2>, tronquées à au plus I<n> caractères, sont égales. Elle "
"renvoie un entier positif lorsqu'à l'emplacement I<i> de la première "
"différence (I<i> E<lt> I<n>), le caractère large correspondant I<s1[i]> est "
"supérieur à I<s2[i]>. Elle renvoie un entier négatif lorsqu'à l'emplacement "
"I<i> de la première différence (I<i> E<lt> I<n>), le caractère large "
"correspondant I<s1[i]> est inférieur à I<s2[i]>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<wcsncmp>()"
msgstr "B<wcsncmp>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, C99."
msgstr "POSIX.1-2001, C99."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<strncmp>(3), B<wcsncasecmp>(3)"
msgstr "B<strncmp>(3), B<wcsncasecmp>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "WCSNCMP"
msgstr "WCSNCMP"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2015-08-08"
msgstr "8 août 2015"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int wcsncmp(const wchar_t *>I<s1>B<, const wchar_t *>I<s2>B<, size_t >I<n>B<);>\n"
msgstr "B<int wcsncmp(const wchar_t *>I<s1>B<, const wchar_t *>I<s2>B<, size_t >I<n>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<wcsncmp>()  function is the wide-character equivalent of the "
"B<strncmp>(3)  function.  It compares the wide-character string pointed to "
"by I<s1> and the wide-character string pointed to by I<s2>, but at most I<n> "
"wide characters from each string.  In each string, the comparison extends "
"only up to the first occurrence of a null wide character (L\\(aq\\e0\\(aq), "
"if any."
msgstr ""
"La fonction B<wcsncmp>() est l'équivalent pour les caractères larges de la "
"fonction B<strncmp>(3). Elle compare la chaîne de caractères larges pointée "
"par I<s1> et celle pointée par I<s2>, en ne considérant que I<n> caractères "
"larges au maximum pour chaque chaîne. Au sein de chaque chaîne, la "
"comparaison n'a lieu que jusqu'à la première occurrence d’un caractère large "
"nul final (L\\(aq\\e0\\(aq), s'il y en a un."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<wcsncmp>()  function returns zero if the wide-character strings at "
"I<s1> and I<s2>, truncated to at most length I<n>, are equal.  It returns an "
"integer greater than zero if at the first differing position I<i> (I<i> "
"E<lt> I<n>), the corresponding wide-character I<s1[i]> is greater than "
"I<s2[i]>.  It returns an integer less than zero if at the first differing "
"position I<i> (i E<lt> I<n>), the corresponding wide-character I<s1[i]> is "
"less than I<s2[i]>."
msgstr ""
"La fonction B<wcsncmp>() renvoie zéro si les chaînes de caractères larges "
"I<s1> et I<s2>, tronquées à au plus I<n> caractères, sont égales. Elle "
"renvoie un entier positif lorsqu'à l'emplacement I<i> de la première "
"différence (I<i> E<lt> I<n>), le caractère large correspondant I<s1[i]> est "
"supérieur à I<s2[i]>. Elle renvoie un entier négatif lorsqu'à l'emplacement "
"I<i> de la première différence (I<i> E<lt> I<n>), le caractère large "
"correspondant I<s1[i]> est inférieur à I<s2[i]>."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
