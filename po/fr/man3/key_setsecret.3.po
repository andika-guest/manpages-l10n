# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:05+0200\n"
"PO-Revision-Date: 2023-07-15 22:48+0200\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "key_setsecret"
msgstr "key_setsecret"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 juillet 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"key_decryptsession, key_encryptsession, key_setsecret, key_gendes, "
"key_secretkey_is_set - interfaces to rpc keyserver daemon"
msgstr ""
"key_decryptsession, key_encryptsession, key_setsecret, key_gendes, "
"key_secretkey_is_set - Interface pour le démon générateur de clé RPC."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>rpc/rpc.hE<gt>>\n"
msgstr "B<#include E<lt>rpc/rpc.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int key_decryptsession(char *>I<remotename>B<, des_block *>I<deskey>B<);>\n"
"B<int key_encryptsession(char *>I<remotename>B<, des_block *>I<deskey>B<);>\n"
msgstr ""
"B<int key_decryptsession(char *>I<nom_distant>B<,> B<des_block *>I<clé_des>B<);>\n"
"B<int key_encryptsession(char *>I<nom_distant>B<,> B<des_block *>I<clé_des>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int key_gendes(des_block *>I<deskey>B<);>\n"
msgstr "B<int key_gendes(des_block *>I<clé_des>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int key_setsecret(char *>I<key>B<);>\n"
"B<int key_secretkey_is_set(void);>\n"
msgstr ""
"B<int key_setsecret(char *>I<clé>B<);>\n"
"B<int key_secretkey_is_set(void);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The functions here are used within the RPC's secure authentication mechanism "
"(AUTH_DES).  There should be no need for user programs to use this functions."
msgstr ""
"Ces fonctions sont utilisées dans le mécanisme d'authentification des RPC "
"sécurisées (AUTH_DES). Les programmes utilisateurs ne devraient pas en avoir "
"besoin."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The function B<key_decryptsession>()  uses the (remote) server netname and "
"takes the DES key for decrypting.  It uses the public key of the server and "
"the secret key associated with the effective UID of the calling process."
msgstr ""
"La fonction B<key_decryptsession>() utilise le nom du serveur (distant) et "
"effectue un décodage avec la clé DES transmise. Elle utilise la clé publique "
"du serveur et la clé privée associée avec l'UID effectif du processus "
"appelant."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The function B<key_encryptsession>()  is the inverse of "
"B<key_decryptsession>().  It encrypts the DES keys with the public key of "
"the server and the secret key associated with the effective UID of the "
"calling process."
msgstr ""
"La fonction B<key_encryptsession>() est l'inverse de "
"B<key_decryptsession>(). Elle crypte les clés DES avec la clé publique du "
"serveur et la clé secrète associée à l'UID effectif du processus appelant."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The function B<key_gendes>()  is used to ask the keyserver for a secure "
"conversation key."
msgstr ""
"La fonction B<key_gendes>() est utilisée pour demander à un serveur une clé "
"de session sécurisée."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The function B<key_setsecret>()  is used to set the key for the effective "
"UID of the calling process."
msgstr ""
"La fonction B<key_setsecret>() définit la clé associée à l'UID effectif du "
"processus appelant."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The function B<key_secretkey_is_set>()  can be used to determine whether a "
"key has been set for the effective UID of the calling process."
msgstr ""
"La fonction B<key_secretkey_is_set>() sert à savoir su une clé a été "
"enregistrée pour l'UID effectif du processus appelant."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These functions return 1 on success and 0 on failure."
msgstr ""
"Ces fonctions renvoient 1 si elles réussissent et zéro si elles échouent."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<key_decryptsession>(),\n"
"B<key_encryptsession>(),\n"
"B<key_gendes>(),\n"
"B<key_setsecret>(),\n"
"B<key_secretkey_is_set>()"
msgstr ""
"B<key_decryptsession>(),\n"
"B<key_encryptsession>(),\n"
"B<key_gendes>(),\n"
"B<key_setsecret>(),\n"
"B<key_secretkey_is_set>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that we talk about two types of encryption here.  One is asymmetric "
"using a public and secret key.  The other is symmetric, the 64-bit DES."
msgstr ""
"Notez bien qu'il y a deux types de cryptage ici\\ : l'un est asymétrique, "
"avec une clé publique et une clé secrète. L'autre est symétrique, avec la "
"clé DES 64-bits."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These routines were part of the Linux/Doors-project, abandoned by now."
msgstr ""
"Ces routines faisaient partie du projet Linux/Doors, à présent abandonné."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<crypt>(3)"
msgstr "B<crypt>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-12-15"
msgstr "15 décembre 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "KEY_SETSECRET"
msgstr "KEY_SETSECRET"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>rpc/rpc.hE<gt>>"
msgstr "B<#include E<lt>rpc/rpc.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<int key_decryptsession(char *>I<remotename>B<,> B<des_block *>I<deskey>B<);"
">"
msgstr ""
"B<int key_decryptsession(char *>I<nom_distant>B<,> B<des_block "
"*>I<clé_des>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<int key_encryptsession(char *>I<remotename>B<,> B<des_block *>I<deskey>B<);"
">"
msgstr ""
"B<int key_encryptsession(char *>I<nom_distant>B<,> B<des_block "
"*>I<clé_des>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int key_gendes(des_block *>I<deskey>B<);>"
msgstr "B<int key_gendes(des_block *>I<clé_des>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int key_setsecret(char *>I<key>B<);>"
msgstr "B<int key_setsecret(char *>I<clé>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int key_secretkey_is_set(void);>"
msgstr "B<int key_secretkey_is_set(void);>"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "B<key_decryptsession>(),\n"
msgstr "B<key_decryptsession>(),\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "B<key_encryptsession>(),\n"
msgstr "B<key_encryptsession>(),\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "B<key_gendes>(),\n"
msgstr "B<key_gendes>(),\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "B<key_setsecret>(),\n"
msgstr "B<key_setsecret>(),\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "B<key_secretkey_is_set>()"
msgstr "B<key_secretkey_is_set>()"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
