# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 16:55+0200\n"
"PO-Revision-Date: 2023-03-27 09:04+0200\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 2.4.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "duplocale"
msgstr "duplocale"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-05-03"
msgstr "3 mai 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "duplocale - duplicate a locale object"
msgstr "duplocale - Dupliquer un objet de paramètres régionaux"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>locale.hE<gt>>\n"
msgstr "B<#include E<lt>locale.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<locale_t duplocale(locale_t >I<locobj>B<);>\n"
msgstr "B<locale_t duplocale(locale_t >I<locobj>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<duplocale>():"
msgstr "B<duplocale>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _XOPEN_SOURCE E<gt>= 700\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.10 :\n"
"        _XOPEN_SOURCE E<gt>= 700\n"
"    Avant la glibc 2.10 :\n"
"        _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<duplocale>()  function creates a duplicate of the locale object "
"referred to by I<locobj>."
msgstr ""
"La fonction B<duplocale>() créée un duplicata de l’objet de paramètres "
"régionaux référencé par I<locobj>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<locobj> is B<LC_GLOBAL_LOCALE>, B<duplocale>()  creates a locale object "
"containing a copy of the global locale determined by B<setlocale>(3)."
msgstr ""
"Si I<locobj> est B<LC_GLOBAL_LOCALE>, B<duplocale>() crée un objet de "
"paramètres régionaux contenant un copie des paramètres régionaux génériques "
"déterminés par B<setlocale>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, B<duplocale>()  returns a handle for the new locale object.  On "
"error, it returns I<(locale_t)\\ 0>, and sets I<errno> to indicate the error."
msgstr ""
"En cas de réussite, B<duplocale>() renvoie un identifiant pour le nouvel "
"objet de paramètres régionaux. En cas d’erreur, il renvoie I<(locale_t)\\ 0> "
"et définit I<errno> pour indiquer l’erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient memory to create the duplicate locale object."
msgstr ""
"Pas assez de mémoire pour créer l’objet de paramètres régionaux dupliqué."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.3."
msgstr "glibc 2.3."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Duplicating a locale can serve the following purposes:"
msgstr ""
"La duplication de paramètres régionaux peut avoir les utilités suivantes :"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "-"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To create a copy of a locale object in which one of more categories are to "
"be modified (using B<newlocale>(3))."
msgstr ""
"créer une copie de l’objet de paramètres régionaux dans lequel au moins une "
"catégorie est à modifier (en utilisant B<newlocale>(3)) ;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To obtain a handle for the current locale which can used in other functions "
"that employ a locale handle, such as B<toupper_l>(3).  This is done by "
"applying B<duplocale>()  to the value returned by the following call:"
msgstr ""
"obtenir un identifiant pour les paramètres régionaux actuels qui peut servir "
"dans d’autres fonctions permettant d’utiliser un identifiant de paramètres "
"régionaux, comme B<toupper_l>(3). C’est réalisé en appliquant B<duplocale>() "
"à la valeur renvoyée par les appels suivants :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "loc = uselocale((locale_t) 0);\n"
msgstr "loc = uselocale((locale_t) 0);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This technique is necessary, because the above B<uselocale>(3)  call may "
"return the value B<LC_GLOBAL_LOCALE>, which results in undefined behavior if "
"passed to functions such as B<toupper_l>(3).  Calling B<duplocale>()  can be "
"used to ensure that the B<LC_GLOBAL_LOCALE> value is converted into a usable "
"locale object.  See EXAMPLES, below."
msgstr ""
"Cette technique est nécessaire, parce que l’appel B<uselocale>(3) précédent "
"pourrait renvoyer la valeur B<LC_GLOBAL_LOCALE>, avec pour conséquence un "
"comportement non défini si passé à des fonctions comme B<toupper_l>(3). Un "
"appel de B<duplocale>() permet de s’assurer que la valeur "
"B<LC_GLOBAL_LOCALE> est convertie en objet de paramètres régionaux "
"utilisable. Consultez B<EXEMPLES> ci-dessous."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Each locale object created by B<duplocale>()  should be deallocated using "
"B<freelocale>(3)."
msgstr ""
"Chaque objet de paramètres régionaux créé par B<duplocale>() devrait être "
"désalloué en utilisant B<freelocale>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program below uses B<uselocale>(3)  and B<duplocale>()  to obtain a "
"handle for the current locale which is then passed to B<toupper_l>(3).  The "
"program takes one command-line argument, a string of characters that is "
"converted to uppercase and displayed on standard output.  An example of its "
"use is the following:"
msgstr ""
"Le programme suivant utilise B<uselocale>(3) et B<duplocale>() pour obtenir "
"un identifiant pour les paramètres régionaux actuels qui sont ensuite passés "
"à B<toupper_l>(3). Le programme prend un argument en ligne de commande, une "
"chaîne de caractères qui est convertie en majuscule et affichée sur la "
"sortie standard. Voici un exemple d’utilisation :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./a.out abc>\n"
"ABC\n"
msgstr ""
"$ B<./a.out abc>\n"
"ABC\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ""
"#define _XOPEN_SOURCE 700\n"
"#include E<lt>ctype.hE<gt>\n"
"#include E<lt>locale.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \\e\n"
"                        } while (0)\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    locale_t loc, nloc;\n"
"\\&\n"
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s string\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* This sequence is necessary, because uselocale() might return\n"
"       the value LC_GLOBAL_LOCALE, which can\\[aq]t be passed as an\n"
"       argument to toupper_l(). */\n"
"\\&\n"
"    loc = uselocale((locale_t) 0);\n"
"    if (loc == (locale_t) 0)\n"
"        errExit(\"uselocale\");\n"
"\\&\n"
"    nloc = duplocale(loc);\n"
"    if (nloc == (locale_t) 0)\n"
"        errExit(\"duplocale\");\n"
"\\&\n"
"    for (char *p = argv[1]; *p; p++)\n"
"        putchar(toupper_l(*p, nloc));\n"
"\\&\n"
"    printf(\"\\en\");\n"
"\\&\n"
"    freelocale(nloc);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. #-#-#-#-#  archlinux: duplocale.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: duplocale.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: duplocale.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-39: duplocale.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: duplocale.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: duplocale.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-6: duplocale.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: duplocale.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<freelocale>(3), B<newlocale>(3), B<setlocale>(3), B<uselocale>(3), "
"B<locale>(5), B<locale>(7)"
msgstr ""
"B<freelocale>(3), B<newlocale>(3), B<setlocale>(3), B<uselocale>(3), "
"B<locale>(5), B<locale>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid "The B<duplocale>()  function were added in glibc 2.3."
msgstr "La fonction B<duplocale>() a été ajooutée dans la glibc 2.3."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _XOPEN_SOURCE 700\n"
"#include E<lt>ctype.hE<gt>\n"
"#include E<lt>locale.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#define _XOPEN_SOURCE 700\n"
"#include E<lt>ctype.hE<gt>\n"
"#include E<lt>locale.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \\e\n"
"                        } while (0)\n"
msgstr ""
"#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \\e\n"
"                        } while (0)\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    locale_t loc, nloc;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    locale_t loc, nloc;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s string\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Utilisation : %s chaîne\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    /* This sequence is necessary, because uselocale() might return\n"
"       the value LC_GLOBAL_LOCALE, which can\\[aq]t be passed as an\n"
"       argument to toupper_l(). */\n"
msgstr ""
"    /* Cette suite est nécessaire, parce que uselocale() pourrait\n"
"       renvoyer la valeur LC_GLOBAL_LOCALE, qui ne peut pas être\n"
"       passée comme un argument à toupper_l() */\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    loc = uselocale((locale_t) 0);\n"
"    if (loc == (locale_t) 0)\n"
"        errExit(\"uselocale\");\n"
msgstr ""
"    loc = uselocale((locale_t) 0);\n"
"    if (loc == (locale_t) 0)\n"
"        errExit(\"uselocale\");\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    nloc = duplocale(loc);\n"
"    if (nloc == (locale_t) 0)\n"
"        errExit(\"duplocale\");\n"
msgstr ""
"    nloc = duplocale(loc);\n"
"    if (nloc == (locale_t) 0)\n"
"        errExit(\"duplocale\");\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    for (char *p = argv[1]; *p; p++)\n"
"        putchar(toupper_l(*p, nloc));\n"
msgstr ""
"    for (char *p = argv[1]; *p; p++)\n"
"        putchar(toupper_l(*p, nloc));\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "    printf(\"\\en\");\n"
msgstr "    printf(\"\\en\");\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "    freelocale(nloc);\n"
msgstr "    freelocale(nloc);\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "DUPLOCALE"
msgstr "DUPLOCALE"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Depuis la glibc 2.10 :"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_XOPEN_SOURCE\\ E<gt>=\\ 700"
msgstr "_XOPEN_SOURCE\\ E<gt>=\\ 700"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "Avant la glibc 2.10 :"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_GNU_SOURCE"
msgstr "_GNU_SOURCE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, B<duplocale>()  returns a handle for the new locale object.  On "
"error, it returns I<(locale_t)\\ 0>, and sets I<errno> to indicate the cause "
"of the error."
msgstr ""
"En cas de réussite, B<duplocale>() renvoie un identifiant pour le nouvel "
"objet de paramètres régionaux. En cas d’erreur, il renvoie I<(locale_t)\\ 0> "
"et définit I<errno> pour indiquer la cause de l’erreur."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<duplocale>()  function first appeared in version 2.3 of the GNU C "
"library."
msgstr ""
"La fonction B<duplocale>() est apparue pour la première fois dans la "
"version 2.3 de la bibliothèque C de GNU."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    loc = uselocale((locale_t) 0);\n"
msgstr "    loc = uselocale((locale_t) 0);\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This technique is necessary, because the above B<uselocale>(3)  call may "
"return the value B<LC_GLOBAL_LOCALE>, which results in undefined behavior if "
"passed to functions such as B<toupper_l>(3).  Calling B<duplocale>()  can be "
"used to ensure that the B<LC_GLOBAL_LOCALE> value is converted into a usable "
"locale object.  See EXAMPLE, below."
msgstr ""
"Cette technique est nécessaire, parce que l’appel B<uselocale>(3) précédent "
"pourrait renvoyer la valeur B<LC_GLOBAL_LOCALE>, avec pour conséquence un "
"comportement non défini si passé à des fonctions comme B<toupper_l>(3). Un "
"appel de B<duplocale>() permet de s’assurer que la valeur "
"B<LC_GLOBAL_LOCALE> est convertie en objet de paramètres régionaux "
"utilisable. Consultez B<EXEMPLE> ci-dessous."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLE"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"#define _XOPEN_SOURCE 700\n"
"#include E<lt>ctype.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>locale.hE<gt>\n"
msgstr ""
"#define _XOPEN_SOURCE 700\n"
"#include E<lt>ctype.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>locale.hE<gt>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    locale_t loc, nloc;\n"
"    char *p;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    locale_t loc, nloc;\n"
"    char *p;\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"    /* This sequence is necessary, because uselocale() might return\n"
"       the value LC_GLOBAL_LOCALE, which can\\(aqt be passed as an\n"
"       argument to toupper_l() */\n"
msgstr ""
"    /* Cette suite est nécessaire, parce que uselocale() pourrait\n"
"       renvoyer la valeur LC_GLOBAL_LOCALE, qui ne peut pas être\n"
"       passée comme un argument à toupper_l() */\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"    for (p = argv[1]; *p; p++)\n"
"        putchar(toupper_l(*p, nloc));\n"
msgstr ""
"    for (p = argv[1]; *p; p++)\n"
"        putchar(toupper_l(*p, nloc));\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
