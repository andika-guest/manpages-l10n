# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2012.
# David Prévot <david@tilapin.org>, 2010, 2012.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:03+0200\n"
"PO-Revision-Date: 2023-03-18 01:38+0100\n"
"Last-Translator: Grégoire Scano <gregoire.scano@malloc.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "isgreater"
msgstr "isgreater"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 juillet 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"isgreater, isgreaterequal, isless, islessequal, islessgreater, isunordered - "
"floating-point relational tests without exception for NaN"
msgstr ""
"isgreater, isgreaterequal, isless, islessequal, islessgreater, isunordered - "
"Relations d'ordre pour nombre à virgule flottante, sans exception pour NaN"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Bibliothèque de math (I<libm>, I<-lm>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int isgreater(>I<x>B<, >I<y>B<);>\n"
"B<int isgreaterequal(>I<x>B<, >I<y>B<);>\n"
"B<int isless(>I<x>B<, >I<y>B<);>\n"
"B<int islessequal(>I<x>B<, >I<y>B<);>\n"
"B<int islessgreater(>I<x>B<, >I<y>B<);>\n"
"B<int isunordered(>I<x>B<, >I<y>B<);>\n"
msgstr ""
"B<int isgreater(>I<x>B<, >I<y>B<);>\n"
"B<int isgreaterequal(>I<x>B<, >I<y>B<);>\n"
"B<int isless(>I<x>B<, >I<y>B<);>\n"
"B<int islessequal(>I<x>B<, >I<y>B<);>\n"
"B<int islessgreater(>I<x>B<, >I<y>B<);>\n"
"B<int isunordered(>I<x>B<, >I<y>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    All functions described here:\n"
"        _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr ""
"    Toutes les fonctions décrites ici :\n"
"        _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The normal relational operations (like B<E<lt>>, \"less than\")  fail if one "
"of the operands is NaN.  This will cause an exception.  To avoid this, C99 "
"defines the macros listed below."
msgstr ""
"Les opérateurs relationnels usuels (comme B<E<lt>>, « inférieur à ») "
"échouent si l'un des paramètres vaut NaN (I<Not a Number>, ce qui signifie "
"« pas un nombre »). Cela déclenchera une exception. Pour l'éviter, C99 "
"définit les macros listées ci-dessous."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These macros are guaranteed to evaluate their arguments only once.  The "
"arguments must be of real floating-point type (note: do not pass integer "
"values as arguments to these macros, since the arguments will I<not> be "
"promoted to real-floating types)."
msgstr ""
"Ces macros garantissent de n'évaluer leurs paramètres qu'une seule fois. Les "
"paramètres peuvent être de n'importe quel type flottant réel (attention, il "
"ne faut pas passer des valeurs entières en paramètre de ces macros, ces "
"paramètres I<ne seront pas> convertis en type flottant réel)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<isgreater>()"
msgstr "B<isgreater>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"determines I<(x)\\ E<gt>\\ (y)> without an exception if I<x> or I<y> is NaN."
msgstr ""
"détermine si I<(x)\\ E<gt>\\ (y)> sans lever d'exception si I<x> ou I<y> "
"vaut NaN."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<isgreaterequal>()"
msgstr "B<isgreaterequal>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"determines I<(x)\\ E<gt>=\\ (y)> without an exception if I<x> or I<y> is NaN."
msgstr ""
"détermine si I<(x)\\ E<gt>=\\ (y)> sans lever d'exception si I<x> ou I<y> "
"vaut NaN."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<isless>()"
msgstr "B<isless>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"determines I<(x)\\ E<lt>\\ (y)> without an exception if I<x> or I<y> is NaN."
msgstr ""
"détermine si I<(x)\\ E<lt>\\ (y)> sans lever d'exception si I<x> ou I<y> "
"vaut NaN."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<islessequal>()"
msgstr "B<islessequal>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"determines I<(x)\\ E<lt>=\\ (y)> without an exception if I<x> or I<y> is NaN."
msgstr ""
"détermine si I<(x)\\ E<lt>=\\ (y)> sans lever d'exception si I<x> ou I<y> "
"vaut NaN."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<islessgreater>()"
msgstr "B<islessgreater>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"determines I<(x)\\ E<lt> (y) || (x) E<gt>\\ (y)> without an exception if "
"I<x> or I<y> is NaN.  This macro is not equivalent to I<x\\ !=\\ y> because "
"that expression is true if I<x> or I<y> is NaN."
msgstr ""
"détermine si I<(x)\\ E<lt> (y) || (x) E<gt>\\ (y)> sans lever d'exception si "
"I<x> ou I<y> vaut NaN. Cette macro n'est pas équivalente à I<x\\ !=\\ y> car "
"cette expression est vraie si I<x> ou I<y> vaut NaN."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<isunordered>()"
msgstr "B<isunordered>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"determines whether its arguments are unordered, that is, whether at least "
"one of the arguments is a NaN."
msgstr ""
"détermine si ses paramètres n'ont pas d'ordre, c'est-à-dire si au moins un "
"de ses paramètres est un NaN."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The macros other than B<isunordered>()  return the result of the relational "
"comparison; these macros return 0 if either argument is a NaN."
msgstr ""
"Les macros autres que B<isunordered>() renvoient le résultat de la relation "
"d'ordre ; ces macros renvoient B<0> si l'un des paramètres est un NaN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<isunordered>()  returns 1 if I<x> or I<y> is NaN and 0 otherwise."
msgstr "B<isunordered>() renvoie B<1> si I<x> ou I<y> vaut NaN et B<0> sinon."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "No errors occur."
msgstr "Aucune erreur ne survient."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<isgreater>(),\n"
"B<isgreaterequal>(),\n"
"B<isless>(),\n"
"B<islessequal>(),\n"
"B<islessgreater>(),\n"
"B<isunordered>()"
msgstr ""
"B<isgreater>(),\n"
"B<isgreaterequal>(),\n"
"B<isless>(),\n"
"B<islessequal>(),\n"
"B<islessgreater>(),\n"
"B<isunordered>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Not all hardware supports these functions, and where hardware support isn't "
"provided, they will be emulated by macros.  This will result in a "
"performance penalty.  Don't use these functions if NaN is of no concern for "
"you."
msgstr ""
"Tous les matériels ne supportent pas ces fonctions, et quand une gestion "
"matérielle n'est pas fournie, elles sont émulées par des macros. Cela "
"diminue les performances. Ne les utilisez pas si NaN ne vous concerne pas."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, C99."
msgstr "POSIX.1-2001, C99."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<fpclassify>(3), B<isnan>(3)"
msgstr "B<fpclassify>(3), B<isnan>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15 décembre 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "ISGREATER"
msgstr "ISGREATER"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int isgreater(>I<x>B<, >I<y>B<);>\n"
msgstr "B<int isgreater(>I<x>B<, >I<y>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int isgreaterequal(>I<x>B<, >I<y>B<);>\n"
msgstr "B<int isgreaterequal(>I<x>B<, >I<y>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int isless(>I<x>B<, >I<y>B<);>\n"
msgstr "B<int isless(>I<x>B<, >I<y>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int islessequal(>I<x>B<, >I<y>B<);>\n"
msgstr "B<int islessequal(>I<x>B<, >I<y>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int islessgreater(>I<x>B<, >I<y>B<);>\n"
msgstr "B<int islessgreater(>I<x>B<, >I<y>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int isunordered(>I<x>B<, >I<y>B<);>\n"
msgstr "B<int isunordered(>I<x>B<, >I<y>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Link with I<-lm>."
msgstr "Éditer les liens avec I<-lm>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "All functions described here:"
msgstr "Toutes les fonctions définies ici :"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"
msgstr "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
