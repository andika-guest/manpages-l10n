# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999,2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2012-2014.
# Denis Barbier <barbier@debian.org>, 2006,2010.
# David Prévot <david@tilapin.org>, 2010-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:07+0200\n"
"PO-Revision-Date: 2023-05-05 17:24+0200\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "mcheck"
msgstr "mcheck"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 juillet 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"mcheck, mcheck_check_all, mcheck_pedantic, mprobe - heap consistency checking"
msgstr ""
"mcheck, mcheck_check_all, mcheck_pedantic, mprobe - Vérifier la cohérence du "
"tas"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>mcheck.hE<gt>>\n"
msgstr "B<#include E<lt>mcheck.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int mcheck(void (*>I<abortfunc>B<)(enum mcheck_status >I<mstatus>B<));>\n"
"B<int mcheck_pedantic(void (*>I<abortfunc>B<)(enum mcheck_status >I<mstatus>B<));>\n"
"B<void mcheck_check_all(void);>\n"
msgstr ""
"B<int mcheck(void (*>I<abortfunc>B<)(enum mcheck_status >I<mstatus>B<));>\n"
"B<int mcheck_pedantic(void (*>I<abortfunc>B<)(enum mcheck_status >I<mstatus>B<));>\n"
"B<void mcheck_check_all(void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<enum mcheck_status mprobe(void *>I<ptr>B<);>\n"
msgstr "B<enum mcheck_status mprobe(void *>I<ptr>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<mcheck>()  function installs a set of debugging hooks for the "
"B<malloc>(3)  family of memory-allocation functions.  These hooks cause "
"certain consistency checks to be performed on the state of the heap.  The "
"checks can detect application errors such as freeing a block of memory more "
"than once or corrupting the bookkeeping data structures that immediately "
"precede a block of allocated memory."
msgstr ""
"La fonction B<mcheck>() installe des fonctions de rappel (« hook ») pour les "
"fonctions d'allocation mémoire B<malloc>(3). Ces fonctions activent des "
"vérifications de cohérence de l'état du tas. Ces vérifications permettent de "
"détecter des erreurs telles que libérer plusieurs fois un bloc de mémoire, "
"ou corrompre les structures internes précédant le bloc de mémoire allouée."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To be effective, the B<mcheck>()  function must be called before the first "
"call to B<malloc>(3)  or a related function.  In cases where this is "
"difficult to ensure, linking the program with I<-lmcheck> inserts an "
"implicit call to B<mcheck>()  (with a NULL argument)  before the first call "
"to a memory-allocation function."
msgstr ""
"Pour être active, la fonction B<mcheck>() doit être appelée avant le premier "
"appel à B<malloc>(3) ou autre fonction associée. S'il est difficile de s'en "
"assurer, lier le programme avec I<-lmcheck> ajoute un appel implicite à "
"B<mcheck>() (avec un paramètre NULL) avant le premier appel à une fonction "
"d'allocation mémoire."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<mcheck_pedantic>()  function is similar to B<mcheck>(), but performs "
"checks on all allocated blocks whenever one of the memory-allocation "
"functions is called.  This can be very slow!"
msgstr ""
"La fonction B<mcheck_pedantic>() est similaire à B<mcheck>(), mais effectue "
"les vérifications pour tous les blocs alloués quelque soit la fonction "
"d'allocation mémoire appelée. Cela peut être très lent !"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<mcheck_check_all>()  function causes an immediate check on all "
"allocated blocks.  This call is effective only if B<mcheck>()  is called "
"beforehand."
msgstr ""
"La fonction B<mcheck_check_all>() déclenche une vérification immédiate de "
"tous les blocs alloués. Cet appel n'est pris en compte que si B<mcheck>() a "
"déjà été appelé."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the system detects an inconsistency in the heap, the caller-supplied "
"function pointed to by I<abortfunc> is invoked with a single argument, "
"I<mstatus>, that indicates what type of inconsistency was detected.  If "
"I<abortfunc> is NULL, a default function prints an error message on "
"I<stderr> and calls B<abort>(3)."
msgstr ""
"Si le système détecte une incohérence dans le tas, la fonction fournie lors "
"de l'appel dans I<abortfunc> est invoquée avec un paramètre unique, "
"I<mstatus>, qui indique le type d'incohérence détectée. Si I<abortfunc> est "
"NULL, la fonction par défaut affiche un message d'erreur sur I<stderr> et "
"appelle B<abort>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<mprobe>()  function performs a consistency check on the block of "
"allocated memory pointed to by I<ptr>.  The B<mcheck>()  function should be "
"called beforehand (otherwise B<mprobe>()  returns B<MCHECK_DISABLED>)."
msgstr ""
"La fonction B<mprobe>() effectue une vérification de cohérence du bloc de "
"mémoire allouée pointé par I<ptr>. La fonction B<mcheck>() doit avoir été "
"déjà appelée, ou B<mprobe>() renverra B<MCHECK_DISABLED>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following list describes the values returned by B<mprobe>()  or passed "
"as the I<mstatus> argument when I<abortfunc> is invoked:"
msgstr ""
"La liste suivante décrit les valeurs renvoyées par B<mprobe>() ou passées "
"via le paramètre I<mstatus> lors de l'appel à I<mstatus>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<MCHECK_DISABLED> (B<mprobe>() only)"
msgstr "B<MCHECK_DISABLED> (seulement pour B<mprobe>())"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mcheck>()  was not called before the first memory allocation function was "
"called.  Consistency checking is not possible."
msgstr ""
"La vérification de cohérence n'a pu être effectuée, car B<mcheck>() n'a pas "
"été appelé avant le premier appel à une fonction d'allocation mémoire."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<MCHECK_OK> (B<mprobe>() only)"
msgstr "B<MCHECK_OK> (seulement pour B<mprobe>())"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "No inconsistency detected."
msgstr "Aucune incohérence détectée."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<MCHECK_HEAD>"
msgstr "B<MCHECK_HEAD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Memory preceding an allocated block was clobbered."
msgstr "La mémoire précédant immédiatement le bloc alloué a été altérée."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<MCHECK_TAIL>"
msgstr "B<MCHECK_TAIL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Memory following an allocated block was clobbered."
msgstr "La mémoire suivant immédiatement le bloc alloué a été altérée."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<MCHECK_FREE>"
msgstr "B<MCHECK_FREE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "A block of memory was freed twice."
msgstr "Un bloc de mémoire a été libéré deux fois."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mcheck>()  and B<mcheck_pedantic>()  return 0 on success, or -1 on error."
msgstr ""
"B<mcheck>() et B<mcheck_pedantic>() renvoient 0 en cas de succès, -1 en cas "
"d'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<mcheck>(),\n"
"B<mcheck_pedantic>(),\n"
"B<mcheck_check_all>(),\n"
"B<mprobe>()"
msgstr ""
"B<mcheck>(),\n"
"B<mcheck_pedantic>(),\n"
"B<mcheck_check_all>(),\n"
"B<mprobe>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"MT-Unsafe race:mcheck\n"
"const:malloc_hooks"
msgstr ""
"MT-Unsafe race:mcheck\n"
"const:malloc_hooks"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<mcheck_pedantic>()"
msgstr "B<mcheck_pedantic>()"

#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<mcheck_check_all>()"
msgstr "B<mcheck_check_all>()"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.2."
msgstr "glibc 2.2."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<mcheck>()"
msgstr "B<mcheck>()"

#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<mprobe>()"
msgstr "B<mprobe>()"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.0."
msgstr "glibc 2.0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#.  But is MALLOC_CHECK_ slower?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Linking a program with I<-lmcheck> and using the B<MALLOC_CHECK_> "
"environment variable (described in B<mallopt>(3))  cause the same kinds of "
"errors to be detected.  But, using B<MALLOC_CHECK_> does not require the "
"application to be relinked."
msgstr ""
"Lier le programme avec I<-lmcheck> comme utiliser la variable "
"d'environnement B<MALLOC_CHECK_> (décrite dans B<mallopt>(3)) permettent de "
"détecter le même type d'erreurs. Cependant, B<MALLOC_CHECK_> ne nécessite "
"pas de modifier l'édition de liens."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program below calls B<mcheck>()  with a NULL argument and then frees the "
"same block of memory twice.  The following shell session demonstrates what "
"happens when running the program:"
msgstr ""
"L'exemple ci-dessous montre ce qu'il se passe lorsqu'un programme appelle "
"B<mcheck>() avec un paramètre NULL puis libère le même bloc de mémoire deux "
"fois."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, fuzzy, no-wrap
#| msgid ""
#| "About to free a second time\n"
#| "block freed twice\n"
#| "Aborted (core dumped)\n"
msgid ""
"$B< ./a.out>\n"
"About to free\n"
"\\&\n"
"About to free a second time\n"
"block freed twice\n"
"Aborted (core dumped)\n"
msgstr ""
"About to free a second time\n"
"block freed twice\n"
"Abandon\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ""
"#include E<lt>mcheck.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    char *p;\n"
"\\&\n"
"    if (mcheck(NULL) != 0) {\n"
"        fprintf(stderr, \"mcheck() failed\\en\");\n"
"\\&\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    p = malloc(1000);\n"
"\\&\n"
"    fprintf(stderr, \"About to free\\en\");\n"
"    free(p);\n"
"    fprintf(stderr, \"\\enAbout to free a second time\\en\");\n"
"    free(p);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. #-#-#-#-#  archlinux: mcheck.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: mcheck.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: mcheck.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-39: mcheck.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: mcheck.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: mcheck.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-6: mcheck.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: mcheck.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<malloc>(3), B<mallopt>(3), B<mtrace>(3)"
msgstr "B<malloc>(3), B<mallopt>(3), B<mtrace>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15 décembre 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"The B<mcheck_pedantic>()  and B<mcheck_check_all>()  functions are available "
"since glibc 2.2.  The B<mcheck>()  and B<mprobe>()  functions are present "
"since at least glibc 2.0"
msgstr ""
"Les fonctions B<mcheck_pedantic>() et B<mcheck_check_all>() sont disponibles "
"depuis la glibc 2.2. Les fonctions B<mcheck>() et B<mprobe>() sont "
"disponibles depuis au moins la glibc 2.0."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "These functions are GNU extensions."
msgstr "Ces fonctions sont des extensions GNU."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out>\n"
"About to free\n"
msgstr ""
"$B< ./a.out>\n"
"About to free\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"About to free a second time\n"
"block freed twice\n"
"Aborted (core dumped)\n"
msgstr ""
"About to free a second time\n"
"block freed twice\n"
"Abandon\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>mcheck.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#include E<lt>mcheck.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    char *p;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    char *p;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (mcheck(NULL) != 0) {\n"
"        fprintf(stderr, \"mcheck() failed\\en\");\n"
msgstr ""
"    if (mcheck(NULL) != 0) {\n"
"        fprintf(stderr, \"échec de mcheck()\\en\");\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "    p = malloc(1000);\n"
msgstr "    p = malloc(1000);\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    fprintf(stderr, \"About to free\\en\");\n"
"    free(p);\n"
"    fprintf(stderr, \"\\enAbout to free a second time\\en\");\n"
"    free(p);\n"
msgstr ""
"    fprintf(stderr, \"About to free\\en\");\n"
"    free(p);\n"
"    fprintf(stderr, \"\\enAbout to free a second time\\en\");\n"
"    free(p);\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "MCHECK"
msgstr "MCHECK"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int mcheck(void (*>I<abortfunc>B<)(enum mcheck_status >I<mstatus>B<));>\n"
msgstr "B<int mcheck(void (*>I<abortfunc>B<)(enum mcheck_status >I<mstatus>B<));>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int mcheck_pedantic(void (*>I<abortfunc>B<)(enum mcheck_status >I<mstatus>B<));>\n"
msgstr "B<int mcheck_pedantic(void (*>I<abortfunc>B<)(enum mcheck_status >I<mstatus>B<));>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void mcheck_check_all(void);>\n"
msgstr "B<void mcheck_check_all(void);>\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<mcheck>(),\n"
"B<mcheck_pedantic>(),\n"
msgstr ""
"B<mcheck>(),\n"
"B<mcheck_pedantic>(),\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<mcheck_check_all>(),\n"
"B<mprobe>()"
msgstr ""
"B<mcheck_check_all>(),\n"
"B<mprobe>()"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "MT-Unsafe race:mcheck\n"
msgstr "MT-Unsafe race:mcheck\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "const:malloc_hooks"
msgstr "const:malloc_hooks"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLE"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>mcheck.hE<gt>\n"
msgstr ""
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>mcheck.hE<gt>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char *p;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char *p;\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
