# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012, 2013.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2011, 2012, 2013.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2021-2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:17+0200\n"
"PO-Revision-Date: 2023-01-14 10:01+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "s390_runtime_instr"
msgstr "s390_runtime_instr"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "s390_runtime_instr - enable/disable s390 CPU run-time instrumentation"
msgstr ""
"s390_runtime_instr - Activer ou désactiver l’instrumentation de l'activité "
"du processeur s390"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>asm/runtime_instr.hE<gt>> /* Definition of B<S390_*> constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>>       /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>asm/runtime_instr.hE<gt>> /* Définition des constantes B<S390_*> */\n"
"B<#include E<lt>sys/syscall.hE<gt>>       /* Définition des constantes B<SYS_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int syscall(SYS_s390_runtime_instr, int >I<command>B<, int >I<signum>B<);>\n"
msgstr "B<int syscall(SYS_s390_runtime_instr, int >I<command>B<, int >I<signum>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<Note>: glibc provides no wrapper for B<s390_runtime_instr>(), "
"necessitating the use of B<syscall>(2)."
msgstr ""
"I<Remarque> : La glibc ne fournit pas d'enveloppe pour "
"B<s390_runtime_instr>() ; appelez-la en utilisant B<syscall>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<s390_runtime_instr>()  system call starts or stops CPU run-time "
"instrumentation for the calling thread."
msgstr ""
"L'appel système B<s390_runtime_instr>() démarre ou arrête l’instrumentation "
"de l'activité du processeur pour le thread appelant."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<command> argument controls whether run-time instrumentation is started "
"(B<S390_RUNTIME_INSTR_START>, 1) or stopped (B<S390_RUNTIME_INSTR_STOP>, 2) "
"for the calling thread."
msgstr ""
"L'argument I<command> décide du démarrage (B<S390_RUNTIME_INSTR_START>, 1) "
"ou de l'arrêt (B<S390_RUNTIME_INSTR_STOP>, 2) de l’instrumentation lors de "
"l'exécution du thread appelant."

#.  commit b38feccd663b55ab07116208b68e1ffc7c3c7e78
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The I<signum> argument specifies the number of a real-time signal.  This "
"argument was used to specify a signal number that should be delivered to the "
"thread if the run-time instrumentation buffer was full or if the run-time-"
"instrumentation-halted interrupt had occurred.  This feature was never used, "
"and in Linux 4.4 support for this feature was removed; thus, in current "
"kernels, this argument is ignored."
msgstr ""
"L'argument I<signum> précise le numéro du signal temps-réel. Cet argument "
"était utilisé pour indiquer un numéro de signal à délivrer au thread si le "
"tampon d’instrumentation était plein ou si une interruption « run-time-"
"instrumentation-halted » était survenue. Cette fonctionnalité n’a jamais été "
"utilisée et sa prise en charge a été supprimée dans Linux 4.4. Par "
"conséquent, dans les noyaux actuels, cet argument est ignoré."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, B<s390_runtime_instr>()  returns 0 and enables the thread for "
"run-time instrumentation by assigning the thread a default run-time "
"instrumentation control block.  The caller can then read and modify the "
"control block and start the run-time instrumentation.  On error, -1 is "
"returned and I<errno> is set to indicate the error."
msgstr ""
"S'il réussit, l'appel B<s390_runtime_instr>() renvoie la valeur B<0> et "
"permet au thread l’instrumentation de l’exécution en lui assignant un bloc "
"de contrôle d’instrumentation par défaut. Le composant appelant peut alors "
"accéder en lecture ou modifier le bloc de contrôle et démarrer "
"l’instrumentation. S'il échoue, l'appel renvoie B<-1> et I<errno> est "
"positionné pour indiquer l'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The value specified in I<command> is not a valid command."
msgstr "La valeur spécifiée dans  I<command> n'est pas une commande valable."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The value specified in I<signum> is not a real-time signal number.  From "
"Linux 4.4 onwards, the I<signum> argument has no effect, so that an invalid "
"signal number will not result in an error."
msgstr ""
"La valeur indiquée dans I<signum> n'est pas un numéro de signal temps réel. "
"À partir de Linux 4.4, l'argument I<signum> n'a pas d'effet, si bien qu'un "
"numéro de signal non valable n’engendrera pas une erreur."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Allocating memory for the run-time instrumentation control block failed."
msgstr ""
"L'allocation de la mémoire pour le bloc de contrôle d’instrumentation a "
"échoué."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr "B<EOPNOTSUPP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The run-time instrumentation facility is not available."
msgstr "Le mécanisme d’instrumentation d'exécution n'est pas disponible."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "Linux notes"
msgid "Linux on s390."
msgstr "Notes pour Linux"

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux 3.7.  System z EC12."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#.  commit df2f815a7df7edb5335a3bdeee6a8f9f6f9c35c4
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The I<asm/runtime_instr.h> header file is available since Linux 4.16."
msgstr ""
"Le fichier d'en-tête I<asm/runtime_instr.h> est disponible depuis Linux 4.16."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Starting with Linux 4.4, support for signalling was removed, as was the "
"check whether I<signum> is a valid real-time signal.  For backwards "
"compatibility with older kernels, it is recommended to pass a valid real-"
"time signal number in I<signum> and install a handler for that signal."
msgstr ""
"À partir de Linux 4.4, la prise en charge des signaux a été supprimée ainsi "
"que la vérification si I<signum> est un signal temps réel valable. Pour une "
"rétro-compatibilité avec les anciens noyaux, il est recommandé de fournir un "
"numéro de signal temps réel valable dans I<signum> et d'installer un "
"gestionnaire pour ce signal."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<syscall>(2), B<signal>(7)"
msgstr "B<syscall>(2), B<signal>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "30 octobre 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "This system call is available since Linux 3.7."
msgstr "Cet appel système est disponible depuis Linux 3.7."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This Linux-specific system call is available only on the s390 architecture.  "
"The run-time instrumentation facility is available beginning with System z "
"EC12."
msgstr ""
"L'appel système spécifique à Linux n'est disponible que pour l'architecture "
"s390. Le mécanisme d’instrumentation d'exécution est disponible à partir "
"d’EC12 de System z."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "S390_RUNTIME_INSTR"
msgstr "S390_RUNTIME_INSTR"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<#include E<lt>asm/runtime_instr.hE<gt>>\n"
msgstr "B<#include E<lt>asm/runtime_instr.hE<gt>>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int s390_runtime_instr(int >I<command>B<, int >I<signum>B<);>\n"
msgstr "B<int s390_runtime_instr(int >I<command>B<, int >I<signum>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The I<signum> argument specifies the number of a real-time signal.  The real-"
"time signal is sent to the thread if the run-time instrumentation buffer is "
"full or if the run-time-instrumentation-halted interrupt occurred."
msgstr ""
"L'argument I<signum> précise le numéro du signal temps-réel. Le signal temps-"
"réel est envoyé au thread si le tampon d’instrumentation est plein ou si "
"l'interruption « run-time-instrumentation-halted » est survenue."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, B<s390_runtime_instr>()  returns 0 and enables the thread for "
"run-time instrumentation by assigning the thread a default run-time "
"instrumentation control block.  The caller can then read and modify the "
"control block and start the run-time instrumentation.  On error, -1 is "
"returned and I<errno> is set to one of the error codes listed below."
msgstr ""
"S'il réussit, l'appel B<s390_runtime_instr>() renvoie la valeur B<0> et "
"active le thread pour l’instrumentation d'exécution en lui assignant un bloc "
"de contrôle d’instrumentation par défaut. Le composant appelant peut alors "
"accéder en lecture ou modifier le bloc de contrôle et démarrer "
"l’instrumentation. S'il échoue, l'appel retourne B<-1> et I<errno> prend "
"pour valeur l'un des codes erreur décrits plus bas."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The value specified in I<command> is not a valid command or the value "
"specified in I<signum> is not a real-time signal number."
msgstr ""
"La valeur spécifiée dans I<command> n'est pas une commande valable ou la "
"valeur spécifiée dans I<signum> n'est pas un numéro de signal temps-réel."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Glibc does not provide a wrapper for this system call, use B<syscall>(2)  to "
"call it."
msgstr ""
"La glibc ne fournit pas d'enveloppe pour cette appel système, utillisez "
"B<syscall>(2) pour l'appeler."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
