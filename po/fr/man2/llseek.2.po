# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:05+0200\n"
"PO-Revision-Date: 2023-03-03 22:30+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "_llseek"
msgstr "_llseek"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "_llseek - reposition read/write file offset"
msgstr "_llseek - Positionner la tête de lecture/écriture dans un fichier"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/syscall.hE<gt>>      /* Définition des constantes B<SYS_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int syscall(SYS__llseek, unsigned int >I<fd>B<, unsigned long >I<offset_high>B<,>\n"
"B<            unsigned long >I<offset_low>B<, loff_t *>I<result>B<,>\n"
"B<            unsigned int >I<whence>B<);>\n"
msgstr ""
"B<int syscall(SYS__llseek, unsigned int >I<fd>B<, unsigned long >I<offset_high>B<,>\n"
"B<            unsigned long >I<offset_low>B<, loff_t *>I<result>B<,>\n"
"B<            unsigned int >I<whence>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<Note>: glibc provides no wrapper for B<_llseek>(), necessitating the use "
"of B<syscall>(2)."
msgstr ""
"I<Remarque> : la glibc ne fournit par d'enveloppe pour B<_llseek>(), "
"imposant l'utilisation de B<syscall>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Note: for information about the B<llseek>(3)  library function, see "
"B<lseek64>(3)."
msgstr ""
"Remarque : pour des informations sur la fonction de bibliothèque "
"B<llseek>(3), voir B<lseek64>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<_llseek>()  system call repositions the offset of the open file "
"description associated with the file descriptor I<fd> to the value"
msgstr ""
"L'appel système B<_llseek>() remet la position de la description du fichier "
"ouvert associée au descripteur de fichier I<fd> à la valeur"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "(offset_high E<lt>E<lt> 32) | offset_low"
msgstr "(offset_high E<lt>E<lt> 32) | offset_low"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This new offset is a byte offset relative to the beginning of the file, the "
"current file offset, or the end of the file, depending on whether I<whence> "
"is B<SEEK_SET>, B<SEEK_CUR>, or B<SEEK_END>, respectively."
msgstr ""
"Cette nouvelle position est un emplacement d'octet par rapport au début du "
"fichier, la position actuelle du fichier ou la fin du fichier, selon que "
"I<whence> est respectivement B<SEEK_SET>, B<SEEK_CUR> ou B<SEEK_END>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The new file offset is returned in the argument I<result>.  The type "
"I<loff_t> is a 64-bit signed type."
msgstr ""
"La nouvelle position du fichier est renvoyée dans le paramètre I<result>. Le "
"type I<loff_t> est un type signé en 64 bits."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This system call exists on various 32-bit platforms to support seeking to "
"large file offsets."
msgstr ""
"Cet appel système existe sur diverses plateformes 32 bits pour gérer le "
"positionnement dans de gros fichiers."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Upon successful completion, B<_llseek>()  returns 0.  Otherwise, a value of "
"-1 is returned and I<errno> is set to indicate the error."
msgstr ""
"En cas de réussite, B<_llseek>() renvoie B<0>, sinon il renvoie B<-1>, "
"auquel cas I<errno> contient le code d'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> is not an open file descriptor."
msgstr "I<fd> n'est pas un descripteur de fichier ouvert."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Problem with copying results to user space."
msgstr "Problème lors de la copie des résultats vers l'espace utilisateur."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<whence> is invalid."
msgstr "I<whence> n’est pas valable."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "You probably want to use the B<lseek>(2)  wrapper function instead."
msgstr ""
"Vous voudrez sans doute utiliser plutôt la fonction enveloppe B<lseek>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<lseek>(2), B<open>(2), B<lseek64>(3)"
msgstr "B<lseek>(2), B<open>(2), B<lseek64>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-01-07"
msgstr "7 janvier 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This function is Linux-specific, and should not be used in programs intended "
"to be portable."
msgstr ""
"Cette fonction est spécifique à Linux et ne devrait pas être employée dans "
"des programmes destinés à être portables."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "LLSEEK"
msgstr "LLSEEK"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<int _llseek(unsigned int >I<fd>B<, unsigned long >I<offset_high>B<,>\n"
"B<            unsigned long >I<offset_low>B<, loff_t *>I<result>B<,>\n"
"B<            unsigned int >I<whence>B<);>\n"
msgstr ""
"B<int _llseek(unsigned int >I<fd>B<, unsigned long >I<offset_high>B<,>\n"
"B<            unsigned long >I<offset_low>B<, loff_t *>I<result>B<,>\n"
"B<            unsigned int >I<whence>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid "I<Note>: There is no glibc wrapper for this system call; see NOTES."
msgstr ""
"I<Note> : il n'existe pas d'enveloppe pour cet appel système dans la glibc ; "
"voir NOTES."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<_llseek>()  system call repositions the offset of the open file "
"description associated with the file descriptor I<fd> to "
"I<(offset_highE<lt>E<lt>32) | offset_low> bytes relative to the beginning of "
"the file, the current file offset, or the end of the file, depending on "
"whether I<whence> is B<SEEK_SET>, B<SEEK_CUR>, or B<SEEK_END>, "
"respectively.  It returns the resulting file position in the argument "
"I<result>."
msgstr ""
"L'appel système B<_llseek>() replace la description du fichier ouvert "
"associée au descripteur de fichier I<fd> à la position "
"I<(offset_highE<lt>E<lt>32) | offset_low> octets du début du fichier, à la "
"position actuelle, ou à la fin du fichier, selon que la valeur I<whence> est "
"B<SEEK_SET>, B<SEEK_CUR> ou B<SEEK_END>, respectivement. Il renvoie la "
"nouvelle position dans l'argument I<result>."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Glibc does not provide a wrapper for this system call.  To invoke it "
"directly, use B<syscall>(2).  However, you probably want to use the "
"B<lseek>(2)  wrapper function instead."
msgstr ""
"La glibc ne fournit pas d'enveloppe pour cet appel système. Pour l'appeler "
"directement, utilisez B<syscall>(2). Cependant, vous voudrez probablement "
"utiliser plutôt l'enveloppe B<lseek>(2)."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
