# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2011.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:03+0200\n"
"PO-Revision-Date: 2023-01-15 12:26+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ioctl_fideduperange"
msgstr "ioctl_fideduperange"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ioctl_fideduperange - share some the data of one file with another file"
msgstr ""
"ioctl_fideduperange - partager les données d'un fichier avec un autre fichier"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/fs.hE<gt>>      /* Definition of B<FIDEDUPERANGE> and\n"
"B<                              FILE_DEDUPE_* >constantsB<*/>\n"
"B<#include E<lt>sys/ioctl.hE<gt>>\n"
msgstr ""
"B<#include E<lt>linux/fs.hE<gt>>      /* Définition des constantes B<FIDEDUPERANGE>\n"
"                                et B<FILE_DEDUPE_* >B<*/>\n"
"B<#include E<lt>sys/stat.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >I<src_fd>B<, FIDEDUPERANGE, struct file_dedupe_range *>I<arg>B<);>\n"
msgstr "B<int ioctl(int >I<src_fd>B<, FIDEDUPERANGE, struct file_dedupe_range *>I<arg>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a filesystem supports files sharing physical storage between multiple "
"files, this B<ioctl>(2)  operation can be used to make some of the data in "
"the B<src_fd> file appear in the B<dest_fd> file by sharing the underlying "
"storage if the file data is identical (\"deduplication\").  Both files must "
"reside within the same filesystem.  This reduces storage consumption by "
"allowing the filesystem to store one shared copy of the data.  If a file "
"write should occur to a shared region, the filesystem must ensure that the "
"changes remain private to the file being written.  This behavior is commonly "
"referred to as \"copy on write\"."
msgstr ""
"Si un système de fichiers prend en charge le stockage physique du partage de "
"fichiers entre plusieurs fichiers (« reflink »), cette opération B<ioctl>(2) "
"peut être utilisée pour faire apparaître des données du fichier I<src_fd> "
"dans le fichier I<dest_fd> en partageant le stockage sous-jacent, si les "
"données du fichier sont identiques (« déduplication »). Les deux fichiers "
"doivent se trouver sur le même système de fichiers. Cela réduit la "
"consommation en stockage en permettant au système de ne stocker qu'une copie "
"partagée des données. Si une écriture de fichier doit avoir lieu sur une "
"région partagée, le système de fichiers doit garantir que les modifications "
"n’appartiennent qu’au fichier qui est écrit. On appelle généralement ce "
"comportement la « copie sur écriture »."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This ioctl performs the \"compare and share if identical\" operation on up "
"to I<src_length> bytes from file descriptor I<src_fd> at offset "
"I<src_offset>.  This information is conveyed in a structure of the following "
"form:"
msgstr ""
"Cet ioctl accomplit l'opération « comparer et partager si identique » "
"jusqu'à I<src_lenght> octets du descripteur de fichier I<src_fd> décalé de "
"I<src_offset>. Cette information est communiquée dans une structure de la "
"forme suivante :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct file_dedupe_range {\n"
"    __u64 src_offset;\n"
"    __u64 src_length;\n"
"    __u16 dest_count;\n"
"    __u16 reserved1;\n"
"    __u32 reserved2;\n"
"    struct file_dedupe_range_info info[0];\n"
"};\n"
msgstr ""
"struct file_dedupe_range {\n"
"    __u64 src_offset;\n"
"    __u64 src_length;\n"
"    __u16 dest_count;\n"
"    __u16 reserved1;\n"
"    __u32 reserved2;\n"
"    struct file_dedupe_range_info info[0];\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Deduplication is atomic with regards to concurrent writes, so no locks need "
"to be taken to obtain a consistent deduplicated copy."
msgstr ""
"La déduplication est atomique par rapport aux écritures concomitantes, vous "
"n'avez donc pas besoin de poser de verrous pour avoir une copie dédupliquée "
"cohérente."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The fields I<reserved1> and I<reserved2> must be zero."
msgstr "Les champs I<reserved1> et I<reserved2> doivent valoir zéro."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Destinations for the deduplication operation are conveyed in the array at "
"the end of the structure.  The number of destinations is given in "
"I<dest_count>, and the destination information is conveyed in the following "
"form:"
msgstr ""
"Les destinations de l'opération de déduplication se placent dans un tableau "
"à la fin de la structure. Le nombre de destinations est donné dans "
"I<dest_count> et les informations sur la destination sont transmises sous la "
"forme suivante :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct file_dedupe_range_info {\n"
"    __s64 dest_fd;\n"
"    __u64 dest_offset;\n"
"    __u64 bytes_deduped;\n"
"    __s32 status;\n"
"    __u32 reserved;\n"
"};\n"
msgstr ""
"struct file_dedupe_range_info {\n"
"    __s64 dest_fd;\n"
"    __u64 dest_offset;\n"
"    __u64 bytes_deduped;\n"
"    __s32 status;\n"
"    __u32 reserved;\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Each deduplication operation targets I<src_length> bytes in file descriptor "
"I<dest_fd> at offset I<dest_offset>.  The field I<reserved> must be zero.  "
"During the call, I<src_fd> must be open for reading and I<dest_fd> must be "
"open for writing.  The combined size of the struct I<file_dedupe_range> and "
"the struct I<file_dedupe_range_info> array must not exceed the system page "
"size.  The maximum size of I<src_length> is filesystem dependent and is "
"typically 16\\~MiB.  This limit will be enforced silently by the "
"filesystem.  By convention, the storage used by I<src_fd> is mapped into "
"I<dest_fd> and the previous contents in I<dest_fd> are freed."
msgstr ""
"Chaque opération de déduplication envoie I<src_length> octets dans le "
"descripteur de fichier I<dest_fd> à la position I<dest_offset>. Le champ "
"I<reserved> doit valoir zéro. Pendant l'appel, I<src_fd> doit être ouvert en "
"lecture et I<dest_fd> en écriture. La taille combinée du tableau des "
"structures I<file_dedupe_range> et I<file_dedupe_range_info> ne doit pas "
"dépasser la taille de la page du système. La taille maximale de "
"I<src_length> dépend du système de fichiers et est généralement de 16\\~Mio. "
"Cette limite sera augmentée silencieusement par le système de fichiers. Par "
"convention, le stockage utilisé par I<src_fd> est associé à I<dest_fd> et le "
"contenu précédent de I<dest_fd> est libéré."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Upon successful completion of this ioctl, the number of bytes successfully "
"deduplicated is returned in I<bytes_deduped> and a status code for the "
"deduplication operation is returned in I<status>.  If even a single byte in "
"the range does not match, the deduplication request will be ignored and "
"I<status> set to B<FILE_DEDUPE_RANGE_DIFFERS>.  The I<status> code is set to "
"B<FILE_DEDUPE_RANGE_SAME> for success, a negative error code in case of "
"error, or B<FILE_DEDUPE_RANGE_DIFFERS> if the data did not match."
msgstr ""
"En cas de succès de cet ioctl, le nombre d'octets dédupliqués avec succès "
"est renvoyé dans I<bytes_deduped> et un code de retour de l'opération de "
"déduplication est renvoyé dans I<status>. Si un seul octet de la plage ne "
"correspond pas, la demande de déduplication sera ignorée et I<status> sera "
"positionné sur B<FILE_DEDUPE_RANGE_DIFFERS>. Le code de I<status> est "
"positionné sur B<FILE_DEDUPE_RANGE_SAME> en cas de succès, sur un code "
"d'erreur négatif en cas d'erreur, ou sur B<FILE_DEDUPE_RANGE_DIFFERS> si les "
"données ne correspondaient pas."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "On error, -1 is returned, and I<errno> is set to indicate the error."
msgstr ""
"En cas d'erreur, la valeur de retour est B<-1> et I<errno> est définie pour "
"préciser l'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Possible errors include (but are not limited to) the following:"
msgstr "Les codes d'erreur possibles comprennent, entre autres, les suivants :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<src_fd> is not open for reading; I<dest_fd> is not open for writing or is "
"open for append-only writes; or the filesystem which I<src_fd> resides on "
"does not support deduplication."
msgstr ""
"I<src_fd> n'est pas ouvert en lecture ; I<dest_fd> n'est pas ouvert en "
"écriture ou il n'est ouvert que pour des compléments d'écriture ; ou le "
"système de fichiers où se trouve I<src_fd> ne gère pas la déduplication."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The filesystem does not support deduplicating the ranges of the given "
"files.  This error can also appear if either file descriptor represents a "
"device, FIFO, or socket.  Disk filesystems generally require the offset and "
"length arguments to be aligned to the fundamental block size.  Neither Btrfs "
"nor XFS support overlapping deduplication ranges in the same file."
msgstr ""
"Le système de fichiers ne gère pas la déduplication des plages des fichiers "
"donnés. Cette erreur peut également apparaître si un descripteur de fichier "
"représente un périphérique, un FIFO ou un socket. Les systèmes de fichiers "
"d'un disque nécessitent généralement que les paramètres de la position et de "
"la longueur soient alignés sur la taille de bloc fondamentale. XFS et Btrfs "
"ne prennent pas en charge le chevauchement des plages de reflink dans le "
"même fichier."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EISDIR>"
msgstr "B<EISDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"One of the files is a directory and the filesystem does not support shared "
"regions in directories."
msgstr ""
"Un ou plusieurs fichiers sont des répertoires et le système de fichiers ne "
"gère pas les régions partagées dans les répertoires."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The kernel was unable to allocate sufficient memory to perform the operation "
"or I<dest_count> is so large that the input argument description spans more "
"than a single page of memory."
msgstr ""
"Le noyau n'a pas pu allouer assez de mémoire pour effectuer l'opération ou "
"I<dest_count> est si grand que la description du paramètre d'entrée remplit "
"plus d'une page de mémoire."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr "B<EOPNOTSUPP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This can appear if the filesystem does not support deduplicating either file "
"descriptor, or if either file descriptor refers to special inodes."
msgstr ""
"Cela peut apparaître si le système de fichiers ne gère pas la déduplication "
"d'un descripteur de fichier ou si un descripteur se rapporte à des inœuds "
"spéciaux."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<dest_fd> is immutable."
msgstr "I<dest_fd> est immuable."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ETXTBSY>"
msgstr "B<ETXTBSY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "One of the files is a swap file.  Swap files cannot share storage."
msgstr ""
"Un des fichiers est un fichier d'échange. Les fichiers d'échange ne peuvent "
"pas partager de stockage."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EXDEV>"
msgstr "B<EXDEV>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<dest_fd> and I<src_fd> are not on the same mounted filesystem."
msgstr ""
"I<dest_fd> et I<src_fd> ne sont pas sur le même système de fichiers monté."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Some filesystems may limit the amount of data that can be deduplicated in a "
"single call."
msgstr ""
"Certains systèmes de fichiers peuvent limiter la quantité de données qui "
"peut être dédupliquée dans un seul appel."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux 4.5."
msgstr "Linux 4.5."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This ioctl operation first appeared in Linux 4.5.  It was previously "
#| "known as B<BTRFS_IOC_FILE_EXTENT_SAME> and was private to Btrfs."
msgid ""
"It was previously known as B<BTRFS_IOC_FILE_EXTENT_SAME> and was private to "
"Btrfs."
msgstr ""
"Cette opération ioctl est apparue pour la première fois dans Linux 4.5. Elle "
"était précédemment connue sous le nom B<BTRFS_IOC_FILE_EXTENT_SAME> et elle "
"était réservée à Btrfs."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Because a copy-on-write operation requires the allocation of new storage, "
"the B<fallocate>(2)  operation may unshare shared blocks to guarantee that "
"subsequent writes will not fail because of lack of disk space."
msgstr ""
"Une opération de copie sur écriture nécessitant l'allocation d'un nouveau "
"stockage, l'opération B<fallocate>(2) peut ne plus partager les blocs "
"partagés pour garantir que les écritures suivantes n'échoueront pas du fait "
"d'un manque d'espace disque."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ioctl>(2)"
msgstr "B<ioctl>(2)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "30 octobre 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This ioctl operation first appeared in Linux 4.5.  It was previously known "
"as B<BTRFS_IOC_FILE_EXTENT_SAME> and was private to Btrfs."
msgstr ""
"Cette opération ioctl est apparue pour la première fois dans Linux 4.5. Elle "
"était précédemment connue sous le nom B<BTRFS_IOC_FILE_EXTENT_SAME> et elle "
"était réservée à Btrfs."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "This API is Linux-specific."
msgstr "Cette API est spécifique à Linux."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "IOCTL-FIDEDUPERANGE"
msgstr "IOCTL-FIDEDUPERANGE"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/ioctl.hE<gt>>"
msgstr "B<#include E<lt>sys/ioctl.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>linux/fs.hE<gt>>"
msgstr "B<#include E<lt>linux/fs.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<int ioctl(int >I<src_fd>B<, FIDEDUPERANGE, struct file_dedupe_range "
"*>I<arg>B<);>"
msgstr ""
"B<int ioctl(int >I<src_fd>B<, FIDEDUPERANGE, struct file_dedupe_range "
"*>I<arg>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Each deduplication operation targets I<src_length> bytes in file descriptor "
"I<dest_fd> at offset I<dest_offset>.  The field I<reserved> must be zero.  "
"During the call, I<src_fd> must be open for reading and I<dest_fd> must be "
"open for writing.  The combined size of the struct I<file_dedupe_range> and "
"the struct I<file_dedupe_range_info> array must not exceed the system page "
"size.  The maximum size of I<src_length> is filesystem dependent and is "
"typically 16\\ MiB.  This limit will be enforced silently by the "
"filesystem.  By convention, the storage used by I<src_fd> is mapped into "
"I<dest_fd> and the previous contents in I<dest_fd> are freed."
msgstr ""
"Chaque opération de déduplication envoie I<src_length> octets dans le "
"descripteur de fichier I<dest_fd> à la position I<dest_offset>. Le champ "
"I<reserved> doit valoir zéro. Pendant l'appel, I<src_fd> doit être ouvert en "
"lecture et I<dest_fd> en écriture. La taille combinée du tableau des "
"structures I<file_dedupe_range> et I<file_dedupe_range_info> ne doit pas "
"dépasser la taille de la page du système. La taille maximale de "
"I<src_length> dépend du système de fichiers et est généralement de 16 Mio. "
"Cette limite sera augmentée silencieusement par le système de fichiers. Par "
"convention, le stockage utilisé par I<src_fd> est associé à I<dest_fd> et le "
"contenu précédent de I<dest_fd> est libéré."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Error codes can be one of, but are not limited to, the following:"
msgstr "Les codes d'erreur peuvent être, entre autres, les suivants :"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
