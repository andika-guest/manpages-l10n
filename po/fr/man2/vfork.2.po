# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2011.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2021-2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-08-27 17:32+0200\n"
"PO-Revision-Date: 2023-02-22 14:19+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "vfork"
msgstr "vfork"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-28"
msgstr "28 juillet 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "vfork - create a child process and block parent"
msgstr "vfork - Créer un processus enfant et bloquer le parent"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<pid_t vfork(void);>\n"
msgstr "B<pid_t vfork(void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<vfork>():"
msgstr "B<vfork>() :"

#.      || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.12:\n"
"        (_XOPEN_SOURCE E<gt>= 500) && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"            || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Before glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""
"    Depuis la glibc 2.12 :\n"
"        (_XOPEN_SOURCE E<gt>= 500) && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"            || /* Depuis la glibc 2.19 : */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19 : */ _BSD_SOURCE\n"
"    Avant la glibc 2.12 :\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Standard description"
msgstr "Description des normes"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(From POSIX.1)  The B<vfork>()  function has the same effect as B<fork>(2), "
"except that the behavior is undefined if the process created by B<vfork>()  "
"either modifies any data other than a variable of type I<pid_t> used to "
"store the return value from B<vfork>(), or returns from the function in "
"which B<vfork>()  was called, or calls any other function before "
"successfully calling B<_exit>(2)  or one of the B<exec>(3)  family of "
"functions."
msgstr ""
"(D'après POSIX.1). La routine B<vfork>() a le même effet que B<fork>(2), "
"sauf que le comportement est indéfini si le processus créé par B<vfork>() "
"effectue l'une des actions suivantes avant d'appeler avec succès B<_exit>(2) "
"ou une routine de la famille B<exec>(3)\\ : modification d'une donnée autre "
"que la variable de type I<pid_t> stockant le retour de B<vfork>(), revenir "
"de la fonction dans laquelle B<vfork>() a été invoqué, appel d'une autre "
"fonction."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Linux description"
msgstr "Description de l'implémentation Linux"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<vfork>(), just like B<fork>(2), creates a child process of the calling "
"process.  For details and return value and errors, see B<fork>(2)."
msgstr ""
"B<vfork>(), tout comme B<fork>(2), crée un processus enfant à partir du "
"processus appelant. Pour plus de détails sur les valeurs renvoyées et les "
"erreurs possibles, consultez B<fork>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<vfork>()  is a special case of B<clone>(2).  It is used to create new "
"processes without copying the page tables of the parent process.  It may be "
"useful in performance-sensitive applications where a child is created which "
"then immediately issues an B<execve>(2)."
msgstr ""
"B<vfork>() est conçu comme un cas particulier de B<clone>(2). Il sert à "
"créer un nouveau processus sans effectuer de copie de la table des pages "
"mémoire du processus parent. Cela peut être utile dans des applications "
"nécessitant une grande rapidité d'exécution, si l'enfant doit invoquer "
"immédiatement un appel B<execve>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<vfork>()  differs from B<fork>(2)  in that the calling thread is suspended "
"until the child terminates (either normally, by calling B<_exit>(2), or "
"abnormally, after delivery of a fatal signal), or it makes a call to "
"B<execve>(2).  Until that point, the child shares all memory with its "
"parent, including the stack.  The child must not return from the current "
"function or call B<exit>(3)  (which would have the effect of calling exit "
"handlers established by the parent process and flushing the parent's "
"B<stdio>(3)  buffers), but may call B<_exit>(2)."
msgstr ""
"B<vfork>() diffère de B<fork>(2) en ce que le thread appelant reste suspendu "
"jusqu'à ce que l'enfant se termine (soit normalement, en appelant "
"B<_exit>(2), soit de façon anormale après l'envoi d'un signal fatal) ou "
"qu'il appelle B<execve>(2). Jusqu'à ce point, l'enfant partage toute la "
"mémoire avec son parent, y compris la pile. Le processus enfant ne doit pas "
"revenir de la fonction en cours, ni appeler B<exit>(3) (ce qui aurait pour "
"effet d'appeler les gestionnaires de sortie établis par le processus parent "
"et de vider les tampons B<stdio>(3) du parent), mais appeler à B<_exit>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"As with B<fork>(2), the child process created by B<vfork>()  inherits copies "
"of various of the caller's process attributes (e.g., file descriptors, "
"signal dispositions, and current working directory); the B<vfork>()  call "
"differs only in the treatment of the virtual address space, as described "
"above."
msgstr ""
"Comme avec B<fork>(2), le processus enfant créé par B<vfork>() hérite des "
"copies de plusieurs attributs du processus appelant (par exemple les "
"descripteurs de fichiers, les dispositions des signaux et le répertoire de "
"travail actuel)\\ ; l'appel B<vfork>() diffère seulement par le traitement "
"de l'espace d'adressage virtuel, comme décrit ci-dessus."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Signals sent to the parent arrive after the child releases the parent's "
"memory (i.e., after the child terminates or calls B<execve>(2))."
msgstr ""
"Les signaux pour le processus parent sont délivrés après que l'enfant libère "
"la mémoire du parent (c'est-à-dire après que l'enfant se termine ou qu'il "
"appelle B<execve>(2))."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Historic description"
msgstr "Description historique"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Under Linux, B<fork>(2)  is implemented using copy-on-write pages, so the "
"only penalty incurred by B<fork>(2)  is the time and memory required to "
"duplicate the parent's page tables, and to create a unique task structure "
"for the child.  However, in the bad old days a B<fork>(2)  would require "
"making a complete copy of the caller's data space, often needlessly, since "
"usually immediately afterward an B<exec>(3)  is done.  Thus, for greater "
"efficiency, BSD introduced the B<vfork>()  system call, which did not fully "
"copy the address space of the parent process, but borrowed the parent's "
"memory and thread of control until a call to B<execve>(2)  or an exit "
"occurred.  The parent process was suspended while the child was using its "
"resources.  The use of B<vfork>()  was tricky: for example, not modifying "
"data in the parent process depended on knowing which variables were held in "
"a register."
msgstr ""
"Sous Linux, B<fork>(2) est implémenté en utilisant un mécanisme de copie en "
"écriture, ainsi ses seuls coûts sont le temps et la mémoire nécessaire pour "
"dupliquer la table des pages mémoire du processus parent, et créer une "
"seulestructure de tâche pour l'enfant. Toutefois, jadis B<fork>(2) "
"nécessitait malheureusement une copie complète de l'espace de données du "
"parent, souvent inutilement, car un appel B<exec>(3) est souvent réalisé "
"immédiatement par l'enfant. Pour améliorer les performances, BSD a introduit "
"un appel système B<vfork>() qui ne copie pas l'espace d'adressage du parent, "
"mais emprunte au parent son espace d'adressage et son fil de contrôle "
"jusqu'à un appel à B<execve>(2) ou qu'une sortie survienne. Le processus "
"parent était suspendu tant que l'enfant utilisait les ressources. "
"L'utilisation de B<vfork>() était loin d'être facile, car, pour éviter de "
"modifier les données du processus parent, il fallait être capable de "
"déterminer quelles variables se trouvaient dans des registres du processeur."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#.  In AIXv3.1 vfork is equivalent to fork.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The requirements put on B<vfork>()  by the standards are weaker than those "
"put on B<fork>(2), so an implementation where the two are synonymous is "
"compliant.  In particular, the programmer cannot rely on the parent "
"remaining blocked until the child either terminates or calls B<execve>(2), "
"and cannot rely on any specific behavior with respect to shared memory."
msgstr ""
"Les exigences que les normes apportent sur B<vfork>() sont plus relâchées "
"que celles sur B<fork>(2), ainsi il est possible d'avoir une implémentation "
"conforme où les deux appels sont synonymes. En particulier, un programmeur "
"ne doit pas s'appuyer sur le fait que le parent reste bloqué jusqu'à ce que "
"l'enfant se termine ou appelle B<execve>(2), ni sur le comportement par "
"rapport à la mémoire partagée."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, fuzzy
#| msgid ""
#| "Some consider the semantics of B<vfork>()  to be an architectural "
#| "blemish, and the 4.2BSD man page stated: \"This system call will be "
#| "eliminated when proper system sharing mechanisms are implemented.  Users "
#| "should not depend on the memory sharing semantics of B<vfork>()  as it "
#| "will, in that case, be made synonymous to B<fork>(2).\" However, even "
#| "though modern memory management hardware has decreased the performance "
#| "difference between B<fork>(2)  and B<vfork>(), there are various reasons "
#| "why Linux and other systems have retained B<vfork>():"
msgid ""
"Some consider the semantics of B<vfork>()  to be an architectural blemish, "
"and the 4.2BSD man page stated: \\[lq]This system call will be eliminated "
"when proper system sharing mechanisms are implemented.  Users should not "
"depend on the memory sharing semantics of I<vfork> as it will, in that case, "
"be made synonymous to I<fork>.\\[rq] However, even though modern memory "
"management hardware has decreased the performance difference between "
"B<fork>(2)  and B<vfork>(), there are various reasons why Linux and other "
"systems have retained B<vfork>():"
msgstr ""
"Certaines personnes considèrent la sémantique de B<vfork>() comme une verrue "
"architecturale, et la page de manuel de 4.2BSD indique que «\\ cet appel "
"système sera supprimé quand des mécanismes de partage appropriés seront "
"implémentés. Il ne faut pas essayer de tirer profit du partage mémoire "
"induit par B<vfork>(), car dans ce cas il serait rendu synonyme de "
"B<fork>(2)\\ ». Cependant, même si le matériel de gestion mémoire moderne a "
"diminué la différence de performances entre B<fork>(2) et B<vfork>(), il "
"existe diverses raisons pour lesquelles Linux et d'autres systèmes ont "
"conservé B<vfork>()\\ :"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "-"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Some performance-critical applications require the small performance "
"advantage conferred by B<vfork>()."
msgstr ""
"Certaines applications de haute performance ont besoin du petit gain apporté "
"par B<vfork>()."

#.  http://stackoverflow.com/questions/4259629/what-is-the-difference-between-fork-and-vfork
#.  http://developers.sun.com/solaris/articles/subprocess/subprocess.html
#.  http://mailman.uclinux.org/pipermail/uclinux-dev/2009-April/000684.html
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<vfork>()  can be implemented on systems that lack a memory-management unit "
"(MMU), but B<fork>(2)  can't be implemented on such systems.  (POSIX.1-2008 "
"removed B<vfork>()  from the standard; the POSIX rationale for the "
"B<posix_spawn>(3)  function notes that that function, which provides "
"functionality equivalent to B<fork>(2)+B<exec>(3), is designed to be "
"implementable on systems that lack an MMU.)"
msgstr ""
"B<vfork>() peut être implémenté sur des systèmes sans unité de gestion "
"mémoire (MMU, pour «\\ memory-management unit\\ »), mais B<fork>(2) ne peut "
"pas être implémenté sur de tels systèmes (POSIX.1-2008 a supprimé B<vfork>() "
"de la norme\\ ; la raison invoquée par POSIX pour la fonction "
"B<posix_spawn>(3) note que cette fonction, qui fournit une fonctionnalité "
"équivalente à B<fork>(2)+B<exec>(3), est conçue pour être implémentable sur "
"des systèmes sans MMU)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On systems where memory is constrained, B<vfork>()  avoids the need to "
"temporarily commit memory (see the description of I</proc/sys/vm/"
"overcommit_memory> in B<proc>(5))  in order to execute a new program.  (This "
"can be especially beneficial where a large parent process wishes to execute "
"a small helper program in a child process.)  By contrast, using B<fork>(2)  "
"in this scenario requires either committing an amount of memory equal to the "
"size of the parent process (if strict overcommitting is in force)  or "
"overcommitting memory with the risk that a process is terminated by the out-"
"of-memory (OOM) killer."
msgstr ""
"Sur les systèmes où la mémoire est restreinte, B<vfork> évite le besoin "
"d'allouer de la mémoire temporairement (voir la description de I</proc/sys/"
"vm/overcommit_memory> dans B<proc>(5)) afin d'exécuter un nouveau programme. "
"Cela peut être particulièrement bénéfique lorsqu'un grand processus parent "
"souhaite exécuter un petit programme d'assistance dans un processus enfant. "
"Par contraste, l'utilisation de B<fork>(2) dans ce scénario nécessite soit "
"l'allocation d'une quantité de mémoire égale à la taille du processus parent "
"(si la gestion stricte du dépassement est en cours) soit un dépassement de "
"mémoire avec le risque qu'un processus soit terminé par la mise à mort sur "
"mémoire saturée (OOM killer)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Linux notes"
msgstr "Notes pour Linux"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Fork handlers established using B<pthread_atfork>(3)  are not called when a "
"multithreaded program employing the NPTL threading library calls "
"B<vfork>().  Fork handlers are called in this case in a program using the "
"LinuxThreads threading library.  (See B<pthreads>(7)  for a description of "
"Linux threading libraries.)"
msgstr ""
"Les gestionnaires enregistrés avec B<pthread_atfork>(3) ne sont pas appelés "
"lorsqu'un programme multithreadé utilisant la bibliothèque de threads NPTL "
"appelle B<vfork>(). En revanche ces gestionnaires sont appelés si le "
"programme utilise la bibliothèque LinuxThreads. (Consultez B<pthreads>(7) "
"pour une description des bibliothèques de threads pour Linux.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A call to B<vfork>()  is equivalent to calling B<clone>(2)  with I<flags> "
"specified as:"
msgstr ""
"Un appel à B<vfork>() est équivalent à appeler B<clone>(2) avec I<flags> "
"valant\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid " CLONE_VM | CLONE_VFORK | SIGCHLD\n"
msgstr " CLONE_VM | CLONE_VFORK | SIGCHLD\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"4.3BSD; POSIX.1-2001 (but marked OBSOLETE).  POSIX.1-2008 removes the "
"specification of B<vfork>()."
msgstr ""
"4.3BSD, POSIX.1-2001 (mais la déclare obsolète). POSIX.1-2008 supprime la "
"spécification de B<vfork>()."

#.  In the release notes for 4.2BSD Sam Leffler wrote: `vfork: Is still
#.  present, but definitely on its way out'.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<vfork>()  system call appeared in 3.0BSD.  In 4.4BSD it was made "
"synonymous to B<fork>(2)  but NetBSD introduced it again; see E<.UR http://"
"www.netbsd.org\\:/Documentation\\:/kernel\\:/vfork.html> E<.UE .> In Linux, "
"it has been equivalent to B<fork>(2)  until Linux 2.2.0-pre6 or so.  Since "
"Linux 2.2.0-pre9 (on i386, somewhat later on other architectures) it is an "
"independent system call.  Support was added in glibc 2.0.112."
msgstr ""
"L'appel système B<vfork>() est apparu dans 3.0BSD. Dans 4.4BSD, il est "
"devenu synonyme de B<fork>(2), mais NetBSD l'a réintroduit à nouveau "
"(consultez E<.UR http://www.netbsd.org\\:/Documentation\\:/kernel\\:/vfork."
"html> E<.UE >). Sous Linux, il a été l'équivalent de B<fork>(2) jusqu'à "
"Linux 2.2.0-pre-6. Depuis Linux 2.2.0-pre-9 (sur i386, un peu plus tard sur "
"d'autres architectures), il s'agit d'un appel système indépendant. La prise "
"en charge a été introduite dans la glibc 2.0.112."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The child process should take care not to modify the memory in unintended "
"ways, since such changes will be seen by the parent process once the child "
"terminates or executes another program.  In this regard, signal handlers can "
"be especially problematic: if a signal handler that is invoked in the child "
"of B<vfork>()  changes memory, those changes may result in an inconsistent "
"process state from the perspective of the parent process (e.g., memory "
"changes would be visible in the parent, but changes to the state of open "
"file descriptors would not be visible)."
msgstr ""
"Le processus enfant devrait veiller à ne pas modifier la mémoire d'une "
"manière inattendue, dans la mesure où les modifications seront vues par le "
"processus parent une fois que l'enfant s'est achevé ou exécute un autre "
"programme. À cet égard, les gestionnaires de signaux peuvent être "
"particulièrement problématiques : si un gestionnaire de signal qui est "
"invoqué dans l'enfant d'un B<vfork>() modifie la mémoire, ces modifications "
"peuvent aboutir à une inconsistance de l'état du processus du point de vue "
"du processus parent (par exemple, les modifications de la mémoire pourraient "
"être visibles dans le parent, mais pas les modifications de l'état des "
"descripteurs de fichiers ouverts)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When B<vfork>()  is called in a multithreaded process, only the calling "
"thread is suspended until the child terminates or executes a new program.  "
"This means that the child is sharing an address space with other running "
"code.  This can be dangerous if another thread in the parent process changes "
"credentials (using B<setuid>(2)  or similar), since there are now two "
"processes with different privilege levels running in the same address "
"space.  As an example of the dangers, suppose that a multithreaded program "
"running as root creates a child using B<vfork>().  After the B<vfork>(), a "
"thread in the parent process drops the process to an unprivileged user in "
"order to run some untrusted code (e.g., perhaps via plug-in opened with "
"B<dlopen>(3)).  In this case, attacks are possible where the parent process "
"uses B<mmap>(2)  to map in code that will be executed by the privileged "
"child process."
msgstr ""
"Lorsque B<vfork>() est appelé dans un processus multithreadé, seul le thread "
"appelant est suspendu jusqu'à ce que l'enfant s'achève ou exécute un nouveau "
"programme. Cela signifie que l'enfant partage un espace d'adresses avec un "
"autre code en cours d'exécution. Cela peut être dangereux si un autre thread "
"dans le processus parent modifie son identifiant (avec B<setuid>(2) ou une "
"autre commande similaire), dans la mesure où il y a alors deux processus "
"avec différents niveaux de privilèges exécutés dans le même espace "
"d'adresses. Comme exemple de risque, imaginez qu'un programme multithreadé "
"exécuté en tant que superutilisateur crée un enfant avec B<vfork>(). Après "
"le B<vfork>(), un thread dans le processus parent transfère le processus à "
"un utilisateur non privilégié afin d'exécuter du code non sûr (par exemple, "
"peut-être au moyen d'un greffon ouvert avec B<dlopen>(3)). Dans ce cas, des "
"attaques sont possibles où le processus parent utilise B<mmap>(2) pour "
"projeter en mémoire du code qui sera exécuté par le processus enfant "
"privilégié."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. #-#-#-#-#  archlinux: vfork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  As far as I can tell, the following is not true in Linux 2.6.19:
#.  Currently (Linux 2.3.25),
#.  .BR strace (1)
#.  cannot follow
#.  .BR vfork ()
#.  and requires a kernel patch.
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: vfork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  As far as I can tell, the following is not true in Linux 2.6.19:
#.  Currently (Linux 2.3.25),
#.  .BR strace (1)
#.  cannot follow
#.  .BR vfork ()
#.  and requires a kernel patch.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: vfork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  As far as I can tell, the following is not true in Linux 2.6.19:
#.  Currently (Linux 2.3.25),
#.  .BR strace (1)
#.  cannot follow
#.  .BR vfork ()
#.  and requires a kernel patch.
#. type: Plain text
#. #-#-#-#-#  fedora-39: vfork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  As far as I can tell, the following is not true in Linux 2.6.19:
#.  Currently (Linux 2.3.25),
#.  .BR strace (1)
#.  cannot follow
#.  .BR vfork ()
#.  and requires a kernel patch.
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: vfork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  As far as I can tell, the following is not true in Linux 2.6.19:
#.  Currently (Linux 2.3.25),
#.  .BR strace (1)
#.  cannot follow
#.  .BR vfork ()
#.  and requires a kernel patch.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: vfork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  As far as I can tell, the following is not true in Linux 2.6.19:
#.  Currently (Linux 2.3.25),
#.  .BR strace (1)
#.  cannot follow
#.  .BR vfork ()
#.  and requires a kernel patch.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: vfork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  As far as I can tell, the following is not true in 2.6.19:
#.  Currently (Linux 2.3.25),
#.  .BR strace (1)
#.  cannot follow
#.  .BR vfork ()
#.  and requires a kernel patch.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: vfork.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  As far as I can tell, the following is not true in Linux 2.6.19:
#.  Currently (Linux 2.3.25),
#.  .BR strace (1)
#.  cannot follow
#.  .BR vfork ()
#.  and requires a kernel patch.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Details of the signal handling are obscure and differ between systems.  The "
"BSD man page states: \"To avoid a possible deadlock situation, processes "
"that are children in the middle of a B<vfork>()  are never sent B<SIGTTOU> "
"or B<SIGTTIN> signals; rather, output or I<ioctl>s are allowed and input "
"attempts result in an end-of-file indication.\""
msgstr ""
"Les détails de la gestion des signaux sont compliqués et varient suivant les "
"systèmes. La page de manuel BSD indique\\ : «\\ Pour éviter une possible "
"situation d'interblocage, les processus qui sont des enfants au milieu d'un "
"B<vfork>() ne reçoivent jamais le signal B<SIGTTOU> ou B<SIGTTIN>\\ ; des "
"sorties et des I<ioctl> sont autorisés, mais des tentatives de lecture "
"donneront une indication de fin de fichier.\\ »"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<clone>(2), B<execve>(2), B<_exit>(2), B<fork>(2), B<unshare>(2), B<wait>(2)"
msgstr ""
"B<clone>(2), B<execve>(2), B<_exit>(2), B<fork>(2), B<unshare>(2), B<wait>(2)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Some consider the semantics of B<vfork>()  to be an architectural blemish, "
"and the 4.2BSD man page stated: \"This system call will be eliminated when "
"proper system sharing mechanisms are implemented.  Users should not depend "
"on the memory sharing semantics of B<vfork>()  as it will, in that case, be "
"made synonymous to B<fork>(2).\" However, even though modern memory "
"management hardware has decreased the performance difference between "
"B<fork>(2)  and B<vfork>(), there are various reasons why Linux and other "
"systems have retained B<vfork>():"
msgstr ""
"Certaines personnes considèrent la sémantique de B<vfork>() comme une verrue "
"architecturale, et la page de manuel de 4.2BSD indique que «\\ cet appel "
"système sera supprimé quand des mécanismes de partage appropriés seront "
"implémentés. Il ne faut pas essayer de tirer profit du partage mémoire "
"induit par B<vfork>(), car dans ce cas il serait rendu synonyme de "
"B<fork>(2)\\ ». Cependant, même si le matériel de gestion mémoire moderne a "
"diminué la différence de performances entre B<fork>(2) et B<vfork>(), il "
"existe diverses raisons pour lesquelles Linux et d'autres systèmes ont "
"conservé B<vfork>()\\ :"

#. type: SS
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "Caveats"
msgstr "Mises en garde"

#. type: SS
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "History"
msgstr "Historique"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pages du manuel de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "VFORK"
msgstr "VFORK"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/types.hE<gt>>"
msgstr "B<#include E<lt>sys/types.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<pid_t vfork(void);>"
msgstr "B<pid_t vfork(void);>"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Since glibc 2.12:"
msgstr "Depuis la glibc 2.12 :"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"(_XOPEN_SOURCE\\ E<gt>=\\ 500) && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE\n"
msgstr ""
"(_XOPEN_SOURCE\\ E<gt>=\\ 500) && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
"    || /* À partir de la glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE\n"

#.      || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-leap-15-6
msgid "Before glibc 2.12: _BSD_SOURCE || _XOPEN_SOURCE\\ E<gt>=\\ 500"
msgstr "Avant la glibc 2.12 : _BSD_SOURCE || _XOPEN_SOURCE\\ E<gt>=\\ 500"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "     CLONE_VM | CLONE_VFORK | SIGCHLD\n"
msgstr "     CLONE_VM | CLONE_VFORK | SIGCHLD\n"

#.  In the release notes for 4.2BSD Sam Leffler wrote: `vfork: Is still
#.  present, but definitely on its way out'.
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<vfork>()  system call appeared in 3.0BSD.  In 4.4BSD it was made "
"synonymous to B<fork>(2)  but NetBSD introduced it again; see E<.UR http://"
"www.netbsd.org\\:/Documentation\\:/kernel\\:/vfork.html> E<.UE .> In Linux, "
"it has been equivalent to B<fork>(2)  until 2.2.0-pre6 or so.  Since 2.2.0-"
"pre9 (on i386, somewhat later on other architectures) it is an independent "
"system call.  Support was added in glibc 2.0.112."
msgstr ""
"L'appel système B<vfork>() est apparu dans 3.0BSD. Dans 4.4BSD, il est "
"devenu synonyme de B<fork>(2), mais NetBSD l'a réintroduit à nouveau "
"(consultez E<.UR http://www.netbsd.org\\:/Documentation\\:/kernel\\:/vfork."
"html> E<.UE >). Sous Linux, il a été l'équivalent de B<fork>(2) jusqu'au "
"noyau 2.2.0-pre-6. Depuis le noyau 2.2.0-pre-9 (sur i386, un peu plus tard "
"sur d'autres architectures), il s'agit d'un appel système indépendant. La "
"prise en charge dans la bibliothèque a été introduite dans la glibc 2.0.112."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
