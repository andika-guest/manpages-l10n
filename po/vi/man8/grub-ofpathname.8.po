# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-07-25 19:36+0200\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB-OFPATHNAME"
msgstr "GRUB-OFPATHNAME"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2023"
msgstr "Tháng 7 năm 2023"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "GRUB 2.06"
msgid "GRUB 2.12~rc1"
msgstr "GRUB 2.06"

#. type: TH
#: archlinux
#, no-wrap
msgid "System Administration Utilities"
msgstr "Tiện ích quản trị hệ thống"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux
msgid "grub-ofpathname - find OpenBOOT path for a device"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux
msgid "B<grub-ofpathname> I<\\,DEVICE\\/>"
msgstr "B<grub-ofpathname> I<\\,THIẾT_BỊ\\/>"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux
msgid "B<grub-probe>(8)"
msgstr "B<grub-probe>(8)"

#. type: Plain text
#: archlinux
msgid ""
"The full documentation for B<grub-ofpathname> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-ofpathname> programs are properly "
"installed at your site, the command"
msgstr ""
"Tài liệu hướng dẫn đầy đủ về B<grub-ofpathname> được bảo trì dưới dạng một "
"sổ tay Texinfo.  Nếu chương trình B<info> và B<grub-ofpathname> được cài đặt "
"đúng ở địa chỉ của bạn thì câu lệnh"

#. type: Plain text
#: archlinux
msgid "B<info grub-ofpathname>"
msgstr "B<info grub-ofpathname>"

#. type: Plain text
#: archlinux
msgid "should give you access to the complete manual."
msgstr "nên cho phép bạn truy cập đến toàn bộ sổ tay."
