# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-10-02 12:13+0200\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OD"
msgstr "OD"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "Tháng 9 năm 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Các câu lệnh"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "od - dump files in octal and other formats"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<od> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<od> [I<\\,TÙY_CHỌN\\/>]… [I<\\,TẬP_TIN\\/>]…"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<od> [I<\\,-abcdfilosx\\/>]... [I<\\,FILE\\/>] [[I<\\,+\\/>]I<\\,OFFSET\\/"
">[I<\\,.\\/>][I<\\,b\\/>]]"
msgstr ""
"B<od> [I<\\,-abcdfilosx\\/>]… [I<\\,TẬP_TIN\\/>] [[I<\\,+\\/>]I<\\,HIỆU\\/"
">[I<\\,.\\/>][I<\\,b\\/>]]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<od> I<\\,--traditional \\/>[I<\\,OPTION\\/>]... [I<\\,FILE\\/>] [[I<\\,+\\/"
">]I<\\,OFFSET\\/>[I<\\,.\\/>][I<\\,b\\/>] [I<\\,+\\/>][I<\\,LABEL\\/>][I<\\,."
"\\/>][I<\\,b\\/>]]"
msgstr ""
"B<od> I<\\,--traditional \\/>[I<\\,TÙY_CHỌN\\/>]… [I<\\,TẬP_TIN\\/>] [[I<\\,+"
"\\/>]I<\\,HIỆU\\/>[I<\\,.\\/>][I<\\,b\\/>] [I<\\,+\\/>][I<\\,NHÃN\\/>][I<\\,."
"\\/>][I<\\,b\\/>]]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Write an unambiguous representation, octal bytes by default, of FILE to "
"standard output.  With more than one FILE argument, concatenate them in the "
"listed order to form the input."
msgstr ""
"Ghi TẬP_TIN biểu diễn không rõ ràng ra đầu ra tiêu chuẩn, theo byte bát phân "
"theo mặc định. Khi có nhiều hơn một đối số TẬP_TIN, thì nối chúng với nhau "
"theo thứ tự liệt kê để tạo dữ liệu vào."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""
"Không chỉ ra TẬP_TIN, hoặc khi TẬP_TIN là “-”, thì đọc từ đầu vào tiêu chuẩn."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If first and second call formats both apply, the second format is assumed if "
"the last operand begins with + or (if there are 2 operands) a digit.  An "
"OFFSET operand means B<-j> OFFSET.  LABEL is the pseudo-address at first "
"byte printed, incremented when dump is progressing.  For OFFSET and LABEL, a "
"0x or 0X prefix indicates hexadecimal; suffixes may be . for octal and b for "
"multiply by 512."
msgstr ""
"Nếu áp dụng cả dạng gọi thứ nhất và thứ hai, thì dạng thứ hai có tác dụng "
"nếu toán hạng cuối cùng bắt đầu với “+” hoặc (nếu có 2 toán hạng) một chữ "
"số. Một toán hạng HIỆU có nghĩa “B<-j> HIỆU”. NHÃN là một địa chỉ giả lập "
"tại byte đầu tiên được in, tăng lên khi tiến trình đổ đang chạy. Đối với "
"HIỆU và NHÃN, một tiền tố “0x” hoặc “0X” cho biết nó là dạng thập lục; hậu "
"tố đuôi có thể là “.” đối với bát phân và “b” để nhân lên 512."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr "Tùy chọn dài yêu cầu đối số thì tùy chọn ngắn cũng vậy."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-A>, B<--address-radix>=I<\\,RADIX\\/>"
msgstr "B<-A>, B<--address-radix>=I<\\,CƠ_SỐ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"output format for file offsets; RADIX is one of [doxn], for Decimal, Octal, "
"Hex or None"
msgstr ""
"định dạng kết xuất cho offset tập tin. Cơ số là một trong [doxn]. Đây là "
"những chữ viết tắt của: Decimal (cho Thập Phân), Octal (Bát Phân), hex (Thập "
"lục phân), None (không)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--endian=>{big|little}"
msgstr "B<--endian=>{big|little}"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "swap input bytes according the specified order"
msgstr "tráo đổi thứ tự byte tuân theo thứ tự đã cho"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-j>, B<--skip-bytes>=I<\\,BYTES\\/>"
msgstr "B<-j>, B<--skip-bytes>=I<\\,SỐ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "skip BYTES input bytes first"
msgstr "bỏ qua SỐ byte đầu tiên từ đầu vào"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-N>, B<--read-bytes>=I<\\,BYTES\\/>"
msgstr "B<-N>, B<--read-bytes>=I<\\,BYTE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "limit dump to BYTES input bytes"
msgstr "giới hạn việc đổ thành BYTE byte dữ liệu vào"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-S> BYTES, B<--strings>[=I<\\,BYTES\\/>]"
msgstr "B<-S> I<\\,BYTE\\/>, B<--strings>[=I<\\,BYTE\\/>]"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"show only NUL terminated strings of at least BYTES (3) printable characters"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--format>=I<\\,TYPE\\/>"
msgstr "B<-t>, B<--format>=I<\\,KIỂU\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "select output format or formats"
msgstr "chọn (các) định dạng kết quả, hay các định dạng"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--output-duplicates>"
msgstr "B<-v>, B<--output-duplicates>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not use * to mark line suppression"
msgstr "không dùng “*” để đánh dấu việc bỏ dòng"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-w[BYTES]>, B<--width>[=I<\\,BYTES\\/>]"
msgstr "B<-w[BYTE]>, B<--width>[=I<\\,BYTE\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"output BYTES bytes per output line; 32 is implied when BYTES is not specified"
msgstr "cho ra BYTE byte trên mỗi dòng 32 là mặc định khi không chỉ ra BYTE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--traditional>"
msgstr "B<--traditional>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "accept arguments in third form above"
msgstr "chấp nhận đối số ở dạng thứ ba ở trên"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "hiển thị trợ giúp này rồi thoát"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "đưa ra thông tin phiên bản rồi thoát"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Traditional format specifications may be intermixed; they accumulate:"
msgstr "Có thể kết hợp tham số ở dạng truyền thống; chúng tích lũy:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid "same as B<-t> a, select named characters, ignoring high-order bit"
msgstr "ký tự có tên, lờ đi bit bậc cao"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>"
msgstr "B<-b>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid "same as B<-t> o1, select octal bytes"
msgstr "byte bát phân"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>"
msgstr "B<-c>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid "same as B<-t> c, select printable characters or backslash escapes"
msgstr "ký tự ASCII hoặc ký tự thoát gạch ngược"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid "same as B<-t> u2, select unsigned decimal 2-byte units"
msgstr "đơn vị hai byte thập phân không dấu"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid "same as B<-t> fF, select floats"
msgstr "dấu chấm động"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>"
msgstr "B<-i>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid "same as B<-t> dI, select decimal ints"
msgstr "số nguyên thập phân"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid "same as B<-t> dL, select decimal longs"
msgstr "chiều dài thập phân"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>"
msgstr "B<-o>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid "same as B<-t> o2, select octal 2-byte units"
msgstr "đơn vị 2 byte bát phân"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>"
msgstr "B<-s>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid "same as B<-t> d2, select decimal 2-byte units"
msgstr "đơn vị 2 byte thập phân"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>"
msgstr "B<-x>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid "same as B<-t> x2, select hexadecimal 2-byte units"
msgstr "đơn vị 2 byte thập lục"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "TYPE is made up of one or more of these specifications:"
msgstr "KIỂU làm từ một hoặc vài dạng đặc tả sau đây:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "a"
msgstr "a"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "named character, ignoring high-order bit"
msgstr "ký tự có tên, lờ đi bit bậc cao"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "c"
msgstr "c"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "printable character or backslash escape"
msgstr "ký tự ASCII hoặc thoát chuỗi gạch ngược"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "d[SIZE]"
msgstr "d[CỠ]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "signed decimal, SIZE bytes per integer"
msgstr "số thập phân có dấu, CỠ byte cho mỗi số nguyên"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "f[SIZE]"
msgstr "f[CỠ]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "floating point, SIZE bytes per float"
msgstr "dấu chấm động, CỠ byte cho mỗi số thực"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "o[SIZE]"
msgstr "o[CỠ]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "octal, SIZE bytes per integer"
msgstr "số bát phân, CỠ byte cho mỗi số nguyên"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "u[SIZE]"
msgstr "u[CỠ]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "unsigned decimal, SIZE bytes per integer"
msgstr "số thập phân không dấu, CỠ byte cho mỗi số nguyên"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "x[SIZE]"
msgstr "x[CỠ]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "hexadecimal, SIZE bytes per integer"
msgstr "số thập lục, CỠ byte cho mỗi số nguyên"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "\n"
#| "SIZE is a number.  For TYPE in [doux], SIZE may also be C for\n"
#| "sizeof(char), S for sizeof(short), I for sizeof(int) or L for\n"
#| "sizeof(long).  If TYPE is f, SIZE may also be F for sizeof(float), D\n"
#| "for sizeof(double) or L for sizeof(long double).\n"
msgid ""
"SIZE is a number.  For TYPE in [doux], SIZE may also be C for sizeof(char), "
"S for sizeof(short), I for sizeof(int) or L for sizeof(long).  If TYPE is f, "
"SIZE may also be F for sizeof(float), D for sizeof(double) or L for "
"sizeof(long double)."
msgstr ""
"\n"
"CỠ là một số.  Đối với KIỂU trong [doux], CỠ còn có thể là:\n"
"  C   sizeof(char)         kích cỡ ký tự\n"
"  S   sizeof(short)        kích cỡ ngắn\n"
"  I   sizeof(int)          kích cỡ số nguyên\n"
"  L   sizeof(long)         kích cỡ dài\n"
"Nếu KIỂU là f, CỠ còn có thể là:\n"
"  F   sizeof(float)        kích cỡ dấu chấm động\n"
"  D   sizeof(double)       kích cỡ chính\n"
"  L   sizeof(long double)  kích cỡ chính đôi dài\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Adding a z suffix to any type displays printable characters at the end of "
"each output line."
msgstr ""
"Thêm hậu tố z vào mọi ký tự có thể hiển thị được tại cuối của từng dòng kết "
"xuất."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BYTES is hex with 0x or 0X prefix, and may have a multiplier suffix:"
msgstr "BYTES là dạng bát phân với tiền tố 0x hoặc 0X, và có thể đặt các hậu tố bội số sau:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "b"
msgstr "b"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "512"
msgstr "512"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "KB"
msgstr "KB"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "1000"
msgstr "1000"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "K"
msgstr "K"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "1024"
msgstr "1024"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MB"
msgstr "MB"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "1000*1000"
msgstr "1000*1000"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "M"
msgstr "M"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "1024*1024"
msgstr "1024*1024"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
msgid ""
"and so on for G, T, P, E, Z, Y, R, Q.  Binary prefixes can be used, too: "
"KiB=K, MiB=M, and so on."
msgstr "và tương tự với G, T, P, E, Z, Y."

# type: =head1
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "VÍ DỤ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<od -A x -t x1z -v>"
msgstr "B<od -A x -t x1z -v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "cannot perform formatted output"
msgid "Display hexdump format output"
msgstr "không thể thực hiện kết xuất đã định dạng"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<od -A o -t oS -w16>"
msgstr "B<od -A o -t oS -w16>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "multiple output formats specified"
msgid "The default output format used by od"
msgstr "đã chỉ ra nhiều định dạng kết quả"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "TÁC GIẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Jim Meyering."
msgstr "Viết bởi Jim Meyering."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Trợ giúp trực tuyến GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Report %s translation bugs to <https://translationproject.org/team/>\n"
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Hãy thông báo lỗi dịch “%s” cho <https://translationproject.org/team/vi."
"html>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "BẢN QUYỀN"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Đây là phần mềm tự do: bạn có quyền sửa đổi và phát hành lại nó. KHÔNG CÓ "
"BẢO HÀNH GÌ CẢ, với điều khiển được pháp luật cho phép."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/odE<gt>"
msgstr ""
"Tài liệu đầy đủ có tại: E<lt>https://www.gnu.org/software/coreutils/odE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) od invocation\\(aq"
msgstr "hoặc sẵn có nội bộ thông qua: info \\(aq(coreutils) od invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "Tháng 9 năm 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 mageia-cauldron opensuse-leap-15-6
msgid ""
"output strings of at least BYTES graphic chars; 3 is implied when BYTES is "
"not specified"
msgstr ""
"cho ra các chuỗi của ít nhất BYTE ký tự đồ họa 3 là mặc định khi không chỉ "
"ra BYTE"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, fuzzy
msgid ""
"and so on for G, T, P, E, Z, Y.  Binary prefixes can be used, too: KiB=K, "
"MiB=M, and so on."
msgstr "và tương tự với G, T, P, E, Z, Y."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Tháng 4 năm 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "Tháng 10 năm 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."
