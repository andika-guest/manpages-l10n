# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2005.
# Marcos Fouces <marcos@debian.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:59+0200\n"
"PO-Revision-Date: 2023-03-01 18:05+0100\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "getpass"
msgstr "getpass"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 ​​Julio 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Páginas de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getpass - get a password"
msgstr "getpass - obtiene una contraseña"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] char *getpass(const char *>I<prompt>B<);>\n"
msgstr "B<[[a extinguir]] char *getpass(const char *>I<prompt>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de Macros de Prueba de Características para glibc (véase "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getpass>():"
msgstr "B<getpass>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.2.2:\n"
"        _XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"            || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Before glibc 2.2.2:\n"
"        none\n"
msgstr ""
"    A partir de glibc 2.2.2:\n"
"        _XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"            || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Antes de glibc 2.2.2:\n"
"        nada\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This function is obsolete.  Do not use it.  See NOTES.  If you want to read "
"input without terminal echoing enabled, see the description of the I<ECHO> "
"flag in B<termios>(3)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<getpass>()  function opens I</dev/tty> (the controlling terminal of "
"the process), outputs the string I<prompt>, turns off echoing, reads one "
"line (the \"password\"), restores the terminal state and closes I</dev/tty> "
"again."
msgstr ""
"La función B<getpass>()  abre el fichero I</dev/tty> (la terminal de control "
"del proceso), escribe la cadena I<prompt>, desactiva el eco, lee una línea "
"(la \"contraseña\"), restablece el estado de la terminal y cierra I</dev/"
"tty> de nuevo."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The function B<getpass>()  returns a pointer to a static buffer containing "
"(the first B<PASS_MAX> bytes of) the password without the trailing newline, "
"terminated by a null byte (\\[aq]\\e0\\[aq]).  This buffer may be "
"overwritten by a following call.  On error, the terminal state is restored, "
"I<errno> is set to indicate the error, and NULL is returned."
msgstr ""
"La función B<getpass>() devuelve un puntero a un buffer estático que "
"contiene (los primeros B<PASS_MAX> bytes de) la contraseña sin el carácter "
"nueva línea, terminada en NULL (\\[aq]\\e0[aq]).  Este buffer puede ser "
"sobreescrito por una llamada posterior.  En caso de error, el estado de la "
"terminal se restablece, se pone un valor adecuado en I<errno>, y se devuelve "
"NULL."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENXIO>"
msgstr "B<ENXIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The process does not have a controlling terminal."
msgstr "El proceso no tiene una terminal de control."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. #-#-#-#-#  archlinux: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  A
#.  .BR getpass ()
#.  function appeared in Version 7 AT&T UNIX.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  A
#.  .BR getpass ()
#.  function appeared in Version 7 AT&T UNIX.
#. type: Plain text
#. #-#-#-#-#  fedora-39: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  A
#.  .BR getpass ()
#.  function appeared in Version 7 AT&T UNIX.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  A
#.  .BR getpass ()
#.  function appeared in Version 7 AT&T UNIX.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/tty>"
msgstr "I</dev/tty>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getpass>()"
msgstr "B<getpass>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe term"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Version 7 AT&T UNIX.  Present in SUSv2, but marked LEGACY.  Removed in "
"POSIX.1-2001."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#.  For libc4 and libc5, the prompt is not written to
#.  .I /dev/tty
#.  but to
#.  .IR stderr .
#.  Moreover, if
#.  .I /dev/tty
#.  cannot be opened, the password is read from
#.  .IR stdin .
#.  The static buffer has length 128 so that only the first 127
#.  bytes of the password are returned.
#.  While reading the password, signal generation
#.  .RB ( SIGINT ,
#.  .BR SIGQUIT ,
#.  .BR SIGSTOP ,
#.  .BR SIGTSTP )
#.  is disabled and the corresponding characters
#.  (usually control-C, control-\e, control-Z and control-Y)
#.  are transmitted as part of the password.
#.  Since libc 5.4.19 also line editing is disabled, so that also
#.  backspace and the like will be seen as part of the password.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "You should use instead B<readpassphrase>(3bsd), provided by I<libbsd>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "For glibc2, if I</dev/tty> cannot be opened, the prompt is written to "
#| "I<stderr> and the password is read from I<stdin>.  There is no limit on "
#| "the length of the password.  Line editing is not disabled."
msgid ""
"In the GNU C library implementation, if I</dev/tty> cannot be opened, the "
"prompt is written to I<stderr> and the password is read from I<stdin>.  "
"There is no limit on the length of the password.  Line editing is not "
"disabled."
msgstr ""
"Para glibc2, si I</dev/tty> no puede abrirse, el prompt se escribe en "
"I<stderr> y la contraseña es leida de I<stdin>.  No hay límite en la "
"longitud de la contraseña.  La edición de la línea no está deshabilitada."

#.  Libc4 and libc5 have never supported
#.  .B PASS_MAX
#.  or
#.  .BR _SC_PASS_MAX .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "According to the SUSv2, the value of PASS_MAX must be defined in "
#| "I<E<lt>limits.hE<gt>> in case it is smaller than 8, and can in any case "
#| "be obtained using I<sysconf(_SC_PASS_MAX)>.  However, POSIX.2 withdraws "
#| "the constants PASS_MAX and _SC_PASS_MAX, and the function B<getpass ().> "
#| "Libc4 and libc5 have never supported PASS_MAX or _SC_PASS_MAX.  Glibc2 "
#| "accepts _SC_PASS_MAX and returns BUFSIZ (e.g., 8192)."
msgid ""
"According to SUSv2, the value of B<PASS_MAX> must be defined in "
"I<E<lt>limits.hE<gt>> in case it is smaller than 8, and can in any case be "
"obtained using I<sysconf(_SC_PASS_MAX)>.  However, POSIX.2 withdraws the "
"constants B<PASS_MAX> and B<_SC_PASS_MAX>, and the function B<getpass>().  "
"The glibc version accepts B<_SC_PASS_MAX> and returns B<BUFSIZ> (e.g., 8192)."
msgstr ""
"Según el estándar SUSv2, el valor de PASS_MAX debe estar definido en "
"I<E<lt>limits.hE<gt>> en caso de que sea menor que 8, y puede ser obtenido "
"en cualquier caso usando I<sysconf(_SC_PASS_MAX)>.  Sin embargo, retira las "
"constantes PASS_MAX y _SC_PASS_MAX, y la función B<getpass ().> Libc4 y "
"libc5 no han soportado nunca PASS_MAX ni _SC_PASS_MAX.  Glibc2 acepta "
"_SC_PASS_MAX y devuelve BUFSIZ (p.e., 8192)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The calling process should zero the password as soon as possible to avoid "
"leaving the cleartext password visible in the process's address space."
msgstr ""
"El proceso que llama a esta función debería poner a cero todos los "
"caracteres de la contraseña tan pronto como le fuera posible para evitar "
"dejar la contraseña sin cifrar visible en el espacio de direcciones del "
"proceso."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<crypt>(3)"
msgstr "B<crypt>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "Present in SUSv2, but marked LEGACY.  Removed in POSIX.1-2001."
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Páginas de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GETPASS"
msgstr "GETPASS"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2016-03-15"
msgstr "15 Marzo 2016"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<char *getpass(const char *>I<prompt>B<);>"
msgstr "B<char *getpass(const char *>I<prompt>B<);>"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Since glibc 2.2.2:"
msgstr "Desde glibc 2.2.2:"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_XOPEN_SOURCE && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200112L)\n"
"    || /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE\n"
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Before glibc 2.2.2:"
msgstr "Antes de glibc 2.2.2:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "none"
msgstr "none"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This function is obsolete.  Do not use it.  If you want to read input "
"without terminal echoing enabled, see the description of the I<ECHO> flag in "
"B<termios>(3)."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The function B<getpass> returns a pointer to a static buffer containing "
#| "the (first PASS_MAX bytes of) the password without the trailing newline, "
#| "terminated by a NUL.  This buffer may be overwritten by a following "
#| "call.  On error, the terminal state is restored, I<errno> is set "
#| "appropriately, and NULL is returned."
msgid ""
"The function B<getpass>()  returns a pointer to a static buffer containing "
"(the first B<PASS_MAX> bytes of) the password without the trailing newline, "
"terminated by a null byte (\\(aq\\e0\\(aq).  This buffer may be overwritten "
"by a following call.  On error, the terminal state is restored, I<errno> is "
"set appropriately, and NULL is returned."
msgstr ""
"La función B<getpass> devuelve un puntero a un buffer estático que contiene "
"(los primeros PASS_MAX bytes de) la contraseña sin el carácter nueva línea, "
"terminada en NUL.  Este buffer puede ser sobreescrito por una llamada "
"posterior.  En caso de error, el estado de la terminal se restablece, se "
"pone un valor adecuado en I<errno>, y se devuelve NULL."

#. type: Plain text
#: opensuse-leap-15-6
msgid "The function may fail if"
msgstr "La función puede fallar si"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
