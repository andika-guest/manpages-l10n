# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Vicente Pastor Gómez <vpastorg@santandersupernet.com>, 1998.
# Juan Piernas <piernas@ditec.um.es>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:10+0200\n"
"PO-Revision-Date: 2000-04-18 19:53+0200\n"
"Last-Translator: Juan Piernas <piernas@ditec.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "mtrace"
msgstr "mtrace"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 ​​Julio 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Páginas de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "mtrace, muntrace - malloc debugging"
msgid "mtrace, muntrace - malloc tracing"
msgstr "mtrace, muntrace - rutinas de depuración para la biblioteca malloc"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<#include E<lt>macheck.hE<gt>>"
msgid "B<#include E<lt>mcheck.hE<gt>>\n"
msgstr "B<#include E<lt>macheck.hE<gt>>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<void muntrace(void);>"
msgid ""
"B<void mtrace(void);>\n"
"B<void muntrace(void);>\n"
msgstr "B<void muntrace(void);>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<mtrace>()  function installs hook functions for the memory-allocation "
"functions (B<malloc>(3), B<realloc>(3)  B<memalign>(3), B<free>(3)).  These "
"hook functions record tracing information about memory allocation and "
"deallocation.  The tracing information can be used to discover memory leaks "
"and attempts to free nonallocated memory in a program."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<muntrace>()  function disables the hook functions installed by "
"B<mtrace>(), so that tracing information is no longer recorded for the "
"memory-allocation functions.  If no hook functions were successfully "
"installed by B<mtrace>(), B<muntrace>()  does nothing."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When B<mtrace>()  is called, it checks the value of the environment variable "
"B<MALLOC_TRACE>, which should contain the pathname of a file in which the "
"tracing information is to be recorded.  If the pathname is successfully "
"opened, it is truncated to zero length."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If B<MALLOC_TRACE> is not set, or the pathname it specifies is invalid or "
"not writable, then no hook functions are installed, and B<mtrace>()  has no "
"effect.  In set-user-ID and set-group-ID programs, B<MALLOC_TRACE> is "
"ignored, and B<mtrace>()  has no effect."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<mtrace>(),\n"
"B<muntrace>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe"
msgstr ""

#.  FIXME: The marking is different from that in the glibc manual,
#.  markings in glibc manual are more detailed:
#.       mtrace: MT-Unsafe env race:mtrace const:malloc_hooks init
#.       muntrace: MT-Unsafe race:mtrace const:malloc_hooks locale
#.  But there is something wrong in glibc manual, for example:
#.  glibc manual says muntrace should have marking locale because it calls
#.  fprintf(), but muntrace does not execute area which cause locale problem.
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In normal usage, B<mtrace>()  is called once at the start of execution of a "
"program, and B<muntrace>()  is never called."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The tracing output produced after a call to B<mtrace>()  is textual, but not "
"designed to be human readable.  The GNU C library provides a Perl script, "
"B<mtrace>(1), that interprets the trace log and produces human-readable "
"output.  For best results, the traced program should be compiled with "
"debugging enabled, so that line-number information is recorded in the "
"executable."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The tracing performed by B<mtrace>()  incurs a performance penalty (if "
"B<MALLOC_TRACE> points to a valid, writable pathname)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The line-number information produced by B<mtrace>(1)  is not always precise: "
"the line number references may refer to the previous or following "
"(nonblank)  line of the source code."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EJEMPLOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The shell session below demonstrates the use of the B<mtrace>()  function "
"and the B<mtrace>(1)  command in a program that has memory leaks at two "
"different locations.  The demonstration uses the following program:"
msgstr ""

#.  SRC BEGIN (t_mtrace.c)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "$ B<cat t_mtrace.c>"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ""
"#include E<lt>mcheck.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    mtrace();\n"
"\\&\n"
"    for (unsigned int j = 0; j E<lt> 2; j++)\n"
"        malloc(100);            /* Never freed--a memory leak */\n"
"\\&\n"
"    calloc(16, 16);             /* Never freed--a memory leak */\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When we run the program as follows, we see that B<mtrace>()  diagnosed "
"memory leaks at two different locations in the program:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<cc -g t_mtrace.c -o t_mtrace>\n"
"$ B<export MALLOC_TRACE=/tmp/t>\n"
"$ B<./t_mtrace>\n"
"$ B<mtrace ./t_mtrace $MALLOC_TRACE>\n"
"Memory not freed:\n"
"-----------------\n"
"   Address     Size     Caller\n"
"0x084c9378     0x64  at /home/cecilia/t_mtrace.c:12\n"
"0x084c93e0     0x64  at /home/cecilia/t_mtrace.c:12\n"
"0x084c9448    0x100  at /home/cecilia/t_mtrace.c:16\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The first two messages about unfreed memory correspond to the two "
"B<malloc>(3)  calls inside the I<for> loop.  The final message corresponds "
"to the call to B<calloc>(3)  (which in turn calls B<malloc>(3))."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "B<malloc>(3), B<malloc_hook>(3)"
msgid "B<mtrace>(1), B<malloc>(3), B<malloc_hook>(3), B<mcheck>(3)"
msgstr "B<malloc>(3), B<malloc_hook>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15 Diciembre 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "These functions are GNU extensions."
msgstr "Estas funciones son extensiones de GNU."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<#include E<lt>sys/types.hE<gt>>\n"
#| "B<#include E<lt>sys/socket.hE<gt>>\n"
#| "B<#include E<lt>netdb.hE<gt>>\n"
msgid ""
"#include E<lt>mcheck.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/socket.hE<gt>>\n"
"B<#include E<lt>netdb.hE<gt>>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    mtrace();\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    for (unsigned int j = 0; j E<lt> 2; j++)\n"
"        malloc(100);            /* Never freed--a memory leak */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    calloc(16, 16);             /* Never freed--a memory leak */\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Páginas de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "MTRACE"
msgstr "MTRACE"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "B<#include E<lt>macheck.hE<gt>>"
msgid "B<#include E<lt>mcheck.hE<gt>>"
msgstr "B<#include E<lt>macheck.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<void mtrace(void);>"
msgstr "B<void mtrace(void);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<void muntrace(void);>"
msgstr "B<void muntrace(void);>"

#.  FIXME: The marking is different from that in the glibc manual,
#.  markings in glibc manual are more detailed:
#.       mtrace: MT-Unsafe env race:mtrace const:malloc_hooks init
#.       muntrace: MT-Unsafe race:mtrace const:malloc_hooks locale
#.  But there is something wrong in glibc manual, for example:
#.  glibc manual says muntrace should have marking locale because it calls
#.  fprintf(), but muntrace does not execute area which cause locale problem.
#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "EJEMPLO"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"$ B<cat t_mtrace.c>\n"
"#include E<lt>mcheck.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int j;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int j;\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    mtrace();\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"    for (j = 0; j E<lt> 2; j++)\n"
"        malloc(100);            /* Never freed--a memory leak */\n"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
