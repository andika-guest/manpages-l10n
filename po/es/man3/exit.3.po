# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:56+0200\n"
"PO-Revision-Date: 2005-02-14 19:53+0200\n"
"Last-Translator: Miguel Pérez Ibars <mpi79470@alu.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<exit>()"
msgid "exit"
msgstr "B<exit>()"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 ​​Julio 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Páginas de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "exit - cause normal program termination"
msgid "exit - cause normal process termination"
msgstr "exit - produce la terminación normal del programa"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<void exit(int >I<status>B<);>\n"
msgid "B<[[noreturn]] void exit(int >I<status>B<);>\n"
msgstr "B<void exit(int >I<status>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<exit>()  function causes normal process termination and the least "
"significant byte of I<status> (i.e., I<status & 0xFF>) is returned to the "
"parent (see B<wait>(2))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All functions registered with B<atexit>(3)  and B<on_exit>(3)  are called, "
"in the reverse order of their registration.  (It is possible for one of "
"these functions to use B<atexit>(3)  or B<on_exit>(3)  to register an "
"additional function to be executed during exit processing; the new "
"registration is added to the front of the list of functions that remain to "
"be called.)  If one of these functions does not return (e.g., it calls "
"B<_exit>(2), or kills itself with a signal), then none of the remaining "
"functions is called, and further exit processing (in particular, flushing of "
"B<stdio>(3)  streams) is abandoned.  If a function has been registered "
"multiple times using B<atexit>(3)  or B<on_exit>(3), then it is called as "
"many times as it was registered."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All open B<stdio>(3)  streams are flushed and closed.  Files created by "
"B<tmpfile>(3)  are removed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The C standard specifies two constants, B<EXIT_SUCCESS> and B<EXIT_FAILURE>, "
"that may be passed to B<exit>()  to indicate successful or unsuccessful "
"termination, respectively."
msgstr ""
"El estándar C especifica dos constantes B<EXIT_SUCCESS> y B<EXIT_FAILURE> "
"que pueden ser pasadas a B<exit>() para indicar una terminación con o sin "
"éxito, respectivamente."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<exit>()  function does not return."
msgstr "La función B<exit>() no devuelve nada ni regresa."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<exit>()"
msgstr "B<exit>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:exit"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<exit>()  function uses a global variable that is not protected, so it "
"is not thread-safe."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgid "C89, POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The behavior is undefined if one of the functions registered using "
"B<atexit>(3)  and B<on_exit>(3)  calls either B<exit>()  or B<longjmp>(3).  "
"Note that a call to B<execve>(2)  removes registrations created using "
"B<atexit>(3)  and B<on_exit>(3)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The use of B<EXIT_SUCCESS> and B<EXIT_FAILURE> is slightly more portable (to "
"non-UNIX environments) than the use of 0 and some nonzero value like 1 or "
"-1.  In particular, VMS uses a different convention."
msgstr ""
"El uso de B<EXIT_SUCCESS> y B<EXIT_FAILURE> es ligeramente más portable "
"(para entornos no UNIX) que el de 0 y algún valor distinto de cero como 1 o "
"-1. En particular, VMS usa una convención diferente."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "BSD has attempted to standardize exit codes - see the file "
#| "I<E<lt>sysexits.hE<gt>>."
msgid ""
"BSD has attempted to standardize exit codes (which some C libraries such as "
"the GNU C library have also adopted); see the file I<E<lt>sysexits.hE<gt>>."
msgstr ""
"BSD ha intentado estandarizar códigos de salida - véase el fichero "
"I<E<lt>sysexits.hE<gt>>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"After B<exit>(), the exit status must be transmitted to the parent process.  "
"There are three cases:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the parent has set B<SA_NOCLDWAIT>, or has set the B<SIGCHLD> handler to "
"B<SIG_IGN>, the status is discarded and the child dies immediately."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the parent was waiting on the child, it is notified of the exit status "
"and the child dies immediately."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Otherwise, the child becomes a \"zombie\" process: most of the process "
"resources are recycled, but a slot containing minimal information about the "
"child process (termination status, resource usage statistics) is retained in "
"process table.  This allows the parent to subsequently use B<waitpid>(2)  "
"(or similar) to learn the termination status of the child; at that point the "
"zombie process slot is released."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the implementation supports the B<SIGCHLD> signal, this signal is sent to "
"the parent.  If the parent has set B<SA_NOCLDWAIT>, it is undefined whether "
"a B<SIGCHLD> signal is sent."
msgstr ""
"Si la implementación soporta la señal B<SIGCHLD>, se envía esta señal al "
"padre. Si el padre tiene definido B<SA_NOCLDWAIT>, es indefinido si se envía "
"esta señal B<SIGCHLD>."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Signals sent to other processes"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If the process is a session leader and its controlling terminal the "
#| "controlling terminal of the session, then each process in the foreground "
#| "process group of this controlling terminal is sent a SIGHUP signal, and "
#| "the terminal is disassociated from this session, allowing it to be "
#| "acquired by a new controlling process."
msgid ""
"If the exiting process is a session leader and its controlling terminal is "
"the controlling terminal of the session, then each process in the foreground "
"process group of this controlling terminal is sent a B<SIGHUP> signal, and "
"the terminal is disassociated from this session, allowing it to be acquired "
"by a new controlling process."
msgstr ""
"Si el grupo es líder de un grupo de procesos y su terminal de control es la "
"terminal de control de la sesión, cada proceso que esté en primer plano "
"dentro del grupo de procesos de esta terminal de control recibe una señal "
"SIGHUP, y la terminal se desvincula de la sesión, permitiendo ser adquirida "
"por un nuevo proceso controlador."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If the exit of the process causes a process group to become orphaned, and "
#| "if any member of the newly-orphaned process group is stopped, then a "
#| "SIGHUP signal followed by a SIGCONT signal will be sent to each process "
#| "in this process group."
msgid ""
"If the exit of the process causes a process group to become orphaned, and if "
"any member of the newly orphaned process group is stopped, then a B<SIGHUP> "
"signal followed by a B<SIGCONT> signal will be sent to each process in this "
"process group.  See B<setpgid>(2)  for an explanation of orphaned process "
"groups."
msgstr ""
"Si la salida del proceso provoca que un grupo de procesos se quede huérfano, "
"y si cualquier miembro del grupo de procesos que se acaba de quedar huérfano "
"es parado, se envía una señal SIGHUP seguida de una señal SIGCONT a cada "
"proceso del grupo de procesos."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Except in the above cases, where the signalled processes may be children of "
"the terminating process, termination of a process does I<not> in general "
"cause a signal to be sent to children of that process.  However, a process "
"can use the B<prctl>(2)  B<PR_SET_PDEATHSIG> operation to arrange that it "
"receives a signal if its parent terminates."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<_exit>(2), B<get_robust_list>(2), B<setpgid>(2), B<wait>(2), B<atexit>(3), "
"B<on_exit>(3), B<tmpfile>(3)"
msgstr ""
"B<_exit>(2), B<get_robust_list>(2), B<setpgid>(2), B<wait>(2), B<atexit>(3), "
"B<on_exit>(3), B<tmpfile>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Páginas de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXIT"
msgstr "EXIT"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void exit(int >I<status>B<);>\n"
msgstr "B<void exit(int >I<status>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<exit>()  function causes normal process termination and the value of "
"I<status & 0377> is returned to the parent (see B<wait>(2))."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: Plain text
#: opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "BSD has attempted to standardize exit codes - see the file "
#| "I<E<lt>sysexits.hE<gt>>."
msgid ""
"BSD has attempted to standardize exit codes; see the file I<E<lt>sysexits."
"hE<gt>>."
msgstr ""
"BSD ha intentado estandarizar códigos de salida - véase el fichero "
"I<E<lt>sysexits.hE<gt>>."

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
