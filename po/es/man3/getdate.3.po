# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:58+0200\n"
"PO-Revision-Date: 2021-01-28 20:39+0100\n"
"Last-Translator: Miguel Pérez Ibars <mpi79470@alu.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getdate>()"
msgid "getdate"
msgstr "B<getdate>()"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 ​​Julio 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Páginas de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "getdate() - convert a string to struct tm"
msgid ""
"getdate, getdate_r - convert a date-plus-time string to broken-down time"
msgstr "getdate() - convierte una cadena a una estructura tm"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>time.hE<gt>>\n"
msgstr "B<#include E<lt>time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<struct tm *getdate(const char *>I<string>B<);>"
msgid "B<struct tm *getdate(const char *>I<string>B<);>\n"
msgstr "B<struct tm *getdate(const char *>I<string>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<extern int getdate_err;>"
msgid "B<extern int getdate_err;>\n"
msgstr "B<extern int getdate_err;>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int getdate_r(const char *>I<string>B<, struct tm *>I<res>B<);>"
msgid "B<int getdate_r(const char *restrict >I<string>B<, struct tm *restrict >I<res>B<);>\n"
msgstr "B<int getdate_r(const char *>I<string>B<, struct tm *>I<res>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de Macros de Prueba de Características para glibc (véase "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getdate>():"
msgstr "B<getdate>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _XOPEN_SOURCE E<gt>= 500\n"
msgstr "    _XOPEN_SOURCE E<gt>= 500\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getdate_r>():"
msgstr "B<getdate_r>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The function B<getdate()> converts a string pointed to by I<string> into "
#| "the tm structure that it returns.  This tm structure may be found in "
#| "static storage, so that it will be overwritten by the next call."
msgid ""
"The function B<getdate>()  converts a string representation of a date and "
"time, contained in the buffer pointed to by I<string>, into a broken-down "
"time.  The broken-down time is stored in a I<tm> structure, and a pointer to "
"this structure is returned as the function result.  This I<tm> structure is "
"allocated in static storage, and consequently it will be overwritten by "
"further calls to B<getdate>()."
msgstr ""
"La función B<getdate()> convierte la cadena apuntada por I<string> en una "
"estructura tm que es devuelta por la función.  Esta estructura tm puede "
"encontrarse en almacenamiento estático, por lo que puede ser sobreescrita "
"por una llamada posterior."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In contrast to B<strptime>(3), (which has a I<format> argument), "
"B<getdate>()  uses the formats found in the file whose full pathname is "
"given in the environment variable B<DATEMSK>.  The first line in the file "
"that matches the given input string is used for the conversion."
msgstr ""
"En contraste con B<strptime>(3), (que tiene un argumento I<format> ), "
"B<getdate>() usa los formatos situados en el fichero del cual se especifica "
"la ruta completa en la variable de entorno B<DATEMSK>.  La primera línea del "
"fichero que concuerde con la cadena de entrada pasada se utiliza para la "
"conversión."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The matching is done case insensitively.  Superfluous whitespace, either in "
"the pattern or in the string to be converted, is ignored."
msgstr ""
"La correspondencia se hace sin tener en cuenta las mayúsculas.  Los espacios "
"innecesarios, ya sea en el patrón o en la cadena que debe ser convertida, se "
"ignoran."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The conversion specifications that a pattern can contain are those given "
#| "for B<strptime>(3).  One more conversion specification is accepted:"
msgid ""
"The conversion specifications that a pattern can contain are those given for "
"B<strptime>(3).  One more conversion specification is specified in "
"POSIX.1-2001:"
msgstr ""
"Las especificaciones de conversión que puede contener un patrón son aquellas "
"que proporciona B<strptime>(3).  Se acepta una especificación de conversión "
"adicional:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<%Z>"
msgstr "B<%Z>"

#.  FIXME Is it (still) true that %Z is not supported in glibc?
#.  Looking at the glibc 2.21 source code, where the implementation uses
#.  strptime(), suggests that it might be supported.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Timezone name.  This is not implemented in glibc."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "When B<%Z> is given, the value to be returned is initialised to the "
#| "broken-down time corresponding to the current time in the given time "
#| "zone.  Otherwise, it is initialised to the broken-down time corresponding "
#| "to the current local time."
msgid ""
"When B<%Z> is given, the structure containing the broken-down time is "
"initialized with values corresponding to the current time in the given "
"timezone.  Otherwise, the structure is initialized to the broken-down time "
"corresponding to the current local time (as by a call to B<localtime>(3))."
msgstr ""
"Cuando se proporciona B<%Z>, el valor que se devuelve se inicializa con el "
"valor de tiempo descompuesto correspondiente a la hora actual en la zona "
"horaria dada.  En otro caso, se inicializa con el valor de tiempo "
"descompuesto correspondiente a la hora local actual."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When only the day of the week is given, the day is taken to be the first "
"such day on or after today."
msgstr ""
"Cuando sólo se especifica el día de la semana, el día que se toma es el "
"primero tras el día actual (que podría ser el propio día actual)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When only the month is given (and no year), the month is taken to be the "
"first such month equal to or after the current month.  If no day is given, "
"it is the first day of the month."
msgstr ""
"Cuando sólo se especifica el mes (y no el año), el mes que se toma es el "
"primero tras el mes actual (que podría ser el propio mes actual).  Si no se "
"especifica el día, se toma el primer día del mes."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "When no hour, minute and second are given, the current hour, minute and "
#| "second are taken."
msgid ""
"When no hour, minute, and second are given, the current hour, minute, and "
"second are taken."
msgstr ""
"Cuando no se dan ni hora, ni minutos, ni segundos, se toman la hora, minutos "
"y segundos actuales."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If no date is given, but we know the hour, then that hour is taken to be the "
"first such hour equal to or after the current hour."
msgstr ""
"Si no se especifica la fecha, pero sabemos la hora, entonces la hora se toma "
"para que sea la primera tras la hora actual (que podría ser la propia hora "
"actual)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<getdate_r>()  is a GNU extension that provides a reentrant version of "
"B<getdate>().  Rather than using a global variable to report errors and a "
"static buffer to return the broken down time, it returns errors via the "
"function result value, and returns the resulting broken-down time in the "
"caller-allocated buffer pointed to by the argument I<res>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "When successful, this function returns a pointer to a B<struct tm>.  "
#| "Otherwise, it returns NULL and sets the global variable B<getdate_err>.  "
#| "Changes to I<errno> are unspecified.  The following values for "
#| "B<getdate_err> are defined:"
msgid ""
"When successful, B<getdate>()  returns a pointer to a I<struct tm>.  "
"Otherwise, it returns NULL and sets the global variable I<getdate_err> to "
"one of the error numbers shown below.  Changes to I<errno> are unspecified."
msgstr ""
"Cuando tiene éxito, esta función devuelve un puntero a una B<estructura "
"tm>.  En otro caso, devuelve NULL y modifica la variable global "
"B<getdate_err>.  Los cambios a I<errno> son indefinidos. Se definen los "
"siguientes valores para B<getdate_err:>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success B<getdate_r>()  returns 0; on error it returns one of the error "
"numbers shown below."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following errors are returned via I<getdate_err> (for B<getdate>())  or "
"as the function result (for B<getdate_r>()):"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "The DATEMSK environment variable is null or undefined."
msgid ""
"The B<DATEMSK> environment variable is not defined, or its value is an empty "
"string."
msgstr "La variable de entorno DATEMSK es null o no está definida."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<2>"
msgstr "B<2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "The template file cannot be opened for reading."
msgid "The template file specified by B<DATEMSK> cannot be opened for reading."
msgstr "No se ha podido abrir el fichero de plantillas para lectura."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<3>"
msgstr "B<3>"

#.  stat()
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Failed to get file status information."
msgstr "Fallo al obtener información del estado del fichero."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<4>"
msgstr "B<4>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The template file is not a regular file."
msgstr "El fichero de plantillas no es un fichero regular."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<5>"
msgstr "B<5>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An error was encountered while reading the template file."
msgstr "Se ha producido un error al leer el fichero de plantillas."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<6>"
msgstr "B<6>"

#.  Error 6 doesn't seem to occur in glibc
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Memory allocation failed (not enough memory available)."
msgstr "Fallo en la reserva de memoria (no hay suficiente memoria disponible)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<7>"
msgstr "B<7>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "There is no line in the file that matches the input."
msgstr "No hay ninguna línea en el fichero que concuerde con la entrada."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<8>"
msgstr "B<8>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Invalid input specification."
msgstr "Especificación inválida de entrada."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ENTORNO"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<DATEMSK>"
msgstr "B<DATEMSK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "File containing format patterns."
msgstr "Fichero que contiene patrones de formato."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<TZ>, B<LC_TIME>"
msgstr "B<TZ>, B<LC_TIME>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Variables used by B<strptime>(3)."
msgstr "Variables usadas por B<strptime>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getdate>()"
msgstr "B<getdate>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:getdate env locale"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getdate_r>()"
msgstr "B<getdate_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Safe locale"
msgid "MT-Safe env locale"
msgstr "Configuración regional de multi-hilo seguro"

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The POSIX.1 specification for B<strptime>(3)  contains conversion "
"specifications using the B<%E> or B<%O> modifier, while such specifications "
"are not given for B<getdate>().  In glibc, B<getdate>()  is implemented "
"using B<strptime>(3), so that precisely the same conversions are supported "
"by both."
msgstr ""
"La especificación POSIX.1 para B<strptime>(3) contiene especificaciones de "
"conversión que usan el modificador B<%E> o B<%O>, mientras que tales "
"especificaciones no se dan para B<getdate>().  La implementación de glibc "
"implementa B<getdate>() usando B<strptime>(3) por lo que automáticamente las "
"mismas conversiones son soportadas por ambas."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EJEMPLOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program below calls B<getdate>()  for each of its command-line "
"arguments, and for each call displays the values in the fields of the "
"returned I<tm> structure.  The following shell session demonstrates the "
"operation of the program:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< TFILE=$PWD/tfile>\n"
"$B< echo \\[aq]%A\\[aq] E<gt> $TFILE >      # Full name of the day of the week\n"
"$B< echo \\[aq]%T\\[aq] E<gt>E<gt> $TFILE>      # Time (HH:MM:SS)\n"
"$B< echo \\[aq]%F\\[aq] E<gt>E<gt> $TFILE>      # ISO date (YYYY-MM-DD)\n"
"$B< date>\n"
"$B< export DATEMSK=$TFILE>\n"
"$B< ./a.out Tuesday \\[aq]2009-12-28\\[aq] \\[aq]12:22:33\\[aq]>\n"
"Sun Sep  7 06:03:36 CEST 2008\n"
"Call 1 (\"Tuesday\") succeeded:\n"
"    tm_sec   = 36\n"
"    tm_min   = 3\n"
"    tm_hour  = 6\n"
"    tm_mday  = 9\n"
"    tm_mon   = 8\n"
"    tm_year  = 108\n"
"    tm_wday  = 2\n"
"    tm_yday  = 252\n"
"    tm_isdst = 1\n"
"Call 2 (\"2009-12-28\") succeeded:\n"
"    tm_sec   = 36\n"
"    tm_min   = 3\n"
"    tm_hour  = 6\n"
"    tm_mday  = 28\n"
"    tm_mon   = 11\n"
"    tm_year  = 109\n"
"    tm_wday  = 1\n"
"    tm_yday  = 361\n"
"    tm_isdst = 0\n"
"Call 3 (\"12:22:33\") succeeded:\n"
"    tm_sec   = 33\n"
"    tm_min   = 22\n"
"    tm_hour  = 12\n"
"    tm_mday  = 7\n"
"    tm_mon   = 8\n"
"    tm_year  = 108\n"
"    tm_wday  = 0\n"
"    tm_yday  = 250\n"
"    tm_isdst = 1\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Código fuente"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, fuzzy, no-wrap
#| msgid ""
#| "        printf(\"Call %d (\\e\"%s\\e\") succeeded:\\en\", j, argv[j]);\n"
#| "        printf(\"    tm_sec   = %d\\en\", tmp-E<gt>tm_sec);\n"
#| "        printf(\"    tm_min   = %d\\en\", tmp-E<gt>tm_min);\n"
#| "        printf(\"    tm_hour  = %d\\en\", tmp-E<gt>tm_hour);\n"
#| "        printf(\"    tm_mday  = %d\\en\", tmp-E<gt>tm_mday);\n"
#| "        printf(\"    tm_mon   = %d\\en\", tmp-E<gt>tm_mon);\n"
#| "        printf(\"    tm_year  = %d\\en\", tmp-E<gt>tm_year);\n"
#| "        printf(\"    tm_wday  = %d\\en\", tmp-E<gt>tm_wday);\n"
#| "        printf(\"    tm_yday  = %d\\en\", tmp-E<gt>tm_yday);\n"
#| "        printf(\"    tm_isdst = %d\\en\", tmp-E<gt>tm_isdst);\n"
#| "    }\n"
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct tm *tmp;\n"
"\\&\n"
"    for (size_t j = 1; j E<lt> argc; j++) {\n"
"        tmp = getdate(argv[j]);\n"
"\\&\n"
"        if (tmp == NULL) {\n"
"            printf(\"Call %zu failed; getdate_err = %d\\en\",\n"
"                   j, getdate_err);\n"
"            continue;\n"
"        }\n"
"\\&\n"
"        printf(\"Call %zu (\\e\"%s\\e\") succeeded:\\en\", j, argv[j]);\n"
"        printf(\"    tm_sec   = %d\\en\", tmp-E<gt>tm_sec);\n"
"        printf(\"    tm_min   = %d\\en\", tmp-E<gt>tm_min);\n"
"        printf(\"    tm_hour  = %d\\en\", tmp-E<gt>tm_hour);\n"
"        printf(\"    tm_mday  = %d\\en\", tmp-E<gt>tm_mday);\n"
"        printf(\"    tm_mon   = %d\\en\", tmp-E<gt>tm_mon);\n"
"        printf(\"    tm_year  = %d\\en\", tmp-E<gt>tm_year);\n"
"        printf(\"    tm_wday  = %d\\en\", tmp-E<gt>tm_wday);\n"
"        printf(\"    tm_yday  = %d\\en\", tmp-E<gt>tm_yday);\n"
"        printf(\"    tm_isdst = %d\\en\", tmp-E<gt>tm_isdst);\n"
"    }\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"        printf(\"Call %d (\\e\"%s\\e\") succeeded:\\en\", j, argv[j]);\n"
"        printf(\"    tm_sec   = %d\\en\", tmp-E<gt>tm_sec);\n"
"        printf(\"    tm_min   = %d\\en\", tmp-E<gt>tm_min);\n"
"        printf(\"    tm_hour  = %d\\en\", tmp-E<gt>tm_hour);\n"
"        printf(\"    tm_mday  = %d\\en\", tmp-E<gt>tm_mday);\n"
"        printf(\"    tm_mon   = %d\\en\", tmp-E<gt>tm_mon);\n"
"        printf(\"    tm_year  = %d\\en\", tmp-E<gt>tm_year);\n"
"        printf(\"    tm_wday  = %d\\en\", tmp-E<gt>tm_wday);\n"
"        printf(\"    tm_yday  = %d\\en\", tmp-E<gt>tm_yday);\n"
"        printf(\"    tm_isdst = %d\\en\", tmp-E<gt>tm_isdst);\n"
"    }\n"

#. #-#-#-#-#  archlinux: getdate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: getdate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: getdate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-39: getdate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: getdate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: getdate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-6: getdate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: getdate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<time>(2), B<localtime>(3), B<setlocale>(3), B<strftime>(3), B<strptime>(3)"
msgstr ""
"B<time>(2), B<localtime>(3), B<setlocale>(3), B<strftime>(3), B<strptime>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "#define _GNU_SOURCE\n"
#| "#include E<lt>time.hE<gt>\n"
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct tm *tmp;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct tm *tmp;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    for (int j = 1; j E<lt> argc; j++) {\n"
#| "        tmp = getdate(argv[j]);\n"
msgid ""
"    for (size_t j = 1; j E<lt> argc; j++) {\n"
"        tmp = getdate(argv[j]);\n"
msgstr ""
"    for (int j = 1; j E<lt> argc; j++) {\n"
"        tmp = getdate(argv[j]);\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "        if (tmp == NULL) {\n"
#| "            printf(\"Call %d failed; getdate_err = %d\\en\",\n"
#| "                   j, getdate_err);\n"
#| "            continue;\n"
#| "        }\n"
msgid ""
"        if (tmp == NULL) {\n"
"            printf(\"Call %zu failed; getdate_err = %d\\en\",\n"
"                   j, getdate_err);\n"
"            continue;\n"
"        }\n"
msgstr ""
"        if (tmp == NULL) {\n"
"            printf(\"Call %d failed; getdate_err = %d\\en\",\n"
"                   j, getdate_err);\n"
"            continue;\n"
"        }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "        printf(\"Call %d (\\e\"%s\\e\") succeeded:\\en\", j, argv[j]);\n"
#| "        printf(\"    tm_sec   = %d\\en\", tmp-E<gt>tm_sec);\n"
#| "        printf(\"    tm_min   = %d\\en\", tmp-E<gt>tm_min);\n"
#| "        printf(\"    tm_hour  = %d\\en\", tmp-E<gt>tm_hour);\n"
#| "        printf(\"    tm_mday  = %d\\en\", tmp-E<gt>tm_mday);\n"
#| "        printf(\"    tm_mon   = %d\\en\", tmp-E<gt>tm_mon);\n"
#| "        printf(\"    tm_year  = %d\\en\", tmp-E<gt>tm_year);\n"
#| "        printf(\"    tm_wday  = %d\\en\", tmp-E<gt>tm_wday);\n"
#| "        printf(\"    tm_yday  = %d\\en\", tmp-E<gt>tm_yday);\n"
#| "        printf(\"    tm_isdst = %d\\en\", tmp-E<gt>tm_isdst);\n"
#| "    }\n"
msgid ""
"        printf(\"Call %zu (\\e\"%s\\e\") succeeded:\\en\", j, argv[j]);\n"
"        printf(\"    tm_sec   = %d\\en\", tmp-E<gt>tm_sec);\n"
"        printf(\"    tm_min   = %d\\en\", tmp-E<gt>tm_min);\n"
"        printf(\"    tm_hour  = %d\\en\", tmp-E<gt>tm_hour);\n"
"        printf(\"    tm_mday  = %d\\en\", tmp-E<gt>tm_mday);\n"
"        printf(\"    tm_mon   = %d\\en\", tmp-E<gt>tm_mon);\n"
"        printf(\"    tm_year  = %d\\en\", tmp-E<gt>tm_year);\n"
"        printf(\"    tm_wday  = %d\\en\", tmp-E<gt>tm_wday);\n"
"        printf(\"    tm_yday  = %d\\en\", tmp-E<gt>tm_yday);\n"
"        printf(\"    tm_isdst = %d\\en\", tmp-E<gt>tm_isdst);\n"
"    }\n"
msgstr ""
"        printf(\"Call %d (\\e\"%s\\e\") succeeded:\\en\", j, argv[j]);\n"
"        printf(\"    tm_sec   = %d\\en\", tmp-E<gt>tm_sec);\n"
"        printf(\"    tm_min   = %d\\en\", tmp-E<gt>tm_min);\n"
"        printf(\"    tm_hour  = %d\\en\", tmp-E<gt>tm_hour);\n"
"        printf(\"    tm_mday  = %d\\en\", tmp-E<gt>tm_mday);\n"
"        printf(\"    tm_mon   = %d\\en\", tmp-E<gt>tm_mon);\n"
"        printf(\"    tm_year  = %d\\en\", tmp-E<gt>tm_year);\n"
"        printf(\"    tm_wday  = %d\\en\", tmp-E<gt>tm_wday);\n"
"        printf(\"    tm_yday  = %d\\en\", tmp-E<gt>tm_yday);\n"
"        printf(\"    tm_isdst = %d\\en\", tmp-E<gt>tm_isdst);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Páginas de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GETDATE"
msgstr "GETDATE"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>time.hE<gt>>"
msgstr "B<#include E<lt>time.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<struct tm *getdate(const char *>I<string>B<);>"
msgstr "B<struct tm *getdate(const char *>I<string>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<extern int getdate_err;>"
msgstr "B<extern int getdate_err;>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int getdate_r(const char *>I<string>B<, struct tm *>I<res>B<);>"
msgstr "B<int getdate_r(const char *>I<string>B<, struct tm *>I<res>B<);>"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-leap-15-6
msgid "_XOPEN_SOURCE\\ E<gt>=\\ 500"
msgstr "_XOPEN_SOURCE\\ E<gt>=\\ 500"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_GNU_SOURCE"
msgstr "_GNU_SOURCE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"When no hour, minute and second are given, the current hour, minute and "
"second are taken."
msgstr ""
"Cuando no se dan ni hora, ni minutos, ni segundos, se toman la hora, minutos "
"y segundos actuales."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "EJEMPLO"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"$B< TFILE=$PWD/tfile>\n"
"$B< echo \\(aq%A\\(aq E<gt> $TFILE >      # Full name of the day of the week\n"
"$B< echo \\(aq%T\\(aq E<gt>E<gt> $TFILE>      # ISO date (YYYY-MM-DD)\n"
"$B< echo \\(aq%F\\(aq E<gt>E<gt> $TFILE>      # Time (HH:MM:SS)\n"
"$B< date>\n"
"$B< export DATEMSK=$TFILE>\n"
"$B< ./a.out Tuesday \\(aq2009-12-28\\(aq \\(aq12:22:33\\(aq>\n"
"Sun Sep  7 06:03:36 CEST 2008\n"
"Call 1 (\"Tuesday\") succeeded:\n"
"    tm_sec   = 36\n"
"    tm_min   = 3\n"
"    tm_hour  = 6\n"
"    tm_mday  = 9\n"
"    tm_mon   = 8\n"
"    tm_year  = 108\n"
"    tm_wday  = 2\n"
"    tm_yday  = 252\n"
"    tm_isdst = 1\n"
"Call 2 (\"2009-12-28\") succeeded:\n"
"    tm_sec   = 36\n"
"    tm_min   = 3\n"
"    tm_hour  = 6\n"
"    tm_mday  = 28\n"
"    tm_mon   = 11\n"
"    tm_year  = 109\n"
"    tm_wday  = 1\n"
"    tm_yday  = 361\n"
"    tm_isdst = 0\n"
"Call 3 (\"12:22:33\") succeeded:\n"
"    tm_sec   = 33\n"
"    tm_min   = 22\n"
"    tm_hour  = 12\n"
"    tm_mday  = 7\n"
"    tm_mon   = 8\n"
"    tm_year  = 108\n"
"    tm_wday  = 0\n"
"    tm_yday  = 250\n"
"    tm_isdst = 1\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct tm *tmp;\n"
"    int j;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct tm *tmp;\n"
"    int j;\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"    for (j = 1; j E<lt> argc; j++) {\n"
"        tmp = getdate(argv[j]);\n"
msgstr ""
"    for (j = 1; j E<lt> argc; j++) {\n"
"        tmp = getdate(argv[j]);\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"        if (tmp == NULL) {\n"
"            printf(\"Call %d failed; getdate_err = %d\\en\",\n"
"                   j, getdate_err);\n"
"            continue;\n"
"        }\n"
msgstr ""
"        if (tmp == NULL) {\n"
"            printf(\"Call %d failed; getdate_err = %d\\en\",\n"
"                   j, getdate_err);\n"
"            continue;\n"
"        }\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"        printf(\"Call %d (\\e\"%s\\e\") succeeded:\\en\", j, argv[j]);\n"
"        printf(\"    tm_sec   = %d\\en\", tmp-E<gt>tm_sec);\n"
"        printf(\"    tm_min   = %d\\en\", tmp-E<gt>tm_min);\n"
"        printf(\"    tm_hour  = %d\\en\", tmp-E<gt>tm_hour);\n"
"        printf(\"    tm_mday  = %d\\en\", tmp-E<gt>tm_mday);\n"
"        printf(\"    tm_mon   = %d\\en\", tmp-E<gt>tm_mon);\n"
"        printf(\"    tm_year  = %d\\en\", tmp-E<gt>tm_year);\n"
"        printf(\"    tm_wday  = %d\\en\", tmp-E<gt>tm_wday);\n"
"        printf(\"    tm_yday  = %d\\en\", tmp-E<gt>tm_yday);\n"
"        printf(\"    tm_isdst = %d\\en\", tmp-E<gt>tm_isdst);\n"
"    }\n"
msgstr ""
"        printf(\"Call %d (\\e\"%s\\e\") succeeded:\\en\", j, argv[j]);\n"
"        printf(\"    tm_sec   = %d\\en\", tmp-E<gt>tm_sec);\n"
"        printf(\"    tm_min   = %d\\en\", tmp-E<gt>tm_min);\n"
"        printf(\"    tm_hour  = %d\\en\", tmp-E<gt>tm_hour);\n"
"        printf(\"    tm_mday  = %d\\en\", tmp-E<gt>tm_mday);\n"
"        printf(\"    tm_mon   = %d\\en\", tmp-E<gt>tm_mon);\n"
"        printf(\"    tm_year  = %d\\en\", tmp-E<gt>tm_year);\n"
"        printf(\"    tm_wday  = %d\\en\", tmp-E<gt>tm_wday);\n"
"        printf(\"    tm_yday  = %d\\en\", tmp-E<gt>tm_yday);\n"
"        printf(\"    tm_isdst = %d\\en\", tmp-E<gt>tm_isdst);\n"
"    }\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
