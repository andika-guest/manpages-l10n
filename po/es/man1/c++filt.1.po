# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-10-02 11:53+0200\n"
"PO-Revision-Date: 1999-04-29 19:53+0200\n"
"Last-Translator: Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#.  ========================================================================
#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Title"
msgstr "Title"

#.  ========================================================================
#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C++FILT 1"
msgstr "C++FILT 1"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C++FILT"
msgstr "C++FILT"

#. type: TH
#: archlinux
#, no-wrap
msgid "2023-08-04"
msgstr "4 Agosto 2023"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "binutils-2.40.00"
msgid "binutils-2.41.0"
msgstr "binutils-2.40.00"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "GNU Development Tools"
msgstr "Herramientas de desarrollo de GNU"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "c++filt - demangle C++ and Java symbols"
msgstr "c++filt - restaura nombres de símbolos de C++ y Java"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Header"
msgstr "Header"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"c++filt [B<-_>|B<--strip-underscore>]\n"
"        [B<-n>|B<--no-strip-underscore>]\n"
"        [B<-p>|B<--no-params>]\n"
"        [B<-t>|B<--types>]\n"
"        [B<-i>|B<--no-verbose>]\n"
"        [B<-r>|B<--no-recurse-limit>]\n"
"        [B<-R>|B<--recurse-limit>]\n"
"        [B<-s> I<format>|B<--format=>I<format>]\n"
"        [B<--help>]  [B<--version>]  [I<symbol>...]\n"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The C++ language provides function overloading, which means that you can "
#| "write many functions with the same name (providing each takes parameters "
#| "of different types).  All C++ function names are encoded into a low-level "
#| "assembly label (this process is known as I<mangling). The> B<c++filt> "
#| "program does the inverse mapping: it decodes (I<demangles>)  low-level "
#| "names into user-level names so that the linker can keep these overloaded "
#| "functions from clashing."
msgid ""
"The C++ and Java languages provide function overloading, which means that "
"you can write many functions with the same name, providing that each "
"function takes parameters of different types.  In order to be able to "
"distinguish these similarly named functions C++ and Java encode them into a "
"low-level assembler name which uniquely identifies each different version.  "
"This process is known as I<mangling>. The \\&B<c++filt> [1] program does the "
"inverse mapping: it decodes (I<demangles>) low-level names into user-level "
"names so that they can be read."
msgstr ""
"El lenguaje C++ proporciona sobrecarga de funciones, lo que significa que "
"uno puede escribir varias funciones con el mismo nombre (siempre que la "
"lista de argumentos de cada una sea distinta). Todos los nombres de "
"funciones de C++ se codifican a una etiqueta de ensamblador de bajo nivel "
"(este proceso se conoce como I<desfiguración (mangling)). El programa> B<c+"
"+filt> realiza la correspondencia inversa: descodifica (I<restaura, "
"demangles>) los nombres de bajo nivel a los nombres que dio el usuario de "
"forma que el enlazador pueda evitar que estas funciones sobrecargadas "
"colisionen."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Every alphanumeric word (consisting of letters, digits, underscores, "
#| "dollars, or periods) seen in the input is a potential label.  If the "
#| "label decodes into a C++ name, the C++ name replaces the low-level name "
#| "in the output."
msgid ""
"Every alphanumeric word (consisting of letters, digits, underscores, "
"dollars, or periods) seen in the input is a potential mangled name.  If the "
"name decodes into a C++ name, the C++ name replaces the low-level name in "
"the output, otherwise the original word is output.  In this way you can pass "
"an entire assembler source file, containing mangled names, through B<c+"
"+filt> and see the same source file containing demangled names."
msgstr ""
"Cada palabra alfanumérica (consistente en letras, dígitos, subrayados, "
"dólares o puntos) vista en la entrada, es una etiqueta en potencia. Si la "
"etiqueta se descodifica a un nombre de C++, este nombre de C++ reemplaza a "
"la etiqueta de bajo nivel en la salida."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "You can use B<c++filt> to decipher individual symbols by specifying these "
#| "symbols on the command line."
msgid ""
"You can also use B<c++filt> to decipher individual symbols by passing them "
"on the command line:"
msgstr ""
"Uno puede emplear B<c++filt> para descifrar símbolos individuales "
"especificándolos en la línea de órdenes."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\& c++filt E<lt>symbolE<gt>"
msgstr "\\& c++filt E<lt>símboloE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If no I<symbol> arguments are given, B<c++filt> reads symbol names from the "
"standard input instead.  All the results are printed on the standard "
"output.  The difference between reading names from the command line versus "
"reading names from the standard input is that command-line arguments are "
"expected to be just mangled names and no checking is performed to separate "
"them from surrounding text.  Thus for example:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\& c++filt -n _Z1fv"
msgstr "\\& c++filt -n _Z1fv"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "will work and demangle the name to \"f()\" whereas:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\& c++filt -n _Z1fv,"
msgstr "\\& c++filt -n _Z1fv,"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"will not work.  (Note the extra comma at the end of the mangled name which "
"makes it invalid).  This command however will work:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\& echo _Z1fv, | c++filt -n"
msgstr "\\& echo _Z1fv, | c++filt -n"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"and will display \"f(),\", i.e., the demangled name followed by a trailing "
"comma.  This behaviour is because when the names are read from the standard "
"input it is expected that they might be part of an assembler source file "
"where there might be extra, extraneous characters trailing after a mangled "
"name.  For example:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\& .type _Z1fv, @function"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-_>"
msgstr "B<-_>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Item"
msgstr "Item"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-_"
msgstr "-_"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--strip-underscore>"
msgstr "B<--strip-underscore>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--strip-underscore"
msgstr "--strip-underscore"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On some systems, both the C and C++ compilers put an underscore in front "
#| "of every name.  For example, the C name B<foo> gets the low-level name "
#| "B<_foo>.  This option removes the leading underscore."
msgid ""
"On some systems, both the C and C++ compilers put an underscore in front of "
"every name.  For example, the C name CW<\\*(C`foo\\*(C'> gets the low-level "
"name CW<\\*(C`_foo\\*(C'>.  This option removes the initial underscore.  "
"Whether \\&B<c++filt> removes the underscore by default is target dependent."
msgstr ""
"En algunos sistemas, los compiladores de C y de C++ ponen un subrayado "
"delante de cada nombre. Por ejemplo, el nombre de C B<fuu> se convierte en "
"el nombre de bajo nivel B<_fuu>.  Esta opción quita el subrayado inicial."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>"
msgstr "B<-n>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-n"
msgstr "-n"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-strip-underscore>"
msgstr "B<--no-strip-underscore>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--no-strip-underscore"
msgstr "--no-strip-underscore"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Do not remove the initial underscore."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>"
msgstr "B<-p>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-p"
msgstr "-p"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-params>"
msgstr "B<--no-params>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--no-params"
msgstr "--no-params"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When demangling the name of a function, do not display the types of the "
"function's parameters."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>"
msgstr "B<-t>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-t"
msgstr "-t"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--types>"
msgstr "B<--types>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--types"
msgstr "--types"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Attempt to demangle types as well as function names.  This is disabled by "
"default since mangled types are normally only used internally in the "
"compiler, and they can be confused with non-mangled names.  For example, a "
"function called \"a\" treated as a mangled type name would be demangled to "
"\"signed char\"."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>"
msgstr "B<-i>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-i"
msgstr "-i"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-verbose>"
msgstr "B<--no-verbose>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--no-verbose"
msgstr "--no-verbose"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Do not include implementation details (if any) in the demangled output."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>"
msgstr "B<-r>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-r"
msgstr "-r"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>"
msgstr "B<-R>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-R"
msgstr "-R"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--recurse-limit>"
msgstr "B<--recurse-limit>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--recurse-limit"
msgstr "--recurse-limit"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-recurse-limit>"
msgstr "B<--no-recurse-limit>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--no-recurse-limit"
msgstr "--no-recurse-limit"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--recursion-limit>"
msgstr "B<--recursion-limit>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--recursion-limit"
msgstr "--recursion-limit"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-recursion-limit>"
msgstr "B<--no-recursion-limit>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--no-recursion-limit"
msgstr "--no-recursion-limit"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Enables or disables a limit on the amount of recursion performed whilst "
"demangling strings.  Since the name mangling formats allow for an infinite "
"level of recursion it is possible to create strings whose decoding will "
"exhaust the amount of stack space available on the host machine, triggering "
"a memory fault.  The limit tries to prevent this from happening by "
"restricting recursion to 2048 levels of nesting."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The default is for this limit to be enabled, but disabling it may be "
"necessary in order to demangle truly complicated names.  Note however that "
"if the recursion limit is disabled then stack exhaustion is possible and any "
"bug reports about such an event will be rejected."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<-r> option is a synonym for the \\&B<--no-recurse-limit> option.  The "
"B<-R> option is a synonym for the B<--recurse-limit> option."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s> I<format>"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-s format"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--format=>I<format>"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--format=format"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"\\&B<c++filt> can decode various methods of mangling, used by different "
"compilers.  The argument to this option selects which method it uses:"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "auto"
msgstr "auto"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Automatic selection based on executable (the default method)"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "gnu"
msgstr "gnu"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "the one used by the GNU C++ compiler (g++)"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "lucid"
msgstr "lucid"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "the one used by the Lucid compiler (lcc)"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "arm"
msgstr "arm"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "the one specified by the C++ Annotated Reference Manual"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "hp"
msgstr "hp"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "the one used by the HP compiler (aCC)"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "edg"
msgstr "edg"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "the one used by the EDG compiler"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "gnu-v3"
msgstr "gnu-v3"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "the one used by the GNU C++ compiler (g++) with the V3 ABI."
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "java"
msgstr "java"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "the one used by the GNU Java compiler (gcj)"
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "gnat"
msgstr "gnat"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "the one used by the GNU Ada compiler (GNAT)."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--help"
msgstr "--help"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print a summary of the options to B<c++filt> and exit."
msgstr "Muestra un sumario de las opciones de B<c++filt> y acaba."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--version"
msgstr "--version"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print the version number of B<c++filt> and exit."
msgstr "Muestra el número de versión de B<c++filt> y acaba."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<@>I<file>"
msgstr "B<@>I<archivo>"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "@file"
msgstr "@archivo"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Read command-line options from I<file>.  The options read are inserted in "
"place of the original @I<file> option.  If I<file> does not exist, or cannot "
"be read, then the option will be treated literally, and not removed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Options in I<file> are separated by whitespace.  A whitespace character may "
"be included in an option by surrounding the entire option in either single "
"or double quotes.  Any character (including a backslash) may be included by "
"prefixing the character to be included with a backslash.  The I<file> may "
"itself contain additional @I<file> options; any such options will be "
"processed recursively."
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "NOTES"
msgid "FOOTNOTES"
msgstr "NOTAS"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "1."
msgstr "1."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"MS-DOS does not allow CW<\\*(C`+\\*(C'> characters in file names, so on MS-"
"DOS this program is named B<CXXFILT>."
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "the Info entries for I<binutils>."
msgstr ""

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Copyright (c) 1991-2023 Free Software Foundation, Inc."
msgstr "Copyright (c) 1991-2023 Free Software Foundation, Inc."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Permission is granted to copy, distribute and/or modify this document under "
"the terms of the GNU Free Documentation License, Version 1.3 or any later "
"version published by the Free Software Foundation; with no Invariant "
"Sections, with no Front-Cover Texts, and with no Back-Cover Texts.  A copy "
"of the license is included in the section entitled \"GNU Free Documentation "
"License\"."
msgstr ""

#. type: ds C+
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"
msgstr "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"

#. type: ds :
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "\\k:\\h'-(\\n(.wu*8/10-\\*(#H+.1m+\\*(#F)'\\v'-\\*(#V'\\z.\\h'.2m+\\*(#F'.\\h'|\\n:u'\\v'\\*(#V'"
msgstr "\\k:\\h'-(\\n(.wu*8/10-\\*(#H+.1m+\\*(#F)'\\v'-\\*(#V'\\z.\\h'.2m+\\*(#F'.\\h'|\\n:u'\\v'\\*(#V'"

#. type: ds 8
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "\\h'\\*(#H'\\(*b\\h'-\\*(#H'"
msgstr "\\h'\\*(#H'\\(*b\\h'-\\*(#H'"

#. type: ds o
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "\\k:\\h'-(\\n(.wu+\\w'\\(de'u-\\*(#H)/2u'\\v'-.3n'\\*(#[\\z\\(de\\v'.3n'\\h'|\\n:u'\\*(#]"
msgstr "\\k:\\h'-(\\n(.wu+\\w'\\(de'u-\\*(#H)/2u'\\v'-.3n'\\*(#[\\z\\(de\\v'.3n'\\h'|\\n:u'\\*(#]"

#. type: ds d-
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "\\h'\\*(#H'\\(pd\\h'-\\w'~'u'\\v'-.25m'I<\\(hy>\\v'.25m'\\h'-\\*(#H'"
msgstr "\\h'\\*(#H'\\(pd\\h'-\\w'~'u'\\v'-.25m'I<\\(hy>\\v'.25m'\\h'-\\*(#H'"

#. type: ds D-
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "D\\k:\\h'-\\w'D'u'\\v'-.11m'\\z\\(hy\\v'.11m'\\h'|\\n:u'"
msgstr "D\\k:\\h'-\\w'D'u'\\v'-.11m'\\z\\(hy\\v'.11m'\\h'|\\n:u'"

#. type: ds th
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "\\*(#[\\v'.3m'\\s+1I\\s-1\\v'-.3m'\\h'-(\\w'I'u*2/3)'\\s-1o\\s+1\\*(#]"
msgstr "\\*(#[\\v'.3m'\\s+1I\\s-1\\v'-.3m'\\h'-(\\w'I'u*2/3)'\\s-1o\\s+1\\*(#]"

#. type: ds Th
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "\\*(#[\\s+2I\\s-2\\h'-\\w'I'u*3/5'\\v'-.3m'o\\v'.3m'\\*(#]"
msgstr "\\*(#[\\s+2I\\s-2\\h'-\\w'I'u*3/5'\\v'-.3m'o\\v'.3m'\\*(#]"

#. type: ds ae
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "a\\h'-(\\w'a'u*4/10)'e"
msgstr "a\\h'-(\\w'a'u*4/10)'e"

#. type: ds Ae
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "A\\h'-(\\w'A'u*4/10)'E"
msgstr "A\\h'-(\\w'A'u*4/10)'E"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-14"
msgstr "14 Enero 2023"

#. type: TH
#: debian-bookworm fedora-39
#, no-wrap
msgid "binutils-2.40.00"
msgstr "binutils-2.40.00"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The C++ language provides function overloading, which means that you can "
#| "write many functions with the same name (providing each takes parameters "
#| "of different types).  All C++ function names are encoded into a low-level "
#| "assembly label (this process is known as I<mangling). The> B<c++filt> "
#| "program does the inverse mapping: it decodes (I<demangles>)  low-level "
#| "names into user-level names so that the linker can keep these overloaded "
#| "functions from clashing."
msgid ""
"The \\*(C+ and Java languages provide function overloading, which means that "
"you can write many functions with the same name, providing that each "
"function takes parameters of different types.  In order to be able to "
"distinguish these similarly named functions \\*(C+ and Java encode them into "
"a low-level assembler name which uniquely identifies each different "
"version.  This process is known as I<mangling>. The \\&B<c++filt> [1] "
"program does the inverse mapping: it decodes (I<demangles>) low-level names "
"into user-level names so that they can be read."
msgstr ""
"El lenguaje C++ proporciona sobrecarga de funciones, lo que significa que "
"uno puede escribir varias funciones con el mismo nombre (siempre que la "
"lista de argumentos de cada una sea distinta). Todos los nombres de "
"funciones de C++ se codifican a una etiqueta de ensamblador de bajo nivel "
"(este proceso se conoce como I<desfiguración (mangling)). El programa> B<c+"
"+filt> realiza la correspondencia inversa: descodifica (I<restaura, "
"demangles>) los nombres de bajo nivel a los nombres que dio el usuario de "
"forma que el enlazador pueda evitar que estas funciones sobrecargadas "
"colisionen."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Every alphanumeric word (consisting of letters, digits, underscores, "
#| "dollars, or periods) seen in the input is a potential label.  If the "
#| "label decodes into a C++ name, the C++ name replaces the low-level name "
#| "in the output."
msgid ""
"Every alphanumeric word (consisting of letters, digits, underscores, "
"dollars, or periods) seen in the input is a potential mangled name.  If the "
"name decodes into a \\*(C+ name, the \\*(C+ name replaces the low-level name "
"in the output, otherwise the original word is output.  In this way you can "
"pass an entire assembler source file, containing mangled names, through B<c+"
"+filt> and see the same source file containing demangled names."
msgstr ""
"Cada palabra alfanumérica (consistente en letras, dígitos, subrayados, "
"dólares o puntos) vista en la entrada, es una etiqueta en potencia. Si la "
"etiqueta se descodifica a un nombre de C++, este nombre de C++ reemplaza a "
"la etiqueta de bajo nivel en la salida."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "will work and demangle the name to \\*(L\"f()\\*(R\" whereas:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"and will display \\*(L\"f(),\\*(R\", i.e., the demangled name followed by a "
"trailing comma.  This behaviour is because when the names are read from the "
"standard input it is expected that they might be part of an assembler source "
"file where there might be extra, extraneous characters trailing after a "
"mangled name.  For example:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "On some systems, both the C and C++ compilers put an underscore in front "
#| "of every name.  For example, the C name B<foo> gets the low-level name "
#| "B<_foo>.  This option removes the leading underscore."
msgid ""
"On some systems, both the C and \\*(C+ compilers put an underscore in front "
"of every name.  For example, the C name CW<\\*(C`foo\\*(C'> gets the low-"
"level name CW<\\*(C`_foo\\*(C'>.  This option removes the initial "
"underscore.  Whether \\&B<c++filt> removes the underscore by default is "
"target dependent."
msgstr ""
"En algunos sistemas, los compiladores de C y de C++ ponen un subrayado "
"delante de cada nombre. Por ejemplo, el nombre de C B<fuu> se convierte en "
"el nombre de bajo nivel B<_fuu>.  Esta opción quita el subrayado inicial."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"Attempt to demangle types as well as function names.  This is disabled by "
"default since mangled types are normally only used internally in the "
"compiler, and they can be confused with non-mangled names.  For example, a "
"function called \\*(L\"a\\*(R\" treated as a mangled type name would be "
"demangled to \\*(L\"signed char\\*(R\"."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "the one used by the \\s-1GNU \\*(C+\\s0 compiler (g++)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "the one specified by the \\*(C+ Annotated Reference Manual"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "the one used by the \\s-1HP\\s0 compiler (aCC)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "the one used by the \\s-1EDG\\s0 compiler"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"the one used by the \\s-1GNU \\*(C+\\s0 compiler (g++) with the V3 \\s-1ABI."
"\\s0"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "the one used by the \\s-1GNU\\s0 Java compiler (gcj)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "the one used by the \\s-1GNU\\s0 Ada compiler (\\s-1GNAT\\s0)."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"MS-DOS does not allow CW<\\*(C`+\\*(C'> characters in file names, so on MS-"
"DOS this program is named B<\\s-1CXXFILT\\s0>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"Permission is granted to copy, distribute and/or modify this document under "
"the terms of the \\s-1GNU\\s0 Free Documentation License, Version 1.3 or any "
"later version published by the Free Software Foundation; with no Invariant "
"Sections, with no Front-Cover Texts, and with no Back-Cover Texts.  A copy "
"of the license is included in the section entitled \\*(L\"\\s-1GNU\\s0 Free "
"Documentation License\\*(R\"."
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2023-09-05"
msgstr "5 Septiembre 2023"

#. type: TH
#: debian-unstable fedora-rawhide
#, fuzzy, no-wrap
#| msgid "binutils-2.40"
msgid "binutils-2.41"
msgstr "binutils-2.40"

#. type: TH
#: fedora-39
#, no-wrap
msgid "2023-07-19"
msgstr "19 ​​Julio 2023"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "2023-08-25"
msgstr "25 Agosto 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-06-27"
msgstr "27 Junio 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "binutils-2.40"
msgstr "binutils-2.40"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-07-13"
msgstr "13 ​​Julio 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "binutils-2.39.0"
msgstr "binutils-2.39.0"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Copyright (c) 1991-2022 Free Software Foundation, Inc."
msgstr "Copyright (c) 1991-2022 Free Software Foundation, Inc."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-05-30"
msgstr "30 Mayo 2023"

#. type: TH
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "binutils-2.40.00"
msgid "binutils-2.40.0"
msgstr "binutils-2.40.00"
