# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Rafael Fontenelle <rafaelff@gnome.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.2.0\n"
"POT-Creation-Date: 2023-08-10 19:07+0200\n"
"PO-Revision-Date: 2021-02-16 02:34-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 3.38.0\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "RECFIX"
msgstr "RECFIX"

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "April 2022"
msgstr "Abril de 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "recfix 1.9"
msgstr "recfix 1.9"

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr "Comandos de usuário"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "recfix - check a recfile for errors"
msgstr "recfix - verifica um arquivo rec por erros"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid ""
"B<recfix> [I<\\,OPTION\\/>]... [I<\\,OPERATION\\/>] [I<\\,OP_OPTION\\/>]... "
"[I<\\,FILE\\/>]"
msgstr ""
"B<recfix> [I<\\,OPÇÃO\\/>]... [I<\\,OPERAÇÃO\\/>] [I<\\,OPÇÃO-OPERAÇÃO\\/"
">]... [I<\\,ARQUIVO\\/>]"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "Check and fix rec files."
msgstr "Verifica e corrige arquivos rec."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "B<--no-external>"
msgstr "B<--no-external>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "don't use external descriptors."
msgstr "não usa descritores externos."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "B<--force>"
msgstr "B<--force>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "force the requested operation."
msgstr "força a operação requisitada."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "print a help message and exit."
msgstr "mostra uma mensagem de ajuda e sai."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "show version and exit."
msgstr "mostra a versão e sai."

#. type: SS
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "Operations:"
msgstr "Operações:"

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "B<--check>"
msgstr "B<--check>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "check integrity of the specified file.  Default."
msgstr "verifica integridade do arquivo especificado. Padrão."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "B<--sort>"
msgstr "B<--sort>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "sort the records in the specified file."
msgstr "ordena os registros no arquivo especificado."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "B<--auto>"
msgstr "B<--auto>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "insert auto-generated fields in records missing them."
msgstr "insere campos auto-gerados em registros que precisem deles."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "B<--encrypt>"
msgstr "B<--encrypt>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "encrypt confidential fields in the specified file."
msgstr "criptografa campos confidenciais no arquivo especificado."

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "B<--decrypt>"
msgstr "B<--decrypt>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "decrypt confidential fields in the specified file."
msgstr "descriptografa campos confidenciais no arquivo especificado."

#. type: SS
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "De/Encryption options:"
msgstr "Opções de (des)criptografia:"

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "B<-s>, B<--password>=I<\\,PASSWORD\\/>"
msgstr "B<-s>, B<--password>=I<\\,SENHA\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "encrypt/decrypt with this password."
msgstr "criptografa/descriptografa com essa senha."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid ""
"If no FILE is specified then the command acts like a filter, getting the "
"data from standard input and writing the result to standard output."
msgstr ""
"Se nenhum ARQUIVO for especificado, então o comando age como um filtro, "
"obtendo dados da saída padrão e escrevendo o resultado na saída padrão."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "Written by Jose E. Marchesi."
msgstr "Escrito por Jose E. Marchesi."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RELATANDO PROBLEMAS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "Report bugs to: bug-recutils@gnu.org"
msgstr "Relate erros para: bug-recutils@gnu.org"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid ""
"GNU recutils home page: E<lt>https://www.gnu.org/software/recutils/E<gt>"
msgstr ""
"Página do GNU recutils: E<lt>https://www.gnu.org/software/recutils/E<gt>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Ajuda geral no uso de software GNU: E<lt>http://www.gnu.org/gethelp/E<gt>"

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "COPYRIGHT"
msgstr "DIREITOS AUTORAIS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid ""
"Copyright \\(co 2010-2020 Jose E. Marchesi.  License GPLv3+: GNU GPL version "
"3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2010-2020 Jose E. Marchesi. Licença GPLv3+: GNU GPL versão 3 "
"ou posterior E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Este é um software livre: você é livre para alterá-lo e redistribuí-lo. NÃO "
"HÁ QUALQUER GARANTIA, na máxima extensão permitida em lei."

#. type: SH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid ""
"The full documentation for B<recfix> is maintained as a Texinfo manual.  If "
"the B<info> and B<recfix> programs are properly installed at your site, the "
"command"
msgstr ""
"A documentação completa para B<recfix> é mantida como um manual Texinfo. Se "
"os programas B<info> e B<recfix> estiverem instalados apropriadamente em sua "
"máquina, o comando"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "B<info recutils>"
msgstr "B<info recutils>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr "deve lhe dar acesso ao manual completo."
