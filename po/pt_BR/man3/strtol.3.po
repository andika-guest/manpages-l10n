# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Marcelo M. de Abreu <mmabreu@terra.com.br>, 2001.
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:24+0200\n"
"PO-Revision-Date: 2001-05-31 17:26+0200\n"
"Last-Translator: André Luiz Fassone <lonely_wolf@ig.com.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strtoll>():"
msgid "strtol"
msgstr "B<strtoll>():"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 julho 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "strtol, strtoll, strtoq - convert a string to a long integer"
msgstr "strtol, strtoll, strtoq - converte uma seqüencia para um inteiro longo"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long strtol(const char *restrict >I<nptr>B<,>\n"
"B<            char **restrict >I<endptr>B<, int >I<base>B<);>\n"
"B<long long strtoll(const char *restrict >I<nptr>B<,>\n"
"B<            char **restrict >I<endptr>B<, int >I<base>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de macro de teste de recursos para o glibc (consulte "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<strtoll>():"
msgstr "B<strtoll>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "_ISOC99_SOURCE\n"
#| "    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgid ""
"    _ISOC99_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE\n"
"    || /* Glibc versões E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strtol()> function converts the string in I<nptr> to a long integer "
#| "value according to the given I<base>, which must be between 2 and 36 "
#| "inclusive, or be the special value 0."
msgid ""
"The B<strtol>()  function converts the initial part of the string in I<nptr> "
"to a long integer value according to the given I<base>, which must be "
"between 2 and 36 inclusive, or be the special value 0."
msgstr ""
"A função B<strtol()> converte a seqüencia em I<ptrnum> para um valor de "
"inteiro longo de acordo com a dada I<base>, a qual deve estar entre 2 e 36 "
"inclusive, ou ser o valor especial 0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The string must begin with an arbitrary amount of white space (as "
#| "determined by B<isspace>(3))  followed by a single optional `+' or `-' "
#| "sign.  If I<base> is zero or 16, the string may then include a `0x' "
#| "prefix, and the number will be read in base 16; otherwise, a zero I<base> "
#| "is taken as 10 (decimal) unless the next character is `0', in which case "
#| "it is taken as 8 (octal)."
msgid ""
"The string may begin with an arbitrary amount of white space (as determined "
"by B<isspace>(3))  followed by a single optional \\[aq]+\\[aq] or \\[aq]-"
"\\[aq] sign.  If I<base> is zero or 16, the string may then include a \"0x\" "
"or \"0X\" prefix, and the number will be read in base 16; otherwise, a zero "
"I<base> is taken as 10 (decimal) unless the next character is \\[aq]0\\[aq], "
"in which case it is taken as 8 (octal)."
msgstr ""
"A seqüencia deve começar com uma quantidade arbitrária de espaço em branco "
"(como determinado por B<isspace>(3)) seguida por um único sinal opcional `+' "
"ou `-'. Se I<base> for zero ou 16, a seqüencia pode então incluir um prefixo "
"`0x', e o número será lido em base 16; caso contrário, uma I<base> zero é "
"tomada como 10 (decimal) a menos que o próximo caractere seja `0', caso no "
"qual ela é tomada como 8 (octal)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The remainder of the string is converted to a long int value in the "
#| "obvious manner, stopping at the first character which is not a valid "
#| "digit in the given base.  (In bases above 10, the letter `A' in either "
#| "upper or lower case represents 10, `B' represents 11, and so forth, with "
#| "`Z' representing 35.)"
msgid ""
"The remainder of the string is converted to a I<long> value in the obvious "
"manner, stopping at the first character which is not a valid digit in the "
"given base.  (In bases above 10, the letter \\[aq]A\\[aq] in either "
"uppercase or lowercase represents 10, \\[aq]B\\[aq] represents 11, and so "
"forth, with \\[aq]Z\\[aq] representing 35.)"
msgstr ""
"O restante da seqüencia é convertido para um valor 'long int' da maneira "
"óbvia, parando ao primeiro caractere que não seja um dígito válido na dada "
"base. (Em bases acima de 10, a letra `A' em maiúscula ou minúscula "
"representa 10, `B' representa 11, e assim por diante, com `Z' representando "
"35.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<endptr> is not NULL, B<strtol>()  stores the address of the first "
#| "invalid character in I<*endptr>.  If there were no digits at all, "
#| "B<strtol>()  stores the original value of I<nptr> in I<*endptr> (and "
#| "returns 0).  In particular, if I<*nptr> is not \\(aq\\e0\\(aq but "
#| "I<**endptr> is \\(aq\\e0\\(aq on return, the entire string is valid."
msgid ""
"If I<endptr> is not NULL, B<strtol>()  stores the address of the first "
"invalid character in I<*endptr>.  If there were no digits at all, "
"B<strtol>()  stores the original value of I<nptr> in I<*endptr> (and returns "
"0).  In particular, if I<*nptr> is not \\[aq]\\e0\\[aq] but I<**endptr> is "
"\\[aq]\\e0\\[aq] on return, the entire string is valid."
msgstr ""
"Se I<ptrfim> não for NULL, B<strtol>() armazena o endereço do primeiro "
"caractere inválido em I<*ptrfim>. Se não houver qualquer dígito, B<strtol>() "
"armazena o valor original de I<ptrnum> em I<*ptrfim> (e retorna 0). (Por "
"isso, se I<*ptrnum> não for \\(aq\\e0\\(aq mas I<**ptrfim> for "
"\\(aq\\e0\\(aq no retorno, a seqüencia inteira é válida.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<strtoll>()  function works just like the B<strtol>()  function but "
"returns a I<long long> integer value."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strtol()> function returns the result of the conversion, unless the "
#| "value would underflow or overflow.  If an underflow occurs, B<strtol()> "
#| "returns LONG_MIN.  If an overflow occurs, B<strtol()> returns LONG_MAX.  "
#| "In both cases, I<errno> is set to ERANGE."
msgid ""
"The B<strtol>()  function returns the result of the conversion, unless the "
"value would underflow or overflow.  If an underflow occurs, B<strtol>()  "
"returns B<LONG_MIN>.  If an overflow occurs, B<strtol>()  returns "
"B<LONG_MAX>.  In both cases, I<errno> is set to B<ERANGE>.  Precisely the "
"same holds for B<strtoll>()  (with B<LLONG_MIN> and B<LLONG_MAX> instead of "
"B<LONG_MIN> and B<LONG_MAX>)."
msgstr ""
"A função B<strtol()> retorna o resultado da conversão, a menos que o valor "
"fosse causar estouro de representação. Se ocorrer estouro para um negativo, "
"B<strtol()> retorna LONG_MIN. Se ocorrer estouro para um positivo, "
"B<strtol()>retorna LONG_MAX. Em ambos os casos, I<errno< é definido para "
"ERANGE."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERROS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "(not in C99)  The given I<base> contains an unsupported value."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The resulting value was out of range."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The implementation may also set I<errno> to B<EINVAL> in case no conversion "
"was performed (no digits seen, and 0 returned)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para uma explicação dos termos usados nesta seção, consulte B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strtol>(),\n"
"B<strtoll>(),\n"
"B<strtoq>()"
msgstr ""
"B<strtol>(),\n"
"B<strtoll>(),\n"
"B<strtoq>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTÓRICO"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strtoll>():"
msgid "B<strtol>()"
msgstr "B<strtoll>():"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgid "POSIX.1-2001, C89, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strtoll>():"
msgid "B<strtoll>()"
msgstr "B<strtoll>():"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, C99."
msgstr "POSIX.1-2001, C99."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since B<strtol>()  can legitimately return 0, B<LONG_MAX>, or B<LONG_MIN> "
"(B<LLONG_MAX> or B<LLONG_MIN> for B<strtoll>())  on both success and "
"failure, the calling program should set I<errno> to 0 before the call, and "
"then determine if an error occurred by checking whether I<errno> has a "
"nonzero value after the call."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"According to POSIX.1, in locales other than \"C\" and \"POSIX\", these "
"functions may accept other, implementation-defined numeric strings."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "BSD also has"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<quad_t strtoq(const char *>I<nptr>B<, char **>I<endptr>B<, int >I<base>B<);>\n"
msgstr "B<quad_t strtoq(const char *>I<ptrnum>B<, char **>I<ptrfim>B<, int >I<base>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"with completely analogous definition.  Depending on the wordsize of the "
"current architecture, this may be equivalent to B<strtoll>()  or to "
"B<strtol>()."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program shown below demonstrates the use of B<strtol>().  The first "
"command-line argument specifies a string from which B<strtol>()  should "
"parse a number.  The second (optional) argument specifies the base to be "
"used for the conversion.  (This argument is converted to numeric form using "
"B<atoi>(3), a function that performs no error checking and has a simpler "
"interface than B<strtol>().)  Some examples of the results produced by this "
"program are the following:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out 123>\n"
"strtol() returned 123\n"
"$B< ./a.out \\[aq]    123\\[aq]>\n"
"strtol() returned 123\n"
"$B< ./a.out 123abc>\n"
"strtol() returned 123\n"
"Further characters after number: \"abc\"\n"
"$B< ./a.out 123abc 55>\n"
"strtol: Invalid argument\n"
"$B< ./a.out \\[aq]\\[aq]>\n"
"No digits were found\n"
"$B< ./a.out 4000000000>\n"
"strtol: Numerical result out of range\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Fonte do programa"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int base;\n"
"    char *endptr, *str;\n"
"    long val;\n"
"\\&\n"
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s str [base]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    str = argv[1];\n"
"    base = (argc E<gt> 2) ? atoi(argv[2]) : 0;\n"
"\\&\n"
"    errno = 0;    /* To distinguish success/failure after call */\n"
"    val = strtol(str, &endptr, base);\n"
"\\&\n"
"    /* Check for various possible errors. */\n"
"\\&\n"
"    if (errno != 0) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (endptr == str) {\n"
"        fprintf(stderr, \"No digits were found\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* If we got here, strtol() successfully parsed a number. */\n"
"\\&\n"
"    printf(\"strtol() returned %ld\\en\", val);\n"
"\\&\n"
"    if (*endptr != \\[aq]\\e0\\[aq])        /* Not necessarily an error... */\n"
"        printf(\"Further characters after number: \\e\"%s\\e\"\\en\", endptr);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. #-#-#-#-#  archlinux: strtol.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: strtol.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: strtol.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-39: strtol.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: strtol.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: strtol.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-6: strtol.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: strtol.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<atof>(3), B<atoi>(3), B<atol>(3), B<strtod>(3), B<strtoimax>(3), "
#| "B<strtoul>(3),"
msgid ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<strtod>(3), B<strtoimax>(3), "
"B<strtoul>(3)"
msgstr ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<strtod>(3), B<strtoimax>(3), "
"B<strtoul>(3),"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 fevereiro 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
#, fuzzy
#| msgid "B<strtol>(): POSIX.1-2001, POSIX.1-2008, C89, C99 SVr4, 4.3BSD."
msgid "B<strtol>(): POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "B<strtol>(): POSIX.1-2001, POSIX.1-2008, C89, C99 SVr4, 4.3BSD."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "B<strtoll>(): POSIX.1-2001, POSIX.1-2008, C99."
msgstr "B<strtoll>(): POSIX.1-2001, POSIX.1-2008, C99."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#include E<lt>limits.hE<gt>\n"
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>errno.hE<gt>\n"
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int base;\n"
"    char *endptr, *str;\n"
"    long val;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int base;\n"
"    char *ptrfim, *str;\n"
"    long val;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s str [base]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    str = argv[1];\n"
#| "    base = (argc E<gt> 2) ? atoi(argv[2]) : 10;\n"
msgid ""
"    str = argv[1];\n"
"    base = (argc E<gt> 2) ? atoi(argv[2]) : 0;\n"
msgstr ""
"    str = argv[1];\n"
"    base = (argc E<gt> 2) ? atoi(argv[2]) : 10;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    errno = 0;    /* To distinguish success/failure after call */\n"
"    val = strtol(str, &endptr, base);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    /* Check for various possible errors. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    cpid = fork();\n"
#| "    if (cpid == -1) {\n"
#| "        perror(\"fork\");\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
msgid ""
"    if (errno != 0) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    cpid = fork();\n"
"    if (cpid == -1) {\n"
"        perror(\"fork\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (endptr == str) {\n"
"        fprintf(stderr, \"No digits were found\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    /* If we got here, strtol() successfully parsed a number. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "    printf(\"strtol() returned %ld\\en\", val);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (*endptr != \\[aq]\\e0\\[aq])        /* Not necessarily an error... */\n"
"        printf(\"Further characters after number: \\e\"%s\\e\"\\en\", endptr);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "STRTOL"
msgstr "STRTOL"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 setembro 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<long int strtol(const char *>I<nptr>B<, char **>I<endptr>B<, int >I<base>B<);>\n"
msgstr "B<long int strtol(const char *>I<ptrnum>B<, char **>I<ptrfim>B<, int >I<base>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<long long int strtoll(const char *>I<nptr>B<, char **>I<endptr>B<, int >I<base>B<);>\n"
msgstr "B<long long int strtoll(const char *>I<ptrnum>B<, char **>I<ptrfim>B<, int >I<base>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_ISOC99_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE\n"
"    || /* Glibc versões E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The string must begin with an arbitrary amount of white space (as "
#| "determined by B<isspace>(3))  followed by a single optional `+' or `-' "
#| "sign.  If I<base> is zero or 16, the string may then include a `0x' "
#| "prefix, and the number will be read in base 16; otherwise, a zero I<base> "
#| "is taken as 10 (decimal) unless the next character is `0', in which case "
#| "it is taken as 8 (octal)."
msgid ""
"The string may begin with an arbitrary amount of white space (as determined "
"by B<isspace>(3))  followed by a single optional \\(aq+\\(aq or \\(aq-\\(aq "
"sign.  If I<base> is zero or 16, the string may then include a \"0x\" or "
"\"0X\" prefix, and the number will be read in base 16; otherwise, a zero "
"I<base> is taken as 10 (decimal) unless the next character is \\(aq0\\(aq, "
"in which case it is taken as 8 (octal)."
msgstr ""
"A seqüencia deve começar com uma quantidade arbitrária de espaço em branco "
"(como determinado por B<isspace>(3)) seguida por um único sinal opcional `+' "
"ou `-'. Se I<base> for zero ou 16, a seqüencia pode então incluir um prefixo "
"`0x', e o número será lido em base 16; caso contrário, uma I<base> zero é "
"tomada como 10 (decimal) a menos que o próximo caractere seja `0', caso no "
"qual ela é tomada como 8 (octal)."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The remainder of the string is converted to a long int value in the "
#| "obvious manner, stopping at the first character which is not a valid "
#| "digit in the given base.  (In bases above 10, the letter `A' in either "
#| "upper or lower case represents 10, `B' represents 11, and so forth, with "
#| "`Z' representing 35.)"
msgid ""
"The remainder of the string is converted to a I<long int> value in the "
"obvious manner, stopping at the first character which is not a valid digit "
"in the given base.  (In bases above 10, the letter \\(aqA\\(aq in either "
"uppercase or lowercase represents 10, \\(aqB\\(aq represents 11, and so "
"forth, with \\(aqZ\\(aq representing 35.)"
msgstr ""
"O restante da seqüencia é convertido para um valor 'long int' da maneira "
"óbvia, parando ao primeiro caractere que não seja um dígito válido na dada "
"base. (Em bases acima de 10, a letra `A' em maiúscula ou minúscula "
"representa 10, `B' representa 11, e assim por diante, com `Z' representando "
"35.)"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"If I<endptr> is not NULL, B<strtol>()  stores the address of the first "
"invalid character in I<*endptr>.  If there were no digits at all, "
"B<strtol>()  stores the original value of I<nptr> in I<*endptr> (and returns "
"0).  In particular, if I<*nptr> is not \\(aq\\e0\\(aq but I<**endptr> is "
"\\(aq\\e0\\(aq on return, the entire string is valid."
msgstr ""
"Se I<ptrfim> não for NULL, B<strtol>() armazena o endereço do primeiro "
"caractere inválido em I<*ptrfim>. Se não houver qualquer dígito, B<strtol>() "
"armazena o valor original de I<ptrnum> em I<*ptrfim> (e retorna 0). (Por "
"isso, se I<*ptrnum> não for \\(aq\\e0\\(aq mas I<**ptrfim> for "
"\\(aq\\e0\\(aq no retorno, a seqüencia inteira é válida.)"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<strtoll>()  function works just like the B<strtol>()  function but "
"returns a long long integer value."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<strtol>(): POSIX.1-2001, POSIX.1-2008, C89, C99 SVr4, 4.3BSD."
msgstr "B<strtol>(): POSIX.1-2001, POSIX.1-2008, C89, C99 SVr4, 4.3BSD."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"According to POSIX.1, in locales other than the \"C\" and \"POSIX\", these "
"functions may accept other, implementation-defined numeric strings."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLO"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"$B< ./a.out 123>\n"
"strtol() returned 123\n"
"$B< ./a.out \\(aq    123\\(aq>\n"
"strtol() returned 123\n"
"$B< ./a.out 123abc>\n"
"strtol() returned 123\n"
"Further characters after number: abc\n"
"$B< ./a.out 123abc 55>\n"
"strtol: Invalid argument\n"
"$B< ./a.out \\(aq\\(aq>\n"
"No digits were found\n"
"$B< ./a.out 4000000000>\n"
"strtol: Numerical result out of range\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
msgstr ""
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"    str = argv[1];\n"
"    base = (argc E<gt> 2) ? atoi(argv[2]) : 10;\n"
msgstr ""
"    str = argv[1];\n"
"    base = (argc E<gt> 2) ? atoi(argv[2]) : 10;\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    /* Check for various possible errors */\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"    if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))\n"
"            || (errno != 0 && val == 0)) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))\n"
"            || (errno != 0 && val == 0)) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    /* If we got here, strtol() successfully parsed a number */\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (*endptr != \\(aq\\e0\\(aq)        /* Not necessarily an error... */\n"
"        printf(\"Further characters after number: %s\\en\", endptr);\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "B<atof>(3), B<atoi>(3), B<atol>(3), B<strtod>(3), B<strtoimax>(3), "
#| "B<strtoul>(3),"
msgid "B<atof>(3), B<atoi>(3), B<atol>(3), B<strtod>(3), B<strtoul>(3)"
msgstr ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<strtod>(3), B<strtoimax>(3), "
"B<strtoul>(3),"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 março 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
