# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Felipe M Pereira <Felipe.Pereira@ic.unicamp.br>, 2001.
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:59+0200\n"
"PO-Revision-Date: 2001-06-02 19:20-0300\n"
"Last-Translator: André Luiz Fassone <lonely_wolf@ig.com.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getmntent>()"
msgid "getmntent"
msgstr "B<getmntent>()"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 julho 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"getmntent, setmntent, addmntent, endmntent, hasmntopt, getmntent_r - get "
"filesystem descriptor file entry"
msgstr ""
"getmntent, setmntent, addmntent, endmntent, hasmntopt, getmntent_r - obtêm "
"entrada de descritor de arquivo do sistema de arquivos"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>stdio.hE<gt>>\n"
"B<#include E<lt>mntent.hE<gt>>\n"
msgstr ""
"B<#include E<lt>stdio.hE<gt>>\n"
"B<#include E<lt>mntent.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<FILE *setmntent(const char *>I<filename>B<, const char *>I<type>B<);>\n"
msgstr "B<FILE *setmntent(const char *>I<filename>B<, const char *>I<type>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<struct mntent *getmntent(FILE *>I<stream>B<);>\n"
msgstr "B<struct mntent *getmntent(FILE *>I<stream>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int addmntent(FILE *>I<stream>B<, const struct mntent *>I<mnt>B<);>\n"
msgid ""
"B<int addmntent(FILE *restrict >I<stream>B<,>\n"
"B<              const struct mntent *restrict >I<mnt>B<);>\n"
msgstr "B<int addmntent(FILE *>I<stream>B<, const struct mntent *>I<mnt>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int endmntent(FILE *>I<streamp>B<);>\n"
msgstr "B<int endmntent(FILE *>I<streamp>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<char *hasmntopt(const struct mntent *>I<mnt>B<, const char *>I<opt>B<);>\n"
msgstr "B<char *hasmntopt(const struct mntent *>I<mnt>B<, const char *>I<opt>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<#include E<lt>dirent.hE<gt>>\n"
msgid ""
"/* GNU extension */\n"
"B<#include E<lt>mntent.hE<gt>>\n"
msgstr "B<#include E<lt>dirent.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<struct mntent *getmntent_r(FILE *>I<streamp>B<, struct mntent *>I<mntbuf>B<,>\n"
#| "B<                           char *>I<buf>B<, int >I<buflen>B<);>\n"
msgid ""
"B<struct mntent *getmntent_r(FILE *restrict >I<streamp>B<,>\n"
"B<              struct mntent *restrict >I<mntbuf>B<,>\n"
"B<              char >I<buf>B<[restrict .>I<buflen>B<], int >I<buflen>B<);>\n"
msgstr ""
"B<struct mntent *getmntent_r(FILE *>I<streamp>B<, struct mntent *>I<mntbuf>B<,>\n"
"B<                           char *>I<buf>B<, int >I<buflen>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de macro de teste de recursos para o glibc (consulte "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<getmntent_r>()"
msgid "B<getmntent_r>():"
msgstr "B<getmntent_r>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    Desde o glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 e anterior:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These routines are used to access the filesystem description file I</etc/"
"fstab> and the mounted filesystem description file I</etc/mtab>."
msgstr ""
"Estas rotinas são utilizadas para acessar o arquivo de descrição do sistema "
"de arquivos I</etc/fstab> e o arquivo de descrição do sistema de arquivos "
"montado I</etc/mtab>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<setmntent()> function opens the file system description file "
#| "I<filep> and returns a file pointer which can be used by B<getmntent()>.  "
#| "The argument I<type> is the type of access required and can take the same "
#| "values as the I<mode> argument of B<fopen>(3)."
msgid ""
"The B<setmntent>()  function opens the filesystem description file "
"I<filename> and returns a file pointer which can be used by B<getmntent>().  "
"The argument I<type> is the type of access required and can take the same "
"values as the I<mode> argument of B<fopen>(3).  The returned stream should "
"be closed using B<endmntent>()  rather than B<fclose>(3)."
msgstr ""
"A função B<setmntent()> abre o arquivo de descrição do sistema de arquivos "
"I<filep> e retorna um ponteiro FILE que pode ser usado por B<getmntent()>. O "
"argumento I<type> é o tipo de acesso solicitado e pode assumir os mesmos "
"valores do argumento I<mode> de B<fopen>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<getmntent()> function reads the next line from the file system "
#| "description file I<filep> and returns a pointer to a structure containing "
#| "the broken out fields from a line in the file.  The pointer points to a "
#| "static area of memory which is overwritten by subsequent calls to "
#| "B<getmntent()>."
msgid ""
"The B<getmntent>()  function reads the next line of the filesystem "
"description file from I<stream> and returns a pointer to a structure "
"containing the broken out fields from a line in the file.  The pointer "
"points to a static area of memory which is overwritten by subsequent calls "
"to B<getmntent>()."
msgstr ""
"A função B<getmntent()> lê a próxima linha do arquivo de descrição do "
"sistema de arquivos I<filep> e retorna um ponteiro para uma estrutura "
"contendo os campos separados obtidas de uma linha do arquivo. O ponteiro "
"aponta para uma área estática da memória a qual é sobrescrita por chamadas "
"subseqüentes a B<getmntent()>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<addmntent()> function adds the mntent structure I<mnt> to the end "
#| "of the open file I<filep>."
msgid ""
"The B<addmntent>()  function adds the I<mntent> structure I<mnt> to the end "
"of the open I<stream>."
msgstr ""
"A função B<addmntent()> adiciona a estrutura mntent I<mnt> ao fim do arquivo "
"aberto I<filep>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<endmntent()> function closes the file system description file "
#| "I<filep>."
msgid ""
"The B<endmntent>()  function closes the I<stream> associated with the "
"filesystem description file."
msgstr ""
"A função B<endmntent()> fecha o arquivo de descrição do sistema de arquivos "
"I<filep>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<hasmntopt()> function scans the I<mnt_opts> field (see below)  of "
#| "the mntent structure I<mnt> for a substring that matches I<opt>.  See "
#| "I<E<lt>mntent.hE<gt>> for valid mount options."
msgid ""
"The B<hasmntopt>()  function scans the I<mnt_opts> field (see below)  of the "
"I<mntent> structure I<mnt> for a substring that matches I<opt>.  See "
"I<E<lt>mntent.hE<gt>> and B<mount>(8)  for valid mount options."
msgstr ""
"A função B<hasmntopt()> percorre o campo I<mnt_opts> (veja abaixo) da "
"estrutura mntent I<mnt> procurando por uma substring que case com I<opt>. "
"Veja I<E<lt>mntent.hE<gt>> para opções válidas de montagem."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The reentrant B<getmntent_r>()  function is similar to B<getmntent>(), but "
"stores the I<mntent> structure in the provided I<*mntbuf>, and stores the "
"strings pointed to by the entries in that structure in the provided array "
"I<buf> of size I<buflen>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The I<mntent> structure is defined in I<E<lt>mntent.hE<gt>> as follows:"
msgstr "A estrutura I<mntent> é definida em I<E<lt>mntent.hE<gt>> como segue:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "struct mntent {\n"
#| "\tchar \t*mnt_fsname;\t\t/* name of mounted file system */\n"
#| "\tchar\t*mnt_dir;\t\t/* file system path prefix */\n"
#| "\tchar\t*mnt_type;\t\t/* mount type (see mntent.h) */\n"
#| "\tchar\t*mnt_opts;\t\t/* mount options (see mntent.h) */\n"
#| "\tint\tmnt_freq;\t\t/* dump frequency in days */\n"
#| "\tint\tmnt_passno;\t\t/* pass number on parallel fsck */\n"
#| "};\n"
msgid ""
"struct mntent {\n"
"    char *mnt_fsname;   /* name of mounted filesystem */\n"
"    char *mnt_dir;      /* filesystem path prefix */\n"
"    char *mnt_type;     /* mount type (see mntent.h) */\n"
"    char *mnt_opts;     /* mount options (see mntent.h) */\n"
"    int   mnt_freq;     /* dump frequency in days */\n"
"    int   mnt_passno;   /* pass number on parallel fsck */\n"
"};\n"
msgstr ""
"struct mntent {\n"
"\tchar \t*mnt_fsname;\t\t/* nome do sistema arq. montado */\n"
"\tchar\t*mnt_dir;\t\t/* pref. diret. do sistema de arq. */\n"
"\tchar\t*mnt_type;\t\t/* tipo (v. mntent.h) */\n"
"\tchar\t*mnt_opts;\t\t/* opções (v. mntent.h) */\n"
"\tint\tmnt_freq;\t\t/* freqüência de dump em dias */\n"
"\tint\tmnt_passno;\t\t/* passadas em fsck paralelos */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since fields in the mtab and fstab files are separated by whitespace, octal "
"escapes are used to represent the characters space (\\e040), tab (\\e011), "
"newline (\\e012), and backslash (\\e\\e) in those files when they occur in "
"one of the four strings in a I<mntent> structure.  The routines "
"B<addmntent>()  and B<getmntent>()  will convert from string representation "
"to escaped representation and back.  When converting from escaped "
"representation, the sequence \\e134 is also converted to a backslash."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<getmntent()> function returns a pointer to the mntent structure or "
#| "NULL on failure."
msgid ""
"The B<getmntent>()  and B<getmntent_r>()  functions return a pointer to the "
"I<mntent> structure or NULL on failure."
msgstr ""
"A função B<getmntent()> retorna um ponteiro para a estrutura mntent ou NULL "
"quando falha."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<addmntent>()  function returns 0 on success and 1 on failure."
msgstr "A função B<addmntent>() retorna 0 no successo e 1 quando falha."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<endmntent>()  function always returns 1."
msgstr "A função B<endmntent>() sempre retorna 1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<hasmntopt>()  function returns the address of the substring if a match "
"is found and NULL otherwise."
msgstr ""
"A função B<hasmntopt>() retorna o endereço da substring se for encontrada ou "
"NULL, caso contrário."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARQUIVOS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/fstab>"
msgstr "I</etc/fstab>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "The field descriptions are:"
msgid "filesystem description file"
msgstr "As descrições dos campos são:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/mtab>"
msgstr "I</etc/mtab>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "The field descriptions are:"
msgid "mounted filesystem description file"
msgstr "As descrições dos campos são:"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para uma explicação dos termos usados nesta seção, consulte B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<setmntent>(),\n"
"B<endmntent>(),\n"
"B<hasmntopt>()"
msgstr ""
"B<setmntent>(),\n"
"B<endmntent>(),\n"
"B<hasmntopt>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getmntent>()"
msgstr "B<getmntent>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Safe env locale"
msgid "MT-Unsafe race:mntentbuf locale"
msgstr "MT-Safe env locale"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<addmntent>()"
msgstr "B<addmntent>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Safe env locale"
msgid "MT-Safe race:stream locale"
msgstr "MT-Safe env locale"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getmntent_r>()"
msgstr "B<getmntent_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTÓRICO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The nonreentrant functions are from SunOS 4.1.3.  A routine "
"B<getmntent_r>()  was introduced in HP-UX 10, but it returns an I<int>.  The "
"prototype shown above is glibc-only."
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, fuzzy
#| msgid ""
#| "SysV also has a B<getmntent()> function but the calling sequence differs, "
#| "and the returned structure is different. Under SysV I</etc/mnttab> is "
#| "used.  BSD 4.4 and Digital Unix have a routine B<getmntinfo()>, a wrapper "
#| "around the system call B<getfsstat()>."
msgid ""
"System V also has a B<getmntent>()  function but the calling sequence "
"differs, and the returned structure is different.  Under System V I</etc/"
"mnttab> is used.  4.4BSD and Digital UNIX have a routine B<\\%getmntinfo>(), "
"a wrapper around the system call B<getfsstat>()."
msgstr ""
"O SysV também possui uma função B<getmntent()> mas a seqüência de chamada é "
"diferente, e a estrutura retornada é diferente. No SysV o I</etc/mnttab> é "
"utilizado. O BSD 4.4 e o Digital Unix têm uma rotina B<getmntinfo()>, um "
"mediador (wrapper) para a chamada de sistema B<getfsstat()>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<fopen>(3), B<fstab>(5), B<mount>(8)"
msgstr "B<fopen>(3), B<fstab>(5), B<mount>(8)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 fevereiro 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "SysV also has a B<getmntent()> function but the calling sequence differs, "
#| "and the returned structure is different. Under SysV I</etc/mnttab> is "
#| "used.  BSD 4.4 and Digital Unix have a routine B<getmntinfo()>, a wrapper "
#| "around the system call B<getfsstat()>."
msgid ""
"System V also has a B<getmntent>()  function but the calling sequence "
"differs, and the returned structure is different.  Under System V I</etc/"
"mnttab> is used.  4.4BSD and Digital UNIX have a routine B<getmntinfo>(), a "
"wrapper around the system call B<getfsstat>()."
msgstr ""
"O SysV também possui uma função B<getmntent()> mas a seqüência de chamada é "
"diferente, e a estrutura retornada é diferente. No SysV o I</etc/mnttab> é "
"utilizado. O BSD 4.4 e o Digital Unix têm uma rotina B<getmntinfo()>, um "
"mediador (wrapper) para a chamada de sistema B<getfsstat()>."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GETMNTENT"
msgstr "GETMNTENT"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 setembro 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int addmntent(FILE *>I<stream>B<, const struct mntent *>I<mnt>B<);>\n"
msgstr "B<int addmntent(FILE *>I<stream>B<, const struct mntent *>I<mnt>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<struct mntent *getmntent_r(FILE *>I<streamp>B<, struct mntent *>I<mntbuf>B<,>\n"
"B<                           char *>I<buf>B<, int >I<buflen>B<);>\n"
msgstr ""
"B<struct mntent *getmntent_r(FILE *>I<streamp>B<, struct mntent *>I<mntbuf>B<,>\n"
"B<                           char *>I<buf>B<, int >I<buflen>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
#| "    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
#| "    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgid ""
"B<getmntent_r>():\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Desde o glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versões E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The B<setmntent()> function opens the file system description file "
#| "I<filep> and returns a file pointer which can be used by B<getmntent()>.  "
#| "The argument I<type> is the type of access required and can take the same "
#| "values as the I<mode> argument of B<fopen>(3)."
msgid ""
"The B<setmntent>()  function opens the filesystem description file "
"I<filename> and returns a file pointer which can be used by B<getmntent>().  "
"The argument I<type> is the type of access required and can take the same "
"values as the I<mode> argument of B<fopen>(3)."
msgstr ""
"A função B<setmntent()> abre o arquivo de descrição do sistema de arquivos "
"I<filep> e retorna um ponteiro FILE que pode ser usado por B<getmntent()>. O "
"argumento I<type> é o tipo de acesso solicitado e pode assumir os mesmos "
"valores do argumento I<mode> de B<fopen>(3)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The reentrant B<getmntent_r>()  function is similar to B<getmntent>(), but "
"stores the I<struct mount> in the provided I<*mntbuf> and stores the strings "
"pointed to by the entries in that struct in the provided array I<buf> of "
"size I<buflen>."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The nonreentrant functions are from SunOS 4.1.3.  A routine "
"B<getmntent_r>()  was introduced in HP-UX 10, but it returns an int.  The "
"prototype shown above is glibc-only."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 março 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
