# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
# Fábio Henrique F. Silva <fabiohfs@mail.com>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:52+0200\n"
"PO-Revision-Date: 2000-08-21 23:11+0200\n"
"Last-Translator: Fábio Henrique F. Silva <fabiohfs@mail.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "close"
msgstr "close"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 março 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "close - close a file descriptor"
msgstr "close - fecha um descritor de arquivo"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int close(int >I<fd>B<);>\n"
msgstr "B<int close(int >I<fd>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<close> closes a file descriptor, so that it no longer refers to any "
#| "file and may be reused. Any locks held on the file it was associated "
#| "with, and owned by the process, are removed (regardless of the file "
#| "descriptor that was used to obtain the lock)."
msgid ""
"B<close>()  closes a file descriptor, so that it no longer refers to any "
"file and may be reused.  Any record locks (see B<fcntl>(2))  held on the "
"file it was associated with, and owned by the process, are removed "
"(regardless of the file descriptor that was used to obtain the lock)."
msgstr ""
"B<close> fecha um descritor de arquivos. Assim que ele não se refira a "
"quaisquer arquivos poderá ser reusado. Qualquer bloqueio aplicado ao arquivo "
"e de propriedade do processo é removido (independente do descritor de "
"arquivo que foi usado para aplicar o bloqueio)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<fd> is the last copy of a particular file descriptor the resources "
#| "associated with it are freed; if the descriptor was the last reference to "
#| "a file which has been removed using B<unlink>(2)  the file is deleted."
msgid ""
"If I<fd> is the last file descriptor referring to the underlying open file "
"description (see B<open>(2)), the resources associated with the open file "
"description are freed; if the file descriptor was the last reference to a "
"file which has been removed using B<unlink>(2), the file is deleted."
msgstr ""
"Se I<fd> é a última cópia de um descritor de arquivos em particular os "
"recursos associados a ele serão liberados; se um descritor tem a última "
"referência para um arquivo o qual foi removido usando e B<unlink>(2) o "
"arquivo é apaguado."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<close> returns zero on success, or -1 if an error occurred."
msgid ""
"B<close>()  returns zero on success.  On error, -1 is returned, and I<errno> "
"is set to indicate the error."
msgstr ""
"B<close> em caso de sucesso, 0 é retornado, caso contrário -1 é retornado."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERROS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> isn't a valid open file descriptor."
msgstr "I<fd> não é válido como descritor de arquivos."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. #-#-#-#-#  archlinux: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Though, it's in doubt whether this error can ever occur; see
#.  https://lwn.net/Articles/576478/ "Returning EINTR from close()"
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Though, it's in doubt whether this error can ever occur; see
#.  https://lwn.net/Articles/576478/ "Returning EINTR from close()"
#. type: Plain text
#. #-#-#-#-#  debian-unstable: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Though, it's in doubt whether this error can ever occur; see
#.  https://lwn.net/Articles/576478/ "Returning EINTR from close()"
#. type: Plain text
#. #-#-#-#-#  fedora-39: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Though, it's in doubt whether this error can ever occur; see
#.  https://lwn.net/Articles/576478/ "Returning EINTR from close()"
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Though, it's in doubt whether this error can ever occur; see
#.  https://lwn.net/Articles/576478/ "Returning EINTR from close()"
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Though, it's in doubt whether this error can ever occur; see
#.  https://lwn.net/Articles/576478/ "Returning EINTR from close()"
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Though, it's in doubt whether this error can ever occur; see
#.  https://lwn.net/Articles/576478/ "Returning EINTR from close()"
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<close>()  call was interrupted by a signal; see B<signal>(7)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An I/O error occurred."
msgstr "Ocorreu um erro de E/S."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>, B<EDQUOT>"
msgstr "B<ENOSPC>, B<EDQUOT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On NFS, these errors are not normally reported against the first write which "
"exceeds the available storage space, but instead against a subsequent "
"B<write>(2), B<fsync>(2), or B<close>()."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See NOTES for a discussion of why B<close>()  should not be retried after an "
"error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTÓRICO"

#.  SVr4 documents an additional ENOLINK error condition.
#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "A successful close does not guarantee that the data has been successfully "
#| "saved to disk, as the kernel defers writes. Is not common for a "
#| "filesystem to flush the buffers when the stream is closed. If you need to "
#| "be sure that the data is physically stored use fsync(2) or sync(2), they "
#| "will get you closer to that goal (it will depend on the disk hardware at "
#| "this point)."
msgid ""
"A successful close does not guarantee that the data has been successfully "
"saved to disk, as the kernel uses the buffer cache to defer writes.  "
"Typically, filesystems do not flush buffers when a file is closed.  If you "
"need to be sure that the data is physically stored on the underlying disk, "
"use B<fsync>(2).  (It will depend on the disk hardware at this point.)"
msgstr ""
"Um fechamento bem sucedido não garante que os dados tenham sido armazenados "
"com sucesso no disco, já que o kernel adia escritas. Isto não é comum para "
"um sistema de arquivos que esvazia os buffers quando o fluxo é fechado. Se "
"você precisa estar certo que os dados foram fisicamente armazenados use "
"fsync(2) ou sync(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The close-on-exec file descriptor flag can be used to ensure that a file "
"descriptor is automatically closed upon a successful B<execve>(2); see "
"B<fcntl>(2)  for details."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Multithreaded processes and close()"
msgstr ""

#. #-#-#-#-#  archlinux: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Date: Tue, 4 Sep 2007 13:57:35 +0200
#.  From: Fredrik Noring <noring@nocrew.org>
#.  One such race involves signals and ERESTARTSYS. If a file descriptor
#.  in use by a system call is closed and then reused by e.g. an
#.  independent open() in some unrelated thread, before the original system
#.  call has restarted after ERESTARTSYS, the original system call will
#.  later restart with the reused file descriptor. This is most likely a
#.  serious programming error.
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Date: Tue, 4 Sep 2007 13:57:35 +0200
#.  From: Fredrik Noring <noring@nocrew.org>
#.  One such race involves signals and ERESTARTSYS. If a file descriptor
#.  in use by a system call is closed and then reused by e.g. an
#.  independent open() in some unrelated thread, before the original system
#.  call has restarted after ERESTARTSYS, the original system call will
#.  later restart with the reused file descriptor. This is most likely a
#.  serious programming error.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Date: Tue, 4 Sep 2007 13:57:35 +0200
#.  From: Fredrik Noring <noring@nocrew.org>
#.  One such race involves signals and ERESTARTSYS. If a file descriptor
#.  in use by a system call is closed and then reused by e.g. an
#.  independent open() in some unrelated thread, before the original system
#.  call has restarted after ERESTARTSYS, the original system call will
#.  later restart with the reused file descriptor. This is most likely a
#.  serious programming error.
#. type: Plain text
#. #-#-#-#-#  fedora-39: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Date: Tue, 4 Sep 2007 13:57:35 +0200
#.  From: Fredrik Noring <noring@nocrew.org>
#.  One such race involves signals and ERESTARTSYS. If a file descriptor
#.  in use by a system call is closed and then reused by e.g. an
#.  independent open() in some unrelated thread, before the original system
#.  call has restarted after ERESTARTSYS, the original system call will
#.  later restart with the reused file descriptor. This is most likely a
#.  serious programming error.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Date: Tue, 4 Sep 2007 13:57:35 +0200
#.  From: Fredrik Noring <noring@nocrew.org>
#.  One such race involves signals and ERESTARTSYS. If a file descriptor
#.  in use by a system call is closed and then reused by e.g. an
#.  independent open() in some unrelated thread, before the original system
#.  call has restarted after ERESTARTSYS, the original system call will
#.  later restart with the reused file descriptor. This is most likely a
#.  serious programming error.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: close.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Date: Tue, 4 Sep 2007 13:57:35 +0200
#.  From: Fredrik Noring <noring@nocrew.org>
#.  One such race involves signals and ERESTARTSYS. If a file descriptor
#.  in use by a system call is closed and then reused by e.g. an
#.  independent open() in some unrelated thread, before the original system
#.  call has restarted after ERESTARTSYS, the original system call will
#.  later restart with the reused file descriptor. This is most likely a
#.  serious programming error.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"It is probably unwise to close file descriptors while they may be in use by "
"system calls in other threads in the same process.  Since a file descriptor "
"may be reused, there are some obscure race conditions that may cause "
"unintended side effects."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Furthermore, consider the following scenario where two threads are "
"performing operations on the same file descriptor:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "(1)"
msgstr "(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"One thread is blocked in an I/O system call on the file descriptor.  For "
"example, it is trying to B<write>(2)  to a pipe that is already full, or "
"trying to B<read>(2)  from a stream socket which currently has no available "
"data."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "(2)"
msgstr "(2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "close - close a file descriptor"
msgid "Another thread closes the file descriptor."
msgstr "close - fecha um descritor de arquivo"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The behavior in this situation varies across systems.  On some systems, when "
"the file descriptor is closed, the blocking system call returns immediately "
"with an error."
msgstr ""

#.  'struct file' in kernel-speak
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On Linux (and possibly some other systems), the behavior is different: the "
"blocking I/O system call holds a reference to the underlying open file "
"description, and this reference keeps the description open until the I/O "
"system call completes.  (See B<open>(2)  for a discussion of open file "
"descriptions.)  Thus, the blocking system call in the first thread may "
"successfully complete after the B<close>()  in the second thread."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Dealing with error returns from close()"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A careful programmer will check the return value of B<close>(), since it is "
"quite possible that errors on a previous B<write>(2)  operation are reported "
"only on the final B<close>()  that releases the open file description.  "
"Failing to check the return value when closing a file may lead to I<silent> "
"loss of data.  This can especially be observed with NFS and with disk quota."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note, however, that a failure return should be used only for diagnostic "
"purposes (i.e., a warning to the application that there may still be I/O "
"pending or there may have been failed I/O)  or remedial purposes (e.g., "
"writing the file once more or creating a backup)."
msgstr ""

#
#.  The file descriptor is released early in close();
#.  close() ==> __close_fd():
#. 			__put_unused_fd() ==> __clear_open_fd()
#. 			return filp_close(file, files);
#.  The errors are returned by filp_close() after the FD has been
#.  cleared for re-use.
#.  filp_close()
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Retrying the B<close>()  after a failure return is the wrong thing to do, "
"since this may cause a reused file descriptor from another thread to be "
"closed.  This can occur because the Linux kernel I<always> releases the file "
"descriptor early in the close operation, freeing it for reuse; the steps "
"that may return an error, such as flushing data to the filesystem or device, "
"occur only later in the close operation."
msgstr ""

#.  FreeBSD documents this explicitly. From the look of the source code
#.  SVR4, ancient SunOS, later Solaris, and AIX all do this.
#.  Issue 8
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Many other implementations similarly always close the file descriptor "
"(except in the case of B<EBADF>, meaning that the file descriptor was "
"invalid)  even if they subsequently report an error on return from "
"B<close>().  POSIX.1 is currently silent on this point, but there are plans "
"to mandate this behavior in the next major release of the standard."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A careful programmer who wants to know about I/O errors may precede "
"B<close>()  with a call to B<fsync>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<EINTR> error is a somewhat special case.  Regarding the B<EINTR> "
"error, POSIX.1-2008 says:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If B<close>()  is interrupted by a signal that is to be caught, it shall "
"return -1 with I<errno> set to B<EINTR> and the state of I<fildes> is "
"unspecified."
msgstr ""

#
#.  FIXME . for later review when Issue 8 is one day released...
#.  POSIX proposes further changes for EINTR
#.  http://austingroupbugs.net/tag_view_page.php?tag_id=8
#.  http://austingroupbugs.net/view.php?id=529
#.  FIXME .
#.  Review the following glibc bug later
#.  https://sourceware.org/bugzilla/show_bug.cgi?id=14627
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This permits the behavior that occurs on Linux and many other "
"implementations, where, as with other errors that may be reported by "
"B<close>(), the file descriptor is guaranteed to be closed.  However, it "
"also permits another possibility: that the implementation returns an "
"B<EINTR> error and keeps the file descriptor open.  (According to its "
"documentation, HP-UX's B<close>()  does this.)  The caller must then once "
"more use B<close>()  to close the file descriptor, to avoid file descriptor "
"leaks.  This divergence in implementation behaviors provides a difficult "
"hurdle for portable applications, since on many implementations, B<close>()  "
"must not be called again after an B<EINTR> error, and on at least one, "
"B<close>()  must be called again.  There are plans to address this conundrum "
"for the next major release of the POSIX.1 standard."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<fcntl>(2), B<fsync>(2), B<open>(2), B<shutdown>(2), B<unlink>(2), "
#| "B<fclose>(3)"
msgid ""
"B<close_range>(2), B<fcntl>(2), B<fsync>(2), B<open>(2), B<shutdown>(2), "
"B<unlink>(2), B<fclose>(3)"
msgstr ""
"B<fcntl>(2), B<fsync>(2), B<open>(2), B<shutdown>(2), B<unlink>(2), "
"B<fclose>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "30 outubro 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#.  SVr4 documents an additional ENOLINK error condition.
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#.  Date: Tue, 4 Sep 2007 13:57:35 +0200
#.  From: Fredrik Noring <noring@nocrew.org>
#.  One such race involves signals and ERESTARTSYS. If a file descriptor
#.  in use by a system call is closed and then reused by e.g. an
#.  independent open() in some unrelated thread, before the original system
#.  call has restarted after ERESTARTSYS, the original system call will
#.  later restart with the reused file descriptor. This is most likely a
#.  serious programming error.
#. type: Plain text
#: fedora-39 fedora-rawhide
msgid ""
"When dealing with sockets, you have to be sure that there is no B<recv>(2)  "
"still blocking on it on another thread, otherwise it might block forever, "
"since no more messages will be send via the socket. Be sure to use "
"B<shutdown>(2)  to shut down all parts the connection before closing the "
"socket."
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "CLOSE"
msgstr "CLOSE"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 setembro 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "B<close> returns zero on success, or -1 if an error occurred."
msgid ""
"B<close>()  returns zero on success.  On error, -1 is returned, and I<errno> "
"is set appropriately."
msgstr ""
"B<close> em caso de sucesso, 0 é retornado, caso contrário -1 é retornado."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On NFS, these errors are not normally reported against the first write which "
"exceeds the available storage space, but instead against a subsequent "
"B<write>(2), B<fsync>(2), or B<close>(2)."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#.  FreeBSD documents this explicitly. From the look of the source code
#.  SVR4, ancient SunOS, later Solaris, and AIX all do this.
#.  Issue 8
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Many other implementations similarly always close the file descriptor "
"(except in the case of B<EBADF>, meaning that the file descriptor was "
"invalid)  even if they subsequently report an error on return from "
"B<close>().  POSIX.1 is currently silent on this point, but there are plans "
"to mandate this behavior in the next major release of the standard"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<EINTR> error is a somewhat special case.  Regarding the B<EINTR> "
"error, POSIX.1-2013 says:"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<fcntl>(2), B<fsync>(2), B<open>(2), B<shutdown>(2), B<unlink>(2), "
"B<fclose>(3)"
msgstr ""
"B<fcntl>(2), B<fsync>(2), B<open>(2), B<shutdown>(2), B<unlink>(2), "
"B<fclose>(3)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
