# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# ASPLINUX <man@asp-linux.co.kr>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:12+0200\n"
"PO-Revision-Date: 2000-07-29 08:57+0900\n"
"Last-Translator: ASPLINUX <man@asp-linux.co.kr>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "outb"
msgstr "outb"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"outb, outw, outl, outsb, outsw, outsl, inb, inw, inl, insb, insw, insl, "
"outb_p, outw_p, outl_p, inb_p, inw_p, inl_p - port I/O"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/io.hE<gt>>\n"
msgstr "B<#include E<lt>sys/io.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<unsigned char inb(unsigned short >I<port>B<);>\n"
"B<unsigned char inb_p(unsigned short >I<port>B<);>\n"
"B<unsigned short inw(unsigned short >I<port>B<);>\n"
"B<unsigned short inw_p(unsigned short >I<port>B<);>\n"
"B<unsigned int inl(unsigned short >I<port>B<);>\n"
"B<unsigned int inl_p(unsigned short >I<port>B<);>\n"
msgstr ""
"B<unsigned char inb(unsigned short >I<port>B<);>\n"
"B<unsigned char inb_p(unsigned short >I<port>B<);>\n"
"B<unsigned short inw(unsigned short >I<port>B<);>\n"
"B<unsigned short inw_p(unsigned short >I<port>B<);>\n"
"B<unsigned int inl(unsigned short >I<port>B<);>\n"
"B<unsigned int inl_p(unsigned short >I<port>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void outb(unsigned char >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outb_p(unsigned char >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outw(unsigned short >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outw_p(unsigned short >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outl(unsigned int >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outl_p(unsigned int >I<value>B<, unsigned short >I<port>B<);>\n"
msgstr ""
"B<void outb(unsigned char >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outb_p(unsigned char >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outw(unsigned short >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outw_p(unsigned short >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outl(unsigned int >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outl_p(unsigned int >I<value>B<, unsigned short >I<port>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void insb(unsigned short >I<port>B<, void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void insw(unsigned short >I<port>B<, void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void insl(unsigned short >I<port>B<, void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void outsb(unsigned short >I<port>B<, const void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void outsw(unsigned short >I<port>B<, const void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void outsl(unsigned short >I<port>B<, const void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
msgstr ""
"B<void insb(unsigned short >I<port>B<, void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void insw(unsigned short >I<port>B<, void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void insl(unsigned short >I<port>B<, void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void outsb(unsigned short >I<port>B<, const void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void outsw(unsigned short >I<port>B<, const void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void outsl(unsigned short >I<port>B<, const void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This family of functions is used to do low-level port input and output.  The "
"out* functions do port output, the in* functions do port input; the b-suffix "
"functions are byte-width and the w-suffix functions word-width; the _p-"
"suffix functions pause until the I/O completes."
msgstr ""

#.  , given the following information
#.  in addition to that given in
#.  .BR outb (9).
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"They are primarily designed for internal kernel use, but can be used from "
"user space."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "You compile with B<-O> or B<-O2> or similar. The functions are defined as "
#| "inline macros, and will not be substituted in without optimization "
#| "enabled, causing unresolved references at link time."
msgid ""
"You must compile with B<-O> or B<-O2> or similar.  The functions are defined "
"as inline macros, and will not be substituted in without optimization "
"enabled, causing unresolved references at link time."
msgstr ""
"B<-O>나 B<-O2>, 혹은 비슷한 것으로 컴파일 할 수 있다. 함수는 내부 매크로로 정"
"의되고, 최적화 되지 않은 상태에서 대체 되지 않는다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"You use B<ioperm>(2)  or alternatively B<iopl>(2)  to tell the kernel to "
"allow the user space application to access the I/O ports in question.  "
"Failure to do this will cause the application to receive a segmentation "
"fault."
msgstr ""
"커널이 문제의 I/O 포트에 접근할 수 있도록 사용자 공간 어플리케이션을 허가하도"
"록 알리는데 B<ioperm>(2) 나 선택적으로 B<iopl>(2) 을 사용한다. 이것을 하는데 "
"실패하면, 어플리케이션은 분할 결점을 받게 된다."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<outb> and friends are hardware specific. The I<port> and I<value> "
#| "arguments are in the opposite order to most DOS implementations."
msgid ""
"B<outb>()  and friends are hardware-specific.  The I<value> argument is "
"passed first and the I<port> argument is passed second, which is the "
"opposite order from most DOS implementations."
msgstr ""
"B<outb> 와 그 부류의 것들은 하드웨어 특화 되어 있다.  I<port> 와 I<value> 독"
"립 변수는 대부분의 도스에서 순서가 반대이다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ioperm>(2), B<iopl>(2)"
msgstr "B<ioperm>(2), B<iopl>(2)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-11-10"
msgstr "2022년 11월 10일"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "OUTB"
msgstr "OUTB"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "2017년 9월 15일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "리눅스"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "리눅스 프로그래머 매뉴얼"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<unsigned char inb(unsigned short int >I<port>B<);>\n"
"B<unsigned char inb_p(unsigned short int >I<port>B<);>\n"
"B<unsigned short int inw(unsigned short int >I<port>B<);>\n"
"B<unsigned short int inw_p(unsigned short int >I<port>B<);>\n"
"B<unsigned int inl(unsigned short int >I<port>B<);>\n"
"B<unsigned int inl_p(unsigned short int >I<port>B<);>\n"
msgstr ""
"B<unsigned char inb(unsigned short int >I<port>B<);>\n"
"B<unsigned char inb_p(unsigned short int >I<port>B<);>\n"
"B<unsigned short int inw(unsigned short int >I<port>B<);>\n"
"B<unsigned short int inw_p(unsigned short int >I<port>B<);>\n"
"B<unsigned int inl(unsigned short int >I<port>B<);>\n"
"B<unsigned int inl_p(unsigned short int >I<port>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<void outb(unsigned char >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outb_p(unsigned char >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outw(unsigned short int >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outw_p(unsigned short int >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outl(unsigned int >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outl_p(unsigned int >I<value>B<, unsigned short int >I<port>B<);>\n"
msgstr ""
"B<void outb(unsigned char >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outb_p(unsigned char >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outw(unsigned short int >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outw_p(unsigned short int >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outl(unsigned int >I<value>B<, unsigned short int >I<port>B<);>\n"
"B<void outl_p(unsigned int >I<value>B<, unsigned short int >I<port>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<void insb(unsigned short int >I<port>B<, void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void insw(unsigned short int >I<port>B<, void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void insl(unsigned short int >I<port>B<, void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void outsb(unsigned short int >I<port>B<, const void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void outsw(unsigned short int >I<port>B<, const void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void outsl(unsigned short int >I<port>B<, const void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
msgstr ""
"B<void insb(unsigned short int >I<port>B<, void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void insw(unsigned short int >I<port>B<, void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void insl(unsigned short int >I<port>B<, void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void outsb(unsigned short int >I<port>B<, const void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void outsw(unsigned short int >I<port>B<, const void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"
"B<void outsl(unsigned short int >I<port>B<, const void *>I<addr>B<,>\n"
"B<           unsigned long int >I<count>B<);>\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "호환"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
