# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# 정강훈 <skyeyes@soback.kornet.net>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:31+0200\n"
"PO-Revision-Date: 2000-04-27 08:57+0900\n"
"Last-Translator: 정강훈 <skyeyes@soback.kornet.net>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "uname"
msgstr "uname"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "uname - get name and information about current kernel"
msgstr "uname - 현재 커널에 관한 이름과 정보를 얻어온다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/utsname.hE<gt>>\n"
msgstr "B<#include E<lt>sys/utsname.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int uname(struct utsname *>I<buf>B<);>\n"
msgstr "B<int uname(struct utsname *>I<buf>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<uname>()  returns system information in the structure pointed to by "
"I<buf>.  The I<utsname> struct is defined in I<E<lt>sys/utsname.hE<gt>>:"
msgstr ""
"B<uname>()은 I<buf>가 가리키는 구조체에 시스템 정보를 리턴한다.  I<utsname> "
"구조체는 다음과 같다.  I<E<lt>sys/utsname.hE<gt>>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"struct utsname {\n"
"    char sysname[];    /* Operating system name (e.g., \"Linux\") */\n"
"    char nodename[];   /* Name within communications network\n"
"                          to which the node is attached, if any */\n"
"    char release[];    /* Operating system release\n"
"                          (e.g., \"2.6.28\") */\n"
"    char version[];    /* Operating system version */\n"
"    char machine[];    /* Hardware type identifier */\n"
"#ifdef _GNU_SOURCE\n"
"    char domainname[]; /* NIS or YP domain name */\n"
"#endif\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The length of the arrays in a I<struct utsname> is unspecified (see NOTES); "
"the fields are terminated by a null byte (\\[aq]\\e0\\[aq])."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"성공시, 0이 리턴된다. 에러시, -1이 리턴되며, I<errno>는 적당한 값으로 설정된"
"다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<buf> is not valid."
msgstr "I<buf>가 유효하지 않다."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "The I<domainname> member is a GNU extension."
msgid ""
"The I<domainname> member (the NIS or YP domain name) is a GNU extension."
msgstr "I<domainname 변수는> GNU 확장이다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The length of the fields in the struct varies.  Some operating systems or "
"libraries use a hardcoded 9 or 33 or 65 or 257.  Other systems use "
"B<SYS_NMLN> or B<_SYS_NMLN> or B<UTSLEN> or B<_UTSNAME_LENGTH>.  Clearly, it "
"is a bad idea to use any of these constants; just use sizeof(...).  SVr4 "
"uses 257, \"to support Internet hostnames\" \\[em] this is the largest value "
"likely to be encountered in the wild."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.4BSD."
msgstr "POSIX.1-2001, SVr4, 4.4BSD."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr ""

#.  That was back before Linux 1.0
#.  That was also back before Linux 1.0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Over time, increases in the size of the I<utsname> structure have led to "
"three successive versions of B<uname>(): I<sys_olduname>()  (slot "
"I<__NR_oldolduname>), I<sys_uname>()  (slot I<__NR_olduname>), and "
"I<sys_newuname>()  (slot I<__NR_uname)>.  The first one used length 9 for "
"all fields; the second used 65; the third also uses 65 but adds the "
"I<domainname> field.  The glibc B<uname>()  wrapper function hides these "
"details from applications, invoking the most recent version of the system "
"call provided by the kernel."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The kernel has the name, release, version, and supported machine type built "
"in.  Conversely, the I<nodename> field is configured by the administrator to "
"match the network (this is what the BSD historically calls the \"hostname\", "
"and is set via B<sethostname>(2)).  Similarly, the I<domainname> field is "
"set via B<setdomainname>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Part of the utsname information is also accessible via I</proc/sys/kernel/"
">{I<ostype>, I<hostname>, I<osrelease>, I<version>, I<domainname>}."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<uname>(1), B<getdomainname>(2), B<gethostname>(2), B<uts_namespaces>(7)"
msgstr ""
"B<uname>(1), B<getdomainname>(2), B<gethostname>(2), B<uts_namespaces>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "2023년 2월 5일"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "UNAME"
msgstr "UNAME"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "2017년 9월 15일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "리눅스"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "리눅스 프로그래머 매뉴얼"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/utsname.hE<gt>>"
msgstr "B<#include E<lt>sys/utsname.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int uname(struct utsname *>I<buf>B<);>"
msgstr "B<int uname(struct utsname *>I<buf>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"struct utsname {\n"
"    char sysname[];    /* Operating system name (e.g., \"Linux\") */\n"
"    char nodename[];   /* Name within \"some implementation-defined\n"
"                          network\" */\n"
"    char release[];    /* Operating system release (e.g., \"2.6.28\") */\n"
"    char version[];    /* Operating system version */\n"
"    char machine[];    /* Hardware identifier */\n"
"#ifdef _GNU_SOURCE\n"
"    char domainname[]; /* NIS or YP domain name */\n"
"#endif\n"
"};\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The length of the arrays in a I<struct utsname> is unspecified (see NOTES); "
"the fields are terminated by a null byte (\\(aq\\e0\\(aq)."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"성공시, 0이 리턴된다. 에러시, -1이 리턴되며, I<errno>는 적당한 값으로 설정된"
"다."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "호환"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4.  There is no B<uname>()  call in 4.3BSD."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This is a system call, and the operating system presumably knows its name, "
"release and version.  It also knows what hardware it runs on.  So, four of "
"the fields of the struct are meaningful.  On the other hand, the field "
"I<nodename> is meaningless: it gives the name of the present machine in some "
"undefined network, but typically machines are in more than one network and "
"have several names.  Moreover, the kernel has no way of knowing about such "
"things, so it has to be told what to answer here.  The same holds for the "
"additional I<domainname> field."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"To this end, Linux uses the system calls B<sethostname>(2)  and "
"B<setdomainname>(2).  Note that there is no standard that says that the "
"hostname set by B<sethostname>(2)  is the same string as the I<nodename> "
"field of the struct returned by B<uname>()  (indeed, some systems allow a "
"256-byte hostname and an 8-byte nodename), but this is true on Linux.  The "
"same holds for B<setdomainname>(2)  and the I<domainname> field."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The length of the fields in the struct varies.  Some operating systems or "
"libraries use a hardcoded 9 or 33 or 65 or 257.  Other systems use "
"B<SYS_NMLN> or B<_SYS_NMLN> or B<UTSLEN> or B<_UTSNAME_LENGTH>.  Clearly, it "
"is a bad idea to use any of these constants; just use sizeof(...).  Often "
"257 is chosen in order to have room for an internet hostname."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<uname>(1), B<getdomainname>(2), B<gethostname>(2), B<namespaces>(7)"
msgstr "B<uname>(1), B<getdomainname>(2), B<gethostname>(2), B<namespaces>(7)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
