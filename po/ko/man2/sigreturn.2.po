# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# ASPLINUX <man@asp-linux.co.kr>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:22+0200\n"
"PO-Revision-Date: 2000-07-26 08:57+0900\n"
"Last-Translator: ASPLINUX <man@asp-linux.co.kr>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "sigreturn"
msgstr "sigreturn"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"sigreturn, rt_sigreturn - return from signal handler and cleanup stack frame"
msgstr ""
"sigreturn, rt_sigreturn - 신호 제어기와 클린업 스텍 프레임으로부터 반환"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int sigreturn(...);>\n"
msgstr "B<int sigreturn(...);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. #-#-#-#-#  archlinux: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  debian-unstable: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  fedora-39: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in 3.17 source code]
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: sigreturn.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  See arch/x86/kernel/signal.c::__setup_frame() [in Linux 3.17 source code]
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the Linux kernel determines that an unblocked signal is pending for a "
"process, then, at the next transition back to user mode in that process (e."
"g., upon return from a system call or when the process is rescheduled onto "
"the CPU), it creates a new frame on the user-space stack where it saves "
"various pieces of process context (processor status word, registers, signal "
"mask, and signal stack settings)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The kernel also arranges that, during the transition back to user mode, the "
"signal handler is called, and that, upon return from the handler, control "
"passes to a piece of user-space code commonly called the \"signal "
"trampoline\".  The signal trampoline code in turn calls B<sigreturn>()."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This B<sigreturn>()  call undoes everything that was done\\[em]changing the "
"process's signal mask, switching signal stacks (see "
"B<sigaltstack>(2))\\[em]in order to invoke the signal handler.  Using the "
"information that was earlier saved on the user-space stack B<sigreturn>()  "
"restores the process's signal mask, switches stacks, and restores the "
"process's context (processor flags and registers, including the stack "
"pointer and instruction pointer), so that the process resumes execution at "
"the point where it was interrupted by the signal."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sigreturn>()  never returns."
msgstr "B<sigreturn>() 은 반환값을 가지지 않는다."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Many UNIX-type systems have a B<sigreturn>()  system call or near "
"equivalent.  However, this call is not specified in POSIX, and details of "
"its behavior vary across systems."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "주의"

#.  See sysdeps/unix/sysv/linux/sigreturn.c and
#.  signal/sigreturn.c in the glibc source
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sigreturn>()  exists only to allow the implementation of signal handlers.  "
"It should B<never> be called directly.  (Indeed, a simple B<sigreturn>()  "
"wrapper in the GNU C library simply returns -1, with I<errno> set to "
"B<ENOSYS>.)  Details of the arguments (if any) passed to B<sigreturn>()  "
"vary depending on the architecture.  (On some architectures, such as x86-64, "
"B<sigreturn>()  takes no arguments, since all of the information that it "
"requires is available in the stack frame that was previously created by the "
"kernel on the user-space stack.)"
msgstr ""

#.  See, for example, sysdeps/unix/sysv/linux/i386/sigaction.c and
#.  sysdeps/unix/sysv/linux/x86_64/sigaction.c in the glibc (2.20) source.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Once upon a time, UNIX systems placed the signal trampoline code onto the "
"user stack.  Nowadays, pages of the user stack are protected so as to "
"disallow code execution.  Thus, on contemporary Linux systems, depending on "
"the architecture, the signal trampoline code lives either in the B<vdso>(7)  "
"or in the C library.  In the latter case, the C library's B<sigaction>(2)  "
"wrapper function informs the kernel of the location of the trampoline code "
"by placing its address in the I<sa_restorer> field of the I<sigaction> "
"structure, and sets the B<SA_RESTORER> flag in the I<sa_flags> field."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The saved process context information is placed in a I<ucontext_t> structure "
"(see I<E<lt>sys/ucontext.hE<gt>>).  That structure is visible within the "
"signal handler as the third argument of a handler established via "
"B<sigaction>(2)  with the B<SA_SIGINFO> flag."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On some other UNIX systems, the operation of the signal trampoline differs a "
"little.  In particular, on some systems, upon transitioning back to user "
"mode, the kernel passes control to the trampoline (rather than the signal "
"handler), and the trampoline code calls the signal handler (and then calls "
"B<sigreturn>()  once the handler returns)."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The original Linux system call was named B<sigreturn>().  However, with the "
"addition of real-time signals in Linux 2.2, a new system call, "
"B<rt_sigreturn>()  was added to support an enlarged I<sigset_t> type.  The "
"GNU C library hides these details from us, transparently employing "
"B<rt_sigreturn>()  when the kernel provides it."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<kill>(2), B<restart_syscall>(2), B<sigaltstack>(2), B<signal>(2), "
"B<getcontext>(3), B<signal>(7), B<vdso>(7)"
msgstr ""
"B<kill>(2), B<restart_syscall>(2), B<sigaltstack>(2), B<signal>(2), "
"B<getcontext>(3), B<signal>(7), B<vdso>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "2023년 2월 5일"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SIGRETURN"
msgstr "SIGRETURN"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "2017년 9월 15일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "리눅스"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "리눅스 프로그래머 매뉴얼"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int sigreturn(...);>"
msgstr "B<int sigreturn(...);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This B<sigreturn>()  call undoes everything that was done\\(emchanging the "
"process's signal mask, switching signal stacks (see "
"B<sigaltstack>(2))\\(emin order to invoke the signal handler.  Using the "
"information that was earlier saved on the user-space stack B<sigreturn>()  "
"restores the process's signal mask, switches stacks, and restores the "
"process's context (processor flags and registers, including the stack "
"pointer and instruction pointer), so that the process resumes execution at "
"the point where it was interrupted by the signal."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "호환"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
