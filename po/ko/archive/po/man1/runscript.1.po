# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-11-14 19:24+0100\n"
"PO-Revision-Date: 2000-04-29 08:57+0900\n"
"Last-Translator: Unknown <>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: runscript.1:5
#, no-wrap
msgid "RUNSCRIPT"
msgstr "RUNSCRIPT"

#. type: TH
#: runscript.1:5
#, no-wrap
msgid "28 Oct 1994"
msgstr "1994년 10월 28일"

#. type: TH
#: runscript.1:5
#, no-wrap
msgid "User's Manual"
msgstr "사용자 설명서"

#. type: SH
#: runscript.1:6
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: runscript.1:8
msgid "runscript - script interpreter for minicom"
msgstr "runscript - minicom을 위한 스크립트 인터프리터"

#. type: SH
#: runscript.1:8
#, no-wrap
msgid "SYNOPSIS"
msgstr "사용법"

#. type: Plain text
#: runscript.1:11
msgid "B<runscript> scriptname"
msgstr "B<runscript> 스크립트"

#. type: SH
#: runscript.1:11
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: runscript.1:16
#, fuzzy
msgid ""
"B<runscript> is a simple script interpreter that can be called from within "
"the minicom communications program to automate tasks like logging in to a "
"unix system or your favorite bbs."
msgstr ""
"이 풀그림의 인자로 단지 하나의 스크립트 파일 이름만을 사용할 수 있다. 또한 이"
"것의 입/출력은 단지 연결된 호스트를 대상으로만 하고 있다. runscript에서 처리하"
"는 모든 메시지는 현재 화면의 것을 입력받아, stderr로 보낸다.  그래서, 이 인터"
"프리터는 B<minicom>에서 작동되도록 고안 작성되었다. 참고: 이 스크립트 파일은 "
"초기값으로 /usr/lib/minicom 경로 안에 있어야한다. (즉, 기본적으로 시스템 관리"
"자만이 이 스크립트 파일을 관리할 수 있다. - 초기값)"

#. type: SH
#: runscript.1:16
#, no-wrap
msgid "INVOCATION"
msgstr ""

#. type: Plain text
#: runscript.1:23
msgid ""
"The program expects a script name as the only argument, and it expects that "
"it's input and output are connected to the \\^\"remote end\\^\", the system "
"you are connecting to. All messages from B<runscript> ment for the local "
"screen are directed to the B<stderr> output. All this is automatically taken "
"care of if you run it from B<minicom>."
msgstr ""

#. type: SH
#: runscript.1:23
#, no-wrap
msgid "KEYWORDS"
msgstr "키워드"

#. type: TP
#: runscript.1:24
#, no-wrap
msgid "Runscript reckognizes the following commands:"
msgstr "runscript는 다음의 명령들을 인식한다:"

#. type: Plain text
#: runscript.1:33
#, no-wrap
msgid ""
"expect   send     goto     gosub    return   \\^!\n"
"exit     print    set      inc      dec      if\n"
"timeout  verbose  sleep    break    call\n"
msgstr ""
"expect   send     goto     gosub    return   \\^!\n"
"exit     print    set      inc      dec      if\n"
"timeout  verbose  sleep    break    call\n"

#. type: SH
#: runscript.1:36
#, no-wrap
msgid "OVERVIEW OF KEYWORDS"
msgstr "키워드 개요"

#. type: TP
#: runscript.1:37
#, no-wrap
msgid "B<send E<lt>stringE<gt>>"
msgstr "B<send E<lt>문자열E<gt>>"

#. type: Plain text
#: runscript.1:43
#, fuzzy, no-wrap
msgid ""
"E<lt>stringE<gt> is sent to the modem. It is followed by a '\\er'.\n"
"E<lt>stringE<gt> can be:\n"
"  - regular text, eg 'send hello'\n"
"  - text enclosed in quotes, eg 'send \\^\"hello world\\^\"'\n"
msgstr ""
"E<lt>문자열E<gt>은 모뎀으로 보낼 문자열이다. E<lt>문자열E<gt>로 사용될 수 있는 것은: \n"
"  - 단순한 단어, 예 'send hello'\n"
"  - 따움표로 둘러싼 단어들, 예 'send \\^\"hello world\\^\"'\n"

#. type: TP
#: runscript.1:43
#, no-wrap
msgid "Within E<lt>stringE<gt> the following sequences are reckognized:"
msgstr "E<lt>문자열E<gt> 안에는 다음과 같은 제어문자들을 사용할 수있다."

#. type: Plain text
#: runscript.1:52
#, no-wrap
msgid ""
"    \\en - newline\n"
"    \\er - carriage return\n"
"    \\ea - bell\n"
"    \\eb - backspace\n"
"    \\ec - don't send the default '\\er'.\n"
"    \\ef - formfeed\n"
"    \\eo - send character B<o> (B<o> is an octal number)\n"
msgstr ""
"    \\en - 새줄\n"
"    \\er - 리턴문자\n"
"    \\ea - 벨소리\n"
"    \\eb - 백스페이스\n"
"    \\ec - 초기값인 '\\er' 자동 보내기를 하지 않음.\n"
"    \\ef - 용지바꿈문자\n"
"    \\eo - B<o>문자 보냄 (B<o>는 8진수를 뜻한다.)\n"

#. type: Plain text
#: runscript.1:60
#, fuzzy
msgid ""
"Also $(environment_variable) can be used, for example $(TERM).  Minicom "
"passes three special environment variables: $(LOGIN), which is the username, "
"$(PASS), which is the password, as defined in the proper entry of the dialing "
"directory, and $(TERMLIN) which is the number of actual terminal lines on "
"your screen (that is, the statusline excluded)."
msgstr ""
"또한 $(환경변수_값)을 사용할 수 있다.  예: $(TERM). minicom에서는 두개의 특별"
"한 환경변수값을 제공하는데, 사용자 ID로 $(LOGIN), 비밀번호로 $(PASS)가 그것이"
"다.  이 값은 전화번호부에서 지정한다."

#. type: TP
#: runscript.1:60
#, no-wrap
msgid "B<print E<lt>stringE<gt>>"
msgstr "B<print E<lt>문자열E<gt>>"

#. type: Plain text
#: runscript.1:64
msgid ""
"Prints E<lt>stringE<gt> to the local screen. Default followed by '\\er\\en'.  "
"See the description of 'send' above."
msgstr ""
"E<lt>문자열E<gt>을 현재 화면에 출력한다.  끝에는 자동으로 '\\er\\en' 문자가 붙"
"는다.  나머지는 'send'와 사용법이 같다."

#. type: TP
#: runscript.1:64
#, no-wrap
msgid "B<label:>"
msgstr "B<label:>"

#. type: Plain text
#: runscript.1:68
msgid "Declares a label (with the name 'label') to use with goto or gosub."
msgstr "goto나 gosub에의해 이동되는 분기점을 지정."

#. type: TP
#: runscript.1:68
#, no-wrap
msgid "B<goto E<lt>labelE<gt>>"
msgstr "B<goto E<lt>labelE<gt>>"

#. type: Plain text
#: runscript.1:71
msgid "Jump to another place in the program."
msgstr "스크립트 안에서 다른 위치로 이동한다."

#. type: TP
#: runscript.1:71
#, no-wrap
msgid "B<gosub E<lt>labelE<gt>>"
msgstr "B<gosub E<lt>labelE<gt>>"

#. type: Plain text
#: runscript.1:76
#, fuzzy
msgid ""
"Jumps to another place in the program. When the statement 'return' is "
"encountered, control returns to the statement after the gosub.  Gosub's can "
"be nested."
msgstr "서브 goto로 'return'에 도달했을 때, 현재의 영역 안에서 이동한다."

#. type: TP
#: runscript.1:76
#, no-wrap
msgid "B<return>"
msgstr "B<return>"

#. type: Plain text
#: runscript.1:79
msgid "Return from a gosub."
msgstr "gosub 영역을 벗어난다."

#. type: TP
#: runscript.1:79
#, no-wrap
msgid "B<! E<lt>commandE<gt>>"
msgstr "B<! E<lt>명령E<gt>>"

#. type: Plain text
#: runscript.1:84
msgid ""
"Runs a shell for you in which 'command' is executed. On return, the variable "
"'$?' is set to the exit status of this command, so you can subsequently test "
"it using 'if'."
msgstr ""
"쉘명령을 실행한다. 그 명령의 리턴값은 $? 변수에 저장된다.  그래서, 'if' 문으"
"로 선택적인 문을 만들 수 있다."

#. type: TP
#: runscript.1:84
#, no-wrap
msgid "B<exit [value]>"
msgstr "B<exit [값]>"

#. type: Plain text
#: runscript.1:87
msgid "Exit from \\^\"runscript\\^\" with an optional exit status. (default 1)"
msgstr "부여한 I<값> 돌려주며, \\^\"runscript\\^\"를 마친다. 초기값 1"

#. type: TP
#: runscript.1:87
#, no-wrap
msgid "B<set E<lt>variableE<gt> E<lt>valueE<gt>>"
msgstr "B<set E<lt>변수명E<gt> E<lt>변수값E<gt>>"

#. type: Plain text
#: runscript.1:92
msgid ""
"Sets the value of E<lt>variableE<gt> (which is a single letter a-z) to the "
"value E<lt>valueE<gt>. If E<lt>variableE<gt> does not exist, it will be "
"created.  E<lt>valueE<gt> can be a integer value or another variable."
msgstr ""
"E<lt>변수명E<gt>은 a부터 z까지의 단일 문자이며, 이 변수에 E<lt>변수값E<gt>을 "
"지정한다. 변수 선언 없이 사용되면, 즉시 만들고, E<lt>변수값E<gt>은 숫자나 다"
"른 변수여야한다."

#. type: TP
#: runscript.1:92
#, no-wrap
msgid "B<inc E<lt>variableE<gt>>"
msgstr "B<inc E<lt>변수명E<gt>>"

#. type: Plain text
#: runscript.1:95
msgid "Increments the value of E<lt>variableE<gt> by one."
msgstr "E<lt>변수명E<gt>의 변수값을 하나씩 증가시킨다."

#. type: TP
#: runscript.1:95
#, no-wrap
msgid "B<dec E<lt>variableE<gt>>"
msgstr "B<dec E<lt>변수명E<gt>>"

#. type: Plain text
#: runscript.1:98
msgid "Decrements the value of E<lt>variableE<gt> by one."
msgstr "E<lt>변수명E<gt>의 변수값을 하나씩 감소시킨다."

#. type: TP
#: runscript.1:98
#, no-wrap
msgid "B<if E<lt>valueE<gt> E<lt>operatorE<gt> E<lt>valueE<gt> E<lt>statementE<gt>>"
msgstr "B<if E<lt>변수E<gt> E<lt>연산자E<gt> E<lt>변수E<gt> E<lt>문E<gt>>"

#. type: Plain text
#: runscript.1:102
msgid ""
"Conditional execution of E<lt>statementE<gt>. E<lt>operatorE<gt> can be "
"E<lt>, E<gt>, != or =.  Eg, 'if a E<gt> 3 goto exitlabel'."
msgstr ""
"사용할 수 있는 연산자는 E<lt>, E<gt>, !=, -.  예, 'if a E<gt> 3 goto "
"exitlabel'."

#. type: TP
#: runscript.1:102
#, no-wrap
msgid "B<timeout E<lt>valueE<gt>>"
msgstr "B<timeout E<lt>값E<gt>>"

#. type: Plain text
#: runscript.1:108
msgid ""
"Sets the global timeout. By default, 'runscript' will exit after 120 seconds. "
"This can be changed with this command. Warning: this command acts differently "
"within an 'expect' statement, but more about that later."
msgstr ""
"제한시간을 설정한다.  초기값으로 'runscript' 전체 실행시간은 2분이다.  즉, 특"
"별히 시간을 정해 놓지 않으면, 2분 후에는 자동으로 종료된다.  이 값을 이명령으"
"로 바꿀 수 있다.  이 명령은 'expect' 문에서는 다르게 동작한다.  아래 참조."

#. type: TP
#: runscript.1:108
#, no-wrap
msgid "B<verbose E<lt>on|offE<gt>>"
msgstr "B<verbose E<lt>on|offE<gt>>"

#. type: Plain text
#: runscript.1:113
#, fuzzy
msgid ""
"By default, this is 'on'. That means that anything that is being read from "
"the modem by 'runscript', gets echoed to the screen.  This is so that you can "
"see what 'runscript' is doing."
msgstr "초기값은 'on'. 'runscript' 작동 상태를 화면에서 볼 수 있게 한다."

#. type: TP
#: runscript.1:113
#, no-wrap
msgid "B<sleep E<lt>valueE<gt>>"
msgstr "B<sleep E<lt>값E<gt>>"

#. type: Plain text
#: runscript.1:116
msgid "Suspend execution for E<lt>valueE<gt> seconds."
msgstr "E<lt>값E<gt> 초 동안 실행을 잠시 멈춘다."

#. type: TP
#: runscript.1:116
#, no-wrap
msgid "B<expect>"
msgstr "B<expect>"

#. type: Plain text
#: runscript.1:125
#, no-wrap
msgid ""
"  expect {\n"
"    pattern  [statement]\n"
"    pattern  [statement]\n"
"    [timeout E<lt>valueE<gt> [statement] ]\n"
"    ....\n"
"  }\n"
msgstr ""
"  expect {\n"
"    패턴 [문]\n"
"    패턴 [문]\n"
"    [timeout E<lt>값E<gt> [문] ]\n"
"    ....\n"
"  }\n"

#. type: Plain text
#: runscript.1:134
msgid ""
"The most important command of all. Expect keeps reading from the input until "
"it reads a pattern that matches one of the specified ones.  If expect "
"encounters an optional statement after that pattern, it will execute it. "
"Otherwise the default is to just break out of the expect. 'pattern' is a "
"string, just as in 'send' (see above).  Normally, expect will timeout in 60 "
"seconds and just exit, but this can be changed with the timeout command."
msgstr ""
"가장 중요한 명령어이다.  패턴을 기다려서, 해당 패턴이 나타나면, 해당 E<lt>문"
"E<gt>을 실행한다.  패턴은 'send'에서 설명한 문자열이며, 일반적으로 첫번째, 패"
"턴에 대한 문은 생략해서, 만일 첫번째 패턴이 나타나면, 바로 expect 문을 벗어나 "
"다음 작업을 계속 하도록 한다.  만일 이때, 첫번째 패턴이 나타나지 않고, 두번째 "
"패턴이 나타난다면, 두번째 패턴에서 지정한 문을 실행한다.  초기값으로 expect의 "
"최대시간은 60초이다. 이것을 바꾸려면 timeout을 사용한다."

#. type: TP
#: runscript.1:134
#, no-wrap
msgid "B<break>"
msgstr "B<break>"

#. type: Plain text
#: runscript.1:139
msgid ""
"Break out of an 'expect' statement. This is normally only useful as argument "
"to 'timeout' within an expect, because the default action of timeout is to "
"exit immediately."
msgstr ""
"\\^'expect' 문을 중지한다. 이 명령은 'timeout' 명령에 대한 문으로 일반적으로 "
"사용된다. 'timeout'에 대한 아무른 작업을 지시하지 않으면, 제한 시간을 초과했"
"을 경우, 그 스크립트 자체를 마쳐버리기 때문이다."

#. type: TP
#: runscript.1:139
#, no-wrap
msgid "B<call E<lt>scriptnameE<gt>>"
msgstr "B<call E<lt>스크립트이름E<gt>>"

#. type: Plain text
#: runscript.1:143
msgid ""
"Transfers control to another scriptfile. When that scriptfile finishes "
"without errors, the original script will continue."
msgstr ""
"다른 E<lt>스크립트이름E<gt>의 파일을 불러와 실행한다.  이때 아무런 오류가 나"
"지 않으면 나머지 스크립트를 실행한다."

#. type: SH
#: runscript.1:143
#, no-wrap
msgid "NOTES"
msgstr "참고"

#. type: Plain text
#: runscript.1:151
msgid ""
"Well, I don't think this is enough information to make you an experienced "
"'programmer' in 'runscript', but together with the examples it shouldn't be "
"too hard to write some useful script files. Things will be easier if you have "
"experience with BASIC.  The B<minicom> source code comes together with two "
"example scripts, B<scriptdemo> and B<unixlogin>. Especially the last one is a "
"good base to build on for your own scripts."
msgstr ""
"그다지 프로그래밍에 대한 지식이 없어도 쉽게 만들 수 있을 것이라 생각한다. "
"BASIC 정도를 한번쯤 사용해 본 사람이라면 쉽게 작성할 수 있을 것이다.  "
"B<minicom> 패키지 안에는 B<scriptdemo>, B<unixlogin> 등 몇개의 스크립트 파일"
"이 예제로 있는데, 이것을 참고한다면, 보다 유용한 스크립트 파일을 만들 수 있을 "
"것이다."

#. type: SH
#: runscript.1:151
#, no-wrap
msgid "BUGS"
msgstr "버그"

#. type: Plain text
#: runscript.1:153
msgid "Runscript should be built in to minicom."
msgstr ""
"runscript는 minicom용으로 고안, 작성되었기 때문에, 다른 곳에서는 제대로 동작하"
"지 않을 수 있다."

#. type: SH
#: runscript.1:153
#, no-wrap
msgid "AUTHOR"
msgstr "저자"

#. type: Plain text
#: runscript.1:154
msgid "Miquel van Smoorenburg, E<lt>miquels@drinkel.ow.orgE<gt>"
msgstr "Miquel van Smoorenburg, E<lt>miquels@drinkel.ow.orgE<gt>"
