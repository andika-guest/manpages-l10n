# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:06+0200\n"
"PO-Revision-Date: 2000-04-29 08:57+0900\n"
"Last-Translator: Unknown <>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "LOOK 1"
msgid "LOOK"
msgstr "LOOK 1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "2022년 5월 11일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr "사용자 명령"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "display lines beginning with a given string"
msgid "look - display lines beginning with a given string"
msgstr "주어진 문자열로 시작하는 줄을 보여준다."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<look> [options] I<string> [I<file>]"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "The E<.Nm look> utility displays any lines in E<.Ar file> which contain "
#| "E<.Ar string> as a prefix.  As E<.Nm look> performs a binary search, the "
#| "lines in E<.Ar file> must be sorted."
msgid ""
"The B<look> utility displays any lines in I<file> which contain I<string> as "
"a prefix. As B<look> performs a binary search, the lines in I<file> must be "
"sorted (where B<sort>(1) was given the same options B<-d> and/or B<-f> that "
"B<look> is invoked with)."
msgstr ""
"E<.Nm look> 명령은 E<.Ar file> 에서 E<.Ar string> 으로 시작하는 줄을 찾아 보"
"여준다.  이진파일 검색일 경우에는, E<.Ar file> 안의 각 줄이 먼저 정열되어야한"
"다."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "If E<.Ar file> is not specified, the file E<.Pa /usr/dict/words> is used, "
#| "only alphanumeric characters are compared and the case of alphabetic "
#| "characters is ignored."
msgid ""
"If I<file> is not specified, the file I</usr/share/dict/words> is used, only "
"alphanumeric characters are compared and the case of alphabetic characters "
"is ignored."
msgstr ""
"E<.Ar file> 이 지정되지 않으면, E<.Pa /usr/dict/words> 파일을 사용한다.  이 "
"파일은 단지 알파벳 단어만 있으며, 대소문자는 무시된다.  (이것은 영문 맞춤법 "
"검사에 유용하게 쓰인다.)"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "옵션"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-a>, B<--alternative>"
msgstr "B<-a>, B<--alternative>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Use the alternative dictionary file."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "B<-a>, B<--all>"
msgid "B<-d>, B<--alphanum>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
msgid ""
"Use normal dictionary character set and order, i.e., only blanks and "
"alphanumeric characters are compared. This is on by default if no file is "
"specified."
msgstr "영문자만을 검색한다."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Note that blanks have been added to dictionary character set for "
"compatibility with B<sort -d> command since version 2.28."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-f>, B<--ignore-case>"
msgstr "B<-f>, B<--ignore-case>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "Ignore the case of alphabetic characters."
msgid ""
"Ignore the case of alphabetic characters. This is on by default if no file "
"is specified."
msgstr "대소문자를 구분하지 않는다."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "B<-t>, B<--test>"
msgid "B<-t>, B<--terminate> I<character>"
msgstr "B<-t>, B<--test>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify a string termination character, i.e., only the characters in "
"I<string> up to and including the first occurrence of I<character> are "
"compared."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "도움말을 보여주고 마친다."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "버전 정보를 보여주고 마친다."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The E<.Nm look> utility exits 0 if one or more lines were found and "
#| "displayed, 1 if no lines were found, and E<gt>1 if an error occurred."
msgid ""
"The B<look> utility exits 0 if one or more lines were found and displayed, 1 "
"if no lines were found, and E<gt>1 if an error occurred."
msgstr ""
"E<.Nm look> 명령은 오류 없이 끝나면 0, 오류가 있으면 1보다 큰 수를 리턴하고 "
"마친다."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "ENVIRONMENT"
msgstr "환경"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "B<EEXIST>"
msgid "B<WORDLIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Path to a dictionary file. The environment variable has greater priority "
"than the dictionary path defined in the B<FILES> segment."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "FILES"
msgstr "파일"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "Pa /usr/dict/words"
msgid "I</usr/share/dict/words>"
msgstr "Pa /usr/dict/words"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "the dictionary"
msgstr "사전"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I</usr/share/dict/web2>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "the dictionary"
msgid "the alternative dictionary"
msgstr "사전"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "E<.Nm Look> appeared in Version 7 AT&T Unix."
msgid "The B<look> utility appeared in Version 7 AT&T Unix."
msgstr "E<.Nm Look> 명령은 Version 7 AT&T Unix에서 나타났다."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLES"
msgstr "폐제"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"sort -d /etc/passwd -o /tmp/look.dict\n"
"look -t: root:foobar /tmp/look.dict\n"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<grep>(1), B<sort>(1)"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "버그 보고"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "가용성"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<look> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "2022년 2월 14일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The E<.Nm look> utility displays any lines in E<.Ar file> which contain "
#| "E<.Ar string> as a prefix.  As E<.Nm look> performs a binary search, the "
#| "lines in E<.Ar file> must be sorted."
msgid ""
"The B<look> utility displays any lines in I<file> which contain I<string>. "
"As B<look> performs a binary search, the lines in I<file> must be sorted "
"(where B<sort>(1) was given the same options B<-d> and/or B<-f> that B<look> "
"is invoked with)."
msgstr ""
"E<.Nm look> 명령은 E<.Ar file> 에서 E<.Ar string> 으로 시작하는 줄을 찾아 보"
"여준다.  이진파일 검색일 경우에는, E<.Ar file> 안의 각 줄이 먼저 정열되어야한"
"다."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "버전 정보를 출력합니다."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Path to a dictionary file. The environment variable has greater priority "
"than the dictionary path defined in FILES segment."
msgstr ""
