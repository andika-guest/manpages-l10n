# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# ASPLINUX <man@asp-linux.co.kr>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:48+0200\n"
"PO-Revision-Date: 2000-07-29 08:57+0900\n"
"Last-Translator: ASPLINUX <man@asp-linux.co.kr>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "acct"
msgstr "acct"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-05-03"
msgstr "2023년 5월 3일"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "acct - execution accounting file"
msgid "acct - process accounting file"
msgstr "acct - 실행 연산파일."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/acct.hE<gt>>\n"
msgstr "B<#include E<lt>sys/acct.hE<gt>>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the kernel is built with the process accounting option enabled "
"(B<CONFIG_BSD_PROCESS_ACCT>), then calling B<acct>(2)  starts process "
"accounting, for example:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "acct(\"/var/log/pacct\");"
msgstr "acct(\"/var/log/pacct\");"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When process accounting is enabled, the kernel writes a record to the "
"accounting file as each process on the system terminates.  This record "
"contains information about the terminated process, and is defined in "
"I<E<lt>sys/acct.hE<gt>> as follows:"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ""
"#define ACCT_COMM 16\n"
"\\&\n"
"typedef u_int16_t comp_t;\n"
"\\&\n"
"struct acct {\n"
"    char ac_flag;           /* Accounting flags */\n"
"    u_int16_t ac_uid;       /* Accounting user ID */\n"
"    u_int16_t ac_gid;       /* Accounting group ID */\n"
"    u_int16_t ac_tty;       /* Controlling terminal */\n"
"    u_int32_t ac_btime;     /* Process creation time\n"
"                               (seconds since the Epoch) */\n"
"    comp_t    ac_utime;     /* User CPU time */\n"
"    comp_t    ac_stime;     /* System CPU time */\n"
"    comp_t    ac_etime;     /* Elapsed time */\n"
"    comp_t    ac_mem;       /* Average memory usage (kB) */\n"
"    comp_t    ac_io;        /* Characters transferred (unused) */\n"
"    comp_t    ac_rw;        /* Blocks read or written (unused) */\n"
"    comp_t    ac_minflt;    /* Minor page faults */\n"
"    comp_t    ac_majflt;    /* Major page faults */\n"
"    comp_t    ac_swaps;     /* Number of swaps (unused) */\n"
"    u_int32_t ac_exitcode;  /* Process termination status\n"
"                               (see wait(2)) */\n"
"    char      ac_comm[ACCT_COMM+1];\n"
"                            /* Command name (basename of last\n"
"                               executed command; null-terminated) */\n"
"    char      ac_pad[I<X>];    /* padding bytes */\n"
"};\n"
"\\&\n"
"enum {          /* Bits that may be set in ac_flag field */\n"
"    AFORK = 0x01,           /* Has executed fork, but no exec */\n"
"    ASU   = 0x02,           /* Used superuser privileges */\n"
"    ACORE = 0x08,           /* Dumped core */\n"
"    AXSIG = 0x10            /* Killed by a signal */\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<comp_t> data type is a floating-point value consisting of a 3-bit, "
"base-8 exponent, and a 13-bit mantissa.  A value, I<c>, of this type can be "
"converted to a (long) integer as follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "    v = (c & 0x1fff) E<lt>E<lt> (((c E<gt>E<gt> 13) & 0x7) * 3);\n"
msgstr "    v = (c & 0x1fff) E<lt>E<lt> (((c E<gt>E<gt> 13) & 0x7) * 3);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<ac_utime>, I<ac_stime>, and I<ac_etime> fields measure time in \"clock "
"ticks\"; divide these values by I<sysconf(_SC_CLK_TCK)> to convert them to "
"seconds."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "acct - execution accounting file"
msgid "Version 3 accounting file format"
msgstr "acct - 실행 연산파일."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Since Linux 2.6.8, an optional alternative version of the accounting file "
"can be produced if the B<CONFIG_BSD_PROCESS_ACCT_V3> option is set when "
"building the kernel.  With this option is set, the records written to the "
"accounting file contain additional fields, and the width of I<c_uid> and "
"I<ac_gid> fields is widened from 16 to 32 bits (in line with the increased "
"size of UID and GIDs in Linux 2.4 and later).  The records are defined as "
"follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct acct_v3 {\n"
"    char      ac_flag;      /* Flags */\n"
"    char      ac_version;   /* Always set to ACCT_VERSION (3) */\n"
"    u_int16_t ac_tty;       /* Controlling terminal */\n"
"    u_int32_t ac_exitcode;  /* Process termination status */\n"
"    u_int32_t ac_uid;       /* Real user ID */\n"
"    u_int32_t ac_gid;       /* Real group ID */\n"
"    u_int32_t ac_pid;       /* Process ID */\n"
"    u_int32_t ac_ppid;      /* Parent process ID */\n"
"    u_int32_t ac_btime;     /* Process creation time */\n"
"    float     ac_etime;     /* Elapsed time */\n"
"    comp_t    ac_utime;     /* User CPU time */\n"
"    comp_t    ac_stime;     /* System time */\n"
"    comp_t    ac_mem;       /* Average memory usage (kB) */\n"
"    comp_t    ac_io;        /* Characters transferred (unused) */\n"
"    comp_t    ac_rw;        /* Blocks read or written\n"
"                               (unused) */\n"
"    comp_t    ac_minflt;    /* Minor page faults */\n"
"    comp_t    ac_majflt;    /* Major page faults */\n"
"    comp_t    ac_swaps;     /* Number of swaps (unused) */\n"
"    char      ac_comm[ACCT_COMM]; /* Command name */\n"
"};\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Although it is present on most systems, it is not standardized, and the "
"details vary somewhat between systems."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.6."
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Process accounting originated on BSD."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Records in the accounting file are ordered by termination time of the "
"process."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Up to and including Linux 2.6.9, a separate accounting record is written for "
"each thread created using the NPTL threading library; since Linux 2.6.10, a "
"single accounting record is written for the entire process on termination of "
"the last thread in the process."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I</proc/sys/kernel/acct> file, described in B<proc>(5), defines settings "
"that control the behavior of process accounting when disk space runs low."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<lastcomm>(1), B<acct>(2), B<accton>(8), B<sa>(8)"
msgstr "B<lastcomm>(1), B<acct>(2), B<accton>(8), B<sa>(8)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-04"
msgstr "2022년 12월 4일"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "#define ACCT_COMM 16\n"
msgstr "#define ACCT_COMM 16\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "typedef u_int16_t comp_t;\n"
msgstr "typedef u_int16_t comp_t;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"struct acct {\n"
"    char ac_flag;           /* Accounting flags */\n"
"    u_int16_t ac_uid;       /* Accounting user ID */\n"
"    u_int16_t ac_gid;       /* Accounting group ID */\n"
"    u_int16_t ac_tty;       /* Controlling terminal */\n"
"    u_int32_t ac_btime;     /* Process creation time\n"
"                               (seconds since the Epoch) */\n"
"    comp_t    ac_utime;     /* User CPU time */\n"
"    comp_t    ac_stime;     /* System CPU time */\n"
"    comp_t    ac_etime;     /* Elapsed time */\n"
"    comp_t    ac_mem;       /* Average memory usage (kB) */\n"
"    comp_t    ac_io;        /* Characters transferred (unused) */\n"
"    comp_t    ac_rw;        /* Blocks read or written (unused) */\n"
"    comp_t    ac_minflt;    /* Minor page faults */\n"
"    comp_t    ac_majflt;    /* Major page faults */\n"
"    comp_t    ac_swaps;     /* Number of swaps (unused) */\n"
"    u_int32_t ac_exitcode;  /* Process termination status\n"
"                               (see wait(2)) */\n"
"    char      ac_comm[ACCT_COMM+1];\n"
"                            /* Command name (basename of last\n"
"                               executed command; null-terminated) */\n"
"    char      ac_pad[I<X>];    /* padding bytes */\n"
"};\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"enum {          /* Bits that may be set in ac_flag field */\n"
"    AFORK = 0x01,           /* Has executed fork, but no exec */\n"
"    ASU   = 0x02,           /* Used superuser privileges */\n"
"    ACORE = 0x08,           /* Dumped core */\n"
"    AXSIG = 0x10            /* Killed by a signal */\n"
"};\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid "The I<acct_v3> structure is defined since glibc 2.6."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"Process accounting originated on BSD.  Although it is present on most "
"systems, it is not standardized, and the details vary somewhat between "
"systems."
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "ACCT"
msgstr "ACCT"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "2017년 9월 15일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "리눅스"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "리눅스 프로그래머 매뉴얼"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/acct.hE<gt>>"
msgstr "B<#include E<lt>sys/acct.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Since kernel 2.6.8, an optional alternative version of the accounting file "
"can be produced if the B<CONFIG_BSD_PROCESS_ACCT_V3> option is set when "
"building the kernel.  With this option is set, the records written to the "
"accounting file contain additional fields, and the width of I<c_uid> and "
"I<ac_gid> fields is widened from 16 to 32 bits (in line with the increased "
"size of UID and GIDs in Linux 2.4 and later).  The records are defined as "
"follows:"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "The I<acct_v3> structure is defined in glibc since version 2.6."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "호환"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"In kernels up to and including 2.6.9, a separate accounting record is "
"written for each thread created using the NPTL threading library; since "
"Linux 2.6.10, a single accounting record is written for the entire process "
"on termination of the last thread in the process."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
