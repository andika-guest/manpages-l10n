# Italian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Giulio Daprelà <giulio@pluto.it>, 2005.
# Elisabetta Galli <lab@kkk.it>, 2007.
# Marco Curreli <marcocurreli@tiscali.it>, 2013, 2018, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2023-08-27 17:31+0200\n"
"PO-Revision-Date: 2021-04-07 00:49+0200\n"
"Last-Translator: Marco Curreli <marcocurreli@tiscali.it>\n"
"Language-Team: Italian <pluto-ildp@lists.pluto.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "UNIMPLEMENTED"
msgstr "UNIMPLEMENTED"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-09"
msgstr "9 ottobre 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"afs_syscall, break, fattach, fdetach, ftime, getmsg, getpmsg, gtty, "
"isastream, lock, madvise1, mpx, prof, profil, putmsg, putpmsg, security, "
"stty, tuxcall, ulimit, vserver - unimplemented system calls"
msgstr ""
"afs_syscall, break, fattach, fdetach, ftime, getmsg, getpmsg, gtty, "
"isastream, lock, madvise1, mpx, prof, profil, putmsg, putpmsg, security, "
"stty, tuxcall, ulimit, vserver - chiamate di sistema non implementate"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINTASSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Unimplemented system calls.\n"
msgstr "Chiamate di sistema non implementate.\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIZIONE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These system calls are not implemented in the Linux kernel."
msgstr "Queste chiamate di sistema non sono implementate nel kernel Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALORE RESTITUITO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These system calls always return -1 and set I<errno> to B<ENOSYS>."
msgstr ""
"Queste chiamate di sistema restituiscono sempre -1 e impostano I<errno> a "
"B<ENOSYS>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that B<ftime>(3), B<profil>(3), and B<ulimit>(3)  are implemented as "
"library functions."
msgstr ""
"Notare che B<ftime>(3), B<profil>(3) e B<ulimit>(3) sono implementate come "
"funzioni di libreria."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Some system calls, like B<alloc_hugepages>(2), B<free_hugepages>(2), "
"B<ioperm>(2), B<iopl>(2), and B<vm86>(2)  exist only on certain "
"architectures."
msgstr ""
"Alcune chiamate di sistema, come B<alloc_hugepages>(2), "
"B<free_hugepages>(2), B<ioperm>(2), B<iopl>(2) e B<vm86>(2) esistono solo su "
"alcune architetture."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Some system calls, like B<ipc>(2), B<create_module>(2), B<init_module>(2), "
"and B<delete_module>(2)  exist only when the Linux kernel was built with "
"support for them."
msgstr ""
"Alcune chiamate di sistema, come B<ipc>(2), B<create_module>(2), "
"B<init_module>(2) e B<delete_module>(2) esistono solo se il kernel Linux è "
"stato compilato con il supporto ad esse."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDERE ANCHE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<syscalls>(2)"
msgstr "B<syscalls>(2)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 settembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuale del programmatore di Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Unimplemented system calls."
msgstr "Chiamate di sistema non implementate."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Questa pagina fa parte del rilascio 4.16 del progetto Linux I<man-pages>. "
"Una descrizione del progetto, le istruzioni per la segnalazione degli "
"errori, e l'ultima versione di questa pagina si trovano su \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
