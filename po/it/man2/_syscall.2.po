# Italian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Elisabetta Galli <lab@kkk.it>, 2007.
# Marco Curreli <marcocurreli@tiscali.it>, 2013, 2018, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2023-08-27 17:25+0200\n"
"PO-Revision-Date: 2021-04-07 01:05+0200\n"
"Last-Translator: Marco Curreli <marcocurreli@tiscali.it>\n"
"Language-Team: Italian <pluto-ildp@lists.pluto.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "A _syscall macro"
msgid "_syscall"
msgstr "Una macro _syscall"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-05-03"
msgstr "3 maggio 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "_syscall - invoking a system call without library support (OBSOLETE)"
msgstr ""
"_syscall - invoca una chiamata di sistema senza supporto di libreria "
"(OBSOLETO)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINTASSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>linux/unistd.hE<gt>>\n"
msgstr "B<#include E<lt>linux/unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "A _syscall macro\n"
msgstr "Una macro _syscall\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "desired system call\n"
msgstr "la chiamata di sistema desiderata\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIZIONE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The important thing to know about a system call is its prototype.  You need "
"to know how many arguments, their types, and the function return type.  "
"There are seven macros that make the actual call into the system easier.  "
"They have the form:"
msgstr ""
"La cosa importante da sapere sulle chiamate di sistema è il loro prototipo. "
"Bisogna conoscere il numero di argomenti, il loro tipo, e il tipo di ritorno "
"della funzione. Ci sono sette macro che rendono più facile la reale chiamata "
"nel sistema. Hanno il formato:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "_syscallI<X>(I<type>,I<name>,I<type1>,I<arg1>,I<type2>,I<arg2>,...)\n"
msgstr "_syscallI<X>(I<tipo>,I<nome>,I<tipo1>,I<arg1>,I<tipo2>,I<arg2>,...)\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "where"
msgstr "dove"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<X> is 0\\(en6, which are the number of arguments taken by the system "
#| "call"
msgid ""
"I<X> is 0\\[en]6, which are the number of arguments taken by the system call"
msgstr ""
"I<X> è tra 0 e 6, che sono il numero di argomenti presi dalla chiamata di "
"sistema"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<type> is the return type of the system call"
msgstr "I<tipo> è il tipo di ritorno della chiamata di sistema"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<name> is the name of the system call"
msgstr "I<nome> è il nome della chiamata di sistema"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<typeN> is the Nth argument's type"
msgstr "I<tipoN> è il tipo dell'N-esimo argomento"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<argN> is the name of the Nth argument"
msgstr "I<argN> è il nome dell'N-esimo argomento"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These macros create a function called I<name> with the arguments you "
"specify.  Once you include the _syscall() in your source file, you call the "
"system call by I<name>."
msgstr ""
"Queste macro creano una funzione chiamata I<nome> con gli argomenti "
"specificati. Una volta inclusa la _syscall() nel proprio file sorgente, si "
"può chiamare la chiamata di sistema con I<nome>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FILE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</usr/include/linux/unistd.h>"
msgstr "I</usr/include/linux/unistd.h>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "STORIA"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Starting around kernel 2.6.18, the _syscall macros were removed from "
#| "header files supplied to user space.  Use B<syscall>(2)  instead.  (Some "
#| "architectures, notably ia64, never provided the _syscall macros; on those "
#| "architectures, B<syscall>(2)  was always required.)"
msgid ""
"Starting around Linux 2.6.18, the _syscall macros were removed from header "
"files supplied to user space.  Use B<syscall>(2)  instead.  (Some "
"architectures, notably ia64, never provided the _syscall macros; on those "
"architectures, B<syscall>(2)  was always required.)"
msgstr ""
"A partire dal kernel 2.6.18 le macro di _syscall sono state rimosse dai file "
"di intestazione messi a disposizione nello spazio utente. Si usi "
"B<syscall>(2) al loro posto. (Alcune architetture, in particolare la ia64, "
"non ha mai messo a disposizione le macro _syscall; su queste architetture è "
"sempre stato richiesto B<syscall>(2))."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The _syscall() macros I<do not> produce a prototype.  You may have to create "
"one, especially for C++ users."
msgstr ""
"Le macro _syscall() I<non> producono un prototipo. Occorrerà quindi crearne "
"uno, specialmente per chi usa C++."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"System calls are not required to return only positive or negative error "
"codes.  You need to read the source to be sure how it will return errors.  "
"Usually, it is the negative of a standard error code, for example, -"
"I<EPERM>.  The _syscall() macros will return the result I<r> of the system "
"call when I<r> is nonnegative, but will return -1 and set the variable "
"I<errno> to -I<r> when I<r> is negative.  For the error codes, see "
"B<errno>(3)."
msgstr ""
"Le chiamate di sistema non devono per forza restituire solo codici di errore "
"positivi o negativi. Occorrerà leggere il sorgente per essere sicuri di come "
"ognuna di esse riporta gli errori. Solitamente, il valore di ritorno è il "
"negato di un codice di errore standard, per esempio -I<EPERM>. Le macro "
"_syscall() restituiranno il risultato I<r> della chiamata di sistema quando "
"I<r> è non-negativo, ma restituiranno -1 e imposteranno la variabile "
"I<errno> a I<r> quando I<r> è negativo. Per i codici di errore si veda "
"B<errno>(3)."

#.  The preferred way to invoke system calls that glibc does not know
#.  about yet is via
#.  .BR syscall (2).
#.  However, this mechanism can be used only if using a libc
#.  (such as glibc) that supports
#.  .BR syscall (2),
#.  and if the
#.  .I <sys/syscall.h>
#.  header file contains the required SYS_foo definition.
#.  Otherwise, the use of a _syscall macro is required.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When defining a system call, the argument types I<must> be passed by-value "
"or by-pointer (for aggregates like structs)."
msgstr ""
"Quando si definisce una chiamata di sistema, i tipi di argomento I<devono> "
"essere passati per valore o per puntatore (per gli aggregati come le "
"strutture)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ESEMPI"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, fuzzy, no-wrap
#| msgid ""
#| "    error = sysinfo(&s_info);\n"
#| "    printf(\"code error = %d\\en\", error);\n"
#| "    printf(\"Uptime = %lds\\enLoad: 1 min %lu / 5 min %lu / 15 min %lu\\en\"\n"
#| "           \"RAM: total %lu / free %lu / shared %lu\\en\"\n"
#| "           \"Memory in buffers = %lu\\enSwap: total %lu / free %lu\\en\"\n"
#| "           \"Number of processes = %d\\en\",\n"
#| "           s_info.uptime, s_info.loads[0],\n"
#| "           s_info.loads[1], s_info.loads[2],\n"
#| "           s_info.totalram, s_info.freeram,\n"
#| "           s_info.sharedram, s_info.bufferram,\n"
#| "           s_info.totalswap, s_info.freeswap,\n"
#| "           s_info.procs);\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>linux/unistd.hE<gt>       /* for _syscallX macros/related stuff */\n"
"#include E<lt>linux/kernel.hE<gt>       /* for struct sysinfo */\n"
"\\&\n"
"_syscall1(int, sysinfo, struct sysinfo *, info);\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct sysinfo s_info;\n"
"    int error;\n"
"\\&\n"
"    error = sysinfo(&s_info);\n"
"    printf(\"code error = %d\\en\", error);\n"
"    printf(\"Uptime = %lds\\enLoad: 1 min %lu / 5 min %lu / 15 min %lu\\en\"\n"
"           \"RAM: total %lu / free %lu / shared %lu\\en\"\n"
"           \"Memory in buffers = %lu\\enSwap: total %lu / free %lu\\en\"\n"
"           \"Number of processes = %d\\en\",\n"
"           s_info.uptime, s_info.loads[0],\n"
"           s_info.loads[1], s_info.loads[2],\n"
"           s_info.totalram, s_info.freeram,\n"
"           s_info.sharedram, s_info.bufferram,\n"
"           s_info.totalswap, s_info.freeswap,\n"
"           s_info.procs);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    error = sysinfo(&s_info);\n"
"    printf(\"code error = %d\\en\", error);\n"
"    printf(\"Uptime = %lds\\enLoad: 1 min %lu / 5 min %lu / 15 min %lu\\en\"\n"
"           \"RAM: total %lu / free %lu / shared %lu\\en\"\n"
"           \"Memory in buffers = %lu\\enSwap: total %lu / free %lu\\en\"\n"
"           \"Number of processes = %d\\en\",\n"
"           s_info.uptime, s_info.loads[0],\n"
"           s_info.loads[1], s_info.loads[2],\n"
"           s_info.totalram, s_info.freeram,\n"
"           s_info.sharedram, s_info.bufferram,\n"
"           s_info.totalswap, s_info.freeswap,\n"
"           s_info.procs);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. #-#-#-#-#  archlinux: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#. #-#-#-#-#  debian-bookworm: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#. #-#-#-#-#  debian-unstable: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#. #-#-#-#-#  fedora-39: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#. #-#-#-#-#  fedora-rawhide: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#. #-#-#-#-#  mageia-cauldron: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#. #-#-#-#-#  opensuse-leap-15-6: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SS
#. #-#-#-#-#  opensuse-tumbleweed: _syscall.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Sample output"
msgstr "Esempio di Output"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"code error = 0\n"
"uptime = 502034s\n"
"Load: 1 min 13376 / 5 min 5504 / 15 min 1152\n"
"RAM: total 15343616 / free 827392 / shared 8237056\n"
"Memory in buffers = 5066752\n"
"Swap: total 27881472 / free 24698880\n"
"Number of processes = 40\n"
msgstr ""
"code error = 0\n"
"uptime = 502034s\n"
"Load: 1 min 13376 / 5 min 5504 / 15 min 1152\n"
"RAM: total 15343616 / free 827392 / shared 8237056\n"
"Memory in buffers = 5066752\n"
"Swap: total 27881472 / free 24698880\n"
"Number of processes = 40\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDERE ANCHE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<intro>(2), B<syscall>(2), B<errno>(3)"
msgstr "B<intro>(2), B<syscall>(2), B<errno>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 febbraio 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "The use of these macros is Linux-specific, and deprecated."
msgstr "L'uso di queste macro è specifico di Linux, ed è deprecato."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"Starting around kernel 2.6.18, the _syscall macros were removed from header "
"files supplied to user space.  Use B<syscall>(2)  instead.  (Some "
"architectures, notably ia64, never provided the _syscall macros; on those "
"architectures, B<syscall>(2)  was always required.)"
msgstr ""
"A partire dal kernel 2.6.18 le macro di _syscall sono state rimosse dai file "
"di intestazione messi a disposizione nello spazio utente. Si usi "
"B<syscall>(2) al loro posto. (Alcune architetture, in particolare la ia64, "
"non ha mai messo a disposizione le macro _syscall; su queste architetture è "
"sempre stato richiesto B<syscall>(2))."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>linux/unistd.hE<gt>       /* for _syscallX macros/related stuff */\n"
"#include E<lt>linux/kernel.hE<gt>       /* for struct sysinfo */\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>linux/unistd.hE<gt>       /* per cose relative alle macro _syscallX */\n"
"#include E<lt>linux/kernel.hE<gt>       /* per sysinfo struct */\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "_syscall1(int, sysinfo, struct sysinfo *, info);\n"
msgstr "_syscall1(int, sysinfo, struct sysinfo *, info);\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct sysinfo s_info;\n"
"    int error;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    struct sysinfo s_info;\n"
"    int error;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    error = sysinfo(&s_info);\n"
"    printf(\"code error = %d\\en\", error);\n"
"    printf(\"Uptime = %lds\\enLoad: 1 min %lu / 5 min %lu / 15 min %lu\\en\"\n"
"           \"RAM: total %lu / free %lu / shared %lu\\en\"\n"
"           \"Memory in buffers = %lu\\enSwap: total %lu / free %lu\\en\"\n"
"           \"Number of processes = %d\\en\",\n"
"           s_info.uptime, s_info.loads[0],\n"
"           s_info.loads[1], s_info.loads[2],\n"
"           s_info.totalram, s_info.freeram,\n"
"           s_info.sharedram, s_info.bufferram,\n"
"           s_info.totalswap, s_info.freeswap,\n"
"           s_info.procs);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    error = sysinfo(&s_info);\n"
"    printf(\"code error = %d\\en\", error);\n"
"    printf(\"Uptime = %lds\\enLoad: 1 min %lu / 5 min %lu / 15 min %lu\\en\"\n"
"           \"RAM: total %lu / free %lu / shared %lu\\en\"\n"
"           \"Memory in buffers = %lu\\enSwap: total %lu / free %lu\\en\"\n"
"           \"Number of processes = %d\\en\",\n"
"           s_info.uptime, s_info.loads[0],\n"
"           s_info.loads[1], s_info.loads[2],\n"
"           s_info.totalram, s_info.freeram,\n"
"           s_info.sharedram, s_info.bufferram,\n"
"           s_info.totalswap, s_info.freeswap,\n"
"           s_info.procs);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "_SYSCALL"
msgstr "_SYSCALL"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 settembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuale del programmatore di Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>linux/unistd.hE<gt>>"
msgstr "B<#include E<lt>linux/unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "A _syscall macro"
msgstr "Una macro _syscall"

#. type: Plain text
#: opensuse-leap-15-6
msgid "desired system call"
msgstr "la chiamata di sistema desiderata"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I<X> is 0\\(en6, which are the number of arguments taken by the system call"
msgstr ""
"I<X> è tra 0 e 6, che sono il numero di argomenti presi dalla chiamata di "
"sistema"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "ESEMPIO"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Questa pagina fa parte del rilascio 4.16 del progetto Linux I<man-pages>. "
"Una descrizione del progetto, le istruzioni per la segnalazione degli "
"errori, e l'ultima versione di questa pagina si trovano su \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 marzo 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
