# Italian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Marco Curreli <marcocurreli@tiscali.it>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2023-08-27 17:33+0200\n"
"PO-Revision-Date: 2021-03-25 21:57+0100\n"
"Last-Translator: Marco Curreli <marcocurreli@tiscali.it>\n"
"Language-Team: Italian <pluto-ildp@lists.pluto.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "XINIT"
msgstr "XINIT"

#. type: TH
#: archlinux mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "xinit 1.4.1"
msgid "xinit 1.4.2"
msgstr "xinit 1.4.1"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "X Version 11"
msgstr "X Version 11"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "xinit - X Window System initializer"
msgstr "xinit - Inizializzatore del X Window System"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINTASSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<xinit> [ [ I<client> ] I<options> \\&.\\|.\\|. ] [ B<-\\^-> [ I<server> ] "
"[ I<display> ] I<options> \\&.\\|.\\|. ]"
msgstr ""
"B<xinit> [ [ I<client> ] I<opzioni> \\&.\\|.\\|. ] [ B<-\\^-> [ I<server> ] "
"[ I<display> ] I<opzioni> \\&.\\|.\\|. ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIZIONE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<xinit> program is used to start the X Window System server and a first "
"client program on systems that are not using a display manager such as "
"B<xdm>(1)  or in environments that use multiple window systems.  When this "
"first client exits, B<xinit> will kill the X server and then terminate."
msgstr ""
"Il programma B<xinit> è utilizzato per avviare il server X Window System e "
"un primo programma client su sistemi che non usano un display manager come "
"B<xdm>(1)  o in ambienti che usano molti sistemi window. Quando esiste "
"questo primo client, B<xinit> terminerà il server X e poi terminerà."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If no specific client program is given on the command line, B<xinit> will "
"look for a file in the user's home directory called I<.xinitrc> to run as a "
"shell script to start up client programs.  If no such file exists, B<xinit> "
"will use the following as a default:"
msgstr ""
"Se non si passa alla riga di comando nessun programma specifico, B<xinit> "
"cercherà un file nella directory home dell'utente chiamato I<.xinitrc> per "
"eseguirlo come uno script di shell per avviare programmi client. Se tale "
"file non esiste, B<xinit> userà il seguente come default:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "  xterm -geometry +1+1 -n login -display :0"
msgstr "  xterm -geometry +1+1 -n login -display :0"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If no specific server program is given on the command line, B<xinit> will "
"look for a file in the user's home directory called I<.xserverrc> to run as "
"a shell script to start up the server.  If no such file exists, I<xinit> "
"will use the following as a default:"
msgstr ""
"Se non si fornisce sulla riga di comando nessun programma server, B<xinit> "
"cercherà un file nella directory home dell'utente chiamato I<.xserverrc> per "
"eseguirlo come uno script di shell per avviare il server. Se tale file non "
"esiste, I<xinit> userà il seguente come default:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "  X :0"
msgstr "  X :0"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that this assumes that there is a program named I<X> in the current "
"search path.  The site administrator should, therefore, make a link to the "
"appropriate type of server on the machine, or create a shell script that "
"runs B<xinit> with the appropriate server."
msgstr ""
"Notare che si presuppone che ci sia un programma chiamato I<X> nell'attuale "
"percorso di ricerca. L'amministratore del sito dovrebbe, perciò, fare un "
"link all'appropriato tipo di server sulla macchina, o creare uno script di "
"shell che esegua B<xinit> con il server appropriato."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note, when using a I<.xserverrc> script be sure to ``exec'' the real X "
"server.  Failing to do this can make the X server slow to start and exit.  "
"For example:"
msgstr ""
"Notare, quando si usa uno script I<.xserverrc> assicurarsi di ``eseguire'' "
"il vero server X. Sbagliando nel far questo si può rendere X lento "
"nell'avvio e farlo chiudere subito. Per esempio:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "  exec Xdisplaytype"
msgstr "  exec Xdisplaytype"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An important point is that programs which are run by I<.xinitrc> should be "
"run in the background if they do not exit right away, so that they don't "
"prevent other programs from starting up.  However, the last long-lived "
"program started (usually a window manager or terminal emulator) should be "
"left in the foreground so that the script won't exit (which indicates that "
"the user is done and that B<xinit> should exit)."
msgstr ""
"Un punto importante è che i programmi che sono eseguiti da I<.xinitrc> "
"dovrebbero essere eseguiti in background per non farli terminare subito, "
"così che essi non prevengano l'apertura di altri programi. Comunque, "
"l'ultimo programma a lunga-vita avviato (usualmente un window manager o un "
"emulatore di terminale) dovrebbe essere lasciato in foreground così che lo "
"script non termini (ciò indicherebbe che l'utente ha finito e che B<xinit> "
"dovrebbe terminare)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An alternate client and/or server may be specified on the command line.  The "
"desired client program and its arguments should be given as the first "
"command line arguments to B<xinit>.  To specify a particular server command "
"line, append a double dash (-\\^-) to the B<xinit> command line (after any "
"client and arguments) followed by the desired server command."
msgstr ""
"Un client e/o un server alternativo può essere specificato sulla riga di "
"comando. Il programma client desiderato e i suoi argomenti dovrebbero essere "
"passati come argomenti a B<xinit> sulla prima riga di comando. Per "
"specificare un particolare servere dalla riga di comando, aggiungere un "
"doppio trattino (-\\^-) alla linea di comando di B<xinit> (dopo eventuali "
"client e argomenti) seguiti dal comando del server desiderato."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Both the client program name and the server program name must begin with a "
"slash (/) or a period (.).  Otherwise, they are treated as an arguments to "
"be appended to their respective startup lines.  This makes it possible to "
"add arguments (for example, foreground and background colors) without having "
"to retype the whole command line."
msgstr ""
"Entrambi i nomi dei programmi client e server devono iniziare con una barra "
"(/) o un punto (.). Diversamente, sono trattati come argomenti da essere "
"aggiunti alle rispettive righe di avvio. Questo rende possibile aggiungere "
"argomenti (per esempio, colori foreground e background) senza dover "
"ridigitare l'intera riga di comando."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If an explicit server name is not given and the first argument following the "
"double dash (-\\^-) is a colon followed by a digit, B<xinit> will use that "
"number as the display number instead of zero.  All remaining arguments are "
"appended to the server command line."
msgstr ""
"Se un nome server esplicito non viene dato e il primo argomento dopo le "
"doppie lineette (-\\^-) sono i duepunti seguiti da un numero, B<xinit> "
"utilizzerà quel numero come numero di display al posto di zero. Tutti i "
"rimanenti argomenti sono aggiunti alla line di comando del server."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ESEMPI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Below are several examples of how command line arguments in B<xinit> are "
"used."
msgstr ""
"Di seguito alcuni esempi di come sono usati gli argomenti della riga di "
"comando in I<xinit>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<xinit>"
msgstr "B<xinit>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This will start up a server named I<X> and run the user's I<.xinitrc>, if it "
"exists, or else start an I<xterm>."
msgstr ""
"Questo avvierà un server chiamato I<X> e eseguirà il I<.xinitrc> "
"dell'utente, se esiste, o altrimenti avvierà un I<xterm>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<xinit -\\^- /usr/bin/Xvnc  :1>"
msgstr "B<xinit -\\^- /usr/bin/Xvnc  :1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is how one could start a specific type of server on an alternate "
"display."
msgstr ""
"Questo è come si possa avviare uno specifico tipo di server su un display "
"alternativo."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<xinit -geometry =80x65+10+10 -fn 8x13 -j -fg white -bg navy>"
msgstr "B<xinit -geometry =80x65+10+10 -fn 8x13 -j -fg white -bg navy>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This will start up a server named I<X>, and will append the given arguments "
"to the default I<xterm> command.  It will ignore I<.xinitrc>."
msgstr ""
"Questo avvierà un server chiamato I<X>, e aggiungerà gli argomenti indicati "
"al comando I<xterm>. Ignorerà I<.xinitrc>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<xinit -e widgets -\\^- ./Xorg -l -c>"
msgstr "B<xinit -e widgets -\\^- ./Xorg -l -c>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This will use the command I<./Xorg -l -c> to start the server and will "
"append the arguments I<-e widgets> to the default I<xterm> command."
msgstr ""
"Questo userà il comando I<./Xorg -l -c> per avviare il server e aggiungerà "
"gli argomenti I<-e widgets> al comando di default I<xterm>."

#. type: TP
#: archlinux mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<xinit /usr/ucb/rsh fasthost cpupig -display ws:1 -\\^-  :1 -a 2 -t 5>"
msgid "B<xinit /usr/bin/ssh -X fasthost cpupig -\\^-  :1 -a 2 -t 5>"
msgstr "B<xinit /usr/ucb/rsh fasthost cpupig -display ws:1 -\\^-  :1 -a 2 -t 5>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This will start a server named I<X> on display 1 with the arguments I<-a 2 -"
"t 5>.  It will then start a remote shell on the machine B<fasthost> in which "
"it will run the command I<cpupig>, telling it to display back on the local "
"workstation."
msgstr ""
"Questo avvierà un server chiamato I<X> sul display 1 con gli argomenti I<-a "
"2 -t 5>. Esso avvierà poi una shell remota sulla macchina B<fasthost> sulla "
"quale eseguirà il comando I<cpupig>, che dice di visualizzare sulla "
"workstation locale."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Below is a sample I<.xinitrc> that starts a clock, several terminals, and "
"leaves the window manager running as the ``last'' application.  Assuming "
"that the window manager has been configured properly, the user then chooses "
"the ``Exit'' menu item to shut down X."
msgstr ""
"Di seguito è un esempio di I<.xinitrc> che avvia un orologio, divesi "
"terminali, e lascia in esecuzione un window manager per ``ultima'' "
"applicazione. Presupposto che il window manager sia stato configurato "
"propriamente, l'utente poi sceglierà la voce ``Exit'' del menu  per chiudere "
"X."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"xrdb -load $HOME/.Xresources\n"
"xsetroot -solid gray &\n"
"xclock -g 50x50-0+0 -bw 0 &\n"
"xload -g 50x50-50+0 -bw 0 &\n"
"xterm -g 80x24+0+0 &\n"
"xterm -g 80x24+0-0 &\n"
"twm\n"
msgstr ""
"xrdb -load $HOME/.Xresources\n"
"xsetroot -solid gray &\n"
"xclock -g 50x50-0+0 -bw 0 &\n"
"xload -g 50x50-50+0 -bw 0 &\n"
"xterm -g 80x24+0+0 &\n"
"xterm -g 80x24+0-0 &\n"
"twm\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Sites that want to create a common startup environment could simply create a "
"default I<.xinitrc> that references a site-wide startup file:"
msgstr ""
"In siti dove si vuole avere un ambiente di avvio comune potrebbero "
"semplicemente creare un default I<.xinitrc> che faccia riferimento un file "
"di avvio d'ambiente-allargato:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid ""
"\\&#!/bin/sh\n"
"\\&. /etc/X11/xinit/site.xinitrc\n"
msgstr ""
"\\&#!/bin/sh\n"
"\\&. /etc/X11/xinit/site.xinitrc\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Another approach is to write a script that starts B<xinit> with a specific "
"shell script.  Such scripts are usually named I<x11>, I<xstart>, or "
"I<startx> and are a convenient way to provide a simple interface for novice "
"users:"
msgstr ""
"Un'altro approccio è di scrivere uno script che avvii I<xinit> con uno "
"specifico script di shell. Tali script sono usualmente denominati I<x11>, "
"I<xstart>, o I<startx> e sono un modo conveniente per provvedere una "
"semplice interfaccia per gli utenti novizi:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid ""
"\\&#!/bin/sh\n"
"xinit /etc/X11/xinit/site.xinitrc -\\^- /usr/bin/X -br\n"
msgstr ""
"\\&#!/bin/sh\n"
"xinit /etc/X11/xinit/site.xinitrc -\\^- /usr/bin/X -br\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT VARIABLES"
msgstr "VARIABILI D'AMBIENTE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<DISPLAY>"
msgstr "B<DISPLAY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This variable gets set to the name of the display to which clients should "
"connect."
msgstr ""
"Questa variabile imposta il nome del display al quale i client dovrebbero "
"connettersi."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<XINITRC>"
msgstr "B<XINITRC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This variable specifies an init file containing shell commands to start up "
"the initial windows.  By default, I<.xinitrc> in the home directory will be "
"used."
msgstr ""
"Questa variabile specifica un file init contenente comandi di shell per "
"avviare le finestre iniziali. Di default, sarà utilizzato I<.xinitrc> nella "
"directory home."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FILE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<.xinitrc>"
msgstr "I<.xinitrc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "default client script"
msgstr "script client di default"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<xterm>"
msgstr "I<xterm>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "client to run if I<.xinitrc> does not exist"
msgstr "client da eseguire se I<.xinitrc> non esiste"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<.xserverrc>"
msgstr "I<.xserverrc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "default server script"
msgstr "script server di default"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<X>"
msgstr "I<X>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "server to run if I<.xserverrc> does not exist"
msgstr "server da eseguire se I<.xserverrc> non esiste"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDERE ANCHE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<X>(7), B<startx>(1), B<Xserver>(1), B<Xorg>(1), B<xorg.conf>(5), "
"B<xterm>(1)"
msgstr ""
"B<X>(7), B<startx>(1), B<Xserver>(1), B<Xorg>(1), B<xorg.conf>(5), "
"B<xterm>(1)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTORE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Bob Scheifler, MIT Laboratory for Computer Science"
msgstr "Bob Scheifler, MIT Laboratory for Computer Science"

#. type: TH
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#, no-wrap
msgid "xinit 1.4.0"
msgstr "xinit 1.4.0"

#. type: TP
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide opensuse-leap-15-6
#, no-wrap
msgid "B<xinit /usr/ucb/rsh fasthost cpupig -display ws:1 -\\^-  :1 -a 2 -t 5>"
msgstr "B<xinit /usr/ucb/rsh fasthost cpupig -display ws:1 -\\^-  :1 -a 2 -t 5>"

#. type: Plain text
#: mageia-cauldron
msgid "  xvt -geometry +1+1 -display :0"
msgstr "  xvt -geometry +1+1 -display :0"

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"\\&#!/bin/sh\n"
"\\&. /usr/libexec/xinit/site.xinitrc\n"
msgstr ""
"\\&#!/bin/sh\n"
"\\&. /usr/libexec/xinit/site.xinitrc\n"

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"\\&#!/bin/sh\n"
"xinit /usr/libexec/xinit/site.xinitrc -\\^- /usr/bin/X -br\n"
msgstr ""
"\\&#!/bin/sh\n"
"xinit /usr/libexec/xinit/site.xinitrc -\\^- /usr/bin/X -br\n"
