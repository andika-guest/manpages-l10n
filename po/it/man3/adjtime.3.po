# Italian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Elisabetta Galli <lab@kkk.it>, 2007-2008.
# Marco Curreli <marcocurreli@tiscali.it>, 2013-2016, 2018, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2023-08-27 16:49+0200\n"
"PO-Revision-Date: 2021-04-07 00:07+0200\n"
"Last-Translator: Marco Curreli <marcocurreli@tiscali.it>\n"
"Language-Team: Italian <pluto-ildp@lists.pluto.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<adjtime>()"
msgid "adjtime"
msgstr "B<adjtime>()"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 luglio 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "adjtime - correct the time to synchronize the system clock"
msgstr "adjtime - corregge l'orario per sincronizzare l'orologio di sistema"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINTASSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/time.hE<gt>>\n"
msgstr "B<#include E<lt>sys/time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int adjtime(const struct timeval *>I<delta>B<, struct timeval *>I<olddelta>B<);>\n"
msgstr "B<int adjtime(const struct timeval *>I<delta>B<, struct timeval *>I<olddelta>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Macro per test di funzionalità per glibc (vedere B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<adjtime>():"
msgstr "B<adjtime>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"    A partire da glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 e precedenti:\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIZIONE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<adjtime>()  function gradually adjusts the system clock (as returned "
"by B<gettimeofday>(2)).  The amount of time by which the clock is to be "
"adjusted is specified in the structure pointed to by I<delta>.  This "
"structure has the following form:"
msgstr ""
"La funzione B<adjtime>() regola in modo graduale l'orario di sistema "
"(restituito da B<gettimeofday>(2)). L'ammontare di tempo con cui regolare "
"l'orologio è specificato nella struttura a cui punta I<delta>. Questa "
"struttura ha la forma seguente:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct timeval {\n"
"    time_t      tv_sec;     /* seconds */\n"
"    suseconds_t tv_usec;    /* microseconds */\n"
"};\n"
msgstr ""
"struct timeval {\n"
"    time_t      tv_sec;     /* secondi */\n"
"    suseconds_t tv_usec;    /* microsecondi */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the adjustment in I<delta> is positive, then the system clock is speeded "
"up by some small percentage (i.e., by adding a small amount of time to the "
"clock value in each second) until the adjustment has been completed.  If the "
"adjustment in I<delta> is negative, then the clock is slowed down in a "
"similar fashion."
msgstr ""
"Se la regolazione di I<delta> è positiva, l'orologio di sistema viene "
"accelerato di qualche punto percentuale (per esempio aggiungendo una piccola "
"quantità di tempo al valore dell'orario per ogni secondo) finché la "
"regolazione non è stata completata. Se la regolazione di I<delta> è "
"negativa, l'orologio viene rallentato in maniera simile."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a clock adjustment from an earlier B<adjtime>()  call is already in "
"progress at the time of a later B<adjtime>()  call, and I<delta> is not NULL "
"for the later call, then the earlier adjustment is stopped, but any already "
"completed part of that adjustment is not undone."
msgstr ""
"Se si verifica una chiamata ad B<adjtime>() mentre è in corso una "
"regolazione dell'orario relativa ad una chiamata precedente di B<adjtime>(), "
"e I<delta> non è NULL per l'ultima chiamata, la chiamata precedente viene "
"terminata, senza però scartare alcuna correzione già completata."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<olddelta> is not NULL, then the buffer that it points to is used to "
"return the amount of time remaining from any previous adjustment that has "
"not yet been completed."
msgstr ""
"Se I<olddelta> non è NULL, allora il buffer a cui punta viene usato per "
"restituire l'ammontare di tempo rimanente dalle precedenti correzioni non "
"ancora completate."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALORE RESTITUITO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, B<adjtime>()  returns 0.  On failure, -1 is returned, and "
"I<errno> is set to indicate the error."
msgstr ""
"Se termina con successo, B<adjtime>() restituisce 0. Se fallisce, viene "
"restituito -1, e I<errno> viene valorizzato per indicare l'errore."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORI"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The adjustment in I<delta> is outside the permitted range."
msgstr "La correzione in I<delta> è fuori dall'intervallo permesso."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The caller does not have sufficient privilege to adjust the time.  Under "
"Linux, the B<CAP_SYS_TIME> capability is required."
msgstr ""
"Il chiamante non ha privilegi sufficienti per correggere l'orario. Sotto "
"Linux è richiesta l'abilitazione a B<CAP_SYS_TIME>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Per la spiegazione dei termini usati in questa sezione, vedere "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaccia"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valore"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<adjtime>()"
msgstr "B<adjtime>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "STORIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "4.3BSD, System V."
msgstr "4.3BSD, System V."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The adjustment that B<adjtime>()  makes to the clock is carried out in such "
"a manner that the clock is always monotonically increasing.  Using "
"B<adjtime>()  to adjust the time prevents the problems that can be caused "
"for certain applications (e.g., B<make>(1))  by abrupt positive or negative "
"jumps in the system time."
msgstr ""
"La correzione che B<adjtime>() effettua sull'orologio viene eseguita in modo "
"che quest'ultimo venga sempre incrementato uniformemente. Usando "
"B<adjtime>() per correggere l'orario si evitano problemi di funzionamento "
"per certe applicazioni (per esempio, B<make>(1)) dovuti a improvvise "
"variazioni positive o negative dell'orario di sistema."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<adjtime>()  is intended to be used to make small adjustments to the system "
"time.  Most systems impose a limit on the adjustment that can be specified "
"in I<delta>.  In the glibc implementation, I<delta> must be less than or "
"equal to (INT_MAX / 1000000 - 2)  and greater than or equal to (INT_MIN / "
"1000000 + 2)  (respectively 2145 and -2145 seconds on i386)."
msgstr ""
"B<adjtime>() è stato progettato per effettuare piccole correzioni all'orario "
"di sistema. Molti sistemi impongono un limite alla correzione, che può "
"essere specificato in I<delta>. Nell'implementazione di glibc, I<delta> "
"dev'essere minore o uguale a (INT_MAX / 1000000 - 2) e maggiore o uguale a "
"(INT_MIN / 1000000 + 2) (rispettivamente 2145 e -2145 secondi su i386)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUG"

#.  http://sourceware.org/bugzilla/show_bug?id=2449
#.  http://bugzilla.kernel.org/show_bug.cgi?id=6761
#.  Thanks to the new adjtimex() ADJ_OFFSET_SS_READ flag
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A longstanding bug meant that if I<delta> was specified as NULL, no valid "
"information about the outstanding clock adjustment was returned in "
"I<olddelta>.  (In this circumstance, B<adjtime>()  should return the "
"outstanding clock adjustment, without changing it.)  This bug is fixed on "
"systems with glibc 2.8 or later and Linux kernel 2.6.26 or later."
msgstr ""
"Un bug di vecchia data faceva in modo che, se I<delta> veniva valorizzato a "
"NULL, a I<olddelta> non veniva restituita alcuna informazione valida "
"riguardo la correzione in sospeso dell'orologio. (In questo caso, "
"B<adjtime>() dovrebbe restituire la correzione dell'orario in sospeso, senza "
"cambiarla.) Questo bug è stato risolto su sistemi con glibc versione 2.8 o "
"successiva, e con kernel Linux versione 2.6.26 o successivo."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDERE ANCHE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<adjtimex>(2), B<gettimeofday>(2), B<time>(7)"
msgstr "B<adjtimex>(2), B<gettimeofday>(2), B<time>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 febbraio 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "ADJTIME"
msgstr "ADJTIME"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 settembre 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuale del programmatore di Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<adjtime>():\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"B<adjtime>():\n"
"    A partire da glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 e precedenti:\n"
"        _BSD_SOURCE\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Questa pagina fa parte del rilascio 4.16 del progetto Linux I<man-pages>. "
"Una descrizione del progetto, le istruzioni per la segnalazione degli "
"errori, e l'ultima versione di questa pagina si trovano su \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 marzo 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
