# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.14.0\n"
"POT-Creation-Date: 2023-10-02 12:14+0200\n"
"PO-Revision-Date: 2022-06-02 17:31+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "PFBTOPFA"
msgstr "PFBTOPFA"

#. type: TH
#: archlinux debian-unstable mageia-cauldron
#, fuzzy, no-wrap
#| msgid "September 2023"
msgid "13 September 2023"
msgstr "september 2023"

#. type: TH
#: archlinux debian-unstable mageia-cauldron
#, no-wrap
msgid "10.02.0"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Ghostscript"
msgstr "Ghostscript"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"pfbtopfa - Convert Postscript .pfb fonts to .pfa format using ghostscript"
msgstr ""
"pfbtopfa - konverter Postscript .pfb-skrifttyper til .pfa-formatet via "
"ghostscript"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "B<pfbtopfa> I<input.pfb> I<[output.pfa]>"
msgstr "B<pfbtopfa> I<inddata.pfb> I<[uddata.pfa]>"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "This script invokes B<gs>(1)  to convert a .pfb file into a .pfa file."
msgstr ""
"Dette skript igangsætter B<gs>(1) for at konvertere en .pfb-fil til en .pfa-"
"fil."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "gs(1)"
msgstr "gs(1)"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "VERSION"
msgstr "VERSION"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
#, fuzzy
#| msgid "This document was last revised for Ghostscript version 9.56.1."
msgid "This document was last revised for Ghostscript version 10.02.0."
msgstr "Dette dokument blev sidst revideret for Ghostscript version 9.56.1."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "FORFATTER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Artifex Software, Inc. are the primary maintainers of Ghostscript.  This "
"manpage by George Ferguson."
msgstr ""
"Artifex Software, Inc. er de primære vedligeholdere af Ghostscript. Denne "
"manualside er skrevet af George Ferguson."

#. type: TH
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid "September 2021"
msgid "21 September 2022"
msgstr "september 2021"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10.00.0"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "This document was last revised for Ghostscript version 9.56.1."
msgid "This document was last revised for Ghostscript version 10.00.0."
msgstr "Dette dokument blev sidst revideret for Ghostscript version 9.56.1."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "4 April 2022"
msgstr "4. april 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "9.56.1"
msgstr "9.56.1"

#. type: Plain text
#: opensuse-tumbleweed
msgid "This document was last revised for Ghostscript version 9.56.1."
msgstr "Dette dokument blev sidst revideret for Ghostscript version 9.56.1."
