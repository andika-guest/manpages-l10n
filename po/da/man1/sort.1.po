# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.9.2\n"
"POT-Creation-Date: 2023-10-02 12:25+0200\n"
"PO-Revision-Date: 2022-05-01 10:45+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SORT"
msgstr "SORT"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "september 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brugerkommandoer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sort - sort lines of text files"
msgstr "sort - sorter linjer i tekstfiler"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sort> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<sort> [I<\\,FLAG\\/>]... [I<\\,FIL\\/>]..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sort> [I<\\,OPTION\\/>]... I<\\,--files0-from=F\\/>"
msgstr "B<sort> [I<\\,FLAG\\/>]... I<\\,--files0-from=F\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Write sorted concatenation of all FILE(s) to standard output."
msgstr "Skriv det samlede, sorterede indhold af FILer til standard-ud."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr "Hvis ingen FIL er angivet, eller FIL er -, læses fra standard-ind."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too.  "
"Ordering options:"
msgstr ""
"Obligatoriske argumenter til lange flag er også obligatoriske for de korte.  "
"Sorteringsflag:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--ignore-leading-blanks>"
msgstr "B<-b>, B<--ignore-leading-blanks>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ignore leading blanks"
msgstr "ignorér indledende blanke tegn"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--dictionary-order>"
msgstr "B<-d>, B<--dictionary-order>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "consider only blanks and alphanumeric characters"
msgstr "tag kun blanke og alfanumeriske tegn i betragtning"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--ignore-case>"
msgstr "B<-f>, B<--ignore-case>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "fold lower case to upper case characters"
msgstr "behandl små bogstaver som store bogstaver"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-g>, B<--general-numeric-sort>"
msgstr "B<-g>, B<--general-numeric-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "compare according to general numerical value"
msgstr "sammenlign ifølge generel numerisk værdi"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-nonprinting>"
msgstr "B<-i>, B<--ignore-nonprinting>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "consider only printable characters"
msgstr "tag kun synlige tegn i betragtning"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-M>, B<--month-sort>"
msgstr "B<-M>, B<--month-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "compare (unknown) E<lt> 'JAN' E<lt> ... E<lt> 'DEC'"
msgstr "sammenlign (ukendt) E<lt> 'JAN' E<lt> ... E<lt> 'DEC'"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--human-numeric-sort>"
msgstr "B<-h>, B<--human-numeric-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "compare human readable numbers (e.g., 2K 1G)"
msgstr "sammenlign tal på læsevenlig form (f.eks. 2K, 1G)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--numeric-sort>"
msgstr "B<-n>, B<--numeric-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "compare according to string numerical value"
msgstr "sammenlign ifølge numerisk værdi af strenge"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>, B<--random-sort>"
msgstr "B<-R>, B<--random-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "shuffle, but group identical keys.  See B<shuf>(1)"
msgstr "bland, men behold ens nøgler sammen.  Se B<shuf>(1)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--random-source>=I<\\,FILE\\/>"
msgstr "B<--random-source>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "get random bytes from FILE"
msgstr "læs tilfældige byte fra FIL"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--reverse>"
msgstr "B<-r>, B<--reverse>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "reverse the result of comparisons"
msgstr "vend resultaterne af sammenligningerne om"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--sort>=I<\\,WORD\\/>"
msgstr "B<--sort>=I<\\,ORD\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"sort according to WORD: general-numeric B<-g>, human-numeric B<-h>, month B<-"
"M>, numeric B<-n>, random B<-R>, version B<-V>"
msgstr ""
"sortér ifølge ORD: general-numeric B<-g>, human-numeric B<-h>, month B<-M>, "
"numeric B<-n>, random B<-R>, version B<-V>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version-sort>"
msgstr "B<-V>, B<--version-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "natural sort of (version) numbers within text"
msgstr "naturlig sortering af (versions-)tal i tekst"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Other options:"
msgstr "Andre flag:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--batch-size>=I<\\,NMERGE\\/>"
msgstr "B<--batch-size>=I<\\,NFLET\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "merge at most NMERGE inputs at once; for more use temp files"
msgstr ""
"flet højst NFLET ind-elementer på en gang; brug midlertidige filer hvis "
"utilstrækkeligt"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--check>, B<--check>=I<\\,diagnose-first\\/>"
msgstr "B<-c>, B<--check>, B<--check>=I<\\,diagnose-first\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "check for sorted input; do not sort"
msgstr "kontrollér sortering af data; sortér ikke"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-C>, B<--check>=I<\\,quiet\\/>, B<--check>=I<\\,silent\\/>"
msgstr "B<-C>, B<--check>=I<\\,quiet\\/>, B<--check>=I<\\,silent\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<-c>, but do not report first bad line"
msgstr "som B<-c>, men anfør ikke første fejllinje"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--compress-program>=I<\\,PROG\\/>"
msgstr "B<--compress-program>=I<\\,PROG\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "compress temporaries with PROG; decompress them with PROG B<-d>"
msgstr "komprimér midlertidige filer med PROG; udpak dem med PROG B<-d>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--debug>"
msgstr "B<--debug>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"annotate the part of the line used to sort, and warn about questionable "
"usage to stderr"
msgstr ""
"skriv noter i delen af linjen der bruges til at sortere, og advar om "
"tvivlsom brug i stderr"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--files0-from>=I<\\,F\\/>"
msgstr "B<--files0-from>=I<\\,F\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"read input from the files specified by NUL-terminated names in file F; If F "
"is - then read names from standard input"
msgstr ""
"læs inddata fra filerne angivet ved NUL-afsluttede navne i filen F; hvis F "
"er -, så læs filnavne fra standard-ind"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--key>=I<\\,KEYDEF\\/>"
msgstr "B<-k>, B<--key>=I<\\,NØGLEDEF\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sort via a key; KEYDEF gives location and type"
msgstr "sortér efter nøgle; NØGLEDEF angiver placering og type"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--merge>"
msgstr "B<-m>, B<--merge>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "merge already sorted files; do not sort"
msgstr "flet allerede sorterede filer; sortér ikke"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write result to FILE instead of standard output"
msgstr "skriv resultat til FIL frem for standard-ud"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--stable>"
msgstr "B<-s>, B<--stable>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "stabilize sort by disabling last-resort comparison"
msgstr "stabilisér sortering ved deaktivering af sidste-udvejs-sammenligning"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--buffer-size>=I<\\,SIZE\\/>"
msgstr "B<-S>, B<--buffer-size>=I<\\,STØR\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use SIZE for main memory buffer"
msgstr "brug størrelsen STØR for indre hukommelsesbuffer"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--field-separator>=I<\\,SEP\\/>"
msgstr "B<-t>, B<--field-separator>=I<\\,SEP\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use SEP instead of non-blank to blank transition"
msgstr "brug SEP i stedet for ikke-mellemrum til  mellemrums-overgang"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--temporary-directory>=I<\\,DIR\\/>"
msgstr "B<-T>, B<--temporary-directory>=I<\\,KAT\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"use DIR for temporaries, not $TMPDIR or I<\\,/tmp\\/>; multiple options "
"specify multiple directories"
msgstr ""
"brug KAT til mellemlagring, ikke $TMPDIR eller I<\\,/tmp\\/>; flere flag "
"angiver flere kataloger"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--parallel>=I<\\,N\\/>"
msgstr "B<--parallel>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "change the number of sorts run concurrently to N"
msgstr "begræns antallet af parallelt kørende sorteringer til N"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--unique>"
msgstr "B<-u>, B<--unique>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"with B<-c>, check for strict ordering; without B<-c>, output only the first "
"of an equal run"
msgstr ""
"med B<-c>: tjek for streng ordning; uden B<-c>: udskriv kun den første af en "
"række ens"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr "B<-z>, B<--zero-terminated>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "line delimiter is NUL, not newline"
msgstr "linjeadskiller er NUL frem for linjeskift"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "vis denne hjælpetekst og afslut"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "vis versionsinformation og afslut"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"KEYDEF is F[.C][OPTS][,F[.C][OPTS]] for start and stop position, where F is "
"a field number and C a character position in the field; both are origin 1, "
"and the stop position defaults to the line's end.  If neither B<-t> nor B<-"
"b> is in effect, characters in a field are counted from the beginning of the "
"preceding whitespace.  OPTS is one or more single-letter ordering options "
"[bdfgiMhnRrV], which override global ordering options for that key.  If no "
"key is given, use the entire line as the key.  Use B<--debug> to diagnose "
"incorrect key usage."
msgstr ""
"NØGLEDEF er F[.T][FLAG][,F[.T][FLAG]], for start- og slutposition, hvor F er "
"et feltnummer og T en tegnposition i feltet.  Begge starter ved 1, og "
"slutpositionen er som standard afslutningen af linjen.  Hvis hverken B<-t> "
"eller B<-b> er valgt, vil tegn i et felt tælles fra begyndelsen af det "
"foregående område af blanke tegn.  FLAG er sat sammen af et eller flere "
"enkeltbogstavs-flag [bdfgiMhnRrV], som tilsidesætter globale sorteringsflag "
"for denne nøgle.  Hvis ingen nøgle er angivet, bruges hele linjen som "
"nøgle.  Brug B<--debug> til at fejlsøge forkert brug af nøgle."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "SIZE may be followed by the following multiplicative suffixes: % 1% of "
#| "memory, b 1, K 1024 (default), and so on for M, G, T, P, E, Z, Y."
msgid ""
"SIZE may be followed by the following multiplicative suffixes: % 1% of "
"memory, b 1, K 1024 (default), and so on for M, G, T, P, E, Z, Y, R, Q."
msgstr ""
"STØR kan efterfølges af de følgende multiplikative endelser: % 1% af "
"hukommelse, b 1, k 1024 (forvalgt), og så videre for M, G, T, P, E, Z, Y."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"*** WARNING *** The locale specified by the environment affects sort order.  "
"Set LC_ALL=C to get the traditional sort order that uses native byte values."
msgstr ""
"*** ADVARSEL *** Regionsdata angivet i miljøet påvirker sorteringsordenen. "
"Sæt LC_ALL=C for at få den traditionelle sorteringsorden som benytter de "
"interne byte-værdier."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "FORFATTER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Mike Haertel and Paul Eggert."
msgstr "Skrevet af Mike Haertel og Paul Eggert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEJLRAPPORTER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Hjælp til GNU coreutils på nettet:: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapporter oversættelsesfejl til E<lt>https://translationproject.org/team/da."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "OPHAVSRET"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Ophavsret \\(co 2023 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller nyere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dette er et frit program: du kan frit ændre og videredistribuere det. Der "
"gives INGEN GARANTI, i den grad som loven tillader dette."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<shuf>(1), B<uniq>(1)"
msgstr "B<shuf>(1), B<uniq>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/sortE<gt>"
msgstr ""
"Fuld dokumentation E<lt>https://www.gnu.org/software/coreutils/sortE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) sort invocation\\(aq"
msgstr ""
"eller lokalt tilgængelig via: info \\(aq(coreutils) sort invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "september 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"SIZE may be followed by the following multiplicative suffixes: % 1% of "
"memory, b 1, K 1024 (default), and so on for M, G, T, P, E, Z, Y."
msgstr ""
"STØR kan efterfølges af de følgende multiplikative endelser: % 1% af "
"hukommelse, b 1, k 1024 (forvalgt), og så videre for M, G, T, P, E, Z, Y."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Ophavsret \\(co 2022 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller nyere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "April 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "oktober 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid "shuffle, but group identical keys.  See shuf(1)"
msgstr "bland, men behold ens nøgler sammen.  Se shuf(1)"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Ophavsret \\(co 2020 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller nyere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "shuf(1), uniq(1)"
msgstr "shuf(1), uniq(1)"
