# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-08-27 16:59+0200\n"
"PO-Revision-Date: 2023-07-17 08:13+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "getloadavg"
msgstr "getloadavg"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 iulie 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pagini de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getloadavg - get system load averages"
msgstr "getloadavg - obține mediile de încărcare ale sistemului"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int getloadavg(double >I<loadavg[]>B<, int >I<nelem>B<);>\n"
msgstr "B<int getloadavg(double >I<loadavg[]>B<, int >I<nelem>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Cerințe pentru macrocomenzi de testare a caracteristicilor pentru glibc "
"(consultați B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getloadavg>():"
msgstr "B<getloadavg>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    In glibc up to and including 2.19:\n"
"        _BSD_SOURCE\n"
msgstr ""
"    Începând cu glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    În glibc până la versiunea 2.19 inclusiv:\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<getloadavg>()  function returns the number of processes in the system "
"run queue averaged over various periods of time.  Up to I<nelem> samples are "
"retrieved and assigned to successive elements of I<loadavg[]>.  The system "
"imposes a maximum of 3 samples, representing averages over the last 1, 5, "
"and 15 minutes, respectively."
msgstr ""
"Funcția B<getloadavg>() returnează numărul de procese din coada de execuție "
"a sistemului, calculat ca medie pe diferite perioade de timp.  Până la "
"I<nelem> eșantioane sunt recuperate și atribuite elementelor succesive ale "
"I<loadavg[]>.  Sistemul impune un maxim de 3 eșantioane, reprezentând medii "
"pe ultimele 1, 5 și, respectiv, 15 minute."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. #-#-#-#-#  archlinux: getloadavg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: getloadavg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  The
#.  BR getloadavg ()
#.  function appeared in
#.  4.3BSD Reno .
#. type: Plain text
#. #-#-#-#-#  debian-unstable: getloadavg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  The
#.  BR getloadavg ()
#.  function appeared in
#.  4.3BSD Reno .
#. type: Plain text
#. #-#-#-#-#  fedora-39: getloadavg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: getloadavg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: getloadavg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  The
#.  BR getloadavg ()
#.  function appeared in
#.  4.3BSD Reno .
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: getloadavg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  The
#.  BR getloadavg ()
#.  function appeared in
#.  4.3BSD Reno .
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: getloadavg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the load average was unobtainable, -1 is returned; otherwise, the number "
"of samples actually retrieved is returned."
msgstr ""
"În cazul în care media de încărcare nu a putut fi obținută, se returnează "
"-1; în caz contrar, se returnează numărul de eșantioane recuperate efectiv."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getloadavg>()"
msgstr "B<getloadavg>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "BSD."
msgstr "BSD."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "4.3BSD-Reno, Solaris.  glibc 2.2."
msgstr "4.3BSD-Reno, Solaris.  glibc 2.2."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<uptime>(1), B<proc>(5)"
msgstr "B<uptime>(1), B<proc>(5)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15 decembrie 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid "This function is available since glibc 2.2."
msgstr "Această funcție este disponibilă începând cu glibc 2.2."

#.  mdoc seems to have a bug - there must be no newline here
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "Not in POSIX.1.  Present on the BSDs and Solaris."
msgstr "Nu există în POSIX.1.  Prezentă pe BSD, Solaris și multe alte sisteme."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pagini de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GETLOADAVG"
msgstr "GETLOADAVG"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2016-03-15"
msgstr "15 martie 2016"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manualul programatorului Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<getloadavg>()  function returns the number of processes in the system "
"run queue averaged over various periods of time.  Up to I<nelem> samples are "
"retrieved and assigned to successive elements of I<loadavg>[].  The system "
"imposes a maximum of 3 samples, representing averages over the last 1, 5, "
"and 15 minutes, respectively."
msgstr ""
"Funcția B<getloadavg>() returnează numărul de procese din coada de execuție "
"a sistemului, calculat ca medie pe diferite perioade de timp.  Până la "
"I<nelem> eșantioane sunt recuperate și atribuite la elementele succesive ale "
"I<loadavg>[].  Sistemul impune un maxim de 3 eșantioane, reprezentând medii "
"pe ultimele 1, 5 și, respectiv, 15 minute."

#. type: Plain text
#: opensuse-leap-15-6
msgid "This function is available in glibc since version 2.2."
msgstr "Această funcție este disponibilă în glibc începând cu versiunea 2.2."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "ÎN CONFORMITATE CU"

# R-GC, scrie:
# COLOPHON (în eng.) = COLOFON (în rom.)
# *****
# Colofon = Notă la sfârșitul unei publicații sau
# pe verso foii de titlu, cuprinzând datele editoriale.
# sau:
# Colofon =  Însemnare la sfârșitul unei cărți în
# epoca manuscriselor și incunabulelor, cuprinzând
# date privind tipograful, locul unde a lucrat,
# autorul și titlul lucrării.
# sau:
# Colofon = Notă, însemnare finală a unei cărți,
# care reproduce sau completează cele spuse în titlu.
# ===============
# M-am decis pentru:
# COLOFON -- NOTĂ FINALĂ
#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON -- NOTĂ FINALĂ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Această pagină face parte din versiunea 4.16 a proiectului Linux I<man-"
"pages>.  O descriere a proiectului, informații despre raportarea erorilor și "
"cea mai recentă versiune a acestei pagini pot fi găsite la \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 martie 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"
