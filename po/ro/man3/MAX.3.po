# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-08-27 17:07+0200\n"
"PO-Revision-Date: 2023-08-24 10:16+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "MAX"
msgstr "MAX"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-05-03"
msgstr "3 mai 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pagini de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "MAX, MIN - maximum or minimum of two values"
msgstr "MAX, MIN - maximul sau minimul a două valori"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/param.hE<gt>>\n"
msgstr "B<#include E<lt>sys/param.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<MAX(>I<a>B<, >I<b>B<);>\n"
"B<MIN(>I<a>B<, >I<b>B<);>\n"
msgstr ""
"B<MAX(>I<a>B<, >I<b>B<);>\n"
"B<MIN(>I<a>B<, >I<b>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "These macros return the maximum or minimum of I<a> and I<b>."
msgstr "Aceste macrocomenzi returnează maximul sau minimul lui I<a> și I<b>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"These macros return the value of one of their arguments, possibly converted "
"to a different type (see BUGS)."
msgstr ""
"Aceste macrocomenzi returnează valoarea unuia dintre argumentele lor, "
"eventual convertit într-un tip diferit (a se vedea ERORI)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"These macros may raise the \"invalid\" floating-point exception when any of "
"the arguments is NaN."
msgstr ""
"Aceste macrocomenzi pot declanșa excepția „invalid” (nevalid) în număr de "
"virgulă mobilă atunci când oricare dintre argumente este NaN („Not a "
"Number”, nu este un număr)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "GNU, BSD."
msgstr "GNU, BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If either of the arguments is of a floating-point type, you might prefer to "
"use B<fmax>(3)  or B<fmin>(3), which can handle NaN."
msgstr ""
"Dacă oricare dintre argumente este de tip virgulă mobilă, este posibil să "
"preferați să folosiți B<fmax>(3) sau B<fmin>(3), care pot gestiona NaN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The arguments may be evaluated more than once, or not at all."
msgstr ""
"Argumentele pot fi evaluate de mai multe ori sau pot să nu fie evaluate "
"deloc."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Some UNIX systems might provide these macros in a different header, or not "
"at all."
msgstr ""
"Unele sisteme UNIX pot furniza aceste macrocomenzi într-un antet diferit sau "
"deloc."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Due to the usual arithmetic conversions, the result of these macros may be "
"very different from either of the arguments.  To avoid this, ensure that "
"both arguments have the same type."
msgstr ""
"Din cauza conversiilor aritmetice obișnuite, rezultatul acestor macrocomenzi "
"poate fi foarte diferit de oricare dintre argumente.  Pentru a evita acest "
"lucru, asigurați-vă că ambele argumente au același tip."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/param.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int a, b, x;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>numE<gt> E<lt>numE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    a = atoi(argv[1]);\n"
"    b = atoi(argv[2]);\n"
"    x = MAX(a, b);\n"
"    printf(\"MAX(%d, %d) is %d\\en\", a, b, x);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/param.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int a, b, x;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Utilizare: %s E<lt>numE<gt> E<lt>numE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    a = atoi(argv[1]);\n"
"    b = atoi(argv[2]);\n"
"    x = MAX(a, b);\n"
"    printf(\"MAX(%d, %d) este %d\\en\", a, b, x);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<fmax>(3), B<fmin>(3)"
msgstr "B<fmax>(3), B<fmin>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "30 octombrie 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid "These nonstandard macros are present in glibc and the BSDs."
msgstr "Aceste macrocomenzi nestandardizate sunt prezente în glibc și în BSD."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/param.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/param.hE<gt>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int a, b, x;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int a, b, x;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>numE<gt> E<lt>numE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Utilizare: %s E<lt>numE<gt> E<lt>numE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    a = atoi(argv[1]);\n"
"    b = atoi(argv[2]);\n"
"    x = MAX(a, b);\n"
"    printf(\"MAX(%d, %d) is %d\\en\", a, b, x);\n"
msgstr ""
"    a = atoi(argv[1]);\n"
"    b = atoi(argv[2]);\n"
"    x = MAX(a, b);\n"
"    printf(\"MAX(%d, %d) este %d\\en\", a, b, x);\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pagini de manual de Linux 6.05"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 martie 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"
