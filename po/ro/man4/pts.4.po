# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-08-27 17:15+0200\n"
"PO-Revision-Date: 2023-05-17 10:46+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "pts"
msgstr "pts"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 octombrie 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pagini de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ptmx, pts - pseudoterminal master and slave"
msgstr "ptmx, pts - pseudoterminal maestru și sclav"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The file I</dev/ptmx> (the pseudoterminal multiplexor device)  is a "
"character file with major number 5 and minor number 2, usually with mode "
"0666 and ownership root:root.  It is used to create a pseudoterminal master "
"and slave pair."
msgstr ""
"Fișierul I</dev/ptmx> (dispozitivul multiplexor pseudoterminal) este un "
"fișier de caractere cu numărul major 5 și numărul minor 2, de obicei cu "
"modul 0666 și proprietar root:root.  Este utilizat pentru a crea o pereche "
"de pseudoterminale maestru și sclav."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"When a process opens I</dev/ptmx>, it gets a file descriptor for a "
"pseudoterminal master and a pseudoterminal slave device is created in the I</"
"dev/pts> directory.  Each file descriptor obtained by opening I</dev/ptmx> "
"is an independent pseudoterminal master with its own associated slave, whose "
"path can be found by passing the file descriptor to B<ptsname>(3)."
msgstr ""
"Atunci când un proces deschide I</dev/ptmx>, acesta primește un descriptor "
"de fișier pentru un pseudoterminal maestru și se creează un dispozitiv "
"pseudoterminal sclav în directorul I</dev/pts>.  Fiecare descriptor de "
"fișier obținut prin deschiderea I</dev/ptmx> este un pseudoterminal maestru "
"independent cu propriul său pseudoterminal sclav asociat, a cărui rută poate "
"fi găsită prin transmiterea descriptorului de fișier către B<ptsname>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Before opening the pseudoterminal slave, you must pass the master's file "
"descriptor to B<grantpt>(3)  and B<unlockpt>(3)."
msgstr ""
"Înainte de a deschide pseudoterminalul sclav, trebuie să transmiteți "
"descriptorul de fișier al maestrului către B<grantpt>(3) și B<unlockpt>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Once both the pseudoterminal master and slave are open, the slave provides "
"processes with an interface that is identical to that of a real terminal."
msgstr ""
"Odată ce atât pseudoterminalul maestru cât și cel sclav sunt deschise, "
"sclavul oferă proceselor o interfață identică cu cea a unui terminal real."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Data written to the slave is presented on the master file descriptor as "
"input.  Data written to the master is presented to the slave as input."
msgstr ""
"Datele scrise pe sclav sunt prezentate în descriptorul de fișier principal "
"ca intrare.  Datele scrise în maestru sunt prezentate sclavului ca intrare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In practice, pseudoterminals are used for implementing terminal emulators "
"such as B<xterm>(1), in which data read from the pseudoterminal master is "
"interpreted by the application in the same way a real terminal would "
"interpret the data, and for implementing remote-login programs such as "
"B<sshd>(8), in which data read from the pseudoterminal master is sent across "
"the network to a client program that is connected to a terminal or terminal "
"emulator."
msgstr ""
"În practică, pseudoterminalele sunt utilizate pentru implementarea "
"emulatorilor de terminale, cum ar fi B<xterm>(1), în care datele citite de "
"la pseudoterminalul maestru sunt interpretate de aplicație în același mod în "
"care un terminal real ar interpreta datele, și pentru implementarea "
"programelor de logare la distanță, cum ar fi B<sshd>(8), în care datele "
"citite de la pseudoterminalul maestru sunt trimise prin rețea către un "
"program client conectat la un terminal sau emulator de terminal."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Pseudoterminals can also be used to send input to programs that normally "
"refuse to read input from pipes (such as B<su>(1), and B<passwd>(1))."
msgstr ""
"Pseudoterminalele pot fi, de asemenea, utilizate pentru a trimite date de "
"intrare către programe care în mod normal refuză să citească date de intrare "
"de la conducte (cum ar fi B<su>(1) și B<passwd>(1))."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/ptmx>, I</dev/pts/*>"
msgstr "I</dev/ptmx>, I</dev/pts/*>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The Linux support for the above (known as UNIX 98 pseudoterminal naming)  is "
"done using the I<devpts> filesystem, which should be mounted on I</dev/pts>."
msgstr ""
"Suportul Linux pentru cele de mai sus (cunoscut sub numele de pseudoterminal "
"UNIX 98) se face folosind sistemul de fișiere I<devpts>, care trebuie montat "
"pe I</dev/pts>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getpt>(3), B<grantpt>(3), B<ptsname>(3), B<unlockpt>(3), B<pty>(7)"
msgstr "B<getpt>(3), B<grantpt>(3), B<ptsname>(3), B<unlockpt>(3), B<pty>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pagini de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "PTS"
msgstr "PTS"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2016-03-15"
msgstr "15 martie 2016"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manualul programatorului Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The file I</dev/ptmx> is a character file with major number 5 and minor "
"number 2, usually of mode 0666 and owner.group of root.root.  It is used to "
"create a pseudoterminal master and slave pair."
msgstr ""
"Fișierul I</dev/ptmx> este un fișier de caractere cu numărul major 5 și "
"numărul minor 2, de obicei cu modul 0666 și cu proprietar.grup de root."
"root.  Acesta este utilizat pentru a crea o pereche de pseudoterminale "
"maestru și sclav."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"When a process opens I</dev/ptmx>, it gets a file descriptor for a "
"pseudoterminal master (PTM), and a pseudoterminal slave (PTS) device is "
"created in the I</dev/pts> directory.  Each file descriptor obtained by "
"opening I</dev/ptmx> is an independent PTM with its own associated PTS, "
"whose path can be found by passing the file descriptor to B<ptsname>(3)."
msgstr ""
"Atunci când un proces deschide I</dev/ptmx>, acesta primește un descriptor "
"de fișier pentru un pseudoterminal maestru (PTM) și se creează un dispozitiv "
"pseudoterminal sclav (PTS) în directorul I</dev/pts>.  Fiecare descriptor de "
"fișier obținut prin deschiderea I</dev/ptmx> este un PTM independent cu "
"propriul său PTS asociat, a cărui rută poate fi găsită prin transmiterea "
"descriptorului de fișier către B<ptsname>(3)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The Linux support for the above (known as UNIX 98 pseudoterminal naming)  is "
"done using the I<devpts> filesystem, that should be mounted on I</dev/pts>."
msgstr ""
"Suportul Linux pentru cele de mai sus (cunoscut sub numele de pseudoterminal "
"UNIX 98) se face folosind sistemul de fișiere I<devpts>, care trebuie montat "
"pe I</dev/pts>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Before this UNIX 98 scheme, master pseudoterminals were called I</dev/"
"ptyp0>, ...  and slave pseudoterminals I</dev/ttyp0>, ...  and one needed "
"lots of preallocated device nodes."
msgstr ""
"Înainte de această schemă UNIX 98, pseudoterminalele maestre se numeau I</"
"dev/ptyp0>, ... și pseudoterminalele sclave I</dev/ttyp0>, ... și era nevoie "
"de o mulțime de noduri de dispozitiv prealocate."

# R-GC, scrie:
# COLOPHON (în eng.) = COLOFON (în rom.)
# *****
# Colofon = Notă la sfârșitul unei publicații sau
# pe verso foii de titlu, cuprinzând datele editoriale.
# sau:
# Colofon =  Însemnare la sfârșitul unei cărți în
# epoca manuscriselor și incunabulelor, cuprinzând
# date privind tipograful, locul unde a lucrat,
# autorul și titlul lucrării.
# sau:
# Colofon = Notă, însemnare finală a unei cărți,
# care reproduce sau completează cele spuse în titlu.
# ===============
# M-am decis pentru:
# COLOFON -- NOTĂ FINALĂ
#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON -- NOTĂ FINALĂ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Această pagină face parte din versiunea 4.16 a proiectului Linux I<man-"
"pages>.  O descriere a proiectului, informații despre raportarea erorilor și "
"cea mai recentă versiune a acestei pagini pot fi găsite la \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"
