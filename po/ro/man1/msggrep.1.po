# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-10-02 12:11+0200\n"
"PO-Revision-Date: 2023-06-29 08:55+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MSGGREP"
msgstr "MSGGREP"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "September 2023"
msgstr "septembrie 2023"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "GNU gettext-tools 0.20.2"
msgid "GNU gettext-tools 0.22.2"
msgstr "GNU gettext-tools 0.20.2"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "msggrep - pattern matching on message catalog"
msgstr "msggrep - potrivire de modele în catalogul de mesaje"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<msggrep> [I<\\,OPTION\\/>] [I<\\,INPUTFILE\\/>]"
msgstr "B<msggrep> [I<\\,OPȚIUNE\\/>] [I<\\,FIȘIER_INTRARE\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Extracts all messages of a translation catalog that match a given pattern or "
"belong to some given source files."
msgstr ""
"Extrage toate mesajele dintr-un catalog de traducere care se potrivesc cu un "
"model dat sau aparțin unor fișiere sursă date."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumentele obligatorii pentru opțiunile lungi sunt de asemenea obligatorii "
"pentru opțiunile scurte."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Input file location:"
msgstr "Locație fișier de intrare:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "INPUTFILE"
msgstr "FIȘIER DE INTRARE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "input PO file"
msgstr "fișier PO de intrare"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-D>, B<--directory>=I<\\,DIRECTORY\\/>"
msgstr "B<-D>, B<--directory>=I<\\,DIRECTOR\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "add DIRECTORY to list for input files search"
msgstr "adaugă DIRECTOR la listă pentru căutarea fișierelor de intrare"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If no input file is given or if it is -, standard input is read."
msgstr ""
"Dacă nu este dat nici un fișier de intrare sau dacă este „-”, se va citi "
"intrarea standard."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Output file location:"
msgstr "Locație fișier de ieșire:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output-file>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output-file>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write output to specified file"
msgstr "scrie ieșirea în fișierul specificat"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The results are written to standard output if no output file is specified or "
"if it is -."
msgstr ""
"Rezultatele sunt scrise la ieșirea standard dacă nu este specificat niciun "
"fișier de ieșire sau dacă acesta este „-”."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Message selection:"
msgstr "Selecție mesaje:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"[-N SOURCEFILE]... [-M DOMAINNAME]...  [-J MSGCTXT-PATTERN] [-K MSGID-"
"PATTERN] [-T MSGSTR-PATTERN] [-C COMMENT-PATTERN] [-X EXTRACTED-COMMENT-"
"PATTERN]"
msgstr ""
"[-N FIȘIERSURSĂ]... [-M NUMEDOMENIU]...  [-J MODEL-MSGCTXT] [-K MODEL-MSGID] "
"[-T MODEL-MSGSTR] [-C MODEL-COMENTARIU] [-X MODEL-COMENTARIU-EXTRAS]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A message is selected if it comes from one of the specified source files, or "
"if it comes from one of the specified domains, or if B<-J> is given and its "
"context (msgctxt) matches MSGCTXT-PATTERN, or if B<-K> is given and its key "
"(msgid or msgid_plural) matches MSGID-PATTERN, or if B<-T> is given and its "
"translation (msgstr) matches MSGSTR-PATTERN, or if B<-C> is given and the "
"translator's comment matches COMMENT-PATTERN, or if B<-X> is given and the "
"extracted comment matches EXTRACTED-COMMENT-PATTERN."
msgstr ""
"Un mesaj este selectat dacă provine dintr-unul dintre fișierele sursă "
"specificate, dacă provine de la unul dintre domeniile specificate, dacă este "
"dată opțiunea B<-J> și contextul său (msgctxt) se potrivește cu MODEL-"
"MSGCTXT, dacă este dată opțiunea B<-K> și cheia sa (msgid sau msgid_plural) "
"se potrivește cu MODEL-MSGID, dacă este dată opțiunea B<-T> și traducerea sa "
"(msgstr) se potrivește cu MODEL-MSGSTR, dacă este dată opțiunea B<-C> și "
"comentariul traducătorului se potrivește cu MODEL-COMENTARIU, sau dacă este "
"dată opțiunea B<-X> și comentariul extras se potrivește cu MODEL-COMENTARIU-"
"EXTRAS."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When more than one selection criterion is specified, the set of selected "
"messages is the union of the selected messages of each criterion."
msgstr ""
"Când este specificat mai mult de un criteriu de selecție, setul de mesaje "
"selectate este uniunea mesajelor selectate ale fiecărui criteriu."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"MSGCTXT-PATTERN or MSGID-PATTERN or MSGSTR-PATTERN or COMMENT-PATTERN or "
"EXTRACTED-COMMENT-PATTERN syntax:"
msgstr ""
"Sintaxa MODEL-MSGCTXT, MODEL-MSGID, MODEL-MSGSTR, MODEL-COMENTARIU, sau "
"MODEL-COMENTARIU-EXTRAS:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "[-E | B<-F]> [-e PATTERN | B<-f> FILE]..."
msgstr "[-E | -F] [-e MODEL | -f FIȘIER]..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"PATTERNs are basic regular expressions by default, or extended regular "
"expressions if B<-E> is given, or fixed strings if B<-F> is given."
msgstr ""
"MODELELE sunt implicit, expresii regulate de bază, sau expresii regulate "
"extinse, dacă este dată opțiunea B<-E>, sau șiruri fixe dacă este dată "
"opțiunea B<-F>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-N>, B<--location>=I<\\,SOURCEFILE\\/>"
msgstr "B<-N>, B<--location>=I<\\,FIȘIER_SURSĂ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "select messages extracted from SOURCEFILE"
msgstr "selectează mesajele extrase din FIȘIER_SURSĂ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-M>, B<--domain>=I<\\,DOMAINNAME\\/>"
msgstr "B<-M>, B<--domain>=I<\\,NUME_DOMENIU\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "select messages belonging to domain DOMAINNAME"
msgstr "selectează mesajele aparținând domeniului NUME_DOMENIU"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-J>, B<--msgctxt>"
msgstr "B<-J>, B<--msgctxt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "start of patterns for the msgctxt"
msgstr "începutul modelelor pentru msgctxt"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-K>, B<--msgid>"
msgstr "B<-K>, B<--msgid>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "start of patterns for the msgid"
msgstr "începutul modelelor pentru msgid"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--msgstr>"
msgstr "B<-T>, B<--msgstr>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "start of patterns for the msgstr"
msgstr "începutul modelelor pentru msgstr"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-C>, B<--comment>"
msgstr "B<-C>, B<--comment>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "start of patterns for the translator's comment"
msgstr "începutul modelelor pentru comentariul traducătorului"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-X>, B<--extracted-comment>"
msgstr "B<-X>, B<--extracted-comment>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "start of patterns for the extracted comment"
msgstr "începutul modelelor pentru comentariul extras"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-E>, B<--extended-regexp>"
msgstr "B<-E>, B<--extended-regexp>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "PATTERN is an extended regular expression"
msgstr "MODEL este o expresie regulată extinsă"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-F>, B<--fixed-strings>"
msgstr "B<-F>, B<--fixed-strings>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "PATTERN is a set of newline-separated strings"
msgstr "MODEL este un set de șiruri separate prin linii noi"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-e>, B<--regexp>=I<\\,PATTERN\\/>"
msgstr "B<-e>, B<--regexp>=I<\\,MODEL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use PATTERN as a regular expression"
msgstr "folosește MODEL ca expresie regulată"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--file>=I<\\,FILE\\/>"
msgstr "B<-f>, B<--file>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "obtain PATTERN from FILE"
msgstr "obține MODEL din FIȘIER"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-case>"
msgstr "B<-i>, B<--ignore-case>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ignore case distinctions"
msgstr "ignoră distincția dintre majuscule și minuscule"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--invert-match>"
msgstr "B<-v>, B<--invert-match>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output only the messages that do not match any selection criterion"
msgstr ""
"afișează numai mesajele care nu corespund niciunui criteriu de selecție"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Input file syntax:"
msgstr "Sintaxă fișier de intrare:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--properties-input>"
msgstr "B<-P>, B<--properties-input>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "input file is in Java .properties syntax"
msgstr "fișierul de intrare este în sintaxa Java .properties"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--stringtable-input>"
msgstr "B<--stringtable-input>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "input file is in NeXTstep/GNUstep .strings syntax"
msgstr "fișierul de intrare este în sintaxa NeXTstep/GNUstep .strings"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Output details:"
msgstr "Detalii ieșire:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--color>"
msgstr "B<--color>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use colors and other text attributes always"
msgstr "utilizează întotdeauna culori și alte atribute ale textului"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--color>=I<\\,WHEN\\/>"
msgstr "B<--color>=I<\\,CÂND\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"use colors and other text attributes if WHEN.  WHEN may be 'always', "
"'never', 'auto', or 'html'."
msgstr ""
"utilizează culori și alte atribute ale textului dacă VALOARE.  VALOARE poate "
"fi: „always” (întotdeauna), „never” (niciodată), „auto” sau „html”."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--style>=I<\\,STYLEFILE\\/>"
msgstr "B<--style>=I<\\,FIȘIER_DE_STIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "specify CSS style rule file for B<--color>"
msgstr "specifică fișierul cu reguli de stil CSS pentru opțiunea B<--color>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-escape>"
msgstr "B<--no-escape>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not use C escapes in output (default)"
msgstr "nu utilizează eludări C în ieșire (implicit)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--escape>"
msgstr "B<--escape>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use C escapes in output, no extended chars"
msgstr "folosește eludări C în ieșire, fără caractere extinse"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--force-po>"
msgstr "B<--force-po>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write PO file even if empty"
msgstr "scrie fișierul PO chiar dacă este gol"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--indent>"
msgstr "B<--indent>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "indented output style"
msgstr "scrie fișierul .po folosind stil indentat"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-location>"
msgstr "B<--no-location>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "suppress '#: filename:line' lines"
msgstr "suprimă liniile „#: nume_fișier:linie”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--add-location>"
msgstr "B<-n>, B<--add-location>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "preserve '#: filename:line' lines (default)"
msgstr "păstrează liniile „#: nume_fișier:linie” (implicit)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--strict>"
msgstr "B<--strict>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "strict Uniforum output style"
msgstr "scrie ieșirea în stil strict Uniforum"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--properties-output>"
msgstr "B<-p>, B<--properties-output>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write out a Java .properties file"
msgstr "scrie un fișier .properties Java"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--stringtable-output>"
msgstr "B<--stringtable-output>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write out a NeXTstep/GNUstep .strings file"
msgstr "scrie un fișier .strings NeXTstep/GNUstep"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--width>=I<\\,NUMBER\\/>"
msgstr "B<-w>, B<--width>=I<\\,NUMĂR\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "set output page width"
msgstr "stabilește lățimea paginii de ieșire"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-wrap>"
msgstr "B<--no-wrap>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"do not break long message lines, longer than the output page width, into "
"several lines"
msgstr ""
"nu împarte liniile lungi de mesaj, mai lungi decât lățimea paginii de "
"ieșire, în mai multe linii"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--sort-output>"
msgstr "B<--sort-output>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "generate sorted output"
msgstr "generează ieșire ordonată alfanumeric"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--sort-by-file>"
msgstr "B<--sort-by-file>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sort output by file location"
msgstr "ordonează ieșirea după locația fișierului"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Informative output:"
msgstr "Ieșire informativă:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afișează acest mesaj de ajutor și iese"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "afișează informațiile despre versiune și iese"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Bruno Haible."
msgstr "Scris de Bruno Haible."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report bugs in the bug tracker at E<lt>https://savannah.gnu.org/projects/"
"gettextE<gt> or by email to E<lt>bug-gettext@gnu.orgE<gt>."
msgstr ""
"Raportați erorile în sistemul de urmărire a erorilor la E<lt>https://"
"savannah.gnu.org/projects/gettextE<gt> sau prin e-mail la E<lt>bug-"
"gettext@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"Copyright \\(co 2001-2023 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Drepturi de autor \\(co 2001-2023 Free Software Foundation, Inc. Licența "
"GPLv3+: GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl."
"htmlE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sunteți liber să-l modificați și să-l "
"redistribuiți.  Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The full documentation for B<msggrep> is maintained as a Texinfo manual.  If "
"the B<info> and B<msggrep> programs are properly installed at your site, the "
"command"
msgstr ""
"Documentația completă pentru B<msggrep> este menținută ca un manual Texinfo. "
"Dacă programele B<info> și B<msggrep> sunt instalate corect pe sistemul "
"dumneavoastră, comanda"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<info msggrep>"
msgstr "B<info msggrep>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2023"
msgstr "februarie 2023"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GNU gettext-tools 0.21"
msgstr "GNU gettext-tools 0.21"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid ""
"Copyright \\(co 2001-2020 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Drepturi de autor \\(co 2001-2020 Free Software Foundation, Inc. Licența "
"GPLv3+: GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl."
"htmlE<gt>"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "June 2023"
msgstr "iunie 2023"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "GNU gettext-tools 0.22"
msgstr "GNU gettext-tools 0.22"

#. type: TH
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "October 2022"
msgstr "octombrie 2022"

#. type: TH
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "GNU gettext-tools 0.21.1"
msgstr "GNU gettext-tools 0.21.1"

#. type: Plain text
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co 2001-2022 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Drepturi de autor \\(co 2001-2022 Free Software Foundation, Inc. Licența "
"GPLv3+: GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl."
"htmlE<gt>"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "April 2020"
msgstr "aprilie 2020"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU gettext-tools 0.20.2"
msgstr "GNU gettext-tools 0.20.2"
