# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-10-02 12:31+0200\n"
"PO-Revision-Date: 2023-05-23 01:33+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "TAIL"
msgstr "TAIL"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "septembrie 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "tail - output the last part of files"
msgstr "tail - afișează ultima parte a fișierelor"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<tail> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<tail> [I<\\,OPȚIUNE\\/>]... [I<\\,FIȘIER\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Print the last 10 lines of each FILE to standard output.  With more than one "
"FILE, precede each with a header giving the file name."
msgstr ""
"Afișează ultimele 10 linii ale fiecărui FIȘIER la ieșirea standard.  Cu mai "
"mult de un FIȘIER, precede fiecare cu un antet care indică numele fișierului."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr "Fără FIȘIER, sau când FIȘIER este „-”, citește intrarea standard."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumentele obligatorii pentru opțiunile lungi sunt de asemenea obligatorii "
"pentru opțiunile scurte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--bytes>=I<\\,[\\/>+]NUM"
msgstr "B<-c>, B<--bytes>=I<\\,[\\/>+]NUM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"output the last NUM bytes; or use B<-c> +NUM to output starting with byte "
"NUM of each file"
msgstr ""
"afișează ultimii NUM octeți; sau dacă se utilizează „B<-c> +NUM” afișează "
"octeții începând cu octetul NUM al fiecărui fișier"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--follow[=>{name|descriptor}]"
msgstr "B<-f>, B<--follow[=>{nume|descriptor}]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output appended data as the file grows;"
msgstr "afișează datele anexate pe măsură ce fișierul crește;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "an absent option argument means 'descriptor'"
msgstr "un argument de opțiune absent înseamnă „descriptor”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-F>"
msgstr "B<-F>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "same as B<--follow>=I<\\,name\\/> B<--retry>"
msgstr "la fel ca „B<--follow>=I<\\,nume\\/> B<--retry>”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--lines>=I<\\,[\\/>+]NUM"
msgstr "B<-n>, B<--lines>=I<\\,[\\/>+]NUM"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "output the last NUM lines, instead of the last 10; or use B<-n> +NUM to "
#| "output starting with line NUM"
msgid ""
"output the last NUM lines, instead of the last 10; or use B<-n> +NUM to skip "
"NUM-1 lines at the start"
msgstr ""
"afișează ultimele NUM linii, în loc de ultimele 10; sau utilizați „B<-n> "
"+NUM” pentru a afișa începând cu linia NUM"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--max-unchanged-stats>=I<\\,N\\/>"
msgstr "B<--max-unchanged-stats>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "with B<--follow>=I<\\,name\\/>, reopen a FILE which has not"
msgstr "cu B<--follow>=I<\\,name\\/>, redeschide un FIȘIER care nu are"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"changed size after N (default 5) iterations to see if it has been unlinked "
"or renamed (this is the usual case of rotated log files); with inotify, this "
"option is rarely useful"
msgstr ""
"schimbată dimensiunea după N (implicit 5) iterații pentru a vedea dacă a "
"fost dezlegat sau redenumit (acesta este cazul obișnuit al fișierelor jurnal "
"rotite); cu inotify, această opțiune este rareori utilă"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--pid>=I<\\,PID\\/>"
msgstr "B<--pid>=I<\\,PID\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "with B<-f>, terminate after process ID, PID dies"
msgstr "cu B<-f>, termină după procesul ID, acest PID moare"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-q>, B<--quiet>, B<--silent>"
msgstr "B<-q>, B<--quiet>, B<--silent>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "never output headers giving file names"
msgstr "nu afișează niciodată antete care oferă nume de fișiere"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--retry>"
msgstr "B<--retry>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "keep trying to open a file if it is inaccessible"
msgstr ""
"continuă să încerce să deschidă un fișier dacă acesta pare să fie inaccesibil"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--sleep-interval>=I<\\,N\\/>"
msgstr "B<-s>, B<--sleep-interval>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"with B<-f>, sleep for approximately N seconds (default 1.0) between "
"iterations; with inotify and B<--pid>=I<\\,P\\/>, check process P at least "
"once every N seconds"
msgstr ""
"cu B<-f>, doarme timp de aproximativ N secunde (implicit 1.0) între "
"iterații; cu inotify și B<--pid>=I<\\,P\\/>, verifică procesul P cel puțin o "
"dată la fiecare N secunde."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "always output headers giving file names"
msgstr "afișează întotdeauna antete care oferă nume de fișiere"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr "B<-z>, B<--zero-terminated>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "line delimiter is NUL, not newline"
msgstr "delimitatorul de linie este NUL, nu linie nouă"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afișează acest mesaj de ajutor și iese"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "afișează informațiile despre versiune și iese"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"NUM may have a multiplier suffix: b 512, kB 1000, K 1024, MB 1000*1000, M "
"1024*1024, GB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y, "
"R, Q.  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"NUM poate avea un sufix multiplicator: b 512, kB 1000, K 1024, MB 1000*1000, "
"M 1024*1024, GB 1000*1000*1000, G 1024*1024*1024, ș.a.m.d. pentru T, P, E, "
"Z, Y,R,Q.  Prefixele binare pot fi folosite, de asemenea: KiB=K, MiB=M, ș.a."
"m.d."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"With B<--follow> (B<-f>), tail defaults to following the file descriptor, "
"which means that even if a tail'ed file is renamed, tail will continue to "
"track its end.  This default behavior is not desirable when you really want "
"to track the actual name of the file, not the file descriptor (e.g., log "
"rotation).  Use B<--follow>=I<\\,name\\/> in that case.  That causes tail to "
"track the named file in a way that accommodates renaming, removal and "
"creation."
msgstr ""
"Cu B<--follow> (B<-f>), tail urmează în mod implicit descriptorul de fișier, "
"ceea ce înseamnă că, chiar dacă un fișier este redenumit, tail va continua "
"să urmărească sfârșitul acestuia.  Acest comportament implicit nu este de "
"dorit atunci când doriți cu adevărat să urmăriți numele real al fișierului, "
"nu descriptorul de fișier (de exemplu, rotația jurnalului).  În acest caz, "
"utilizați B<--follow>=I<\\, nume\\/>.  Acest lucru face ca tail să "
"urmărească fișierul numit într-un mod care se adaptează la redenumire, "
"eliminare și creare."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Written by Paul Rubin, David MacKenzie, Ian Lance Taylor, and Jim Meyering."
msgstr ""
"Scris de Paul Rubin, David MacKenzie, Ian Lance Taylor, și Jim Meyering."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Ajutor online GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Raportați orice erori de traducere la: E<lt>https://translationproject.org/"
"team/ro.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2023 Free Software Foundation, Inc. Licența GPLv3+: "
"GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl."
"htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sunteți liber să-l modificați și să-l "
"redistribuiți.  Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<head>(1)"
msgstr "B<head>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/tailE<gt>"
msgstr ""
"Documentația completă este disponibilă la E<lt>https://www.gnu.org/software/"
"coreutils/tailE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) tail invocation\\(aq"
msgstr "sau local rulând comanda: «info \\(aq(coreutils) tal invocation\\(aq»"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "septembrie 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 mageia-cauldron opensuse-leap-15-6
msgid ""
"output the last NUM lines, instead of the last 10; or use B<-n> +NUM to "
"output starting with line NUM"
msgstr ""
"afișează ultimele NUM linii, în loc de ultimele 10; sau utilizați „B<-n> "
"+NUM” pentru a afișa începând cu linia NUM"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"NUM may have a multiplier suffix: b 512, kB 1000, K 1024, MB 1000*1000, M "
"1024*1024, GB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, "
"Y.  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"NUM poate avea un sufix multiplicator: b 512, kB 1000, K 1024, MB 1000*1000, "
"M 1024*1024, GB 1000*1000*1000, G 1024*1024*1024, ș.a.m.d. pentru T, P, E, "
"Z, Y.  Prefixele binare pot fi folosite, de asemenea: KiB=K, MiB=M, ș.a.m.d."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2022 Free Software Foundation, Inc. Licența GPLv3+: "
"GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl."
"htmlE<gt>."

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "aprilie 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "octombrie 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2020 Free Software Foundation, Inc. Licența GPLv3+: "
"GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl."
"htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "head(1)"
msgstr "head(1)"
