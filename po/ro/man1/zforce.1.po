# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-08-27 17:34+0200\n"
"PO-Revision-Date: 2023-06-27 16:36+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ZFORCE"
msgstr "ZFORCE"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "zforce - force a '.gz' extension on all gzip files"
msgstr "zforce - forțează o extensie „.gz” pentru toate fișierele gzip"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<zforce> [ name ...  ]"
msgstr "B<zforce> [ nume ...  ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<zforce> command forces a B<.gz> extension on all B<gzip> files so that "
"B<gzip> will not compress them twice.  This can be useful for files with "
"names truncated after a file transfer.  On systems with a 14 char limitation "
"on file names, the original name is truncated to make room for the .gz "
"suffix. For example, 12345678901234 is renamed to 12345678901.gz. A file "
"name such as foo.tgz is left intact."
msgstr ""
"Comanda B<zforce> forțează o extensie B<.gz> pentru toate fișierele B<gzip>, "
"astfel încât B<gzip> nu le va comprima de două ori.  Acest lucru poate fi "
"util pentru fișierele cu nume trunchiate după un transfer de fișiere.  Pe "
"sistemele cu o limitare de 14 caractere pentru numele fișierelor, numele "
"original este trunchiat pentru a face loc sufixului .gz. De exemplu, "
"12345678901234 este redenumit în 12345678901.gz. Un nume de fișier, cum ar "
"fi foo.tgz, rămâne intact."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "gzip(1), znew(1), zmore(1), zgrep(1), zdiff(1), gzexe(1)"
msgstr "gzip(1), znew(1), zmore(1), zgrep(1), zdiff(1), gzexe(1)"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I<zforce> forces a .gz extension on all I<gzip> files so that I<gzip> will "
"not compress them twice.  This can be useful for files with names truncated "
"after a file transfer.  On systems with a 14 char limitation on file names, "
"the original name is truncated to make room for the .gz suffix. For example, "
"12345678901234 is renamed to 12345678901.gz. A file name such as foo.tgz is "
"left intact."
msgstr ""
"I<zforce> forțează o extensie .gz pentru toate fișierele I<gzip>, astfel "
"încât I<gzip> nu le va comprima de două ori.  Acest lucru poate fi util "
"pentru fișierele cu nume trunchiate după un transfer de fișiere.  Pe "
"sistemele cu o limitare de 14 caractere pentru numele fișierelor, numele "
"original este trunchiat pentru a face loc sufixului .gz. De exemplu, "
"12345678901234 este redenumit în 12345678901.gz. Un nume de fișier, cum ar "
"fi foo.tgz, rămâne intact."
