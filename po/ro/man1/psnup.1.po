# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-08-10 19:05+0200\n"
"PO-Revision-Date: 2023-06-25 23:29+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "PSNUP"
msgstr "PSNUP"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "February 2023"
msgstr "februarie 2023"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "psnup 2.10"
msgstr "psnup 2.10"

#. type: TH
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "psnup - put multiple pages of a PostScript document on to one page"
msgstr ""
"psnup - pune mai multe pagini ale unui document PostScript pe o singură "
"pagină"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<psnup> [I<\\,OPTION\\/>...] I<\\,-NUP \\/>[I<\\,INFILE \\/>[I<\\,OUTFILE\\/"
">]]"
msgstr ""
"B<psnuo> [I<\\,OPȚIUNE\\/>...] I<\\,-NUP \\/>[I<\\,FIȘIER-INTRARE \\/>[I<\\,"
"FIȘIER-IEȘIRE\\/>]]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Put multiple pages of a PostScript document on to one page."
msgstr ""
"Pune mai multe pagini ale unui document PostScript pe o singură pagină."

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-NUMBER>"
msgstr "B<-[NUMĂR]>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "number of pages to impose on each output page"
msgstr "numărul de pagini care trebuie plasate pe fiecare pagină de ieșire"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--paper>=I<\\,PAPER\\/>"
msgstr "B<-p>, B<--paper>=I<\\,HÂRTIE\\/>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "output paper name or dimensions"
msgstr "numele sau dimensiunile hârtiei de ieșire"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--inpaper>=I<\\,PAPER\\/>"
msgstr "B<-P>, B<--inpaper>=I<\\,HÂRTIE\\/>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "input paper name or dimensions"
msgstr "numele sau dimensiunile hârtiei de intrare"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--margin>=I<\\,DIMENSION\\/>"
msgstr "B<-m>, B<--margin>=I<\\,DIMENSIUNE\\/>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"width of margin around each output page [default 0pt]; useful for thumbnail "
"sheets, as the original page margins will be shrunk"
msgstr ""
"lățimea marginii din jurul fiecărei pagini de ieșire [implicit 0pt]; utilă "
"pentru foile miniaturale, deoarece marginile paginii originale vor fi "
"micșorate"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--border>=I<\\,DIMENSION\\/>"
msgstr "B<-b>, B<--border>=I<\\,DIMENSIUNE\\/>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "width of border around each input page"
msgstr "lățimea chenarului din jurul fiecărei pagini de intrare"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--draw>[=I<\\,DIMENSION\\/>]"
msgstr "B<-d>, B<--draw>[=I<\\,DIMENSIUNE\\/>]"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"draw a line of given width around each page [relative to input page size; "
"argument defaults to 1pt; default is no line]"
msgstr ""
"trasează o linie de o lățime dată în jurul fiecărei pagini [în raport cu "
"dimensiunea paginii de intrare; valoarea implicită a argumentului este 1pt; "
"valoarea implicită este nici o linie]."

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--rotatedleft>"
msgstr "B<-l>, B<--rotatedleft>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "input pages are rotated left 90 degrees"
msgstr "paginile de intrare sunt rotite la stânga cu 90 de grade"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--rotatedright>"
msgstr "B<-r>, B<--rotatedright>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "input pages are rotated right 90 degrees"
msgstr "paginile de intrare sunt rotite la dreapta cu 90 de grade"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--flip>"
msgstr "B<-f>, B<--flip>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "swap output pages' width and height"
msgstr "interschimbă lățimea și înălțimea paginilor de ieșire"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--transpose>"
msgstr "B<-c>, B<--transpose>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "swap columns and rows (column-major order)"
msgstr "interschimbă coloanele și rândurile (ordinea coloană-majoră)"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--tolerance>=I<\\,NUMBER\\/>"
msgstr "B<-t>, B<--tolerance>=I<\\,NUMĂR\\/>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "maximum wasted area in square pt [default: 100,000]"
msgstr "suprafața maximă pierdută în puncte pătrate [implicit: 100.000]"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "don't show page numbers being output"
msgstr "nu afișează numerele de pagină la ieșire"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afișează acest mesaj de ajutor și iese"

#. type: TP
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr "B<-v>, B<--version>"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "display version information and exit"
msgstr "afișează informațiile despre versiune și iese"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"psnup aborts with an error if it cannot arrange the input pages so as to "
"waste less than the given tolerance."
msgstr ""
"psnup abandonează cu o eroare dacă nu poate aranja paginile de intrare "
"astfel încât să piardă mai puțin decât toleranța dată."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The output paper size defaults to the input paper size; if that is not "
"given, the default given by the `paper' command is used."
msgstr ""
"Dimensiunea hârtiei de ieșire este în mod implicit egală cu dimensiunea "
"hârtiei de intrare; dacă aceasta nu este indicată, se utilizează dimensiunea "
"implicită dată de comanda «paper»."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "The input paper size defaults to the output paper size."
msgstr ""
"Dimensiunea implicită a hârtiei de intrare este egală cu dimensiunea hârtiei "
"de ieșire."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"In row-major order (the default), adjacent pages are placed in rows across "
"the paper; in column-major order, they are placed in columns down the page."
msgstr ""
"În ordinea rând-major (ordinea implicită), paginile adiacente sunt așezate "
"în rânduri de-a lungul hârtiei; în ordinea coloană-majoră, acestea sunt "
"așezate în coloane în josul paginii."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"B<psnup> uses B<pstops> to impose multiple logical pages on to each physical "
"sheet of paper."
msgstr ""
"B<psnup> utilizează B<pstops> pentru a plasa mai multe pagini logice pe "
"fiecare foaie fizică de hârtie."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Paper sizes can be given either as a name (see B<paper(1)>)  or as "
"B<width>xB<height> (see B<psutils>(1)  for the available units)."
msgstr ""
"Dimensiunile hârtiei pot fi date fie ca un nume (a se vedea B<paper(1)>), "
"fie ca B<lățime>xB<înălțime> (a se vedea B<psutils>(1) pentru unitățile "
"disponibile)."

#. type: SS
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr "Starea de ieșire:"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "0"
msgstr "0"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "if OK,"
msgstr "dacă totul este OK,"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "1"
msgstr "1"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"if arguments or options are incorrect, or there is some other problem "
"starting up,"
msgstr ""
"dacă argumentele sau opțiunile sunt incorecte sau dacă există o altă "
"problemă la pornire,"

#. type: TP
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2"
msgstr "2"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"if there is some problem during processing, typically an error reading or "
"writing an input or output file."
msgstr ""
"dacă există o problemă în timpul procesării, de obicei o eroare de citire "
"sau de scriere a unui fișier de intrare sau de ieșire."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The potential use of this utility is varied but one particular use is in "
"conjunction with B<psbook>(1).  For example, using groff to create a "
"PostScript document and lpr as the E<.SM UNIX > print spooler a typical "
"command line might look like this:"
msgstr ""
"Potențialul de utilizare al acestui instrument este variat, dar o utilizare "
"specială este în combinație cu B<psbook>(1).  De exemplu, folosind «groff» "
"pentru a crea un document PostScript și «lpr» ca procesator de imprimare E<."
"SM UNIX >, o linie de comandă tipică ar putea arăta astfel:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "groff -Tps -ms I<file> | psbook | psnup -2 | lpr"
msgstr "groff -Tps -ms I<fișier> | psbook | psnup -2 | lpr"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"where file is a 4 page document this command will result in a two page "
"document printing two pages of I<file> per page and rearranges the page "
"order to match the input pages 4 and 1 on the first output page and pages 2 "
"then 3 of the input document on the second output page."
msgstr ""
"în cazul în care fișierul este un document de 4 pagini, această comandă va "
"avea ca rezultat un document de două pagini, imprimând două pagini de "
"I<fișier> pe pagină și rearanjează ordinea paginilor pentru a se potrivi cu "
"paginile de intrare 4 și 1 pe prima pagină de ieșire și cu paginile 2 și 3 "
"din documentul de intrare pe a doua pagină de ieșire."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Written by Angus J. C. Duggan and Reuben Thomas."
msgstr "Scris de Angus J. C. Duggan și Reuben Thomas."

#. type: SH
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
msgid ""
"Copyright \\(co Reuben Thomas 2016-2022.  Released under the GPL version 3, "
"or (at your option) any later version."
msgstr ""
"Drepturi de autor \\(co Reuben Thomas 2016-2022.  Publicat sub GPL versiunea "
"3 sau (la alegerea dumneavoastră) orice versiune ulterioară."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "TRADEMARKS"
msgstr "MĂRCI ÎNREGISTRATE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<PostScript> is a trademark of Adobe Systems Incorporated."
msgstr "B<PostScript> este o marcă înregistrată a Adobe Systems Incorporated."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<psutils>(1), B<paper>(1)"
msgstr "B<psutils>(1), B<paper>(1)"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "PSUtils Release 1 Patchlevel 17"
msgstr "PSUtils Release 1 Patchlevel 17"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "psnup - multiple pages per sheet"
msgstr "psnup - mai multe pagini pe foaie"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<psnup> [ B<-w>I<width> ] [ B<-h>I<height> ] [ B<-p>I<paper> ] [ B<-"
"W>I<width> ] [ B<-H>I<height> ] [ B<-P>I<paper> ] [ B<-l> ] [ B<-r> ] [ B<-"
"f> ] [ B<-c> ] [ B<-m>I<margin> ] [ B<-b>I<border> ] [ B<-d>I<lwidth> ] [ B<-"
"s>I<scale> ] [ B<->I<nup> ] [ B<-q> ] [ I<infile> [ I<outfile> ] ]"
msgstr ""
"B<psnup> [ B<-w>I<lățime> ] [ B<-h>I<înălțime> ] [ B<-p>I<hârtie> ] [ B<-"
"W>I<lățime> ] [ B<-H>I<înălțime> ] [ B<-P>I<hârtie> ] [ B<-l> ] [ B<-r> ] "
"[ B<-f> ] [ B<-c> ] [ B<-m>I<margine> ] [ B<-b>I<chenar> ] [ B<-d>I<lățime-"
"linie> ] [ B<-s>I<scara> ] [ B<->I<nup> ] [ B<-q> ] [ I<fișier-intrare> "
"[ I<fișier-ieșire> ] ]"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<Psnup> puts multiple logical pages onto each physical sheet of paper.  The "
"input PostScript file should follow the Adobe Document Structuring "
"Conventions."
msgstr ""
"I<psnup> pune mai multe pagini logice pe fiecare foaie de hârtie fizică.  "
"Fișierul PostScript de intrare trebuie să respecte convențiile de "
"structurare a documentelor Adobe."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The I<-w> option gives the paper width, and the I<-h> option gives the paper "
"height, normally specified in B<cm> or B<in>.  The I<-p> option can be used "
"as an alternative, to set the paper size to B<a0, a1, a2, a3, a4, a5, b5, "
"letter, legal, tabloid, statement,> executive, folio, quarto or B<10x14.> "
"The default paper size is normally B<a4,> but on a Debian system, /etc/"
"papersize is consulted.  The I<-W, -H,> and I<-P> options set the input "
"paper size, if it is different from the output size. This makes it easy to "
"impose pages of one size on a different size of paper."
msgstr ""
"Opțiunea I<-w> indică lățimea hârtiei, iar opțiunea I<-h> indică înălțimea "
"hârtiei, în mod normal specificată în B<cm> sau B<in>.  Opțiunea I<-p> poate "
"fi utilizată ca alternativă, pentru a stabili dimensiunea hârtiei la B<a0, "
"a1, a2, a3, a4, a5, b5, letter, legal, tabloid, statement,> executive, "
"folio, quarto sau B<10x14.> Dimensiunea implicită a hârtiei este, în mod "
"normal, B<a4,> dar pe un sistem Debian, se consultă /etc/papersize.  "
"Opțiunile I<-W, -H,> și I<-P> stabilesc dimensiunea hârtiei de intrare, dacă "
"aceasta este diferită de cea de ieșire. Acest lucru facilitează plasarea "
"paginilor de o dimensiune pe o hârtie de altă dimensiune."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The I<-l> option should be used for pages which are in landscape orientation "
"(rotated 90 degrees anticlockwise). The I<-r> option should be used for "
"pages which are in seascape orientation (rotated 90 degrees clockwise), and "
"the I<-f> option should be used for pages which have the width and height "
"interchanged, but are not rotated."
msgstr ""
"Opțiunea I<-l> ar trebui utilizată pentru paginile care sunt orientate pe "
"orizontală (rotite cu 90 de grade în sens invers acelor de ceasornic). "
"Opțiunea I<-r> trebuie utilizată pentru paginile care sunt în orientare "
"peisaj (rotite cu 90 de grade în sensul acelor de ceasornic), iar opțiunea "
"I<-f> trebuie utilizată pentru paginile care au lățimea și înălțimea "
"schimbate, dar nu sunt rotite."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<Psnup> normally uses `row-major' layout, where adjacent pages are placed "
"in rows across the paper.  The I<-c> option changes the order to `column-"
"major', where successive pages are placed in columns down the paper."
msgstr ""
"În mod normal, I<psnup> utilizează o dispunere „rând-major”, în care "
"paginile adiacente sunt așezate în rânduri pe hârtie.  Opțiunea I<-c> "
"schimbă ordinea în „columnă-majoră”, unde paginile succesive sunt plasate în "
"coloane de-a lungul hârtiei."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"A margin to leave around the whole page can be specified with the I<-m> "
"option. This is useful for sheets of `thumbnail' pages, because the normal "
"page margins are reduced by putting multiple pages on a single sheet."
msgstr ""
"O margine care să fie lăsată în jurul întregii pagini poate fi specificată "
"cu opțiunea I<-m>. Acest lucru este util pentru foile de pagini „miniatură”, "
"deoarece marginile normale ale paginii sunt reduse prin punerea mai multor "
"pagini pe o singură foaie."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The I<-b> option is used to specify an additional margin around each page on "
"a sheet."
msgstr ""
"Opțiunea I<-b> se utilizează pentru a specifica o margine suplimentară în "
"jurul fiecărei pagini de pe o foaie."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The I<-d> option draws a line around the border of each page, of the "
"specified width.  If the I<lwidth> parameter is omitted, a default linewidth "
"of 1 point is assumed. The linewidth is relative to the original page "
"dimensions, I<i.e.> it is scaled down with the rest of the page."
msgstr ""
"Opțiunea I<-d> trasează o linie în jurul marginii fiecărei pagini, cu "
"lățimea specificată.  Dacă parametrul I<lățime-linie> este omis, se "
"presupune o lățime de linie implicită de 1 punct. Lățimea liniei este "
"relativă la dimensiunile originale ale paginii,  I<adică> este "
"redimensionată odată cu restul paginii."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The scale chosen by I<psnup> can be overridden with the I<-s> option. This "
"is useful to merge pages which are already reduced."
msgstr ""
"Scara aleasă de I<psnup> poate fi înlocuită cu opțiunea I<-s>. Acest lucru "
"este util pentru a fuziona pagini care sunt deja reduse."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The I<-nup> option selects the number of logical pages to put on each sheet "
"of paper. This can be any whole number; I<psnup> tries to optimise the "
"layout so that the minimum amount of space is wasted. If I<psnup> cannot "
"find a layout within its tolerance limit, it will abort with an error "
"message. The alternative form I<-i nup> can also be used, for compatibility "
"with other n-up programs."
msgstr ""
"Opțiunea I<-nup> selectează numărul de pagini logice care trebuie puse pe "
"fiecare foaie de hârtie. Acesta poate fi orice număr întreg; I<psnup> "
"încearcă să optimizeze aranjamentul astfel încât să se irosească cât mai "
"puțin spațiu. În cazul în care I<psnup> nu poate găsi un aranjament care să "
"se încadreze în limita sa de toleranță, va întrerupe procesul cu un mesaj de "
"eroare. Se poate utiliza și forma alternativă I<-i nup>, pentru "
"compatibilitate cu alte programe n-up."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<Psnup> normally prints the page numbers of the pages re-arranged; the I<-"
"q> option suppresses this."
msgstr ""
"I<psnup> imprimă în mod normal numerele de pagină ale paginilor rearanjate; "
"opțiunea I<-q> suprimă acest lucru."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The potential use of this utility is varied but one particular use is in "
"conjunction with I<psbook(1).> For example, using groff to create a "
"PostScript document and lpr as the E<.SM UNIX > print spooler a typical "
"command line might look like this:"
msgstr ""
"Potențialul de utilizare al acestui instrument este variat, dar o utilizare "
"specială este în combinație cu I<psbook>(1).  De exemplu, folosind «groff» "
"pentru a crea un document PostScript și «lpr» ca procesator de imprimare E<."
"SM UNIX >, o linie de comandă tipică ar putea arăta astfel:"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Where file is a 4 page document this command will result in a two page "
"document printing two pages of I<file> per page and rearranges the page "
"order to match the input pages 4 and 1 on the first output page and pages 2 "
"then 3 of the input document on the second output page."
msgstr ""
"În cazul în care fișierul este un document de 4 pagini, această comandă va "
"avea ca rezultat un document de două pagini, imprimând două pagini de "
"I<fișier> pe pagină și rearanjează ordinea paginilor pentru a se potrivi cu "
"paginile de intrare 4 și 1 pe prima pagină de ieșire și cu paginile 2 și 3 "
"din documentul de intrare pe a doua pagină de ieșire."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgstr "Drepturi de autor © Angus J. C. Duggan 1991-1995"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"
msgstr ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "I<Psnup> does not accept all DSC comments."
msgstr "I<psnup> nu acceptă toate comentariile DSC."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "October 2021"
msgstr "octombrie 2021"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "psnup 2.07"
msgstr "psnup 2.07"

#. type: TP
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: mageia-cauldron
msgid ""
"B<Psnup> uses B<Pstops> to impose multiple logical pages on to each physical "
"sheet of paper."
msgstr ""
"B<psnup> utilizează B<pstops> pentru a plasa mai multe pagini logice pe "
"fiecare foaie fizică de hârtie."

#. type: Plain text
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co Reuben Thomas 2016-2021.  Released under the GPL version 3, "
"or (at your option) any later version."
msgstr ""
"Drepturi de autor \\(co Reuben Thomas 2016-2021.  Publicat sub GPL versiunea "
"3 sau (la alegerea dumneavoastră) orice versiune ulterioară."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "December 2021"
msgstr "decembrie 2021"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "psnup 2.08"
msgstr "psnup 2.08"
