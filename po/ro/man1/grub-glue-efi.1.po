# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-10-02 12:02+0200\n"
"PO-Revision-Date: 2023-05-23 09:00+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-GLUE-EFI"
msgstr "GRUB-GLUE-EFI"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2023"
msgstr "iulie 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12rc1-1"
msgstr "GRUB 2:2.12rc1-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-glue-efi - generate a fat binary for EFI"
msgstr "grub-glue-efi - generează un fișier binar „gras” pentru EFI"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-glue-efi> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgstr "B<grub-glue-efi> [I<\\,OPȚIUNE\\/>...] [I<\\,OPȚIUNI\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"grub-glue-efi processes ia32 and amd64 EFI images and glues them according "
"to Apple format."
msgstr ""
"grub-glue-efi procesează imaginile EFI ia32 și amd64 și le lipește în "
"conformitate cu formatul Apple."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Glue 32-bit and 64-bit binary into Apple universal one."
msgstr ""
"Unește binarii pe 32 de biți și pe 64 biți într-un binar universal de Apple."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-3>, B<--input32>=I<\\,FILE\\/>"
msgstr "B<-3>, B<--input32>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set input filename for 32-bit part."
msgstr ""
"stabilește numele fișierului de intrare pentru o partiție pe 32 de biți."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-6>, B<--input64>=I<\\,FILE\\/>"
msgstr "B<-6>, B<--input64>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set input filename for 64-bit part."
msgstr ""
"stabilește numele fișierului de intrare pentru o partiție pe 64 de biți."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set output filename. Default is STDOUT"
msgstr ""
"stabilește numele fișierului de ieșire. Implicit este STDOUT (ieșirea "
"standard)"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "afișează mesaje detaliate."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "oferă această listă de ajutor"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "oferă un mesaj de utilizare scurt"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "afișează versiunea programului"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Argumentele obligatorii sau opționale pentru opțiunile lungi sunt "
"obligatorii sau opționale și pentru opțiunile corespunzătoare scurte."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Raportați erorile la E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-glue-efi> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-glue-efi> programs are properly installed "
"at your site, the command"
msgstr ""
"Documentația completă pentru B<grub-glue-efi> este menținută ca un manual "
"Texinfo.  Dacă programele B<info> și B<grub-glue-efi> sunt instalate corect "
"în sistemul dvs., comanda"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-glue-efi>"
msgstr "B<info grub-glue-efi>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "April 2023"
msgstr "aprilie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13"
msgstr "GRUB 2.06-13"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "September 2023"
msgstr "septembrie 2023"

#. type: TH
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "GRUB 2:2.12rc1-1"
msgid "GRUB 2.12~rc1-10"
msgstr "GRUB 2:2.12rc1-1"
