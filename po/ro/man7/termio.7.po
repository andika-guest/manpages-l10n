# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-08-27 17:29+0200\n"
"PO-Revision-Date: 2023-06-28 13:06+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "termio"
msgstr "termio"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 octombrie 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pagini de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "termio - System V terminal driver interface"
msgstr "termio - interfață pentru controlorul de terminal System V"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<termio> is the name of the old System V terminal driver interface.  This "
"interface defined a I<termio> structure used to store terminal settings, and "
"a range of B<ioctl>(2)  operations to get and set terminal attributes."
msgstr ""
"B<termio> este numele vechii interfețe a controlorului de terminal System "
"V.  Această interfață definea o structură I<termio> utilizată pentru a stoca "
"parametrii terminalului și o serie de operații B<ioctl>(2) pentru a obține "
"și stabili atributele terminalului."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<termio> interface is now obsolete: POSIX.1-1990 standardized a "
"modified version of this interface, under the name B<termios>.  The POSIX.1 "
"data structure differs slightly from the System V version, and POSIX.1 "
"defined a suite of functions to replace the various B<ioctl>(2)  operations "
"that existed in System V.  (This was done because B<ioctl>(2)  was "
"unstandardized, and its variadic third argument does not allow argument type "
"checking.)"
msgstr ""
"Interfața B<termio> este acum depășită: POSIX.1-1990 a standardizat o "
"versiune modificată a acestei interfețe, sub numele B<termios>.  Structura "
"de date POSIX.1 diferă ușor de versiunea System V, iar POSIX.1 a definit o "
"suită de funcții pentru a înlocui diversele operații B<ioctl>(2) care "
"existau în System V (acest lucru a fost făcut deoarece B<ioctl>(2) nu era "
"standardizat, iar al treilea argument variadic (de funcție variadică) al "
"acestuia nu permite verificarea tipului de argument)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If you're looking for a page called \"termio\", then you can probably find "
"most of the information that you seek in either B<termios>(3)  or "
"B<ioctl_tty>(2)."
msgstr ""
"Dacă sunteți în căutarea unei pagini numite „termio”, atunci probabil că "
"puteți găsi majoritatea informațiilor pe care le căutați fie în "
"B<termios>(3), fie în B<ioctl_tty>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<reset>(1), B<setterm>(1), B<stty>(1), B<ioctl_tty>(2), B<termios>(3), "
"B<tty>(4)"
msgstr ""
"B<reset>(1), B<setterm>(1), B<stty>(1), B<ioctl_tty>(2), B<termios>(3), "
"B<tty>(4)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pagini de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "TERMIO"
msgstr "TERMIO"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-05-03"
msgstr "3 mai 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manualul programatorului Linux"

# R-GC, scrie:
# COLOPHON (în eng.) = COLOFON (în rom.)
# *****
# Colofon = Notă la sfârșitul unei publicații sau
# pe verso foii de titlu, cuprinzând datele editoriale.
# sau:
# Colofon =  Însemnare la sfârșitul unei cărți în
# epoca manuscriselor și incunabulelor, cuprinzând
# date privind tipograful, locul unde a lucrat,
# autorul și titlul lucrării.
# sau:
# Colofon = Notă, însemnare finală a unei cărți,
# care reproduce sau completează cele spuse în titlu.
# ===============
# M-am decis pentru:
# COLOFON -- NOTĂ FINALĂ
#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON -- NOTĂ FINALĂ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Această pagină face parte din versiunea 4.16 a proiectului Linux I<man-"
"pages>.  O descriere a proiectului, informații despre raportarea erorilor și "
"cea mai recentă versiune a acestei pagini pot fi găsite la \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"
