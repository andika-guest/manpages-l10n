# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-06-27 19:48+0200\n"
"PO-Revision-Date: 2023-07-14 23:03+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: IX
#: debian-bookworm debian-unstable
#, no-wrap
msgid "sane-pnm"
msgstr "sane-pnm"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "14 Jul 2008"
msgstr "14 iulie 2008"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE: „Scanner Access Now Easy”"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: debian-bookworm debian-unstable
msgid "sane-pnm - SANE PNM image reader pseudo-backend"
msgstr "sane-pnm - pseudo-controlor pentru cititorul de imagini SANE PNM"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<sane-pnm> library implements a SANE (Scanner Access Now Easy) backend "
"that provides access to PNM (Portable aNyMap files, which covers PBM bitmap "
"files, PGM grayscale files, and PPM pixmap files).  The purpose of this "
"backend is primarily to aid in debugging of SANE frontends.  It also serves "
"as an illustrative example of a minimal SANE backend."
msgstr ""
"Biblioteca B<sane-pnm> implementează un controlor SANE (Scanner Access Now "
"Easy) care oferă acces la fișierele PNM (fișiere PNM (Portable aNyMap, care "
"acoperă fișierele bitmap PBM, fișierele în tonuri de gri PGM și fișierele "
"pixmap PPM).  Scopul acestui controlor este în primul rând de a ajuta la "
"depanarea interfețelor SANE.  De asemenea, acesta servește ca exemplu "
"ilustrativ al unui controlor SANE minimal."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DEVICE NAMES"
msgstr "NUME DE DISPOZITIVE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "This backend provides two devices called B<0> and B<1.>"
msgstr "Acest control pune la dispoziție două dispozitive numite B<0> și B<1.>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "CONFIGURATION"
msgstr "CONFIGURARE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "No configuration required."
msgstr "Nu este necesară nicio configurare."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-pnm.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-pnm.a>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: debian-bookworm debian-unstable
msgid "The static library implementing this backend."
msgstr "Biblioteca statică care implementează acest controlor."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-pnm.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-pnm.so>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Biblioteca partajată care implementează acest controlor (prezentă pe "
"sistemele care acceptă încărcare dinamică)."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "ENVIRONMENT"
msgstr "MEDIU"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<SANE_DEBUG_PNM>"
msgstr "B<SANE_DEBUG_PNM>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend.  E.g., a value of 128 "
"requests all debug output to be printed.  Smaller levels reduce verbosity."
msgstr ""
"Dacă biblioteca a fost compilată cu suportul de depanare activat, această "
"variabilă de mediu controlează nivelul de depanare pentru acest controlor.  "
"De exemplu, o valoare de 128 solicită imprimarea tuturor datelor de "
"depanare.  Nivelurile mai mici reduc volumul de informații."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If the B<sane-pnm> backend is installed and B<saned>(8)  is used to allow "
"users on remote computers to scan on the local machine, PNM files can be "
"read by the remote user. This is limited to the files B<saned>(8)  can "
"access (usually it's running as user \"sane\"). All PNM files can be read if "
"B<saned>(8)  runs as root which isn't recommended anyway. The B<sane-pnm> "
"backend is disabled by default. If you want to use it, enable it with "
"configure (see I<configure --help> for details). Be sure that only trusted "
"users can access the B<sane-pnm> backend over B<saned>(8)."
msgstr ""
"Dacă controlorul B<sane-pnm> este instalat și B<saned>(8) este utilizat "
"pentru a permite utilizatorilor de pe calculatoare la distanță să scaneze pe "
"calculatorul local, fișierele PNM pot fi citite de către utilizatorul de la "
"distanță. Acest lucru este limitat la fișierele pe care B<saned>(8) le poate "
"accesa (de obicei, acesta rulează ca utilizator „sane”). Toate fișierele PNM "
"pot fi citite dacă B<saned>(8) rulează ca root, ceea ce oricum nu este "
"recomandat. Controlorul B<sane-pnm> este dezactivat în mod implicit. Dacă "
"doriți să îl utilizați, activați-l cu «configure» (consultați I<configure --"
"help> pentru detalii). Asigurați-vă că numai utilizatorii de încredere pot "
"accesa controlorul B<sane-pnm> prin B<saned>(8)."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Andreas Beck, Gordon Matzigkeit, and David Mosberger"
msgstr "Andreas Beck, Gordon Matzigkeit și David Mosberger"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<sane>(7), B<saned>(8)"
msgstr "B<sane>(7), B<saned>(8)"
