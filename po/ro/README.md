# manpages-ro

This is the Romanian language section of the manpages-l10n project,
which contains the Romanian language translations of the manual 
pages of  the GNU/Linux software.

If you have found a mistake (grammatical, typing or expression), or if you
have an idea to improve an existing translation, or if you have translated
(totally or partially) a manual page, or if you want to translate such a page,
please contact with the current person responsible of this section,
[Remus-Gabriel Chelu](mailto:remusgabriel.chelu@disroot.org).

If you have noticed that a manual page is missing from the "library" of this
project, please contact [Helge Kreutzmann](mailto:debian@helgefjell.de) (maintainer of this project) or with
[Mario Blättermann](mailto:mario.blaettermann@gmail.com) (the maintainer and mentor of this project).

If your favorite GNU/(Linux, Hurd, BSD) distribution does not contain this
package in its repository, please contact the maintainers of that distribution
to ask them to include this package in their package repository.

The current status of these translations (statistics) is available on the [page](https://manpages-l10n-team.pages.debian.net/manpages-l10n/).

********************************************************************************

Aceasta este secțiunea în limba romănă din cadrul proiectului manpages-l10n,
ce conține traducerile în limba română a paginilor de manual al software-ului
GNU/Linux.

Dacă ați găsit vreo greșeală (gramaticală, de tastare sau de exprimare), sau
dacă aveți o idee de-a îmbunătăți o traducere existentă, sau dacă aveți tradusă
(total sau parțial) o pagină de manual, sau doriți să traduceți o astfel de pagină,
puneți-vă în contact cu responsabilul actual al acestei secțiuni,
[Remus-Gabriel Chelu](mailto:remusgabriel.chelu@disroot.org).

Dacă distribuția dumneavoastră preferată de GNU/(Linux, Hurd, BSD) nu conține
în depozitul său acest pachet, puneți-vă în contact cu responsabilii acelei
distribuții pentru ai ruga să includă acest pachet în depozitul acesteia de pachete.

Starea curentă a acestor traduceri (statistica) este disponibilă [pe această pagină](https://manpages-l10n-team.pages.debian.net/manpages-l10n/).

