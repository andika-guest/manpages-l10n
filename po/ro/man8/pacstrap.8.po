# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-02-28 18:56+0100\n"
"PO-Revision-Date: 2023-07-03 01:05+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "PACSTRAP"
msgstr "PACSTRAP "

#. type: TH
#: archlinux
#, no-wrap
msgid "11/20/2022"
msgstr "20 noiembrie 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr "\\ \" "

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr "\\ \""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux
msgid "pacstrap - install packages to the specified new root directory"
msgstr "pacstrap - instalează pachetele în noul director rădăcină specificat"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux
msgid "pacstrap [options] root [packages\\&...]"
msgstr "pacstrap [opțiuni] root [pachete\\&...]"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux
msgid ""
"pacstrap is designed to create a new system installation from scratch\\&. "
"The specified packages will be installed into a given directory after "
"setting up some basic mountpoints\\&. By default, the host system\\(cqs "
"pacman signing keys and mirrorlist will be used to seed the chroot\\&."
msgstr ""
"«pacstrap» este conceput pentru a crea o nouă instalare de sistem pornind de "
"la zero\\&. Pachetele specificate vor fi instalate într-un anumit director "
"după ce se vor stabili câteva puncte de montare de bază\\&. În mod implicit, "
"cheile de semnare «pacman» și lista de oglinzi ale sistemului gazdă vor fi "
"utilizate pentru a crea „chroot-ul”\\&."

#. type: Plain text
#: archlinux
msgid ""
"If no packages are specified to be installed, the I<base> metapackage will "
"be installed\\&."
msgstr ""
"Dacă nu este specificat niciun pachet care să fie instalat, va fi instalat "
"metapachetul I<base>\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: archlinux
msgid "B<-C> E<lt>configE<gt>"
msgstr "B<-C> E<lt>fișier-configurareE<gt>"

#. type: Plain text
#: archlinux
msgid "Use an alternate config file for pacman\\&."
msgstr "Utilizează un fișier de configurare alternativ pentru «pacman»\\&."

#. type: Plain text
#: archlinux
msgid "B<-c>"
msgstr "B<-c>"

#. type: Plain text
#: archlinux
msgid "Use the package cache on the host, rather than the target\\&."
msgstr ""
"Utilizează zona de prestocare a pachetelor de pe gazdă, mai degrabă decât "
"cea de pe țintă (chroot)."

#. type: Plain text
#: archlinux
msgid "B<-D>"
msgstr "B<-D>"

#. type: Plain text
#: archlinux
msgid "Skip pacman dependency checks\\&."
msgstr "Omite verificările de dependență ale «pacman»\\&."

#. type: Plain text
#: archlinux
msgid "B<-G>"
msgstr "B<-G>"

#. type: Plain text
#: archlinux
msgid "Avoid copying the host\\(cqs pacman keyring to the target\\&."
msgstr "Evită copierea inelului de chei «pacman» al gazdei pe țintă\\&."

#. type: Plain text
#: archlinux
msgid "B<-i>"
msgstr "B<-i>"

#. type: Plain text
#: archlinux
msgid "Prompt for package confirmation when needed (run interactively)\\&."
msgstr ""
"Solicită confirmarea pachetului atunci când este necesar (se execută "
"interactiv)\\&."

#. type: Plain text
#: archlinux
msgid "B<-K>"
msgstr "B<-K>"

#. type: Plain text
#: archlinux
msgid "Initialize an empty pacman keyring in the target (implies I<-G>)\\&."
msgstr ""
"Inițializează un inel de chei «pacman» gol în țintă (implică I<-G>)\\&."

#. type: Plain text
#: archlinux
msgid "B<-M>"
msgstr "B<-M>"

#. type: Plain text
#: archlinux
msgid "Avoid copying the host\\(cqs mirrorlist to the target\\&."
msgstr "Evitați copierea listei de oglinzi a gazdei pe țintă\\&."

#. type: Plain text
#: archlinux
msgid "B<-N>"
msgstr "B<-N>"

#. type: Plain text
#: archlinux
msgid ""
"Run in unshare mode\\&. This will use B<unshare>(1)  to create a new mount "
"and user namespace, allowing regular users to create new system "
"installations\\&."
msgstr ""
"Rulează în modul „unshare” (nepartajat)\\&. Aceasta va utiliza B<unshare>(1) "
"pentru a crea un nou spațiu de montare și un nou spațiu de nume de "
"utilizator, permițând utilizatorilor obișnuiți să creeze noi instalații de "
"sistem\\&."

#. type: Plain text
#: archlinux
msgid "B<-P>"
msgstr "B<-P>"

#. type: Plain text
#: archlinux
msgid "Copy the host\\(cqs pacman config to the target\\&."
msgstr "Copiază configurația «pacman» a gazdei pe țintă\\&."

#. type: Plain text
#: archlinux
msgid "B<-U>"
msgstr "B<-U>"

#. type: Plain text
#: archlinux
msgid ""
"Use pacman -U to install packages\\&. Useful for obtaining fine-grained "
"control over the installed packages\\&."
msgstr ""
"Utilizează «pacman -U» pentru a instala pachetele\\&. Util pentru a obține "
"un control fin asupra pachetelor instalate\\&."

#. type: Plain text
#: archlinux
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: archlinux
msgid "Output syntax and command line options\\&."
msgstr "Afișează sintaxa și opțiunile liniei de comandă\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux
msgid "B<pacman>(8)"
msgstr "B<pacman>(8)"

#. type: SH
#: archlinux
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux
msgid ""
"Bugs can be reported on the bug tracker I<https://bugs\\&.archlinux\\&.org> "
"in the Arch Linux category and title prefixed with [arch-install-scripts] or "
"via arch-projects@archlinux\\&.org\\&."
msgstr ""
"Erorile pot fi raportate în sistemul de urmărire a erorilor I<https://"
"bugs\\&.archlinux\\&&.org> în categoria Arch Linux și cu titlul prefixat cu "
"[arch-install-scripts] sau prin intermediul arch-projects@archlinux\\&."
"org\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

#. type: Plain text
#: archlinux
msgid "Maintainers:"
msgstr "Responsabilii actuali:"

#. type: Plain text
#: archlinux
msgid "Dave Reisner E<lt>dreisner@archlinux\\&.orgE<gt>"
msgstr "Dave Reisner E<lt>dreisner@archlinux\\&.orgE<gt>"

#. type: Plain text
#: archlinux
msgid "Eli Schwartz E<lt>eschwartz@archlinux\\&.orgE<gt>"
msgstr "Eli Schwartz E<lt>eschwartz@archlinux\\&.orgE<gt>"

#. type: Plain text
#: archlinux
msgid ""
"For additional contributors, use git shortlog -s on the arch-install-"
"scripts\\&.git repository\\&."
msgstr ""
"Pentru contribuitori suplimentari, folosiți git shortlog -s în depozitul "
"arch-install-scripts\\&.git\\&."
