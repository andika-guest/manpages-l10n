# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-10-02 12:03+0200\n"
"PO-Revision-Date: 2023-05-23 08:49+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-PROBE"
msgstr "GRUB-PROBE"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2023"
msgstr "iulie 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12rc1-1"
msgstr "GRUB 2:2.12rc1-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "System Administration Utilities"
msgstr "Utilitare de administrare a sistemului"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-probe - probe device information for GRUB"
msgstr "grub-probe - verifică informațiile despre dispozitiv pentru GRUB"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"B<grub-probe> [I<\\,OPTION\\/>...] [I<\\,OPTION\\/>]... [I<\\,PATH|DEVICE\\/"
">]"
msgstr ""
"B<grub-probe> [I<\\,OPȚIUNE\\/>...] [I<\\,OPȚIUNE\\/>]... [I<\\,RUTA|"
"DISPOZITIV\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Probe device information for a given path (or device, if the B<-d> option is "
"given)."
msgstr ""
"Verifică informația dispozitivului pentru o rută dată (sau dispozitiv, dacă "
"este dată opțiunea „-d”)."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-0>"
msgstr "B<-0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "separate items in output using ASCII NUL characters"
msgstr "separă elemente de la ieșire utilizând caractere ASCII NUL"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--device>"
msgstr "B<-d>, B<--device>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "given argument is a system device, not a path"
msgstr "argumentul dat este un dispozitiv de sistem, nu o rută"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-m>, B<--device-map>=I<\\,FILE\\/>"
msgstr "B<-m>, B<--device-map>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux
msgid "use FILE as the device map [default=//boot/grub/device.map]"
msgstr ""
"utilizează FIȘIERUL ca hartă de dispozitive [default=/boot/grub/device.map]"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-t>, B<--target>=I<\\,TARGET\\/>"
msgstr "B<-t>,B<--target>=I<\\,OBIECTIV_ȚINTĂ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"print TARGET available targets: abstraction, arc_hints, baremetal_hints, "
"bios_hints, compatibility_hint, cryptodisk_uuid, device, disk, drive, "
"efi_hints, fs, fs_label, fs_uuid, gpt_parttype, hints_string, "
"ieee1275_hints, msdos_parttype, partmap, partuuid, zero_check [default=fs]"
msgstr ""
"afișează OBIECTIVUL_ȚINTĂ; obiective_țintă disponibile: abstraction, "
"arc_hints, baremetal_hints, bios_hints, compatibility_hint, cryptodisk_uuid, "
"device, disk, drive, efi_hints, fs, fs_label, fs_uuid, gpt_parttype, "
"hints_string, ieee1275_hints, msdos_parttype, partmap, partuuid, zero_check "
"[default=fs]"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-unstable
msgid "print verbose messages (pass twice to enable debug printing)."
msgstr ""
"afișează mesaje informative detaliate (treceți de două ori pentru a activa "
"afișarea de depanare)."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "oferă această listă de ajutor"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "oferă un mesaj de utilizare scurt"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "afișează versiunea programului"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Argumentele obligatorii sau opționale pentru opțiunile lungi sunt "
"obligatorii sau opționale și pentru opțiunile corespunzătoare scurte."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Raportați erorile la E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-fstest>(1)"
msgstr "B<grub-fstest>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-probe> is maintained as a Texinfo manual.  "
"If the B<info> and B<grub-probe> programs are properly installed at your "
"site, the command"
msgstr ""
"Documentația completă pentru B<grub-probe> este menținută ca un manual "
"Texinfo.  Dacă programele B<info> și B<grub-probe> sunt instalate corect în "
"sistemul dvs., comanda"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-probe>"
msgstr "B<info grub-probe>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "April 2023"
msgstr "aprilie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13"
msgstr "GRUB 2.06-13"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use FILE as the device map [default=/boot/grub/device.map]"
msgstr ""
"utilizează FIȘIERUL ca hartă de dispozitive [default=/boot/grub/device.map]"

#. type: Plain text
#: debian-bookworm
msgid "print verbose messages."
msgstr "afișează mesaje detaliate."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "September 2023"
msgstr "septembrie 2023"

#. type: TH
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "GRUB 2:2.12rc1-1"
msgid "GRUB 2.12~rc1-10"
msgstr "GRUB 2:2.12rc1-1"
