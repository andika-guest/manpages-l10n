# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2022-10-22 09:21+0200\n"
"PO-Revision-Date: 2023-07-16 19:51+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "FIPS-MODE-SETUP"
msgstr "FIPS-MODE-SETUP"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "08/24/2019"
msgstr "24 august 2019"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "fips-mode-setup"
msgstr "fips-mode-setup"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "\\ \""
msgstr "\\ \""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-unstable
msgid "fips-mode-setup - Check, enable, or disable the system FIPS mode\\&."
msgstr ""
"fips-mode-setup - verifică, activează sau dezactivează modul FIPS al "
"sistemului\\&."

#. type: SH
#: debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: debian-unstable
msgid "B<fips-mode-setup> [I<COMMAND>]"
msgstr "B<fips-mode-setup> [I<COMANDA>]"

#. type: SH
#: debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-unstable
msgid ""
"fips-mode-setup(8) is used to check and control the system FIPS mode\\&."
msgstr ""
"fips-mode-setup(8) este utilizat pentru a verifica și controla modul FIPS al "
"sistemului\\&."

#. type: Plain text
#: debian-unstable
msgid ""
"When enabling the system FIPS mode the command completes the installation of "
"FIPS modules if needed by calling I<fips-finish-install> and changes the "
"system crypto policy to FIPS\\&."
msgstr ""
"Atunci când se activează modul FIPS al sistemului, comanda finalizează "
"instalarea modulelor FIPS, dacă este necesar, prin apelarea I<fips-finish-"
"install> și schimbă politica de criptare a sistemului în FIPS\\&."

#. type: Plain text
#: debian-unstable
msgid ""
"Then the command modifies the boot loader configuration to add I<fips=1> and "
"I<boot=E<lt>boot-deviceE<gt>> options to the kernel command line\\&."
msgstr ""
"Apoi, comanda modifică configurația încărcătorului de pornire pentru a "
"adăuga opțiunile I<fips=1> și I<boot=E<lt>boot-deviceE<gt>> la linia de "
"comandă a nucleului"

#. type: Plain text
#: debian-unstable
msgid ""
"When disabling the system FIPS mode the system crypto policy is switched to "
"DEFAULT and the kernel command line option I<fips=0> is set\\&."
msgstr ""
"Atunci când se dezactivează modul FIPS al sistemului, politica de criptare a "
"sistemului este comutată la DEFAULT, iar opțiunea de linie de comandă a "
"nucleului I<fips=0> este stabilită\\&."

#. type: SH
#: debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: debian-unstable
msgid "The following options are available in fips-mode-setup tool\\&."
msgstr ""
"Următoarele opțiuni sunt disponibile în instrumentul «fips-mode-setup»:"

#. type: Plain text
#: debian-unstable
msgid "--enable: Enables the system FIPS mode\\&."
msgstr "--enable: Activează modul FIPS al sistemului\\&."

#. type: Plain text
#: debian-unstable
msgid "--disable: Disables the system FIPS mode\\&."
msgstr "--disable: Dezactivează modul FIPS al sistemului\\&."

#. type: Plain text
#: debian-unstable
msgid "--check: Checks the system FIPS mode status\\&."
msgstr "--check: Verifică starea modului FIPS al sistemului\\&."

#. type: Plain text
#: debian-unstable
msgid ""
"--is-enabled: Checks the system FIPS mode status and returns failure error "
"code if disabled (2) or inconsistent (1)\\&."
msgstr ""
"--is-enabled: Verifică starea modului FIPS al sistemului și returnează un "
"cod de eroare de eșec dacă este dezactivat (2) sau inconsistent (1)\\&."

#. type: Plain text
#: debian-unstable
msgid ""
"--no-bootcfg: The tool will not attempt to change the boot loader "
"configuration and it just prints the options that need to be added to the "
"kernel command line\\&."
msgstr ""
"--no-bootcfg: Instrumentul nu va încerca să schimbe configurația "
"încărcătorului de pornire și doar afișează opțiunile care trebuie adăugate "
"la linia de comandă a nucleului\\&."

#. type: SH
#: debian-unstable
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: Plain text
#: debian-unstable
msgid "/proc/sys/crypto/fips_enabled"
msgstr "/proc/sys/crypto/fips_enabled"

#. type: Plain text
#: debian-unstable
msgid "The kernel FIPS mode flag\\&."
msgstr "Indicatorul modului FIPS al nucleului\\&."

#. type: SH
#: debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-unstable
msgid "update-crypto-policies(8), fips-finish-install(8)"
msgstr "update-crypto-policies(8), fips-finish-install(8)"

#. type: SH
#: debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-unstable
msgid "Written by Tomáš Mráz\\&."
msgstr "Scris de Tomáš Mráz\\&."
