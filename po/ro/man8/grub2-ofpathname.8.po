# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2023-10-02 12:02+0200\n"
"PO-Revision-Date: 2023-05-16 16:38+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "GRUB-OFPATHNAME"
msgstr "GRUB-OFPATHNAME"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "September 2023"
msgstr "septembrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "System Administration Utilities"
msgstr "Utilitare de administrare a sistemului"

#. type: SH
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "grub-ofpathname - find OpenBOOT path for a device"
msgstr "grub-ofpathname - găsește ruta OpenBOOT pentru un dispozitiv"

#. type: SH
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<grub-ofpathname> I<\\,DEVICE\\/>"
msgstr "B<grub-ofpathname> I<\\,DISPOZITIV\\/>"

#. type: SH
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: SH
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<grub-probe>(8)"
msgstr "B<grub-probe>(8)"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-ofpathname> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-ofpathname> programs are properly "
"installed at your site, the command"
msgstr ""
"Documentația completă pentru B<grub-ofpathname> este menținută ca un manual "
"Texinfo.  Dacă programele B<info> și B<grub-ofpathname> sunt instalate "
"corect în sistemul dvs., comanda"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<info grub-ofpathname>"
msgstr "B<info grub-ofpathname>"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "May 2023"
msgstr "mai 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "grub-ofpathname 2.06"
msgstr "grub-ofpathname 2.06"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "August 2023"
msgstr "august 2023"

#. type: TH
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "GRUB 2:2.12rc1-1"
msgid "GRUB2 2.12~rc1"
msgstr "GRUB 2:2.12rc1-1"
