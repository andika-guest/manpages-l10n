# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-08-27 17:29+0200\n"
"PO-Revision-Date: 2023-09-09 23:55+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "time"
msgstr "time"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 martie 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pagini de manual de Linux 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "time - get time in seconds"
msgstr "time - obține timpul în secunde"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>time.hE<gt>>\n"
msgstr "B<#include E<lt>time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<time_t time(time_t *_Nullable >I<tloc>B<);>\n"
msgstr "B<time_t time(time_t *_Nullable >I<tloc>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<time>()  returns the time as the number of seconds since the Epoch, "
"1970-01-01 00:00:00 +0000 (UTC)."
msgstr ""
"B<time>() returnează timpul ca număr de secunde de la Epoca, 1970-01-01 "
"00:00:00:00 +0000 (UTC)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<tloc> is non-NULL, the return value is also stored in the memory "
"pointed to by I<tloc>."
msgstr ""
"Dacă I<tloc> nu este NULL, valoarea de returnare este, de asemenea, stocată "
"în memoria indicată de I<tloc>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, the value of time in seconds since the Epoch is returned.  On "
"error, I<((time_t)\\ -1)> is returned, and I<errno> is set to indicate the "
"error."
msgstr ""
"În caz de succes, se returnează valoarea timpului în secunde de la Epoca "
"(Epoch).  În caz de eroare, se returnează I<((time_t)\\ -1)>, iar I<errno> "
"este configurată pentru a indica eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<tloc> points outside your accessible address space (but see BUGS)."
msgstr ""
"I<tloc> indică în afara spațiului de adrese accesibil (dar consultați ERORI)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On systems where the C library B<time>()  wrapper function invokes an "
"implementation provided by the B<vdso>(7)  (so that there is no trap into "
"the kernel), an invalid address may instead trigger a B<SIGSEGV> signal."
msgstr ""
"Pe sistemele în care funcția de învăluire a bibliotecii C B<time>() invocă o "
"implementare furnizată de B<vdso>(7) (astfel încât să nu existe o captură în "
"nucleu), o adresă nevalidă poate declanșa în schimb un semnal B<SIGSEGV>."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX.1 defines I<seconds since the Epoch> using a formula that approximates "
"the number of seconds between a specified time and the Epoch.  This formula "
"takes account of the facts that all years that are evenly divisible by 4 are "
"leap years, but years that are evenly divisible by 100 are not leap years "
"unless they are also evenly divisible by 400, in which case they are leap "
"years.  This value is not the same as the actual number of seconds between "
"the time and the Epoch, because of leap seconds and because system clocks "
"are not required to be synchronized to a standard reference.  The intention "
"is that the interpretation of seconds since the Epoch values be consistent; "
"see POSIX.1-2008 Rationale A.4.15 for further rationale."
msgstr ""
"POSIX.1 definește I<secunde de la Epoca> folosind o formulă care aproximează "
"numărul de secunde dintre un moment specificat și Epoca.  Această formulă ia "
"în considerare faptul că toți anii care sunt divizibili în mod egal cu 4 "
"sunt ani bisecți, dar anii care sunt divizibili în mod egal cu 100 nu sunt "
"ani bisecți decât dacă sunt, de asemenea, divizibili în mod egal cu 400, caz "
"în care sunt ani bisecți.  Această valoare nu este identică cu numărul real "
"de secunde dintre oră și Epoca, din cauza secundelor bisecte și a faptului "
"că ceasurile de sistem nu trebuie să fie sincronizate cu o referință "
"standard.  Intenția este ca interpretarea secundelor de la valorile Epocii "
"să fie consecventă; a se vedea POSIX.1-2008 Rationale A.4.15 pentru o "
"justificare suplimentară."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On Linux, a call to B<time>()  with I<tloc> specified as NULL cannot fail "
"with the error B<EOVERFLOW>, even on ABIs where I<time_t> is a signed 32-bit "
"integer and the clock reaches or exceeds 2**31 seconds (2038-01-19 03:14:08 "
"UTC, ignoring leap seconds).  (POSIX.1 permits, but does not require, the "
"B<EOVERFLOW> error in the case where the seconds since the Epoch will not "
"fit in I<time_t>.)  Instead, the behavior on Linux is undefined when the "
"system time is out of the I<time_t> range.  Applications intended to run "
"after 2038 should use ABIs with I<time_t> wider than 32 bits."
msgstr ""
"În Linux, un apel la B<time>() cu I<tloc> specificat ca fiind NULL nu poate "
"eșua cu eroarea B<EOVERFLOW>, chiar și în cazul ABI-urilor în care I<time_t> "
"este un număr întreg pe 32 de biți cu semn și ceasul atinge sau depășește "
"2**31 secunde (2038-01-19 03:14:08 UTC, ignorând secundele bisecte).  "
"(POSIX.1 permite, dar nu necesită, eroarea B<EOVERFLOW> în cazul în care "
"secundele de la Epoca nu încap în I<time_t>).  În schimb, comportamentul în "
"Linux este nedefinit atunci când ora sistemului este în afara intervalului "
"I<time_t>.  Aplicațiile destinate să ruleze după 2038 ar trebui să utilizeze "
"ABI-uri cu I<time_t> mai largi de 32 de biți."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "diferențe între biblioteca C și nucleu"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On some architectures, an implementation of B<time>()  is provided in the "
"B<vdso>(7)."
msgstr ""
"Pe unele arhitecturi, o implementare a B<time>() este furnizată în "
"B<vdso>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. Under 4.3BSD, this call is obsoleted by
#. .BR gettimeofday (2).
#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "SVr4, 4.3BSD, C89, POSIX.1-2001."
msgstr "SVr4, 4.3BSD, C89, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Error returns from this system call are indistinguishable from successful "
"reports that the time is a few seconds I<before> the Epoch, so the C library "
"wrapper function never sets I<errno> as a result of this call."
msgstr ""
"Returnările de eroare de la acest apel de sistem nu pot fi deosebite de "
"rapoartele de succes care indică faptul că timpul este cu câteva secunde "
"I<înainte> de Epoca, astfel încât funcția de învăluire a bibliotecii C nu "
"configurează niciodată I<errno> ca rezultat al acestui apel."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<tloc> argument is obsolescent and should always be NULL in new code.  "
"When I<tloc> is NULL, the call cannot fail."
msgstr ""
"Argumentul I<tloc> este învechit și ar trebui să fie întotdeauna NULL în "
"codul nou.  Atunci când I<tloc> este NULL, apelul nu poate eșua."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<date>(1), B<gettimeofday>(2), B<ctime>(3), B<ftime>(3), B<time>(7), "
"B<vdso>(7)"
msgstr ""
"B<date>(1), B<gettimeofday>(2), B<ctime>(3), B<ftime>(3), B<time>(7), "
"B<vdso>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-29"
msgstr "29 decembrie 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. Under 4.3BSD, this call is obsoleted by
#. .BR gettimeofday (2).
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"SVr4, 4.3BSD, C99, POSIX.1-2001.  POSIX does not specify any error "
"conditions."
msgstr ""
"SVr4, 4.3BSD, C99, POSIX.1-2001.  POSIX nu specifică nicio condiție de "
"eroare."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Pagini de manual de Linux 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "TIME"
msgstr "TIME"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembrie 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manualul programatorului Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>time.hE<gt>>"
msgstr "B<#include E<lt>time.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<time_t time(time_t *>I<tloc>B<);>"
msgstr "B<time_t time(time_t *>I<tloc>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, the value of time in seconds since the Epoch is returned.  On "
"error, I<((time_t)\\ -1)> is returned, and I<errno> is set appropriately."
msgstr ""
"În caz de succes, se returnează valoarea timpului în secunde de la Epoca "
"(Epoch).  În caz de eroare, se returnează I<((time_t)\\ -1)>, iar I<errno> "
"este configurată în mod corespunzător."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "ÎN CONFORMITATE CU"

#. Under 4.3BSD, this call is obsoleted by
#. .BR gettimeofday (2).
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"SVr4, 4.3BSD, C89, C99, POSIX.1-2001.  POSIX does not specify any error "
"conditions."
msgstr ""
"SVr4, 4.3BSD, C89, C99, POSIX.1-2001.  POSIX nu specifică nicio condiție de "
"eroare."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On Linux, a call to B<time>()  with I<tloc> specified as NULL cannot fail "
"with the error B<EOVERFLOW>, even on ABIs where I<time_t> is a signed 32-bit "
"integer and the clock ticks past the time 2**31 (2038-01-19 03:14:08 UTC, "
"ignoring leap seconds).  (POSIX.1 permits, but does not require, the "
"B<EOVERFLOW> error in the case where the seconds since the Epoch will not "
"fit in I<time_t>.)  Instead, the behavior on Linux is undefined when the "
"system time is out of the I<time_t> range.  Applications intended to run "
"after 2038 should use ABIs with I<time_t> wider than 32 bits."
msgstr ""
"În Linux, un apel la B<time>() cu I<tloc> specificat ca fiind NULL nu poate "
"eșua cu eroarea B<EOVERFLOW>, chiar și în cazul ABI-urilor în care I<time_t> "
"este un întreg pe 32 de biți cu semn și ceasul trece de ora 2**31 "
"(2038-01-19 03:14:08 UTC, ignorând secundele bisecte).  (POSIX.1 permite, "
"dar nu necesită, eroarea B<EOVERFLOW> în cazul în care secundele de la Epoca "
"nu încap în I<time_t>).  În schimb, comportamentul în Linux este nedefinit "
"atunci când ora sistemului este în afara intervalului I<time_t>.  "
"Aplicațiile destinate să ruleze după 2038 ar trebui să utilizeze ABI-uri cu "
"I<time_t> mai largi de 32 de biți."

# R-GC, scrie:
# COLOPHON (în eng.) = COLOFON (în rom.)
# *****
# Colofon = Notă la sfârșitul unei publicații sau
# pe verso foii de titlu, cuprinzând datele editoriale.
# sau:
# Colofon =  Însemnare la sfârșitul unei cărți în
# epoca manuscriselor și incunabulelor, cuprinzând
# date privind tipograful, locul unde a lucrat,
# autorul și titlul lucrării.
# sau:
# Colofon = Notă, însemnare finală a unei cărți,
# care reproduce sau completează cele spuse în titlu.
# ===============
# M-am decis pentru:
# COLOFON -- NOTĂ FINALĂ
#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON -- NOTĂ FINALĂ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Această pagină face parte din versiunea 4.16 a proiectului Linux I<man-"
"pages>.  O descriere a proiectului, informații despre raportarea erorilor și "
"cea mai recentă versiune a acestei pagini pot fi găsite la \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"
