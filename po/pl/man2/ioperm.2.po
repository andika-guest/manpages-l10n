# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:03+0200\n"
"PO-Revision-Date: 2002-12-12 10:12+0100\n"
"Last-Translator: Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 19.08.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ioperm"
msgstr ""

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 marca 2023 r."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ioperm - set port input/output permissions"
msgstr "ioperm - ustawienie uprawnień dla portu wejścia/wyjścia"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/io.hE<gt>>\n"
msgstr "B<#include E<lt>sys/io.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int ioperm(unsigned long >I<from>B<, unsigned long >I<num>B<, int >I<turn_on>B<);>\n"
msgstr "B<int ioperm(unsigned long >I<from>B<, unsigned long >I<num>B<, int >I<turn_on>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<Ioperm> sets the port access permission bits for the process for I<num> "
#| "bytes starting from port address B<from> to the value B<turn_on>.  The "
#| "use of B<ioperm> requires root privileges."
msgid ""
"B<ioperm>()  sets the port access permission bits for the calling thread for "
"I<num> bits starting from port address I<from>.  If I<turn_on> is nonzero, "
"then permission for the specified bits is enabled; otherwise it is "
"disabled.  If I<turn_on> is nonzero, the calling thread must be privileged "
"(B<CAP_SYS_RAWIO>)."
msgstr ""
"B<Ioperm> ustawia bity dostępu do portów dla procesu dla I<num> bajtów, "
"poczynając od adresu portu B<from> do wartości B<turn_on>. Użycie B<ioperm> "
"wymaga uprawnień roota."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Before Linux 2.6.8, only the first 0x3ff I/O ports could be specified in "
"this manner.  For more ports, the B<iopl>(2)  system call had to be used "
"(with a I<level> argument of 3).  Since Linux 2.6.8, 65,536 I/O ports can be "
"specified."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Permissions are inherited by the child created by B<fork>(2)  (but see "
"NOTES).  Permissions are preserved across B<execve>(2); this is useful for "
"giving port access permissions to unprivileged programs."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This call is mostly for the i386 architecture.  On many other architectures "
"it does not exist or will always return an error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"Po pomyślnym zakończeniu zwracane jest zero. Po błędzie zwracane jest -1 i "
"ustawiane jest I<errno> wskazując błąd."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Invalid values for I<from> or I<num>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "(on PowerPC) This call is not supported."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#.  Could not allocate I/O bitmap.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Out of memory."
msgstr "Brak pamięci."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The calling thread has insufficient privilege."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "WERSJE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Libc5 treats it as a system call and has a prototype in I<E<lt>unistd."
#| "hE<gt>>.  Glibc1 does not have a prototype. Glibc2 has a prototype both "
#| "in I<E<lt>sys/io.hE<gt>> and in I<E<lt>sys/perm.hE<gt>>.  Avoid the "
#| "latter, it is available on i386 only."
msgid ""
"glibc has an B<ioperm>()  prototype both in I<E<lt>sys/io.hE<gt>> and in "
"I<E<lt>sys/perm.hE<gt>>.  Avoid the latter, it is available on i386 only."
msgstr ""
"Libc5 traktuje to jak wywołanie systemowe i posiada dla niego prototyp w "
"I<E<lt>unistd.hE<gt>>. Glibc1 nie posiada prototypu. Glibc2 posiada prototyp "
"zarówno w I<E<lt>sys/io.hE<gt>>, jak i w I<E<lt>sys/perm.hE<gt>>. Należy "
"unikać tego ostatniego, gdyż jest dostępne tylko na i386."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

# All that section was initially translated by PB. Readded with minor corrections by MK. --MK
#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Before Linux 2.4, permissions were not inherited by a child created by "
"B<fork>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I</proc/ioports> file shows the I/O ports that are currently allocated "
"on the system."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<iopl>(2), B<outb>(2), B<capabilities>(7)"
msgstr "B<iopl>(2), B<outb>(2), B<capabilities>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<ioperm>()  is Linux-specific and should not be used in programs intended "
"to be portable."
msgstr ""
"B<ioperm>() jest specyficzne dla Linuksa i nie powinno być używane w "
"przenośnych programach."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "IOPERM"
msgstr "IOPERM"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/io.hE<gt>> /* for glibc */"
msgstr "B<#include E<lt>sys/io.hE<gt>> /* dla glibc */"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<int ioperm(unsigned long >I<from>B<, unsigned long >I<num>B<, int "
">I<turn_on>B<);>"
msgstr ""
"B<int ioperm(unsigned long >I<from>B<, unsigned long >I<num>B<, int "
">I<turn_on>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"Po pomyślnym zakończeniu zwracane jest zero. Po błędzie zwracane jest -1 i "
"odpowiednio ustawiane jest I<errno>."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Libc5 treats it as a system call and has a prototype in I<E<lt>unistd."
#| "hE<gt>>.  Glibc1 does not have a prototype. Glibc2 has a prototype both "
#| "in I<E<lt>sys/io.hE<gt>> and in I<E<lt>sys/perm.hE<gt>>.  Avoid the "
#| "latter, it is available on i386 only."
msgid ""
"Glibc has an B<ioperm>()  prototype both in I<E<lt>sys/io.hE<gt>> and in "
"I<E<lt>sys/perm.hE<gt>>.  Avoid the latter, it is available on i386 only."
msgstr ""
"Libc5 traktuje to jak wywołanie systemowe i posiada dla niego prototyp w "
"I<E<lt>unistd.hE<gt>>. Glibc1 nie posiada prototypu. Glibc2 posiada prototyp "
"zarówno w I<E<lt>sys/io.hE<gt>>, jak i w I<E<lt>sys/perm.hE<gt>>. Należy "
"unikać tego ostatniego, gdyż jest dostępne tylko na i386."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
