# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1999.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:00+0200\n"
"PO-Revision-Date: 2002-12-12 12:04+0100\n"
"Last-Translator: Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 19.08.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getsockname>(2)"
msgid "getsockname"
msgstr "B<getsockname>(2)"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-04-03"
msgstr "3 kwietnia 2023 r."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getsockname - get socket name"
msgstr "getsockname - pobranie nazwy gniazda"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr "B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getsockname(int >I<sockfd>B<, struct sockaddr *restrict >I<addr>B<,>\n"
"B<                socklen_t *restrict >I<addrlen>B<);>\n"
msgstr ""
"B<int getsockname(int >I<sockfd>B<, struct sockaddr *restrict >I<addr>B<,>\n"
"B<                socklen_t *restrict >I<addrlen>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<Getsockname> returns the current I<name> for the specified socket.  The "
#| "I<namelen> parameter should be initialized to indicate the amount of "
#| "space pointed to by I<name>.  On return it contains the actual size of "
#| "the name returned (in bytes)."
msgid ""
"B<getsockname>()  returns the current address to which the socket I<sockfd> "
"is bound, in the buffer pointed to by I<addr>.  The I<addrlen> argument "
"should be initialized to indicate the amount of space (in bytes) pointed to "
"by I<addr>.  On return it contains the actual size of the socket address."
msgstr ""
"B<getsockname> zwraca bieżącą nazwę I<name> dla zadanego gniazda. Parametr "
"I<namelen> powinien być zainicjalizowany tak, aby podawał rozmiar obszaru "
"wskazywanego przez I<name>. Po zakończeniu, zawiera on rzeczywisty rozmiar "
"zwróconej nazwy (w bajtach)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The returned address is truncated if the buffer provided is too small; in "
"this case, I<addrlen> will return a value greater than was supplied to the "
"call."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"Po pomyślnym zakończeniu zwracane jest zero. Po błędzie zwracane jest -1 i "
"ustawiane jest I<errno> wskazując błąd."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The argument I<sockfd> is not a valid file descriptor."
msgstr "Argument I<sockfd> nie jest prawidłowym deskryptorem."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<addr> argument points to memory not in a valid part of the process "
"address space."
msgstr "Parametr I<addr> wskazuje poza dostępną przestrzeń adresową procesu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<addrlen> is invalid (e.g., is negative)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOBUFS>"
msgstr "B<ENOBUFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Insufficient resources were available in the system to perform the operation."
msgstr ""
"Dostępna ilość zasobów systemowych jest niewystarczająca dla wykonania "
"operacji."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTSOCK>"
msgstr "B<ENOTSOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The file descriptor I<sockfd> does not refer to a socket."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

# All that section was initially translated by PB. Readded with minor corrections by MK. --MK
#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#.  SVr4 documents additional ENOMEM
#.  and ENOSR error codes.
#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgid "POSIX.1-2001, SVr4, 4.4BSD (first appeared in 4.2BSD)."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<bind>(2), B<socket>(2), B<getifaddrs>(3), B<ip>(7), B<socket>(7), "
"B<unix>(7)"
msgstr ""
"B<bind>(2), B<socket>(2), B<getifaddrs>(3), B<ip>(7), B<socket>(7), "
"B<unix>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "30 października 2022 r."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#.  SVr4 documents additional ENOMEM
#.  and ENOSR error codes.
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (B<getsockname>()  first appeared "
"in 4.2BSD)."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "For background on the I<socklen_t> type, see B<accept>(2)."
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GETSOCKNAME"
msgstr "GETSOCKNAME"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int getsockname(int >I<sockfd>B<, struct sockaddr *>I<addr>B<, socklen_t *>I<addrlen>B<);>\n"
msgstr "B<int getsockname(int >I<sockfd>B<, struct sockaddr *>I<addr>B<, socklen_t *>I<addrlen>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"Po pomyślnym zakończeniu zwracane jest zero. Po błędzie zwracane jest -1 i "
"odpowiednio ustawiane jest I<errno>."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
