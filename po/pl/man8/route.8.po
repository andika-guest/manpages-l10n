# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 2000.
# Michał Kułach <michal.kulach@gmail.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-08-10 19:08+0200\n"
"PO-Revision-Date: 2013-04-19 22:53+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.4\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ROUTE"
msgstr "ROUTE"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "2014-02-17"
msgstr "17 lutego 2014 r."

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "net-tools"
msgstr "net-tools"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Linux System Administrator's Manual"
msgstr "Podręcznik administratora systemu Linux"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "route - show / manipulate the IP routing table"
msgstr "route - pokazuje / obsługuje tablicę trasowania protokołu IP"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "B<route> [B<-CFvnNee>] [B<-A> family |B<-4>|B<-6>]"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<route>"
msgstr "B<route>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "[B<-v>] [B<-A> family] B<add> [B<-net>|B<-host>] target [B<netmask> Nm] "
#| "[B<gw> Gw] [B<metric> N] [B<mss> M] [B<window> W] [B<irtt> I] [B<reject>] "
#| "[B<mod>] [B<dyn>] [B<reinstate>] [[B<dev>] If]"
msgid ""
"[B<-v>] [B<-A> family |B<-4>|B<-6>] B<add> [B<-net>|B<-host>] I<target> "
"[B<netmask> I<Nm>] [B<gw> I<Gw>] [B<metric> I<N>] [B<mss> I<M>] [B<window> "
"I<W>] [B<irtt> I<I>] [B<reject>] [B<mod>] [B<dyn>] [B<reinstate>] [[B<dev>] "
"I<If>]"
msgstr ""
"[B<-v>] [B<-A> I<rodzina>] B<add> [B<-net>|B<-host>] I<cel> [B<netmask> "
"I<maska>] [B<gw> I<brama>] [B<metric> I<metryka>] [B<mss> I<rozmiar>] "
"[B<window> I<okno>] [B<irtt> I<czas>] [B<reject>] [B<mod>] [B<dyn>] "
"[B<reinstate>] [[B<dev>] I<interfejs>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "[B<-v>] [B<-A> family] B<del> [B<-net>|B<-host>] target [B<gw> Gw] "
#| "[B<netmask> Nm] [B<metric> N] [[B<dev>] If]"
msgid ""
"[B<-v>] [B<-A> I<family> |B<-4>|B<-6>] B<del> [B<-net>|B<-host>] I<target> "
"[B<gw> I<Gw>] [B<netmask> I<Nm>] [B<metric> I<M>] [[B<dev>] I<If>]"
msgstr ""
"[B<-v>] [B<-A> I<rodzina>] B<del> [B<-net>|B<-host>] I<cel> [B<gw> I<brama>] "
"[B<netmask> I<maska>] [B<metric> I<metryka>] [[B<dev>] I<urządzenie>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "[B<-V>] [B<--version>] [B<-h>] [B<--help>]"
msgstr "[B<-V>] [B<--version>] [B<-h>] [B<--help>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<Route> manipulates the kernel's IP routing tables.  Its primary use is to "
"set up static routes to specific hosts or networks via an interface after it "
"has been configured with the B<ifconfig>(8)  program."
msgstr ""
"B<Route> obsługuje jądrowe tablice trasowania protokołu IP. Jego podstawowym "
"zadaniem jest ustawianie B<statycznych> tras do określonych stacji lub sieci "
"poprzez interfejs, skonfigurowany programem B<ifconfig>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"When the B<add> or B<del> options are used, B<route> modifies the routing "
"tables.  Without these options, B<route> displays the current contents of "
"the routing tables."
msgstr ""
"Jeśli wykorzystywane są opcje B<add> lub B<del>, B<route> modyfikuje tablice "
"trasowań. Bez tych opcji program wyświetla obecną zawartość tablic "
"trasowania."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-A >I<family>"
msgstr "B<-A >I<rodzina>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "use the specified address family (eg `inet'; use `route --help' for a "
#| "full list)."
msgid ""
"use the specified address family (eg `inet'). Use B<route --help> for a full "
"list. You can use B<-6> as an alias for B<--inet6> and B<-4> as an alias for "
"B<-A inet>"
msgstr ""
"używa podanej rodziny adresów (np. \"inet\"; polecenie \"route --help\" "
"wyświetli pełną listę)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-F>"
msgstr "B<-F>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"operate on the kernel's FIB (Forwarding Information Base) routing table.  "
"This is the default."
msgstr ""
"działa na jądrowej tablicy trasowania FIB (Forwarding Information Base: Baza "
"Informacji Przerzutowych). Jest to opcja domyślna."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-C>"
msgstr "B<-C>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "operate on the kernel's routing cache."
msgstr "działa na jądrowym buforze podręcznym trasowania"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "select verbose operation."
msgstr "wybiera szczegółowe wypisywanie informacji."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-n>"
msgstr "B<-n>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"show numerical addresses instead of trying to determine symbolic host names. "
"This is useful if you are trying to determine why the route to your "
"nameserver has vanished."
msgstr ""
"pokazuje adresy numeryczne zamiast nazw. Jest to przydatne jeśli próbuje się "
"określić dlaczego trasa do serwera nazw wyparowała."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-e>"
msgstr "B<-e>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"use B<netstat>(8)-format for displaying the routing table.  B<-ee> will "
"generate a very long line with all parameters from the routing table."
msgstr ""
"używa formatu B<netstat>(8)  do wyświetlania tablicy trasowania.  B<-ee> "
"generuje bardzo długie wiersze ze wszystkimi parametrami z tablicy "
"trasowania."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<del>"
msgstr "B<del>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "delete a route."
msgstr "kasuje trasę."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<add>"
msgstr "B<add>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "add a new route."
msgstr "dodaje nową trasę."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "I<target>"
msgstr "I<cel>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"the destination network or host. You can provide an addresses or symbolic "
"network or host name. Optionally you can use B</>I<prefixlen> notation "
"instead of using the B<netmask> option."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-net>"
msgstr "B<-net>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "the I<target> is a network."
msgstr "oznacza, że I<cel> jest siecią."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-host>"
msgstr "B<-host>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "the I<target> is a host."
msgstr "oznacza, że I<cel> jest stacją."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<netmask >I<NM>"
msgstr "B<netmask >I<maska>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "when adding a network route, the netmask to be used."
msgstr "podczas dodawania trasy sieciowej używana jest maska-sieci."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<gw >I<GW>"
msgstr "B<gw >I<brama>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "route packets via a gateway."
msgstr "trasuje pakiety poprzez bramkę (gateway)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<NOTE:> The specified gateway must be reachable first. This usually means "
"that you have to set up a static route to the gateway beforehand. If you "
"specify the address of one of your local interfaces, it will be used to "
"decide about the interface to which the packets should be routed to. This is "
"a BSDism compatibility hack."
msgstr ""
"B<UWAGA:> Podana bramka musi być wpierw osiągalna. Oznacza to zwykle, że "
"trzeba mieć ustawioną do niej wcześniej statyczną trasę. Jeśli poda się "
"adres jednego ze swoich lokalnych interfejsów, to zostanie on użyty do "
"zdecydowania o interfejsie, do którego trasować pakiety. Jest to "
"kompatybilność w kierunku BSD."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<metric >I<M>"
msgstr "B<metric >I<metryka>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"set the metric field in the routing table (used by routing daemons) to I<M>. "
"If this option is not specified the metric for inet6 (IPv6) address family "
"defaults to '1', for inet (IPv4) it defaults to '0'. You should always "
"specify an explicit metric value to not rely on those defaults - they also "
"differ from iproute2."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<mss >I<M>"
msgstr "B<mss >I<rozmiar>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"sets MTU (Maximum Transmission Unit) of the route to I<M> bytes.  Note that "
"the current implementation of the route command does not allow the option to "
"set the Maximum Segment Size (MSS)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<window >I<W>"
msgstr "B<window >I<okno>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"set the TCP window size for connections over this route to I<W> bytes. This "
"is typically only used on AX.25 networks and with drivers unable to handle "
"back to back frames."
msgstr ""
"ustawia dla połączeń tej trasy rozmiar okna TCP na I<okno> bajtów. Jest to "
"zwykle używane jedynie w sieciach AX.25 i ze sterownikami, nie potrafiącymi "
"obsługiwać ramek back to back."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<irtt >I<I>"
msgstr "B<irtt >I<czas>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"set the initial round trip time (irtt) for TCP connections over this route "
"to I<I> milliseconds (1-12000). This is typically only used on AX.25 "
"networks. If omitted the RFC 1122 default of 300ms is used."
msgstr ""
"ustawia początkowy czas przelotu (initial round trip time, irtt) dla "
"połączeń TCP na tej trasie na I<czas> milisekund (1-12000). Jest to zwykle "
"wykorzystywane jedynie w sieciach AX.25. Domyślnie przyjmuje się wartość z "
"RFC 1122, 300ms."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<reject>"
msgstr "B<reject>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"install a blocking route, which will force a route lookup to fail.  This is "
"for example used to mask out networks before using the default route. This "
"is NOT for firewalling."
msgstr ""
"instaluje trasę blokującą, która wymusza niepowodzenie podejrzenia trasy.  "
"Jest to używane na przykład do maskowania sieci przed używaniem trasy "
"domyślnej. Nie jest to zapora ogniowa."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<mod, dyn, reinstate>"
msgstr "B<mod>, B<dyn>, B<reinstate>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"install a dynamic or modified route. These flags are for diagnostic "
"purposes, and are generally only set by routing daemons."
msgstr ""
"instaluje trasę dynamiczną lub zmodyfikowaną. Flagi te są przeznaczone dla "
"celów diagnostycznych i ogólnie są ustawiane tylko przez demony trasowania."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<dev >I<If>"
msgstr "B<dev >I<urządzenie>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"force the route to be associated with the specified device, as the kernel "
"will otherwise try to determine the device on its own (by checking already "
"existing routes and device specifications, and where the route is added to). "
"In most normal networks you won't need this."
msgstr ""
"wymusza związanie trasy z podanym urządzeniem. W przeciwnym przypadku, jądro "
"spróbuje określić urządzenie samodzielnie (sprawdzając istniejące już trasy "
"i specyfikacje urządzeń). W większości normalnych sieci nie potrzeba tego."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"If B<dev >I<If> is the last option on the command line, the word B<dev> may "
"be omitted, as it's the default. Otherwise the order of the route modifiers "
"(B<metric netmask gw dev>) doesn't matter."
msgstr ""
"Jeśli B<dev >I<urządzenie> jest ostatnią opcją linii poleceń, to słowo "
"B<dev> może zostać pominięte, gdyż jest przyjmowane za domyślne. W "
"przeciwnym przypadku, kolejność modyfikatorów trasy (B<metric netmask gw "
"dev>) nie ma znaczenia."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr "PRZYKŁADY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<route add -net 127.0.0.0 netmask 255.0.0.0 metric 1024 dev lo>"
msgstr "B<route add -net 127.0.0.0 netmask 255.0.0.0 metric 1024 dev lo>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"adds the normal loopback entry, using netmask 255.0.0.0 and associated with "
"the \"lo\" device (assuming this device was previously set up correctly with "
"B<ifconfig>(8))."
msgstr ""
"dodaje normalny wpis zapętlenia używający maski sieciowej 255.0.0.0, "
"związany z urządzeniem \"lo\" (zakładamy, że urządzenie to zostało wcześniej "
"poprawnie skonfigurowane programem B<ifconfig>(8))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<route add -net 192.56.76.0 netmask 255.255.255.0 metric 1024 dev eth0>"
msgstr "B<route add -net 192.56.76.0 netmask 255.255.255.0 metric 1024 dev eth0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"adds a route to the local network 192.56.76.x via \"eth0\".  The word "
"\"dev\" can be omitted here."
msgstr ""
"dodaje trasę do sieci 192.56.76.x poprzez \"eth0\". Słowo \"dev\" można "
"pominąć."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<route del default>"
msgstr "B<route del default>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"deletes the current default route, which is labeled \"default\" or 0.0.0.0 "
"in the destination field of the current routing table."
msgstr ""
"usuwa bieżącą trasę domyślną określoną etykietą \"default\" lub 0.0.0.0 w "
"polu docelowym bieżącej tabeli trasowania."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "B<route del -net 192.56.76.0 netmask 255.255.255.0>"
msgstr "B<route del -net 192.56.76.0 netmask 255.255.255.0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
msgid ""
"deletes the route. Since the Linux routing kernel uses classless addressing, "
"you pretty much always have to specify the netmask that is same as as seen "
"in 'route -n' listing."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<route add default gw mango>"
msgstr "B<route add default gw mango>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "adds a default route (which will be used if no other route matches).  All "
#| "packets using this route will be gatewayed through \"mango-gw\". The "
#| "device which will actually be used for that route depends on how we can "
#| "reach \"mango-gw\" - the static route to \"mango-gw\" will have to be set "
#| "up before."
msgid ""
"adds a default route (which will be used if no other route matches).  All "
"packets using this route will be gatewayed through the address of a node "
"named \"mango\". The device which will actually be used for that route "
"depends on how we can reach \"mango\" - \"mango\" must be on directly "
"reachable route."
msgstr ""
"dodaje domyślną trasę (która będzie używana gdy nie będzie pasować żadna "
"inna trasa). Wszystkie pakiety używające tej trasy będą bramkowane przez "
"\"mango-gw\". Urządzenie używane do tej trasy zależy od tego, jak osiągamy "
"\"mango-gw\" - wcześniej należy więc skonfigurować trasę do \"mango-gw\"."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<route add mango sl0>"
msgstr "B<route add mango sl0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "Adds the route to the \"ipx4\" host via the SLIP interface (assuming that "
#| "\"ipx4\" is the SLIP host)."
msgid ""
"Adds the route to the host named \"mango\" via the SLIP interface (assuming "
"that \"mango\" is the SLIP host)."
msgstr ""
"Dodaje trasę do stacji \"ipx4\" poprzez interfejs SLIP (zakładamy, że "
"\"ipx4\" jest stacją SLIP)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<route add -net 192.57.66.0 netmask 255.255.255.0 gw mango>"
msgstr "B<route add -net 192.57.66.0 netmask 255.255.255.0 gw mango>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"This command adds the net \"192.57.66.x\" to be gatewayed through the former "
"route to the SLIP interface."
msgstr ""
"Polecenie to dodaje sieć \"192.57.66.x\" do tras bramkowanych przez "
"poprzednią trasę przez interfejs SLIP."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<route add -net 224.0.0.0 netmask 240.0.0.0 dev eth0>"
msgstr "B<route add -net 224.0.0.0 netmask 240.0.0.0 dev eth0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"This is an obscure one documented so people know how to do it. This sets all "
"of the class D (multicast) IP routes to go via \"eth0\". This is the correct "
"normal configuration line with a multicasting kernel."
msgstr ""
"Jest to bardzo dobrze udokumentowane, więc wszystko powinno być jasne. "
"Ustawiane są wszystkie trasy IP klasy D (grupowe--multicast) na \"eth0\". "
"Jest to prawidłowa normalna linia konfiguracji dla jądra grupowującego."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<route add -net 10.0.0.0 netmask 255.0.0.0 metric 1024 reject>"
msgstr "B<route add -net 10.0.0.0 netmask 255.0.0.0 metric 1024 reject>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "This installs a rejecting route for the private network \"10.x.x.x.\""
msgstr "Instaluje to trasę odrzucającą dla sieci prywatnej \"10.x.x.x\"."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<route -6 add 2001:0002::/48 metric 1 dev eth0>"
msgstr "B<route -6 add 2001:0002::/48 metric 1 dev eth0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"This adds a IPv6 route with the specified metric to be directly reachable "
"via eth0."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "OUTPUT"
msgstr "WYJŚCIE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"The output of the kernel routing table is organized in the following columns"
msgstr ""
"Wyprowadzanie tablic trasowania jest organizowane w następujące kolumny"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<Destination>"
msgstr "B<Destination>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "The destination network or destination host."
msgstr "Sieć docelowa lub stacja docelowa."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<Gateway>"
msgstr "B<Gateway>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "The gateway address or '*' if none set."
msgstr "Adres bramki lub \"*\" gdy jej nie ma."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<Genmask>"
msgstr "B<Genmask>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"The netmask for the destination net; '255.255.255.255' for a host "
"destination and '0.0.0.0' for the B<default> route."
msgstr ""
"Maska sieci do sieci docelowej; \"255.255.255.255\" dla celu stacji i "
"\"0.0.0.0\" dla trasy B<domyślnej>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<Flags>"
msgstr "B<Flags>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "Possible flags include"
msgstr "Dopuszczalne flagi to"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "B<U> (route is B<up>)"
msgstr "B<U> (trasa jest zestawiona [B<up>])"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "B<H> (target is a B<host>)"
msgstr "B<H> (cel jest stacją [B<host>])"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "B<G> (use B<gateway>)"
msgstr "B<G> (używa bramki [B<gateway>])"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "B<R> (B<reinstate> route for dynamic routing)"
msgstr "B<R> (przywraca trasę na trasowanie dynamiczne [B<reinstate>])"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "B<D> (B<dynamically> installed by daemon or redirect)"
msgstr "B<D> (B<dynamicznie> instalowana przez demona lub przekierowanie)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "B<M> (B<modified> from routing daemon or redirect)"
msgstr "B<M> (B<modyfikowana> z demona trasowania lub przekierowania)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "B<A> (installed by B<addrconf>)"
msgstr "B<A> (instalowana przez B<addrconf>)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "B<C> (B<cache> entry)"
msgstr "B<C> (wpis bufora podręcznego [B<cache>])"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "B<!> (B<reject> route)"
msgstr "B<!> (trasa odrzucenia [B<reject>])"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<Metric>"
msgstr "B<Metric>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "The 'distance' to the target (usually counted in hops)."
msgstr "\"Odległość\" do celu (zwykle liczona w przeskokach)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<Ref>"
msgstr "B<Ref>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "Number of references to this route. (Not used in the Linux kernel.)"
msgstr "Liczba referencji do tej trasy. (Nie używane w jądrze Linux)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<Use>"
msgstr "B<Use>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"Count of lookups for the route.  Depending on the use of -F and -C this will "
"be either route cache misses (-F) or hits (-C)."
msgstr ""
"Liczba podglądnięć trasy (lookups). Zależnie od użycia -F i -C będą to "
"chybienia bufora podręcznego trasowania (-F) lub trafienia (-C)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<Iface>"
msgstr "B<Iface>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "Interface to which packets for this route will be sent."
msgstr "Interfejs, przez który przesyłane są pakiety tej trasy."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<MSS>"
msgstr "B<MSS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "Default maximum segment size for TCP connections over this route."
msgstr "Domyślny maksymalny rozmiar segmentu dla połączeń TCP na tej trasie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<Window>"
msgstr "B<Window>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "Default window size for TCP connections over this route."
msgstr "Domyślny rozmiar okna dla połączeń TCP na tej trasie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<irtt>"
msgstr "B<irtt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"Initial RTT (Round Trip Time). The kernel uses this to guess about the best "
"TCP protocol parameters without waiting on (possibly slow) answers."
msgstr ""
"Początkowy czas przelotu (RTT-Round Trip Time). Jądro używa tego do "
"zgadywania najlepszych parametrów protokołu TCP bez oczekiwania na (powolne) "
"odpowiedzi."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<HH (cached only)>"
msgstr "B<HH> (tylko buforowane)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"The number of ARP entries and cached routes that refer to the hardware "
"header cache for the cached route. This will be -1 if a hardware address is "
"not needed for the interface of the cached route (e.g. lo)."
msgstr ""
"Liczba wpisów ARP i tras buforowanych, odnoszących się do sprzętowego bufora "
"nagłówkowego (hardware header cache) buforowanej trasy. Będzie to -1 jeśli "
"adres sprzętowy nie jest wymagany dla interfejsu buforowanej trasy (np. lo)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<Arp (cached only)>"
msgstr "B<Arp> (tylko buforowane)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "Whether or not the hardware address for the cached route is up to date."
msgstr "Czy adres sprzętowy buforowanej trasy jest aktualny."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "I</proc/net/ipv6_route>"
msgstr "I</proc/net/ipv6_route>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "I</proc/net/route>"
msgstr "I</proc/net/route>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid "I</proc/net/rt_cache>"
msgstr "I</proc/net/rt_cache>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
msgid ""
"B<ethers>(5), B<arp>(8), B<rarp>(8), B<route>(8), B<ifconfig>(8), "
"B<netstat>(8)"
msgstr ""

# All that section was initially translated by PB. Readded with minor corrections by MK. --MK
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<Route> for Linux was originally written by Fred N.  van Kempen, "
"E<lt>waltje@uwalt.nl.mugnet.orgE<gt> and then modified by Johannes Stille "
"and Linus Torvalds for pl15. Alan Cox added the mss and window options for "
"Linux 1.1.22. irtt support and merged with netstat from Bernd Eckenfels."
msgstr ""
"B<Route> do Linuksa zostało napisane pierwotnie przez Freda N. van Kempena, "
"E<lt>waltje@uwalt.nl.mugnet.orgE<gt>, a następnie zostało zmodyfikowane "
"przez Johannesa Stille'a oraz Linusa Torvaldsa do pl15. Alan Cox dodał opcje "
"mss oraz window do Linuksa 1.1.22. Wsparcie irtt i połączenie z netstatem "
"pochodzi od Bernda Eckenfelsa."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron
msgid ""
"Currently maintained by Phil Blundell E<lt>Philip.Blundell@pobox.comE<gt> "
"and Bernd Eckenfels E<lt>net-tools@lina.inka.deE<gt>."
msgstr ""
"Obecnie opiekunem jest Phil Blundell E<lt>Philip.Blundell@pobox.comE<gt> i "
"Bernd Eckenfels E<lt>net-tools@lina.inka.deE<gt>."

#. type: SH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "NOTE"
msgstr "UWAGA"

#. type: Plain text
#: fedora-39 fedora-rawhide
msgid "This program is obsolete. For replacement check B<ip route>."
msgstr ""

#. type: Plain text
#: fedora-39 fedora-rawhide
msgid "I<ip>(8)"
msgstr "I<ip>(8)"
