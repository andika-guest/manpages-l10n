# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Paweł Wilk <siefca@pl.qmail.org>, 1999.
# Robert Luberda <robert@debian.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-08-27 17:21+0200\n"
"PO-Revision-Date: 2019-08-17 10:40+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SETLEDS"
msgstr "SETLEDS"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "24 Sep 1994"
msgstr "24 września 1994"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "kbd"
msgstr "kbd"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "setleds - set the keyboard leds"
msgstr "setleds - ustawia diody klawiatury"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<setleds> [I<-v>] [I<-L>] [I<-D>] [I<-F>] [I<{+|-}num>] [I<{+|-}caps>] "
"[I<{+|-}scroll>]"
msgstr ""
"B<setleds> [I<-v>] [I<-L>] [I<-D>] [I<-F>] [I<{+|-}num>] [I<{+|-}caps>] "
"[I<{+|-}scroll>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: IX
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "User Commands"
msgid "setleds command"
msgstr "Polecenia użytkownika"

#. type: IX
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "\\fLsetleds\\fR command"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"B<Setleds> reports and changes the led flag settings of a VT (namely "
"NumLock, CapsLock and ScrollLock).  Without arguments, B<setleds> prints the "
"current settings.  With arguments, it sets or clears the indicated flags "
"(and leaves the others unchanged). The settings before and after the change "
"are reported if the -v flag is given."
msgstr ""
"B<setleds> odczytuje i zmienia ustawienia światełek LED (zwanych NumLock, "
"CapsLock i ScrollLock) bieżącego VT (termianla wirtualnego). Bez argumentów "
"polecenie B<setleds> wypisuje obecne ustawienia. Z argumentami ustawia lub "
"wyłącza wskazane flagi (pozostawiając inne niezmienione). Ustawienia przed i "
"po zmianie są zgłaszane jeśli podano opcję -v."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The led flag settings are specific for each VT (and the VT corresponding to "
"stdin is used)."
msgstr ""
"Ustawienia flag did są specyficzne dla każdego VT (i używany jest VT "
"odpowiadający strumieniowi wejścia, czyli stdin)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"By default (or with option -F), B<setleds> will only change the VT flags "
"(and their setting may be reflected by the keyboard leds)."
msgstr ""
"B<setleds> domyślnie (lub z opcją -F) tylko zmienia flagi VT (i ich "
"ustawienie może wpłynąć na diody klawiatury)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"With option -D, B<setleds> will change both the VT flags and their default "
"settings (so that a subsequent reset will not undo the change).  This might "
"be useful for people who always want to have numlock set."
msgstr ""
"Użyty z opcją -D, B<setleds> zmieni obydwie flagi VT i ich domyślne "
"ustawienia (a więc kolejny reset nie odwoła zmiany). Może to być użyteczne "
"dla ludzi, którzy zawsze chcą mieć włączony numlock."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"With option -L, B<setleds> will not touch the VT flags, but only change the "
"leds.  From this moment on, the leds will no longer reflect the VT flags "
"(but display whatever is put into them). The command B<setleds -L> (without "
"further arguments) will restore the situation in which the leds reflect the "
"VT flags."
msgstr ""
"Użyty z opcją -L, B<setleds> nie rusza flag VT, a jedynie zmienia stan diod. "
"Od tej chwili diody nie będą oddawać rzeczywistego stanu flag VT (ale "
"zareagują na każdą zmianę).  Polecenie B<setleds -L> (bez kolejnych "
"argumentów) przywróci sytuację, w której diody oddają stan flag VT."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"One might use B<setleds> in /etc/rc to define the initial and default state "
"of NumLock, e.g. by"
msgstr ""
"Można użyć B<setleds> w /etc/rc, by zdefiniować początkowy i domyślny stan "
"NumLock, na przykład tak:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "INITTY=/dev/tty[1-8]"
msgstr "INITTY=/dev/tty[1-8]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "for tty in $INITTY; do"
msgstr "for tty in $INITTY; do"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "setleds -D +num E<lt> $tty"
msgstr "setleds -D +num E<lt> $tty"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "done"
msgstr "done"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-num +num"
msgstr "-num +num"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Clear or set NumLock.  (At present, the NumLock setting influences the "
"interpretation of keypad keys.  Pressing the NumLock key complements the "
"NumLock setting.)"
msgstr ""
"Czyści lub ustawia NumLock. (Od teraz, ustawienie NumLock wpływa na "
"interpretację klawiszy numerycznych. Wciśnięcie klawisza NumLock uzupełnia "
"ustawienie NumLock)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-caps +caps"
msgstr "-caps +caps"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Clear or set CapsLock.  (At present, the CapsLock setting complements the "
"Shift key when applied to letters.  Pressing the CapsLock key complements "
"the CapsLock setting.)"
msgstr ""
"Czyści lub ustawia CapsLock. (Od teraz, ustawienie CapsLock dodaje klawisz "
"Shift w stosunku do wprowadzanych liter. Wciśnięcie klawisza CapsLock "
"uzupełnia ustawienie CapsLock)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-scroll +scroll"
msgstr "-scroll +scroll"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Clear or set ScrollLock.  (At present, pressing the ScrollLock key (or ^S/"
"^Q) stops/starts console output.)"
msgstr ""
"Czyści lub ustawia ScrollLock. (Od teraz, wciśnięcie klawisza ScrollLock "
"(lub ^S/^Q) zatrzymuje/wznawia wyjście konsoli)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BŁĘDY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In keyboard application mode the NumLock key does not influence the NumLock "
"flag setting."
msgstr ""
"W trybie aplikacji klawiatury klawisz NumLock nie wpływa na ustawienie flagi "
"NumLock."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<loadkeys>(1)"
msgstr "B<loadkeys>(1)"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<Setleds> reports and changes the led flag settings of a VT (namely "
"NumLock, CapsLock and ScrollLock).  Without arguments, B<setleds> prints the "
"current settings.  With arguments, it sets or clears the indicated flags "
"(and leaves the others unchanged).  The settings before and after the change "
"are reported if the -v flag is given."
msgstr ""
"B<setleds> odczytuje i zmienia ustawienia światełek LED (zwanych NumLock, "
"CapsLock i ScrollLock) bieżącego VT (termianla wirtualnego). Bez argumentów "
"polecenie B<setleds> wypisuje obecne ustawienia. Z argumentami ustawia lub "
"wyłącza wskazane flagi (pozostawiając inne niezmienione). Ustawienia przed i "
"po zmianie są zgłaszane jeśli podano opcję -v."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"With option -L, B<setleds> will not touch the VT flags, but only change the "
"leds.  From this moment on, the leds will no longer reflect the VT flags "
"(but display whatever is put into them).  The command B<setleds -L> (without "
"further arguments) will restore the situation in which the leds reflect the "
"VT flags."
msgstr ""
"Użyty z opcją -L, B<setleds> nie rusza flag VT, a jedynie zmienia stan diod. "
"Od tej chwili diody nie będą oddawać rzeczywistego stanu flag VT (ale "
"zareagują na każdą zmianę).  Polecenie B<setleds -L> (bez kolejnych "
"argumentów) przywróci sytuację, w której diody oddają stan flag VT."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"One might use B<setleds> in /etc/rc to define the initial and default state "
"of NumLock, e.g.\\& by"
msgstr ""
"Można użyć B<setleds> w /etc/rc, by zdefiniować początkowy i domyślny stan "
"NumLock, na przykład tak:"
