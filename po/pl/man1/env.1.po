# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1999.
# Wojtek Kotwica <wkotwica@post.pl>, 2000.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-10-02 11:57+0200\n"
"PO-Revision-Date: 2020-02-28 23:11+0100\n"
"Last-Translator: Szymon Lamkiewicz <s.lam@o2.pl>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ENV"
msgstr "ENV"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "wrzesień 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Polecenia użytkownika"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "env - run a program in a modified environment"
msgstr "env - uruchamia program w zmienionym środowisku"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<env> [I<\\,OPTION\\/>]... [I<\\,-\\/>] [I<\\,NAME=VALUE\\/>]... [I<\\,"
"COMMAND \\/>[I<\\,ARG\\/>]...]"
msgstr ""
"B<env> [I<OPCJA>]... [B<->] [I<NAZWA>B<=>I<WARTOŚĆ>]... [I<POLECENIE "
">[I<ARG>]...]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Set each NAME to VALUE in the environment and run COMMAND."
msgstr ""
"Ustawia każdą zmienną środowiskową I<NAZWA> na I<WARTOŚĆ> i wykonuje "
"I<POLECENIE>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumenty, które są obowiązkowe dla długich opcji, są również obowiązkowe "
"dla krótkich."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-environment>"
msgstr "B<-i>, B<--ignore-environment>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "start with an empty environment"
msgstr "zaczyna z pustym środowiskiem"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-0>, B<--null>"
msgstr "B<-0>, B<--null>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "end each output line with NUL, not newline"
msgstr "kończy każdy wiersz bajtem NUL, zamiast znakiem nowego wiersza"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--unset>=I<\\,NAME\\/>"
msgstr "B<-u>, B<--unset>=I<NAZWA>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "remove variable from the environment"
msgstr "usuwa zmienną I<NAZWA> ze środowiska"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-C>, B<--chdir>=I<\\,DIR\\/>"
msgstr "B<-C>, B<--chdir>=I<\\,KATALOG\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "change working directory to DIR"
msgstr "zmienia katalog roboczy na B<KATALOG>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--split-string>=I<\\,S\\/>"
msgstr "B<-S>, B<--split-string>=I<\\,S\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"process and split S into separate arguments; used to pass multiple arguments "
"on shebang lines"
msgstr ""
"procesuje i dzieli B<S> na osobne argumenty; używane by przekazać kilka "
"argumentów na linię shebang"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--block-signal>[=I<\\,SIG\\/>]"
msgstr "B<--block-signal>[=I<\\,SYGNAŁ\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "block delivery of SIG signal(s) to COMMAND"
msgstr "blokuje dostarczenie sygnał(ów) B<SYGNAŁ> do komendy"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--default-signal>[=I<\\,SIG\\/>]"
msgstr "B<--default-signal>[=I<\\,SYGNAŁ\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "reset handling of SIG signal(s) to the default"
msgstr "resetuje sposób obsługi sygnał(ów) B<SYGNAŁ> do ustawień domyślnych"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--ignore-signal>[=I<\\,SIG\\/>]"
msgstr "B<--ignore-signal>[=I<\\,SYGNAŁ\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "set handling of SIG signals(s) to do nothing"
msgid "set handling of SIG signal(s) to do nothing"
msgstr "ustawia sposób obsługi sygnał(ów) B<SYGNAŁ> by nie miały efektu"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--list-signal-handling>"
msgstr "B<--list-signal-handling>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "list non default signal handling to stderr"
msgstr "wypisuje niedomyślne sposoby obsługi sygnałów na stderr"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--debug>"
msgstr "B<-v>, B<--debug>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "print verbose information for each processing step"
msgstr "wypisuje szczegółowe informacje dla każdego kroku procesowania"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "wyświetla ten tekst i kończy pracę"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "wyświetla informacje o wersji i kończy działanie"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A mere - implies B<-i>.  If no COMMAND, print the resulting environment."
msgstr ""
"Argument B<-> implikuje B<-i>. Jeśli nie podano I<POLECENIA>, wyświetlane "
"jest otrzymane środowisko."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "SIG may be a signal name like 'PIPE', or a signal number like '13'.  "
#| "Without SIG, all known signals are included.  Multiple signals can be "
#| "comma-separated."
msgid ""
"SIG may be a signal name like 'PIPE', or a signal number like '13'.  Without "
"SIG, all known signals are included.  Multiple signals can be comma-"
"separated.  An empty SIG argument is a no-op."
msgstr ""
"B<SYGNAŁ> może być nazwą sygnału jak \"PIPE\" lub numerem sygnału jak "
"\"13\". Bez B<SYGNAŁU>, wszystkie znane sygnały są dołączone. Kolejne "
"sygnały należy rozdzielić średnikiem."

#. type: SS
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr ""

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "125"
msgstr "125"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "if the env command itself fails"
msgstr ""

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "126"
msgstr "126"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "if COMMAND is found but cannot be invoked"
msgstr ""

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "127"
msgstr "127"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "if COMMAND cannot be found"
msgstr ""

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "-"
msgstr "-"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "the exit status of COMMAND otherwise"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-S/--split-string usage in scripts"
msgstr "Użycie -S/--split-string w skryptach"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<-S> option allows specifying multiple parameters in a script.  Running "
"a script named B<1.pl> containing the following first line:"
msgstr ""
"Opcja B<-S> zezwala na określenie kilku parametrów w skrypcie. Uruchomienie "
"skryptu o nazwie B<1.pl> zawierającego następującą pierwszą linię:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"#!/usr/bin/env -S perl -w -T\n"
"\\&...\n"
msgstr ""
"#!/usr/bin/env -S perl -w -T\n"
"\\&...\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Will execute B<perl -w -T 1.pl .>"
msgstr "Wywoła B<perl -w -T 1.pl .>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Without the B<'-S'> parameter the script will likely fail with:"
msgstr "Bez parametru B<'-S'> skrypt z argumentu prawdopodobnie zwróci błąd:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "/usr/bin/env: 'perl -w -T': No such file or directory\n"
msgstr "/usr/bin/env: 'perl -w -T': No such file or directory\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See the full documentation for more details."
msgstr "Więcej detali w pełnej dokumentacji."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "--default-signal[=SIG] usage"
msgstr "użycie --default-signal[=SYGNAŁ] "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This option allows setting a signal handler to its default action, which is "
"not possible using the traditional shell trap command.  The following "
"example ensures that seq will be terminated by SIGPIPE no matter how this "
"signal is being handled in the process invoking the command."
msgstr ""
"Ta opcja pozwala ustawić obsługę sygnału do wartości domyślnych, co nie jest "
"możliwe używając tradycyjnej komendy shell trap. Następujący przykład "
"zapewnia, że ta sekwencja zostanie zakończona przez SIGPIPE, bez znaczenia "
"jak sygnał jest obsługiwany w procesie wywołującym komendę."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sh -c 'env --default-signal=PIPE seq inf | head -n1'\n"
msgstr "sh -c 'env --default-signal=PIPE seq inf | head -n1'\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX's exec(2) pages says:"
msgid "POSIX's B<exec>(3p) pages says:"
msgstr "Pordręcznik POSIX dla exec(2) mówi:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"many existing applications wrongly assume that they start with certain "
"signals set to the default action and/or unblocked.... Therefore, it is best "
"not to block or ignore signals across execs without explicit reason to do "
"so, and especially not to block signals across execs of arbitrary (not "
"closely cooperating) programs."
msgstr ""
"wiele istniejących aplikacji błędnie zakłada, że startują one z konkretnymi "
"sygnałami ustawionymi na akcje domyślne lub/i odblokowanymi... Dlatego, "
"najlepiej nie blokować czy ignorować sygnałów wśród wywołań beż szczególnego "
"powodu, a szczególnie nie blokować sygnałów pośród wywołań arbitralnych (nie "
"współpracujących ze sobą) programów."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Richard Mlynarik, David MacKenzie, and Assaf Gordon."
msgstr "Napisane przez Richarda Mlynarika, Davida MacKenzie oraz Assaf Gordon."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ZGŁASZANIE BŁĘDÓW"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Strona internetowa z pomocą GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"O błędach tłumaczenia poinformuj przez E<lt>https://translationproject.org/"
"team/pl.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "PRAWA AUTORSKIE"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Jest to wolne oprogramowanie: można je zmieniać i rozpowszechniać. Nie ma "
"ŻADNEJ GWARANCJI, w granicach określonych przez prawo."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "sigaction(2), sigprocmask(2), signal(7)"
msgid "B<sigaction>(2), B<sigprocmask>(2), B<signal>(7)"
msgstr "sigaction(2), sigprocmask(2), signal(7)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/envE<gt>"
msgstr ""
"Pełna dokumentacja na stronie: E<lt>https://www.gnu.org/software/coreutils/"
"envE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) env invocation\\(aq"
msgstr "lub lokalnie, za pomocą B<info \\(aq(coreutils) env invocation\\(aq>"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "wrzesień 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"SIG may be a signal name like 'PIPE', or a signal number like '13'.  Without "
"SIG, all known signals are included.  Multiple signals can be comma-"
"separated."
msgstr ""
"B<SYGNAŁ> może być nazwą sygnału jak \"PIPE\" lub numerem sygnału jak "
"\"13\". Bez B<SYGNAŁU>, wszystkie znane sygnały są dołączone. Kolejne "
"sygnały należy rozdzielić średnikiem."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "kwiecień 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "październik 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid "set handling of SIG signals(s) to do nothing"
msgstr "ustawia sposób obsługi sygnał(ów) B<SYGNAŁ> by nie miały efektu"

#. type: Plain text
#: opensuse-leap-15-6
msgid "POSIX's exec(2) pages says:"
msgstr "Pordręcznik POSIX dla exec(2) mówi:"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Licencja GPLv3+: GNU GPL "
"w wersji 3 lub późniejszej E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "sigaction(2), sigprocmask(2), signal(7)"
msgstr "sigaction(2), sigprocmask(2), signal(7)"
