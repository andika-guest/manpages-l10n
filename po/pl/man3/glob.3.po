# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1999.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
# Robert Luberda <robert@debian.org>, 2014, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-08-27 17:00+0200\n"
"PO-Revision-Date: 2019-08-16 21:18+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "glob"
msgstr "glob"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 lipca 2023 r."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"glob, globfree - find pathnames matching a pattern, free memory from glob()"
msgstr ""
"glob, globfree - znalezienie ścieżek odpowiadających wzorcowi, zwolnienie "
"pamięć z glob()"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>glob.hE<gt>>\n"
msgstr "B<#include E<lt>glob.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int glob(const char *>I<pattern>B<, int >I<flags>B<,>\n"
#| "B<         int (*>I<errfunc>B<) (const char *>I<epath>B<, int >I<eerrno>B<),>\n"
#| "B<         glob_t *>I<pglob>B<);>\n"
#| "B<void globfree(glob_t *>I<pglob>B<);>\n"
msgid ""
"B<int glob(const char *restrict >I<pattern>B<, int >I<flags>B<,>\n"
"B<         int (*>I<errfunc>B<)(const char *>I<epath>B<, int >I<eerrno>B<),>\n"
"B<         glob_t *restrict >I<pglob>B<);>\n"
"B<void globfree(glob_t *>I<pglob>B<);>\n"
msgstr ""
"B<int glob(const char *>I<pattern>B<, int >I<flags>B<,>\n"
"B<         int (*>I<errfunc>B<) (const char *>I<epath>B<, int >I<eerrno>B<),>\n"
"B<         glob_t *>I<pglob>B<);>\n"
"B<void globfree(glob_t *>I<pglob>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<glob>()  function searches for all the pathnames matching I<pattern> "
"according to the rules used by the shell (see B<glob>(7)).  No tilde "
"expansion or parameter substitution is done; if you want these, use "
"B<wordexp>(3)."
msgstr ""
"Funkcja B<glob>() przeszukuje wszystkie ścieżki odpowiadające wzorcowi "
"I<pattern>, stosując przy tym reguły takie, jakich użyłaby powłoka (zobacz "
"B<glob>(7)). Nie jest dokonywane rozwinięcie tyldy ani podstawienie "
"parametrów. Jeśli są one potrzebne, to należy użyć B<wordexp>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<globfree>()  function frees the dynamically allocated storage from an "
"earlier call to B<glob>()."
msgstr ""
"Funkcja B<globfree>() zwalnia obszar pamięci zaalokowany dynamicznie przez "
"wcześniejsze wywołanie funkcji B<glob>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The results of a B<glob>()  call are stored in the structure pointed to by "
"I<pglob>.  This structure is of type I<glob_t> (declared in I<E<lt>glob."
"hE<gt>>)  and includes the following elements defined by POSIX.2 (more may "
"be present as an extension):"
msgstr ""
"W wyniku wywołania B<glob>() tworzona jest struktura, na którą wskazuje "
"I<pglob>. Struktura jest typu B<glob_t> (deklarowany w I<E<lt>glob.hE<gt>>) "
"i zawiera następujące elementy zdefiniowane przez POSIX.2 (mogą też "
"występować dodatkowe jako rozszerzenie):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"typedef struct {\n"
"    size_t   gl_pathc;    /* Count of paths matched so far  */\n"
"    char   **gl_pathv;    /* List of matched pathnames.  */\n"
"    size_t   gl_offs;     /* Slots to reserve in I<gl_pathv>.  */\n"
"} glob_t;\n"
msgstr ""
"typedef struct {\n"
"    size_t   gl_pathc;    /* Liczba odpowiadających dotąd ścieżek.  */\n"
"    char   **gl_pathv;    /* Lista odpowiadających ścieżek  */\n"
"    size_t   gl_offs;     /*  Sloty do rezerwowania w I<gl_pathv>.  */\n"
"} glob_t;\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Results are stored in dynamically allocated storage."
msgstr "Wyniki są zachowywane w dynamicznie przydzielanym obszarze pamięci."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The argument I<flags> is made up of the bitwise OR of zero or more the "
"following symbolic constants, which modify the behavior of B<glob>():"
msgstr ""
"Argument I<flags> jest bitowym OR-em zera lub więcej następujących symboli "
"stałych modyfikujących zachowanie B<glob>():"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_ERR>"
msgstr "B<GLOB_ERR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Return upon a read error (because a directory does not have read permission, "
"for example).  By default, B<glob>()  attempts carry on despite errors, "
"reading all of the directories that it can."
msgstr ""
"Powraca po napotkaniu błędu odczytu (na przykład, jeśli nie ma praw do "
"odczytu katalogu). Domyślnie B<glob>() kontynuuje działanie mimo błędów, "
"odczytując wszystkie katalogi, do których ma uprawnienia."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_MARK>"
msgstr "B<GLOB_MARK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Append a slash to each path which corresponds to a directory."
msgstr "Dodaje ukośnik do każdej ścieżki, która odpowiada katalogowi."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_NOSORT>"
msgstr "B<GLOB_NOSORT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Don't sort the returned pathnames.  The only reason to do this is to save "
"processing time.  By default, the returned pathnames are sorted."
msgstr ""
"Nie sortuje zwracanych nazw ścieżek. Jedynym powodem użycia tej opcji może "
"być zaoszczędzenie czasu przetwarzania. Domyślnie zwracane nazwy ścieżek są "
"sortowane."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_DOOFFS>"
msgstr "B<GLOB_DOOFFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Reserve I<pglob-E<gt>gl_offs> slots at the beginning of the list of strings "
"in I<pglob-E<gt>pathv>.  The reserved slots contain null pointers."
msgstr ""
"Rezerwuje sloty I<pglob-E<gt>gl_offs> na początku listy napisów w I<pglob-"
"E<gt>pathv>. Zarezerwowane sloty mogą zawierać wskaźniki null."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_NOCHECK>"
msgstr "B<GLOB_NOCHECK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If no pattern matches, return the original pattern.  By default, B<glob>()  "
"returns B<GLOB_NOMATCH> if there are no matches."
msgstr ""
"Jeśli żaden wzorzec nie zostanie dopasowany, to zwracany jest oryginalny "
"wzorzec. Domyślnie B<glob>() w razie braku dopasowań zwraca B<GLOB_NOMATCH>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_APPEND>"
msgstr "B<GLOB_APPEND>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Append the results of this call to the vector of results returned by a "
"previous call to B<glob>().  Do not set this flag on the first invocation of "
"B<glob>()."
msgstr ""
"Doklejać wyniki bieżącego wywołania do listy wyników poprzedniego wywołania. "
"Nie należy ustawiać tej flagi przy pierwszym wywołaniu B<glob>()."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_NOESCAPE>"
msgstr "B<GLOB_NOESCAPE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Don't allow backslash (\\(aq\\e\\(aq) to be used as an escape character.  "
#| "Normally, a backslash can be used to quote the following character, "
#| "providing a mechanism to turn off the special meaning metacharacters."
msgid ""
"Don't allow backslash (\\[aq]\\e\\[aq]) to be used as an escape character.  "
"Normally, a backslash can be used to quote the following character, "
"providing a mechanism to turn off the special meaning metacharacters."
msgstr ""
"Nie pozwala na użycie znaku odwrotnego ukośnika (\"\\e\") jako znaku "
"cytowania. Domyślnie odwrotny ukośnik cytuje następujący po nim znak, "
"dostarczając mechanizmu wyłączania specjalnego znaczenia metaznaków."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<flags> may also include any of the following, which are GNU extensions and "
"not defined by POSIX.2:"
msgstr ""
"Parametr I<flags> może również zawierać następujące znaczniki, będące "
"rozszerzeniami GNU niedefiniowanymi przez POSIX.2:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_PERIOD>"
msgstr "B<GLOB_PERIOD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Allow a leading period to be matched by metacharacters.  By default, "
"metacharacters can't match a leading period."
msgstr ""
"Pozwala na to, by początkowa kropka była dopasowywana przez metaznaki. "
"Domyślnie metaznaki nie są dopasowywane do początkowej kropki."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_ALTDIRFUNC>"
msgstr "B<GLOB_ALTDIRFUNC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Use alternative functions I<pglob-E<gt>gl_closedir>, I<pglob-"
"E<gt>gl_readdir>, I<pglob-E<gt>gl_opendir>, I<pglob-E<gt>gl_lstat>, and "
"I<pglob-E<gt>gl_stat> for filesystem access instead of the normal library "
"functions."
msgstr ""
"Zamiast zwykłych funkcji bibliotecznych używa alternatywnych funkcji I<pglob-"
"E<gt>gl_closedir>, I<pglob-E<gt>gl_readdir>, I<pglob-E<gt>gl_opendir>, "
"I<pglob-E<gt>gl_lstat> oraz I<pglob-E<gt>gl_stat> do dostępu do systemu "
"plików."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_BRACE>"
msgstr "B<GLOB_BRACE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Expand B<csh>(1)  style brace expressions of the form B<{a,b}>.  Brace "
"expressions can be nested.  Thus, for example, specifying the pattern \"{foo/"
"{,cat,dog},bar}\" would return the same results as four separate B<glob>()  "
"calls using the strings: \"foo/\", \"foo/cat\", \"foo/dog\", and \"bar\"."
msgstr ""
"Rozwija wyrażenia nawiasowe stylu B<csh>(1) w formacie B<{a,b}>. Wyrażenia "
"nawiasowe mogą być zagnieżdżane. Dlatego na przykład podanie wzoraca \"{foo/"
"{,cat,dog},bar}\" zwróci takie same wyniki jak czterokrotne wywołanie "
"B<glob>() z następującymi argumentami: \"foo/\", \"foo/cat\", \"foo/dog\" "
"oraz \"bar\"."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_NOMAGIC>"
msgstr "B<GLOB_NOMAGIC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the pattern contains no metacharacters, then it should be returned as the "
"sole matching word, even if there is no file with that name."
msgstr ""
"Jeśli wzorzec nie zawiera metaznaków, to powinien być zwrócony jako jedyne "
"pasujące słowo nawet wtedy, gdy nie ma plików o takiej nazwie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_TILDE>"
msgstr "B<GLOB_TILDE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Carry out tilde expansion.  If a tilde (\\(aq~\\(aq) is the only "
#| "character in the pattern, or an initial tilde is followed immediately by "
#| "a slash (\\(aq/\\(aq), then the home directory of the caller is "
#| "substituted for the tilde.  If an initial tilde is followed by a username "
#| "(e.g., \"~andrea/bin\"), then the tilde and username are substituted by "
#| "the home directory of that user.  If the username is invalid, or the home "
#| "directory cannot be determined, then no substitution is performed."
msgid ""
"Carry out tilde expansion.  If a tilde (\\[aq]\\[ti]\\[aq]) is the only "
"character in the pattern, or an initial tilde is followed immediately by a "
"slash (\\[aq]/\\[aq]), then the home directory of the caller is substituted "
"for the tilde.  If an initial tilde is followed by a username (e.g., "
"\"\\[ti]andrea/bin\"), then the tilde and username are substituted by the "
"home directory of that user.  If the username is invalid, or the home "
"directory cannot be determined, then no substitution is performed."
msgstr ""
"Przeprowadza ekspansję tyldy. Jeśli tylda (\"~\") jest jedynym znakiem we "
"wzorcu lub zaraz po początkowym znaku tyldy występuje ukośnik (\"/\"), to "
"tylda jest zastępowana przez nazwę katalogu domowego użytkownika "
"wywołującego B<glob>(). Jeśli po początkowej tyldzie występuje nazwa "
"użytkownika (np. \"~andrea/bin\"), to tylda i nazwa użytkownika "
"są zastępowane przez katalog domowy tego użytkownika. Jeśli nazwa "
"użytkownika jest niepoprawna lub katalog domowy nie może być określony, to "
"żadne zastępowanie nie jest przeprowadzane."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_TILDE_CHECK>"
msgstr "B<GLOB_TILDE_CHECK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This provides behavior similar to that of B<GLOB_TILDE>.  The difference is "
"that if the username is invalid, or the home directory cannot be determined, "
"then instead of using the pattern itself as the name, B<glob>()  returns "
"B<GLOB_NOMATCH> to indicate an error."
msgstr ""
"Zachowuje się podobnie do B<GLOB_TILDE>, z tą różnicą że jeśli nazwa "
"użytkownika jest niepoprawna lub jeśli nie można określić katalogu domowego "
"użytkownika, to B<glob>() zamiast używać wzorca jako nazwy zwróci "
"B<GLOB_NOMATCH>, wskazując, że wystąpił błąd."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_ONLYDIR>"
msgstr "B<GLOB_ONLYDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is a I<hint> to B<glob>()  that the caller is interested only in "
"directories that match the pattern.  If the implementation can easily "
"determine file-type information, then nondirectory files are not returned to "
"the caller.  However, the caller must still check that returned files are "
"directories.  (The purpose of this flag is merely to optimize performance "
"when the caller is interested only in directories.)"
msgstr ""
"Jest to I<wskazówka> dla funkcji B<glob>(), mówiąca, że funkcja wywołująca "
"jest zainteresowana tylko katalogami pasującymi do wzorca. Jeśli "
"implementacja może w łatwy sposób określić informację o typie pliku, to "
"pliki niebędące katalogami nie są zwracane. Jednakże funkcja wywołująca musi "
"sprawdzić, że zwrócone pliki są katalogami. (Celem tej flagi jest "
"poprawienie wydajności w przypadku, gdy funkcja wywołująca jest "
"zainteresowana tylko katalogami)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<errfunc> is not NULL, it will be called in case of an error with the "
"arguments I<epath>, a pointer to the path which failed, and I<eerrno>, the "
"value of I<errno> as returned from one of the calls to B<opendir>(3), "
"B<readdir>(3), or B<stat>(2).  If I<errfunc> returns nonzero, or if "
"B<GLOB_ERR> is set, B<glob>()  will terminate after the call to I<errfunc>."
msgstr ""
"Jeśli I<errfunc> nie jest równe NULL, to w wypadku błędu będzie ono wywołane "
"z argumentami I<epath>, czyli wskaźnikiem do ścieżki, na której coś się nie "
"powiodło, i z I<eerrno>, przechowującym wartość I<errno>, zwróconą przez "
"wywołanie do B<opendir>(3), B<readdir>(3) lub B<stat>(2). Jeśli I<errfunc> "
"zwraca wartość niezerową lub jeśli ustawiony jest znacznik B<GLOB_ERR>, to "
"B<glob>() zakończy działanie po wywołaniu funkcji I<errfunc>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Upon successful return, I<pglob-E<gt>gl_pathc> contains the number of "
"matched pathnames and I<pglob-E<gt>gl_pathv> contains a pointer to the list "
"of pointers to matched pathnames.  The list of pointers is terminated by a "
"null pointer."
msgstr ""
"Po pomyślnym zakończeniu, I<pglob-E<gt>gl_pathc> zawiera liczbę pasujących "
"ścieżek, a I<pglob-E<gt>gl_pathv> wskaźnik do listy wskaźników do "
"dopasowanych ścieżek. Lista ta jest zakończona wskaźnikiem null."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"It is possible to call B<glob>()  several times.  In that case, the "
"B<GLOB_APPEND> flag has to be set in I<flags> on the second and later "
"invocations."
msgstr ""
"Możliwe jest wywoływanie B<glob>() wielokrotnie. W takim wypadku należy w "
"następnych wywołaniach ustawić w I<flags> znacznik B<GLOB_APPEND>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"As a GNU extension, I<pglob-E<gt>gl_flags> is set to the flags specified, "
"B<or>ed with B<GLOB_MAGCHAR> if any metacharacters were found."
msgstr ""
"Jako rozszerzenie GNU, I<pglob-E<gt>gl_flags> jest ustawiane jako B<or> "
"podanych znaczników i B<GLOB_MAGCHAR>, gdy występują metaznaki."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On successful completion, B<glob>()  returns zero.  Other possible returns "
"are:"
msgstr ""
"Po pomyślnym zakończeniu B<glob>() zwraca zero. Inne możliwe wartości to:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_NOSPACE>"
msgstr "B<GLOB_NOSPACE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "for running out of memory,"
msgstr "przy braku pamięci,"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_ABORTED>"
msgstr "B<GLOB_ABORTED>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "for a read error, and"
msgstr "przy błędzie odczytu i"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<GLOB_NOMATCH>"
msgstr "B<GLOB_NOMATCH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "for no found matches."
msgstr "gdy niczego nie dopasowano."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<glob>()"
msgstr "B<glob>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Unsafe race:utent env\n"
msgid ""
"MT-Unsafe race:utent env\n"
"sig:ALRM timer locale"
msgstr "MT-Unsafe race:utent env\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<globfree>()"
msgstr "B<globfree>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the above table, I<utent> in I<race:utent> signifies that if any of the "
"functions B<setutent>(3), B<getutent>(3), or B<endutent>(3)  are used in "
"parallel in different threads of a program, then data races could occur.  "
"B<glob>()  calls those functions, so we use race:utent to remind users."
msgstr ""
"W powyższej tabeli I<utent> w I<race:utent> oznacza, że jeśli któraś z "
"funkcji B<setutent>(3), B<getutent>(3) lub B<endutent>(3) jest używana "
"równolegle w różnych wątkach programu, może nastąpić sytuacja wyścigu "
"danych. Ponieważ B<glob>() wywołuje te funkcje, stąd dla przypomnienia "
"użytkownikom używamy race:utent."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

# All that section was initially translated by PB. Readded with minor corrections by MK. --MK
#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001, POSIX.1-2008."
msgid "POSIX.1-2001, POSIX.2."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The structure elements I<gl_pathc> and I<gl_offs> are declared as I<size_t> "
"in glibc 2.1, as they should be according to POSIX.2, but are declared as "
"I<int> in glibc 2.0."
msgstr ""
"Elementy I<gl_pathc> i I<gl_offs> struktury są w glibc 2.1 zadeklarowane "
"jako I<size_t>, jak powinno być zgodnie z POSIX.2, ale są zadeklarowane jako "
"I<int> w glibc 2.0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BŁĘDY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<glob>()  function may fail due to failure of underlying function "
"calls, such as B<malloc>(3)  or B<opendir>(3).  These will store their error "
"code in I<errno>."
msgstr ""
"Funkcja B<glob>() może zawieść z powodu błędu wywołanych przez nią funkcji, "
"takich jak B<malloc>(3) czy B<opendir>(3). Wywołania te zapiszą kod błędu w "
"I<errno>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "PRZYKŁADY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "One example of use is the following code, which simulates typing"
msgstr "Jednym z przykładów użycia jest następujący kod, emulujący wpisanie"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ls -l *.c ../*.c\n"
msgstr "ls -l *.c ../*.c\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "in the shell:"
msgstr "w powłoce:"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, fuzzy, no-wrap
#| msgid ""
#| "globbuf.gl_offs = 2;\n"
#| "glob(\"*.c\", GLOB_DOOFFS, NULL, &globbuf);\n"
#| "glob(\"../*.c\", GLOB_DOOFFS | GLOB_APPEND, NULL, &globbuf);\n"
#| "globbuf.gl_pathv[0] = \"ls\";\n"
#| "globbuf.gl_pathv[1] = \"-l\";\n"
#| "execvp(\"ls\", &globbuf.gl_pathv[0]);\n"
msgid ""
"glob_t globbuf;\n"
"\\&\n"
"globbuf.gl_offs = 2;\n"
"glob(\"*.c\", GLOB_DOOFFS, NULL, &globbuf);\n"
"glob(\"../*.c\", GLOB_DOOFFS | GLOB_APPEND, NULL, &globbuf);\n"
"globbuf.gl_pathv[0] = \"ls\";\n"
"globbuf.gl_pathv[1] = \"-l\";\n"
"execvp(\"ls\", &globbuf.gl_pathv[0]);\n"
msgstr ""
"globbuf.gl_offs = 2;\n"
"glob(\"*.c\", GLOB_DOOFFS, NULL, &globbuf);\n"
"glob(\"../*.c\", GLOB_DOOFFS | GLOB_APPEND, NULL, &globbuf);\n"
"globbuf.gl_pathv[0] = \"ls\";\n"
"globbuf.gl_pathv[1] = \"-l\";\n"
"execvp(\"ls\", &globbuf.gl_pathv[0]);\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<ls>(1), B<sh>(1), B<stat>(2), B<exec>(3), B<fnmatch>(3), B<malloc>(3), "
"B<opendir>(3), B<readdir>(3), B<wordexp>(3), B<glob>(7)"
msgstr ""
"B<ls>(1), B<sh>(1), B<stat>(2), B<exec>(3), B<fnmatch>(3), B<malloc>(3), "
"B<opendir>(3), B<readdir>(3), B<wordexp>(3), B<glob>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, POSIX.2."
msgstr "POSIX.1-2001, POSIX.1-2008, POSIX.2."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "glob_t globbuf;\n"
msgstr "glob_t globbuf;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"globbuf.gl_offs = 2;\n"
"glob(\"*.c\", GLOB_DOOFFS, NULL, &globbuf);\n"
"glob(\"../*.c\", GLOB_DOOFFS | GLOB_APPEND, NULL, &globbuf);\n"
"globbuf.gl_pathv[0] = \"ls\";\n"
"globbuf.gl_pathv[1] = \"-l\";\n"
"execvp(\"ls\", &globbuf.gl_pathv[0]);\n"
msgstr ""
"globbuf.gl_offs = 2;\n"
"glob(\"*.c\", GLOB_DOOFFS, NULL, &globbuf);\n"
"glob(\"../*.c\", GLOB_DOOFFS | GLOB_APPEND, NULL, &globbuf);\n"
"globbuf.gl_pathv[0] = \"ls\";\n"
"globbuf.gl_pathv[1] = \"-l\";\n"
"execvp(\"ls\", &globbuf.gl_pathv[0]);\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GLOB"
msgstr "GLOB"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<int glob(const char *>I<pattern>B<, int >I<flags>B<,>\n"
"B<         int (*>I<errfunc>B<) (const char *>I<epath>B<, int >I<eerrno>B<),>\n"
"B<         glob_t *>I<pglob>B<);>\n"
"B<void globfree(glob_t *>I<pglob>B<);>\n"
msgstr ""
"B<int glob(const char *>I<pattern>B<, int >I<flags>B<,>\n"
"B<         int (*>I<errfunc>B<) (const char *>I<epath>B<, int >I<eerrno>B<),>\n"
"B<         glob_t *>I<pglob>B<);>\n"
"B<void globfree(glob_t *>I<pglob>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Don't allow backslash (\\(aq\\e\\(aq) to be used as an escape character.  "
"Normally, a backslash can be used to quote the following character, "
"providing a mechanism to turn off the special meaning metacharacters."
msgstr ""
"Nie pozwala na użycie znaku odwrotnego ukośnika (\"\\e\") jako znaku "
"cytowania. Domyślnie odwrotny ukośnik cytuje następujący po nim znak, "
"dostarczając mechanizmu wyłączania specjalnego znaczenia metaznaków."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Carry out tilde expansion.  If a tilde (\\(aq~\\(aq) is the only character "
"in the pattern, or an initial tilde is followed immediately by a slash "
"(\\(aq/\\(aq), then the home directory of the caller is substituted for the "
"tilde.  If an initial tilde is followed by a username (e.g., \"~andrea/"
"bin\"), then the tilde and username are substituted by the home directory of "
"that user.  If the username is invalid, or the home directory cannot be "
"determined, then no substitution is performed."
msgstr ""
"Przeprowadza ekspansję tyldy. Jeśli tylda (\"~\") jest jedynym znakiem we "
"wzorcu lub zaraz po początkowym znaku tyldy występuje ukośnik (\"/\"), to "
"tylda jest zastępowana przez nazwę katalogu domowego użytkownika "
"wywołującego B<glob>(). Jeśli po początkowej tyldzie występuje nazwa "
"użytkownika (np. \"~andrea/bin\"), to tylda i nazwa użytkownika "
"są zastępowane przez katalog domowy tego użytkownika. Jeśli nazwa "
"użytkownika jest niepoprawna lub katalog domowy nie może być określony, to "
"żadne zastępowanie nie jest przeprowadzane."

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "MT-Unsafe race:utent env\n"
msgstr "MT-Unsafe race:utent env\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "sig:ALRM timer locale"
msgstr "sig:ALRM timer locale"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "PRZYKŁAD"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 marca 2023 r."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
