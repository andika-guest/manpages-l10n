# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
#  Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
# Robert Luberda <robert@debian.org>, 2014, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-08-27 17:00+0200\n"
"PO-Revision-Date: 2019-08-16 21:20+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "getservent"
msgstr "getservent"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 lipca 2023 r."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"getservent, getservbyname, getservbyport, setservent, endservent - get "
"service entry"
msgstr ""
"getservent, getservbyname, getservbyport, setservent, endservent - "
"odczytanie wpisu dotyczącego usługi"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>netdb.hE<gt>>\n"
msgstr "B<#include E<lt>netdb.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<struct servent *getservent(void);>\n"
msgstr "B<struct servent *getservent(void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct servent *getservbyname(const char *>I<name>B<, const char *>I<proto>B<);>\n"
"B<struct servent *getservbyport(int >I<port>B<, const char *>I<proto>B<);>\n"
msgstr ""
"B<struct servent *getservbyname(const char *>I<name>B<, const char *>I<proto>B<);>\n"
"B<struct servent *getservbyport(int >I<port>B<, const char *>I<proto>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void setservent(int >I<stayopen>B<);>\n"
"B<void endservent(void);>\n"
msgstr ""
"B<void setservent(int >I<stayopen>B<);>\n"
"B<void endservent(void);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<getservent>()  function reads the next entry from the services "
"database (see B<services>(5))  and returns a I<servent> structure containing "
"the broken-out fields from the entry.  A connection is opened to the "
"database if necessary."
msgstr ""
"Funkcja B<getservent>() odczytuje następny wpis z bazy danych usług (patrz "
"B<services>(5)) i zwraca strukturę I<servent> zawierającą pola powstałe z "
"rozłożenia pól wpisu. Połączenie do bazy danych jest otwierane, jeśli jest "
"to potrzebne."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<getservbyname>()  function returns a I<servent> structure for the "
"entry from the database that matches the service I<name> using protocol "
"I<proto>.  If I<proto> is NULL, any protocol will be matched.  A connection "
"is opened to the database if necessary."
msgstr ""
"Funkcja B<getservbyname>() zwraca strukturę I<servent> zawierającą ten wpis "
"z bazy danych, który odpowiada usłudze I<name> korzystającej z protokołu "
"I<proto>. Jeśli I<proto> jest równe NULL, to pasował będzie dowolny "
"protokół. Połączenie do bazy danych jest otwierane, jeśli jest to potrzebne."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<getservbyport>()  function returns a I<servent> structure for the "
"entry from the database that matches the port I<port> (given in network byte "
"order)  using protocol I<proto>.  If I<proto> is NULL, any protocol will be "
"matched.  A connection is opened to the database if necessary."
msgstr ""
"Funkcja B<getservbyport>() zwraca strukturę I<servent> zawierającą wartości "
"tego wpisu z bazy danych, który odpowiada portowi I<port> (podanemu w "
"sieciowej kolejności bajtów) dla protokołu I<proto>. Jeśli I<proto> jest "
"równe NULL, to pasował będzie dowolny protokół. Połączenie do bazy danych "
"jest otwierane, jeśli jest to potrzebne."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<setservent>()  function opens a connection to the database, and sets "
"the next entry to the first entry.  If I<stayopen> is nonzero, then the "
"connection to the database will not be closed between calls to one of the "
"B<getserv*>()  functions."
msgstr ""
"Funkcja B<setservent>() otwiera połączenie do bazy danych i ustawia wskaźnik "
"następnego wpisu na pierwszy wpis. Jeśli I<stayopen> jest niezerowe, to "
"połączenie do bazy danych nie będzie zamykane pomiędzy wywołaniami funkcji "
"B<getserv*>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<endservent>()  function closes the connection to the database."
msgstr "Funkcja B<endservent>() zamyka połączenie do bazy danych."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The I<servent> structure is defined in I<E<lt>netdb.hE<gt>> as follows:"
msgstr ""
"Struktura I<servent> jest zdefiniowana w I<E<lt>netdb.hE<gt>> następująco:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct servent {\n"
"    char  *s_name;       /* official service name */\n"
"    char **s_aliases;    /* alias list */\n"
"    int    s_port;       /* port number */\n"
"    char  *s_proto;      /* protocol to use */\n"
"}\n"
msgstr ""
"struct servent {\n"
"    char  *s_name;       /* oficjalna nazwa usługi */\n"
"    char **s_aliases;    /* lista aliasów */\n"
"    int    s_port;       /* numer portu */\n"
"    char  *s_proto;      /* używany protokół */\n"
"}\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The members of the I<servent> structure are:"
msgstr "Polami struktury I<servent> są:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<s_name>"
msgstr "I<s_name>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The official name of the service."
msgstr "Oficjalna nazwa usługi."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<s_aliases>"
msgstr "I<s_aliases>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "A NULL-terminated list of alternative names for the service."
msgstr "Zakończona NULL-em lista alternatywnych nazw tej usługi."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<s_port>"
msgstr "I<s_port>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The port number for the service given in network byte order."
msgstr "Numer portu tej usługi podany w sieciowej kolejności bajtów."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<s_proto>"
msgstr "I<s_proto>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The name of the protocol to use with this service."
msgstr "Nazwa protokołu z którego korzysta dana usługa."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<getservent>(), B<getservbyname>(), and B<getservbyport>()  functions "
"return a pointer to a statically allocated I<servent> structure, or NULL if "
"an error occurs or the end of the file is reached."
msgstr ""
"Funkcje B<getservent>(), B<getservbyname>() i B<getservbyport>() zwracają "
"wskaźnik do statycznej struktury I<servent> lub wskaźnik NULL, gdy wystąpi "
"błąd lub napotkany zostanie koniec pliku."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/services>"
msgstr "I</etc/services>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "services database file"
msgstr "plik bazy danych o usługach"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getservent>()"
msgstr "B<getservent>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Unsafe race:servent\n"
msgid ""
"MT-Unsafe race:servent\n"
"race:serventbuf locale"
msgstr "MT-Unsafe race:servent\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getservbyname>()"
msgstr "B<getservbyname>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Unsafe race:servbyname\n"
msgid ""
"MT-Unsafe race:servbyname\n"
"locale"
msgstr "MT-Unsafe race:servbyname\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getservbyport>()"
msgstr "B<getservbyport>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Unsafe race:servbyport\n"
msgid ""
"MT-Unsafe race:servbyport\n"
"locale"
msgstr "MT-Unsafe race:servbyport\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<setservent>(),\n"
"B<endservent>()"
msgstr ""
"B<setservent>(),\n"
"B<endservent>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Unsafe race:servent\n"
msgid ""
"MT-Unsafe race:servent\n"
"locale"
msgstr "MT-Unsafe race:servent\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the above table, I<servent> in I<race:servent> signifies that if any of "
"the functions B<setservent>(), B<getservent>(), or B<endservent>()  are used "
"in parallel in different threads of a program, then data races could occur."
msgstr ""
"W powyższej tabeli, I<servent> w I<race:servent> oznacza, że jeśli któraś z "
"funkcji B<setservent>(), B<getservent>() lub B<endservent>() jest używana "
"równolegle w różnych wątkach programu, może nastąpić sytuacja wyścigu danych."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

# All that section was initially translated by PB. Readded with minor corrections by MK. --MK
#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, 4.3BSD."
msgstr "POSIX.1-2001, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getnetent>(3), B<getprotoent>(3), B<getservent_r>(3), B<services>(5)"
msgstr "B<getnetent>(3), B<getprotoent>(3), B<getservent_r>(3), B<services>(5)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15 grudnia 2022 r."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, 4.3BSD."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GETSERVENT"
msgstr "GETSERVENT"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<struct servent *getservbyname(const char *>I<name>B<, const char *>I<proto>B<);>\n"
msgstr "B<struct servent *getservbyname(const char *>I<name>B<, const char *>I<proto>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<struct servent *getservbyport(int >I<port>B<, const char *>I<proto>B<);>\n"
msgstr "B<struct servent *getservbyport(int >I<port>B<, const char *>I<proto>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void setservent(int >I<stayopen>B<);>\n"
msgstr "B<void setservent(int >I<stayopen>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void endservent(void);>\n"
msgstr "B<void endservent(void);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<getservent>(), B<getservbyname>()  and B<getservbyport>()  functions "
"return a pointer to a statically allocated I<servent> structure, or NULL if "
"an error occurs or the end of the file is reached."
msgstr ""
"Funkcje B<getservent>(), B<getservbyname>() i B<getservbyport>() zwracają "
"wskaźnik do statycznej struktury I<servent> lub wskaźnik NULL, gdy wystąpi "
"błąd lub napotkany zostanie koniec pliku."

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "MT-Unsafe race:servent\n"
msgstr "MT-Unsafe race:servent\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "race:serventbuf locale"
msgstr "race:serventbuf locale"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "MT-Unsafe race:servbyname\n"
msgstr "MT-Unsafe race:servbyname\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "locale"
msgstr "locale"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "MT-Unsafe race:servbyport\n"
msgstr "MT-Unsafe race:servbyport\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "B<setservent>(),\n"
msgstr "B<setservent>(),\n"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "B<endservent>()"
msgstr "B<endservent>()"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 marca 2023 r."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
