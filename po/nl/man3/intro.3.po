# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021-2023.
#
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2023-08-27 17:02+0200\n"
"PO-Revision-Date: 2023-02-23 22:30+0100\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch - The Netherlands <kde-i18n-nl@kde.org>\n"
"Language: nl_NL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 42.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "intro"
msgstr "intro"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5 februari 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pagina's 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "intro - introduction to library functions"
msgstr "intro - introductie van bibliotheek functies"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Section 3 of the manual describes all library functions excluding the "
"library functions (system call wrappers)  described in Section 2, which "
"implement system calls."
msgstr ""
"Sectie 3 van de handleiding beschrijft bibliotheek functies, behalve de "
"bibliotheek functies (systeem aanroep omwikkelfuncties) beschreven in sectie "
"2, die systeem aanroepen implementeert."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Many of the functions described in the section are part of the Standard C "
"Library (I<libc>).  Some functions are part of other libraries (e.g., the "
"math library, I<libm>, or the real-time library, I<librt>)  in which case "
"the manual page will indicate the linker option needed to link against the "
"required library (e.g., I<-lm> and I<-lrt>, respectively, for the "
"aforementioned libraries)."
msgstr ""
"Veel van de hier beschreven functies in deze sectie zijn onderdeel van de "
"Standaard C Bibliotheek (I<libc>). Sommige functies zijn onderdeel van "
"andere bibliotheken (b.v. de math bibliotheek, I<libm>, of van de Realtime "
"bibliotheek, I<librt> in welk geval de man-pagina zal vermelden welke optie "
"nodig is om de linker de vereiste bibliotheek te laten linken (b.v. I<-lm> "
"respectievelijk I<-lrt>, voor de voorgenoemde bibliotheken.  "

#
#.  There
#.  are various function groups which can be identified by a letter which
#.  is appended to the chapter number:
#.  .IP (3C)
#.  These functions,
#.  the functions from chapter 2 and from chapter 3S are
#.  contained in the C standard library libc,
#.  which will be used by
#.  .BR cc (1)
#.  by default.
#.  .IP (3S)
#.  These functions are parts of the
#.  .BR stdio (3)
#.  library.  They are contained in the standard C library libc.
#.  .IP (3M)
#.  These functions are contained in the arithmetic library libm.  They are
#.  used by the
#.  .BR f77 (1)
#.  FORTRAN compiler by default,
#.  but not by the
#.  .BR cc (1)
#.  C compiler,
#.  which needs the option \fI\-lm\fP.
#.  .IP (3F)
#.  These functions are part of the FORTRAN library libF77.  There are no
#.  special compiler flags needed to use these functions.
#.  .IP (3X)
#.  Various special libraries.  The manual pages documenting their functions
#.  specify the library names.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In some cases, the programmer must define a feature test macro in order to "
"obtain the declaration of a function from the header file specified in the "
"man page SYNOPSIS section.  (Where required, these I<feature test macros> "
"must be defined before including I<any> header files.)  In such cases, the "
"required macro is described in the man page.  For further information on "
"feature test macros, see B<feature_test_macros>(7)."
msgstr ""
"In een aantal gevallen moet de programmeur bepaalde feature test macro "
"definiëren om zodoende de declaratie van de functie te verkrijgen waarvan  "
"het header bestand gevonden wordt in de man-pagina SYNOPSIS sectie. (indien "
"nodig moet de I<feature test macro>  gedefinieerd worden vóór het invoegen "
"van I<elk> header bestand.) In deze gevallen wordt de benodigde macro "
"beschreven in de man-pagina. Voor verdere informatie over de feature test "
"macro, zie B<feature_test_macros>(7)"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Subsections"
msgstr "Subsecties"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Section 3 of this manual is organized into subsections that reflect the "
"complex structure of the standard C library and its many implementations:"
msgstr ""
"Sectie 3 van deze handleiding is onderverdeeld in subsecties die de complexe "
"structuur van de standaard C bibliotheek reflecteren en zijn vele "
"implementaties:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "3const"
msgstr "3const"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "3head"
msgstr "3head"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "3type"
msgstr "3type"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This difficult history frequently makes it a poor example to follow in "
"design, implementation, and presentation."
msgstr ""
"Door de ingewikkelde geschiedenis is het vaak een slecht voorbeeld voor "
"ontwerp,, implementatie en presentatie."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Ideally, a library for the C language is designed such that each header file "
"presents the interface to a coherent software module.  It provides a small "
"number of function declarations and exposes only data types and constants "
"that are required for use of those functions.  Together, these are termed an "
"API or I<application program interface>.  Types and constants to be shared "
"among multiple APIs should be placed in header files that declare no "
"functions.  This organization permits a C library module to be documented "
"concisely with one header file per manual page.  Such an approach improves "
"the readability and accessibility of library documentation, and thereby the "
"usability of the software."
msgstr ""
"Idealiter, wordt een bibliotheek voor de C taal zo ontworpen dat elk header "
"bestand het interface naar een coherent software module vertegenwoordigd. "
"Het voorziet in een klein aantal functie declaraties en toont slechts data "
"typen en constanten die nodig zijn voor deze functies. Samen worden zij "
"aangeduid als API of I<Application Program Interface>. Typen en constanten "
"die met meerdere API´s gedeeld worden, moeten worden geplaatst in header "
"bestanden die geen functies declareren. Deze organisatie staat toe een C "
"bibliotheek module consistent te documenteren met een header bestand per "
"handleiding. Zulk een aanpak verbeterd de leesbaarheid en toegankelijkheid "
"van de bibliotheek handleiding, en daarom de bruikbaarheid van de software."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "VOLDOET AAN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Certain terms and abbreviations are used to indicate UNIX variants and "
"standards to which calls in this section conform.  See B<standards>(7)."
msgstr ""
"Bepaalde termen en afkortingen worden gebruikt om UNIX varianten en "
"standaarden aan te duiden waaraan de aanroepen in deze sectie voldoen. Zie "
"B<standards>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Authors and copyright conditions"
msgstr "Auteurs en auteursrechten"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Look at the header of the manual page source for the author(s) and copyright "
"conditions.  Note that these can be different from page to page!"
msgstr ""
"Zie de kop van de  man-pagina bron voor de namen van auteur(s) en "
"auteursrechten. Merk op dat deze verschillend kunnen zijn voor verschillende "
"pagina's!"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7), "
"B<system_data_types>(7)"
msgstr ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7), "
"B<system_data_types>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pagina's 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pagina's 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "INTRO"
msgstr "INTRO"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 september 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux Programmeurs Handleiding"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Section 3 of the manual describes all library functions excluding the "
"library functions (system call wrappers) described in Section 2, which "
"implement system calls."
msgstr ""
"Sectie 3 van de handleiding beschrijft bibliotheek functies, behalve de "
"bibliotheek functies (systeem aanroep wrappers) beschreven in sectie 2, die "
"systeem aanroepen implementeert."

#
#.  There
#.  are various function groups which can be identified by a letter which
#.  is appended to the chapter number:
#.  .IP (3C)
#.  These functions, the functions from chapter 2 and from chapter 3S are
#.  contained in the C standard library libc, which will be used by
#.  .BR cc (1)
#.  by default.
#.  .IP (3S)
#.  These functions are parts of the
#.  .BR stdio (3)
#.  library.  They are contained in the standard C library libc.
#.  .IP (3M)
#.  These functions are contained in the arithmetic library libm.  They are
#.  used by the
#.  .BR f77 (1)
#.  FORTRAN compiler by default, but not by the
#.  .BR cc (1)
#.  C compiler, which needs the option \fI\-lm\fP.
#.  .IP (3F)
#.  These functions are part of the FORTRAN library libF77.  There are no
#.  special compiler flags needed to use these functions.
#.  .IP (3X)
#.  Various special libraries.  The manual pages documenting their functions
#.  specify the library names.
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"In some cases, the programmer must define a feature test macro in order to "
"obtain the declaration of a function from the header file specified in the "
"man page SYNOPSIS section.  (Where required, these feature test macros must "
"be defined before including I<any> header files.)  In such cases, the "
"required macro is described in the man page.  For further information on "
"feature test macros, see B<feature_test_macros>(7)."
msgstr ""
"In een aantal gevallen moet de programmeur bepaalde feature test macro "
"definiëren om zodoende de declaratie van de functie te verkrijgen waarvan  "
"het header bestand gevonden wordt in de man-pagina SYNOPSIS sectie. (indien "
"nodig moet de feature test macro ´gedefinieerd worden vóór het invoegen van "
"I<elk> header bestand.) In deze gevallen wordt de benodigde macro beschreven "
"in de man-pagina. Voor verdere informatie over de feature test macro, zie "
"B<feature_test_macros>(7)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "VOLDOET AAN"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7)"
msgstr ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 4.16 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
