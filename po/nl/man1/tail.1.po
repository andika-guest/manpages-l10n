# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2023-10-02 12:31+0200\n"
"PO-Revision-Date: 2021-02-04 22:26+0100\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "TAIL"
msgstr "TAIL"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "September 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Opdrachten voor gebruikers"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "tail - output the last part of files"
msgstr "tail - toon het laatste deel van bestanden"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<tail> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<tail> [I<\\,OPTIE\\/>]... [I<\\,BESTAND\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Print the last 10 lines of each FILE to standard output.  With more than one "
"FILE, precede each with a header giving the file name."
msgstr ""
"Kopieert de laatste 10 regels van elk gegeven BESTAND naar standaard "
"uitvoer. Als er meer dan een BESTAND gegeven is, dan wordt die voorafgegaan "
"door een kop met daarin de bestandsnaam"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""
"Zonder BESTAND, of wanneer BESTAND '-' is, wordt standaard invoer gelezen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Een verplicht argument bij een lange optie is ook verplicht voor de korte "
"optie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--bytes>=I<\\,[\\/>+]NUM"
msgstr "B<-c>, B<--bytes>=I<\\,[\\/>+]GETAL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"output the last NUM bytes; or use B<-c> +NUM to output starting with byte "
"NUM of each file"
msgstr ""
"de laatste GETAL bytes uitvoeren; bij 'B<-c> +GETAL' uitvoeren vanaf byte "
"GETAL van elk bestand"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--follow[=>{name|descriptor}]"
msgstr "B<-f>, B<--follow[=>{name|descriptor}]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output appended data as the file grows;"
msgstr "toegevoegde gegevens tonen terwijl bestand groeit;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "an absent option argument means 'descriptor'"
msgstr "een afwezige optie argument betekent 'descriptor'"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-F>"
msgstr "B<-F>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "same as B<--follow>=I<\\,name\\/> B<--retry>"
msgstr "hetzelfde als B<--follow>=I<\\,name\\/> B<--retry>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--lines>=I<\\,[\\/>+]NUM"
msgstr "B<-n>, B<--lines>=I<\\,[\\/>+]GETAL"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "output the last NUM lines, instead of the last 10; or use B<-n> +NUM to "
#| "output starting with line NUM"
msgid ""
"output the last NUM lines, instead of the last 10; or use B<-n> +NUM to skip "
"NUM-1 lines at the start"
msgstr ""
"de laatste GETAL (i.p.v. 10) regels uitvoeren; bij +GETAL uitvoeren vanaf "
"regel GETAL van elk bestand"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--max-unchanged-stats>=I<\\,N\\/>"
msgstr "B<--max-unchanged-stats>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "with B<--follow>=I<\\,name\\/>, reopen a FILE which has not"
msgstr "met B<--follow>=I<\\,name\\/>, een BESTAND heropenen als diens"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"changed size after N (default 5) iterations to see if it has been unlinked "
"or renamed (this is the usual case of rotated log files); with inotify, this "
"option is rarely useful"
msgstr ""
"grootte na N iteraties (standaard 5) onveranderd is, om te zien of het is "
"ontkoppeld of hernoemd (gebruikelijk bij roterende logbestanden); met "
"'inotify' is deze optie zelden nuttig"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--pid>=I<\\,PID\\/>"
msgstr "B<--pid>=I<\\,PID\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "with B<-f>, terminate after process ID, PID dies"
msgstr "met B<-f>, afsluiten wanneer proces PID eindigt"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-q>, B<--quiet>, B<--silent>"
msgstr "B<-q>, B<--quiet>, B<--silent>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "never output headers giving file names"
msgstr "geen bestandsnaam koppen tonen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--retry>"
msgstr "B<--retry>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "keep trying to open a file if it is inaccessible"
msgstr ""
"blijven proberen om een bestand te openen zelfs als het ontoegankelijk is of "
"wordt; nuttig wanneer op naam gevolgd wordt (bij B<--follow=name> dus)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--sleep-interval>=I<\\,N\\/>"
msgstr "B<-s>, B<--sleep-interval>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"with B<-f>, sleep for approximately N seconds (default 1.0) between "
"iterations; with inotify and B<--pid>=I<\\,P\\/>, check process P at least "
"once every N seconds"
msgstr ""
"met B<-f>: tussen twee iteraties ongeveer N seconden slapen (standaard 1 "
"seconde); met 'inotify' en B<--pid>=I<\\,P\\/>: dit proces P minstens eens "
"per N seconden controleren"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "always output headers giving file names"
msgstr "altijd bestandsnaam koppen tonen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr "B<-z>, B<--zero-terminated>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "line delimiter is NUL, not newline"
msgstr "regels afsluiten met 0-byte, niet met nieuweregel"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "toon de helptekst en stop"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "toon programmaversie en stop"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "NUM may have a multiplier suffix: b 512, kB 1000, K 1024, MB 1000*1000, M "
#| "1024*1024, GB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, "
#| "Y.  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgid ""
"NUM may have a multiplier suffix: b 512, kB 1000, K 1024, MB 1000*1000, M "
"1024*1024, GB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y, "
"R, Q.  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"Het GETAL mag gevolgd worden door een vermenigvuldigings achtervoegsel: b = "
"512, kB = 1000, K = 1024, MB = 1000*1000, M = 1024*1024, GB = "
"1000*1000*1000, G = 1024*1024*1024, en zo verder voor T, P, E, Z en Y."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"With B<--follow> (B<-f>), tail defaults to following the file descriptor, "
"which means that even if a tail'ed file is renamed, tail will continue to "
"track its end.  This default behavior is not desirable when you really want "
"to track the actual name of the file, not the file descriptor (e.g., log "
"rotation).  Use B<--follow>=I<\\,name\\/> in that case.  That causes tail to "
"track the named file in a way that accommodates renaming, removal and "
"creation."
msgstr ""
"Met B<--follow> (B<-f>) volgt 'tail' de bestandsdescriptor, wat betekent dat "
"het bestand nog steeds gevolgd wordt als de bestandsnaam wijzigt.  Dit "
"standaardgedrag is ongewenst als u echt de naam wilt volgen en niet de "
"descriptor, bijvoorbeeld bij rotatie van logbestanden.  Gebruik dan B<--"
"follow>=I<\\,name\\/>; dit maakt dat 'tail' het genoemde bestand volgt op "
"een manier die rekening houdt met hernoemen, verwijderen en opnieuw aanmaken."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Written by Paul Rubin, David MacKenzie, Ian Lance Taylor, and Jim Meyering."
msgstr ""
"Geschreven door Paul Rubin, David MacKenzie, Ian Lance Taylor en Jim "
"Meyering."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTEREN VAN BUGS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Online hulp bij GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Meld alle vertaalfouten op E<lt>https://translationproject.org/team/nl."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dit is vrije software: u mag het vrijelijk wijzigen en verder verspreiden. "
"Deze software kent GEEN GARANTIE, voor zover de wet dit toestaat."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<head>(1)"
msgstr "B<head>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/tailE<gt>"
msgstr ""
"Volledige documentatie op: E<lt>https://www.gnu.org/software/coreutils/"
"tailE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) tail invocation\\(aq"
msgstr "of lokaal via: info \\(aq(coreutils) tail invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 mageia-cauldron opensuse-leap-15-6
msgid ""
"output the last NUM lines, instead of the last 10; or use B<-n> +NUM to "
"output starting with line NUM"
msgstr ""
"de laatste GETAL (i.p.v. 10) regels uitvoeren; bij +GETAL uitvoeren vanaf "
"regel GETAL van elk bestand"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"NUM may have a multiplier suffix: b 512, kB 1000, K 1024, MB 1000*1000, M "
"1024*1024, GB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, "
"Y.  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"Het GETAL mag gevolgd worden door een vermenigvuldigings achtervoegsel: b = "
"512, kB = 1000, K = 1024, MB = 1000*1000, M = 1024*1024, GB = "
"1000*1000*1000, G = 1024*1024*1024, en zo verder voor T, P, E, Z en Y."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "April 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "Oktober 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "head(1)"
msgstr "head(1)"
