# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2023-08-27 17:16+0200\n"
"PO-Revision-Date: 2023-01-28 17:44+0100\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "read"
msgstr "read"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-04-03"
msgstr "3 april 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pagina's 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read - read from a file descriptor"
msgstr "read - lees van een bestandbeschrijving"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard C bibliotheek  (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<ssize_t read(int >I<fd>B<, void >I<buf>B<[.>I<count>B<], size_t >I<count>B<);>\n"
msgstr "B<ssize_t read(int >I<fd>B<, void >I<buf>B<[.>I<tel>B<], size_t >I<tel>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<read>()  attempts to read up to I<count> bytes from file descriptor I<fd> "
"into the buffer starting at I<buf>."
msgstr ""
"B<read>() probeert tot aan I<tel> bytes van bestandsbeschrijving I<bes_ind> "
"in te lezen naar de buffer I<buf>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On files that support seeking, the read operation commences at the file "
"offset, and the file offset is incremented by the number of bytes read.  If "
"the file offset is at or past the end of file, no bytes are read, and "
"B<read>()  returns zero."
msgstr ""
"In bestanden die zoeken ondersteunen begint de lees-operatie bij de "
"bestandspositie, en wordt de bestandspositie verhoogd met het aantal gelezen "
"bytes. Als de bestandspositie op of voorbij het einde van het bestand staat, "
"dan worden geen bytes gelezen en geeft B<read>() nul terug."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<count> is zero, B<read>()  I<may> detect the errors described below.  "
"In the absence of any errors, or if B<read>()  does not check for errors, a "
"B<read>()  with a I<count> of 0 returns zero and has no other effects."
msgstr ""
"Als I<tel> nul is, I<kan> B<read>() fouten zoals hieronder beschreven "
"detecteren. In afwezigheid van enige fout, of als B<read>() niet op fouten "
"controleert, zal een B<read>() met een I<tel> gelijk 0 een 0 teruggeven en "
"heeft verder geen andere effecten. "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"According to POSIX.1, if I<count> is greater than B<SSIZE_MAX>, the result "
"is implementation-defined; see NOTES for the upper limit on Linux."
msgstr ""
"Volgens POSIX.1 als I<tel> groter is dan B<SSIZE_MAX> dan is het resultaat "
"implementatie afhankelijk; zie OPMERKINGEN voor de boven grens op Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, the number of bytes read is returned (zero indicates end of "
"file), and the file position is advanced by this number.  It is not an error "
"if this number is smaller than the number of bytes requested; this may "
"happen for example because fewer bytes are actually available right now "
"(maybe because we were close to end-of-file, or because we are reading from "
"a pipe, or from a terminal), or because B<read>()  was interrupted by a "
"signal.  See also NOTES."
msgstr ""
"Bij success wordt het aantal gelezen bytes teruggegeven (nul betekend einde "
"van het bestand); de positie in het bestand wordt met dit aantal "
"vooruitgezet. Het is niet fout als dat aantal kleiner is dan het gevraagde "
"aantal bytes, dat kan bijvoorbeeld gebeuren als er minder bytes voorhanden "
"zijn op dat ogenblik (wellicht omdat we dicht bij eind-van-bestand zijn, "
"omdat we van een pijp lezen, of van een terminal, of omdat B<read>() "
"onderbroken werd door een signaal). Zie ook OPMERKINGEN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On error, -1 is returned, and I<errno> is set to indicate the error.  In "
"this case, it is left unspecified whether the file position (if any) changes."
msgstr ""
"Bij falen wordt -1 teruggegeven, en I<errno> wordt overeenkomstig gezet. In "
"dit geval is niet bepaald of de plaats in het bestand (als die bestaat) "
"veranderd."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The file descriptor I<fd> refers to a file other than a socket and has been "
"marked nonblocking (B<O_NONBLOCK>), and the read would block.  See "
"B<open>(2)  for further details on the B<O_NONBLOCK> flag."
msgstr ""
"De bestandsbeschrijving I<bes_ind> refereert naar een ander bestand dan de "
"socket en is gemarkeerd als niet-blokkerend (B<O_NONBLOCK>, en de B<read> "
"zou blokkeren. Zie B<open>(2) voor meer details over de B<O_NONBLOCK> vlag."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN> or B<EWOULDBLOCK>"
msgstr "B<EAGAIN> of B<EWOULDBLOCK>"

#.  Actually EAGAIN on Linux
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The file descriptor I<fd> refers to a socket and has been marked nonblocking "
"(B<O_NONBLOCK>), and the read would block.  POSIX.1-2001 allows either error "
"to be returned for this case, and does not require these constants to have "
"the same value, so a portable application should check for both "
"possibilities."
msgstr ""
"De bestandsbeschrijving I<bes_ind> refereert naar een socket en werd "
"gemarkeerd als niet-blokkerend (B<O_NONBLOCK>, en de B<read>() zou "
"blokkeren. POSIX.1-2001 staat toe in dit geval een fout te retourneren en "
"vereist niet dat deze constanten dezelfde waarde hebben, waardoor een "
"overdraagbare applicatie op beide mogelijkheden zou moeten controleren."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> is not a valid file descriptor or is not open for reading."
msgstr ""
"I<bes_ind> is geen geldige bestandsbeschrijving, of is niet open voor lezen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<buf> is outside your accessible address space."
msgstr "I<buf> ligt buiten de door u toegankelijke adres ruimte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The call was interrupted by a signal before any data was read; see "
"B<signal>(7)."
msgstr ""
"De aanroep werd onderbroken door een signaal voordat enige data werd "
"gelezen; zie B<signal>(7)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<fd> is attached to an object which is unsuitable for reading; or the file "
"was opened with the B<O_DIRECT> flag, and either the address specified in "
"I<buf>, the value specified in I<count>, or the file offset is not suitably "
"aligned."
msgstr ""
"I<bes_ind> is gekoppeld aan een object dat ongeschikt is om van te lezen; of "
"het bestand werd geopend met de B<O_DIRECT> vlag en ofwel het adres "
"opgegeven in I<buf>, de waarde opgegeven I<tel> of de bestandsplaats is niet "
"goed uitgelijnd."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<fd> was created via a call to B<timerfd_create>(2)  and the wrong size "
"buffer was given to B<read>(); see B<timerfd_create>(2)  for further "
"information."
msgstr ""
"I<bes_ind> werd aangemaakt door een aanroep van B<timerfd_create>(2)  en de "
"verkeerde buffer grootte werd meegegeven aan B<read>(); zie "
"B<timerfd_create>(2) voor meer informatie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I/O error.  This will happen for example when the process is in a background "
"process group, tries to read from its controlling terminal, and either it is "
"ignoring or blocking B<SIGTTIN> or its process group is orphaned.  It may "
"also occur when there is a low-level I/O error while reading from a disk or "
"tape.  A further possible cause of B<EIO> on networked filesystems is when "
"an advisory lock had been taken out on the file descriptor and this lock has "
"been lost.  See the I<Lost locks> section of B<fcntl>(2)  for further "
"details."
msgstr ""
"Invoer/Uitvoer fout. Dit gebeurd bijvoorbeeld als een proces uit een "
"achtergrond-proces-groep probeert van zijn controlerende terminal te lezen "
"en, ofwel het negeert/blokkeert B<SIGTTIN>, ofwel zijn proces-groep is "
"verweesd. Het kan ook optreden als er een laag-niveau Invoer/Uitvoer fout is "
"terwijl er van schijf of tape gelezen wordt. Een andere mogelijke oorzaak "
"van B<EIO> op netwerk bestandssystemen is wanneer een geadviseerd lock werd "
"verwijderd van de bestandsbeschrijving en dat die lock verloren raakte. Zie "
"de I<Verloren locks> sectie van B<fcntl>(2) voor meer details."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EISDIR>"
msgstr "B<EISDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> refers to a directory."
msgstr "I<bes_ind> wijst naar een map."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Other errors may occur, depending on the object connected to I<fd>."
msgstr ""
"Andere fouten kunnen optreden afhankelijk van dat wat verbonden is met I<bi>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "VOLDOET AAN"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHIEDENIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "SVr4, 4.3BSD, POSIX.1-2001."
msgstr "SVr4, 4.3BSD, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#.  commit e28cc71572da38a5a12c1cfe4d7032017adccf69
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On Linux, B<read>()  (and similar system calls) will transfer at most "
"0x7ffff000 (2,147,479,552) bytes, returning the number of bytes actually "
"transferred.  (This is true on both 32-bit and 64-bit systems.)"
msgstr ""
"Op Linux zal B<read>() (en vergelijkbare systeem aanroepen) op zijn hoogst "
"0x7ffff000 (2,147,479,552) bytes overdragen, en het daadwerkelijk aantal "
"overgedragen bytes retourneren. (Dit is waar op beide 32-bit en 64-bit "
"systemen.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On NFS filesystems, reading small amounts of data will update the timestamp "
"only the first time, subsequent calls may not do so.  This is caused by "
"client side attribute caching, because most if not all NFS clients leave "
"I<st_atime> (last file access time)  updates to the server, and client side "
"reads satisfied from the client's cache will not cause I<st_atime> updates "
"on the server as there are no server-side reads.  UNIX semantics can be "
"obtained by disabling client-side attribute caching, but in most situations "
"this will substantially increase server load and decrease performance."
msgstr ""
"Bij NFS bestandssystemen zal het lezen van kleine hoeveelheden gegevens de "
"tijdstempel alleen de eerste keer veranderen, volgende aanroepen laten de "
"tijdstempel onveranderd. Dit wordt veroorzaakt door bufferen van "
"bestandskenmerken aan de zijde van de cliënt: de meeste -zo niet alle- NFS "
"cliënten laten het bijwerken van de `atime' aan de server over; als een lees-"
"opdracht dan genoeg heeft aan de cliënt-kant buffer vind er geen lees-"
"opdracht plaats naar de server en blijft de atime dus onveranderd. UNIX "
"gedrag kan verkregen worden door het bufferen aan de zijde van de cliënt uit "
"te schakelen, maar dat zal in de meeste situaties de last op de server flink "
"vergroten, en zijn prestaties  nadelig beïnvloeden."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"According to POSIX.1-2008/SUSv4 Section XSI 2.9.7 (\"Thread Interactions "
"with Regular File Operations\"):"
msgstr ""
"Volgens POSIX.1-2008/SUSv4 Sectie XSI 2.9.7 (\"Thread interacties met "
"Reguliere Bestand Operaties\"):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All of the following functions shall be atomic with respect to each other in "
"the effects specified in POSIX.1-2008 when they operate on regular files or "
"symbolic links: ..."
msgstr ""
"Alle volgende functie zullen onderling atomair zijn  voor wat betreft de "
"effecten die in POSIX.1-2008 gespecificeerd zijn indien ze werken op "
"reguliere bestanden of symbolische koppelingen: ..."

#.  http://thread.gmane.org/gmane.linux.kernel/1649458
#.     From: Michael Kerrisk (man-pages <mtk.manpages <at> gmail.com>
#.     Subject: Update of file offset on write() etc. is non-atomic with I/O
#.     Date: 2014-02-17 15:41:37 GMT
#.     Newsgroups: gmane.linux.kernel, gmane.linux.file-systems
#.  commit 9c225f2655e36a470c4f58dbbc99244c5fc7f2d4
#.     Author: Linus Torvalds <torvalds@linux-foundation.org>
#.     Date:   Mon Mar 3 09:36:58 2014 -0800
#.         vfs: atomic f_pos accesses as per POSIX
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Among the APIs subsequently listed are B<read>()  and B<readv>(2).  And "
"among the effects that should be atomic across threads (and processes)  are "
"updates of the file offset.  However, before Linux 3.14, this was not the "
"case: if two processes that share an open file description (see B<open>(2))  "
"perform a B<read>()  (or B<readv>(2))  at the same time, then the I/O "
"operations were not atomic with respect updating the file offset, with the "
"result that the reads in the two processes might (incorrectly) overlap in "
"the blocks of data that they obtained.  This problem was fixed in Linux 3.14."
msgstr ""
"Tussen de achtereenvolgens vermelde API´s staan B<read>() en B<readv>(2). En "
"tussen de effecten die atomair zouden moeten zijn voor alle threads (en "
"processen) zijn aanpassingen aan de bestandspositie.  Echter vóór Linux "
"versie 3.14 was dit niet het geval: als twee processen die een open "
"bestandsindicator delen (zie B<open>(2)) een B<read>() (of B<readv>(2)) op "
"hetzelfde tijdstip uitvoeren, dan zijn de Invoer/Uitvoer operaties niet "
"atomair met betrekking tot aanpassen van de bestandspositie, hetgeen erin "
"resulteert dat de lees acties in beide processen (foutief) zou kunnen "
"overlappen in de datablokken die ze zouden verkrijgen. Dit probleem werd "
"opgelost in Linux 3.14."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<close>(2), B<fcntl>(2), B<ioctl>(2), B<lseek>(2), B<open>(2), B<pread>(2), "
"B<readdir>(2), B<readlink>(2), B<readv>(2), B<select>(2), B<write>(2), "
"B<fread>(3)"
msgstr ""
"B<close>(2), B<fcntl>(2), B<ioctl>(2), B<lseek>(2), B<open>(2), B<pread>(2), "
"B<readdir>(2), B<readlink>(2), B<readv>(2), B<select>(2), B<write>(2), "
"B<fread>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-04"
msgstr "4 december 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pagina's 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"The types I<size_t> and I<ssize_t> are, respectively, unsigned and signed "
"integer data types specified by POSIX.1."
msgstr ""
"De typen I<size_t> en I<ssize_t> zijn, respectively, gehele getal typen met "
"of zonder teken zoals gespecificeerd door POSIX.1."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pagina's 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "READ"
msgstr "READ"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2018-02-02"
msgstr "2 februari 2018"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux Programmeurs Handleiding"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<ssize_t read(int >I<fd>B<, void *>I<buf>B<, size_t >I<count>B<);>\n"
msgstr "B<ssize_t read(int >I<bes_ind>B<, void *>I<buf>B<, size_t >I<tel>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On error, -1 is returned, and I<errno> is set appropriately.  In this case, "
"it is left unspecified whether the file position (if any) changes."
msgstr ""
"Bij falen wordt -1 teruggegeven, en I<errno> wordt naar behoren gezet. In "
"dit geval is het onbepaald gelaten ofde plaats in het bestand (als die "
"bestaat) veranderd."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "VOLDOET AAN"

#
#.  http://thread.gmane.org/gmane.linux.kernel/1649458
#.     From: Michael Kerrisk (man-pages <mtk.manpages <at> gmail.com>
#.     Subject: Update of file offset on write() etc. is non-atomic with I/O
#.     Date: 2014-02-17 15:41:37 GMT
#.     Newsgroups: gmane.linux.kernel, gmane.linux.file-systems
#.  commit 9c225f2655e36a470c4f58dbbc99244c5fc7f2d4
#.     Author: Linus Torvalds <torvalds@linux-foundation.org>
#.     Date:   Mon Mar 3 09:36:58 2014 -0800
#.         vfs: atomic f_pos accesses as per POSIX
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Among the APIs subsequently listed are B<read>()  and B<readv>(2).  And "
"among the effects that should be atomic across threads (and processes)  are "
"updates of the file offset.  However, on Linux before version 3.14, this was "
"not the case: if two processes that share an open file description (see "
"B<open>(2))  perform a B<read>()  (or B<readv>(2))  at the same time, then "
"the I/O operations were not atomic with respect updating the file offset, "
"with the result that the reads in the two processes might (incorrectly) "
"overlap in the blocks of data that they obtained.  This problem was fixed in "
"Linux 3.14."
msgstr ""
"Tussen de achtereenvolgende vermelde API´s staan B<read>() en B<readv>(2). "
"En tussen de effecten die atomair zouden moeten zijn voor alle threads (en "
"processen) zijn aanpassingen aan de bestandspositie.  Echter vóór Linux "
"versie 3.14 was dit niet het geval: als twee processen die een open "
"bestandsbeschrijving delen (zie B<open>(2)) een B<read>() (of B<readv>(2)) "
"op hetzelfde tijdstip uitvoeren, dan zijn de Invoer/Uitvoer operaties niet "
"atomair met betrekking tot aanpassen van de bestandspositie, hetgeen "
"resulteert dat de lees acties in beide processen (foutief) zou kunnen "
"overlappen in de datablokken die ze zouden verkrijgen. Dit probleem werd "
"opgelost in Linux 3.14."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 4.16 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
