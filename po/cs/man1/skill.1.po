# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Stinovlas <stinovlas@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:22+0200\n"
"PO-Revision-Date: 2022-05-06 20:48+0200\n"
"Last-Translator: Stinovlas <stinovlas@gmail.com>\n"
"Language-Team: Czech <translation-team-cs@lists.sourceforge.net>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SKILL"
msgstr "SKILL"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2011"
msgstr "Říjen 2011"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr "Příručka uživatele"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: opensuse-leap-15-6
msgid "skill, snice - send a signal or report process status"
msgstr "skill, snice - posílá signál, nebo mění prioritu procesu"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "POUŽITÍ"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<skill> [I<signal>] [I<options>] I<expression>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<snice> [I<new priority>] [I<options>] I<expression>"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "These tools are probably obsolete and unportable. The command syntax is "
#| "poorly defined. Consider using the killall, pkill, and pgrep commands "
#| "instead."
msgid ""
"These tools are obsolete and unportable.  The command syntax is poorly "
"defined.  Consider using the killall, pkill, and pgrep commands instead."
msgstr ""
"Tyto nástroje jsou pravděpodobně již zastaralé a nepřenositelné. Sytnax "
"těchto příkazů je špatně definovaná. Místo těchto příkazů raději použijte "
"killall, pkill a pgrep."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The default signal for skill is TERM.  Use -l or -L to list available "
"signals.  Particularly useful signals include HUP, INT, KILL, STOP, CONT, "
"and 0.  Alternate signals may be specified in three ways: -9 -SIGKILL -KILL."
msgstr ""
"Standartní signál pro skill je TERM. Použijte -l nebo -L ke zjištění "
"dostupných signálů.  Nejpoužívanější signály zahrnují HUP, INT, KILL, STOP, "
"CONT a 0.  Signály mohou být specifikovány třemi způsoby: -9 -SIGKILL -KILL."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The default priority for snice is +4.  Priority numbers range from +20 "
"(slowest) to -20 (fastest).  Negative priority numbers are restricted to "
"administrative users."
msgstr ""
"Implicitně nastavuje snice prioritu procesu na +4.  Priorita procesu je "
"číslo mezi +20 (nejpomalejší) a -20 (nejrychlejší).  Záporné priority jsou "
"vyhrazeny pro administrátory."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "VOLBY"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-f>,B<\\ --fast>"
msgstr "B<-f>,B<\\ --fast>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Fast mode.  This option has not been implemented."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-i>,B<\\ --interactive>"
msgstr "B<-i>,B<\\ --interactive>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "You will be asked to approve each action."
msgid "Interactive use.  You will be asked to approve each action."
msgstr "Před provedením každé akce budete dotázáni.\n"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-l>,B<\\ --list>"
msgstr "B<-l>,B<\\ --list>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "List all signal names."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-L>,B<\\ --table>"
msgstr "B<-L>,B<\\ --table>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "List all signal names in a nice table."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-n>,B<\\ --no-action>"
msgstr "B<-n>,B<\\ --no-action>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"No action; perform a simulation of events that would occur but do not "
"actually change the system."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-v>,B<\\ --verbose>"
msgstr "B<-v>,B<\\ --verbose>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Verbose; explain what is being done."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-w>,B<\\ --warnings>"
msgstr "B<-w>,B<\\ --warnings>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Enable warnings.  This option has not been implemented."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Zobrazí nápovědu a skončí."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "Displays version of program."
msgid "Display version information."
msgstr "Vypíše verzi programu."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "PROCESS SELECTION OPTIONS"
msgstr "KRITÉRIA PRO VÝBĚR PROCESU"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Selection criteria can be: terminal, user, pid, command.  The options below "
"may be used to ensure correct interpretation."
msgstr ""
"Výběrová kritéria mohou být: terminál, uživatel, ID procesu a příkaz.  Volby "
"níže mohou mohou být použity pro ujištění o správné interpretaci."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-t>, B<--tty> I<tty>"
msgstr "B<-t>, B<--tty> I<tty>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "The next argument is a terminal (tty or pty)."
msgid "The next expression is a terminal (tty or pty)."
msgstr "Následující argument je terminál (tty nebo pty)."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-u>, B<--user> I<user>"
msgstr "B<-u>, B<--user> I<uživatel>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "The next argument is a username."
msgid "The next expression is a username."
msgstr "Následující argument je uživatelské jméno."

#. type: TP
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "B<-h>, B<--help>"
msgid "B<-p>, B<--pid> I<pid>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "The next argument is a process ID number."
msgid "The next expression is a process ID number."
msgstr "Následující argument je ID procesu."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-c>, B<--command> I<command>"
msgstr "B<-c>, B<--command> I<příkaz>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "The next argument is a command name."
msgid "The next expression is a command name."
msgstr "Následující argument je jméno příkazu."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<--ns >I<pid>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Match the processes that belong to the same namespace as pid."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<--nslist >I<ns,...>"
msgstr "B<--nslist >I<ns,...>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"list which namespaces will be considered for the --ns option.  Available "
"namespaces: ipc, mnt, net, pid, user, uts."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SIGNALS"
msgstr "SIGNÁLY"

#. type: Plain text
#: opensuse-leap-15-6
msgid "The behavior of signals is explained in B<signal>(7)  manual page."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLES"
msgstr "PŘÍKLADY"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<snice -c seti -c crack +7>"
msgstr "B<snice -c seti -c crack +7>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "Slow down seti and crack"
msgid "Slow down seti and crack commands."
msgstr "Zpomalí seti a crack"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<skill -KILL -t /dev/pts/*>"
msgstr "B<skill -KILL -t /dev/pts/*>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "Kill users on new-style PTY devices"
msgid "Kill users on PTY devices."
msgstr "Zabije uživatele na PTY zařízeních"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<skill -STOP -u viro -u lm -u davem>"
msgstr "B<skill -STOP -u viro -u lm -u davem>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Stop three users."
msgstr "Zastaví tři uživatele."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<kill>(1), B<kill>(2), B<killall>(1), B<nice>(1), B<pkill>(1), "
"B<renice>(1), B<signal>(7)"
msgstr ""
"B<kill>(1), B<kill>(2), B<killall>(1), B<nice>(1), B<pkill>(1), "
"B<renice>(1), B<signal>(7)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: opensuse-leap-15-6
msgid "No standards apply."
msgstr "Nejsou popsány žádné standardy."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"E<.UR albert@users.sf.net> Albert Cahalan E<.UE> wrote skill and snice in "
"1999 as a replacement for a non-free version."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "HLÁŠENÍ CHYB"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Chyby týkající se programu prosím zasílejte na E<.UR procps@freelists.org> "
"E<.UE>"
