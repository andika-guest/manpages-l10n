# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Pavel Heimlich <tropikhajma@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:22+0200\n"
"PO-Revision-Date: 2009-02-09 20:06+0100\n"
"Last-Translator: Pavel Heimlich <tropikhajma@gmail.com>\n"
"Language-Team: Czech <translation-team-cs@lists.sourceforge.net>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "shells"
msgstr "shells"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30. října 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "shells - pathnames of valid login shells"
msgstr "shells - cesty přihlašovacích shellů"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I</etc/shells> is a text file which contains the full pathnames of valid "
"login shells.  This file is consulted by B<chsh>(1)  and available to be "
"queried by other programs."
msgstr ""
"I</etc/shells> je textový soubor obsahující plná jména souborů, které mohou "
"být použity jako přihlašovací shelly. Tento soubor je konzultován programem "
"B<chsh>(1)  a je též možné, že i jinými programy."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Be aware that there are programs which consult this file to find out if a "
"user is a normal user; for example, FTP daemons traditionally disallow "
"access to users with shells not included in this file."
msgstr ""
"Pozor, některé programy používají tento soubor, aby zjistily, zda je "
"uživatel platným uživatelem. Např. FTP démoni obvykle nepovolí přístup "
"uživatelům, jejichž shell není v tomto souboru přítomen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "SOUBORY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</etc/shells>"
msgstr "I</etc/shells>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "PŘÍKLADY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</etc/shells> may contain the following paths:"
msgstr "I</etc/shells> může obsahovat tyto cesty:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"I</bin/sh>\n"
"I</bin/bash>\n"
"I</bin/csh>\n"
msgstr ""
"I</bin/sh>\n"
"I</bin/bash>\n"
"I</bin/csh>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<chsh>(1), B<getusershell>(3), B<pam_shells>(8)"
msgstr "B<chsh>(1), B<getusershell>(3), B<pam_shells>(8)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SHELLS"
msgstr "SHELLS"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-11-26"
msgstr "26. listopadu 2017"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux - příručka programátora"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "PŘÍKLAD"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "TIRÁŽ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Tato stránka je součástí projektu Linux I<man-pages> v4.16. Popis projektu a "
"informace o hlášení chyb najdete na \\%https://www.kernel.org/doc/man\\-"
"pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
