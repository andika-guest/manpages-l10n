# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Artyom Kunyov <artkun@guitarplayer.ru>, 2012.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2012, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Konstantin Shvaykovskiy <kot.shv@gmail.com>, 2012.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:05+0200\n"
"PO-Revision-Date: 2019-09-21 08:19+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<lgamma>():"
msgid "lgamma"
msgstr "B<lgamma>():"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"lgamma, lgammaf, lgammal, lgamma_r, lgammaf_r, lgammal_r, signgam - log "
"gamma function"
msgstr ""
"lgamma, lgammaf, lgammal, lgamma_r, lgammaf_r, lgammal_r, signgam - логарифм "
"гамма-функции"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double lgamma(double >I<x>B<);>\n"
"B<float lgammaf(float >I<x>B<);>\n"
"B<long double lgammal(long double >I<x>B<);>\n"
msgstr ""
"B<double lgamma(double >I<x>B<);>\n"
"B<float lgammaf(float >I<x>B<);>\n"
"B<long double lgammal(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double lgamma_r(double >I<x>B<, int *>I<signp>B<);>\n"
"B<float lgammaf_r(float >I<x>B<, int *>I<signp>B<);>\n"
"B<long double lgammal_r(long double >I<x>B<, int *>I<signp>B<);>\n"
msgstr ""
"B<double lgamma_r(double >I<x>B<, int *>I<signp>B<);>\n"
"B<float lgammaf_r(float >I<x>B<, int *>I<signp>B<);>\n"
"B<long double lgammal_r(long double >I<x>B<, int *>I<signp>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<extern int >I<signgam>B<;>\n"
msgstr "B<extern int >I<signgam>B<;>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L || _XOPEN_SOURCE\n"
#| "    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
#| "    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgid ""
"B<lgamma>():\n"
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L || _XOPEN_SOURCE\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L || _XOPEN_SOURCE\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* в версии glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<lgammaf>(), B<lgammal>():"
msgstr "B<lgammaf>(), B<lgammal>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<lgamma_r>(), B<lgammaf_r>(), B<lgammal_r>():"
msgstr "B<lgamma_r>(), B<lgammaf_r>(), B<lgammal_r>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<signgam>:"
msgstr "I<signgam>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE\n"
"        || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "For the definition of the Gamma function, see B<tgamma>(3)."
msgstr "Определение гамма-функции смотрите в B<tgamma>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<lgamma>(), B<lgammaf>(), and B<lgammal>()  functions return the "
"natural logarithm of the absolute value of the Gamma function.  The sign of "
"the Gamma function is returned in the external integer I<signgam> declared "
"in I<E<lt>math.hE<gt>>.  It is 1 when the Gamma function is positive or "
"zero, -1 when it is negative."
msgstr ""
"Функции B<lgamma>(), B<lgammaf>() и B<lgammal>() возвращают натуральный "
"логарифм абсолютного значения гамма-функции. Знак гамма-функции возвращается "
"во внешнем целом I<signgam>, объявленном в I<E<lt>math.hE<gt>>. Оно равно 1, "
"если гамма-функция положительна или равна нулю, и -1, если она отрицательна."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since using a constant location I<signgam> is not thread-safe, the functions "
"B<lgamma_r>(), B<lgammaf_r>(), and B<lgammal_r>()  have been introduced; "
"they return the sign via the argument I<signp>."
msgstr ""
"Так как использование постоянного расположения I<signgam> не является "
"безопасным при работе с нитями, то была созданы функции B<lgamma_r>(), "
"B<lgammaf_r>() и B<lgammal_r>(); они возвращают знак через аргумент I<signp>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "On success, these functions return the natural logarithm of Gamma(x)."
msgstr ""
"При успешном выполнении данные функции возвращают натуральный логарифм "
"Gamma(x)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is a NaN, a NaN is returned."
msgstr "Если I<x> имеет значение NaN, будет возвращено NaN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is 1 or 2, +0 is returned."
msgstr "Если I<x> равен 1 или 2, то возвращается +0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is positive infinity or negative infinity, positive infinity is "
"returned."
msgstr ""
"Если I<x> равен плюс бесконечности или минус бесконечности, возвращается "
"плюс бесконечность."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is a nonpositive integer, a pole error occurs, and the functions "
"return +B<HUGE_VAL>, +B<HUGE_VALF>, or +B<HUGE_VALL>, respectively."
msgstr ""
"Если I<x> равно не положительному целому, генерируется ошибка особой точки, "
"а функции возвращают +B<HUGE_VAL>, +B<HUGE_VALF> или +B<HUGE_VALL>, "
"соответственно."

#.  e.g., lgamma(DBL_MAX)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the result overflows, a range error occurs, and the functions return "
"B<HUGE_VAL>, B<HUGE_VALF>, or B<HUGE_VALL>, respectively, with the correct "
"mathematical sign."
msgstr ""
"Если происходит переполнение результата, возникает ошибка диапазона, а "
"функция возвращает B<HUGE_VAL>, B<HUGE_VALF> или B<HUGE_VALL>, "
"соответственно, с математически правильным знаком."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"Смотрите B<math_error>(7), чтобы определить, какие ошибки могут возникать "
"при вызове этих функций."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr "Могут возникать следующие ошибки:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Pole error: I<x> is a nonpositive integer"
msgstr "Ошибка особой точки: I<x> равно не положительному целому"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE> (but see BUGS).  A divide-by-zero floating-"
"point exception (B<FE_DIVBYZERO>)  is raised."
msgstr ""
"I<errno> устанавливается в B<ERANGE> (но смотрите ДЕФЕКТЫ). Возникает "
"исключение деления плавающей запятой на ноль (B<FE_DIVBYZERO>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result overflow"
msgstr "Ошибка диапазона: результат превысил разрядность"

#.  glibc (as at 2.8) also supports an inexact
#.  exception for various cases.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE>.  An overflow floating-point exception "
"(B<FE_OVERFLOW>)  is raised."
msgstr ""
"Значение I<errno> устанавливается в B<ERANGE>. Возникает исключение "
"переполнения плавающей запятой (B<FE_OVERFLOW>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<lgamma>():"
msgid "B<lgamma>()"
msgstr "B<lgamma>():"

#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<lgamma>():"
msgid "B<lgammaf>()"
msgstr "B<lgamma>():"

#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<lgamma>():"
msgid "B<lgammal>()"
msgstr "B<lgamma>():"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "I<signgam>:"
msgid "I<signgam>"
msgstr "I<signgam>:"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<lgamma>():"
msgid "B<lgamma_r>()"
msgstr "B<lgamma>():"

#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<lgamma>():"
msgid "B<lgammaf_r>()"
msgstr "B<lgamma>():"

#. type: TQ
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<lgamma>():"
msgid "B<lgammal_r>()"
msgstr "B<lgamma>():"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "None"
msgid "None."
msgstr "None"

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C99, POSIX.1-2001."
msgstr "C99, POSIX.1-2001."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#. #-#-#-#-#  archlinux: lgamma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6777
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: lgamma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6777
#. type: Plain text
#. #-#-#-#-#  debian-unstable: lgamma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6777
#. type: Plain text
#. #-#-#-#-#  fedora-39: lgamma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6777
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: lgamma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6777
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: lgamma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6777
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: lgamma.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6777
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In glibc 2.9 and earlier, when a pole error occurs, I<errno> is set to "
#| "B<EDOM>; instead of the POSIX-mandated B<ERANGE>.  Since version 2.10, "
#| "glibc does the right thing."
msgid ""
"In glibc 2.9 and earlier, when a pole error occurs, I<errno> is set to "
"B<EDOM>; instead of the POSIX-mandated B<ERANGE>.  Since glibc 2.10, glibc "
"does the right thing."
msgstr ""
"В glibc 2.9 и ранее при возникновении ошибки особой точки, значению I<errno> "
"присваивается B<EDOM> вместо B<ERANGE> согласно POSIX. Начиная с версии "
"2.10, в glibc это исправлено."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<tgamma>(3)"
msgstr "B<tgamma>(3)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"The B<lgamma>()  functions are specified in C99, POSIX.1-2001, and "
"POSIX.1-2008.  I<signgam> is specified in POSIX.1-2001 and POSIX.1-2008, but "
"not in C99.  The B<lgamma_r>()  functions are nonstandard, but present on "
"several other systems."
msgstr ""
"Функции B<lgamma>() определены в C99, POSIX.1-2001 и POSIX.1-2008. "
"Переменная I<signgam> определена в POSIX.1-2001 и POSIX.1-2008, но "
"отсутствует в C99. Функции B<lgamma_r>() являются нестандартными, но "
"существуют в нескольких других системах."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "LGAMMA"
msgstr "LGAMMA"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Link with I<-lm>."
msgstr "Компонуется при указании параметра I<-lm>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<lgamma>():"
msgstr "B<lgamma>():"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L || _XOPEN_SOURCE\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L || _XOPEN_SOURCE\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* в версии glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* в версиях glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"/* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"/* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* в версии glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_XOPEN_SOURCE\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* версии glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6777
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"In glibc 2.9 and earlier, when a pole error occurs, I<errno> is set to "
"B<EDOM>; instead of the POSIX-mandated B<ERANGE>.  Since version 2.10, glibc "
"does the right thing."
msgstr ""
"В glibc 2.9 и ранее при возникновении ошибки особой точки, значению I<errno> "
"присваивается B<EDOM> вместо B<ERANGE> согласно POSIX. Начиная с версии "
"2.10, в glibc это исправлено."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
