# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Alexey <a.chepugov@gmail.com>, 2015.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# ITriskTI <ITriskTI@gmail.com>, 2013.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Малянов Евгений Викторович <maljanow@outlook.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:09+0200\n"
"PO-Revision-Date: 2019-10-06 08:59+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<mq_open>()"
msgid "mq_open"
msgstr "B<mq_open>()"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr "20 июля 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mq_open - open a message queue"
msgstr "mq_open - открывает очередь сообщений"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Real-time library (I<librt>, I<-lrt>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>>           /* For O_* constants */\n"
"B<#include E<lt>sys/stat.hE<gt>>        /* For mode constants */\n"
"B<#include E<lt>mqueue.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>>           /* Постоянные вида O_* */\n"
"B<#include E<lt>sys/stat.hE<gt>>        /* Постоянные для mode */\n"
"B<#include E<lt>mqueue.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<mqd_t mq_open(const char *>I<name>B<, int >I<oflag>B<);>\n"
"B<mqd_t mq_open(const char *>I<name>B<, int >I<oflag>B<, mode_t >I<mode>B<,>\n"
"B<              struct mq_attr *>I<attr>B<);>\n"
msgstr ""
"B<mqd_t mq_open(const char *>I<name>B<, int >I<oflag>B<);>\n"
"B<mqd_t mq_open(const char *>I<name>B<, int >I<oflag>B<, mode_t >I<mode>B<,>\n"
"B<              struct mq_attr *>I<attr>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mq_open>()  creates a new POSIX message queue or opens an existing queue.  "
"The queue is identified by I<name>.  For details of the construction of "
"I<name>, see B<mq_overview>(7)."
msgstr ""
"Функция B<mq_open>() создает новую очередь сообщений POSIX или открывает "
"существующую очередь. Очередь опознаётся по имени I<name>. Для получения "
"дополнительной информации о создании имени I<name>, смотрите "
"B<mq_overview>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<oflag> argument specifies flags that control the operation of the "
"call.  (Definitions of the flags values can be obtained by including "
"I<E<lt>fcntl.hE<gt>>.)  Exactly one of the following must be specified in "
"I<oflag>:"
msgstr ""
"В параметре I<oflag> задаются флаги, которые управляют работой вызова "
"(значения флагов могут быть получены при включении I<E<lt>fcntl.hE<gt>>). "
"Поместить в параметр I<oflag> можно только один из ниже приведенных флагов:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<O_RDONLY>"
msgstr "B<O_RDONLY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Open the queue to receive messages only."
msgstr "Открыть очередь только для получения сообщений."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<O_WRONLY>"
msgstr "B<O_WRONLY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Open the queue to send messages only."
msgstr "Открыть очередь только для отправки сообщений."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<O_RDWR>"
msgstr "B<O_RDWR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Open the queue to both send and receive messages."
msgstr "Открыть очередь для отправки и получения сообщений."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Zero or more of the following flags can additionally be I<OR>ed in I<oflag>:"
msgstr ""
"Также в  I<oflag> можно добавить ноль и более флагов, объединённых через ИЛИ:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<O_CLOEXEC> (since Linux 2.6.26)"
msgstr "B<O_CLOEXEC> (начиная с Linux 2.6.26)"

#.  commit 269f21344b23e552c21c9e2d7ca258479dcd7a0a
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set the close-on-exec flag for the message queue descriptor.  See "
"B<open>(2)  for a discussion of why this flag is useful."
msgstr ""
"Установить флаг close-on-exec на файловом дескрипторе очереди сообщений. "
"Описание полезности этого флага смотрите в B<open>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<O_CREAT>"
msgstr "B<O_CREAT>"

#.  In reality the filesystem IDs are used on Linux.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Create the message queue if it does not exist.  The owner (user ID) of the "
"message queue is set to the effective user ID of the calling process.  The "
"group ownership (group ID) is set to the effective group ID of the calling "
"process."
msgstr ""
"Создать очередь сообщений, если она не существует. Владельцем (ID "
"пользователя) очереди сообщений назначается эффективный ID пользователя "
"вызывающего процесса. Владельцем-группой (ID группы) назначается эффективный "
"ID группы вызывающего процесса."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<O_EXCL>"
msgstr "B<O_EXCL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If B<O_CREAT> was specified in I<oflag>, and a queue with the given I<name> "
"already exists, then fail with the error B<EEXIST>."
msgstr ""
"Если в поле I<oflag> выставлен флаг B<O_CREAT> и очередь с заданным именем "
"I<name>  уже существует, то завершить вызов ошибкой B<EEXIST>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<O_NONBLOCK>"
msgstr "B<O_NONBLOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Open the queue in nonblocking mode.  In circumstances where "
"B<mq_receive>(3)  and B<mq_send>(3)  would normally block, these functions "
"instead fail with the error B<EAGAIN>."
msgstr ""
"Открыть очередь в неблокирующем режиме. При обстоятельствах, из-за которых "
"B<mq_receive>(3) и B<mq_send>(3), обычно, блокируются, теперь эти функции "
"будут завершаться ошибкой B<EAGAIN>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If B<O_CREAT> is specified in I<oflag>, then two additional arguments must "
"be supplied.  The I<mode> argument specifies the permissions to be placed on "
"the new queue, as for B<open>(2).  (Symbolic definitions for the permissions "
"bits can be obtained by including I<E<lt>sys/stat.hE<gt>>.)  The permissions "
"settings are masked against the process umask."
msgstr ""
"Если в I<oflag> указан B<O_CREAT>, требуется задать два дополнительных "
"аргумента. В аргументе I<mode> задаются права доступа к новой очереди, как "
"для B<open>(2) (символические определения бит прав можно получить, включив "
"I<E<lt>sys/stat.hE<gt>>). В настройках прав учитывается umask процесса."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The fields of the I<struct mq_attr> pointed to I<attr> specify the maximum "
"number of messages and the maximum size of messages that the queue will "
"allow.  This structure is defined as follows:"
msgstr ""
"В полях структуры I<struct mq_attr>, на которую указывает I<attr>, задаётся "
"максимальное количество сообщений и максимальный размер сообщений, "
"разрешённых в очереди. Эта структура определена следующим образом:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct mq_attr {\n"
"    long mq_flags;       /* Flags (ignored for mq_open()) */\n"
"    long mq_maxmsg;      /* Max. # of messages on queue */\n"
"    long mq_msgsize;     /* Max. message size (bytes) */\n"
"    long mq_curmsgs;     /* # of messages currently in queue\n"
"                            (ignored for mq_open()) */\n"
"};\n"
msgstr ""
"struct mq_attr {\n"
"    long mq_flags;       /* флаги (игнорируются в mq_open()) */\n"
"    long mq_maxmsg;      /* макс. кол-во сообщений в очереди */\n"
"    long mq_msgsize;     /* макс. размер сообщения (в байтах) */\n"
"    long mq_curmsgs;     /* кол-во сообщений в очереди в данный момент\n"
"                            (игнорируется в mq_open()) */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Only the I<mq_maxmsg> and I<mq_msgsize> fields are employed when calling "
"B<mq_open>(); the values in the remaining fields are ignored."
msgstr ""
"В функции B<mq_open>() используются только поля I<mq_maxmsg> и "
"I<mq_msgsize>; остальные значения полей игнорируются."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<attr> is NULL, then the queue is created with implementation-defined "
"default attributes.  Since Linux 3.5, two I</proc> files can be used to "
"control these defaults; see B<mq_overview>(7)  for details."
msgstr ""
"Если I<attr> равно NULL, то очередь создаётся с атрибутами по умолчанию, "
"зависящими от реализации. Начиная с Linux 3.5, для управления атрибутами по "
"умолчанию можно управлять через два файла в I</proc>; подробности смотрите в "
"B<mq_overview>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, B<mq_open>()  returns a message queue descriptor for use by "
"other message queue functions.  On error, B<mq_open>()  returns I<(mqd_t)\\ "
"-1>, with I<errno> set to indicate the error."
msgstr ""
"При успешном выполнении B<mq_open>()  возвращает файловый дескриптор очереди "
"для использования в других функциях работы с очередями сообщений. При ошибке "
"B<mq_open>() возвращает I<(mqd_t)\\ -1>, а в I<errno> записывается код "
"ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The queue exists, but the caller does not have permission to open it in the "
"specified mode."
msgstr ""
"Очередь существует, но вызывающий не имеет прав для её открытия с заданным в "
"I<mode> режиме."

#.  Note that this isn't consistent with the same case for sem_open()
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<name> contained more than one slash."
msgstr "Поле I<name> содержит больше чем одну косую черту."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Both B<O_CREAT> and B<O_EXCL> were specified in I<oflag>, but a queue with "
"this I<name> already exists."
msgstr ""
"В I<oflag> указаны B<O_CREAT> и B<O_EXCL>, но очередь I<name> уже существует."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#.  glibc checks whether the name starts with a "/" and if not,
#.  gives this error
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<name> doesn't follow the format in B<mq_overview>(7)."
msgstr ""
"Параметр I<name> не соответствует формату, описанному в B<mq_overview>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<O_CREAT> was specified in I<oflag>, and I<attr> was not NULL, but I<attr-"
"E<gt>mq_maxmsg> or I<attr-E<gt>mq_msqsize> was invalid.  Both of these "
"fields must be greater than zero.  In a process that is unprivileged (does "
"not have the B<CAP_SYS_RESOURCE> capability), I<attr-E<gt>mq_maxmsg> must be "
"less than or equal to the I<msg_max> limit, and I<attr-E<gt>mq_msgsize> must "
"be less than or equal to the I<msgsize_max> limit.  In addition, even in a "
"privileged process, I<attr-E<gt>mq_maxmsg> cannot exceed the B<HARD_MAX> "
"limit.  (See B<mq_overview>(7)  for details of these limits.)"
msgstr ""
"В I<oflag> указан B<O_CREAT> и I<attr> не равно NULL, но в I<attr-"
"E<gt>mq_maxmsg> или I<attr-E<gt>mq_msqsize>  содержится некорректное "
"значение. Оба этих поля должны быть больше нуля. Если процесс без прав (не "
"имеет мандата B<CAP_SYS_RESOURCE>), то I<attr-E<gt>mq_maxmsg> должно быть "
"меньше или равно ограничению I<msg_max> и I<attr-E<gt>mq_msgsize> должно "
"быть меньше или равно ограничению I<msgsize_max>. Также, даже для "
"привилегированных процессов, значение I<attr-E<gt>mq_maxmsg> не должно "
"превышать ограничения B<HARD_MAX> (описание ограничений смотрите в "
"B<mq_overview>(7))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file and message queue "
"descriptors has been reached (see the description of B<RLIMIT_NOFILE> in "
"B<getrlimit>(2))."
msgstr ""
"Было достигнуто ограничение на количество открытых дескрипторов файлов и "
"очередей сообщений (смотрите описание B<RLIMIT_NOFILE> в B<getrlimit>(2))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<name> was too long."
msgstr "Слишком длинное значение аргумента I<name>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files and message queues "
"has been reached."
msgstr ""
"Достигнуто ограничение на общее количество открытых файлов в системе и "
"очередей сообщений."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<O_CREAT> flag was not specified in I<oflag>, and no queue with this "
"I<name> exists."
msgstr "В I<oflag> не указан B<O_CREAT> и не существует очередь I<name>."

#.  Note that this isn't consistent with the same case for sem_open()
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<name> was just \"/\" followed by no other characters."
msgstr "В I<name> есть только «/» и нет других символов."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient memory."
msgstr "Недостаточно памяти."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Insufficient space for the creation of a new message queue.  This probably "
"occurred because the I<queues_max> limit was encountered; see "
"B<mq_overview>(7)."
msgstr ""
"Недостаточно места для создания новой очереди сообщений. Вероятно, это "
"произошло из-за ограничения I<queues_max>; смотрите B<mq_overview>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<mq_open>()"
msgstr "B<mq_open>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Отличия между библиотекой C и ядром"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<mq_open>()  library function is implemented on top of a system call of "
"the same name.  The library function performs the check that the I<name> "
"starts with a slash (/), giving the B<EINVAL> error if it does not.  The "
"kernel system call expects I<name> to contain no preceding slash, so the C "
"library function passes I<name> without the preceding slash (i.e., "
"I<name+1>)  to the system call."
msgstr ""
"Библиотечная функция B<mq_open>() реализована поверх системного вызова с тем "
"же именем. Библиотечная функция выполняет проверку того, что I<name> "
"начинается с косой черты (/) и выдаёт ошибку B<EINVAL>, если это не так. "
"Системный вызов ядра ожидает I<name> без начальной косой черты, поэтому "
"библиотечная функция C  передаёт в системный вызов I<name> без начальной "
"косой черты (т. е., I<name+1>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In kernels before 2.6.14, the process umask was not applied to the "
#| "permissions specified in I<mode>."
msgid ""
"Before Linux 2.6.14, the process umask was not applied to the permissions "
"specified in I<mode>."
msgstr ""
"В ядрах до версии 2.6.14, значение umask процесса не накладывалось на права, "
"указанные в I<mode>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mq_close>(3), B<mq_getattr>(3), B<mq_notify>(3), B<mq_receive>(3), "
"B<mq_send>(3), B<mq_unlink>(3), B<mq_overview>(7)"
msgstr ""
"B<mq_close>(3), B<mq_getattr>(3), B<mq_notify>(3), B<mq_receive>(3), "
"B<mq_send>(3), B<mq_unlink>(3), B<mq_overview>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "MQ_OPEN"
msgstr "MQ_OPEN"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Link with I<-lrt>."
msgstr "Компонуется при указании параметра I<-lrt>."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"In kernels before 2.6.14, the process umask was not applied to the "
"permissions specified in I<mode>."
msgstr ""
"В ядрах до версии 2.6.14, значение umask процесса не накладывалось на права, "
"указанные в I<mode>."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
