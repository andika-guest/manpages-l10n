# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014, 2016-2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:29+0200\n"
"PO-Revision-Date: 2019-09-16 18:46+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "timeradd"
msgstr ""

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"timeradd, timersub, timercmp, timerclear, timerisset - timeval operations"
msgstr ""
"timeradd, timersub, timercmp, timerclear, timerisset - операции со "
"структурой timeval"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/time.hE<gt>>\n"
msgstr "B<#include E<lt>sys/time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<void timeradd(struct timeval *>I<a>B<, struct timeval *>I<b>B<,>\n"
#| "B<              struct timeval *>I<res>B<);>\n"
msgid ""
"B<void timeradd(struct timeval *>I<a>B<, struct timeval *>I<b>B<,>\n"
"B<              struct timeval *>I<res>B<);>\n"
"B<void timersub(struct timeval *>I<a>B<, struct timeval *>I<b>B<,>\n"
"B<              struct timeval *>I<res>B<);>\n"
msgstr ""
"B<void timeradd(struct timeval *>I<a>B<, struct timeval *>I<b>B<,>\n"
"B<              struct timeval *>I<res>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int timercmp(struct timeval *>I<a>B<, struct timeval *>I<b>B<, >I<CMP>B<);>\n"
msgid ""
"B<void timerclear(struct timeval *>I<tvp>B<);>\n"
"B<int timerisset(struct timeval *>I<tvp>B<);>\n"
msgstr "B<int timercmp(struct timeval *>I<a>B<, struct timeval *>I<b>B<, >I<CMP>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int timercmp(struct timeval *>I<a>B<, struct timeval *>I<b>B<, >I<CMP>B<);>\n"
msgstr "B<int timercmp(struct timeval *>I<a>B<, struct timeval *>I<b>B<, >I<CMP>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "All functions shown above:"
msgstr "Все функции, показанные выше:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"    Начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    В версии glibc 2.19 и более ранних:\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The macros are provided to operate on I<timeval> structures, defined in "
"I<E<lt>sys/time.hE<gt>> as:"
msgstr ""
"Макросы предназначены для работы со структурой I<timeval>, которая "
"определена в I<E<lt>sys/time.hE<gt>> следующим образом:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct timeval {\n"
"    time_t      tv_sec;     /* seconds */\n"
"    suseconds_t tv_usec;    /* microseconds */\n"
"};\n"
msgstr ""
"struct timeval {\n"
"    time_t      tv_sec;     /* секунды */\n"
"    suseconds_t tv_usec;    /* микросекунды */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<timeradd>()  adds the time values in I<a> and I<b>, and places the sum in "
"the I<timeval> pointed to by I<res>.  The result is normalized such that "
"I<res-E<gt>tv_usec> has a value in the range 0 to 999,999."
msgstr ""
"Макрос B<timeradd>() складывает значения времени I<a> и I<b>, и помещает "
"сумму в I<timeval>, на которую указывает I<res>. Результат нормализуется "
"так, что значение I<res-E<gt>tv_usec> лежит в диапазоне от 0 до 999999."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<timersub>()  subtracts the time value in I<b> from the time value in I<a>, "
"and places the result in the I<timeval> pointed to by I<res>.  The result is "
"normalized such that I<res-E<gt>tv_usec> has a value in the range 0 to "
"999,999."
msgstr ""
"Макрос B<timersub>() вычитает значение времени I<b> из I<a>, и помещает "
"результат в I<timeval>, на которую указывает I<res>. Результат нормализуется "
"так, что значение I<res-E<gt>tv_usec> лежит в диапазоне от 0 до 999999."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<timerclear>()  zeros out the I<timeval> structure pointed to by I<tvp>, so "
"that it represents the Epoch: 1970-01-01 00:00:00 +0000 (UTC)."
msgstr ""
"Макрос B<timerclear>() обнуляет структуру I<timeval>, на которую указывает "
"I<tvp>; полученное значение соответствует представлению эпохи: 1970-01-01 "
"00:00:00 +0000 (UTC)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<timerisset>()  returns true (nonzero) if either field of the I<timeval> "
"structure pointed to by I<tvp> contains a nonzero value."
msgstr ""
"Макрос B<timerisset>() возвращает истину (не ноль), если любое из полей "
"структуры I<timeval>, на которую указывает I<tvp>, содержит ненулевое "
"значение."

#.  HP-UX, Tru64, Irix have a definition like:
#. #define timercmp(tvp, uvp, cmp) #.     ((tvp)->tv_sec cmp (uvp)->tv_sec || #.     (tvp)->tv_sec == (uvp)->tv_sec && (tvp)->tv_usec cmp (uvp)->tv_usec)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<timercmp>()  compares the timer values in I<a> and I<b> using the "
"comparison operator I<CMP>, and returns true (nonzero) or false (0) "
"depending on the result of the comparison.  Some systems (but not Linux/"
"glibc), have a broken B<timercmp>()  implementation, in which I<CMP> of "
"I<E<gt>=>, I<E<lt>=>, and I<==> do not work; portable applications can "
"instead use"
msgstr ""
"Макрос B<timercmp>() сравнивает значения таймера I<a> и I<b> с помощью "
"оператора сравнения I<CMP>, и возвращает истину (не ноль) или ложь (0), в "
"зависимости от результата сравнения. Некоторые системы (но не Linux/glibc) "
"содержат некорректную реализацию B<timercmp>(), в которой I<CMP> для "
"I<E<gt>=>, I<E<lt>=> и I<==> не работает; переносимые приложения могут "
"использовать вместо неё"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    !timercmp(..., E<lt>)\n"
#| "    !timercmp(..., E<gt>)\n"
#| "    !timercmp(..., !=)\n"
msgid ""
"!timercmp(..., E<lt>)\n"
"!timercmp(..., E<gt>)\n"
"!timercmp(..., !=)\n"
msgstr ""
"    !timercmp(..., E<lt>)\n"
"    !timercmp(..., E<gt>)\n"
"    !timercmp(..., !=)\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<timerisset>()  and B<timercmp>()  return true (nonzero) or false (0)."
msgstr ""
"Макросы B<timerisset>() и B<timercmp>() возвращают истину (не ноль) или ложь "
"(0)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "No errors are defined."
msgstr "Ошибок не предполагается."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "None"
msgid "None."
msgstr "None"

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "BSD."
msgstr "BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<gettimeofday>(2), B<time>(7)"
msgstr "B<gettimeofday>(2), B<time>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "Not in POSIX.1.  Present on most BSD derivatives."
msgstr "Не являются частью POSIX.1. Присутствует в вариациях BSD."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "TIMERADD"
msgstr "TIMERADD"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<void timeradd(struct timeval *>I<a>B<, struct timeval *>I<b>B<,>\n"
"B<              struct timeval *>I<res>B<);>\n"
msgstr ""
"B<void timeradd(struct timeval *>I<a>B<, struct timeval *>I<b>B<,>\n"
"B<              struct timeval *>I<res>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<void timersub(struct timeval *>I<a>B<, struct timeval *>I<b>B<,>\n"
"B<              struct timeval *>I<res>B<);>\n"
msgstr ""
"B<void timersub(struct timeval *>I<a>B<, struct timeval *>I<b>B<,>\n"
"B<              struct timeval *>I<res>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<void timerclear(struct timeval *>I<tvp>B<);>\n"
msgstr "B<void timerclear(struct timeval *>I<tvp>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int timerisset(struct timeval *>I<tvp>B<);>\n"
msgstr "B<int timerisset(struct timeval *>I<tvp>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"All functions shown above:\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"Все вышеперечисленные функции:\n"
"    начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 и старее:\n"
"        _BSD_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"    !timercmp(..., E<lt>)\n"
"    !timercmp(..., E<gt>)\n"
"    !timercmp(..., !=)\n"
msgstr ""
"    !timercmp(..., E<lt>)\n"
"    !timercmp(..., E<gt>)\n"
"    !timercmp(..., !=)\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
