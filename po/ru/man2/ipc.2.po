# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:03+0200\n"
"PO-Revision-Date: 2019-10-05 08:17+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ipc"
msgstr "ipc"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ipc - System V IPC system calls"
msgstr "ipc - системные вызовы System V IPC"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<#include E<lt>fcntl.hE<gt>           >/* Definition of AT_* constants */\n"
#| "B<#include E<lt>unistd.hE<gt>>\n"
msgid ""
"B<#include E<lt>linux/ipc.hE<gt>>        /* Definition of needed constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* определения констант of AT_* */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int syscall(SYS_ipc, unsigned int >I<call>B<, int >I<first>B<,>\n"
"B<            unsigned long >I<second>B<, unsigned long >I<third>B<, void *>I<ptr>B<,>\n"
"B<            long >I<fifth>B<);>\n"
msgstr ""
"B<int syscall(SYS_ipc, unsigned int >I<call>B<, int >I<first>B<,>\n"
"B<            unsigned long >I<second>B<, unsigned long >I<third>B<, void *>I<ptr>B<,>\n"
"B<            long >I<fifth>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Glibc does not provide a wrapper for B<finit_module>(); call it using "
#| "B<syscall>(2)."
msgid ""
"I<Note>: glibc provides no wrapper for B<ipc>(), necessitating the use of "
"B<syscall>(2)."
msgstr ""
"В glibc нет обёртки для B<finit_module>(); запускайте его с помощью "
"B<syscall>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<ipc>()  is a common kernel entry point for the System\\ V IPC calls for "
"messages, semaphores, and shared memory.  I<call> determines which IPC "
"function to invoke; the other arguments are passed through to the "
"appropriate call."
msgstr ""
"B<ipc>() является обобщённым системным вызовом для работы с сообщениями, "
"семафорами и разделяемой памятью согласно System\\ V IPC (InterProcess "
"Communication — межпроцессное взаимодействие). В параметре I<call> задаётся "
"какая функция IPC вызывается; значения других аргументов определяется "
"используемой функцией."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"User-space programs should call the appropriate functions by their usual "
"names.  Only standard library implementors and kernel hackers need to know "
"about B<ipc>()."
msgstr ""
"Программы пространства пользователя должны вызывать соответствующие функции "
"через их обычные имена. Только тем, кто пишет стандартные библиотеки и "
"хакерам ядра может понадобиться знать о вызове B<ipc>()."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On some architectures\\(emfor example x86-64 and ARM\\(emthere is no "
#| "B<ipc>()  system call; instead, B<msgctl>(2), B<semctl>(2), B<shmctl>(2), "
#| "and so on really are implemented as separate system calls."
msgid ""
"On some architectures\\[em]for example x86-64 and ARM\\[em]there is no "
"B<ipc>()  system call; instead, B<msgctl>(2), B<semctl>(2), B<shmctl>(2), "
"and so on really are implemented as separate system calls."
msgstr ""
"На некоторых архитектурах, например x86-64 и ARM, нет системного вызова "
"B<ipc>(); вместо него реализованы системные вызовы B<msgctl>(2), "
"B<semctl>(2), B<shmctl>(2) и т.д."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<msgctl>(2), B<msgget>(2), B<msgrcv>(2), B<msgsnd>(2), B<semctl>(2), "
"B<semget>(2), B<semop>(2), B<semtimedop>(2), B<shmat>(2), B<shmctl>(2), "
"B<shmdt>(2), B<shmget>(2), B<sysvipc>(7)"
msgstr ""
"B<msgctl>(2), B<msgget>(2), B<msgrcv>(2), B<msgsnd>(2), B<semctl>(2), "
"B<semget>(2), B<semop>(2), B<semtimedop>(2), B<shmat>(2), B<shmctl>(2), "
"B<shmdt>(2), B<shmget>(2), B<sysvipc>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<ipc>()  is Linux-specific, and should not be used in programs intended to "
"be portable."
msgstr ""
"Вызов B<ipc>() есть только в Linux и не должен использоваться в переносимых "
"программах."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "IPC"
msgstr "IPC"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<int ipc(unsigned int >I<call>B<, int >I<first>B<, int >I<second>B<, int >I<third>B<,>\n"
"B<        void *>I<ptr>B<, long >I<fifth>B<);>\n"
msgstr ""
"B<int ipc(unsigned int >I<call>B<, int >I<first>B<, int >I<second>B<, int >I<third>B<,>\n"
"B<        void *>I<ptr>B<, long >I<fifth>B<);>\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On some architectures\\(emfor example x86-64 and ARM\\(emthere is no "
"B<ipc>()  system call; instead, B<msgctl>(2), B<semctl>(2), B<shmctl>(2), "
"and so on really are implemented as separate system calls."
msgstr ""
"На некоторых архитектурах, например x86-64 и ARM, нет системного вызова "
"B<ipc>(); вместо него реализованы системные вызовы B<msgctl>(2), "
"B<semctl>(2), B<shmctl>(2) и т.д."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<msgctl>(2), B<msgget>(2), B<msgrcv>(2), B<msgsnd>(2), B<semctl>(2), "
"B<semget>(2), B<semop>(2), B<semtimedop>(2), B<shmat>(2), B<shmctl>(2), "
"B<shmdt>(2), B<shmget>(2), B<svipc>(7)"
msgstr ""
"B<msgctl>(2), B<msgget>(2), B<msgrcv>(2), B<msgsnd>(2), B<semctl>(2), "
"B<semget>(2), B<semop>(2), B<semtimedop>(2), B<shmat>(2), B<shmctl>(2), "
"B<shmdt>(2), B<shmget>(2), B<svipc>(7)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
