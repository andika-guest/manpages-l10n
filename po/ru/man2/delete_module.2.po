# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:54+0200\n"
"PO-Revision-Date: 2019-10-05 07:56+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "delete_module"
msgstr "delete_module"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "create_module - create a loadable module entry"
msgid "delete_module - unload a kernel module"
msgstr "create_module - создать элемент загружаемого модуля"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>>            /* Definition of B<O_*> constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>>            /* определения констант B<O_*> */\n"
"B<#include E<lt>sys/syscall.hE<gt>>      /* определения констант B<SYS_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "B<int syscall(SYS_delete_module, const char *>I<name>B<, unsigned int >I<flags>B<);>\n"
msgstr "B<int syscall(SYS_delete_module, const char *>I<name>B<, unsigned int >I<flags>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
msgid ""
"I<Note>: glibc provides no wrapper for B<delete_module>(), necessitating the "
"use of B<syscall>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<delete_module>()  system call attempts to remove the unused loadable "
"module entry identified by I<name>.  If the module has an I<exit> function, "
"then that function is executed before unloading the module.  The I<flags> "
"argument is used to modify the behavior of the system call, as described "
"below.  This system call requires privilege."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Module removal is attempted according to the following rules:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "(1)"
msgstr "(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If there are other loaded modules that depend on (i.e., refer to symbols "
"defined in) this module, then the call fails."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "(2)"
msgstr "(2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Otherwise, if the reference count for the module (i.e., the number of "
"processes currently using the module)  is zero, then the module is "
"immediately unloaded."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "(3)"
msgstr "(3)"

#.   	O_TRUNC == KMOD_REMOVE_FORCE in kmod library
#.   	O_NONBLOCK == KMOD_REMOVE_NOWAIT in kmod library
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a module has a nonzero reference count, then the behavior depends on the "
"bits set in I<flags>.  In normal usage (see NOTES), the B<O_NONBLOCK> flag "
"is always specified, and the B<O_TRUNC> flag may additionally be specified."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "The various combinations for I<flags> have the following effect:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<flags == O_NONBLOCK>"
msgstr "B<flags == O_NONBLOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "The call returns immediately, with an error."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<flags == (O_NONBLOCK | O_TRUNC)>"
msgstr "B<flags == (O_NONBLOCK | O_TRUNC)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The module is unloaded immediately, regardless of whether it has a nonzero "
"reference count."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<(flags & O_NONBLOCK) == 0>"
msgstr "B<(flags & O_NONBLOCK) == 0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<flags> does not specify B<O_NONBLOCK>, the following steps occur:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "The module is marked so that no new references are permitted."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the module's reference count is nonzero, the caller is placed in an "
"uninterruptible sleep state (B<TASK_UNINTERRUPTIBLE>)  until the reference "
"count is zero, at which point the call unblocks."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "The module is unloaded in the usual way."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<O_TRUNC> flag has one further effect on the rules described above.  By "
"default, if a module has an I<init> function but no I<exit> function, then "
"an attempt to remove the module fails.  However, if B<O_TRUNC> was "
"specified, this requirement is bypassed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Using the B<O_TRUNC> flag is dangerous! If the kernel was not built with "
"B<CONFIG_MODULE_FORCE_UNLOAD>, this flag is silently ignored.  (Normally, "
"B<CONFIG_MODULE_FORCE_UNLOAD> is enabled.)  Using this flag taints the "
"kernel (TAINT_FORCED_RMMOD)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned and I<errno> is "
#| "set appropriately."
msgid ""
"On success, zero is returned.  On error, -1 is returned and I<errno> is set "
"to indicate the error."
msgstr ""
"При успешном выполнении возвращается ноль. В случае ошибки возвращается -1, "
"а I<errno> устанавливается в соответствующее значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBUSY>"
msgstr "B<EBUSY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The module is not \"live\" (i.e., it is still being initialized or is "
"already marked for removal); or, the module has an I<init> function but has "
"no I<exit> function, and B<O_TRUNC> was not specified in I<flags>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "I<name> is outside the program's accessible address space."
msgid ""
"I<name> refers to a location outside the process's accessible address space."
msgstr "I<name> вне доступного программного адресного пространства."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "A module by that name already exists."
msgid "No module by that name exists."
msgstr "Модуль с таким именем уже существует."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The caller was not privileged (did not have the B<CAP_SYS_MODULE> "
#| "capability)."
msgid ""
"The caller was not privileged (did not have the B<CAP_SYS_MODULE> "
"capability), or module unloading is disabled (see I</proc/sys/kernel/"
"modules_disabled> in B<proc>(5))."
msgstr "Вызывающий не имеет прав (не имеет мандата B<CAP_SYS_MODULE>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EWOULDBLOCK>"
msgstr "B<EWOULDBLOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Other modules depend on this module; or, B<O_NONBLOCK> was specified in "
"I<flags>, but the reference count of this module is nonzero and B<O_TRUNC> "
"was not specified in I<flags>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This obsolete system call is not supported by glibc.  No declaration is "
#| "provided in glibc headers, but, through a quirk of history, glibc "
#| "versions before 2.23 did export an ABI for this system call.  Therefore, "
#| "in order to employ this system call, it was sufficient to manually "
#| "declare the interface in your code; alternatively, you could invoke the "
#| "system call using B<syscall>(2)."
msgid ""
"The B<delete_module>()  system call is not supported by glibc.  No "
"declaration is provided in glibc headers, but, through a quirk of history, "
"glibc versions before glibc 2.23 did export an ABI for this system call.  "
"Therefore, in order to employ this system call, it is (before glibc 2.23) "
"sufficient to manually declare the interface in your code; alternatively, "
"you can invoke the system call using B<syscall>(2)."
msgstr ""
"Устаревший системный вызов, не поддерживается glibc. В заголовочных файлах "
"glibc он не объявлен, но в недавнем прошлом версии glibc до 2.23 "
"экспортировали ABI для этого системного вызова. Поэтому, чтобы получить "
"данный системный вызов достаточно вручную объявить интерфейс в своём коде; "
"или же можно вызвать его через B<syscall>(2)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Linux 2.4 and earlier"
msgstr "Linux версии 2.4 и более ранние"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "In Linux 2.4 and earlier, the system call took only one argument:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B< int delete_module(const char *>I<name>B<);>"
msgstr "B< int delete_module(const char *>I<name>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<name> is NULL, all unused modules marked auto-clean are removed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Some further details of differences in the behavior of B<delete_module>()  "
"in Linux 2.4 and earlier are I<not> currently explained in this manual page."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The uninterruptible sleep that may occur if B<O_NONBLOCK> is omitted from "
"I<flags> is considered undesirable, because the sleeping process is left in "
"an unkillable state.  As at Linux 3.7, specifying B<O_NONBLOCK> is optional, "
"but in future kernels it is likely to become mandatory."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<create_module>(2), B<init_module>(2), B<query_module>(2), B<lsmod>(8), "
"B<modprobe>(8), B<rmmod>(8)"
msgstr ""
"B<create_module>(2), B<init_module>(2), B<query_module>(2), B<lsmod>(8), "
"B<modprobe>(8), B<rmmod>(8)"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid "B<delete_module>()  is Linux-specific."
msgstr "B<delete_module>() есть только в Linux."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "DELETE_MODULE"
msgstr "DELETE_MODULE"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<int delete_module(const char *>I<name>B<, int >I<flags>B<);>\n"
msgstr "B<int delete_module(const char *>I<name>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I<Note>: No declaration of this system call is provided in glibc headers; "
"see NOTES."
msgstr ""
"I<Замечание>: В заголовочных файлах glibc этой системный вызов отсутствует; "
"смотрите ЗАМЕЧАНИЯ."

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "1."
msgstr "1."

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "2."
msgstr "2."

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "3."
msgstr "3."

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, zero is returned.  On error, -1 is returned and I<errno> is set "
"appropriately."
msgstr ""
"При успешном выполнении возвращается ноль. В случае ошибки возвращается -1, "
"а I<errno> устанавливается в соответствующее значение."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "This obsolete system call is not supported by glibc.  No declaration is "
#| "provided in glibc headers, but, through a quirk of history, glibc "
#| "versions before 2.23 did export an ABI for this system call.  Therefore, "
#| "in order to employ this system call, it was sufficient to manually "
#| "declare the interface in your code; alternatively, you could invoke the "
#| "system call using B<syscall>(2)."
msgid ""
"The B<delete_module>()  system call is not supported by glibc.  No "
"declaration is provided in glibc headers, but, through a quirk of history, "
"glibc versions before 2.23 did export an ABI for this system call.  "
"Therefore, in order to employ this system call, it is (before glibc 2.23) "
"sufficient to manually declare the interface in your code; alternatively, "
"you can invoke the system call using B<syscall>(2)."
msgstr ""
"Устаревший системный вызов, не поддерживается glibc. В заголовочных файлах "
"glibc он не объявлен, но в недавнем прошлом версии glibc до 2.23 "
"экспортировали ABI для этого системного вызова. Поэтому, чтобы получить "
"данный системный вызов достаточно вручную объявить интерфейс в своём коде; "
"или же можно вызвать его через B<syscall>(2)."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
