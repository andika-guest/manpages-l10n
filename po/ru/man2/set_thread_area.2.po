# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:21+0200\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "set_thread_area"
msgstr "set_thread_area"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"get_thread_area, set_thread_area - manipulate thread-local storage "
"information"
msgstr ""
"get_thread_area, set_thread_area - управляют информацией области локального "
"хранилища нити"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/syscall.hE<gt>>     /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/syscall.hE<gt>>     /* определения констант B<SYS_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#if defined __i386__ || defined __x86_64__>\n"
"B<# include E<lt>asm/ldt.hE<gt>>        /* Definition of B<struct user_desc> */\n"
msgstr ""
"B<#if defined __i386__ || defined __x86_64__>\n"
"B<# include E<lt>asm/ldt.hE<gt>>        /* определения B<struct user_desc> */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int syscall(SYS_get_thread_area, struct user_desc *>I<u_info>B<);>\n"
"B<int syscall(SYS_set_thread_area, struct user_desc *>I<u_info>B<);>\n"
msgstr ""
"B<int syscall(SYS_get_thread_area, struct user_desc *>I<u_info>B<);>\n"
"B<int syscall(SYS_set_thread_area, struct user_desc *>I<u_info>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#elif defined __m68k__>\n"
msgstr "B<#elif defined __m68k__>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int syscall(SYS_get_thread_area);>\n"
"B<int syscall(SYS_set_thread_area, unsigned long >I<tp>B<);>\n"
msgstr ""
"B<int syscall(SYS_get_thread_area);>\n"
"B<int syscall(SYS_set_thread_area, unsigned long >I<tp>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#elif defined __mips__>\n"
msgstr "B<#elif defined __mips__>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int syscall(SYS_set_thread_area, unsigned long >I<addr>B<);>\n"
msgstr "B<int syscall(SYS_set_thread_area, unsigned long >I<addr>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#endif>\n"
msgstr "B<#endif>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Glibc does not provide a wrapper for this system call; call it using "
#| "B<syscall>(2)."
msgid ""
"I<Note>: glibc provides no wrappers for these system calls, necessitating "
"the use of B<syscall>(2)."
msgstr ""
"В glibc нет обёртки для данного системного вызова; запускайте его с помощью "
"B<syscall>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"These calls provide architecture-specific support for a thread-local storage "
"implementation.  At the moment, B<set_thread_area>()  is available on m68k, "
"MIPS, and x86 (both 32-bit and 64-bit variants); B<get_thread_area>()  is "
"available on m68k and x86."
msgstr ""
"Эти вызовы предоставляют зависимую от архитектуры реализацию поддержки "
"информации области локального хранилища нити. В настоящее время вызов "
"B<set_thread_area>() доступен для m68k, MIPS и x86 (32-битный и 64-битный "
"вариант); B<get_thread_area>() доступен для m68k и x86."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On m68k and MIPS, B<set_thread_area>()  allows storing an arbitrary pointer "
"(provided in the B<tp> argument on m68k and in the B<addr> argument on "
"MIPS)  in the kernel data structure associated with the calling thread; this "
"pointer can later be retrieved using B<get_thread_area>()  (see also NOTES "
"for information regarding obtaining the thread pointer on MIPS)."
msgstr ""
"Для m68k и MIPS, B<set_thread_area>() позволяет сохранить произвольный "
"указатель (указанный в аргументе B<tp> на m68k и в аргументе B<addr> на "
"MIPS) структуре данных ядра, связанной с вызывающей нитью; этот указатель "
"позднее может быть получен с помощью B<get_thread_area>() (информацию о "
"получении указателя нити на MIPS также смотрите ЗАМЕЧАНИЯ)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On x86, Linux dedicates three global descriptor table (GDT) entries for "
"thread-local storage.  For more information about the GDT, see the Intel "
"Software Developer's Manual or the AMD Architecture Programming Manual."
msgstr ""
"На x86 в Linux под локальное хранилище нити отдано три элемента глобальной "
"таблицы дескрипторов (GDT). Подробней о GDT читайте в Intel Software "
"Developer's Manual или AMD Architecture Programming Manual."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Both of these system calls take an argument that is a pointer to a structure "
"of the following type:"
msgstr "Этим системным вызовам передаётся указатель на структуру вида:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"struct user_desc {\n"
"    unsigned int  entry_number;\n"
"    unsigned int  base_addr;\n"
"    unsigned int  limit;\n"
"    unsigned int  seg_32bit:1;\n"
"    unsigned int  contents:2;\n"
"    unsigned int  read_exec_only:1;\n"
"    unsigned int  limit_in_pages:1;\n"
"    unsigned int  seg_not_present:1;\n"
"    unsigned int  useable:1;\n"
"#ifdef __x86_64__\n"
"    unsigned int  lm:1;\n"
"#endif\n"
"};\n"
msgstr ""
"struct user_desc {\n"
"    unsigned int  entry_number;\n"
"    unsigned int  base_addr;\n"
"    unsigned int  limit;\n"
"    unsigned int  seg_32bit:1;\n"
"    unsigned int  contents:2;\n"
"    unsigned int  read_exec_only:1;\n"
"    unsigned int  limit_in_pages:1;\n"
"    unsigned int  seg_not_present:1;\n"
"    unsigned int  useable:1;\n"
"#ifdef __x86_64__\n"
"    unsigned int  lm:1;\n"
"#endif\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<get_thread_area>()  reads the GDT entry indicated by I<u_info-"
"E<gt>entry_number> and fills in the rest of the fields in I<u_info>."
msgstr ""
"Вызов B<get_thread_area>() читает элемент GDT, указанный в I<u_info-"
"E<gt>entry_number> и заполняет оставшиеся поля в I<u_info>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<set_thread_area>()  sets a TLS entry in the GDT."
msgstr "Вызов B<set_thread_area>() изменяет элемент TLS в GDT."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The TLS array entry set by B<set_thread_area>()  corresponds to the value of "
"I<u_info-E<gt>entry_number> passed in by the user.  If this value is in "
"bounds, B<set_thread_area>()  writes the TLS descriptor pointed to by "
"I<u_info> into the thread's TLS array."
msgstr ""
"Элемент массива TLS, устанавливаемый B<set_thread_area>(), соответствует "
"значению  I<u_info-E<gt>entry_number>, которое передал пользователь. Если "
"это значение находится в допустимых пределах, то B<set_thread_area>() "
"записывает дескриптор TLS, на который указывает I<u_info>, в массив TLS нити."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When B<set_thread_area>()  is passed an I<entry_number> of -1, it searches "
"for a free TLS entry.  If B<set_thread_area>()  finds a free TLS entry, the "
"value of I<u_info-E<gt>entry_number> is set upon return to show which entry "
"was changed."
msgstr ""
"Когда B<set_thread_area>() передаётся I<entry_number> со значением -1, то "
"ищется свободный элемент TLS. Если B<set_thread_area>() находит свободный "
"элемент TLS, то значение I<u_info-E<gt>entry_number> устанавливается после "
"возврата для показа того, какой же элемент был изменён."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"A I<user_desc> is considered \"empty\" if I<read_exec_only> and "
"I<seg_not_present> are set to 1 and all of the other fields are 0.  If an "
"\"empty\" descriptor is passed to B<set_thread_area>(), the corresponding "
"TLS entry will be cleared.  See BUGS for additional details."
msgstr ""
"Структура I<user_desc> считается «пустой», если I<read_exec_only> и "
"I<seg_not_present> равны 1, а все остальные поля равны 0. Если «пустой» "
"дескриптор передаётся в B<set_thread_area>(), то соответствующий элемент TLS "
"будет очищен. Дополнительную информацию смотрите в разделе ДЕФЕКТЫ."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since Linux 3.19, B<set_thread_area>()  cannot be used to write non-present "
"segments, 16-bit segments, or code segments, although clearing a segment is "
"still acceptable."
msgstr ""
"Начиная с Linux 3.19, B<set_thread_area>() нельзя использовать для записи "
"отсутствующих сегментов, 16-битных сегментов или сегментов кода, но "
"допускается очистка таких сегментов."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On x86, these system calls return 0 on success, and -1 on failure, with "
#| "I<errno> set appropriately."
msgid ""
"On x86, these system calls return 0 on success, and -1 on failure, with "
"I<errno> set to indicate the error."
msgstr ""
"На x86 данные системные вызовы возвращают 0 при успешном выполнении и 1 при "
"ошибке, записывая в I<errno> соответствующее значение."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On MIPS and m68k, B<set_thread_area>()  always returns 0.  On m68k, "
"B<get_thread_area>()  returns the thread area pointer value (previously set "
"via B<set_thread_area>())."
msgstr ""
"На MIPS и m68k вызов B<set_thread_area>() всегда возвращает 0. На m68k вызов "
"B<get_thread_area>() возвращает значение указателя области нити "
"(установленный ране с помощью B<set_thread_area>())."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<u_info> is an invalid pointer."
msgstr "I<u_info> является некорректным указателем."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<u_info-E<gt>entry_number> is out of bounds."
msgstr "I<u_info-E<gt>entry_number> вне допустимых границ."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<get_thread_area>()  or B<set_thread_area>()  was invoked as a 64-bit "
"system call."
msgstr ""
"Вызов B<get_thread_area>() или B<set_thread_area>() был вызван как 64-битный "
"системный вызов."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr "B<ESRCH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "(B<set_thread_area>())  A free TLS entry could not be located."
msgstr "(B<set_thread_area>()) Невозможно найти свободный элемент TLS."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "set_thread_area"
msgid "B<set_thread_area>()"
msgstr "set_thread_area"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux 2.5.29."
msgstr ""

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "set_thread_area"
msgid "B<get_thread_area>()"
msgstr "set_thread_area"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "util-linux 2.37.4"
msgid "Linux 2.5.32."
msgstr "util-linux 2.37.4"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"These system calls are generally intended for use only by threading "
"libraries."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<arch_prctl>(2)  can interfere with B<set_thread_area>()  on x86.  See "
"B<arch_prctl>(2)  for more details.  This is not normally a problem, as "
"B<arch_prctl>(2)  is normally used only by 64-bit programs."
msgstr ""
"На x86 вызов B<arch_prctl>(2) может влиять на B<set_thread_area>(). "
"Подробней смотрите в B<arch_prctl>(2). Обычно это не вызывает проблем, так "
"как B<arch_prctl>(2) обычно используется только в 64-битных программах."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On MIPS, the current value of the thread area pointer can be obtained using "
"the instruction:"
msgstr ""
"На MIPS текущее значение указателя области нити можно получить с помощью "
"инструкции:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "rdhwr dest, $29\n"
msgstr "rdhwr dest, $29\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "This instruction traps and is handled by kernel."
msgstr "Эта инструкция ловится и обрабатывается ядром."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#.  commit e30ab185c490e9a9381385529e0fd32f0a399495
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On 64-bit kernels before Linux 3.19, one of the padding bits in "
"I<user_desc>, if set, would prevent the descriptor from being considered "
"empty (see B<modify_ldt>(2)).  As a result, the only reliable way to clear a "
"TLS entry is to use B<memset>(3)  to zero the entire I<user_desc> structure, "
"including padding bits, and then to set the I<read_exec_only> and "
"I<seg_not_present> bits.  On Linux 3.19, a I<user_desc> consisting entirely "
"of zeros except for I<entry_number> will also be interpreted as a request to "
"clear a TLS entry, but this behaved differently on older kernels."
msgstr ""
"В 64-битных ядрах до Linux 3.19, если был установлен один из битов "
"заполнения в I<user_desc>, то это приводило к тому, что дескриптор не "
"считался пустым (смотрите B<modify_ldt>(2)). В результате, единственным "
"надёжным способом очистить элемент TLS было задействование B<memset>(3) для "
"обнуления всей структуры I<user_desc>, включая биты заполнения, и затем "
"установка битов I<read_exec_only> и I<seg_not_present>. В Linux 3.19, "
"структура I<user_desc>, полностью состоящая из нулей кроме I<entry_number>, "
"также будет считаться запросом на очистку элемента TLS, что отличается от "
"работы старых ядер."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Prior to Linux 3.19, the DS and ES segment registers must not reference TLS "
"entries."
msgstr ""
"До Linux 3.19, сегментные регистры DS и ES не должны ссылаться на элементы "
"TLS."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<arch_prctl>(2), B<modify_ldt>(2), B<ptrace>(2)  (B<PTRACE_GET_THREAD_AREA> "
"and B<PTRACE_SET_THREAD_AREA>)"
msgstr ""
"B<arch_prctl>(2), B<modify_ldt>(2), B<ptrace>(2)  (B<PTRACE_GET_THREAD_AREA> "
"and B<PTRACE_SET_THREAD_AREA>)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<set_thread_area>()  first appeared in Linux 2.5.29.  B<get_thread_area>()  "
"first appeared in Linux 2.5.32."
msgstr ""
"Вызов B<set_thread_area>() появился в версии 2.5.29. Вызов "
"B<get_thread_area>() появился в Linux 2.5.32."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"B<set_thread_area>()  and B<get_thread_area>()  are Linux-specific and "
"should not be used in programs that are intended to be portable."
msgstr ""
"Вызовы B<set_thread_area>() и B<get_thread_area>() есть только в Linux, и "
"они не должны использоваться в переносимых программах."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SET_THREAD_AREA"
msgstr "SET_THREAD_AREA"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "get_thread_area, set_thread_area - manipulate thread-local storage "
#| "information"
msgid ""
"get_thread_area, set_thread_area - set a GDT entry for thread-local storage"
msgstr ""
"get_thread_area, set_thread_area - управляют информацией области локального "
"хранилища нити"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<#include E<lt>linux/unistd.hE<gt>>\n"
"B<#include E<lt>asm/ldt.hE<gt>>\n"
msgstr ""
"B<#include E<lt>linux/unistd.hE<gt>>\n"
"B<#include E<lt>asm/ldt.hE<gt>>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<int get_thread_area(struct user_desc *>I<u_info>B<);>\n"
"B<int set_thread_area(struct user_desc *>I<u_info>B<);>\n"
msgstr ""
"B<int get_thread_area(struct user_desc *>I<u_info>B<);>\n"
"B<int set_thread_area(struct user_desc *>I<u_info>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid "I<Note>: There are no glibc wrappers for these system calls; see NOTES."
msgstr ""
"I<Замечание>: В glibc нет обёрточных функций для этих системных вызовов; "
"смотрите ЗАМЕЧАНИЯ."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "On x86, Linux dedicates three global descriptor table (GDT) entries for "
#| "thread-local storage.  For more information about the GDT, see the Intel "
#| "Software Developer's Manual or the AMD Architecture Programming Manual."
msgid ""
"Linux dedicates three global descriptor table (GDT) entries for thread-local "
"storage.  For more information about the GDT, see the Intel Software "
"Developer's Manual or the AMD Architecture Programming Manual."
msgstr ""
"На x86 в Linux под локальное хранилище нити отдано три элемента глобальной "
"таблицы дескрипторов (GDT). Подробней о GDT читайте в Intel Software "
"Developer's Manual или AMD Architecture Programming Manual."

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"struct user_desc {\n"
"    unsigned int  entry_number;\n"
"    unsigned long base_addr;\n"
"    unsigned int  limit;\n"
"    unsigned int  seg_32bit:1;\n"
"    unsigned int  contents:2;\n"
"    unsigned int  read_exec_only:1;\n"
"    unsigned int  limit_in_pages:1;\n"
"    unsigned int  seg_not_present:1;\n"
"    unsigned int  useable:1;\n"
"};\n"
msgstr ""
"struct user_desc {\n"
"    unsigned int  entry_number;\n"
"    unsigned long base_addr;\n"
"    unsigned int  limit;\n"
"    unsigned int  seg_32bit:1;\n"
"    unsigned int  contents:2;\n"
"    unsigned int  read_exec_only:1;\n"
"    unsigned int  limit_in_pages:1;\n"
"    unsigned int  seg_not_present:1;\n"
"    unsigned int  useable:1;\n"
"};\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"A I<user_desc> is considered \"empty\" if I<read_exec_only> and "
"I<seg_not_present> are set to 1 and all of the other fields are 0.  If an "
"\"empty\" descriptor is passed to B<set_thread_area,> the corresponding TLS "
"entry will be cleared.  See BUGS for additional details."
msgstr ""
"Структура I<user_desc> считается «пустой», если I<read_exec_only> и "
"I<seg_not_present> равны 1, а все остальные поля равны 0. Если «пустой» "
"дескриптор передаётся в B<set_thread_area>, то соответствующий элемент TLS "
"будет очищен. Дополнительную информацию смотрите в разделе ДЕФЕКТЫ."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "On x86, these system calls return 0 on success, and -1 on failure, with "
#| "I<errno> set appropriately."
msgid ""
"These system calls return 0 on success, and -1 on failure, with I<errno> set "
"appropriately."
msgstr ""
"На x86 данные системные вызовы возвращают 0 при успешном выполнении и 1 при "
"ошибке, записывая в I<errno> соответствующее значение."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "B<setfsuid>()  is Linux-specific and should not be used in programs "
#| "intended to be portable."
msgid ""
"B<set_thread_area>()  is Linux-specific and should not be used in programs "
"that are intended to be portable."
msgstr ""
"Вызов B<setfsuid>() есть только в Linux и не должен использоваться в "
"переносимых программах."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Glibc does not provide wrappers for these system calls, since they are "
"generally intended for use only by threading libraries.  In the unlikely "
"event that you want to call them directly, use B<syscall>(2)."
msgstr ""
"В glibc нет обёрточных функций для этих системных вызовов, так как они "
"предназначены только для использования в библиотеках нитей. Если вам всё-"
"таки нужно их вызвать, используйте B<syscall>(2)."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "B<arch_prctl>(2)  can interfere with B<set_thread_area>()  on x86.  See "
#| "B<arch_prctl>(2)  for more details.  This is not normally a problem, as "
#| "B<arch_prctl>(2)  is normally used only by 64-bit programs."
msgid ""
"B<arch_prctl>(2)  can interfere with B<set_thread_area>().  See "
"B<arch_prctl>(2)  for more details.  This is not normally a problem, as "
"B<arch_prctl>(2)  is normally used only by 64-bit programs."
msgstr ""
"На x86 вызов B<arch_prctl>(2) может влиять на B<set_thread_area>(). "
"Подробней смотрите в B<arch_prctl>(2). Обычно это не вызывает проблем, так "
"как B<arch_prctl>(2) обычно используется только в 64-битных программах."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<arch_prctl>(2), B<modify_ldt>(2)"
msgstr "B<arch_prctl>(2), B<modify_ldt>(2)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
