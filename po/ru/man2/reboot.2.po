# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2016.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Lockal <lockalsash@gmail.com>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Баринов Владимир, 2016.
# Иван Павлов <pavia00@gmail.com>, 2017,2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:16+0200\n"
"PO-Revision-Date: 2019-10-06 09:20+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "reboot"
msgstr "reboot"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "reboot - reboot or enable/disable Ctrl-Alt-Del"
msgstr ""
"reboot - перезагружает систему и разрешает/запрещает использование "
"комбинации Ctrl-Alt-Del"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "/* Since kernel version 2.1.30 there are symbolic names B<LINUX_REBOOT_*>\n"
#| "   for the constants and a fourth argument to the call: */\n"
msgid ""
"/* Since Linux 2.1.30 there are symbolic names B<LINUX_REBOOT_*>\n"
"   for the constants and a fourth argument to the call: */\n"
msgstr ""
"/* Начиная с версии ядра 2.1.30 появились символьные имена B<LINUX_REBOOT_*>\n"
"   для констант и четвёртый аргумент вызова: */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/reboot.hE<gt>  >/* Definition of B<LINUX_REBOOT_*> constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>   >/* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>linux/reboot.hE<gt>  >/* определения констант B<LINUX_REBOOT_*> */\n"
"B<#include E<lt>sys/syscall.hE<gt>   >/* определения констант B<SYS_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int syscall(SYS_reboot, int >I<magic>B<, int >I<magic2>B<, int >I<cmd>B<, void *>I<arg>B<);>\n"
msgstr "B<int syscall(SYS_reboot, int >I<magic>B<, int >I<magic2>B<, int >I<cmd>B<, void *>I<arg>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"/* Under glibc and most alternative libc's (including uclibc, dietlibc,\n"
"   musl and a few others), some of the constants involved have gotten\n"
"   symbolic names B<RB_*>, and the library call is a 1-argument\n"
"   wrapper around the system call: */\n"
msgstr ""
"/* В glibc и в большинстве альтернативных libc (включая uclibc,\n"
"   deitlibc, musl и других) некоторым константам присвоены\n"
"   символьные имена B<RB_*>, а библиотечная функция является\n"
"   обёрткой с одним аргументом вокруг системного вызова: */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/reboot.hE<gt>    >/* Definition of B<RB_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/reboot.hE<gt>    >/* определения констант B<RB_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int reboot(int >I<cmd>B<);>\n"
msgstr "B<int reboot(int >I<cmd>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<reboot>()  call reboots the system, or enables/disables the reboot "
"keystroke (abbreviated CAD, since the default is Ctrl-Alt-Delete; it can be "
"changed using B<loadkeys>(1))."
msgstr ""
"Вызов B<reboot>() перезагружает систему или разрешает/запрещает "
"использование для перезагрузки специального сочетания клавиш (сокращённо "
"CAD, от комбинации по умолчанию \\(em Ctrl-Alt-Delete; может быть изменена с "
"помощью B<loadkeys>(1))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This system call fails (with the error B<EINVAL>)  unless I<magic> equals "
#| "B<LINUX_REBOOT_MAGIC1> (that is, 0xfee1dead) and I<magic2> equals "
#| "B<LINUX_REBOOT_MAGIC2> (that is, 672274793).  However, since 2.1.17 also "
#| "B<LINUX_REBOOT_MAGIC2A> (that is, 85072278)  and since 2.1.97 also "
#| "B<LINUX_REBOOT_MAGIC2B> (that is, 369367448)  and since 2.5.71 also "
#| "B<LINUX_REBOOT_MAGIC2C> (that is, 537993216)  are permitted as values for "
#| "I<magic2>.  (The hexadecimal values of these constants are meaningful.)"
msgid ""
"This system call fails (with the error B<EINVAL>)  unless I<magic> equals "
"B<LINUX_REBOOT_MAGIC1> (that is, 0xfee1dead) and I<magic2> equals "
"B<LINUX_REBOOT_MAGIC2> (that is, 0x28121969).  However, since Linux 2.1.17 "
"also B<LINUX_REBOOT_MAGIC2A> (that is, 0x05121996)  and since Linux 2.1.97 "
"also B<LINUX_REBOOT_MAGIC2B> (that is, 0x16041998)  and since Linux 2.5.71 "
"also B<LINUX_REBOOT_MAGIC2C> (that is, 0x20112000)  are permitted as values "
"for I<magic2>.  (The hexadecimal values of these constants are meaningful.)"
msgstr ""
"Данный системный вызов завершается ошибкой (B<EINVAL>), если I<magic> не "
"равен B<LINUX_REBOOT_MAGIC1> (0xfee1dead) и I<magic2> не равен "
"B<LINUX_REBOOT_MAGIC2> (672274793). Однако, начиная с 2.1.17 в I<magic2> "
"также можно использовать B<LINUX_REBOOT_MAGIC2A> (85072278)  и начиная с "
"2.1.97 — B<LINUX_REBOOT_MAGIC2B> (369367448) и начиная с 2.5.71 — "
"B<LINUX_REBOOT_MAGIC2C> (537993216) (шестнадцатеричные значения этих "
"констант говорят сами за себя)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The I<cmd> argument can have the following values:"
msgstr "Аргумент I<cmd> может принимать следующие значения:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_CAD_OFF>"
msgstr "B<LINUX_REBOOT_CMD_CAD_OFF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<RB_DISABLE_CAD>, 0).  CAD is disabled.  This means that the CAD keystroke "
"will cause a B<SIGINT> signal to be sent to init (process 1), whereupon this "
"process may decide upon a proper action (maybe: kill all processes, sync, "
"reboot)."
msgstr ""
"(B<RB_DISABLE_CAD>, 0). Запретить использование сочетания клавиш для "
"перезагрузки системы (CAD). Это означает, что нажатие комбинации клавиш CAD "
"приведёт к тому, что процессу init (с идентификатором 1) будет послан сигнал "
"B<SIGINT>, после чего этот процесс может сам решить какие действия выполнять "
"(возможно, послать сигналы процессам, выполнить команду sync, reboot)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_CAD_ON>"
msgstr "B<LINUX_REBOOT_CMD_CAD_ON>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<RB_ENABLE_CAD>, 0x89abcdef).  CAD is enabled.  This means that the CAD "
"keystroke will immediately cause the action associated with "
"B<LINUX_REBOOT_CMD_RESTART>."
msgstr ""
"(B<RB_ENABLE_CAD>, 0x89abcdef). Разрешить использование сочетания клавиш для "
"перезагрузки (CAD). Это означает, что нажатие комбинации клавиш CAD приведёт "
"к немедленному выполнению действия, связанного с B<LINUX_REBOOT_CMD_RESTART>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_HALT>"
msgstr "B<LINUX_REBOOT_CMD_HALT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<RB_HALT_SYSTEM>, 0xcdef0123; since Linux 1.1.76).  The message \"System "
"halted.\" is printed, and the system is halted.  Control is given to the ROM "
"monitor, if there is one.  If not preceded by a B<sync>(2), data will be "
"lost."
msgstr ""
"(B<RB_HALT_SYSTEM>, 0xcdef0123; начиная с Linux 1.1.76) Выводится сообщение "
"«System halted.» и система останавливается. Управление передается монитору в "
"ПЗУ, если таковой имеется. Если вызову этой функции не предшествует "
"B<sync>(2), то данные будут потеряны."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_KEXEC>"
msgstr "B<LINUX_REBOOT_CMD_KEXEC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<RB_KEXEC>, 0x45584543, since Linux 2.6.13).  Execute a kernel that has "
"been loaded earlier with B<kexec_load>(2).  This option is available only if "
"the kernel was configured with B<CONFIG_KEXEC>."
msgstr ""
"(B<RB_KEXEC>, 0x45584543, начиная с Linux 2.6.13) Выполняет ядро, которое "
"было загружено ранее с помощью B<kexec_load>(2). Этот параметр доступен "
"только, если ядро собрано с параметром B<CONFIG_KEXEC>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_POWER_OFF>"
msgstr "B<LINUX_REBOOT_CMD_POWER_OFF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<RB_POWER_OFF>, 0x4321fedc; since Linux 2.1.30).  The message \"Power down."
"\" is printed, the system is stopped, and all power is removed from the "
"system, if possible.  If not preceded by a B<sync>(2), data will be lost."
msgstr ""
"(B<RB_POWER_OFF>, 0x4321fedc; начиная с Linux 2.1.30) Выводится сообщение "
"«Power down.», система останавливается, и у системы отключаются все "
"источники питания, если это возможно. Если вызову этой функции не "
"предшествует B<sync>(2), то данные будут потеряны."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_RESTART>"
msgstr "B<LINUX_REBOOT_CMD_RESTART>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<RB_AUTOBOOT>, 0x1234567).  The message \"Restarting system.\" is printed, "
"and a default restart is performed immediately.  If not preceded by a "
"B<sync>(2), data will be lost."
msgstr ""
"(B<RB_AUTOBOOT>, 0x1234567) Выводится сообщение «Restarting system.», и по "
"умолчанию сразу выполняется перезагрузка системы. Если вызову этой функции "
"не предшествует команда B<sync>(2), то данные будут потеряны."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_RESTART2>"
msgstr "B<LINUX_REBOOT_CMD_RESTART2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "(0xa1b2c3d4; since Linux 2.1.30).  The message \"Restarting system with "
#| "command \\(aq%s\\(aq\" is printed, and a restart (using the command "
#| "string given in I<arg>)  is performed immediately.  If not preceded by a "
#| "B<sync>(2), data will be lost."
msgid ""
"(0xa1b2c3d4; since Linux 2.1.30).  The message \"Restarting system with "
"command \\[aq]%s\\[aq]\" is printed, and a restart (using the command string "
"given in I<arg>)  is performed immediately.  If not preceded by a "
"B<sync>(2), data will be lost."
msgstr ""
"(0xa1b2c3d4; начиная с Linux 2.1.30) Выводится сообщение «Restarting system "
"with command \\(aq%s\\(aq» и немедленно выполняется перезагрузка системы (с "
"использованием командной строки, заданной в I<arg>). Если вызову этой "
"функции не предшествует B<sync>(2), то данные будут потеряны."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_SW_SUSPEND>"
msgstr "B<LINUX_REBOOT_CMD_SW_SUSPEND>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<RB_SW_SUSPEND>, 0xd000fce1; since Linux 2.5.18).  The system is suspended "
"(hibernated) to disk.  This option is available only if the kernel was "
"configured with B<CONFIG_HIBERNATION>."
msgstr ""
"(B<RB_SW_SUSPEND>, 0xd000fce1; начиная с Linux 2.5.18) Система переводится в "
"режим ожидания (suspended, hibernated) на диск. Этот параметр доступен "
"только, если ядро собрано с параметром B<CONFIG_HIBERNATION>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Only the superuser may call B<reboot>()."
msgstr "Только суперпользователь может вызывать B<reboot>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The precise effect of the above actions depends on the architecture.  For "
"the i386 architecture, the additional argument does not do anything at "
"present (2.1.122), but the type of reboot can be determined by kernel "
"command-line arguments (\"reboot=...\") to be either warm or cold, and "
"either hard or through the BIOS."
msgstr ""
"Конкретное действие описанных выше команд зависит от архитектуры системы. "
"Что касается i386, то дополнительный аргумент в данное время ничего не даёт "
"(2.1.122), а тип перезагрузки можно задать в командной строке ядра "
"(\"reboot=...\"), определив, будет ли перезагрузка \"тёплой\" или "
"\"холодной\", а также аппаратной или посредством BIOS."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Behavior inside PID namespaces"
msgstr "Поведение внутри пространств имён PID"

#.  commit cf3f89214ef6a33fad60856bc5ffd7bb2fc4709b
#.  see also commit 923c7538236564c46ee80c253a416705321f13e3
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since Linux 3.4, if B<reboot>()  is called from a PID namespace other than "
"the initial PID namespace with one of the I<cmd> values listed below, it "
"performs a \"reboot\" of that namespace: the \"init\" process of the PID "
"namespace is immediately terminated, with the effects described in "
"B<pid_namespaces>(7)."
msgstr ""
"Начиная с Linux 3.4, если B<reboot>() вызывается из пространства имён PID, "
"отличающегося от начального пространства имён PID, и и значение I<cmd> равно "
"одному из перечисленных ниже, то выполняется «перезагрузка» в этом "
"пространстве имён: процесс «init» пространства имён PID завершается "
"немедленно, что приводит к результатам, описанным в B<pid_namespaces>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The values that can be supplied in I<cmd> when calling B<reboot>()  in this "
"case are as follows:"
msgstr ""
"Возможные значения в этом случае, передаваемые в I<cmd> при вызове "
"B<reboot>(), следующие:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_RESTART>, B<LINUX_REBOOT_CMD_RESTART2>"
msgstr "B<LINUX_REBOOT_CMD_RESTART>, B<LINUX_REBOOT_CMD_RESTART2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The \"init\" process is terminated, and B<wait>(2)  in the parent process "
"reports that the child was killed with a B<SIGHUP> signal."
msgstr ""
"Процесс «init» завершается и B<wait>(2) в родительском процессе возвращает, "
"что поток завершился по сигналу B<SIGHUP>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_POWER_OFF>, B<LINUX_REBOOT_CMD_HALT>"
msgstr "B<LINUX_REBOOT_CMD_POWER_OFF>, B<LINUX_REBOOT_CMD_HALT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The \"init\" process is terminated, and B<wait>(2)  in the parent process "
"reports that the child was killed with a B<SIGINT> signal."
msgstr ""
"Процесс «init» завершается и B<wait>(2) в родительском процессе возвращает, "
"что поток завершился по сигналу B<SIGINT>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For the other I<cmd> values, B<reboot>()  returns -1 and I<errno> is set to "
"B<EINVAL>."
msgstr ""
"При других значениях I<cmd> вызов B<reboot>() возвращает -1 и I<errno> "
"присваивается значение B<EINVAL>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "For the values of I<cmd> that stop or restart the system, a successful "
#| "call to B<reboot>()  does not return.  For the other I<cmd> values, zero "
#| "is returned on success.  In all cases, -1 is returned on failure, and "
#| "I<errno> is set appropriately."
msgid ""
"For the values of I<cmd> that stop or restart the system, a successful call "
"to B<reboot>()  does not return.  For the other I<cmd> values, zero is "
"returned on success.  In all cases, -1 is returned on failure, and I<errno> "
"is set to indicate the error."
msgstr ""
"При значениях I<cmd>, по которым система останавливается или "
"перезагружается, в случае успешной работы B<reboot>() ничего не "
"возвращается. При других значений I<cmd> в случае успешной работы "
"возвращается ноль. При ошибке всегда возвращается -1, а I<errno> "
"устанавливается в соответствующее значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Problem with getting user-space data under B<LINUX_REBOOT_CMD_RESTART2>."
msgstr ""
"Проблема получения данных пользовательского пространства при "
"B<LINUX_REBOOT_CMD_RESTART2>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Bad magic numbers or I<cmd>."
msgstr "Неправильные идентификационные числа или I<cmd>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The calling process has insufficient privilege to call B<reboot>(); the "
"caller must have the B<CAP_SYS_BOOT> inside its user namespace."
msgstr ""
"Вызывающий процесс не имеет достаточно прав для вызова B<reboot>(); "
"вызывающий должен иметь мандат B<CAP_SETGID> в своём пользовательском "
"пространстве имён."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<systemctl>(1), B<systemd>(1), B<kexec_load>(2), B<sync>(2), "
"B<bootparam>(7), B<capabilities>(7), B<ctrlaltdel>(8), B<halt>(8), "
"B<shutdown>(8)"
msgstr ""
"B<systemctl>(1), B<systemd>(1), B<kexec_load>(2), B<sync>(2), "
"B<bootparam>(7), B<capabilities>(7), B<ctrlaltdel>(8), B<halt>(8), "
"B<shutdown>(8)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-08"
msgstr "8 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<reboot>()  is Linux-specific, and should not be used in programs intended "
"to be portable."
msgstr ""
"Вызов B<reboot>() есть только в Linux, и он не должен использоваться в "
"переносимых программах."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "REBOOT"
msgstr "REBOOT"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"/* Since kernel version 2.1.30 there are symbolic names LINUX_REBOOT_*\n"
"   for the constants and a fourth argument to the call: */\n"
msgstr ""
"/* Начиная с версии ядра 2.1.30 появились символьные имена LINUX_REBOOT_*\n"
"   для констант и четвёртый аргумент вызова: */\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>linux/reboot.hE<gt>>"
msgstr "B<#include E<lt>linux/reboot.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<int reboot(int >I<magic>B<, int >I<magic2>B<, int >I<cmd>B<, void "
"*>I<arg>B<);>"
msgstr ""
"B<int reboot(int >I<magic>B<, int >I<magic2>B<, int >I<cmd>B<, void "
"*>I<arg>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"/* Under glibc and most alternative libc's (including uclibc, dietlibc,\n"
"   musl and a few others), some of the constants involved have gotten\n"
"   symbolic names RB_*, and the library call is a 1-argument\n"
"   wrapper around the system call: */\n"
msgstr ""
"/* В glibc и в большинстве альтернативных libc (включая uclibc,\n"
"   deitlibc, musl и других) некоторым константам присвоены\n"
"   символьные имена RB_*, а библиотечная функция является\n"
"   обёрткой с одним аргументом вокруг системного вызова: */\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<#include E<lt>sys/reboot.hE<gt>>"
msgstr "B<#include E<lt>sys/reboot.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<int reboot(int >I<cmd>B<);>"
msgstr "B<int reboot(int >I<cmd>B<);>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This system call fail (with the error B<EINVAL>)  unless I<magic> equals "
"B<LINUX_REBOOT_MAGIC1> (that is, 0xfee1dead) and I<magic2> equals "
"B<LINUX_REBOOT_MAGIC2> (that is, 672274793).  However, since 2.1.17 also "
"B<LINUX_REBOOT_MAGIC2A> (that is, 85072278)  and since 2.1.97 also "
"B<LINUX_REBOOT_MAGIC2B> (that is, 369367448)  and since 2.5.71 also "
"B<LINUX_REBOOT_MAGIC2C> (that is, 537993216)  are permitted as values for "
"I<magic2>.  (The hexadecimal values of these constants are meaningful.)"
msgstr ""
"Данный системный вызов завершается ошибкой (B<EINVAL>), если I<magic> не "
"равен B<LINUX_REBOOT_MAGIC1> (0xfee1dead) и I<magic2> не равен "
"B<LINUX_REBOOT_MAGIC2> (672274793). Однако, начиная с 2.1.17 в I<magic2> "
"также можно использовать B<LINUX_REBOOT_MAGIC2A> (85072278)  и начиная с "
"2.1.97 — B<LINUX_REBOOT_MAGIC2B> (369367448) и начиная с 2.5.71 — "
"B<LINUX_REBOOT_MAGIC2C> (537993216) (шестнадцатеричные значения этих "
"констант говорят сами за себя)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"(0xa1b2c3d4; since Linux 2.1.30).  The message \"Restarting system with "
"command \\(aq%s\\(aq\" is printed, and a restart (using the command string "
"given in I<arg>)  is performed immediately.  If not preceded by a "
"B<sync>(2), data will be lost."
msgstr ""
"(0xa1b2c3d4; начиная с Linux 2.1.30) Выводится сообщение «Restarting system "
"with command \\(aq%s\\(aq» и немедленно выполняется перезагрузка системы (с "
"использованием командной строки, заданной в I<arg>). Если вызову этой "
"функции не предшествует B<sync>(2), то данные будут потеряны."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"For the values of I<cmd> that stop or restart the system, a successful call "
"to B<reboot>()  does not return.  For the other I<cmd> values, zero is "
"returned on success.  In all cases, -1 is returned on failure, and I<errno> "
"is set appropriately."
msgstr ""
"При значениях I<cmd>, по которым система останавливается или "
"перезагружается, в случае успешной работы B<reboot>() ничего не "
"возвращается. При других значений I<cmd> в случае успешной работы "
"возвращается ноль. При ошибке всегда возвращается -1, а I<errno> "
"устанавливается в соответствующее значение."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
