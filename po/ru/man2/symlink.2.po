# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:24+0200\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "symlink"
msgstr "symlink"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "symlink, symlinkat - make a new name for a file"
msgstr "symlink, symlinkat - создаёт новое имя для файла"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int symlink(const char *>I<target>B<, const char *>I<linkpath>B<);>\n"
msgstr "B<int symlink(const char *>I<target>B<, const char *>I<linkpath>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition of B<AT_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* определения констант B<AT_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int symlinkat(const char *>I<target>B<, int >I<newdirfd>B<, const char *>I<linkpath>B<);>\n"
msgstr "B<int symlinkat(const char *>I<target>B<, int >I<newdirfd>B<, const char *>I<linkpath>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<symlink>():"
msgstr "B<symlink>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    _XOPEN_SOURCE E<gt>= 500 || _POSIX_C_SOURCE E<gt>= 200112L\n"
#| "        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
msgid ""
"    _XOPEN_SOURCE E<gt>= 500 || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 500 || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* в версиях Glibc E<lt>= 2.19: */ _BSD_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<symlinkat>():"
msgstr "B<symlinkat>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _ATFILE_SOURCE\n"
msgstr ""
"    Начиная с glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    До glibc 2.10:\n"
"        _ATFILE_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<symlink>()  creates a symbolic link named I<linkpath> which contains the "
"string I<target>."
msgstr ""
"Вызов B<symlink>() создаёт символьную ссылку с именем I<linkpath>, которая "
"содержит строку I<target>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Symbolic links are interpreted at run time as if the contents of the link "
"had been substituted into the path being followed to find a file or "
"directory."
msgstr ""
"Символьные ссылки интерпретируются «на лету», как будто бы содержимое ссылки "
"было подставлено вместо пути, по которому идёт поиск файла или каталога."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Symbolic links may contain I<..> path components, which (if used at the "
"start of the link) refer to the parent directories of that in which the link "
"resides."
msgstr ""
"Символьные ссылки могут содержать компоненты пути I<..>, которые (если "
"используются в начале ссылки) ссылаются на родительский каталог того "
"каталога, в котором находится ссылка."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A symbolic link (also known as a soft link) may point to an existing file or "
"to a nonexistent one; the latter case is known as a dangling link."
msgstr ""
"Символьная ссылка (также известная как «мягкая ссылка») может указывать как "
"на существующий, так и на несуществующий файлы; в последнем случае такая "
"ссылка называется повисшей (dangling)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The permissions of a symbolic link are irrelevant; the ownership is "
#| "ignored when following the link, but is checked when removal or renaming "
#| "of the link is requested and the link is in a directory with the sticky "
#| "bit (B<S_ISVTX>)  set."
msgid ""
"The permissions of a symbolic link are irrelevant; the ownership is ignored "
"when following the link (except when the I<protected_symlinks> feature is "
"enabled, as explained in B<proc>(5)), but is checked when removal or "
"renaming of the link is requested and the link is in a directory with the "
"sticky bit (B<S_ISVTX>)  set."
msgstr ""
"Права доступа символьной ссылки не имеют значения; принадлежность "
"определённому владельцу игнорируется при переходе по ссылке, но проверяется "
"при удалении или переименовании ссылки, а также ссылки в каталог с "
"установленным закрепляющим (sticky) битом (B<S_ISVTX>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<linkpath> exists, it will I<not> be overwritten."
msgstr "Если I<linkpath> существует, то он I<не> будет перезаписан."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "symlinkat()"
msgstr "symlinkat()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<symlinkat>()  system call operates in exactly the same way as "
"B<symlink>(), except for the differences described here."
msgstr ""
"Системный вызов B<symlinkat>() работает также как системный вызов "
"B<symlink>(), за исключением случаев, описанных здесь."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the pathname given in I<linkpath> is relative, then it is interpreted "
"relative to the directory referred to by the file descriptor I<newdirfd> "
"(rather than relative to the current working directory of the calling "
"process, as is done by B<symlink>()  for a relative pathname)."
msgstr ""
"Если в I<linkpath> задан относительный путь, то он считается относительно "
"каталога, на который ссылается файловый дескриптор I<newdirfd> (а не "
"относительно текущего рабочего каталога вызывающего процесса, как это "
"делается в B<symlink>())."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<linkpath> is relative and I<newdirfd> is the special value B<AT_FDCWD>, "
"then I<linkpath> is interpreted relative to the current working directory of "
"the calling process (like B<symlink>())."
msgstr ""
"Если в I<linkpath> задан относительный путь и значение I<newdirfd> равно "
"B<AT_FDCWD>, то I<linkpath> рассматривается относительно текущего рабочего "
"каталога вызывающего процесса (как B<symlink>())."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<linkpath> is absolute, then I<newdirfd> is ignored."
msgstr "Если в I<linkpath> задан абсолютный путь, то I<newdirfd> игнорируется."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "See B<openat>(2)  for an explanation of the need for B<scandirat>()."
msgid "See B<openat>(2)  for an explanation of the need for B<symlinkat>()."
msgstr "Смотрите в B<openat>(2) объяснение необходимости B<scandirat>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"При успешном выполнении возвращается 0. В случае ошибки возвращается -1, а "
"I<errno> устанавливается в соответствующее значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Write access to the directory containing I<linkpath> is denied, or one of "
"the directories in the path prefix of I<linkpath> did not allow search "
"permission.  (See also B<path_resolution>(7).)"
msgstr ""
"Нет прав на запись в каталог, содержащийся в I<linkpath>, или в одном из "
"каталогов в I<linkpath> не разрешён поиск (смотрите также "
"B<path_resolution>(7))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"(B<symlinkat>())  I<linkpath> is relative but I<newdirfd> is neither "
"B<AT_FDCWD> nor a valid file descriptor."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EDQUOT>"
msgstr "B<EDQUOT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The user's quota of resources on the filesystem has been exhausted.  The "
"resources could be inodes or disk blocks, depending on the filesystem "
"implementation."
msgstr ""
"Исчерпана пользовательская квота на ресурсы файловой системы. Ресурсами "
"могут быть иноды или дисковые блоки, в зависимости от реализации файловой "
"системы."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<linkpath> already exists."
msgstr "I<linkpath> уже существует."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<target> or I<linkpath> points outside your accessible address space."
msgstr ""
"Значение I<target> или I<linkpath> указывают за пределы доступного адресного "
"пространства."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An I/O error occurred."
msgstr "Произошла ошибка ввода-вывода."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Too many symbolic links were encountered in resolving I<linkpath>."
msgstr ""
"Во время определения I<linkpath> встретилось слишком много символьных ссылок."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<target> or I<linkpath> was too long."
msgstr "Слишком длинное значение аргумента I<target> или I<linkpath>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A directory component in I<linkpath> does not exist or is a dangling "
"symbolic link, or I<target> or I<linkpath> is an empty string."
msgstr ""
"Компонент пути I<linkpath> не существует или является повисшей символьной "
"ссылкой или значение I<target> или I<linkpath> равно пустой строке."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<linkpath> is a relative pathname and I<newdirfd> refers to a directory "
#| "that has been deleted."
msgid ""
"(B<symlinkat>())  I<linkpath> is a relative pathname and I<newdirfd> refers "
"to a directory that has been deleted."
msgstr ""
"Значение I<linkpath> является относительным путём и I<newdirfd> ссылается на "
"каталог, который был удалён."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "Недостаточное количество памяти ядра."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The device containing the file has no room for the new directory entry."
msgstr ""
"На устройстве, содержащем файл, нет места для создания нового элемента "
"каталога."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A component used as a directory in I<linkpath> is not, in fact, a directory."
msgstr ""
"Компонент пути, использованный как каталог в I<linkpath>, в действительности "
"таковым не является."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<linkpath> is relative and I<newdirfd> is a file descriptor referring to "
#| "a file other than a directory."
msgid ""
"(B<symlinkat>())  I<linkpath> is relative and I<newdirfd> is a file "
"descriptor referring to a file other than a directory."
msgstr ""
"Значение I<linkpath> содержит относительный путь и I<newdirfd> содержит "
"файловый дескриптор, указывающий на файл, а не на каталог."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The filesystem containing I<linkpath> does not support the creation of "
"symbolic links."
msgstr ""
"Файловая система, содержащая I<linkpath>, не поддерживает создание "
"символьных ссылок."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EROFS>"
msgstr "B<EROFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<linkpath> is on a read-only filesystem."
msgstr ""
"I<linkpath> расположен в файловой системе, доступной только для чтения."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<symlink>():"
msgid "B<symlink>()"
msgstr "B<symlink>():"

#.  SVr4 documents additional error codes EDQUOT and ENOSYS.
#.  See
#.  .BR open (2)
#.  re multiple files with the same name, and NFS.
#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "SVr4, 4.3BSD, POSIX.1-2001."
msgstr "SVr4, 4.3BSD, POSIX.1-2001."

#. type: TP
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<symlinkat>():"
msgid "B<symlinkat>()"
msgstr "B<symlinkat>():"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008.  Linux 2.6.16, glibc 2.4."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "glibc notes"
msgstr "Замечания по glibc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On older kernels where B<symlinkat>()  is unavailable, the glibc wrapper "
"function falls back to the use of B<symlink>().  When I<linkpath> is a "
"relative pathname, glibc constructs a pathname based on the symbolic link in "
"I</proc/self/fd> that corresponds to the I<newdirfd> argument."
msgstr ""
"В старых ядрах, где B<symlinkat>() отсутствует, обёрточная функция glibc "
"использует B<symlink>(). Если I<linkpath> является относительным путём, то "
"glibc собирает путь относительно символической ссылки в I</proc/self/fd>, "
"которая соответствует аргументу I<newdirfd>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "No checking of I<target> is done."
msgstr "Не выполняется проверка I<target>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Deleting the name referred to by a symbolic link will actually delete the "
"file (unless it also has other hard links).  If this behavior is not "
"desired, use B<link>(2)."
msgstr ""
"При удаление имени, на который ссылается символьная ссылка, произойдёт "
"удаление файла (если только у него нет других жёстких ссылок). Если такое "
"поведение нежелательно, используйте B<link>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<ln>(1), B<namei>(1), B<lchown>(2), B<link>(2), B<lstat>(2), B<open>(2), "
"B<readlink>(2), B<rename>(2), B<unlink>(2), B<path_resolution>(7), "
"B<symlink>(7)"
msgstr ""
"B<ln>(1), B<namei>(1), B<lchown>(2), B<link>(2), B<lstat>(2), B<open>(2), "
"B<readlink>(2), B<rename>(2), B<unlink>(2), B<path_resolution>(7), "
"B<symlink>(7)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
#, fuzzy
#| msgid ""
#| "B<symlinkat>()  was added to Linux in kernel 2.6.16; library support was "
#| "added to glibc in version 2.4."
msgid ""
"B<symlinkat>()  was added in Linux 2.6.16; library support was added in "
"glibc 2.4."
msgstr ""
"Системный вызов B<symlinkat>() был добавлен в ядро Linux версии 2.6.16; "
"поддержка в glibc доступна с версии 2.4."

#.  SVr4 documents additional error codes EDQUOT and ENOSYS.
#.  See
#.  .BR open (2)
#.  re multiple files with the same name, and NFS.
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "B<symlink>(): SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."
msgstr "B<symlink>(): SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "B<symlinkat>(): POSIX.1-2008."
msgstr "B<symlinkat>(): POSIX.1-2008."

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SYMLINK"
msgstr "SYMLINK"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition of AT_* constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* определения констант of AT_* */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500 || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500 || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* в версии glibc E<lt>= 2.19: */ _BSD_SOURCE\n"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Начиная с glibc 2.10:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "До glibc 2.10:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_ATFILE_SOURCE"
msgstr "_ATFILE_SOURCE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The permissions of a symbolic link are irrelevant; the ownership is ignored "
"when following the link, but is checked when removal or renaming of the link "
"is requested and the link is in a directory with the sticky bit "
"(B<S_ISVTX>)  set."
msgstr ""
"Права доступа символьной ссылки не имеют значения; принадлежность "
"определённому владельцу игнорируется при переходе по ссылке, но проверяется "
"при удалении или переименовании ссылки, а также ссылки в каталог с "
"установленным закрепляющим (sticky) битом (B<S_ISVTX>)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"При успешном выполнении возвращается 0. В случае ошибки возвращается -1, а "
"I<errno> устанавливается в соответствующее значение."

#. type: Plain text
#: opensuse-leap-15-6
msgid "The following additional errors can occur for B<symlinkat>():"
msgstr "В B<symlinkat>() дополнительно могут возникнуть следующие ошибки:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "I<newdirfd> is not a valid file descriptor."
msgstr "Значение I<newdirfd> не является правильным файловым дескриптором."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I<linkpath> is a relative pathname and I<newdirfd> refers to a directory "
"that has been deleted."
msgstr ""
"Значение I<linkpath> является относительным путём и I<newdirfd> ссылается на "
"каталог, который был удалён."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I<linkpath> is relative and I<newdirfd> is a file descriptor referring to a "
"file other than a directory."
msgstr ""
"Значение I<linkpath> содержит относительный путь и I<newdirfd> содержит "
"файловый дескриптор, указывающий на файл, а не на каталог."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<symlinkat>()  was added to Linux in kernel 2.6.16; library support was "
"added to glibc in version 2.4."
msgstr ""
"Системный вызов B<symlinkat>() был добавлен в ядро Linux версии 2.6.16; "
"поддержка в glibc доступна с версии 2.4."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SS
#: opensuse-leap-15-6
#, no-wrap
msgid "Glibc notes"
msgstr "Замечания по glibc"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
