# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 16:59+0200\n"
"PO-Revision-Date: 2019-10-05 08:06+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "get_robust_list"
msgstr "get_robust_list"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "get_robust_list, set_robust_list - get/set list of robust futexes"
msgstr ""
"get_robust_list, set_robust_list - возвращает/назначает список надёжных "
"фьютексов (futexes)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/futex.hE<gt>>   /* Definition of B<struct robust_list_head> */\n"
"B<#include E<lt>sys/syscall.hE<gt>>   /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>linux/futex.hE<gt>>   /* определения B<struct robust_list_head> */\n"
"B<#include E<lt>sys/syscall.hE<gt>>   /* определения констант B<SYS_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long syscall(SYS_get_robust_list, int >I<pid>B<,>\n"
"B<             struct robust_list_head **>I<head_ptr>B<, size_t *>I<len_ptr>B<);>\n"
"B<long syscall(SYS_set_robust_list,>\n"
"B<             struct robust_list_head *>I<head>B<, size_t >I<len>B<);>\n"
msgstr ""
"B<long syscall(SYS_get_robust_list, int >I<pid>B<,>\n"
"B<             struct robust_list_head **>I<head_ptr>B<, size_t *>I<len_ptr>B<);>\n"
"B<long syscall(SYS_set_robust_list,>\n"
"B<             struct robust_list_head *>I<head>B<, size_t >I<len>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Glibc does not provide a wrapper for this system call; call it using "
#| "B<syscall>(2)."
msgid ""
"I<Note>: glibc provides no wrappers for these system calls, necessitating "
"the use of B<syscall>(2)."
msgstr ""
"В glibc нет обёртки для данного системного вызова; запускайте его с помощью "
"B<syscall>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These system calls deal with per-thread robust futex lists.  These lists are "
"managed in user space: the kernel knows only about the location of the head "
"of the list.  A thread can inform the kernel of the location of its robust "
"futex list using B<set_robust_list>().  The address of a thread's robust "
"futex list can be obtained using B<get_robust_list>()."
msgstr ""
"Данные системные вызовы служат для ведения понетевых списков надёжных "
"фьютексов. Данные списки управляются из пользовательского пространства: ядро "
"знает только расположение начала списка. Нить может информировать ядро о "
"расположении своего списка надёжных фьютексов с помощью "
"B<set_robust_list>(). Адрес списка надёжных фьютексов нити можно получить с "
"помощью B<get_robust_list>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The purpose of the robust futex list is to ensure that if a thread "
"accidentally fails to unlock a futex before terminating or calling "
"B<execve>(2), another thread that is waiting on that futex is notified that "
"the former owner of the futex has died.  This notification consists of two "
"pieces: the B<FUTEX_OWNER_DIED> bit is set in the futex word, and the kernel "
"performs a B<futex>(2)  B<FUTEX_WAKE> operation on one of the threads "
"waiting on the futex."
msgstr ""
"Предназначением списка надёжных фьютексов является гарантия того, что если "
"нить неожиданно из-за ошибки не разблокирует фьютекс перед завершением или "
"вызовом B<execve>(2), другая ожидающая этот фьютекс нить получит уведомление "
"о том, что бывший владелец фьютекса прекратил работу. Данное уведомление "
"состоит из двух частей: установленного бита B<FUTEX_OWNER_DIED> в слове "
"фьютекса и выполнение ядром B<futex>(2) с операцией B<FUTEX_WAKE> для одной "
"из нитей, ожидающих фьютекс."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<get_robust_list>()  system call returns the head of the robust futex "
"list of the thread whose thread ID is specified in I<pid>.  If I<pid> is 0, "
"the head of the list for the calling thread is returned.  The list head is "
"stored in the location pointed to by I<head_ptr>.  The size of the object "
"pointed to by I<**head_ptr> is stored in I<len_ptr>."
msgstr ""
"Системный вызов B<get_robust_list>() возвращает начало списка надёжных "
"фьютексов нити, идентификатор которой указан в I<pid>. Если значение I<pid> "
"равно 0, то возвращается начало списка вызывающей нити. Начало списка "
"сохраняется в расположение, указанное I<head_ptr>. Размер объекта, "
"указываемый I<**head_ptr>, сохраняется в I<len_ptr>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Permission to employ B<get_robust_list>()  is governed by a ptrace access "
"mode B<PTRACE_MODE_READ_REALCREDS> check; see B<ptrace>(2)."
msgstr ""
"Право вызывать B<get_robust_list>() определяется проверкой режима доступа "
"ptrace B<PTRACE_MODE_READ_REALCREDS>; смотрите B<ptrace>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<set_robust_list>()  system call requests the kernel to record the head "
"of the list of robust futexes owned by the calling thread.  The I<head> "
"argument is the list head to record.  The I<len> argument should be "
"I<sizeof(*head)>."
msgstr ""
"Системный вызов B<set_robust_list>() запрашивает ядро записать начало списка "
"надёжных фьютексов, принадлежащего вызывающей нити. Аргумент I<head> "
"содержит начало списка для записи. Аргумент I<len> должен быть равен "
"I<sizeof(*head)>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<set_robust_list>()  and B<get_robust_list>()  system calls return zero "
"when the operation is successful, an error code otherwise."
msgstr ""
"Системные вызовы B<set_robust_list>() и B<get_robust_list>() возвращают ноль "
"при успешном выполнении и код ошибки в противном случае."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<set_robust_list>()  system call can fail with the following error:"
msgstr ""
"Системный вызов B<pthread_setcancelstate>() может завершиться со следующей "
"ошибкой:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<len> does not equal I<sizeof(struct\\ robust_list_head)>."
msgstr "Значение I<len> не равно I<sizeof(struct\\ robust_list_head)>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<get_robust_list>()  system call can fail with the following errors:"
msgstr ""
"Системный вызов B<get_robust_list>() может завершиться со следующими "
"ошибками:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The head of the robust futex list can't be stored at the location I<head>."
msgstr ""
"Начало списка надёжных фьютексов невозможно сохранить в расположение I<head>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The calling process does not have permission to see the robust futex list of "
"the thread with the thread ID I<pid>, and does not have the "
"B<CAP_SYS_PTRACE> capability."
msgstr ""
"Вызывающий процесс не имеет прав на просмотр списка надёжных фьютексов нити "
"с идентификатором I<pid> и не имеет мандата B<CAP_SYS_PTRACE>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr "B<ESRCH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "No thread with the thread ID I<pid> could be found."
msgstr "Нить с идентификатором I<pid> не найдена."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These system calls were added in Linux 2.6.17."
msgstr "Данные системные вызовы были добавлены в Linux 2.6.17."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "These system calls are nonstandard Linux extensions."
msgid "These system calls are not needed by normal applications."
msgstr "Данные системные вызовы являются нестандартными расширениями Linux."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A thread can have only one robust futex list; therefore applications that "
"wish to use this functionality should use the robust mutexes provided by "
"glibc."
msgstr ""
"В нити может быть только один список надёжных фьютексов; поэтому приложения, "
"которым требуется данное свойство, должны использовать мьютексы, "
"предоставляемые glibc."

#.  commit 8141c7f3e7aee618312fa1c15109e1219de784a7
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the initial implementation, a thread waiting on a futex was notified that "
"the owner had died only if the owner terminated.  Starting with Linux "
"2.6.28, notification was extended to include the case where the owner "
"performs an B<execve>(2)."
msgstr ""
"В первоначальной реализации нить, ожидающая фьютекс, уведомлялась о кончине "
"владельца только, если владелец прекращал работу. Начиная с Linux 2.6.28 "
"уведомление также посылается при выполнении владельцем B<execve>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The thread IDs mentioned in the main text are I<kernel> thread IDs of the "
"kind returned by B<clone>(2)  and B<gettid>(2)."
msgstr ""
"Идентификаторы нитей, упоминаемые в основном тексте, являются I<ядерными> "
"идентификаторами нити, которые возвращаются из B<clone>(2) и B<gettid>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<futex>(2), B<pthread_mutexattr_setrobust>(3)"
msgstr "B<futex>(2), B<pthread_mutexattr_setrobust>(3)"

#. #-#-#-#-#  archlinux: get_robust_list.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: get_robust_list.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: get_robust_list.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-39: get_robust_list.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: get_robust_list.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: get_robust_list.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: get_robust_list.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://lwn.net/Articles/172149/
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: get_robust_list.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Documentation/robust-futexes.txt> and I<Documentation/robust-futex-ABI."
"txt> in the Linux kernel source tree"
msgstr ""
"Файлы I<Documentation/robust-futexes.txt> и I<Documentation/robust-futex-ABI."
"txt> в дереве исходного кода ядра Linux"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GET_ROBUST_LIST"
msgstr "GET_ROBUST_LIST"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux System Calls"
msgstr "Системные вызовы Linux"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<#include E<lt>linux/futex.hE<gt>>\n"
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>syscall.hE<gt>>\n"
msgstr ""
"B<#include E<lt>linux/futex.hE<gt>>\n"
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>syscall.hE<gt>>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<long get_robust_list(int >I<pid>B<, struct robust_list_head **>I<head_ptr>B<,>\n"
"B<                     size_t *>I<len_ptr>B<);>\n"
"B<long set_robust_list(struct robust_list_head *>I<head>B<, size_t >I<len>B<);>\n"
msgstr ""
"B<long get_robust_list(int >I<pid>B<, struct robust_list_head **>I<head_ptr>B<,>\n"
"B<                     size_t *>I<len_ptr>B<);>\n"
"B<long set_robust_list(struct robust_list_head *>I<head>B<, size_t >I<len>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid "I<Note>: There are no glibc wrappers for these system calls; see NOTES."
msgstr ""
"I<Замечание>: В glibc нет обёрточных функций для этих системных вызовов; "
"смотрите ЗАМЕЧАНИЯ."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"These system calls are not needed by normal applications.  No support for "
"them is provided in glibc.  In the unlikely event that you want to call them "
"directly, use B<syscall>(2)."
msgstr ""
"Эти системные вызовы не нужны обычным приложениям. Поддержка в glibc "
"отсутствует. В маловероятном случае, когда вы хотите вызвать их напрямую, "
"используйте B<syscall>(2)."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
