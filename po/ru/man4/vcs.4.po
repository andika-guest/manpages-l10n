# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2016.
# Dmitriy Ovchinnikov <dmitriyxt5@gmail.com>, 2012.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2012-2019.
# Иван Павлов <pavia00@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:32+0200\n"
"PO-Revision-Date: 2019-10-15 19:00+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "vcs"
msgstr "vcs"

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-05-03"
msgstr "3 мая 2023 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "vcs, vcsa - virtual console memory"
msgstr "vcs, vcsa - память виртуальной консоли"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I</dev/vcs0> is a character device with major number 7 and minor number 0, "
"usually with mode 0644 and ownership root:tty.  It refers to the memory of "
"the currently displayed virtual console terminal."
msgstr ""
"I</dev/vcs0> представляет собой символьное устройство со старшим номером 7 и "
"младшим 0, обычно имеет права доступа 0644 и принадлежит root:tty. "
"Устройство указывает на память отображаемого в данный момент виртуального "
"консольного терминала."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I</dev/vcs[1-63]> are character devices for virtual console terminals, they "
"have major number 7 and minor number 1 to 63, usually mode 0644 and "
"ownership root:tty.  I</dev/vcsa[0-63]> are the same, but using I<unsigned "
"short>s (in host byte order) that include attributes, and prefixed with four "
"bytes giving the screen dimensions and cursor position: I<lines>, "
"I<columns>, I<x>, I<y>.  (I<x> = I<y> = 0 at the top left corner of the "
"screen.)"
msgstr ""
"I</dev/vcs[1-63]> представляют собой символьные устройства виртуальных "
"консольных терминалов, имеют старший номер 7 и младшие от 1 до 63, обычно "
"имеют права доступа 0644 и принадлежат root:tty. I</dev/vcsa[0-63]> "
"представляют собой  те же устройства, но имеют атрибуты в виде чисел типа "
"I<unsigned short> (с порядком байт узла) и приставкой из четырех байтов, "
"задающих размеры экрана и положение курсора: I<lines>, I<columns>, I<x>, "
"I<y>.  (I<x> = I<y> = 0 означает верхний левый угол экрана.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "When a 512-character font is loaded, the 9th bit position can be fetched "
#| "by applying the B<ioctl>(2)  B<VT_GETHIFONTMASK> operation (available in "
#| "Linux kernels 2.6.18 and above)  on I</dev/tty[1-63]>; the value is "
#| "returned in the I<unsigned short> pointed to by the third B<ioctl>(2)  "
#| "argument."
msgid ""
"When a 512-character font is loaded, the 9th bit position can be fetched by "
"applying the B<ioctl>(2)  B<VT_GETHIFONTMASK> operation (available since "
"Linux 2.6.18)  on I</dev/tty[1-63]>; the value is returned in the I<unsigned "
"short> pointed to by the third B<ioctl>(2)  argument."
msgstr ""
"При загрузке шрифта с 512 символами значение 9-го бита можно получить через "
"B<ioctl>(2) с помощью операции B<VT_GETHIFONTMASK> (доступна в ядрах Linux "
"версии 2.6.18 и новее) над I</dev/tty[1-63]>; третий аргумент B<ioctl>(2) "
"содержит указатель на I<unsigned short>, куда возвращается результат."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These devices replace the screendump B<ioctl>(2)  operations of "
"B<ioctl_console>(2), so the system administrator can control access using "
"filesystem permissions."
msgstr ""
"Эти устройства заменяют операции B<ioctl>(2) по снятию снимка экрана "
"B<ioctl_console>(2), и позволяют системному администратору контролировать "
"доступ с помощью файловой системы."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The devices for the first eight virtual consoles may be created by:"
msgstr ""
"Устройства для первых восьми виртуальных консолей могут быть созданы с "
"помощью следующих команд:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"for x in 0 1 2 3 4 5 6 7 8; do\n"
"    mknod -m 644 /dev/vcs$x c 7 $x;\n"
"    mknod -m 644 /dev/vcsa$x c 7 $[$x+128];\n"
"done\n"
"chown root:tty /dev/vcs*\n"
msgstr ""
"for x in 0 1 2 3 4 5 6 7 8; do\n"
"    mknod -m 644 /dev/vcs$x c 7 $x;\n"
"    mknod -m 644 /dev/vcsa$x c 7 $[$x+128];\n"
"done\n"
"chown root:tty /dev/vcs*\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "No B<ioctl>(2)  requests are supported."
msgstr "Запросы B<ioctl>(2) не поддерживаются."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/vcs[0-63]>"
msgstr "I</dev/vcs[0-63]>"

#.  .SH AUTHOR
#.  Andries Brouwer <aeb@cwi.nl>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/vcsa[0-63]>"
msgstr "I</dev/vcsa[0-63]>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "Introduced with version 1.1.92 of the Linux kernel."
msgid "Introduced with Linux 1.1.92."
msgstr "Впервые появились в версии ядра Linux 1.1.92."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "You may do a screendump on vt3 by switching to vt1 and typing"
msgstr "Вы можете сделать снимок экрана vt3, переключившись на vt1 и написав"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "cat /dev/vcs3 E<gt>foo\n"
msgstr "cat /dev/vcs3 E<gt>foo\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that the output does not contain newline characters, so some processing "
"may be required, like in"
msgstr ""
"Заметим, что вывод не будет содержать символов новой строки, поэтому может "
"потребоваться дополнительная обработка, например"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "fold -w 81 /dev/vcs3 | lpr\n"
msgstr "fold -w 81 /dev/vcs3 | lpr\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or (horrors)"
msgstr "или (жуть)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "setterm -dump 3 -file /proc/self/fd/1\n"
msgstr "setterm -dump 3 -file /proc/self/fd/1\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The I</dev/vcsa0> device is used for Braille support."
msgstr "Устройство I</dev/vcsa0> используется для поддержки дисплея Брайля."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This program displays the character and screen attributes under the cursor "
"of the second virtual console, then changes the background color there:"
msgstr ""
"Эта программа выводит символ и атрибуты экрана, находящиеся под курсором на "
"второй виртуальной консоли, и затем меняет цвет фона в том же месте."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide
#, fuzzy, no-wrap
#| msgid ""
#| "    fd = open(console, O_RDWR);\n"
#| "    if (fd E<lt> 0) {\n"
#| "        perror(console);\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
#| "    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
#| "        perror(\"VT_GETHIFONTMASK\");\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
#| "    (void) close(fd);\n"
#| "    fd = open(device, O_RDWR);\n"
#| "    if (fd E<lt> 0) {\n"
#| "        perror(device);\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
#| "    (void) read(fd, &scrn, 4);\n"
#| "    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), SEEK_SET);\n"
#| "    (void) read(fd, &s, 2);\n"
#| "    ch = s & 0xff;\n"
#| "    if (s & mask)\n"
#| "        ch |= 0x100;\n"
#| "    attrib = ((s & \\(timask) E<gt>E<gt> 8);\n"
#| "    printf(\"ch=%#03x attrib=%#02x\\en\", ch, attrib);\n"
#| "    s \\(ha= 0x1000;\n"
#| "    (void) lseek(fd, -2, SEEK_CUR);\n"
#| "    (void) write(fd, &s, 2);\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>linux/vt.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    int fd;\n"
"    char *device = \"/dev/vcsa2\";\n"
"    char *console = \"/dev/tty2\";\n"
"    struct {unsigned char lines, cols, x, y;} scrn;\n"
"    unsigned short s;\n"
"    unsigned short mask;\n"
"    unsigned char attrib;\n"
"    int ch;\n"
"\\&\n"
"    fd = open(console, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(console);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
"        perror(\"VT_GETHIFONTMASK\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) close(fd);\n"
"    fd = open(device, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(device);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) read(fd, &scrn, 4);\n"
"    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), SEEK_SET);\n"
"    (void) read(fd, &s, 2);\n"
"    ch = s & 0xff;\n"
"    if (s & mask)\n"
"        ch |= 0x100;\n"
"    attrib = ((s & \\[ti]mask) E<gt>E<gt> 8);\n"
"    printf(\"ch=%#03x attrib=%#02x\\en\", ch, attrib);\n"
"    s \\[ha]= 0x1000;\n"
"    (void) lseek(fd, -2, SEEK_CUR);\n"
"    (void) write(fd, &s, 2);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    fd = open(console, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(console);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
"        perror(\"VT_GETHIFONTMASK\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) close(fd);\n"
"    fd = open(device, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(device);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) read(fd, &scrn, 4);\n"
"    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), SEEK_SET);\n"
"    (void) read(fd, &s, 2);\n"
"    ch = s & 0xff;\n"
"    if (s & mask)\n"
"        ch |= 0x100;\n"
"    attrib = ((s & \\(timask) E<gt>E<gt> 8);\n"
"    printf(\"ch=%#03x attrib=%#02x\\en\", ch, attrib);\n"
"    s \\(ha= 0x1000;\n"
"    (void) lseek(fd, -2, SEEK_CUR);\n"
"    (void) write(fd, &s, 2);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ioctl_console>(2), B<tty>(4), B<ttyS>(4), B<gpm>(8)"
msgstr "B<ioctl_console>(2), B<tty>(4), B<ttyS>(4), B<gpm>(8)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>linux/vt.hE<gt>\n"
msgstr ""
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>linux/vt.hE<gt>\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    int fd;\n"
"    char *device = \"/dev/vcsa2\";\n"
"    char *console = \"/dev/tty2\";\n"
"    struct {unsigned char lines, cols, x, y;} scrn;\n"
"    unsigned short s;\n"
"    unsigned short mask;\n"
"    unsigned char attrib;\n"
"    int ch;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    int fd;\n"
"    char *device = \"/dev/vcsa2\";\n"
"    char *console = \"/dev/tty2\";\n"
"    struct {unsigned char lines, cols, x, y;} scrn;\n"
"    unsigned short s;\n"
"    unsigned short mask;\n"
"    unsigned char attrib;\n"
"    int ch;\n"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    fd = open(console, O_RDWR);\n"
#| "    if (fd E<lt> 0) {\n"
#| "        perror(console);\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
#| "    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
#| "        perror(\"VT_GETHIFONTMASK\");\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
#| "    (void) close(fd);\n"
#| "    fd = open(device, O_RDWR);\n"
#| "    if (fd E<lt> 0) {\n"
#| "        perror(device);\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
#| "    (void) read(fd, &scrn, 4);\n"
#| "    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), SEEK_SET);\n"
#| "    (void) read(fd, &s, 2);\n"
#| "    ch = s & 0xff;\n"
#| "    if (s & mask)\n"
#| "        ch |= 0x100;\n"
#| "    attrib = ((s & \\(timask) E<gt>E<gt> 8);\n"
#| "    printf(\"ch=%#03x attrib=%#02x\\en\", ch, attrib);\n"
#| "    s \\(ha= 0x1000;\n"
#| "    (void) lseek(fd, -2, SEEK_CUR);\n"
#| "    (void) write(fd, &s, 2);\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"    fd = open(console, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(console);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
"        perror(\"VT_GETHIFONTMASK\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) close(fd);\n"
"    fd = open(device, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(device);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) read(fd, &scrn, 4);\n"
"    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), SEEK_SET);\n"
"    (void) read(fd, &s, 2);\n"
"    ch = s & 0xff;\n"
"    if (s & mask)\n"
"        ch |= 0x100;\n"
"    attrib = ((s & \\[ti]mask) E<gt>E<gt> 8);\n"
"    printf(\"ch=%#03x attrib=%#02x\\en\", ch, attrib);\n"
"    s \\[ha]= 0x1000;\n"
"    (void) lseek(fd, -2, SEEK_CUR);\n"
"    (void) write(fd, &s, 2);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    fd = open(console, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(console);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
"        perror(\"VT_GETHIFONTMASK\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) close(fd);\n"
"    fd = open(device, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(device);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) read(fd, &scrn, 4);\n"
"    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), SEEK_SET);\n"
"    (void) read(fd, &s, 2);\n"
"    ch = s & 0xff;\n"
"    if (s & mask)\n"
"        ch |= 0x100;\n"
"    attrib = ((s & \\(timask) E<gt>E<gt> 8);\n"
"    printf(\"ch=%#03x attrib=%#02x\\en\", ch, attrib);\n"
"    s \\(ha= 0x1000;\n"
"    (void) lseek(fd, -2, SEEK_CUR);\n"
"    (void) write(fd, &s, 2);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "VCS"
msgstr "VCS"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-05-03"
msgstr "3 мая 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I</dev/vcs0> is a character device with major number 7 and minor number 0, "
"usually of mode 0644 and owner root.tty.  It refers to the memory of the "
"currently displayed virtual console terminal."
msgstr ""
"I</dev/vcs0> представляет собой символьное устройство со старшим номером 7 и "
"младшим 0, обычно имеет права доступа 0644 и принадлежит root:tty. "
"Устройство указывает на память отображаемого в данный момент виртуального "
"консольного терминала."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I</dev/vcs[1-63]> are character devices for virtual console terminals, they "
"have major number 7 and minor number 1 to 63, usually mode 0644 and owner "
"root.tty.  I</dev/vcsa[0-63]> are the same, but using I<unsigned short>s (in "
"host byte order) that include attributes, and prefixed with four bytes "
"giving the screen dimensions and cursor position: I<lines>, I<columns>, "
"I<x>, I<y>.  (I<x> = I<y> = 0 at the top left corner of the screen.)"
msgstr ""
"I</dev/vcs[1-63]> представляют собой символьные устройства виртуальных "
"консольных терминалов, имеют старший номер 7 и младшие от 1 до 63, обычно "
"имеют права доступа 0644 и принадлежат root:tty. I</dev/vcsa[0-63]> "
"представляют собой  те же устройства, но имеют атрибуты в виде чисел типа "
"I<unsigned short> (с порядком байт узла) и приставкой из четырех байтов, "
"задающих размеры экрана и положение курсора: I<lines>, I<columns>, I<x>, "
"I<y>.  (I<x> = I<y> = 0 означает верхний левый угол экрана.)"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"When a 512-character font is loaded, the 9th bit position can be fetched by "
"applying the B<ioctl>(2)  B<VT_GETHIFONTMASK> operation (available in Linux "
"kernels 2.6.18 and above)  on I</dev/tty[1-63]>; the value is returned in "
"the I<unsigned short> pointed to by the third B<ioctl>(2)  argument."
msgstr ""
"При загрузке шрифта с 512 символами значение 9-го бита можно получить через "
"B<ioctl>(2) с помощью операции B<VT_GETHIFONTMASK> (доступна в ядрах Linux "
"версии 2.6.18 и новее) над I</dev/tty[1-63]>; третий аргумент B<ioctl>(2) "
"содержит указатель на I<unsigned short>, куда возвращается результат."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Introduced with version 1.1.92 of the Linux kernel."
msgstr "Впервые появились в версии ядра Linux 1.1.92."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "ПРИМЕР"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    cat /dev/vcs3 E<gt>foo\n"
msgstr "    cat /dev/vcs3 E<gt>foo\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    fold -w 81 /dev/vcs3 | lpr\n"
msgstr "    fold -w 81 /dev/vcs3 | lpr\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "    xetterm -dump 3 -file /proc/self/fd/1\n"
msgstr "    xetterm -dump 3 -file /proc/self/fd/1\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    int fd;\n"
"    char *device = \"/dev/vcsa2\";\n"
"    char *console = \"/dev/tty2\";\n"
"    struct {unsigned char lines, cols, x, y;} scrn;\n"
"    unsigned short s;\n"
"    unsigned short mask;\n"
"    unsigned char ch, attrib;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    int fd;\n"
"    char *device = \"/dev/vcsa2\";\n"
"    char *console = \"/dev/tty2\";\n"
"    struct {unsigned char lines, cols, x, y;} scrn;\n"
"    unsigned short s;\n"
"    unsigned short mask;\n"
"    unsigned char ch, attrib;\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"    fd = open(console, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(console);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
"        perror(\"VT_GETHIFONTMASK\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) close(fd);\n"
"    fd = open(device, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(device);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) read(fd, &scrn, 4);\n"
"    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), 0);\n"
"    (void) read(fd, &s, 2);\n"
"    ch = s & 0xff;\n"
"    if (attrib & mask)\n"
"        ch |= 0x100;\n"
"    attrib = ((s & ~mask) E<gt>E<gt> 8);\n"
"    printf(\"ch=\\(aq%c\\(aq attrib=0x%02x\\en\", ch, attrib);\n"
"    attrib ^= 0x10;\n"
"    (void) lseek(fd, -1, 1);\n"
"    (void) write(fd, &attrib, 1);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    fd = open(console, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(console);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    if (ioctl(fd, VT_GETHIFONTMASK, &mask) E<lt> 0) {\n"
"        perror(\"VT_GETHIFONTMASK\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) close(fd);\n"
"    fd = open(device, O_RDWR);\n"
"    if (fd E<lt> 0) {\n"
"        perror(device);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    (void) read(fd, &scrn, 4);\n"
"    (void) lseek(fd, 4 + 2*(scrn.y*scrn.cols + scrn.x), 0);\n"
"    (void) read(fd, &s, 2);\n"
"    ch = s & 0xff;\n"
"    if (attrib & mask)\n"
"        ch |= 0x100;\n"
"    attrib = ((s & ~mask) E<gt>E<gt> 8);\n"
"    printf(\"ch=\\(aq%c\\(aq attrib=0x%02x\\en\", ch, attrib);\n"
"    attrib ^= 0x10;\n"
"    (void) lseek(fd, -1, 1);\n"
"    (void) write(fd, &attrib, 1);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
