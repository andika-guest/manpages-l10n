# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014, 2016-2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:29+0200\n"
"PO-Revision-Date: 2019-09-16 18:46+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "/etc/terminfo"
msgid "termio"
msgstr "/etc/terminfo"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "termio - System V terminal driver interface"
msgstr "termio - интерфейс драйвера терминала System V"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<termio> is the name of the old System V terminal driver interface.  This "
"interface defined a I<termio> structure used to store terminal settings, and "
"a range of B<ioctl>(2)  operations to get and set terminal attributes."
msgstr ""
"B<termio> — название старого интерфейса драйвера терминала System V. В этом "
"интерфейсе определена структура I<termio>, которая используется для хранения "
"настроек терминала, и набор операций B<ioctl>(2) для выборки и изменения "
"атрибутов терминала."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<termio> interface is now obsolete: POSIX.1-1990 standardized a "
"modified version of this interface, under the name B<termios>.  The POSIX.1 "
"data structure differs slightly from the System V version, and POSIX.1 "
"defined a suite of functions to replace the various B<ioctl>(2)  operations "
"that existed in System V.  (This was done because B<ioctl>(2)  was "
"unstandardized, and its variadic third argument does not allow argument type "
"checking.)"
msgstr ""
"В настоящее время интерфейс B<termio> устарел: в POSIX.1-1990 "
"стандартизована изменённая версия данного интерфейса под именем B<termios>. "
"В POSIX.1 структура данных немного отличается от версии System V, а также в "
"POSIX.1 определён комплект функций, заменяющих различные операции "
"B<ioctl>(2), которые существовали в System V (это сделано из-за "
"нестандаризованности B<ioctl>(2) и непостоянной длины (variadic) третьего "
"аргумента, что не позволяет проверить тип аргумента)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If you're looking for a page called \"termio\", then you can probably find "
"most of the information that you seek in either B<termios>(3)  or "
"B<ioctl_tty>(2)."
msgstr ""
"Если вы ищете данные на странице с именем «termio», то большинство "
"информации найдёте на странице B<termios>(3) или B<ioctl_tty>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<reset>(1), B<setterm>(1), B<stty>(1), B<ioctl_tty>(2), B<termios>(3), "
"B<tty>(4)"
msgstr ""
"B<reset>(1), B<setterm>(1), B<stty>(1), B<ioctl_tty>(2), B<termios>(3), "
"B<tty>(4)"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr "Linux man-pages 6.05"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "TERMIO"
msgstr "TERMIO"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-05-03"
msgstr "3 мая 2017 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
