# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-10-02 12:02+0200\n"
"PO-Revision-Date: 2022-07-22 22:20+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-GLUE-EFI"
msgstr "GRUB-GLUE-EFI"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2023"
msgstr "juli 2023"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "GRUB 2.06-13"
msgid "GRUB 2:2.12rc1-1"
msgstr "GRUB 2.06-13"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-glue-efi - generate a fat binary for EFI"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-glue-efi> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgstr "B<grub-glue-efi> [I<\\,FLAGGA\\/>...] [I<\\,FLAGGOR\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"grub-glue-efi processes ia32 and amd64 EFI images and glues them according "
"to Apple format."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Glue 32-bit and 64-bit binary into Apple universal one."
msgstr "Limma ihop 32- och 64-bitars binär till en Apple universal."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-3>, B<--input32>=I<\\,FILE\\/>"
msgstr "B<-3>, B<--input32>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set input filename for 32-bit part."
msgstr "ange inmatningsfilnamn för 32-bitarsdel."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-6>, B<--input64>=I<\\,FILE\\/>"
msgstr "B<-6>, B<--input64>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set input filename for 64-bit part."
msgstr "ange inmatningsfilnamn för 64-bitarsdel."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set output filename. Default is STDOUT"
msgstr "ange filnamn för utdata. Standard är STDUT"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "skriv ut informativa meddelanden."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "visa denna hjälplista"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "ge ett kort användningsmeddelande"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "skriv ut programversion"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Obligatoriska eller valfria argument till långa flaggor är också "
"obligatoriska eller valfria för motsvarande korta flaggor."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapportera fel till E<lt>bug-grub@gnu.orgE<gt>. Skicka synpunkter på "
"översättningen till E<gt>tp-sv@listor.tp-sv.seE<lt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-glue-efi> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-glue-efi> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fullständiga dokumentationen för B<grub-glue-efi> underhålls som en "
"Texinfo-manual. Om programmen B<info> och B<grub-glue-efi> är ordentligt "
"installerade på ditt system, bör kommandot"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-glue-efi>"
msgstr "B<info grub-glue-efi>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "ge dig tillgång till den kompletta manualen."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "April 2023"
msgstr "april 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13"
msgstr "GRUB 2.06-13"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "September 2023"
msgstr "september 2023"

#. type: TH
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "GRUB 2.06-13"
msgid "GRUB 2.12~rc1-10"
msgstr "GRUB 2.06-13"
