# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-10-02 12:02+0200\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-EDITENV"
msgstr "GRUB-EDITENV"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "September 2023"
msgstr "September 2023"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brukerkommandoer"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Tool to edit environment block."
msgid "grub-editenv - edit GRUB environment block"
msgstr "Verktøy for å redigere miljøblokk."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-editenv> [I<\\,OPTION\\/>...] I<\\,FILENAME COMMAND\\/>"
msgstr "B<grub-editenv> [I<\\,VALG\\/>...] I<\\,FILNAVN KOMMANDO\\/>"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Tool to edit environment block."
msgstr "Verktøy for å redigere miljøblokk."

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Commands:"
msgstr "Kommandoer:"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "create"
msgstr "create"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Create a blank environment block file."
msgstr "Lag en tom miljø-blokkfil."

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "unset [NAME ...]"
msgid "incr [NAME ...]"
msgstr "unset [NAVN …]"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid "Delete variables."
msgid "Increase value of integer variables."
msgstr "Fjern variabler."

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "list"
msgstr "list"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "List the current variables."
msgstr "Vis gjeldende variabler."

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "set [NAME=VALUE ...]"
msgstr "set [NAVN=VERDI …]"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Set variables."
msgstr "Velg («set») verdier for variabler."

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "unset [NAME ...]"
msgstr "unset [NAVN …]"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Delete variables."
msgstr "Fjern variabler."

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Options:"
msgstr "Valg:"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "vis denne hjelpelista"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "vis en kortfattet bruksanvisning"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print verbose messages."
msgstr "skriv ut detaljerte meldinger"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print program version"
msgstr "skriv ut programversjon"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"If FILENAME is `-', the default value I<\\,/boot/grub2/grubenv\\/> is used."
msgstr ""
"Hvis FILNAVN er «-», brukes standardverdien I<\\,/boot/grub2/grubenv\\/>."

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"There is no `delete' command; if you want to delete the whole environment "
"block, use `rm /boot/grub2/grubenv'."
msgstr ""
"Det finnes ingen «delete»-kommando. Bruk «rm /boot/grub2/grubenv» hvis du "
"vil slette hele miljøblokka."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Rapporter feil til E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-reboot>(8), B<grub-set-default>(8)"
msgstr "B<grub-reboot>(8), B<grub-set-default>(8)"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-editenv> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-editenv> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fullstendige dokumentasjonen for B<grub-editenv> opprettholdes som en "
"Texinfo manual. Dersom B<info> og B<grub-editenv> programmene er riktig "
"installert på ditt sted burde kommandoen"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<info grub-editenv>"
msgstr "B<info grub-editenv>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "gi deg tilgang til hele manualen."

#. type: TH
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "March 2023"
msgid "May 2023"
msgstr "Mars 2023"

#. type: TH
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "grub-editenv (GRUB2) 2.04"
msgid "grub-editenv (GRUB2) 2.06"
msgstr "grub-editenv (GRUB2) 2.04"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "August 2023"
msgstr "August 2023"

#. type: TH
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "GRUB 2:2.12rc1-1"
msgid "GRUB2 2.12~rc1"
msgstr "GRUB 2:2.12rc1-1"
