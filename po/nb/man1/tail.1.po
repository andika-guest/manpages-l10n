# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-10-02 12:31+0200\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "TAIL"
msgstr "TAIL"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "September 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brukerkommandoer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "tail - output the last part of files"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<tail> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<tail> [I<\\,VALG\\/>]... [I<\\,FIL\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Print the last 10 lines of each FILE to standard output.  With more than one "
"FILE, precede each with a header giving the file name."
msgstr ""
"Skriv ut de siste 10 linjene fra hver FIL til standardutdata. Hvis flere "
"FILer er valgt, vises hver av dem med filnavnet sitt som overskrift."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""
"Hvis ingen FIL er valgt, eller hvis FIL er «-», leser programmet standard "
"inndata."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumenter som er obligatoriske for lange valg, er også obligatoriske for "
"korte valg."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--bytes>=I<\\,[\\/>+]NUM"
msgstr "B<-c>, B<--bytes>=I<\\,[\\/>+]ANT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"output the last NUM bytes; or use B<-c> +NUM to output starting with byte "
"NUM of each file"
msgstr ""
"Skriv ut siste valgt ANTall byte, eller bruk B<-c> +ANT for å hoppe over "
"valgt ANTall byte i hver fil."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--follow[=>{name|descriptor}]"
msgstr "B<-f>, B<--follow[=>{name|descriptor}]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output appended data as the file grows;"
msgstr "Skriv ut mer data etter hvert som fila vokser."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "an absent option argument means 'descriptor'"
msgstr "(hvis du ikke argumenterer, brukes «descriptor»)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-F>"
msgstr "B<-F>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "same as B<--follow>=I<\\,name\\/> B<--retry>"
msgstr "Tilsvarer B<--follow>=I<\\,name\\/> B<--retry>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--lines>=I<\\,[\\/>+]NUM"
msgstr "B<-n>, B<--lines>=I<\\,[\\/>+]ANT"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "output the last NUM lines, instead of the last 10; or use B<-n> +NUM to "
#| "output starting with line NUM"
msgid ""
"output the last NUM lines, instead of the last 10; or use B<-n> +NUM to skip "
"NUM-1 lines at the start"
msgstr ""
"Skriv ut de siste valgt ANTall linjene, i stedet for de siste 10 (eller bruk "
"«B<-n> +ANT» for å skrive ut linjer fra og med den ANT-ende)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--max-unchanged-stats>=I<\\,N\\/>"
msgstr "B<--max-unchanged-stats>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "with B<--follow>=I<\\,name\\/>, reopen a FILE which has not"
msgstr "Ved bruk sammen med B<--follow>=I<\\,name\\/> åpnes en FIL som"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"changed size after N (default 5) iterations to see if it has been unlinked "
"or renamed (this is the usual case of rotated log files); with inotify, this "
"option is rarely useful"
msgstr ""
"ikke har endret størrelse etter N (standard: %d) endringer på nytt, for å se "
"om den har blitt avlenket eller fått nytt navn (sistnevnte er typisk for "
"rullerende loggfiler). Dette valget er sjeldent nyttig når du bruker "
"«inotify»."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--pid>=I<\\,PID\\/>"
msgstr "B<--pid>=I<\\,PID\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "with B<-f>, terminate after process ID, PID dies"
msgstr ""
"Ved bruk sammen med B<-f> avsluttes tail etter at valgt prosess-ID dør."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-q>, B<--quiet>, B<--silent>"
msgstr "B<-q>, B<--quiet>, B<--silent>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "never output headers giving file names"
msgstr "Ikke skriv ut filnavn som overskrifter."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--retry>"
msgstr "B<--retry>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "keep trying to open a file if it is inaccessible"
msgstr "Prøv å åpne en fil på nytt hvis den er eller blir utilgjengelig."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--sleep-interval>=I<\\,N\\/>"
msgstr "B<-s>, B<--sleep-interval>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"with B<-f>, sleep for approximately N seconds (default 1.0) between "
"iterations; with inotify and B<--pid>=I<\\,P\\/>, check process P at least "
"once every N seconds"
msgstr ""
"Ved bruk sammen med B<-f> tar programmet pause i cirka N antall sekunder "
"(standard: 1.0) mellom gjennomkjøringer. Ved bruk sammen med «inotify» og "
"B<--pid>=I<\\,P\\/>, sjekkes prosess P minst én gang per N antall sekunder."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "always output headers giving file names"
msgstr "Skriv alltid ut overskrifter med filnavn."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr "B<-z>, B<--zero-terminated>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "line delimiter is NUL, not newline"
msgstr "Skill mellom linjer med NUL i stedet for linjeskift."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "Vis denne hjelpeteksten og avslutt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "Vis versjonsinformasjon og avslutt."

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "NUM may have a multiplier suffix: b 512, kB 1000, K 1024, MB 1000*1000, M "
#| "1024*1024, GB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, "
#| "Y.  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgid ""
"NUM may have a multiplier suffix: b 512, kB 1000, K 1024, MB 1000*1000, M "
"1024*1024, GB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y, "
"R, Q.  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"ANT kan brukes med multiplikasjonssuffiks, f.eks. slik: b 512, kB 1000, K "
"1024, MB 1000*1000, M 1024*1024, GB 1000*1000*1000, G 1024*1024*1024, og så "
"videre for T, P, E, Z, Y. Du kan også bruke binærprefiks i form av KiB (K), "
"MiB (M), og så videre."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"With B<--follow> (B<-f>), tail defaults to following the file descriptor, "
"which means that even if a tail'ed file is renamed, tail will continue to "
"track its end.  This default behavior is not desirable when you really want "
"to track the actual name of the file, not the file descriptor (e.g., log "
"rotation).  Use B<--follow>=I<\\,name\\/> in that case.  That causes tail to "
"track the named file in a way that accommodates renaming, removal and "
"creation."
msgstr ""
"Ved bruk av B<--follow> (B<-f>) følger tail etter filbeskriveren, som betyr "
"at tail kan fortsette overvåking av filer selv om de evt. får nye navn. "
"Denne standardoppførselen er uønskelig når du vil overvåke det faktiske "
"filnavnet fremfor filbeskriveren (aktuelt f.eks. for roterende loggfiler). "
"Bruk i så fall B<--follow>=I<\\,name\\/>, som gjør at tail ikke forholder "
"seg bevisst til annet enn filnavnet, uansett om du fjerner og oppretter "
"aktuell fil på nytt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "OPPHAVSMANN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Written by Paul Rubin, David MacKenzie, Ian Lance Taylor, and Jim Meyering."
msgstr ""
"Skrevet av Paul Rubin, David MacKenzie, Ian Lance Taylor og Jim Meyering."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Få hjelp til bruk av GNU coreutils på nett: E<lt>https://www.gnu.org/"
"software/coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapporter oversettelsesfeil til E<lt>https://translationproject.org/team/nb."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "OPPHAVSRETT"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dette er fri programvare. Du kan endre og dele den videre. Det stilles INGEN "
"GARANTI, i den grad dette tillates av gjeldende lovverk."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "head(1)"
msgid "B<head>(1)"
msgstr "B<head>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/tailE<gt>"
msgstr ""
"Fullstendig dokumentasjon: E<lt>https://www.gnu.org/software/coreutils/"
"tailE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) tail invocation\\(aq"
msgstr "eller lokalt: info \\(aq(coreutils) tail invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 mageia-cauldron opensuse-leap-15-6
msgid ""
"output the last NUM lines, instead of the last 10; or use B<-n> +NUM to "
"output starting with line NUM"
msgstr ""
"Skriv ut de siste valgt ANTall linjene, i stedet for de siste 10 (eller bruk "
"«B<-n> +ANT» for å skrive ut linjer fra og med den ANT-ende)."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"NUM may have a multiplier suffix: b 512, kB 1000, K 1024, MB 1000*1000, M "
"1024*1024, GB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, "
"Y.  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"ANT kan brukes med multiplikasjonssuffiks, f.eks. slik: b 512, kB 1000, K "
"1024, MB 1000*1000, M 1024*1024, GB 1000*1000*1000, G 1024*1024*1024, og så "
"videre for T, P, E, Z, Y. Du kan også bruke binærprefiks i form av KiB (K), "
"MiB (M), og så videre."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "April 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "Oktober 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "head(1)"
msgstr "B<head>(1)"
