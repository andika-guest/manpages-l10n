# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-10-02 12:33+0200\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "UNIQ"
msgstr "UNIQ"

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2023"
msgstr "September 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brukerkommandoer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "too many repeated lines"
msgid "uniq - report or omit repeated lines"
msgstr "for mange duplikatlinjer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<uniq> [I<\\,OPTION\\/>]... [I<\\,INPUT \\/>[I<\\,OUTPUT\\/>]]"
msgstr "B<uniq> [I<\\,VALG\\/>]... [I<\\,INNDATA \\/>[I<\\,OUTDATA\\/>]]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Filter adjacent matching lines from INPUT (or standard input), writing to "
"OUTPUT (or standard output)."
msgstr ""
"Filtrer nærliggende samsvarende linjer ut av INNDATA (eller standard "
"inndata), og skriv til UTDATA (eller standardutdata)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With no options, matching lines are merged to the first occurrence."
msgstr ""
"Hvis du ikke bruker noen av valgene, slåes samsvarende linjer sammen til "
"første forekomst."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumenter som er obligatoriske for lange valg, er også obligatoriske for "
"korte valg."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--count>"
msgstr "B<-c>, B<--count>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "prefix lines by the number of occurrences"
msgstr "Vis antall forekomster i begynnelsen av hver linje."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--repeated>"
msgstr "B<-d>, B<--repeated>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "only print duplicate lines, one for each group"
msgstr "Bare skriv ut duplikatlinjer (én per gruppe)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-D>"
msgstr "B<-D>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "print all duplicate lines"
msgstr "Skriv ut alle dupliserte linjer."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--all-repeated>[=I<\\,METHOD\\/>]"
msgstr "B<--all-repeated>[=I<\\,METODE\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"like B<-D>, but allow separating groups with an empty line; "
"METHOD={none(default),prepend,separate}"
msgstr ""
"Likner B<-D>, men holder grupper adskilt med tom linje; "
"METODE={none(standard),prepend,separate}."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--skip-fields>=I<\\,N\\/>"
msgstr "B<-f>, B<--skip-fields>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "avoid comparing the first N fields"
msgstr "Ikke sammenlikn de første N antall feltene."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--group>[=I<\\,METHOD\\/>]"
msgstr "B<--group>[=I<\\,METODE\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"show all items, separating groups with an empty line; "
"METHOD={separate(default),prepend,append,both}"
msgstr ""
"Vis alt, og skill mellom grupper med tomme linjer; "
"METODE={separate(standard),prepend,append,both}."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-case>"
msgstr "B<-i>, B<--ignore-case>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ignore differences in case when comparing"
msgstr "Ignorer små og store bokstaver ved sammenlikning."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--skip-chars>=I<\\,N\\/>"
msgstr "B<-s>, B<--skip-chars>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "avoid comparing the first N characters"
msgstr "Ikke sammenlikn de første N antall tegnene."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--unique>"
msgstr "B<-u>, B<--unique>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "only print unique lines"
msgstr "Bare skriv ut unike linjer."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr "B<-z>, B<--zero-terminated>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "line delimiter is NUL, not newline"
msgstr "Skill mellom linjer med NUL i stedet for linjeskift."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--check-chars>=I<\\,N\\/>"
msgstr "B<-w>, B<--check-chars>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "compare no more than N characters in lines"
msgstr "Ikke sammenlikn flere enn N antall tegn per linje."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "Vis denne hjelpeteksten og avslutt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "Vis versjonsinformasjon og avslutt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A field is a run of blanks (usually spaces and/or TABs), then non-blank "
"characters.  Fields are skipped before chars."
msgstr ""
"Et felt er en streng med tomrom (i form av mellomrom og/eller "
"tabulator(er)), etterfulgt av tegn som ikke betyr tomrom. Programmet hopper "
"over felt før tegn."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note: 'uniq' does not detect repeated lines unless they are adjacent.  You "
"may want to sort the input first, or use 'sort B<-u>' without 'uniq'."
msgstr ""
"Merk: B<uniq> oppdager ikke duplikatlinjer med mindre de ligger etter "
"hverandre. Det kan være lurt å sortere inndata, f.eks. med «sort B<-u>», i "
"stedet for å bruke uniq."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "OPPHAVSMANN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Richard M. Stallman and David MacKenzie."
msgstr "Skrevet av Richard M. Stallman og David MacKenzie."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Få hjelp til bruk av GNU coreutils på nett: E<lt>https://www.gnu.org/"
"software/coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapporter oversettelsesfeil til E<lt>https://translationproject.org/team/nb."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "OPPHAVSRETT"

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dette er fri programvare. Du kan endre og dele den videre. Det stilles INGEN "
"GARANTI, i den grad dette tillates av gjeldende lovverk."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "comm(1), join(1), sort(1)"
msgid "B<comm>(1), B<join>(1), B<sort>(1)"
msgstr "B<comm>(1), B<join>(1), B<sort>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/uniqE<gt>"
msgstr ""
"Fullstendig dokumentasjon: E<lt>https://www.gnu.org/software/coreutils/"
"uniqE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) uniq invocation\\(aq"
msgstr "eller lokalt: info \\(aq(coreutils) uniq invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-39
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "April 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "Oktober 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "comm(1), join(1), sort(1)"
msgstr "B<comm>(1), B<join>(1), B<sort>(1)"
