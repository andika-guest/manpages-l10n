# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-07-25 19:36+0200\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB-OFPATHNAME"
msgstr "GRUB-OFPATHNAME"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2023"
msgstr "juli 2023"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "GRUB 2.11"
msgid "GRUB 2.12~rc1"
msgstr "GRUB 2.11"

#. type: TH
#: archlinux
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemadministrasjonsverktøy"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux
msgid "grub-ofpathname - find OpenBOOT path for a device"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: archlinux
msgid "B<grub-ofpathname> I<\\,DEVICE\\/>"
msgstr "B<grub-ofpathname> I<\\,ENHET\\/>"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux
msgid "B<grub-probe>(8)"
msgstr "B<grub-probe>(8)"

#. type: Plain text
#: archlinux
msgid ""
"The full documentation for B<grub-ofpathname> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-ofpathname> programs are properly "
"installed at your site, the command"
msgstr ""
"Den fullstendige dokumentasjonen for B<grub-ofpathname> opprettholdes som en "
"Texinfo manual. Dersom B<info> og B<grub-ofpathname> programmene er riktig "
"installert på ditt sted burde kommandoen"

#. type: Plain text
#: archlinux
msgid "B<info grub-ofpathname>"
msgstr "B<info grub-ofpathname>"

#. type: Plain text
#: archlinux
msgid "should give you access to the complete manual."
msgstr "gi deg tilgang til hele manualen."
