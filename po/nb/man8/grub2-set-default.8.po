# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-10-02 12:02+0200\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-SET-DEFAULT"
msgstr "GRUB-SET-DEFAULT"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "September 2023"
msgstr "September 2023"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemadministrasjonsverktøy"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Set the default boot menu entry for GRUB."
msgid "grub-set-default - set the saved default boot entry for GRUB"
msgstr "Velg standardoppføring på GRUB-oppstartsmenyen."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-set-default> [I<\\,OPTION\\/>] I<\\,MENU_ENTRY\\/>"
msgstr "B<grub-set-default> [I<\\,VALG\\/>] I<\\,MENYOPPFØRING\\/>"

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Set the default boot menu entry for GRUB.  This requires setting "
"GRUB_DEFAULT=saved in I<\\,/etc/default/grub\\/>."
msgstr ""
"Velg standardoppføring på GRUB-oppstartsmenyen. Dette krever at du gir en "
"verdi til «GRUB_DEFAULT=saved» i fila I<\\,/etc/default/grub\\/>."

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print this message and exit"
msgstr "skriv ut denne meldinga og avslutt"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print the version information and exit"
msgstr "skriv ut versjon og avslutt"

#. type: TP
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--boot-directory>=I<\\,DIR\\/>"
msgstr "B<--boot-directory>=I<\\,MAPPE\\/>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"expect GRUB images under the directory DIR/grub2 instead of the I<\\,/boot/"
"grub2\\/> directory"
msgstr ""
"forvent at GRUB-bilder ligger i mappa DIR/grub2 i stedet for I<\\,/boot/"
"grub2\\/>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "MENU_ENTRY is a number, a menu item title or a menu item identifier."
msgstr ""
"MENYOPPFØRING er enten et nummer, en tittel eller en menyoppførings-ID."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Rapporter feil til E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-reboot>(8), B<grub-editenv>(1)"
msgstr "B<grub-reboot>(8), B<grub-editenv>(1)"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-set-default> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-set-default> programs are properly "
"installed at your site, the command"
msgstr ""
"Den fullstendige dokumentasjonen for B<grub-set-default> opprettholdes som "
"en Texinfo manual. Dersom B<info> og B<grub-set-default> programmene er "
"riktig installert på ditt sted burde kommandoen"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<info grub-set-default>"
msgstr "B<info grub-set-default>"

#. type: Plain text
#: fedora-39 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "gi deg tilgang til hele manualen."

#. type: TH
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "March 2023"
msgid "May 2023"
msgstr "Mars 2023"

#. type: TH
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "grub-set-default (GRUB2) 2.04"
msgid "grub-set-default (GRUB2) 2.06"
msgstr "grub-set-default (GRUB2) 2.04"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "August 2023"
msgstr "August 2023"

#. type: TH
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "GRUB 2:2.12rc1-1"
msgid "GRUB2 2.12~rc1"
msgstr "GRUB 2:2.12rc1-1"
