# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Kovács Emese <emese@eik.bme.hu>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-10 19:09+0200\n"
"PO-Revision-Date: 2001-01-05 12:34+0100\n"
"Last-Translator: Kovács Emese <emese@eik.bme.hu>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: Dd
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "August 15, 1999"
msgstr "1999. augusztus 15."

#. type: Dt
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "RWHO 1"
msgstr "RWHO 1"

#. type: Os
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux NetKit (0.17)"
msgstr "Linux NetKit (0.17)"

#. type: Sh
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "E<.Nm rwho>"
msgstr "E<.Nm rwho>"

#. type: Nd
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "who is logged in on local machines"
msgstr "ki van bejelenkezve a lokális gépekre"

#. type: Sh
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÖSSZEGZÉS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "E<.Nm rwho> E<.Op Fl a>"
msgstr "E<.Nm rwho> E<.Op Fl a>"

#. type: Sh
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid ""
"The E<.Nm rwho> command produces output similar to E<.Xr who>, but for all "
"machines on the local network.  If no report has been received from a "
"machine for 11 minutes then E<.Nm rwho> assumes the machine is down, and "
"does not report users last known to be logged into that machine."
msgstr ""
"Az E<.Nm rwho> parancs kimenete hasonlít a E<.Xr who > kimenetéhez, de a "
"lokális hálózat minden gépére vonatkozik.  Ha egy gép 11 percnél hosszabb "
"időn keresztül nem jelentkezik, az E<.Nm rwho> úgy veszi, hogy a gép ki van "
"kapcsolva és nem jelzi ki a bejelenkezett felhasználók utolsó ismert "
"állapotát."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid ""
"If a users hasn't typed to the system for a minute or more, then E<.Nm rwho> "
"reports this idle time.  If a user hasn't typed to the system for an hour or "
"more, then the user will be omitted from the output of E<.Nm rwho> unless "
"the E<.Fl a> flag is given."
msgstr ""
"Ha egy felhasználó egy percnél hosszabb időn keresztűl nem gépel, az E<.Nm "
"rwho> kijelzi az inaktivitás idejét (\"idle time\"). Ha egy felhasználó több "
"mint egy órán keresztül nem gépel, az E<.Nm rwho> által adott kimenetbôl "
"hiányozni fog a szóban forgó felhasználó neve, hacsak nem adjuk meg az E<.Fl "
"a> opciót."

#. type: Sh
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "FILES"
msgstr "FÁJLOK"

#. type: It
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "Pa /var/spool/rwho/whod.*"
msgstr "Pa /var/spool/rwho/whod.*"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "information about other machines"
msgstr "információ más gépekről"

#. type: Sh
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr "LÁSD MÉG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid ""
"E<.Xr finger 1>, E<.Xr rup 1>, E<.Xr ruptime 1>, E<.Xr rusers 1>, E<.Xr who "
"1>, E<.Xr rwhod 8>"
msgstr ""
"E<.Xr finger 1>, E<.Xr rup 1>, E<.Xr ruptime 1>, E<.Xr rusers 1>, E<.Xr who "
"1>, E<.Xr rwhod 8>"

#. type: Sh
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "HISTORY"
msgstr "TÖRTÉNET"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "The E<.Nm rwho> command appeared in E<.Bx 4.3>."
msgstr "Az E<.Nm rwho> parancs a E<.Bx 4.3> -ben jelent meg."

#. type: Sh
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
#, no-wrap
msgid "BUGS"
msgstr "HIBÁK"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-39 fedora-rawhide
msgid "This is unwieldy when the number of machines on the local net is large."
msgstr ""
"A program használata kényelmetlenné válik, amikor a lokális hálózaton lévő "
"gépek száma nagy."
