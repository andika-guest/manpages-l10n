# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Tevesz Tamás <ice@rulez.org>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-08-27 16:30+0200\n"
"PO-Revision-Date: 2001-01-05 12:34+0100\n"
"Last-Translator: Tevesz Tamás <ice@rulez.org>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: original/man1/watch.1:3
#, no-wrap
msgid "W"
msgstr "W"

#. type: TH
#: original/man1/watch.1:3
#, no-wrap
msgid "2 Sep 1997 "
msgstr ""

#. type: TH
#: original/man1/watch.1:3
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: SH
#: original/man1/watch.1:4
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: original/man1/watch.1:6
msgid "watch - watch a program with update intervals"
msgstr "watch - program figyelése frissítéssel"

#. type: SH
#: original/man1/watch.1:6
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÁTTEKINTÉS"

#. type: Plain text
#: original/man1/watch.1:13
msgid "B<watch> [-n] I<seconds program> [ I<args ...> ]"
msgstr "B<watch> [-n] I<másodperc program> [ I<argumentumok ...> ]"

#. type: SH
#: original/man1/watch.1:13
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: original/man1/watch.1:23
msgid ""
"B<watch > is a curses(3)-based program that allows you to watch a program as "
"it changes.  By default, it updates itself every 2 seconds.  You can specify "
"the number of seconds with the -n option.  The curses packages allows for "
"quick updating of the screen through cursor optimization.  The program will "
"end with a keyboard interrupt, which will leave the screen in a valid yet "
"cleared state."
msgstr ""
"A B<watch > egy curses(3)-alapú program, amely alkalmas másik program "
"futásának változását figyelni. Alapértelmezés szerint kétmásodpercenként "
"frissíti magát.  A -n opció használatával megadható a frissítési intervallum. "
"A curses könyvtár használata miatt a képernyő frissítése gyors. A program "
"futása a billentyűzetről kiadott megszakítással fejeződik be.  A futás "
"befejezése után a képernyő érvényes, törölt állapotban marad."

#. type: SH
#: original/man1/watch.1:24
#, no-wrap
msgid "AUTHORS"
msgstr "SZERZŐK"

#. type: Plain text
#: original/man1/watch.1:26
msgid "B<watch> was written by Tony Rems E<lt>rembo@unisoft.comE<gt>"
msgstr "A B<watch> -ot Tom Rems E<lt>rembo@unisoft.comE<gt> írta."
