# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-10-02 12:30+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-SOFT-REBOOT\\&.SERVICE"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd-soft-reboot.service"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "systemd-soft-reboot.service - Userspace reboot operation"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "systemd-soft-reboot\\&.service"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"systemd-soft-reboot\\&.service is a system service that is pulled in by soft-"
"reboot\\&.target and is responsible for performing a userspace-only reboot "
"operation\\&. When invoked, it will send the B<SIGTERM> signal to any "
"processes left running (but does not follow up with B<SIGKILL>, and does not "
"wait for the processes to exit)\\&. If the /run/nextroot/ directory exists "
"(which may be a regular directory, a directory mount point or a symlink to "
"either) then it will switch the file system root to it\\&. It then "
"reexecutes the service manager off the (possibly now new) root file system, "
"which will enqueue a new boot transaction as in a normal reboot\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Such a userspace-only reboot operation permits updating or resetting the "
"entirety of userspace with minimal downtime, as the reboot operation does "
"I<not> transition through:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"The second phase of regular shutdown, as implemented by B<systemd-"
"shutdown>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"The third phase of regular shutdown, i\\&.e\\&. the return to the initrd "
"context"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "The hardware reboot operation"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "The firmware initialization"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "The boot loader initialization"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "The kernel initialization"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "The initrd initialization"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "However this form of reboot comes with drawbacks as well:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"The OS update remains incomplete, as the kernel is not reset and continues "
"running\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Kernel settings (such as /proc/sys/ settings, a\\&.k\\&.a\\&. \"sysctl\", "
"or /sys/ settings) are not reset\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"These limitations may be addressed by various means, which are outside of "
"the scope of this documentation, such as kernel live-patching and "
"sufficiently comprehensive /etc/sysctl\\&.d/ files\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "RESOURCE PASS-THROUGH"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Various runtime OS resources can passed from a system runtime to the next, "
"through the userspace reboot operation\\&. Specifically:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"File descriptors placed in the file descriptor store of services that remain "
"active until the very end are passed to the next boot, where they are placed "
"in the file descriptor store of the same unit\\&. For this to work, units "
"must declare I<DefaultDependencies=no> (and avoid a manual "
"I<Conflicts=shutdown\\&.target> or similar) to ensure they are not "
"terminated as usual during the system shutdown operation\\&. Alternatively, "
"use I<FileDescriptorStorePreserve=> to allow the file descriptor store to "
"remain pinned even when the unit is down\\&. See B<systemd.service>(5)  for "
"details about the file descriptor store\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Similar to this, file descriptors associated with \\&.socket units remain "
"open (and connectible) if the units are not stopped during the "
"transition\\&. (Achieved by I<DefaultDependencies=no>\\&.)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"The /run/ file system remains mounted and populated and may be used to pass "
"state information between such userspace reboot cycles\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Service processes may continue to run over the transition, if they are "
"placed in services that remain active until the very end of shutdown (which "
"again is achieved via I<DefaultDependencies=no>)\\&. They must also be set "
"up to avoid being killed by the aforementioned B<SIGTERM> spree (as per "
"\\m[blue]B<systemd and Storage Daemons for the Root File "
"System>\\m[]\\&\\s-2\\u[1]\\d\\s+2)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"File system mounts may remain mounted during the transition, and complex "
"storage attached, if configured to remain until the very end of the shutdown "
"process\\&. (Also achieved via I<DefaultDependencies=no>, and by avoiding "
"I<Conflicts=umount\\&.target>)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
msgid ""
"Even though passing resources from one soft reboot cycle to the next is "
"possible this way, we strongly suggest to use this functionality sparingly "
"only, as it creates a more fragile system as resources from different "
"versions of the OS and applications might be mixed with unforeseen "
"consequences\\&. In particular it\\*(Aqs recommended to I<avoid> allowing "
"processes to survive the soft reboot operation, as this means code updates "
"will necessarily be incomplete, and processes typically pin various other "
"resources (such as the file system they are backed by), thus increasing "
"memory usage (as two versions of the OS/application/file system might be "
"kept in memory)\\&. Leaving processes running during a soft-reboot operation "
"requires disconnecting the service comprehensively from the rest of the OS, "
"i\\&.e\\&. minimizing IPC and reducing sharing of resources with the rest of "
"the OS\\&. A possible mechanism to achieve this is the concept of "
"\\m[blue]B<Portable Services>\\m[]\\&\\s-2\\u[2]\\d\\s+2, but make sure no "
"resource from the host\\*(Aqs OS filesystems is pinned via I<BindPaths=> or "
"similar unit settings, otherwise the old, originating filesystem will remain "
"mounted as long as the unit is running\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Note that because B<systemd-shutdown>(8)  is not executed, the executables "
"in /usr/lib/systemd/system-shutdown/ are not executed either\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Note that systemd-soft-reboot\\&.service (and related units) should never be "
"executed directly\\&. Instead, trigger system shutdown with a command such "
"as \"systemctl soft-reboot\"\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemctl>(1), B<systemd.special>(7), B<systemd-poweroff."
"service>(8), B<systemd-suspend.service>(8), B<bootup>(7)"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "systemd and Storage Daemons for the Root File System"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "\\%https://systemd.io/ROOT_STORAGE_DAEMONS"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid " 2."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "Portable Services"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "\\%https://systemd.io/PORTABLE_SERVICES"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"Note that because B<systemd-shutdown>(8)  is not executed, the executables "
"in /lib/systemd/system-shutdown/ are not executed either\\&."
msgstr ""

#. type: Plain text
#: fedora-39
msgid ""
"Even though passing resources from one soft reboot cycle to the next is "
"possible this way, we strongly suggest to use this functionality sparingly "
"only, as it creates a more fragile system as resources from different "
"versions of the OS and applications might be mixed with unforeseen "
"consequences\\&. In particular it\\*(Aqs recommended to I<avoid> allowing "
"processes to survive the soft reboot operation, as this means code updates "
"will necessarily be incomplete, and processes typically pin various other "
"resources (such as the file system they are backed by), thus increasing "
"memory usage (as two versions of the OS/application/file system might be "
"kept in memory)\\&. Leaving processes running during a soft-reboot operation "
"requires disconnecting the service comprehensively from the rest of the OS, "
"i\\&.e\\&. minimizing IPC and reducing sharing of resources with the rest of "
"the OS\\&. A possible mechanism to achieve this is the concept of "
"\\m[blue]B<Portable Services>\\m[]\\&\\s-2\\u[2]\\d\\s+2, but make sure no "
"resource from the host\\*(Aqs root filesystem is pinned via I<BindPaths=> or "
"similar unit settings, otherwise the old root filesystem will be kept in "
"memory as long as the unit is running\\&."
msgstr ""
