# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-10-02 12:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "JOURNAL-REMOTE\\&.CONF"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "journal-remote.conf"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"journal-remote.conf, journal-remote.conf.d - Configuration files for the "
"service accepting remote journal uploads"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "/etc/systemd/journal-remote\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "/etc/systemd/journal-remote\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "/run/systemd/journal-remote\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "/usr/lib/systemd/journal-remote\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These files configure various parameters of B<systemd-journal-remote."
"service>(8)\\&. See B<systemd.syntax>(7)  for a general description of the "
"syntax\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION DIRECTORIES AND PRECEDENCE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. "
"Initially, the main configuration file in /etc/systemd/ contains commented "
"out entries showing the defaults as a guide to the administrator\\&. Local "
"overrides can be created by editing this file or by creating drop-ins, as "
"described below\\&. Using drop-ins for local configuration is recommended "
"over modifications to the main configuration file\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In addition to the \"main\" configuration file, drop-in configuration "
"snippets are read from /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/"
"systemd/*\\&.conf\\&.d/, and /etc/systemd/*\\&.conf\\&.d/\\&. Those drop-ins "
"have higher precedence and override the main configuration file\\&. Files in "
"the *\\&.conf\\&.d/ configuration subdirectories are sorted by their "
"filename in lexicographic order, regardless of in which of the "
"subdirectories they reside\\&. When multiple files specify the same option, "
"for options which accept just a single value, the entry in the file sorted "
"last takes precedence, and for options which accept a list of values, "
"entries are collected as they occur in the sorted files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To disable a configuration file supplied by the vendor, the recommended way "
"is to place a symlink to /dev/null in the configuration directory in /etc/, "
"with the same filename as the vendor configuration file\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "All options are configured in the [Remote] section:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<Seal=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Periodically sign the data in the journal using Forward Secure Sealing\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<SplitMode=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "One of \"host\" or \"none\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<ServerKeyFile=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "SSL key in PEM format\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<ServerCertificateFile=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "SSL certificate in PEM format\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<TrustedCertificateFile=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "SSL CA certificate\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "I<MaxUse=>, I<KeepFree=>, I<MaxFileSize=>, I<MaxFiles=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"These are analogous to I<SystemMaxUse=>, I<SystemKeepFree=>, "
"I<SystemMaxFileSize=> and I<SystemMaxFiles=> in B<journald.conf>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"I<MaxUse=> controls how much disk space the B<systemd-journal-remote> may "
"use up at most\\&.  I<KeepFree=> controls how much disk space B<systemd-"
"journal-remote> shall leave free for other uses\\&.  B<systemd-journal-"
"remote> will respect both limits and use the smaller of the two values\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"I<MaxFiles=> controls how many individual journal files to keep at most\\&. "
"Note that only archived files are deleted to reduce the number of files "
"until this limit is reached; active files will stay around\\&. This means "
"that, in effect, there might still be more journal files around in total "
"than this limit after a vacuuming operation is complete\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<journald.conf>(5), B<systemd>(1), B<systemd-journal-remote.service>(8), "
"B<systemd-journald.service>(8)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 252"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<systemd-journal-remote.service>(8), B<systemd>(1), B<systemd-journald."
"service>(8)"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "systemd 249"
msgstr ""
