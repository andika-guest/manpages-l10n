# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 16:56+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "expm1"
msgstr ""

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "expm1, expm1f, expm1l - exponential minus 1"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double expm1(double >I<x>B<);>\n"
"B<float expm1f(float >I<x>B<);>\n"
"B<long double expm1l(long double >I<x>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<expm1>():"
msgstr ""

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<expm1f>(), B<expm1l>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These functions return a value equivalent to"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "    exp(x) - 1\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The result is computed in a way that is accurate even if the value of I<x> "
"is near zero\\[em]a case where I<exp(x) - 1> would be inaccurate due to "
"subtraction of two numbers that are nearly equal."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "On success, these functions return I<exp(x)\\ -\\ 1>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is a NaN, a NaN is returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is +0 (-0), +0 (-0) is returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is positive infinity, positive infinity is returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is negative infinity, -1 is returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the result overflows, a range error occurs, and the functions return -"
"B<HUGE_VAL>, -B<HUGE_VALF>, or -B<HUGE_VALL>, respectively."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Range error, overflow"
msgstr ""

#
#. #-#-#-#-#  archlinux: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 specifies an optional range error (underflow) if
#.  x is subnormal.  glibc does not implement this.
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 specifies an optional range error (underflow) if
#.  x is subnormal.  glibc does not implement this.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 specifies an optional range error (underflow) if
#.  x is subnormal.  glibc does not implement this.
#. type: Plain text
#. #-#-#-#-#  fedora-39: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 specifies an optional range error (underflow) if
#.  x is subnormal.  glibc does not implement this.
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 specifies an optional range error (underflow) if
#.  x is subnormal.  glibc does not implement this.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 specifies an optional range error (underflow) if
#.  x is subnormal.  glibc does not implement this.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 specifies an optional range error (underflow) if
#.  x is subnormal.  Glibc does not implement this.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 specifies an optional range error (underflow) if
#.  x is subnormal.  glibc does not implement this.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE> (but see BUGS).  An overflow floating-point "
"exception (B<FE_OVERFLOW>)  is raised."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<expm1>(),\n"
"B<expm1f>(),\n"
"B<expm1l>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "C99, POSIX.1-2001.  BSD."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#. #-#-#-#-#  archlinux: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6778
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6778
#. type: Plain text
#. #-#-#-#-#  debian-unstable: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6778
#. type: Plain text
#. #-#-#-#-#  fedora-39: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6778
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6778
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6778
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6778
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Before glibc 2.17, on certain architectures (e.g., x86, but not x86_64)  "
"B<expm1>()  raised a bogus underflow floating-point exception for some large "
"negative I<x> values (where the function result approaches -1)."
msgstr ""

#. #-#-#-#-#  archlinux: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6814
#.  e.g., expm1(1e5) through expm1(1.00199970127e5),
#.  but not expm1(1.00199970128e5) and beyond.
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6814
#.  e.g., expm1(1e5) through expm1(1.00199970127e5),
#.  but not expm1(1.00199970128e5) and beyond.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6814
#.  e.g., expm1(1e5) through expm1(1.00199970127e5),
#.  but not expm1(1.00199970128e5) and beyond.
#. type: Plain text
#. #-#-#-#-#  fedora-39: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6814
#.  e.g., expm1(1e5) through expm1(1.00199970127e5),
#.  but not expm1(1.00199970128e5) and beyond.
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6814
#.  e.g., expm1(1e5) through expm1(1.00199970127e5),
#.  but not expm1(1.00199970128e5) and beyond.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6814
#.  e.g., expm1(1e5) through expm1(1.00199970127e5),
#.  but not expm1(1.00199970128e5) and beyond.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6814
#.  e.g., expm1(1e5) through expm1(1.00199970127e5),
#.  but not expm1(1.00199970128e5) and beyond.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Before approximately glibc 2.11, B<expm1>()  raised a bogus invalid floating-"
"point exception in addition to the expected overflow exception, and returned "
"a NaN instead of positive infinity, for some large positive I<x> values."
msgstr ""

#. #-#-#-#-#  archlinux: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It looks like the fix was in glibc 2.11, or possibly glibc 2.12.
#.  I have no test system for glibc 2.11, but glibc 2.12 passes.
#.  From the source (sysdeps/i386/fpu/s_expm1.S) it looks
#.  like the changes were in glibc 2.11.
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6788
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It looks like the fix was in glibc 2.11, or possibly glibc 2.12.
#.  I have no test system for glibc 2.11, but glibc 2.12 passes.
#.  From the source (sysdeps/i386/fpu/s_expm1.S) it looks
#.  like the changes were in glibc 2.11.
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6788
#. type: Plain text
#. #-#-#-#-#  debian-unstable: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It looks like the fix was in glibc 2.11, or possibly glibc 2.12.
#.  I have no test system for glibc 2.11, but glibc 2.12 passes.
#.  From the source (sysdeps/i386/fpu/s_expm1.S) it looks
#.  like the changes were in glibc 2.11.
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6788
#. type: Plain text
#. #-#-#-#-#  fedora-39: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It looks like the fix was in glibc 2.11, or possibly glibc 2.12.
#.  I have no test system for glibc 2.11, but glibc 2.12 passes.
#.  From the source (sysdeps/i386/fpu/s_expm1.S) it looks
#.  like the changes were in glibc 2.11.
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6788
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It looks like the fix was in glibc 2.11, or possibly glibc 2.12.
#.  I have no test system for glibc 2.11, but glibc 2.12 passes.
#.  From the source (sysdeps/i386/fpu/s_expm1.S) it looks
#.  like the changes were in glibc 2.11.
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6788
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It looks like the fix was in glibc 2.11, or possibly glibc 2.12.
#.  I have no test system for glibc 2.11, but glibc 2.12 passes.
#.  From the source (sysdeps/i386/fpu/s_expm1.S) it looks
#.  like the changes were in glibc 2.11.
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6788
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: expm1.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It looks like the fix was in glibc 2.11, or possibly glibc 2.12.
#.  I have no test system for glibc 2.11, but glibc 2.12 passes.
#.  From the source (sysdeps/i386/fpu/s_expm1.S) it looks
#.  like the changes were in glibc 2.11.
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6788
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Before glibc 2.11, the glibc implementation did not set I<errno> to "
"B<ERANGE> when a range error occurred."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<exp>(3), B<log>(3), B<log1p>(3)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#.  BSD.
#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXPM1"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Link with I<-lm>."
msgstr ""

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || _XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The result is computed in a way that is accurate even if the value of I<x> "
"is near zero\\(ema case where I<exp(x) - 1> would be inaccurate due to "
"subtraction of two numbers that are nearly equal."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#.  FIXME .
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6778
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"For some large negative I<x> values (where the function result approaches "
"-1), B<expm1>()  raises a bogus underflow floating-point exception."
msgstr ""

#.  FIXME .
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6814
#.  e.g., expm1(1e5) through expm1(1.00199970127e5),
#.  but not expm1(1.00199970128e5) and beyond.
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"For some large positive I<x> values, B<expm1>()  raises a bogus invalid "
"floating-point exception in addition to the expected overflow exception, and "
"returns a NaN instead of positive infinity."
msgstr ""

#.  It looks like the fix was in 2.11, or possibly 2.12.
#.  I have no test system for 2.11, but 2.12 passes.
#.  From the source (sysdeps/i386/fpu/s_expm1.S) it looks
#.  like the changes were in 2.11.
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6788
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Before version 2.11, the glibc implementation did not set I<errno> to "
"B<ERANGE> when a range error occurred."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
