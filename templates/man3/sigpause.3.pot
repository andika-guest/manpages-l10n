# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:22+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "sigpause"
msgstr ""

#. type: TH
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid "2023-07-20"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sigpause - atomically release blocked signals and wait for interrupt"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] int sigpause(int >I<sigmask>B<);  /* BSD (but see NOTES) */>\n"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] int sigpause(int >I<sig>B<);      /* POSIX.1 / SysV / UNIX 95 */>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Don't use this function.  Use B<sigsuspend>(2)  instead."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The function B<sigpause>()  is designed to wait for some signal.  It changes "
"the process's signal mask (set of blocked signals), and then waits for a "
"signal to arrive.  Upon arrival of a signal, the original signal mask is "
"restored."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If B<sigpause>()  returns, it was interrupted by a signal and the return "
"value is -1 with I<errno> set to B<EINTR>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux fedora-39 fedora-rawhide
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<sigpause>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#
#
#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On Linux, this routine is a system call only on the Sparc (sparc64)  "
"architecture."
msgstr ""

#.  Libc4 and libc5 know only about the BSD version.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"glibc uses the BSD version if the B<_BSD_SOURCE> feature test macro is "
"defined and none of B<_POSIX_SOURCE>, B<_POSIX_C_SOURCE>, B<_XOPEN_SOURCE>, "
"B<_GNU_SOURCE>, or B<_SVID_SOURCE> is defined.  Otherwise, the System V "
"version is used, and feature test macros must be defined as follows to "
"obtain the declaration:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr ""

#.  || (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Since glibc 2.26: _XOPEN_SOURCE E<gt>= 500"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "glibc 2.25 and earlier: _XOPEN_SOURCE"
msgstr ""

#
#.  For the BSD version, one usually uses a zero
#.  .I sigmask
#.  to indicate that no signals are to be blocked.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since glibc 2.19, only the System V version is exposed by I<E<lt>signal."
"hE<gt>>; applications that formerly used the BSD B<sigpause>()  should be "
"amended to use B<sigsuspend>(2)."
msgstr ""

# #-#-#-#-#  debian-bookworm: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#
#
# #-#-#-#-#  debian-unstable: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#
#
# #-#-#-#-#  mageia-cauldron: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#
#
#. #-#-#-#-#  archlinux: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-bookworm: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#. #-#-#-#-#  debian-unstable: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#. #-#-#-#-#  fedora-39: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-rawhide: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  mageia-cauldron: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001.  Obsoleted in POSIX.1-2008."
msgstr ""

#.  __xpg_sigpause: UNIX 95, spec 1170, SVID, SVr4, XPG
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The classical BSD version of this function appeared in 4.2BSD.  It sets the "
"process's signal mask to I<sigmask>.  UNIX 95 standardized the incompatible "
"System V version of this function, which removes only the specified signal "
"I<sig> from the process's signal mask.  The unfortunate situation with two "
"incompatible functions with the same name was solved by the B<\\"
"%sigsuspend>(2)  function, that takes a I<sigset_t\\ *> argument (instead of "
"an I<int>)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<kill>(2), B<sigaction>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<sigblock>(3), B<sigvec>(3), B<feature_test_macros>(7)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<int sigpause(int >I<sigmask>B<);  /* BSD (but see NOTES) */>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<int sigpause(int >I<sig>B<);      /* System V / UNIX 95 */>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"The System V version of B<sigpause>()  is standardized in POSIX.1-2001.  It "
"is also specified in POSIX.1-2008, where it is marked obsolete."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: SS
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "History"
msgstr ""

#. type: SS
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "Linux notes"
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "SIGPAUSE"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2017-09-15"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#
#
#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#.  Libc4 and libc5 know only about the BSD version.
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Glibc uses the BSD version if the B<_BSD_SOURCE> feature test macro is "
"defined and none of B<_POSIX_SOURCE>, B<_POSIX_C_SOURCE>, B<_XOPEN_SOURCE>, "
"B<_GNU_SOURCE>, or B<_SVID_SOURCE> is defined.  Otherwise, the System V "
"version is used, and feature test macros must be defined as follows to "
"obtain the declaration:"
msgstr ""

#. type: IP
#: opensuse-leap-15-6
#, no-wrap
msgid "*"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Glibc 2.25 and earlier: _XOPEN_SOURCE"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
