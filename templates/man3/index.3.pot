# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:02+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "index"
msgstr ""

#. type: TH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "index, rindex - locate character in string"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>strings.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]] char *index(const char *>I<s>B<, int >I<c>B<);>\n"
"B<[[deprecated]] char *rindex(const char *>I<s>B<, int >I<c>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<index>()  is identical to B<strchr>(3)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<rindex>()  is identical to B<strrchr>(3)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Use B<strchr>(3)  and B<strrchr>(3)  instead of these functions."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"4.3BSD; marked as LEGACY in POSIX.1-2001.  Removed in POSIX.1-2008, "
"recommending B<strchr>(3)  and B<strrchr>(3)  instead."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<strchr>(3), B<strrchr>(3)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "2023-01-05"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"4.3BSD; marked as LEGACY in POSIX.1-2001.  POSIX.1-2008 removes the "
"specifications of B<index>()  and B<rindex>(), recommending B<strchr>(3)  "
"and B<strrchr>(3)  instead."
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.05"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "INDEX"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2015-03-02"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<char *index(const char *>I<s>B<, int >I<c>B<);>\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<char *rindex(const char *>I<s>B<, int >I<c>B<);>\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<index>()  function returns a pointer to the first occurrence of the "
"character I<c> in the string I<s>."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<rindex>()  function returns a pointer to the last occurrence of the "
"character I<c> in the string I<s>."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The terminating null byte (\\(aq\\e0\\(aq) is considered to be a part of the "
"strings."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<index>()  and B<rindex>()  functions return a pointer to the matched "
"character or NULL if the character is not found."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"B<index>(),\n"
"B<rindex>()"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<memchr>(3), B<strchr>(3), B<string>(3), B<strpbrk>(3), B<strrchr>(3), "
"B<strsep>(3), B<strspn>(3), B<strstr>(3), B<strtok>(3)"
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
