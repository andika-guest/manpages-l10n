# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:21+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SETTERM"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "setterm - set terminal attributes"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<setterm> [options]"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<setterm> writes to standard output a character string that will invoke the "
"specified terminal capabilities. Where possible I<terminfo> is consulted to "
"find the string to use. Some options however (marked \"virtual consoles "
"only\" below) do not correspond to a B<terminfo>(5) capability. In this "
"case, if the terminal type is \"con\" or \"linux\" the string that invokes "
"the specified capabilities on the PC Minix virtual console driver is output. "
"Options that are not implemented by the terminal are ignored."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For boolean options (B<on> or B<off>), the default is B<on>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Below, an I<8-color> can be B<black>, B<red>, B<green>, B<yellow>, B<blue>, "
"B<magenta>, B<cyan>, or B<white>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"A I<16-color> can be an I<8-color>, or B<grey>, or B<bright> followed by "
"B<red>, B<green>, B<yellow>, B<blue>, B<magenta>, B<cyan>, or B<white>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The various color options may be set independently, at least on virtual "
"consoles, though the results of setting multiple modes (for example, B<--"
"underline> and B<--half-bright>) are hardware-dependent."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The optional arguments are recommended with \\(aq=\\(aq (equals sign) and "
"not space between the option and the argument. For example --"
"option=argument. B<setterm> can interpret the next non-option argument as an "
"optional argument too."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--appcursorkeys> on|off"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Sets Cursor Key Application Mode on or off. When on, ESC O A, ESC O B, etc. "
"will be sent for the cursor keys instead of ESC [ A, ESC [ B, etc. See the "
"I<vi and Cursor-Keys> section of the I<Text-Terminal-HOWTO> for how this can "
"cause problems for B<vi> users. Virtual consoles only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--append> I<console_number>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Like B<--dump>, but appends to the snapshot file instead of overwriting it. "
"Only works if no B<--dump> options are given."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--background> I<8-color>|default"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Sets the background text color."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--blank>[=0-60|force|poke]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Sets the interval of inactivity, in minutes, after which the screen will be "
"automatically blanked (using APM if available). Without an argument, it gets "
"the blank status (returns which vt was blanked, or zero for an unblanked "
"vt). Virtual consoles only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The B<force> argument keeps the screen blank even if a key is pressed."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The B<poke> argument unblanks the screen."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--bfreq>[=I<number>]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Sets the bell frequency in Hertz. Without an argument, it defaults to B<0>. "
"Virtual consoles only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--blength>[=0-2000]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Sets the bell duration in milliseconds. Without an argument, it defaults to "
"B<0>. Virtual consoles only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--blink> on|off"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Turns blink mode on or off. Except on a virtual console, B<--blink off> "
"turns off all attributes (bold, half-brightness, blink, reverse)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--bold> on|off"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"urns bold (extra bright) mode on or off. Except on a virtual console, B<--"
"bold off> turns off all attributes (bold, half-brightness, blink, reverse)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--clear>[=all|rest]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Without an argument or with the argument B<all>, the entire screen is "
"cleared and the cursor is set to the home position, just like B<clear>(1) "
"does. With the argument B<rest>, the screen is cleared from the current "
"cursor position to the end."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--clrtabs>[=I<tab1 tab2 tab3> ...]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Clears tab stops from the given horizontal cursor positions, in the range "
"B<1-160>. Without arguments, it clears all tab stops. Virtual consoles only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--cursor> on|off"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Turns the terminal\\(cqs cursor on or off."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--default>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Sets the terminal\\(cqs rendering options to the default values."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--dump>[=I<console_number>]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Writes a snapshot of the virtual console with the given number to the file "
"specified with the B<--file> option, overwriting its contents; the default "
"is I<screen.dump>. Without an argument, it dumps the current virtual "
"console. This overrides B<--append>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--file> I<filename>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Sets the snapshot file name for any B<--dump> or B<--append> options on the "
"same command line. If this option is not present, the default is I<screen."
"dump> in the current directory. A path name that exceeds the system maximum "
"will be truncated, see B<PATH_MAX> from I<linux/limits.h> for the value."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--foreground> I<8-color>|default"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Sets the foreground text color."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--half-bright> on|off"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Turns dim (half-brightness) mode on or off. Except on a virtual console, B<--"
"half-bright off> turns off all attributes (bold, half-brightness, blink, "
"reverse)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--hbcolor> [bright] I<16-color>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Sets the color for half-bright characters."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--initialize>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Displays the terminal initialization string, which typically sets the "
"terminal\\(cqs rendering options, and other attributes to the default values."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--inversescreen> on|off"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Swaps foreground and background colors for the whole screen."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--linewrap> on|off"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Makes the terminal continue on a new line when a line is full."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--msg> on|off"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Enables or disables the sending of kernel B<printk>() messages to the "
"console. Virtual consoles only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--msglevel> 0-8"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Sets the console logging level for kernel B<printk()> messages. All messages "
"strictly more important than this will be printed, so a logging level of "
"B<0> has the same effect as B<--msg on> and a logging level of B<8> will "
"print all kernel messages. B<klogd>(8) may be a more convenient interface to "
"the logging of kernel messages."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Virtual consoles only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--powerdown>[=0-60]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Sets the VESA powerdown interval in minutes. Without an argument, it "
"defaults to B<0> (disable powerdown). If the console is blanked or the "
"monitor is in suspend mode, then the monitor will go into vsync suspend mode "
"or powerdown mode respectively after this period of time has elapsed."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--powersave> I<mode>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Valid values for I<mode> are:"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<vsync|on>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Puts the monitor into VESA vsync suspend mode."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<hsync>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Puts the monitor into VESA hsync suspend mode."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<powerdown>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Puts the monitor into VESA powerdown mode."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<off>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Turns monitor VESA powersaving features."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--regtabs>[=1-160]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Clears all tab stops, then sets a regular tab stop pattern, with one tab "
"every specified number of positions. Without an argument, it defaults to "
"B<8>. Virtual consoles only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--repeat> on|off"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Turns keyboard repeat on or off. Virtual consoles only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--reset>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Displays the terminal reset string, which typically resets the terminal to "
"its power-on state."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--resize>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Reset terminal size by assessing maximum row and column. This is useful when "
"actual geometry and kernel terminal driver are not in sync. Most notable use "
"case is with serial consoles, that do not use B<ioctl>(3p) but just byte "
"streams and breaks."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--reverse> on|off"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Turns reverse video mode on or off. Except on a virtual console, B<--reverse "
"off> turns off all attributes (bold, half-brightness, blink, reverse)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--store>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Stores the terminal\\(cqs current rendering options (foreground and "
"background colors) as the values to be used at reset-to-default. Virtual "
"consoles only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--tabs>[=I<tab1 tab2 tab3> ...]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Sets tab stops at the given horizontal cursor positions, in the range "
"B<1-160>. Without arguments, it shows the current tab stop settings."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--term> I<terminal_name>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Overrides the B<TERM> environment variable."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--ulcolor> [bright] I<16-color>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Sets the color for underlined characters. Virtual consoles only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--underline> on|off"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Turns underline mode on or off."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "COMPATIBILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Since version 2.25 B<setterm> has support for long options with two hyphens, "
"for example B<--help>, beside the historical long options with a single "
"hyphen, for example B<-help>. In scripts it is better to use the backward-"
"compatible single hyphen rather than the double hyphen. Currently there are "
"no plans nor good reasons to discontinue single-hyphen compatibility."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Differences between the Minix and Linux versions are not documented."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<stty>(1), B<tput>(1), B<tty>(4), B<terminfo>(5)"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<setterm> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The optional arguments require \\(aq=\\(aq (equals sign) and not space "
"between the option and the argument. For example --option=argument."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Displays version information and exits."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Displays a help text and exits."
msgstr ""
