# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-10-02 12:02+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-EMU"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "April 2023"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "grub-emu - GRUB emulator"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<grub-emu> [I<\\,OPTION\\/>...]"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "GRUB emulator."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use GRUB files in the directory DIR [default=/boot/grub]"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-H>, B<--hold>[=I<\\,SECS\\/>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "wait until a debugger will attach"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-m>, B<--device-map>=I<\\,FILE\\/>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use FILE as the device map [default=/boot/grub/device.map]"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--memdisk>=I<\\,FILE\\/>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use FILE as memdisk"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-r>, B<--root>=I<\\,DEVICE_NAME\\/>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Set root device."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "give this help list"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "print program version"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If you are trying to install GRUB, then you should use B<grub-install>(8)  "
"rather than this program."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-emu> is maintained as a Texinfo manual.  "
"If the B<info> and B<grub-emu> programs are properly installed at your site, "
"the command"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<info grub-emu>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "September 2023"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12~rc1-10"
msgstr ""

#. type: TP
#: debian-unstable
#, no-wrap
msgid "B<-X>, B<--kexec>"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"use kexec to boot Linux kernels via systemctl (pass twice to enable "
"dangerous fallback to non-systemctl)."
msgstr ""
