# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:03+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "IPCRM"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "ipcrm - remove certain IPC resources"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<ipcrm> [options]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<ipcrm> [B<shm>|B<msg>|B<sem>] I<ID> ..."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<ipcrm> removes System V inter-process communication (IPC) objects and "
"associated data structures from the system. In order to delete such objects, "
"you must be superuser, or the creator or owner of the object."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"System V IPC objects are of three types: shared memory, message queues, and "
"semaphores. Deletion of a message queue or semaphore object is immediate "
"(regardless of whether any process still holds an IPC identifier for the "
"object). A shared memory object is only removed after all currently attached "
"processes have detached (B<shmdt>(2)) the object from their virtual address "
"space."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Two syntax styles are supported. The old Linux historical syntax specifies a "
"three-letter keyword indicating which class of object is to be deleted, "
"followed by one or more IPC identifiers for objects of this type."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The SUS-compliant syntax allows the specification of zero or more objects of "
"all three types in a single command line, with objects specified either by "
"key or by identifier (see below). Both keys and identifiers may be specified "
"in decimal, hexadecimal (specified with an initial \\(aq0x\\(aq or "
"\\(aq0X\\(aq), or octal (specified with an initial \\(aq0\\(aq)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The details of the removes are described in B<shmctl>(2), B<msgctl>(2), and "
"B<semctl>(2). The identifiers and keys can be found by using B<ipcs>(1)."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-a>, B<--all> [B<shm>] [B<msg>] [B<sem>]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Remove all resources. When an option argument is provided, the removal is "
"performed only for the specified resource types."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"I<Warning!> Do not use B<-a> if you are unsure how the software using the "
"resources might react to missing objects. Some programs create these "
"resources at startup and may not have any code to deal with an unexpected "
"disappearance."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-M>, B<--shmem-key> I<shmkey>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Remove the shared memory segment created with I<shmkey> after the last "
"detach is performed."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-m>, B<--shmem-id> I<shmid>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Remove the shared memory segment identified by I<shmid> after the last "
"detach is performed."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-Q>, B<--queue-key> I<msgkey>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Remove the message queue created with I<msgkey>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-q>, B<--queue-id> I<msgid>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Remove the message queue identified by I<msgid>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-S>, B<--semaphore-key> I<semkey>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Remove the semaphore created with I<semkey>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-s>, B<--semaphore-id> I<semid>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Remove the semaphore identified by I<semid>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"In its first Linux implementation, B<ipcrm> used the deprecated syntax shown "
"in the second line of the B<SYNOPSIS>. Functionality present in other *nix "
"implementations of B<ipcrm> has since been added, namely the ability to "
"delete resources by key (not just identifier), and to respect the same "
"command-line syntax. For backward compatibility the previous syntax is still "
"supported."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<ipcmk>(1), B<ipcs>(1), B<msgctl>(2), B<msgget>(2), B<semctl>(2), "
"B<semget>(2), B<shmctl>(2), B<shmdt>(2), B<shmget>(2), B<ftok>(3), "
"B<sysvipc>(7)"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<ipcrm> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr ""
