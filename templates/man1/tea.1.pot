# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-09-04 17:35+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "TEA"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "October 17, 2009"
msgstr ""

#
#.  Please adjust this date whenever revising the manpage.
#.  Some roff macros, for reference:
#.  .nh        disable hyphenation
#.  .hy        enable hyphenation
#.  .ad l      left justify
#.  .ad b      justify to both left and right margins
#.  .nf        disable filling
#.  .fi        enable filling
#.  .br        insert line break
#.  .sp <n>    insert n+1 empty lines
#.  for manpage-specific macros, see man(7)
#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "tea - text editor with syntax highlighting & UTF support"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<tea> files"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"TEA is a modest and easy-to-use Qt-based editor with many useful features "
"for HTML editing.  It features a small footprint, a tabbed layout engine, "
"support for multiple encodings, code snippets, templates, customizable "
"hotkeys, an \"open at cursor\" function for HTML files and images, "
"miscellaneous HTML tools, preview in external browser, string manipulation "
"functions, SRT subtitles editing, Morse-code tools, bookmarks, syntax "
"highlighting, and drag-and-drop support."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "--crapbook - start TEA with the Crapbook already opened."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"--charset=charset_name - set the charset for the opening file.  For example, "
"you want to open file1 with CP1251 charset, and file2 with UTF-8.  So you "
"write: tea --charset=cp1251 file1 --charset=utf-8 file2"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Please refer to chapter 16 - \"command line options\" at /usr/share/doc/tea-"
"data/en.html for more info."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "TEA was written by Peter Semiletov E<lt>peter.semiletov@gmail.comE<gt>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"This manual page was written by Lior Kaplan E<lt>kaplan@debian.orgE<gt>, for "
"the Debian project (but may be used by others)."
msgstr ""
