# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-10-02 12:16+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "PS2PS"
msgstr ""

#. type: TH
#: archlinux debian-unstable mageia-cauldron
#, no-wrap
msgid "13 September 2023"
msgstr ""

#. type: TH
#: archlinux debian-unstable mageia-cauldron
#, no-wrap
msgid "10.02.0"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Ghostscript Tools"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "ps2ps, eps2eps - Ghostscript PostScript \"distiller\""
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<ps2ps> [ I<options> ] I<input output.ps>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<eps2eps> [ I<options> ] I<input output.eps>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<ps2ps> uses I<gs>(1) to convert B<PostScript>(tm) or B<PDF>(tm) file "
"\"input\" to simpler, normalized and (usually) faster PostScript in \"output."
"ps\".  The output is level 2 DSC 3.0 conforming PostScript."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<eps2eps> performs the equivalent optimization, creating Encapsulated "
"PostScript (EPS) files. NB, despite the name, the input need not be an EPS "
"file, PostScript or indeed PDF files are equally acceptable."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Both accept any general Ghostscript command line options, and options "
"specific to the ps2write and eps2write devices."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Run \"B<gs -h>\" to find the location of Ghostscript documentation on your "
"system, from which you can get more details."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "ps2pdf(1), ps2ascii(1), ps2epsi(1)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "VERSION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
msgid "This document was last revised for Ghostscript version 10.02.0."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Artifex Software, Inc. are the primary maintainers of Ghostscript."
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "21 September 2022"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10.00.0"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This document was last revised for Ghostscript version 10.00.0."
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "27 March 2023"
msgstr ""

#. type: TH
#: fedora-39 fedora-rawhide
#, no-wrap
msgid "10.01.2"
msgstr ""

#. type: Plain text
#: fedora-39 fedora-rawhide
msgid "This document was last revised for Ghostscript version 10.01.2."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "4 April 2022"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "9.56.1"
msgstr ""

#. type: Plain text
#: opensuse-tumbleweed
msgid "This document was last revised for Ghostscript version 9.56.1."
msgstr ""
