# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-10-02 12:29+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-NOTIFY"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "systemd-notify"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"systemd-notify - Notify service manager about start-up completion and other "
"daemon status changes"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<systemd-notify >B<[OPTIONS...]>B< >B<[VARIABLE=VALUE...]>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"B<systemd-notify --exec >B<[OPTIONS...]>B< >B<[VARIABLE=VALUE...]>B< ; "
">B<[CMDLINE...]>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<systemd-notify> may be called by service scripts to notify the invoking "
"service manager about status changes\\&. It can be used to send arbitrary "
"information, encoded in an environment-block-like list of strings\\&. Most "
"importantly, it can be used for start-up completion notification\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is mostly just a wrapper around B<sd_notify()> and makes this "
"functionality available to shell scripts\\&. For details see "
"B<sd_notify>(3)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The command line may carry a list of environment variables to send as part "
"of the status update\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Note that systemd will refuse reception of status updates from this command "
"unless I<NotifyAccess=> is appropriately set for the service unit this "
"command is called from\\&. See B<systemd.service>(5)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Note that B<sd_notify()> notifications may be attributed to units correctly "
"only if either the sending process is still around at the time the service "
"manager processes the message, or if the sending process is explicitly "
"runtime-tracked by the service manager\\&. The latter is the case if the "
"service manager originally forked off the process, i\\&.e\\&. on all "
"processes that match I<NotifyAccess=>B<main> or I<NotifyAccess=>B<exec>\\&. "
"Conversely, if an auxiliary process of the unit sends an B<sd_notify()> "
"message and immediately exits, the service manager might not be able to "
"properly attribute the message to the unit, and thus will ignore it, even if "
"I<NotifyAccess=>B<all> is set for it\\&. To address this B<systemd-notify> "
"will wait until the notification message has been processed by the service "
"manager\\&. When B<--no-block> is used, this synchronization for reception "
"of notifications is disabled, and hence the aforementioned race may occur if "
"the invoking process is not the service manager or spawned by the service "
"manager\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<systemd-notify> will first attempt to invoke B<sd_notify()> pretending to "
"have the PID of the parent process of B<systemd-notify> (i\\&.e\\&. the "
"invoking process)\\&. This will only succeed when invoked with sufficient "
"privileges\\&. On failure, it will then fall back to invoking it under its "
"own PID\\&. This behaviour is useful in order that when the tool is invoked "
"from a shell script the shell process \\(em and not the B<systemd-notify> "
"process \\(em appears as sender of the message, which in turn is helpful if "
"the shell process is the main process of a service, due to the limitations "
"of I<NotifyAccess=>B<all>\\&. Use the B<--pid=> switch to tweak this "
"behaviour\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following options are understood:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<--ready>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Inform the invoking service manager about service start-up or configuration "
"reload completion\\&. This is equivalent to B<systemd-notify READY=1>\\&. "
"For details about the semantics of this option see B<sd_notify>(3)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--reloading>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Inform the invoking service manager about the beginning of a configuration "
"reload cycle\\&. This is equivalent to B<systemd-notify RELOADING=1> (but "
"implicitly also sets a I<MONOTONIC_USEC=> field as required for "
"I<Type=notify-reload> services, see B<systemd.service>(5)  for details)\\&. "
"For details about the semantics of this option see B<sd_notify>(3)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--stopping>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Inform the invoking service manager about the beginning of the shutdown "
"phase of the service\\&. This is equivalent to B<systemd-notify "
"STOPPING=1>\\&. For details about the semantics of this option see "
"B<sd_notify>(3)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<--pid=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Inform the service manager about the main PID of the service\\&. Takes a PID "
"as argument\\&. If the argument is specified as \"auto\" or omitted, the PID "
"of the process that invoked B<systemd-notify> is used, except if that\\*(Aqs "
"the service manager\\&. If the argument is specified as \"self\", the PID of "
"the B<systemd-notify> command itself is used, and if \"parent\" is specified "
"the calling process\\*(Aq PID is used \\(em even if it is the service "
"manager\\&. The latter is equivalent to B<systemd-notify MAINPID=$PID>\\&. "
"For details about the semantics of this option see B<sd_notify>(3)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If this switch is used in an B<systemd-notify> invocation from a process "
"that shall become the new main process of a service \\(em and which is not "
"the process forked off by the service manager (or the current main process) "
"\\(em, then it is essential to set I<NotifyAccess=all> in the service unit "
"file, or otherwise the notification will be ignored for security reasons\\&. "
"See B<systemd.service>(5)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<--uid=>I<USER>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set the user ID to send the notification from\\&. Takes a UNIX user name or "
"numeric UID\\&. When specified the notification message will be sent with "
"the specified UID as sender, in place of the user the command was invoked "
"as\\&. This option requires sufficient privileges in order to be able "
"manipulate the user identity of the process\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<--status=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Send a free-form human readable status string for the daemon to the service "
"manager\\&. This option takes the status string as argument\\&. This is "
"equivalent to B<systemd-notify STATUS=\\&...>\\&. For details about the "
"semantics of this option see B<sd_notify>(3)\\&. This information is shown "
"in B<systemctl>(1)\\*(Aqs B<status> output, among other places\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<--booted>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Returns 0 if the system was booted up with systemd, non-zero otherwise\\&. "
"If this option is passed, no message is sent\\&. This option is hence "
"unrelated to the other options\\&. For details about the semantics of this "
"option, see B<sd_booted>(3)\\&. An alternate way to check for this state is "
"to call B<systemctl>(1)  with the B<is-system-running> command\\&. It will "
"return \"offline\" if the system was not booted with systemd\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<--no-block>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Do not synchronously wait for the requested operation to finish\\&. Use of "
"this option is only recommended when B<systemd-notify> is spawned by the "
"service manager, or when the invoking process is directly spawned by the "
"service manager and has enough privileges to allow B<systemd-notify> to send "
"the notification on its behalf\\&. Sending notifications with this option "
"set is prone to race conditions in all other cases\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "B<--exec>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"If specified B<systemd-notify> will execute another command line after it "
"completed its operation, replacing its own process\\&. If used, the list of "
"assignments to include in the message sent must be followed by a \";\" "
"character (as separate argument), followed by the command line to "
"execute\\&. This permits \"chaining\" of commands, i\\&.e\\&. issuing one "
"operation, followed immediately by another, without changing PIDs\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Note that many shells interpret \";\" as their own separator for command "
"lines, hence when B<systemd-notify> is invoked from a shell the semicolon "
"must usually be escaped as \"\\e;\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "B<--fd=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Send a file descriptor along with the notification message\\&. This is "
"useful when invoked in services that have the I<FileDescriptorStoreMax=> "
"setting enabled, see B<systemd.service>(5)  for details\\&. The specified "
"file descriptor must be passed to B<systemd-notify> when invoked\\&. This "
"option may be used multiple times to pass multiple file descriptors in a "
"single notification message\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"To use this functionality from a B<bash> shell, use an expression like the "
"following:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd-notify --fd=4 --fd=5 4E<lt>/some/file 5E<lt>/some/other/file\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid "B<--fdname=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide opensuse-tumbleweed
msgid ""
"Set a name to assign to the file descriptors passed via B<--fd=> (see "
"above)\\&. This controls the \"FDNAME=\" field\\&. This setting may only be "
"specified once, and applies to all file descriptors passed\\&. Invoke this "
"tool multiple times in case multiple file descriptors with different file "
"descriptor names shall be submitted\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print a short version string and exit\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "On success, 0 is returned, a non-zero failure code otherwise\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<Example\\ \\&1.\\ \\&Start-up Notification and Status Updates>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A simple shell daemon that sends start-up notifications after having set up "
"its communication channel\\&. During runtime it sends further status updates "
"to the init system:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "#!/bin/sh\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"mkfifo /tmp/waldo\n"
"systemd-notify --ready --status=\"Waiting for data\\&...\"\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"while : ; do\n"
"        read -r a E<lt> /tmp/waldo\n"
"        systemd-notify --status=\"Processing $a\"\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "        # Do something with $a \\&...\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"        systemd-notify --status=\"Waiting for data\\&...\"\n"
"done\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-39 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-39 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemctl>(1), B<systemd.unit>(5), B<systemd.service>(5), "
"B<sd_notify>(3), B<sd_booted>(3)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 252"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<systemd-notify> may be called by daemon scripts to notify the init system "
"about status changes\\&. It can be used to send arbitrary information, "
"encoded in an environment-block-like list of strings\\&. Most importantly, "
"it can be used for start-up completion notification\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Note that systemd will refuse reception of status updates from this command "
"unless I<NotifyAccess=> is set for the service unit this command is called "
"from\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Note that B<sd_notify()> notifications may be attributed to units correctly "
"only if either the sending process is still around at the time PID 1 "
"processes the message, or if the sending process is explicitly runtime-"
"tracked by the service manager\\&. The latter is the case if the service "
"manager originally forked off the process, i\\&.e\\&. on all processes that "
"match I<NotifyAccess=>B<main> or I<NotifyAccess=>B<exec>\\&. Conversely, if "
"an auxiliary process of the unit sends an B<sd_notify()> message and "
"immediately exits, the service manager might not be able to properly "
"attribute the message to the unit, and thus will ignore it, even if "
"I<NotifyAccess=>B<all > is set for it\\&. When B<--no-block> is used, all "
"synchronization for reception of notifications is disabled, and hence the "
"aforementioned race may occur if the invoking process is not the service "
"manager or spawned by the service manager\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Hence, B<systemd-notify> will first attempt to invoke B<sd_notify()> "
"pretending to have the PID of the invoking process\\&. This will only "
"succeed when invoked with sufficient privileges\\&. On failure, it will then "
"fall back to invoking it under its own PID\\&. This behaviour is useful in "
"order that when the tool is invoked from a shell script the shell process "
"\\(em and not the B<systemd-notify> process \\(em appears as sender of the "
"message, which in turn is helpful if the shell process is the main process "
"of a service, due to the limitations of I<NotifyAccess=>B<all>\\&. Use the "
"B<--pid=> switch to tweak this behaviour\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Inform the init system about service start-up completion\\&. This is "
"equivalent to B<systemd-notify READY=1>\\&. For details about the semantics "
"of this option see B<sd_notify>(3)\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Inform the service manager about the main PID of the daemon\\&. Takes a PID "
"as argument\\&. If the argument is specified as \"auto\" or omitted, the PID "
"of the process that invoked B<systemd-notify> is used, except if that\\*(Aqs "
"the service manager\\&. If the argument is specified as \"self\", the PID of "
"the B<systemd-notify> command itself is used, and if \"parent\" is specified "
"the calling process\\*(Aq PID is used \\(em even if it is the service "
"manager\\&. This is equivalent to B<systemd-notify MAINPID=$PID>\\&. For "
"details about the semantics of this option see B<sd_notify>(3)\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Send a free-form status string for the daemon to the init systemd\\&. This "
"option takes the status string as argument\\&. This is equivalent to "
"B<systemd-notify STATUS=\\&...>\\&. For details about the semantics of this "
"option see B<sd_notify>(3)\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<systemd>(1), B<systemctl>(1), B<systemd.unit>(5), B<sd_notify>(3), "
"B<sd_booted>(3)"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "systemd 249"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "#!/bin/bash\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"while : ; do\n"
"        read a E<lt> /tmp/waldo\n"
"        systemd-notify --status=\"Processing $a\"\n"
msgstr ""
