# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:01+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "HARDLINK"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-08-04"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "hardlink - link multiple copies of a file"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<hardlink> [options] [I<directory>|I<file>]..."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<hardlink> is a tool that replaces copies of a file with either hardlinks "
"or copy-on-write clones, thus saving space."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<hardlink> first creates a binary tree of file sizes and then compares the "
"content of files that have the same size. There are two basic content "
"comparison methods. The B<memcmp> method directly reads data blocks from "
"files and compares them. The other method is based on checksums (like "
"SHA256); in this case for each data block a checksum is calculated by the "
"Linux kernel crypto API, and this checksum is stored in userspace and used "
"for file comparisons."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"For each file also an \"intro\" buffer (32 bytes) is cached. This buffer is "
"used independently from the comparison method and requested cache-size and "
"io-size.  The \"intro\" buffer dramatically reduces operations with data "
"content as files are very often different from the beginning."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Verbose output, explain to the user what is being done. If specified once, "
"every hardlinked file is displayed. If specified twice, it also shows every "
"comparison."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-q>, B<--quiet>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Quiet mode, don\\(cqt print anything."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-n>, B<--dry-run>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Do not act, just print what would happen."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-y>, B<--method> I<name>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Set the file content comparison method. The currently supported methods are "
"sha256, sha1, crc32c and memcmp. The default is sha256, or memcmp if Linux "
"Crypto API is not available. The methods based on checksums are implemented "
"in zero-copy way, in this case file contents are not copied to the userspace "
"and all calculation is done in kernel."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--reflink>[=I<when>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Create copy-on-write clones (aka reflinks) rather than hardlinks. The "
"reflinked files share only on-disk data, but the file mode and owner can be "
"different. It\\(cqs recommended to use it with B<--ignore-owner> and B<--"
"ignore-mode> options. This option implies B<--skip-reflinks> to ignore "
"already cloned files."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The optional argument I<when> can be B<never>, B<always>, or B<auto>. If the "
"I<when> argument is omitted, it defaults to B<auto>, in this case, "
"B<hardlink> checks filesystem type and uses reflinks on BTRFS and XFS only, "
"and fallback to hardlinks when creating reflink is impossible.  The argument "
"B<always> disables filesystem type detection and fallback to hardlinks, in "
"this case, only reflinks are allowed."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--skip-reflinks>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Ignore already cloned files. This option may be used without B<--reflink> "
"when creating classic hardlinks."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-f>, B<--respect-name>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Only try to link files with the same (base)name. It\\(cqs strongly "
"recommended to use long options rather than B<-f> which is interpreted in a "
"different way by other B<hardlink> implementations."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-p>, B<--ignore-mode>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Link and compare files even if their mode is different. Results may be "
"slightly unpredictable."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-o>, B<--ignore-owner>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Link and compare files even if their owner information (user and group) "
"differs. Results may be unpredictable."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-t>, B<--ignore-time>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Link and compare files even if their time of modification is different. This "
"is usually a good choice."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-c> B<--content>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Consider only file content, not attributes, when determining whether two "
"files are equal. Same as B<-pot>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-X>, B<--respect-xattrs>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Only try to link files with the same extended attributes."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-m>, B<--maximize>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Among equal files, keep the file with the highest link count."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-M>, B<--minimize>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Among equal files, keep the file with the lowest link count."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-O>, B<--keep-oldest>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Among equal files, keep the oldest file (least recent modification time). By "
"default, the newest file is kept. If B<--maximize> or B<--minimize> is "
"specified, the link count has a higher precedence than the time of "
"modification."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-x>, B<--exclude> I<regex>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"A regular expression which excludes files from being compared and linked."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-i>, B<--include> I<regex>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"A regular expression to include files. If the option B<--exclude> has been "
"given, this option re-includes files which would otherwise be excluded. If "
"the option is used without B<--exclude>, only files matched by the pattern "
"are included."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-s>, B<--minimum-size> I<size>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The minimum size to consider. By default this is 1, so empty files will not "
"be linked. The I<size> argument may be followed by the multiplicative "
"suffixes KiB (=1024), MiB (=1024*1024), and so on for GiB, TiB, PiB, EiB, "
"ZiB and YiB (the \"iB\" is optional, e.g., \"K\" has the same meaning as "
"\"KiB\")."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-S>, B<--maximum-size> I<size>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The maximum size to consider. By default this is 0 and 0 has the special "
"meaning of unlimited. The I<size> argument may be followed by the "
"multiplicative suffixes KiB (=1024), MiB (=1024*1024), and so on for GiB, "
"TiB, PiB, EiB, ZiB and YiB (the \"iB\" is optional, e.g., \"K\" has the same "
"meaning as \"KiB\")."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-b>, B<--io-size> I<size>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The size of the B<read>(2) or B<sendfile>(2) buffer used when comparing file "
"contents.  The I<size> argument may be followed by the multiplicative "
"suffixes KiB, MiB, etc.  The \"iB\" is optional, e.g., \"K\" has the same "
"meaning as \"KiB\". The default is 8KiB for memcmp method and 1MiB for the "
"other methods. The only memcmp method uses process memory for the buffer, "
"other methods use zero-copy way and I/O operation is done in the kernel. The "
"size may be altered on the fly to fit a number of cached content checksums."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-r>, B<--cache-size> I<size>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The size of the cache for content checksums. All non-memcmp methods "
"calculate checksum for each file content block (see B<--io-size>), these "
"checksums are cached for the next comparison. The size is important for "
"large files or a large sets of files of the same size. The default is 10MiB."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "ARGUMENTS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<hardlink> takes one or more directories which will be searched for files "
"to be linked."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The original B<hardlink> implementation uses the option B<-f> to force "
"hardlinks creation between filesystem. This very rarely usable feature is no "
"more supported by the current B<hardlink>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<hardlink> assumes that the trees it operates on do not change during "
"operation. If a tree does change, the result is undefined and potentially "
"dangerous. For example, if a regular file is replaced by a device, "
"B<hardlink> may start reading from the device. If a component of a path is "
"replaced by a symbolic link or file permissions change, security may be "
"compromised. Do not run B<hardlink> on a changing tree or on a tree "
"controlled by another user."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"There are multiple B<hardlink> implementations. The very first "
"implementation is from Jakub Jelinek for Fedora distribution, this "
"implementation has been used in util-linux between versions v2.34 to v2.36. "
"The current implementations is based on Debian version from Julian Andres "
"Klode."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<hardlink> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<hardlink> is a tool which replaces copies of a file with hardlinks, "
"therefore saving space."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "print quick usage details to the screen."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"More verbose output. If specified once, every hardlinked file is displayed, "
"if specified twice, it also shows every comparison."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Only try to link files with the same (basename). It\\(cqs strongly "
"recommended to use long options rather than B<-f> which is interpreted in a "
"different way by others B<hardlink> implementations."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Link/compare files even if their mode is different. This may be a bit "
"unpredictable."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Link/compare files even if their owner (user and group) is different. It is "
"not predictable."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Link/compare files even if their time of modification is different. You "
"almost always want this."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The original B<hardlink> implementation uses the option B<-f> to force "
"hardlinks creation between filesystem. This very rarely usable feature is no "
"more supported by the current hardlink."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<hardlink> assumes that the trees it operates on do not change during "
"operation. If a tree does change, the result is undefined and potentially "
"dangerous. For example, if a regular file is replaced by a device, hardlink "
"may start reading from the device. If a component of a path is replaced by a "
"symbolic link or file permissions change, security may be compromised. Do "
"not run hardlink on a changing tree or on a tree controlled by another user."
msgstr ""
